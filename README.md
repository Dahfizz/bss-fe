# bss-fe

    This is a demo web portal meant for integration with ONAP.
    Information on setting up the front and backend can be found on the ONAP wiki:
    https://wiki.onap.org/display/DW/Swisscom+BSS+UI+portal+mockup
