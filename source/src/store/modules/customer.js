import Vue from 'vue'
import Vuex from 'vuex'
// import apiServices from '../../services/apiServices'

Vue.use(Vuex)

const loadState = type => {
  try {
    const serializedState = localStorage.getItem(type)
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

const saveState = (state, type) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem(type, serializedState)
  } catch (err) {
    console.error(`Something went wrong: ${err}`)
  }
}

const state = loadState('state') || {
  products: {},
  selectedProduct: {},
  currentStep: 0,
  orders: [],
  order: {},
  moveAvailable: 0,
  logs: null,
  processingClass: 'progress_circle',
  completedClass: 'progress_circle',
  completedText: 'Order Completed',
  completedLineClass: 'progress_line',
  processingTopBorder: '',
  mainText:
    'Thank you for your order we are working on it. You will receive your Internet Box in a few days.',
  cardBoarder: 'card--green'
}
console.log('initial state', state)

const actions = {
  switchState (context) {
    console.log('state', context)
  }
}

const mutations = {
  setProducts (state, products) {
    state.products = products.products
  },
  setProduct (state, product) {
    state.selectedProduct = product
  },
  setCurrentStep (state, currentStep) {
    state.currentStep = currentStep
    if (currentStep === 0) {
      state = {}
    }
    saveState(state, 'state')
  },
  setOrders (state, orders) {
    state.orders = orders.orders
    saveState(state, 'state')
  },
  setOrder (state, order) {
    console.log('s order', order.state_description)
    state.order = order
    saveState(state, 'state')

    // switch bullet classes and texts
    switch (state.order.state_description) {
      case 'INPROGRESS':
        state.processingClass = 'progress_circle_in_progress'
        state.mainText =
          'Thank you for your order we are working on it. Please stand by....'
        state.completedClass = 'progress_circle'
        state.completedText = 'Order completed'
        state.completedLineClass = 'progress_line'
        state.cardBoarder = 'card--message-important'
        break
      case 'FAILED':
        state.processingClass = 'progress_circle_success'
        state.mainText = 'Your order has failed. Please...'
        state.completedClass = 'progress_circle_failed'
        state.completedText = 'Order failed'
        state.completedLineClass = 'progress_line_failed'
        state.cardBoarder = 'card--red'
        break
      case 'REJECTED':
        state.processingClass = 'progress_circle_success'
        state.mainText = 'Your order has rejected. Please...'
        state.completedClass = 'progress_circle_rejected'
        state.completedText = 'Order rejected'
        state.completedLineClass = 'progress_line_rejected'
        state.cardBoarder = 'card--red'
        break
      case 'COMPLETED':
        state.processingClass = 'progress_circle_success'
        state.mainText = 'Your internet connection is ready.'
        state.completedClass = 'progress_circle_success'
        state.completedText = 'Order completed'
        state.completedLineClass = 'progress_line_success'
        state.cardBoarder = 'card--green'
        break
      default:
        state.processingClass = 'progress_circle'
        state.mainText =
          'Thank you for your order we are working on it. You will receive your Internet Box in a few days.'
        state.completedClass = 'progress_circle'
        state.completedText = 'Order completed'
        state.completedLineClass = 'progress_line'
        state.cardBoarder = 'card--green'
        break
    }
  },
  setProcessingClass (state, processingClass) {
    state.processingClass = processingClass
  },
  setMainText (state, text) {
    state.mainText = text
  },
  setCardBoarder (state, stateClass) {
    state.cardBoarder = stateClass
  },
  setLogs (state, logs) {
    state.logs = logs.events
  }
}

const getters = {
  getProducts (state) {
    return state.products
  },
  getSelectedProduct (state) {
    return state.selectedProduct
  },
  getCurrentStep (state) {
    return state.currentStep
  },
  isBoring (state) {
    return state.currentStep === 1
  },
  getOrders (state) {
    return state.orders
  },
  getOrder (state) {
    return state.order
  },
  getMoveAvailable (state) {
    return state.moveAvailable
  },
  getLogs (state) {
    return state.logs
  },
  getProcessingClass (state) {
    return state.processingClass
  },
  getCompletedClass (state) {
    return state.completedClass
  },
  getCompletedText (state) {
    return state.completedText
  },
  getCompletedLineClass (state) {
    return state.completedLineClass
  },
  getMainText (state) {
    return state.mainText
  },
  getCardBoarder (state) {
    return state.cardBoarder
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
