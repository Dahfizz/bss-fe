import Vue from 'vue'
import Vuex from 'vuex'

import { customer, operator } from './modules'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    customer,
    operator
  }
})

export default store
