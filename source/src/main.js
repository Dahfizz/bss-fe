// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '@/assets/sdx/dist/css/sdx.css'
import '@/assets/sdx/dist/js/sdx.min.js'
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import store from './store'
import { defineCustomElements } from '@/assets/sdx/dist/js/webcomponents/esm/es5/webcomponents.define.js'

Vue.config.ignoredElements = [/sdx-\w*/]

defineCustomElements(window)

Vue.use(Vuex)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
  isBoring: false
})
