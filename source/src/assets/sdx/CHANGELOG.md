# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.4.7] - 2018-02-28
### Added
- The `sdxcode` npm module is now available on the [internal](https://artifactory.swisscom.com/api/npm/oxd-ref-npm-virtual/sdxcode) and [external](https://bin.swisscom.com/api/npm/oxd-ref-npm-virtual/sdxcode) registries, see SDX-35, thx @mario.goller

### Fixed
- Fixes select dropdown width overflow, see SDX-51, thx @adam.misuda
- Fixes error when using tab key on dropdown with no selected item, see SDX-78, thx @adam.misuda

## [1.4.4] - 2018-02-27
### Fixed
- Fixes single select dropdowns with no placeholder option: selection cannot be deselected, see SDX-68, thx @bruno.ritz

## [1.4.3] - 2018-02-22
### Fixed
- Fixes select dropdowns with more than 10 items on IE11, see SDX-65, thx @matthias.inauen

## [1.4.2] - 2018-01-26
### Fixed
- Fixes Autocomplete tab key to select a suggestion, see SDX-22, thx @daniel.hofmann

## [1.4.1] - 2018-01-09
### Fixed
- Fixes `HTMLSelectElement.selectedOptions` as it causes trouble in IE, see SDX-37, thx @Benjamin Windler
- Fixes typo with `destoy()` method (backwards compatible), see SDX-47, thx @Ces

### Added
- Adds icons `254` to `256`, see SDX-46.

## [1.4] - 2017-11-25
### Fixed
- Fixes Autocomplete not working with entries starts with a "+", see #89
- Fixes missaligned shadow on flyout indicator, see #94, thx @emanuel.schaedler
- Fixes IE10 input field validation icon broken in ie10, see #92, thx @emanuel.schaedler
- Fixes invalid tablehead centering on Win7/IE11, see #91, thx @emanuel.schaedler
- Fixes typos with `destroy()` methods (backwards compatible), see SDX-36, thx @Ces
- Fixes iOS 11 input field bug when displayed in a modal
- Fixes p fontsize in links

### Changed
- Change select selected items search scope, see #88, thx @oliveti

### Added
- Added NodeJS 9 support

## [1.3.6] - 2017-10-06
### Added
- Adds icons `252` to `253`.

## [1.3.5] - 2017-09-29
### Fixed
- Fixes null reference error when closing notification header #85, thx @florin.
- Fixes first options always selected in Menu (dropdown), #83
- Fixes LoadingSpinner alignment with ZoomLevel, #86
- Fixes readme formatting

## [1.3.4] - 2017-09-17
### Fixed
- Flyout menu is no longer suppressing click events, this fixes #74. Thx @denu5

### Added
- Adds opened/closed events to `MenuFlyout` component. See #81.
- Notification header checks the return value of the message click callback function before closing, See #82.
- Modal dialog content now scrolls content.

## [1.3.3] - 2017-08-25
### Fixed
- Fixes an issue with wrapped font definitions in `sdx-wrapped`. See Issue #80, thx @ces.

## [1.3.2] - 2017-08-21
### Added
- Adds icons `242` to `251`.
- Adds documentation for visibility utilities. See Issue #76
- Fix flyout destroy listener !68 thx @oliveti

### Fixed
- Fixes issue with anchor styling on n-th anchor in text. See Issue #75.
- Wraps now everything in sdx-container in the sdx-wrapped file. See Issue #73.

## [1.3.1] - 2017-07-12
### Added
- Adds `input-field--static-label` class to InputField.

## [1.3] - 2017-07-11
### Added
- Tabs
- Improves `Modal` JS documentation by adding event
- `destroy()` method to `Modal` JS component
- Modal dialog now supports multiple trigger elements
- Updates minimal NodeJS version to 4.5

### Fixed
- Fixes a display issue with the checkbox hover state

## [1.2.3] - 2017-06-26
### Added
- Jenkins CI build script.

### Fixed
- Fixes issues with invalid icon-class to icon-font matching, this removes support for #57.

## [1.2.2] - 2017-06-23
### Added
- Adds `sdx-no-modernizr` javascript bundle.

### Fixed
- Fixes an exception if `destroy()` is calles on the `autocomplete` component.
- !67 Table sorting fix: changes columnIndex comparison to columns, thx @gobeli

## [1.2.1] - 2017-06-14
### Added
- Adds `change` event to `Autocomplete` component.
- Adds `value` property to `Autocomplete` component.

## [1.2.0] - 2017-06-02
### Added
- Adds autocompletion support to text-input field using the new `Autocomplete` component.
- Adds utilities section to styleguide. Utilities section includes documentation for common helpers like:
  - Clearfix
  - Margins / Paddings
  - Screenreaders
  - Text
- Adds accessibility focus states to Radio and Checkbox elements

## [1.1.3] - 2017-06-01
### Added
- Makes flyout animation duration configurable, this fixes #61
- Exposes `TextArea` API on the sdx object, thx @oliveti
- Registers gulp tasks in npm (`dev`, `build`,``dist`) use with `npm start <command>`.
- Input Fields: Adds `invalid invalid--inline` state
- Exports `InputField` to `sdx` global scope
- Adds `showError(text)` function to `InputField` to add and remove error messages on input fields.
- Adds error message support to:
  - Checkboxes
  - Radio buttons
  - Dropdown menu
- Adds inline support for radio buttons and checkboxes
- Adds grouping for radio buttons and checkboxes
- Expose textarea api #60

### Fixed
- Fixes an issue where labels would appear on InputFields even if Chrome did not autofill any value
- Fixes keyboard selection issue in the `Select` component.
- Fixes ie bug in notification modal (see Trello Issue: https://trello.com/c/mtcUHjlX)
- Fixes Media queries behave incorrectly at breakpoints at non-100% browser zoom on Firefox & Edge #67
- Fixes Pointless numbers in icon names #57

## [1.1.2] - 2017-04-03
### Added
- Adds icons `229` to `241`.
- Adds digitalexperience icon sample page
- Adds SDX version number to generated css files
- Adds SDX version number variable `sdx.VERSION` to bundle

### Changed
- merges gulp `sass` and `dist:sass` tasks into one
- `normalize.scss` is now included in source, see #43

### Fixed
- Fixes an issue where the footer is not correctly displayed on large screens in safari, see #59 thx @fliptation
- Fixes an issue where footer items collapsed on mobile would be hidden after resize to desktop breakpoints
- Fixes responsive wrapping issue in `button-group`
- Fixes browser-sync css reload task
- Fixes sourcemap generation by removing `includecss` gulp plugin

## [1.1.1] - 2017-03-27
### Added
- Adds icon `227-speech-bubble-filled`
- Adds icon `228-star-filled`

### Fixed
- Pie Chart: fixes #55 by adjusting the minimal slice size
- Input Fields: fixes missing label issue when chrome autofill is used

## [1.1.0] - 2017-03-02
### Added
- Charts
  - Vertical Bar Charts
- Comments
- Table
  - Display table styles
- Documentation
  - Documentation for `b1, b2` text styles

### Fixed
- Removes redundant gridsystem classes. This reduces the minified css to 270KB (30KB less then before).
- selectedOptions polyfill breaks JS in safari mobile 9 #50
- Updates replay icon `icon-066-replay`

## [1.0.0] - 2017-02-10
>> This release contains breaking changes!

### Changed
- Removes obsolete code from pre 1.0 versions #47
    - JS: 
        - Removes fallback support for `init="auto"` attribute. Update all existing attributes to `data-init="auto"`
    - SASS:
        - Changes some color variable names (eg. `$color-blue--40` to `$color-blue-40`)
    - CSS:
        - Links: `a` link element requires the `link` class
        - List: list elements (`ul`, `ol`, `dt`) requires the `list` class
        - Badges: replaces class `badge__content__text` with `badge__text`
        - Changes some color classes (eg. `color-blue--40` to `color-blue-40`)
        - `text--underline` is replaced with `text-underline` on the following components:
            - Accordion Sidebar
            - Navigation Level 0
            - Navigation Level 1

## [0.8.1] - 2017-02-10
### Fixed
- Removes invalid styleguide entry for image table styles

## [0.8.0] - 2017-02-10
### Added
- Menu
  - Flyout
  - Context
- Charts
  - Pie Charts
  - Horizontal Bar Charts
- Navigation
  - Live Search
- Updates `ul` and `ol` lists to support the classname `list` in the future (see Trello Issue: https://trello.com/c/mnawNAZv)
- Adds new sdx-wrapped.css output that wraps all sdx definitions with the `sdx-container` class.
- Makes sure the modal backdrop is placed inside the `sdx-container` if one is found in the current document.
- !46 Adds support for the yarn package manager to ensure deterministic package installs for release builds on the CI server.
- New SDX Icons 203 - 206

### Fixed
- Fixes button group in IE
- Parsing error in SVG font files `TheSerifB_600_svg` and `TheSerifB_700i.svg`
- SDX-Wrapper: apply html and body styles to sdx-container #46

## [0.7.3] - 2017-02-07
### Fixed
- select dropdown not working on IE10 #44

### Added
- adds clean-css to the build pipeline to minify sdx #45

## [0.7.2] - 2017-01-31
### Fixed
- Disable checkbox hover on mobile devices #38
- Updates checkbox to support multiple lines of text (see Trello Issue: https://trello.com/c/ssj0VHQt)

## [0.7.1] - 2017-01-30
### Fixed
- Wrong import type for normalize.css #41

## [0.7.0] - 2017-01-30
### Added
- Carousel
- Table
  - Data-Driven
  - Responsive
  - Reflow
  - Display
- Updated all components to use new `data-init` attribute. Added obsolescence warning for old attributes.
- Card: News
- Tests with Node.js 7 (#17)
- JavaScipt component documentation at [/doc](https://sdxcode.swisscom.ch/doc).

### Fixed
- Range Slider issue on Nexus 6 #11
- Dropdown content is not visible beyond 400px. #21 thx @gobeli
- Fixes build error with `normalize.css` import

## [0.6.9] - 2017-01-25
### Fixed
- Edge: Loading spinner not displayed correctly (no animation) #37

## [0.6.8] - 2017-01-16
### Fixed
- Documentation typo in icons #25
- Typo in colors #27
- Dark background spinner has border #30
- Modal alignment on IE 11 (not centered) #31
- "badge__content__text" Naming #32
- Free slider bad cursor (range thumb) position for min/max values. #33 thx @samcontesse
- Free slider not working correctly when min value is bigger than zero. #34 thx @samcontesse

## [0.6.7] - 2017-01-04
### Added
- Adds full width helper class to use in responsive containers

## [0.6.6] - 2016-12-23
### Fixed
- Modal titles get cut off at the bottom #23

## [0.6.5] - 2016-12-23
### Fixed
- Height calculation issues if `textarea` components are hidden on initialization #22.

## [0.6.4] - 2016-12-13
### Added
- Grid: adds `container-fluid` class to the grid system

## [0.6.3] - 2016-12-09
### Fixed
- !40 Fixes fonts loaded through scss, thx @mastertiner

## [0.6.2] - 2016-12-09
### Fixed
- Fixes Fontpath Issue with TheSerif Font. #18

## [0.6.1] - 2016-12-08
### Added
- Cards
  - Documents
  - Travel

### Fixed
- Fixes markup where `div` tags where used inside `button` elements. This also fixes stability issues with the FF Inspector.
- Makes normalize.css an NPM dependency, #13 thx @mastertinner
- !36 Removes unknown invisible characters from scss files, thx @mastertinner
- !37 Add $image-path variable, thx @mastertinner

## [0.6.0] - 2016-11-30
### Added
- Accordion
- Cards
  - Uploaded Photos
  - Uploaded Files
  - Corner option for cards
- Footer
  - Full
- Search
  - Input
- Textarea
- Navigation Tabs
  
### Fixed
- Fixes Accordion Aria Labels
- Adds a SCSS `$font-path` variable for customizing the font path, #7 thx @mastertinner
- Import of normalize doesn't work with Angular CLI, #8 thx @mastertinner
- Input fields now require the `input-field` wrapper class
- Fixes issues with displaying navigation items on IE
- Fixes Slider touch event issue on Google Chrome for Android #11

## [0.5.6] - 2016-11-16
### Added
- The nova flag now links to nova.swisscom.com

## [0.5.5] - 2016-11-04
### Added
- Cards
 - General Notification
 - Actionable Notification
 - Image Top Notification
 - Image Bottom Notification
 - Promo Notification
 - Notes Notification
- Footer
 - Compact
- Empty States
 - Standalone
 - Modal

### Fixed
- Fixes wordmark on retina displays bug
- Fixes modal dialog page in the styleguide
- Makes sure modal dialogs are not measured when hidden by setting them to `display: none;`
- Changes sample code to Pug template engine

## [0.5.4] - 2016-10-20
### Added
- Flexbox Grid documentation

### Fixed
- Fixes Menu z-index arrow bug

## [0.5.3] - 2016-10-15
### Added
- The lifeform is now animated using png sprites

### Fixed
- Fixes Navigation Level 1 Margin

## [0.5.2] - 2016-10-15
### Added
- Navigation
  - Adds support for Level 1 navigation sections without content sections
  - Adds Nova Flag

## [0.5.1] - 2016-10-15
### Added
- Navigation Level 0 (CFU Switcher)
- Navigation Level 1 (Mega Menu)
- `.button-group--fill` that makes buttons in the group fill the container space and share the width equally.
- Navigation Mobile

### Fixed
- Removed BG Dark and Gray Paddings
- Fixes vertical spacing issues with validated form fields.
- Adds optional `modifierClass` parameter to `Notification.showOnHeader` method to specify the notification type.
- Fixes form field error-message icon alignment
- Fixes z-index issue with notification header
- Replaces Swisscom Fonts with the most current ones

## [0.4.1] - 2016-09-20
### Fixes
- Updates iconfont baseline
- Updates icon button

## [0.4.0] - 2016-09-20
### Added
- Progress (Light)
- Progress (Full)
- Tooltip
- Toolbar
- Notification Header
- Notification Modal
- Sharing Icon

### Fixed
- Updated Heart and Exclamation Mark Icon
- Fixes link size when nested in p tag
- Fixes Modal dialog issues
- Fixes Thin Button states
- Adds a cancel callback to the Notification component

## [0.3.0] - 2016-09-05
### Added
- Icons
- Buttons
 - Icons Buttons
 - Responsive Buttons
- Badges
- Responsive Mixins
- Select
- Range Sliders
- Modal Dialogs

### Fixed
- Global text colors

### Updated
- Updated npm packages to latest versions
- Updates scss linter ruleset

## [0.2.0] - 2016-08-10
### Added
- Lists
- Switches
- Loaders
 - Loading Spinner
 - Loading Bar
- Form Fields
 - Input
 - Checkbox
 - Radio

### Fixed
- Adds missing TextLink underline on dark backgrounds

## [0.1.1] - 2016-07-13
### Added
- Buttons
- Dividers
- Text Links
- Typography definitions
- Colors
- Grid System
