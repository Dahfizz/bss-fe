//
// Tooltip
// --------------------------------------------------

/*doc
---
title: Tooltip Overview
name: 02-components-18-tooltip-01-overview
category: Components - Toolbar & Tooltips
---

<style>
  .tooltip {
    margin: 24px 12px 0 12px;
  }

  .tooltip:first-child {
    margin-left: 0;
  }

  .tooltip--left {
    margin-left: 4px;
    padding-left: 8px;
  }

  .tooltip--right {
    margin-right: 4px;
    padding-right: 8px;
  }

</style>

SDX provides a Tooltip with two alignments. Top and right aligned.

*/

/*doc
---
title: Tooltip Usage
name: 02-components-18-tooltip-02-usage
category: Components - Toolbar & Tooltips
---

**Note**: To make tooltips work on mobile make sure that a click handler is present on the
element. For non button or link elements just add `onclick="void(0)"` to your markup.

```html_example
<div class="tooltip tooltip--top" aria-label="Tooltip top" onclick="void(0)">
  Tooltip on top
</div>
<div class="tooltip tooltip--bottom" aria-label="Tooltip bottom" onclick="void(0)">
  Tooltip on bottom
</div>
<div class="tooltip tooltip--left" aria-label="Tooltip left" onclick="void(0)">
  Tooltip on left
</div>
<div class="tooltip tooltip--right" aria-label="Tooltip right" onclick="void(0)">
  Tooltip on right
</div>
```
*/


// --------------------------------------------------

$tooltip-zindex: 1000000;

$tooltip-fontsize: 16px;

$tooltip-max-width: 200px;
$tooltip-height: 32px;

$tooltip-padding-vertical: 8px;
$tooltip-padding-horizontal: 15px;

$tooltip-arrow-size: 8px;

$tooltip-offset-vertical: 2px;
$tooltip-offset-horizontal: 2px;

$tooltip-transition-distance: 8px;

// --------------------------------------------------


// Tooltip wrapper styles
// --------------------------------------------------

.tooltip {
  display: inline-block;
  position: relative;

  z-index: 1;
  outline: none;

  // Fixing iOS Safari event issue.
  // More info at: https://goo.gl/w8JF4W
  cursor: pointer;

  &::before,
  &::after {
    position: absolute;

    transform: translate3d(0, 0, 0);

    visibility: hidden;
    opacity: 0;

    z-index: $tooltip-zindex;
    pointer-events: none;

    transition:
      visibility 200ms $standard-easing,
      opacity 200ms $standard-easing,
      transform 300ms $standard-easing;

    transition-delay: 0ms;
  }

  &:hover {
    &::before,
    &::after {
      visibility: visible;
      opacity: 1;

      transition-delay: 100ms;
    }
  }

  // Tooltip arrow
  // -------------------------

  &::before {
    position: absolute;

    background: 0 0;
    border: $tooltip-arrow-size solid transparent;

    z-index: $tooltip-zindex + 1;

    content: '';
  }

  // Tooltip body
  // -------------------------

  &::after {
    @include font-sans;
    @include font-semi-light;
    @include text-truncate;
    @include font-smoothing;

    max-width: $tooltip-max-width;
    height: $tooltip-height;

    padding: $tooltip-padding-vertical $tooltip-padding-horizontal;

    color: $color-white;

    border-radius: 8px;
    background: $color-int-blue--active;

    font-size: $tooltip-fontsize;
    line-height: $tooltip-fontsize;

    content: attr(aria-label);
  }

  // Tooltip default / top
  // -------------------------

  &,
  &--top {
    &::before,
    &::after {
      left: 50%;
      bottom: calc(100% + #{$tooltip-offset-vertical});

      margin: 0;
    }

    &::before {
      left: calc(50% - #{$tooltip-arrow-size});
      margin-bottom: -2 * $tooltip-arrow-size + 1px;

      border-color: transparent;
      border-top-color: $color-int-blue--active;
    }

    &::after {
      transform: translateX(-50%);
    }

    // Hover
    // -------------------------

    &:hover,
    &:focus {
      &::before {
        transform: translateY(-1 * $tooltip-transition-distance);
      }

      &::after {
        transform:
          translateX(-50%)
          translateY(-1 * $tooltip-transition-distance);
      }
    }
  }

  // Tooltip bottom
  // -------------------------

  &--bottom {
    &::before,
    &::after {
      left: 50%;
      top: calc(100% + #{$tooltip-offset-vertical});
      bottom: auto;

      margin: 0;
    }

    &::before {
      left: calc(50% - #{$tooltip-arrow-size});
      margin-top: -2 * $tooltip-arrow-size + 1px;

      border-color: transparent;
      border-bottom-color: $color-int-blue--active;
    }

    &::after {
      transform: translateX(-50%);
    }

    // Hover
    // -------------------------

    &:hover,
    &:focus {
      &::before {
        transform: translateY($tooltip-transition-distance);
      }

      &::after {
        transform:
          translateX(-50%)
          translateY($tooltip-transition-distance);
      }
    }
  }

  // Tooltip left
  // -------------------------

  &--left {
    &::before,
    &::after {
      left: auto;
      right: calc(100% + #{$tooltip-offset-horizontal});
      bottom: 50%;

      margin: 0;
    }

    &::before {
      margin-right: -2 * $tooltip-arrow-size + 1px;
      margin-bottom: -1 * $tooltip-arrow-size;

      border-color: transparent;
      border-left-color: $color-int-blue--active;


    }

    &::after {
      transform:
        translateX(0)
        translateY(0);

      margin-bottom: -1 * floor($tooltip-height / 2);
    }

    // Hover
    // -------------------------

    &:hover,
    &:focus {
      &::before {
        transform:
          translateX(-1 * $tooltip-transition-distance);
      }

      &::after {
        transform:
          translateX(-1 * $tooltip-transition-distance)
          translateY(0);
      }
    }
  }

  // Tooltip right
  // -------------------------

  &--right {
    &::before,
    &::after {
      left: calc(100% + #{$tooltip-offset-horizontal});
      bottom: 50%;

      margin: 0;
    }

    &::before {
      margin-left: -2 * $tooltip-arrow-size + 1px;
      margin-bottom: -1 * $tooltip-arrow-size;

      border-color: transparent;
      border-right-color: $color-int-blue--active;
    }

    &::after {
      transform:
        translateX(0)
        translateY(0);

      margin-bottom: -1 * floor($tooltip-height / 2);
    }

    // Hover
    // -------------------------

    &:hover,
    &:focus {
      &::before {
        transform:
          translateX($tooltip-transition-distance);
      }

      &::after {
        transform:
          translateX($tooltip-transition-distance)
          translateY(0);
      }
    }
  }

  // Multiline support
  // -------------------------

  &--multiline::after {
    height: auto;

    text-align: center;

    white-space: pre-wrap;
    word-break: break-all;
  }

  // Hide Tooltip if aria-label is empty
  // -------------------------

  &:not([aria-label]),
  &[aria-label=''] {
    &::before,
    &::after {
      display: none !important;
    }
  }
}
