//
// Modal
// --------------------------------------------------

/*doc
---
title: Modal dialog
name: 02-components-13-modals-01-overview
category: Components - Modals
---

Modals are used to display content in a layer above the app. There can only be one active modal dialog at a given time.

#### Interactive sample
A responsive test page for the Modal dialog is available at [samples/modal](/samples/modal.html).
In additon to the responsive behavior it also demonstrates how to open and close the modal dialog.
*/

/*doc
---
title: Usage
name: 02-components-13-modals-02-usage
category: Components - Modals
---

<style>
  .codeExample .backdrop {
    position: absolute;
    width: 100%;
    height: 100%;
  }

  .codeExample .modal {
    position: relative;
  }

  codeExample. .modal__content {
    margin: 24px;
    width: 100%;
  }
</style>

There are serveral options for opening and closing a modal dialog, either use the `Modal` JavaScript component
directly and call the `open()` and `close()` methods on the component; or add a button with the class `modal-trigger`
and a `href` attribute with the id of the target modal dialog.

```html
<button class="button button--primary modal-trigger" href="myModalDialog">Open modal</button>
```

To close the dialog either call `close()` on the component manually or apply either the `modal-close` or `modal-cancel` class
to a button in the current modal dialog. Both of this states will dispatch an event when executed, this enables other frontend code
to react to the selected user action if required.

#### Modal dialog with two buttons

```html_example
<div class="modal modal--open" role="dialog" tabindex="-1">
  <div class="modal__content">
    <div class="modal__header">
      <h1>Would you like to delete this file?</h1>
      <button class="modal__close modal-cancel" aria-label="Close">
        <i class="icon icon-022-close" aria-hidden="true"></i>
      </button>
    </div>
    <div class="modal__body">
      <p>
        Do you really want to delete <strong>SDX_Library_Master.git</strong>
      </p>
      <div class="button-group button-group--right">
        <button class="button button--responsive button--primary align-right modal-close">
          Delete
        </button>
        <button class="button button--responsive button--secondary modal-cancel">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>

<div class="backdrop backdrop--open"></div>
```


#### Modal dialog with three buttons

```html_example
<div class="modal modal--open" role="dialog" tabindex="-1">
  <div class="modal__content">
    <div class="modal__header">
      <h1>Would you like to delete this file?</h1>
      <button class="modal__close modal-cancel" aria-label="Close">
        <i class="icon icon-022-close" aria-hidden="true"></i>
      </button>
    </div>
    <div class="modal__body">
      <p>
        Do you really want to delete <strong>SDX_Library_Master.git</strong>
      </p>
      <div class="button-group button-group--right">
        <button class="button button--responsive button--secondary">
          Learn More
        </button>
        <button class="button button--responsive button--primary modal-close">
          Delete
        </button>
        <button class="button button--responsive button--secondary modal-cancel">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>
<div class="backdrop backdrop--open"></div>
```
*/

// --------------------------------------------------

// Modal open trigger
// -------------------------

.js-modal-open {
  position: fixed;
  width: 100%;
  overflow: hidden;
}

// Modal Dialog
// --------------------------------------------------

.modal {

  // NOTE: This is set to flex in JS to makes sure the animations work
  // and the modal does not take up space when hidden.
  display: none;

  position: fixed;
  top: 0;
  left: 0;

  z-index: $zindex-modal + 1;

  width: 100%;
  height: 100%;

  outline: none;
  pointer-events: none;

  &--open {
    display: flex;

    .modal__content {
      transform: scale(1);

      visibility: visible;
      opacity: 1;
    }

  }

  // Container
  // -------------------------

  > .container {
    display: flex;
  }

  // Modal Content
  // -------------------------

  &__content {
    position: relative;

    margin: auto;
    max-height: calc(100vh - #{$modal-spacing-height});

    align-self: center;

    padding: 0;

    box-shadow: 0 0 4px rgba($color-black, .15);
    background: $color-white;

    overflow-x: hidden;
    overflow-y: auto;

    pointer-events: auto;

    visibility: hidden;
    opacity: 0;

    transform: scale(.92);
    transition:
      transform 300ms 50ms,
      opacity 300ms 50ms,
      visibility 300ms 50ms;

  }

  // Modal Close Button
  // -------------------------

  &__close {
    position: absolute;
    top: 16px;
    right: 13px;

    width: 32px;
    height: 32px;

    cursor: pointer;
    color: $color-navy;

    @include bp-desktop {
      top: 24px;
      right: 26px;
    }

    .icon {
      line-height: 32px;

      font-size: 24px;
      font-weight: 600;

      @include bp-desktop {
        font-size: 30px;
        font-weight: 600;
      }
    }
  }

  // Modal Header
  // -------------------------

  &__header {
    border-bottom: 1px solid $color-gray-20;

    padding: 20px 64px 0 $modal-margin;

    width: 100%;
    height: 64px;

    @include bp-desktop {
      padding: 24px 64px 0 $modal-margin-desktop;

      height: 80px;
    }

    @include headings {
      @include text-truncate;

      margin-bottom: 0;
    }

    h1 {
      @include font-semi-bold;
      @include typo-headline-5;

      @include bp-desktop {
        @include font-light;
        @include typo-headline-4;
      }
    }
  }

  // Modal Body
  // -------------------------

  &__body {
    @include typo-body-1;

    padding: 21px $modal-margin $modal-margin;

    @include bp-desktop {
      padding: 29px $modal-margin-desktop $modal-margin-desktop;
    }

    .button-group {
      margin-top: 21px;

      @include bp-desktop {
        margin-top: 34px;
      }
    }
  }

  // Grid Container in Modal
  // --------------------------------------------------

  > .container {
    @include bp-xs-max {
      width: calc(102% - (2 * #{$modal-margin}));
    }
  }
}

// Modal Backdrop
// --------------------------------------------------

.backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  transition: opacity 600ms, visibility 600ms;

  visibility: hidden;
  opacity: 0;

  z-index: $zindex-modal;

  background: rgba(0, 0, 0, .45);

  width: 100%;
  height: 100%;

  &--open {
    visibility: visible;
    opacity: 1;

    pointer-events: auto;
  }
}
