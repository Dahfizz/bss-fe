//
// Flexbox Grid
// --------------------------------------------------

// This Grid is based on the Bootstrap Grid.
// Credit: Bootstrap

// --------------------------------------------------

/*doc
---
title: Flexbox grid system
name: 01-foundation-05-flexgrid-01
category: Foundation - Grid
---

<style>
.exampleOutput .row {
  margin-bottom: 15px;
}

.exampleOutput .row-special {
  min-height: 80px;
  background-color: #ECF0F1;
}

.exampleOutput .row [class^=col-] {
  padding-top: 10px;
  padding-bottom: 10px;
  background-color: #ECF0F1;
  border: 1px solid #95A5A6;
}
</style>

<!-- -------------------------------------------------- -->

The flexbox grid system behaves similar to the classic grid system, but with a few notable differences:
<ul class="list">
  <li>Nesting, offsets, pushes, and pulls are all supported in the flexbox grid system.</li>
  <li>Flexbox grid columns without a set width will automatically layout with equal widths. For example, four columns will each automatically be 25% wide.</li>
  <li>Flexbox grid columns have significantly more alignment options available, including vertical alignment.</li>
  <li>Flexbox grid system requires a `.col-{breakpoint}` class for each tier.</li>
</ul>

<!-- -------------------------------------------------- -->

## Auto-layout columns
Utilize breakpoint-specific column classes for equal-width columns. Add any number of `.col-{breakpoint}`s for each breakpoint you need and every column will be the same width.

### Equal-width
For example, here are two grid layouts that apply to every device and viewport, from xs to ul.

```html_example
<div class="row">
  <div class="col-xs">
    1 of 2
  </div>
  <div class="col-xs">
    1 of 2
  </div>
</div>
<div class="row">
  <div class="col-xs">
    1 of 3
  </div>
  <div class="col-xs">
    1 of 3
  </div>
  <div class="col-xs">
    1 of 3
  </div>
</div>
```

### Setting one column width
Auto-layout for flexbox grid columns also means you can set the width of one column and the others will automatically resize around it. You may use predefined grid classes (as shown below), grid mixins, or inline widths. Note that the other columns will resize no matter the width of the center column.

```html_example
<div class="row">
  <div class="col-xs">
    1 of 3
  </div>
  <div class="col-xs-6">
    2 of 3 (wider)
  </div>
  <div class="col-xs">
    3 of 3
  </div>
</div>
<div class="row">
  <div class="col-xs">
    1 of 3
  </div>
  <div class="col-xs-5">
    2 of 3 (wider)
  </div>
  <div class="col-xs">
    3 of 3
  </div>
</div>
```

### Variable width content
Using the `col-{breakpoint}-auto` classes, columns can size itself based on the natural width of its content. This is super handy with single line content like inputs, numbers, etc. This, in conjunction with "horizontal alignment" classes, is very useful for centering layouts with uneven column sizes as viewport width changes.

```html_example
<div class="row flex-items-md-center">
  <div class="col-xs col-lg-2">
    1 of 3
  </div>
  <div class="col-xs-12 col-md-auto">
    Variable width content
  </div>
  <div class="col-xs col-lg-2">
    3 of 3
  </div>
</div>
<div class="row">
  <div class="col-xs">
    1 of 3
  </div>
  <div class="col-xs-12 col-md-auto">
    Variable width content
  </div>
  <div class="col-xs col-lg-2">
    3 of 3
  </div>
</div>
```

<!-- -------------------------------------------------- -->

## Responsive classes
The SDX grid includes six tiers of predefined classes for building complex responsive layouts. Customize the size of your columns on extra small (xs), small (sm), medium (md), large (lg), extra large (xl) or ultra large (ul) devices however you see fit.

### All breakpoints
For grids that are the same from the smallest of devices to the largest, use the `.col-xs` and `.col-xs-*` classes. Specify a numbered class when you need a particularly sized column; otherwise, feel free to stick to `.col-xs`.

The grid requires a class for full-width columns. If you have a `.col-sm-6` and don’t add `.col-xs-12`, your `xs` grid will not render correctly.

```html_example
<div class="row">
  <div class="col-xs">col-xs</div>
  <div class="col-xs">col-xs</div>
  <div class="col-xs">col-xs</div>
  <div class="col-xs">col-xs</div>
</div>
<div class="row">
  <div class="col-xs-8">col-xs-8</div>
  <div class="col-xs-4">col-xs-4</div>
</div>
```

```html_example
<div class="row">
  <div class="col-xs-12 col-md-6">
    1 of 2 (stacked on mobile)
  </div>
  <div class="col-xs-12 col-md-6">
    1 of 2 (stacked on mobile)
  </div>
</div>
```

<!-- -------------------------------------------------- -->

## Alignment
Use flexbox alignment utilities to vertically and horizontally align columns.

### Vertical alignment

```html_example
<div class="row row-special flex-items-xs-top">
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
</div>
<div class="row row-special flex-items-xs-middle">
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
</div>
<div class="row row-special flex-items-xs-bottom">
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
</div>
```

```html_example
<div class="row row-special">
  <div class="col-xs flex-xs-top">
    One of three columns
  </div>
  <div class="col-xs flex-xs-middle">
    One of three columns
  </div>
  <div class="col-xs flex-xs-bottom">
    One of three columns
  </div>
</div>
```

### Horizontal alignment

```html_example
<div class="row flex-items-xs-left">
  <div class="col-xs-4">
    One of two columns
  </div>
  <div class="col-xs-4">
    One of two columns
  </div>
</div>
<div class="row flex-items-xs-center">
  <div class="col-xs-4">
    One of two columns
  </div>
  <div class="col-xs-4">
    One of two columns
  </div>
</div>
<div class="row flex-items-xs-right">
  <div class="col-xs-4">
    One of two columns
  </div>
  <div class="col-xs-4">
    One of two columns
  </div>
</div>
<div class="row flex-items-xs-around">
  <div class="col-xs-4">
    One of two columns
  </div>
  <div class="col-xs-4">
    One of two columns
  </div>
</div>
<div class="row flex-items-xs-between">
  <div class="col-xs-4">
    One of two columns
  </div>
  <div class="col-xs-4">
    One of two columns
  </div>
</div>
```

<!-- -------------------------------------------------- -->

## No gutters
The gutters between columns in our predefined grid classes can be removed with `.no-gutters`. This removes the negative `margins` from `.row` and the horizontal `padding` from all immediate children columns.

```html_example
<div class="row no-gutters">
  <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>
```

<!-- -------------------------------------------------- -->

## Reordering

### Flex order
Use flexbox utilities for controlling the visual order of your content.

```html_example
<div class="row">
  <div class="col-xs flex-xs-unordered">
    First, but unordered
  </div>
  <div class="col-xs flex-xs-last">
    Second, but last
  </div>
  <div class="col-xs flex-xs-first">
    Third, but first
  </div>
</div>
```

### Offsetting columns
Move columns to the right using `.offset-md-*` classes. These classes increase the left margin of a column by `*` columns. For example, `.offset-md-4` moves `.col-md-4` over four columns.

```html_example
<div class="row">
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4 offset-md-4">.col-md-4 .offset-md-4</div>
</div>
<div class="row">
  <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
  <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
</div>
<div class="row">
  <div class="col-md-6 offset-md-3">.col-md-6 .offset-md-3</div>
</div>
```

### Push and pull
Easily change the order of our built-in grid columns with `.push-md-*` and `.pull-md-*` modifier classes.

```html_example
<div class="row">
  <div class="col-md-9 push-md-3">.col-md-9 .push-md-3</div>
  <div class="col-md-3 pull-md-9">.col-md-3 .pull-md-9</div>
</div>
```
*/


// --------------------------------------------------

// Custom styles for additional flex alignment options.

@each $breakpoint in map-keys($grid-breakpoints) {
  @include media-breakpoint-up($breakpoint) {
    // Flex column reordering
    .flex-#{$breakpoint}-first {
      order: -1;
    }
    .flex-#{$breakpoint}-last {
      order: 1;
    }
    .flex-#{$breakpoint}-unordered {
      order: 0;
    }

    // Alignment for every item
    .flex-items-#{$breakpoint}-top {
      align-items: flex-start;
    }
    .flex-items-#{$breakpoint}-middle {
      align-items: center;
    }
    .flex-items-#{$breakpoint}-bottom {
      align-items: flex-end;
    }

    // Alignment per item
    .flex-#{$breakpoint}-top {
      align-self: flex-start;
    }
    .flex-#{$breakpoint}-middle {
      align-self: center;
    }
    .flex-#{$breakpoint}-bottom {
      align-self: flex-end;
    }

    // Horizontal alignment of item
    .flex-items-#{$breakpoint}-left {
      justify-content: flex-start;
    }
    .flex-items-#{$breakpoint}-center {
      justify-content: center;
    }
    .flex-items-#{$breakpoint}-right {
      justify-content: flex-end;
    }
    .flex-items-#{$breakpoint}-around {
      justify-content: space-around;
    }
    .flex-items-#{$breakpoint}-between {
      justify-content: space-between;
    }
  }
}
