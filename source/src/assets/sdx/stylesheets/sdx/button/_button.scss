//
// Buttons
// --------------------------------------------------

/*doc
---
title: Button Types
name: 02-components-01-button-01-types
category: Components - Buttons
---
Use the button classes on an `<a>`, `<button>`, or `<input>` element.

```html_example
<div class="button-group">
  <a class="button button--primary" href="#" role="button">Link</a>
  <button class="button button--primary">Button</button>
  <input class="button button--primary" type="button" value="Input">
  <input class="button button--primary" type="submit" value="Submit">
</div>
```

If your button is actually a link to another page, please use the
`<a>` element, while if your button performs an action, such as submitting
a form or triggering some javascript event, then use a `<button>` element.


#### Dark background
Button types on dark background.

```html_example
<div class="bg bg--dark">
  <div class="button-group">
    <a class="button button--primary" href="#" role="button">Link</a>
    <button class="button button--primary">Button</button>
    <input class="button button--primary" type="button" value="Input">
    <input class="button button--primary" type="submit" value="Submit">
  </div>
</div>
```
*/

/*doc
---
title: Button Styles
name: 02-components-01-button-02-styles
category: Components - Buttons
---
There are three basic buttons styles: Primary, secondary and
confirm. Simply apply the style modifier class for the desired action.

```html_example
<div class="button-group">
  <!-- Primary button -->
  <button class="button button--primary">Primary</button>
  <button class="button button--primary disabled">Unselectable</button>
</div>
<div class="button-group">
  <!-- Secondary button -->
  <button class="button button--secondary">Secondary</button>
  <button class="button button--secondary disabled">Unselectable</button>
</div>
<div class="button-group">
  <!-- Confirm button -->
  <button class="button button--confirm">Confirm</button>
  <button class="button button--confirm disabled">Unselectable</button>
</div>
```

#### Dark background
Button styles on dark background.

```html_example
<div class="bg bg--dark">
  <div class="button-group">
    <!-- Primary button -->
    <button class="button button--primary">Primary</button>
    <button class="button button--primary disabled">Unselectable</button>
  </div>
  <div class="button-group">
    <!-- Secondary button -->
    <button class="button button--secondary">Secondary</button>
    <button class="button button--secondary disabled">Unselectable</button>
  </div>
  <div class="button-group">
    <!-- Confirm button -->
    <button class="button button--confirm">Confirm</button>
    <button class="button button--confirm disabled">Unselectable</button>
  </div>
</div>
```
*/

/*doc
---
title: Button Sizes
name: 02-components-01-button-03-sizes
category: Components - Buttons
---
There is one general button size. The minimal button size is 160x48 pixels.

With two an additional modifier that will make the button take the
full width of the container or act as a responsive button. It may be used with the any of the button
size and style modifiers.

```html_example
<div class="button-group">
  <button class="button button--primary">Button</button>
  <button class="button button--secondary">Button with a longer button description</button>
</div>
```

#### Full Width Button
To make a button use 100% of the container width add the `button--full` modifier.

```html_example
<div class="button-group">
  <button type="button" class="button button--primary button--full">Full width button</button>
  <button type="button" class="button button--secondary button--full">Full width button</button>
</div>
```

#### Responsive Button
To make a button responsive add the `button--responsive` modifier.

```html_example
<div class="button-group">
  <button type="button" class="button button--primary button--responsive">Responsive button</button>
  <button type="button" class="button button--secondary button--responsive">Responsive button</button>
</div>
```
*/

/*doc
---
title: Button with Icons
name: 02-components-01-button-04-icon
category: Components - Buttons
---

```html_example
<div class="button-group">
  <button class="button button__icon button--primary">
    <i class="icon icon-079-shopping-trolley" aria-hidden="true"></i>
    Order
  </button>
  <button class="button button__icon button--secondary">
    <i class="icon icon-079-shopping-trolley" aria-hidden="true"></i>
    Order Now
  </button>
  <button class="button button__icon button--confirm">
    <i class="icon icon-079-shopping-trolley" aria-hidden="true"></i>
    Order very much now
  </button>
</div>
```
*/


// --------------------------------------------------


$buttons: (
  primary: (
    button-color: $color-button-primary,
    button-color--active: $color-button-primary--active,
    color: $color-white,
    text-opacity: .6
  ),
  secondary: (
    button-color: $color-button-secondary,
    button-color--active: $color-button-secondary--active,
    color: $color-gray,
    text-opacity: .4
  ),
  confirm: (
    button-color: $color-button-confirm,
    button-color--active: $color-button-confirm--active,
    color: $color-white,
    text-opacity: .6
  )
);

// --------------------------------------------------


// Global Button styles
// --------------------------------------------------

button,
.button {
  cursor: pointer;
}


// Button Normal
// --------------------------------------------------

.button {
  @include typo-body-1;
  @include text-truncate;
  @include font-smoothing;

  display: inline-block;

  position: relative;

  transition: all 70ms $button-easing;

  margin: 0;

  outline: none;
  border: 0;
  border-bottom-style: none;
  border-radius: $button-border-radius;
  background: none;
  cursor: pointer;

  padding:
  $button-padding-vertical
  $button-padding-horizontal;

  min-width: 160px;

  vertical-align: middle;
  text-align: center;
  text-decoration: none;
  text-shadow: 0 0 0;

  transform-origin: 50% 50%;

  user-select: none;

  // Active styles
  // -------------------------

  &:active {
    transform: scale(.98);
  }

  // Disabled styles
  // -------------------------

  &.disabled,
  &:disabled {
    cursor: not-allowed;
    pointer-events: none;
  }

  @each $button, $settings in $buttons {
    @if $button == primary {
      &,
      &--#{$button} {
        @include button($settings);
      }
    } @else {
      &--#{$button} {
        @include button($settings);
      }
    }
  }

  // Icon styles
  // -------------------------

  &__icon {
    padding: 12px 24px 12px 16px;
  }

  .icon {
    margin-right: 7px;
    font-size: 18px;
  }
}


// Button Thin
// --------------------------------------------------

.button--thin,
%button--thin {
  $color: $color-white;

  // -------------------------

  padding:
  $button-padding-vertical - $button-border-width
  $button-padding-horizontal - $button-border-width;

  @each $button, $settings in $buttons {
    @if $button == primary {
      &,
      &.button--#{$button} {
        @include button-thin($settings, $color-white);
      }
    } @else {
      &.button--#{$button} {
        @include button-thin($settings, $color-white);
      }
    }
  }
}


// Dark Background
// --------------------------------------------------

.bg--dark {
  .button {
    @extend %button--thin;
  }
}


// Full button
// --------------------------------------------------

.button--full,
%button--full {
  display: block;
  width: 100%;

  &:active {
    transform: scale(.9925, .98);
  }
}


// Responsive button
// --------------------------------------------------

.button--responsive,
%button--responsive {
  width: 100%;

  @include bp-sm {
    width: auto;
  }
}


// Specific overrides
// --------------------------------------------------

input[type='submit'],
input[type='reset'],
input[type='button'] {
  &.button--full {
    @extend %button--full;
  }

  &.button--responsive {
    @extend %button--responsive;
  }
}
