export class MenuFlyoutListItem {
    constructor() {
        /**
         * If an item is not selectable, it is neither highlighted nor has it cursor: pointer.
         */
        this.selectable = true;
        /**
         * @private
         */
        this.direction = "bottom-right";
    }
    hostData() {
        return {
            class: {
                selectable: this.selectable,
                [this.direction]: true
            }
        };
    }
    render() {
        return (h("div", { class: "item" },
            h("div", { class: "arrow" }),
            h("div", { class: "body" },
                h("slot", null))));
    }
    static get is() { return "sdx-menu-flyout-list-item"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "direction": {
            "type": String,
            "attr": "direction"
        },
        "selectable": {
            "type": Boolean,
            "attr": "selectable"
        }
    }; }
    static get style() { return "/**style-placeholder:sdx-menu-flyout-list-item:**/"; }
}
