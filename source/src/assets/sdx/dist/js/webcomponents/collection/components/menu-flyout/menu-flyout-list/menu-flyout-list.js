export class MenuFlyoutList {
    render() {
        return (h("slot", null));
    }
    static get is() { return "sdx-menu-flyout-list"; }
    static get encapsulation() { return "shadow"; }
    static get style() { return "/**style-placeholder:sdx-menu-flyout-list:**/"; }
}
