export class Checkbox {
    constructor() {
        this.checked = false;
    }
    hostData() {
        return {
            class: {
                checked: this.checked
            }
        };
    }
    render() {
        return (h("div", { class: "wrapper" },
            h("div", { class: "check" },
                h("div", { class: "tick" },
                    h("i", { class: "icon icon-011-check-mark" }))),
            h("div", { class: "label" },
                h("slot", null))));
    }
    static get is() { return "sdx-checkbox"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "checked": {
            "type": Boolean,
            "attr": "checked"
        }
    }; }
    static get style() { return "/**style-placeholder:sdx-checkbox:**/"; }
}
