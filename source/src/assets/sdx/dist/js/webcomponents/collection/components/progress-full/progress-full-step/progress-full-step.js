export class ProgressFullStep {
    constructor() {
        /**
         * @Private
         */
        this.value = 0;
        /**
         * @Private
         */
        this.status = "none";
        /**
         * @Private
         */
        this.position = "none";
        /**
         * Triggered when a user clicks on the button or description of a completed step.
         */
        this.onStepClick = () => undefined;
    }
    /**
     * Trigger click event when completed state.
     */
    clicked() {
        if (this.status === "completed") {
            this.onStepClick();
        }
    }
    render() {
        return (h("div", { class: "step-container" },
            h("div", { class: "progress-line-right" }),
            h("div", { class: "progress-line-left" }),
            h("div", { class: "button-container" },
                h("button", { onClick: () => this.clicked() }, this.value)),
            h("br", { class: "br-hide" }),
            h("div", { onClick: () => this.clicked(), class: "progress-content" },
                h("slot", null))));
    }
    static get is() { return "sdx-progress-full-step"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "el": {
            "elementRef": true
        },
        "onStepClick": {
            "type": "Any",
            "attr": "on-step-click"
        },
        "position": {
            "type": String,
            "attr": "position"
        },
        "status": {
            "type": String,
            "attr": "status"
        },
        "value": {
            "type": Number,
            "attr": "value"
        }
    }; }
    static get style() { return "/**style-placeholder:sdx-progress-full-step:**/"; }
}
