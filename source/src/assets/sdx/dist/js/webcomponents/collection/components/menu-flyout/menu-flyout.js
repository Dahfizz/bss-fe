import { TweenLite, Power1, Power4 } from "gsap";
export class MenuFlyout {
    constructor() {
        this.animationDuration = 0.3;
        this.arrowOffset = 31; // sum of offset and 45deg rotation of arrow
        this.animationOffset = 20; // arrow height plus some spacing
        this.isOpen = false;
        this.listPosition = [0, 0];
        this.isClicking = false;
        /**
         * In which direction the flyout opens.
         */
        this.direction = "bottom-right";
        /**
         * Close if the user clicks on the flyout.
         */
        this.closeOnClick = false;
    }
    directionChanged() {
        if (this.isOpen) { // If flyout is open, re-open it with new direction
            this.close().then(() => {
                this.assignPropsToChildren();
                this.open();
            });
        }
        else {
            this.assignPropsToChildren();
        }
    }
    componentDidLoad() {
        // Setup
        this.setChildReferences();
        this.assignPropsToChildren();
        // Listen to children changes
        const observer = new MutationObserver(() => this.onChildrenChange());
        observer.observe(this.el, { childList: true, characterData: true, subtree: true });
        // Close by default
        this.close();
    }
    onClick() {
        // Let the component know that a click event is happening (for later handling of global "window:click")
        this.isClicking = true;
    }
    onWindowClick() {
        // Close the flyout if the user clicks anywhere on the screen
        // except on the flyout itself (if not overriden by this.closeOnClick)
        if (!this.isClicking || (this.isOpen && this.closeOnClick)) {
            this.close();
        }
        this.isClicking = false;
    }
    /**
     * Fired by the MutationObserver whenever children change.
     */
    onChildrenChange() {
        this.setChildReferences();
        this.assignPropsToChildren();
    }
    /**
     * Toggles the flyout.
     */
    toggle() {
        if (this.isOpen) {
            return this.close();
        }
        return this.open();
    }
    /**
     * Opens the flyout.
     */
    open() {
        return new Promise((resolve) => {
            if (!this.listEl) {
                return;
            }
            TweenLite.killTweensOf(this.listEl);
            // Set display to block so browsers can measure its accurate position
            TweenLite.set(this.listEl, {
                display: "block"
            });
            const animationOffset = this.direction === "top-left" || this.direction === "top-right" ? -this.animationOffset : this.animationOffset;
            this.listPosition = this.getListPosition(this.direction);
            TweenLite.set(this.listEl, {
                transform: this.positionToTranslate3d(this.listPosition)
            });
            TweenLite.to(this.listEl, this.animationDuration, {
                transform: this.positionToTranslate3d(this.listPosition, [0, animationOffset]),
                opacity: 1,
                ease: [
                    Power1.easeIn, Power4.easeOut
                ],
                onComplete: () => {
                    this.el.setAttribute("aria-expanded", "true");
                    this.isOpen = true;
                    resolve();
                }
            });
        });
    }
    /**
     * Closes the flyout.
     */
    close() {
        return new Promise((resolve) => {
            if (!this.listEl) {
                return;
            }
            TweenLite.killTweensOf(this.listEl);
            if (!this.isOpen) { // close() is called while flyout wasn't open
                TweenLite.set(this.listEl, {
                    display: "none",
                    opacity: 0
                });
                resolve();
            }
            else { // flyout is open
                TweenLite.to(this.listEl, this.animationDuration, {
                    transform: this.positionToTranslate3d(this.listPosition),
                    opacity: 0,
                    ease: [
                        Power1.easeIn, Power4.easeOut
                    ],
                    onComplete: () => {
                        TweenLite.set(this.listEl, {
                            display: "none"
                        });
                        this.el.setAttribute("aria-expanded", "false");
                        this.isOpen = false;
                        resolve();
                    }
                });
            }
        });
    }
    /**
     * Traverse through child components, keep references and pass props to them.
     */
    setChildReferences() {
        this.toggleEl = this.el.querySelector("sdx-menu-flyout-toggle");
        this.listEl = this.el.querySelector("sdx-menu-flyout-list");
        this.listItemEls = this.el.querySelectorAll("sdx-menu-flyout-list-item");
        if (!(this.toggleEl && this.toggleEl.children[0] && this.listEl)) {
            return;
        }
        this.toggleElChild = this.toggleEl.children[0];
    }
    /**
     * Return the position where the flyout will appear.
     * @param direction Desired direction.
     */
    getListPosition(direction) {
        if (!(this.listEl && this.toggleElChild)) {
            return [0, 0];
        }
        const listElRect = this.listEl.getBoundingClientRect();
        const toggleElChildRect = this.toggleElChild.getBoundingClientRect();
        const top = -listElRect.height;
        const bottom = toggleElChildRect.height;
        const right = (toggleElChildRect.width / 2) - this.arrowOffset;
        const left = -(listElRect.width - toggleElChildRect.width) - right;
        switch (direction) {
            case "bottom-right":
                return [right, bottom];
            case "bottom-left":
                return [left, bottom];
            case "top-right":
                return [right, top];
            case "top-left":
                return [left, top];
            default:
                return [0, 0];
        }
    }
    /**
     * Generates a string "translated3d(...)" that can be passed to the transition style property.
     * @param position Position to be transformed into a string.
     * @param offset Will be added to the position.
     */
    positionToTranslate3d(position, offset = [0, 0]) {
        return `translate3d(${position[0] + offset[0]}px, ${position[1] + offset[1]}px, 0)`;
    }
    /**
     * Inform the children about the their props.
     */
    assignPropsToChildren() {
        if (!(this.toggleEl)) {
            return;
        }
        // Assign toggle callback
        this.toggleEl.toggle = this.toggle.bind(this);
        // Assign direction
        for (let i = 0; i < this.listItemEls.length; ++i) {
            const itemEl = this.listItemEls[i];
            itemEl.setAttribute("direction", this.direction);
        }
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "sdx-menu-flyout"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "close": {
            "method": true
        },
        "closeOnClick": {
            "type": Boolean,
            "attr": "close-on-click"
        },
        "direction": {
            "type": String,
            "attr": "direction",
            "watchCallbacks": ["directionChanged"]
        },
        "el": {
            "elementRef": true
        },
        "open": {
            "method": true
        },
        "toggle": {
            "method": true
        }
    }; }
    static get listeners() { return [{
            "name": "click",
            "method": "onClick"
        }, {
            "name": "touchend",
            "method": "onClick",
            "passive": true
        }, {
            "name": "window:click",
            "method": "onWindowClick"
        }, {
            "name": "window:touchend",
            "method": "onWindowClick",
            "passive": true
        }]; }
    static get style() { return "/**style-placeholder:sdx-menu-flyout:**/"; }
}
