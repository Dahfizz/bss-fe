export class Item {
    constructor() {
        /**
         * @private
         */
        this.open = false;
    }
    activeItemChanged() {
        this.decideCollapseHeaderDisplay();
        this.decideCollapseBodyDisplay();
    }
    componentWillLoad() {
        this.setChildReferences();
        this.decideCollapseHeaderDisplay();
    }
    componentDidLoad() {
        this.decideCollapseBodyDisplay();
    }
    /**
     * Assign element references to used properties.
     */
    setChildReferences() {
        this.itemHeaderEl = this.el.querySelector("sdx-accordion-item-header");
        this.itemBodyEl = this.el.querySelector("sdx-accordion-item-body");
    }
    /**
     * Decides based on open property the display of header and its and behaviour.
     */
    decideCollapseHeaderDisplay() {
        if (this.itemHeaderEl) {
            this.itemHeaderEl.setAttribute("expand", this.open ? "true" : "false");
        }
    }
    /**
     * Decides based on open property the display of body.
     */
    decideCollapseBodyDisplay() {
        if (this.itemBodyEl) {
            this.itemBodyEl.toggle(this.open);
        }
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "sdx-accordion-item"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "el": {
            "elementRef": true
        },
        "open": {
            "type": Boolean,
            "attr": "open",
            "watchCallbacks": ["activeItemChanged"]
        }
    }; }
    static get style() { return "/**style-placeholder:sdx-accordion-item:**/"; }
}
