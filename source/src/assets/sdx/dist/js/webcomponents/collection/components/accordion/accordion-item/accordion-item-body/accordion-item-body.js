import { TweenLite, Power1, Power4 } from "gsap";
const CLASS_OPEN = "open";
const ANIMATION_OPEN = 0.3;
const ANIMATION_DELAY_OPEN = 0.2;
const ANIMATION_VISIBLE_OPEN = 0.5;
const ANIMATION_VISIBLE_CLOSE = 0.1;
export class Body {
    constructor() {
        this.initialLoad = true;
        /**
         * @private
         */
        this.arrowPosition = "none";
    }
    /**
     * Toggles body directly when initial load or with an animation.
     * @param isOpen Open state of the accordion item.
     */
    toggle(isOpen) {
        if (this.initialLoad) {
            this.initiateOpenState(isOpen);
        }
        else if (isOpen) {
            this.openCollapseSection();
        }
        else {
            this.closeCollapseSection();
        }
    }
    /**
     * Sets class to handle immediately the open/close state.
     * @param newState Open State of the accordion item.
     */
    initiateOpenState(newState) {
        if (newState) {
            this.el.classList.add(CLASS_OPEN);
        }
        this.initialLoad = false;
    }
    /**
     * Opens section with an animation.
     */
    openCollapseSection() {
        TweenLite.killTweensOf(this.el);
        TweenLite.set(this.el, {
            display: "block"
        });
        TweenLite.to(this.el, ANIMATION_OPEN, {
            className: `+=${CLASS_OPEN}`,
            ease: [
                Power1.easeIn, Power4.easeOut
            ],
            onComplete: () => {
                this.el.setAttribute("aria-expanded", "true");
            }
        });
        TweenLite.to(this.el, ANIMATION_VISIBLE_OPEN, {
            autoAlpha: 1,
            delay: ANIMATION_DELAY_OPEN
        });
    }
    /**
     * Closes section with an animation.
     */
    closeCollapseSection() {
        TweenLite.killTweensOf(this.el);
        TweenLite.to(this.el, ANIMATION_VISIBLE_CLOSE, {
            autoAlpha: 0
        });
        TweenLite.to(this.el, ANIMATION_OPEN, {
            className: `-=${CLASS_OPEN}`,
            ease: [
                Power1.easeIn, Power4.easeOut
            ],
            onComplete: () => {
                TweenLite.set(this.el, {
                    clearProps: "display, visibility, opacity"
                });
                this.el.setAttribute("aria-expanded", "false");
            }
        });
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "sdx-accordion-item-body"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "arrowPosition": {
            "type": String,
            "attr": "arrow-position"
        },
        "el": {
            "elementRef": true
        },
        "toggle": {
            "method": true
        }
    }; }
    static get style() { return "/**style-placeholder:sdx-accordion-item-body:**/"; }
}
