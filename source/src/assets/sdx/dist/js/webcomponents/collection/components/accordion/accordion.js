const OPENED_ELEMENT = "open";
export class Accordion {
    constructor() {
        this.openedItems = [];
        /**
         * Position of the arrow in the header.
         */
        this.arrowPosition = "none";
        /**
         * Allow to keep multiple accordion items opened.
         */
        this.keepOpen = false;
    }
    arrowPropertyChanged() {
        this.initiateComponent();
    }
    componentWillLoad() {
        this.initiateComponent();
    }
    componentDidLoad() {
        // Listen to children changes
        const observer = new MutationObserver(() => this.onChildrenChange());
        observer.observe(this.el, { childList: true });
    }
    /**
     * Fired by the MutationObserver whenever children change.
     */
    onChildrenChange() {
        this.initiateComponent();
    }
    /**
     * Closes the accordion item.
     * @param index Index of the accordion item.
     */
    close(index) {
        let itemEl = this.accordionItemEls[index];
        if (!this.keepOpen) {
            this.closeNotIgnoredItems(index);
        }
        const headerEl = itemEl.querySelector("sdx-accordion-item-header");
        if (headerEl) {
            itemEl.setAttribute(OPENED_ELEMENT, "false");
            this.trackOpenItems(index, "false");
        }
    }
    /**
     * Closes all accordion items.
     */
    closeAll() {
        this.openedItems = [];
        for (let i = 0; i < this.accordionItemEls.length; i++) {
            let itemEl = this.accordionItemEls[i];
            const headerEl = itemEl.querySelector("sdx-accordion-item-header");
            if (headerEl) {
                itemEl.setAttribute(OPENED_ELEMENT, "false");
                this.trackOpenItems(i, "false");
            }
        }
    }
    /**
     * Toggle display of the accordion item.
     * @param index Index of the accordion item.
     */
    toggle(index) {
        let itemEl = this.accordionItemEls[index];
        if (!this.keepOpen) {
            this.closeNotIgnoredItems(index);
        }
        const headerEl = itemEl.querySelector("sdx-accordion-item-header");
        if (headerEl) {
            const itemFound = itemEl.getAttribute(OPENED_ELEMENT) || "false";
            const isOpen = itemFound === "false" ? "true" : "false";
            itemEl.setAttribute(OPENED_ELEMENT, isOpen);
            this.trackOpenItems(index, isOpen);
        }
    }
    /**
     * Opens the accordion item.
     * @param index Index of the accordion item.
     */
    open(index) {
        let itemEl = this.accordionItemEls[index];
        if (!this.keepOpen) {
            this.closeNotIgnoredItems(index);
        }
        const headerEl = itemEl.querySelector("sdx-accordion-item-header");
        if (headerEl) {
            itemEl.setAttribute(OPENED_ELEMENT, "true");
            this.trackOpenItems(index, "true");
        }
    }
    /**
     * Opens all accordion items.
     */
    openAll() {
        if (this.keepOpen || this.accordionItemEls.length === 1) {
            this.openedItems = [];
            for (let i = 0; i < this.accordionItemEls.length; i++) {
                let itemEl = this.accordionItemEls[i];
                const headerEl = itemEl.querySelector("sdx-accordion-item-header");
                if (headerEl) {
                    itemEl.setAttribute(OPENED_ELEMENT, "true");
                    this.trackOpenItems(i, "true");
                }
            }
        }
    }
    initiateComponent() {
        this.setChildReferences();
        this.initiateAccordionItems();
    }
    /**
     * Sets child reference and add to every header a toggle function.
     */
    setChildReferences() {
        this.accordionItemEls = this.el.querySelectorAll("sdx-accordion-item");
    }
    /**
     * Modify items with initial settings.
     */
    initiateAccordionItems() {
        this.openedItems = [];
        for (let i = 0; i < this.accordionItemEls.length; ++i) {
            const itemEl = this.accordionItemEls[i];
            const headerEl = itemEl.querySelector("sdx-accordion-item-header");
            if (headerEl) {
                let isOpen = itemEl.getAttribute(OPENED_ELEMENT) || "false";
                itemEl.setAttribute(OPENED_ELEMENT, isOpen);
                itemEl.setAttribute("arrow-position", this.arrowPosition);
                headerEl.setAttribute("arrow-position", this.arrowPosition);
                const bodyEl = itemEl.querySelector("sdx-accordion-item-body");
                if (bodyEl) {
                    bodyEl.setAttribute("arrow-position", this.arrowPosition);
                }
                this.trackOpenItems(i, isOpen);
                headerEl.toggle = this.toggle.bind(this, i);
            }
        }
    }
    /**
     * Closes all items when keepOpen is false, to ensure only 1 accordion item is opened max.
     * @param ignoreIndex Index for which the closing of item will be ignored.
     */
    closeNotIgnoredItems(ignoreIndex) {
        for (let i = 0; i < this.openedItems.length; i++) {
            if (this.openedItems[i] !== ignoreIndex) {
                const itemEl = this.accordionItemEls[this.openedItems[i]];
                itemEl.setAttribute(OPENED_ELEMENT, "false");
            }
        }
        this.openedItems = [];
    }
    /**
     * Track which item is opened in case keepOpen is set to true.
     * @param index Index of the opened item.
     * @param isOpen Open state of the item.
     */
    trackOpenItems(index, isOpen) {
        if (!this.keepOpen && isOpen === "true") {
            this.openedItems.push(index);
        }
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "sdx-accordion"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "arrowPosition": {
            "type": String,
            "attr": "arrow-position",
            "watchCallbacks": ["arrowPropertyChanged"]
        },
        "close": {
            "method": true
        },
        "closeAll": {
            "method": true
        },
        "el": {
            "elementRef": true
        },
        "keepOpen": {
            "type": Boolean,
            "attr": "keep-open",
            "watchCallbacks": ["arrowPropertyChanged"]
        },
        "open": {
            "method": true
        },
        "openAll": {
            "method": true
        },
        "toggle": {
            "method": true
        }
    }; }
    static get style() { return "/**style-placeholder:sdx-accordion:**/"; }
}
