import { Power1, TimelineLite, TweenLite, TweenMax } from "gsap";
const OPEN_CLASSNAME = "open";
const CLOSE_CLASSNAME = "hide-content";
const ARROW_HIDDEN_CLASSNAME = "arrow--hidden";
const HIDE_ARROWS_CLASSNAME = "hide-arrows";
const STEP_CHANGE_ANIMATION = 0.4;
export class ProgressFull {
    constructor() {
        this.lastSentActiveStep = -1;
        this.initIndex = 1; // arrays are counted from 0, steps from 1
        this.stepsCount = 0;
        this.allowedVisibleSteps = 1;
        this.minVisible = 0;
        this.maxVisible = 0;
        this.minPossibleBars = 3;
        /**
         * Current active step of the progress bar.
         */
        this.activeStep = -1;
        /**
         * Current active step of the progress bar.
         */
        this.previousActiveStep = undefined;
        /**
         * Current active step of the progress bar.
         */
        this.step = 1;
        /**
         * Label used next to total amount of steps when not all steps are being displayed.
         */
        this.stepsLabel = "";
        /**
         * @private
         * Animate transitions between visible stops
         */
        this.animated = true;
    }
    stepChanged() {
        this.setActiveStep(this.step, this.animated);
    }
    componentWillLoad() {
        if (this.activeStep < 0) {
            this.activeStep = this.activeStep && this.activeStep !== -1 ? this.activeStep : this.step;
        }
    }
    componentDidLoad() {
        this.setChildReferences();
        this.setEventsOnSteps();
        this.setActiveStep(this.activeStep, this.animated);
        // Listen to children changes
        const observer = new MutationObserver(() => this.onChildrenChange());
        observer.observe(this.el, { childList: true, subtree: true });
    }
    /**
     * Listen to window resize, so steps can be redrawn based on the width.
     */
    resizePerformed() {
        clearTimeout(this.resizeTimer);
        this.resizeTimer = setTimeout(() => {
            this.setActiveStep(this.activeStep, false);
        }, 10);
    }
    /**
     * Move to next step if its available.
     */
    nextStep() {
        if (this.stepEls) {
            if (this.activeStep < this.stepsCount) {
                this.setActiveStep(++this.activeStep, this.animated);
            }
        }
    }
    /**
     * Move to previous step if its available.
     */
    previousStep() {
        if (this.stepEls) {
            if (this.activeStep > this.indexUpdate(0)) {
                this.setActiveStep(--this.activeStep, this.animated);
            }
        }
    }
    /**
     * Get the current active step
     */
    getActiveStep() {
        return this.activeStep;
    }
    /**
     * Set a step as active based on an index.
     * @param index Index of the new active step.
     * @param animation Allow animations when moving between steps.
     */
    setActiveStep(index, animation) {
        if (!this.stepEls) {
            return;
        }
        if (isNaN(index) || index < 1) {
            this.activeStep = this.initIndex;
        }
        else if (index > this.stepsCount) {
            this.activeStep = this.stepsCount + this.initIndex - 1;
        }
        else {
            this.activeStep = index;
        }
        this.calculateVisibleSteps();
        this.updateStepElements(animation);
        this.setPreviousStep(this.activeStep);
    }
    /**
     * Scroll the visible steps one step to the left.
     * This does not change the activeStep.
     */
    scrollLeft() {
        if (!this.stepEls || !this.shouldDisplayLeftArrow()) {
            return;
        }
        this.shiftVisibleStepLeft();
        this.updateStepElements(this.animated);
    }
    /**
     * Scroll the visible steps one step to the right.
     * This does not change the activeStep.
     */
    scrollRight() {
        if (!this.stepEls || !this.shouldDisplayRightArrow()) {
            return;
        }
        this.shiftVisibleStepRight();
        this.updateStepElements(this.animated);
    }
    /**
     * Traverse through child components, keep references and pass props to them.
     */
    setChildReferences() {
        this.stepEls = this.el.querySelectorAll("sdx-progress-full-step");
        if (this.stepEls) {
            this.stepsCount = this.stepEls.length;
        }
        if (!this.el.shadowRoot) {
            return;
        }
        const leftArrowEls = this.el.shadowRoot.querySelectorAll(".left-arrow");
        if (leftArrowEls && leftArrowEls.length > 0) {
            this.leftArrowEl = leftArrowEls[0];
        }
        const rightArrowEls = this.el.shadowRoot.querySelectorAll(".right-arrow");
        if (rightArrowEls && rightArrowEls.length > 0) {
            this.rightArrowEl = rightArrowEls[0];
        }
    }
    /**
     * Set on step element the functionality to change step. For example when user clicks a completed button
     */
    setEventsOnSteps() {
        for (let i = 0; i < this.stepsCount; i++) {
            this.stepEls[i].onStepClick = this.setActiveStep.bind(this, this.indexUpdate(i), this.animated);
        }
    }
    /**
     * Calculates steps, that should be displayed to the user based on the width of the parent element.
     */
    calculateVisibleSteps() {
        this.allowedVisibleSteps = Math.floor(this.el.offsetWidth / 100);
        // Calculate how many steps should be displayed
        if (this.stepsCount <= this.minPossibleBars) {
            this.allowedVisibleSteps = this.stepsCount;
        }
        else if (this.allowedVisibleSteps < this.minPossibleBars) {
            this.allowedVisibleSteps = this.minPossibleBars;
        }
        else if (this.stepsCount < this.allowedVisibleSteps) {
            this.allowedVisibleSteps = this.stepsCount;
        }
        // Set min and max step to be visible
        if (this.activeStep < this.allowedVisibleSteps) {
            this.minVisible = 1;
            this.maxVisible = this.allowedVisibleSteps;
        }
        else if (this.activeStep < this.stepsCount - 1) {
            this.minVisible = this.activeStep - this.allowedVisibleSteps + 2;
            this.maxVisible = this.activeStep + 1;
        }
        else {
            this.minVisible = this.stepsCount - this.allowedVisibleSteps + 1;
            this.maxVisible = this.stepsCount;
        }
    }
    shiftVisibleStepLeft() {
        if (this.minVisible > 1) {
            this.minVisible--;
            this.maxVisible--;
        }
    }
    shiftVisibleStepRight() {
        if (this.maxVisible < this.stepsCount) {
            this.minVisible++;
            this.maxVisible++;
        }
    }
    /**
     * Updates attributes and classes of the step element, which controls what step will be displayed / hidden.
     * @param animation Animate the state change transition.
     */
    updateStepElements(animation) {
        if (!this.stepEls) {
            return;
        }
        this.updateInfoElement();
        this.updateArrows();
        for (let i = 0; i < this.stepsCount; i++) {
            this.updateStepElement(i, animation);
        }
        this.informActiveStepChanged();
    }
    updateArrows() {
        if (!this.leftArrowEl || !this.rightArrowEl) {
            return;
        }
        const arrowWidth = this.getArrowWidth();
        TweenMax.set(this.leftArrowEl, { width: arrowWidth });
        TweenMax.set(this.rightArrowEl, { width: arrowWidth });
        if (this.shouldDisplayLeftArrow()) {
            this.leftArrowEl.classList.remove(ARROW_HIDDEN_CLASSNAME);
        }
        else {
            this.leftArrowEl.classList.add(ARROW_HIDDEN_CLASSNAME);
        }
        if (this.shouldDisplayRightArrow()) {
            this.rightArrowEl.classList.remove(ARROW_HIDDEN_CLASSNAME);
        }
        else {
            this.rightArrowEl.classList.add(ARROW_HIDDEN_CLASSNAME);
        }
        if (this.shouldDisplayArrows()) {
            this.el.classList.remove(HIDE_ARROWS_CLASSNAME);
        }
        else {
            this.el.classList.add(HIDE_ARROWS_CLASSNAME);
        }
    }
    shouldDisplayArrows() {
        return this.allowedVisibleSteps !== this.stepsCount;
    }
    shouldDisplayRightArrow() {
        return this.maxVisible < this.stepsCount && this.activeStep >= this.maxVisible;
    }
    shouldDisplayLeftArrow() {
        return this.minVisible > 1;
    }
    updateStepElement(elIndex, animation) {
        const stepIndex = this.indexUpdate(elIndex);
        this.setStepElementAttributes(elIndex, stepIndex);
        if (this.isLeftOutOfSight(stepIndex) || this.isRightOutOfSight(stepIndex)) {
            this.handleOutofSightElement(animation, elIndex, stepIndex);
        }
        else {
            this.handleInSightElement(animation, elIndex);
        }
    }
    handleInSightElement(animation, elIndex) {
        const stepElement = this.stepEls[elIndex];
        TweenLite.killTweensOf(stepElement);
        TweenMax.set(stepElement, { display: "inline-block" });
        TweenMax.set(stepElement, { width: this.getCorrectWidth() });
        if (this.shouldAnimateElementFadeIn(animation, elIndex)) {
            this.fadeInElement(elIndex);
        }
        else {
            this.showElement(elIndex);
        }
    }
    handleOutofSightElement(animation, elIndex, stepIndex) {
        const marginOutOfSight = "-" + this.getCorrectWidth();
        if (this.shouldAnimateElementFadeOut(animation, elIndex)) {
            this.fadeOutElement(elIndex, stepIndex, marginOutOfSight);
        }
        else {
            this.hideElement(elIndex, stepIndex, marginOutOfSight);
        }
    }
    showElement(elIndex) {
        const stepElement = this.stepEls[elIndex];
        TweenMax.set(stepElement, {
            "margin-left": "0%",
            "margin-right": "0%"
        });
        TweenLite.set(stepElement, { clearProps: "opacity" });
        stepElement.classList.add(OPEN_CLASSNAME);
        stepElement.classList.remove(CLOSE_CLASSNAME);
    }
    fadeInElement(elIndex) {
        const stepElement = this.stepEls[elIndex];
        new TimelineLite().to(stepElement, STEP_CHANGE_ANIMATION, {
            "margin-left": "0%",
            "margin-right": "0%",
            opacity: 1,
            ease: Power1.easeOut,
            onComplete: () => {
                TweenLite.set(stepElement, {
                    clearProps: "opacity"
                });
                stepElement.classList.add(OPEN_CLASSNAME);
                stepElement.classList.remove(CLOSE_CLASSNAME);
            }
        });
    }
    hideElement(elIndex, stepIndex, marginOutOfSight) {
        const stepElement = this.stepEls[elIndex];
        TweenMax.set(stepElement, { display: "none" });
        TweenMax.set(stepElement, {
            "margin-left": this.isLeftOutOfSight(stepIndex) ? marginOutOfSight : "0%",
            "margin-right": this.isRightOutOfSight(stepIndex) ? marginOutOfSight : "0%"
        });
        stepElement.classList.add(CLOSE_CLASSNAME);
        stepElement.classList.remove(OPEN_CLASSNAME);
    }
    fadeOutElement(elIndex, stepIndex, marginOutOfSight) {
        const stepElement = this.stepEls[elIndex];
        new TimelineLite().to(stepElement, STEP_CHANGE_ANIMATION, {
            "margin-left": this.isLeftOutOfSight(stepIndex) ? marginOutOfSight : "0%",
            "margin-right": this.isRightOutOfSight(stepIndex) ? marginOutOfSight : "0%",
            opacity: 0,
            ease: Power1.easeInOut,
            onComplete: () => {
                TweenLite.set(stepElement, {
                    clearProps: "opacity"
                });
                stepElement.classList.add(CLOSE_CLASSNAME);
                stepElement.classList.remove(OPEN_CLASSNAME);
                TweenMax.set(stepElement, { display: "none" });
            }
        });
    }
    shouldAnimateElementFadeIn(animation, elIndex) {
        return animation && this.stepEls[elIndex].classList.contains(CLOSE_CLASSNAME);
    }
    shouldAnimateElementFadeOut(animation, elIndex) {
        return animation && this.stepEls[elIndex].classList.contains(OPEN_CLASSNAME);
    }
    setStepElementAttributes(elIndex, stepIndex) {
        const stepElement = this.stepEls[elIndex];
        stepElement.setAttribute("status", this.getStepStatus(stepIndex));
        stepElement.setAttribute("value", stepIndex.toString());
        stepElement.setAttribute("total", (this.allowedVisibleSteps).toString());
        stepElement.setAttribute("position", this.recalculateStepPosition(stepIndex));
    }
    getStepStatus(index) {
        if (index > this.activeStep) {
            return "none";
        }
        else if (index === this.activeStep) {
            return "active";
        }
        return "completed";
    }
    isRightOutOfSight(index) {
        return index > this.maxVisible;
    }
    isLeftOutOfSight(index) {
        return index < this.minVisible;
    }
    /**
     * Updates steps label to be visible.
     */
    updateInfoElement() {
        if (this.allowedVisibleSteps !== this.stepsCount && this.stepsLabel) {
            this.el.classList.remove("hide-steps-label");
        }
        else {
            this.el.classList.add("hide-steps-label");
        }
    }
    /**
     * Based on the position and ammount of visible steps a css class name is recalculuated for the step.
     * @param index Position of the step.
     */
    recalculateStepPosition(index) {
        if (index === 1) {
            return "first";
        }
        else if (index === this.stepsCount) {
            return "last";
        }
        else if (index === this.minVisible) {
            return "middle-left";
        }
        else if (index === this.maxVisible) {
            return "middle-right";
        }
        else if (index > 1 && index < this.stepsCount) {
            return "middle";
        }
        return "none";
    }
    /**
     * Adjusts the index when we convert from element index to step index
     * @param index Index, that will be updated to reflect true position.
     */
    indexUpdate(index) {
        return index + this.initIndex;
    }
    /**
     * Fired by the MutationObserver whenever children change.
     */
    onChildrenChange() {
        this.setChildReferences();
        this.setEventsOnSteps();
    }
    /**
     * Retrieves width of every step based on the parent element width.
     */
    getCorrectWidth() {
        return (Math.floor(100 / this.allowedVisibleSteps)).toString() + "%";
    }
    /**
     * Calculates the width of the arrow (click-able area of the arrow)
     * The width covers the line for the gradient effect.
     */
    getArrowWidth() {
        const width = Math.floor(100 / this.allowedVisibleSteps) + 100 % this.allowedVisibleSteps - 1;
        return `calc((${width}% - 24px) / 2)`;
    }
    /**
     * Calls a function, that is attached to the onSelectChange function when active step has changed.
     */
    informActiveStepChanged() {
        if (this.lastSentActiveStep !== this.activeStep) {
            this.lastSentActiveStep = this.activeStep;
            let onStepChange = () => null;
            if (typeof this.onStepChange === "string") {
                onStepChange = new Function(this.onStepChange);
            }
            else if (typeof this.onStepChange === "function") {
                onStepChange = this.onStepChange;
            }
            onStepChange.call(window, this.activeStep, this.previousActiveStep);
        }
    }
    setPreviousStep(previousStep) {
        this.previousActiveStep = previousStep;
    }
    render() {
        return [
            h("div", { class: "info-content" },
                this.stepsCount,
                " ",
                this.stepsLabel),
            h("slot", null),
            h("div", { class: "left-arrow", onClick: () => this.scrollLeft() },
                h("div", { class: "arrow" })),
            h("div", { class: "right-arrow", onClick: () => this.scrollRight() },
                h("div", { class: "arrow" }))
        ];
    }
    static get is() { return "sdx-progress-full"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "activeStep": {
            "state": true
        },
        "animated": {
            "type": Boolean,
            "attr": "animated"
        },
        "el": {
            "elementRef": true
        },
        "getActiveStep": {
            "method": true
        },
        "nextStep": {
            "method": true
        },
        "onStepChange": {
            "type": String,
            "attr": "on-step-change"
        },
        "previousActiveStep": {
            "state": true
        },
        "previousStep": {
            "method": true
        },
        "setActiveStep": {
            "method": true
        },
        "step": {
            "type": Number,
            "attr": "step",
            "watchCallbacks": ["stepChanged"]
        },
        "stepsLabel": {
            "type": String,
            "attr": "steps-label",
            "watchCallbacks": ["stepChanged"]
        }
    }; }
    static get listeners() { return [{
            "name": "window:resize",
            "method": "resizePerformed",
            "passive": true
        }]; }
    static get style() { return "/**style-placeholder:sdx-progress-full:**/"; }
}
