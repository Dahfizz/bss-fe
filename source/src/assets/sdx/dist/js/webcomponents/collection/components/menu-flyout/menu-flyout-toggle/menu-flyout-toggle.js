export class MenuFlyoutToggle {
    constructor() {
        /**
         * Callback fired on toggle element click (will be set internally).
         */
        this.toggle = () => undefined;
    }
    onClick() {
        this.toggle();
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "sdx-menu-flyout-toggle"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "toggle": {
            "type": "Any",
            "attr": "toggle"
        }
    }; }
    static get listeners() { return [{
            "name": "click",
            "method": "onClick"
        }]; }
    static get style() { return "/**style-placeholder:sdx-menu-flyout-toggle:**/"; }
}
