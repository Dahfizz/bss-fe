var global$1 = "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, _gsScope = "undefined" != typeof window ? window : "undefined" != typeof module && module.exports && void 0 !== global$1 ? global$1 : {}, TweenLite = function (t, e) { var i = {}, s = t.document, r = t.GreenSockGlobals = t.GreenSockGlobals || t; if (r.TweenLite)
    return r.TweenLite; var n, a, o, l, h, _, u, c = function (t) { var e, i = t.split("."), s = r; for (e = 0; e < i.length; e++)
    s[i[e]] = s = s[i[e]] || {}; return s; }, f = c("com.greensock"), p = function (t) { var e, i = [], s = t.length; for (e = 0; e !== s; i.push(t[e++]))
    ; return i; }, m = function () { }, d = (_ = Object.prototype.toString, u = _.call([]), function (t) { return null != t && (t instanceof Array || "object" == typeof t && !!t.push && _.call(t) === u); }), g = {}, y = function (t, e, s, n) { this.sc = g[t] ? g[t].sc : [], g[t] = this, this.gsClass = null, this.func = s; var a = []; this.check = function (o) { for (var l, h, _, u, f = e.length, p = f; --f > -1;)
    (l = g[e[f]] || new y(e[f], [])).gsClass ? (a[f] = l.gsClass, p--) : o && l.sc.push(this); if (0 === p && s)
    for (_ = (h = ("com.greensock." + t).split(".")).pop(), u = c(h.join("."))[_] = this.gsClass = s.apply(s, a), n && (r[_] = i[_] = u), f = 0; f < this.sc.length; f++)
        this.sc[f].check(); }, this.check(!0); }, v = t._gsDefine = function (t, e, i, s) { return new y(t, e, i, s); }, T = f._class = function (t, e, i) { return e = e || function () { }, v(t, [], function () { return e; }, i), e; }; v.globals = r; var x = [0, 0, 1, 1], w = T("easing.Ease", function (t, e, i, s) { this._func = t, this._type = i || 0, this._power = s || 0, this._params = e ? x.concat(e) : x; }, !0), b = w.map = {}, P = w.register = function (t, e, i, s) { for (var r, n, a, o, l = e.split(","), h = l.length, _ = (i || "easeIn,easeOut,easeInOut").split(","); --h > -1;)
    for (n = l[h], r = s ? T("easing." + n, null, !0) : f.easing[n] || {}, a = _.length; --a > -1;)
        o = _[a], b[n + "." + o] = b[o + n] = r[o] = t.getRatio ? t : t[o] || new t; }; for ((o = w.prototype)._calcEnd = !1, o.getRatio = function (t) { if (this._func)
    return this._params[0] = t, this._func.apply(null, this._params); var e = this._type, i = this._power, s = 1 === e ? 1 - t : 2 === e ? t : t < .5 ? 2 * t : 2 * (1 - t); return 1 === i ? s *= s : 2 === i ? s *= s * s : 3 === i ? s *= s * s * s : 4 === i && (s *= s * s * s * s), 1 === e ? 1 - s : 2 === e ? s : t < .5 ? s / 2 : 1 - s / 2; }, a = (n = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"]).length; --a > -1;)
    o = n[a] + ",Power" + a, P(new w(null, null, 1, a), o, "easeOut", !0), P(new w(null, null, 2, a), o, "easeIn" + (0 === a ? ",easeNone" : "")), P(new w(null, null, 3, a), o, "easeInOut"); b.linear = f.easing.Linear.easeIn, b.swing = f.easing.Quad.easeInOut; var S = T("events.EventDispatcher", function (t) { this._listeners = {}, this._eventTarget = t || this; }); (o = S.prototype).addEventListener = function (t, e, i, s, r) { r = r || 0; var n, a, o = this._listeners[t], _ = 0; for (this !== l || h || l.wake(), null == o && (this._listeners[t] = o = []), a = o.length; --a > -1;)
    (n = o[a]).c === e && n.s === i ? o.splice(a, 1) : 0 === _ && n.pr < r && (_ = a + 1); o.splice(_, 0, { c: e, s: i, up: s, pr: r }); }, o.removeEventListener = function (t, e) { var i, s = this._listeners[t]; if (s)
    for (i = s.length; --i > -1;)
        if (s[i].c === e)
            return void s.splice(i, 1); }, o.dispatchEvent = function (t) { var e, i, s, r = this._listeners[t]; if (r)
    for ((e = r.length) > 1 && (r = r.slice(0)), i = this._eventTarget; --e > -1;)
        (s = r[e]) && (s.up ? s.c.call(s.s || i, { type: t, target: i }) : s.c.call(s.s || i)); }; var O = t.requestAnimationFrame, k = t.cancelAnimationFrame, R = Date.now || function () { return (new Date).getTime(); }, A = R(); for (a = (n = ["ms", "moz", "webkit", "o"]).length; --a > -1 && !O;)
    O = t[n[a] + "RequestAnimationFrame"], k = t[n[a] + "CancelAnimationFrame"] || t[n[a] + "CancelRequestAnimationFrame"]; T("Ticker", function (t, e) { var i, r, n, a, o, _ = this, u = R(), c = !(!1 === e || !O) && "auto", f = 500, p = 33, d = function (t) { var e, s, l = R() - A; l > f && (u += l - p), A += l, _.time = (A - u) / 1e3, e = _.time - o, (!i || e > 0 || !0 === t) && (_.frame++, o += e + (e >= a ? .004 : a - e), s = !0), !0 !== t && (n = r(d)), s && _.dispatchEvent("tick"); }; S.call(_), _.time = _.frame = 0, _.tick = function () { d(!0); }, _.lagSmoothing = function (t, e) { if (!arguments.length)
    return f < 1e10; f = t || 1e10, p = Math.min(e, f, 0); }, _.sleep = function () { null != n && (c && k ? k(n) : clearTimeout(n), r = m, n = null, _ === l && (h = !1)); }, _.wake = function (t) { null !== n ? _.sleep() : t ? u += -A + (A = R()) : _.frame > 10 && (A = R() - f + 5), r = 0 === i ? m : c && O ? O : function (t) { return setTimeout(t, 1e3 * (o - _.time) + 1 | 0); }, _ === l && (h = !0), d(2); }, _.fps = function (t) { if (!arguments.length)
    return i; a = 1 / ((i = t) || 60), o = this.time + a, _.wake(); }, _.useRAF = function (t) { if (!arguments.length)
    return c; _.sleep(), c = t, _.fps(i); }, _.fps(t), setTimeout(function () { "auto" === c && _.frame < 5 && "hidden" !== (s || {}).visibilityState && _.useRAF(!1); }, 1500); }), (o = f.Ticker.prototype = new f.events.EventDispatcher).constructor = f.Ticker; var C = T("core.Animation", function (t, e) { if (this.vars = e = e || {}, this._duration = this._totalDuration = t || 0, this._delay = Number(e.delay) || 0, this._timeScale = 1, this._active = !0 === e.immediateRender, this.data = e.data, this._reversed = !0 === e.reversed, Z) {
    h || l.wake();
    var i = this.vars.useFrames ? $ : Z;
    i.add(this, i._time), this.vars.paused && this.paused(!0);
} }); l = C.ticker = new f.Ticker, (o = C.prototype)._dirty = o._gc = o._initted = o._paused = !1, o._totalTime = o._time = 0, o._rawPrevTime = -1, o._next = o._last = o._onUpdate = o._timeline = o.timeline = null, o._paused = !1; var M = function () { h && R() - A > 2e3 && ("hidden" !== (s || {}).visibilityState || !l.lagSmoothing()) && l.wake(); var t = setTimeout(M, 2e3); t.unref && t.unref(); }; M(), o.play = function (t, e) { return null != t && this.seek(t, e), this.reversed(!1).paused(!1); }, o.pause = function (t, e) { return null != t && this.seek(t, e), this.paused(!0); }, o.resume = function (t, e) { return null != t && this.seek(t, e), this.paused(!1); }, o.seek = function (t, e) { return this.totalTime(Number(t), !1 !== e); }, o.restart = function (t, e) { return this.reversed(!1).paused(!1).totalTime(t ? -this._delay : 0, !1 !== e, !0); }, o.reverse = function (t, e) { return null != t && this.seek(t || this.totalDuration(), e), this.reversed(!0).paused(!1); }, o.render = function (t, e, i) { }, o.invalidate = function () { return this._time = this._totalTime = 0, this._initted = this._gc = !1, this._rawPrevTime = -1, !this._gc && this.timeline || this._enabled(!0), this; }, o.isActive = function () { var t, e = this._timeline, i = this._startTime; return !e || !this._gc && !this._paused && e.isActive() && (t = e.rawTime(!0)) >= i && t < i + this.totalDuration() / this._timeScale - 1e-7; }, o._enabled = function (t, e) { return h || l.wake(), this._gc = !t, this._active = this.isActive(), !0 !== e && (t && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !t && this.timeline && this._timeline._remove(this, !0)), !1; }, o._kill = function (t, e) { return this._enabled(!1, !1); }, o.kill = function (t, e) { return this._kill(t, e), this; }, o._uncache = function (t) { for (var e = t ? this : this.timeline; e;)
    e._dirty = !0, e = e.timeline; return this; }, o._swapSelfInParams = function (t) { for (var e = t.length, i = t.concat(); --e > -1;)
    "{self}" === t[e] && (i[e] = this); return i; }, o._callback = function (t) { var e = this.vars, i = e[t], s = e[t + "Params"], r = e[t + "Scope"] || e.callbackScope || this; switch (s ? s.length : 0) {
    case 0:
        i.call(r);
        break;
    case 1:
        i.call(r, s[0]);
        break;
    case 2:
        i.call(r, s[0], s[1]);
        break;
    default: i.apply(r, s);
} }, o.eventCallback = function (t, e, i, s) { if ("on" === (t || "").substr(0, 2)) {
    var r = this.vars;
    if (1 === arguments.length)
        return r[t];
    null == e ? delete r[t] : (r[t] = e, r[t + "Params"] = d(i) && -1 !== i.join("").indexOf("{self}") ? this._swapSelfInParams(i) : i, r[t + "Scope"] = s), "onUpdate" === t && (this._onUpdate = e);
} return this; }, o.delay = function (t) { return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + t - this._delay), this._delay = t, this) : this._delay; }, o.duration = function (t) { return arguments.length ? (this._duration = this._totalDuration = t, this._uncache(!0), this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== t && this.totalTime(this._totalTime * (t / this._duration), !0), this) : (this._dirty = !1, this._duration); }, o.totalDuration = function (t) { return this._dirty = !1, arguments.length ? this.duration(t) : this._totalDuration; }, o.time = function (t, e) { return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(t > this._duration ? this._duration : t, e)) : this._time; }, o.totalTime = function (t, e, i) { if (h || l.wake(), !arguments.length)
    return this._totalTime; if (this._timeline) {
    if (t < 0 && !i && (t += this.totalDuration()), this._timeline.smoothChildTiming) {
        this._dirty && this.totalDuration();
        var s = this._totalDuration, r = this._timeline;
        if (t > s && !i && (t = s), this._startTime = (this._paused ? this._pauseTime : r._time) - (this._reversed ? s - t : t) / this._timeScale, r._dirty || this._uncache(!1), r._timeline)
            for (; r._timeline;)
                r._timeline._time !== (r._startTime + r._totalTime) / r._timeScale && r.totalTime(r._totalTime, !0), r = r._timeline;
    }
    this._gc && this._enabled(!0, !1), this._totalTime === t && 0 !== this._duration || (z.length && Q(), this.render(t, e, !1), z.length && Q());
} return this; }, o.progress = o.totalProgress = function (t, e) { var i = this.duration(); return arguments.length ? this.totalTime(i * t, e) : i ? this._time / i : this.ratio; }, o.startTime = function (t) { return arguments.length ? (t !== this._startTime && (this._startTime = t, this.timeline && this.timeline._sortChildren && this.timeline.add(this, t - this._delay)), this) : this._startTime; }, o.endTime = function (t) { return this._startTime + (0 != t ? this.totalDuration() : this.duration()) / this._timeScale; }, o.timeScale = function (t) { if (!arguments.length)
    return this._timeScale; var e, i; for (t = t || 1e-10, this._timeline && this._timeline.smoothChildTiming && (i = (e = this._pauseTime) || 0 === e ? e : this._timeline.totalTime(), this._startTime = i - (i - this._startTime) * this._timeScale / t), this._timeScale = t, i = this.timeline; i && i.timeline;)
    i._dirty = !0, i.totalDuration(), i = i.timeline; return this; }, o.reversed = function (t) { return arguments.length ? (t != this._reversed && (this._reversed = t, this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)), this) : this._reversed; }, o.paused = function (t) { if (!arguments.length)
    return this._paused; var e, i, s = this._timeline; return t != this._paused && s && (h || t || l.wake(), i = (e = s.rawTime()) - this._pauseTime, !t && s.smoothChildTiming && (this._startTime += i, this._uncache(!1)), this._pauseTime = t ? e : null, this._paused = t, this._active = this.isActive(), !t && 0 !== i && this._initted && this.duration() && (e = s.smoothChildTiming ? this._totalTime : (e - this._startTime) / this._timeScale, this.render(e, e === this._totalTime, !0))), this._gc && !t && this._enabled(!0, !1), this; }; var D = T("core.SimpleTimeline", function (t) { C.call(this, 0, t), this.autoRemoveChildren = this.smoothChildTiming = !0; }); (o = D.prototype = new C).constructor = D, o.kill()._gc = !1, o._first = o._last = o._recent = null, o._sortChildren = !1, o.add = o.insert = function (t, e, i, s) { var r, n; if (t._startTime = Number(e || 0) + t._delay, t._paused && this !== t._timeline && (t._pauseTime = this.rawTime() - (t._timeline.rawTime() - t._pauseTime)), t.timeline && t.timeline._remove(t, !0), t.timeline = t._timeline = this, t._gc && t._enabled(!0, !0), r = this._last, this._sortChildren)
    for (n = t._startTime; r && r._startTime > n;)
        r = r._prev; return r ? (t._next = r._next, r._next = t) : (t._next = this._first, this._first = t), t._next ? t._next._prev = t : this._last = t, t._prev = r, this._recent = t, this._timeline && this._uncache(!0), this; }, o._remove = function (t, e) { return t.timeline === this && (e || t._enabled(!1, !0), t._prev ? t._prev._next = t._next : this._first === t && (this._first = t._next), t._next ? t._next._prev = t._prev : this._last === t && (this._last = t._prev), t._next = t._prev = t.timeline = null, t === this._recent && (this._recent = this._last), this._timeline && this._uncache(!0)), this; }, o.render = function (t, e, i) { var s, r = this._first; for (this._totalTime = this._time = this._rawPrevTime = t; r;)
    s = r._next, (r._active || t >= r._startTime && !r._paused && !r._gc) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (t - r._startTime) * r._timeScale, e, i) : r.render((t - r._startTime) * r._timeScale, e, i)), r = s; }, o.rawTime = function () { return h || l.wake(), this._totalTime; }; var L = T("TweenLite", function (e, i, s) { if (C.call(this, i, s), this.render = L.prototype.render, null == e)
    throw "Cannot tween a null target."; this.target = e = "string" != typeof e ? e : L.selector(e) || e; var r, n, a, o = e.jquery || e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType), l = this.vars.overwrite; if (this._overwrite = l = null == l ? W[L.defaultOverwrite] : "number" == typeof l ? l >> 0 : W[l], (o || e instanceof Array || e.push && d(e)) && "number" != typeof e[0])
    for (this._targets = a = p(e), this._propLookup = [], this._siblings = [], r = 0; r < a.length; r++)
        (n = a[r]) ? "string" != typeof n ? n.length && n !== t && n[0] && (n[0] === t || n[0].nodeType && n[0].style && !n.nodeType) ? (a.splice(r--, 1), this._targets = a = a.concat(p(n))) : (this._siblings[r] = K(n, this, !1), 1 === l && this._siblings[r].length > 1 && tt(n, this, null, 1, this._siblings[r])) : "string" == typeof (n = a[r--] = L.selector(n)) && a.splice(r + 1, 1) : a.splice(r--, 1);
else
    this._propLookup = {}, this._siblings = K(e, this, !1), 1 === l && this._siblings.length > 1 && tt(e, this, null, 1, this._siblings); (this.vars.immediateRender || 0 === i && 0 === this._delay && !1 !== this.vars.immediateRender) && (this._time = -1e-10, this.render(Math.min(0, -this._delay))); }, !0), E = function (e) { return e && e.length && e !== t && e[0] && (e[0] === t || e[0].nodeType && e[0].style && !e.nodeType); }; (o = L.prototype = new C).constructor = L, o.kill()._gc = !1, o.ratio = 0, o._firstPT = o._targets = o._overwrittenProps = o._startAt = null, o._notifyPluginsOfEnabled = o._lazy = !1, L.version = "2.0.2", L.defaultEase = o._ease = new w(null, null, 1, 1), L.defaultOverwrite = "auto", L.ticker = l, L.autoSleep = 120, L.lagSmoothing = function (t, e) { l.lagSmoothing(t, e); }, L.selector = t.$ || t.jQuery || function (e) { var i = t.$ || t.jQuery; return i ? (L.selector = i, i(e)) : (s || (s = t.document), s ? s.querySelectorAll ? s.querySelectorAll(e) : s.getElementById("#" === e.charAt(0) ? e.substr(1) : e) : e); }; var z = [], F = {}, I = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi, X = /[\+-]=-?[\.\d]/, B = function (t) { for (var e, i = this._firstPT; i;)
    e = i.blob ? 1 === t && null != this.end ? this.end : t ? this.join("") : this.start : i.c * t + i.s, i.m ? e = i.m.call(this._tween, e, this._target || i.t, this._tween) : e < 1e-6 && e > -1e-6 && !i.blob && (e = 0), i.f ? i.fp ? i.t[i.p](i.fp, e) : i.t[i.p](e) : i.t[i.p] = e, i = i._next; }, N = function (t, e, i, s) { var r, n, a, o, l, h, _, u = [], c = 0, f = "", p = 0; for (u.start = t, u.end = e, t = u[0] = t + "", e = u[1] = e + "", i && (i(u), t = u[0], e = u[1]), u.length = 0, r = t.match(I) || [], n = e.match(I) || [], s && (s._next = null, s.blob = 1, u._firstPT = u._applyPT = s), l = n.length, o = 0; o < l; o++)
    _ = n[o], f += (h = e.substr(c, e.indexOf(_, c) - c)) || !o ? h : ",", c += h.length, p ? p = (p + 1) % 5 : "rgba(" === h.substr(-5) && (p = 1), _ === r[o] || r.length <= o ? f += _ : (f && (u.push(f), f = ""), a = parseFloat(r[o]), u.push(a), u._firstPT = { _next: u._firstPT, t: u, p: u.length - 1, s: a, c: ("=" === _.charAt(1) ? parseInt(_.charAt(0) + "1", 10) * parseFloat(_.substr(2)) : parseFloat(_) - a) || 0, f: 0, m: p && p < 4 ? Math.round : 0 }), c += _.length; return (f += e.substr(c)) && u.push(f), u.setRatio = B, X.test(e) && (u.end = null), u; }, Y = function (t, e, i, s, r, n, a, o, l) { "function" == typeof s && (s = s(l || 0, t)); var h = typeof t[e], _ = "function" !== h ? "" : e.indexOf("set") || "function" != typeof t["get" + e.substr(3)] ? e : "get" + e.substr(3), u = "get" !== i ? i : _ ? a ? t[_](a) : t[_]() : t[e], c = "string" == typeof s && "=" === s.charAt(1), f = { t: t, p: e, s: u, f: "function" === h, pg: 0, n: r || e, m: n ? "function" == typeof n ? n : Math.round : 0, pr: 0, c: c ? parseInt(s.charAt(0) + "1", 10) * parseFloat(s.substr(2)) : parseFloat(s) - u || 0 }; if (("number" != typeof u || "number" != typeof s && !c) && (a || isNaN(u) || !c && isNaN(s) || "boolean" == typeof u || "boolean" == typeof s ? (f.fp = a, f = { t: N(u, c ? parseFloat(f.s) + f.c + (f.s + "").replace(/[0-9\-\.]/g, "") : s, o || L.defaultStringFilter, f), p: "setRatio", s: 0, c: 1, f: 2, pg: 0, n: r || e, pr: 0, m: 0 }) : (f.s = parseFloat(u), c || (f.c = parseFloat(s) - f.s || 0))), f.c)
    return (f._next = this._firstPT) && (f._next._prev = f), this._firstPT = f, f; }, j = L._internals = { isArray: d, isSelector: E, lazyTweens: z, blobDif: N }, U = L._plugins = {}, V = j.tweenLookup = {}, q = 0, G = j.reservedProps = { ease: 1, delay: 1, overwrite: 1, onComplete: 1, onCompleteParams: 1, onCompleteScope: 1, useFrames: 1, runBackwards: 1, startAt: 1, onUpdate: 1, onUpdateParams: 1, onUpdateScope: 1, onStart: 1, onStartParams: 1, onStartScope: 1, onReverseComplete: 1, onReverseCompleteParams: 1, onReverseCompleteScope: 1, onRepeat: 1, onRepeatParams: 1, onRepeatScope: 1, easeParams: 1, yoyo: 1, immediateRender: 1, repeat: 1, repeatDelay: 1, data: 1, paused: 1, reversed: 1, autoCSS: 1, lazy: 1, onOverwrite: 1, callbackScope: 1, stringFilter: 1, id: 1, yoyoEase: 1 }, W = { none: 0, all: 1, auto: 2, concurrent: 3, allOnStart: 4, preexisting: 5, true: 1, false: 0 }, $ = C._rootFramesTimeline = new D, Z = C._rootTimeline = new D, H = 30, Q = j.lazyRender = function () { var t, e = z.length; for (F = {}; --e > -1;)
    (t = z[e]) && !1 !== t._lazy && (t.render(t._lazy[0], t._lazy[1], !0), t._lazy = !1); z.length = 0; }; Z._startTime = l.time, $._startTime = l.frame, Z._active = $._active = !0, setTimeout(Q, 1), C._updateRoot = L.render = function () { var t, e, i; if (z.length && Q(), Z.render((l.time - Z._startTime) * Z._timeScale, !1, !1), $.render((l.frame - $._startTime) * $._timeScale, !1, !1), z.length && Q(), l.frame >= H) {
    for (i in H = l.frame + (parseInt(L.autoSleep, 10) || 120), V) {
        for (t = (e = V[i].tweens).length; --t > -1;)
            e[t]._gc && e.splice(t, 1);
        0 === e.length && delete V[i];
    }
    if ((!(i = Z._first) || i._paused) && L.autoSleep && !$._first && 1 === l._listeners.tick.length) {
        for (; i && i._paused;)
            i = i._next;
        i || l.sleep();
    }
} }, l.addEventListener("tick", C._updateRoot); var K = function (t, e, i) { var s, r, n = t._gsTweenID; if (V[n || (t._gsTweenID = n = "t" + q++)] || (V[n] = { target: t, tweens: [] }), e && ((s = V[n].tweens)[r = s.length] = e, i))
    for (; --r > -1;)
        s[r] === e && s.splice(r, 1); return V[n].tweens; }, J = function (t, e, i, s) { var r, n, a = t.vars.onOverwrite; return a && (r = a(t, e, i, s)), (a = L.onOverwrite) && (n = a(t, e, i, s)), !1 !== r && !1 !== n; }, tt = function (t, e, i, s, r) { var n, a, o, l; if (1 === s || s >= 4) {
    for (l = r.length, n = 0; n < l; n++)
        if ((o = r[n]) !== e)
            o._gc || o._kill(null, t, e) && (a = !0);
        else if (5 === s)
            break;
    return a;
} var h, _ = e._startTime + 1e-10, u = [], c = 0, f = 0 === e._duration; for (n = r.length; --n > -1;)
    (o = r[n]) === e || o._gc || o._paused || (o._timeline !== e._timeline ? (h = h || et(e, 0, f), 0 === et(o, h, f) && (u[c++] = o)) : o._startTime <= _ && o._startTime + o.totalDuration() / o._timeScale > _ && ((f || !o._initted) && _ - o._startTime <= 2e-10 || (u[c++] = o))); for (n = c; --n > -1;)
    if (l = (o = u[n])._firstPT, 2 === s && o._kill(i, t, e) && (a = !0), 2 !== s || !o._firstPT && o._initted && l) {
        if (2 !== s && !J(o, e))
            continue;
        o._enabled(!1, !1) && (a = !0);
    } return a; }, et = function (t, e, i) { for (var s = t._timeline, r = s._timeScale, n = t._startTime; s._timeline;) {
    if (n += s._startTime, r *= s._timeScale, s._paused)
        return -100;
    s = s._timeline;
} return (n /= r) > e ? n - e : i && n === e || !t._initted && n - e < 2e-10 ? 1e-10 : (n += t.totalDuration() / t._timeScale / r) > e + 1e-10 ? 0 : n - e - 1e-10; }; o._init = function () { var t, e, i, s, r, n, a = this.vars, o = this._overwrittenProps, l = this._duration, h = !!a.immediateRender, _ = a.ease; if (a.startAt) {
    for (s in this._startAt && (this._startAt.render(-1, !0), this._startAt.kill()), r = {}, a.startAt)
        r[s] = a.startAt[s];
    if (r.data = "isStart", r.overwrite = !1, r.immediateRender = !0, r.lazy = h && !1 !== a.lazy, r.startAt = r.delay = null, r.onUpdate = a.onUpdate, r.onUpdateParams = a.onUpdateParams, r.onUpdateScope = a.onUpdateScope || a.callbackScope || this, this._startAt = L.to(this.target || {}, 0, r), h)
        if (this._time > 0)
            this._startAt = null;
        else if (0 !== l)
            return;
}
else if (a.runBackwards && 0 !== l)
    if (this._startAt)
        this._startAt.render(-1, !0), this._startAt.kill(), this._startAt = null;
    else {
        for (s in 0 !== this._time && (h = !1), i = {}, a)
            G[s] && "autoCSS" !== s || (i[s] = a[s]);
        if (i.overwrite = 0, i.data = "isFromStart", i.lazy = h && !1 !== a.lazy, i.immediateRender = h, this._startAt = L.to(this.target, 0, i), h) {
            if (0 === this._time)
                return;
        }
        else
            this._startAt._init(), this._startAt._enabled(!1), this.vars.immediateRender && (this._startAt = null);
    } if (this._ease = _ = _ ? _ instanceof w ? _ : "function" == typeof _ ? new w(_, a.easeParams) : b[_] || L.defaultEase : L.defaultEase, a.easeParams instanceof Array && _.config && (this._ease = _.config.apply(_, a.easeParams)), this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, this._targets)
    for (n = this._targets.length, t = 0; t < n; t++)
        this._initProps(this._targets[t], this._propLookup[t] = {}, this._siblings[t], o ? o[t] : null, t) && (e = !0);
else
    e = this._initProps(this.target, this._propLookup, this._siblings, o, 0); if (e && L._onPluginEvent("_onInitAllProps", this), o && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), a.runBackwards)
    for (i = this._firstPT; i;)
        i.s += i.c, i.c = -i.c, i = i._next; this._onUpdate = a.onUpdate, this._initted = !0; }, o._initProps = function (e, i, s, r, n) { var a, o, l, h, _, u; if (null == e)
    return !1; for (a in F[e._gsTweenID] && Q(), this.vars.css || e.style && e !== t && e.nodeType && U.css && !1 !== this.vars.autoCSS && function (t, e) { var i, s = {}; for (i in t)
    G[i] || i in e && "transform" !== i && "x" !== i && "y" !== i && "width" !== i && "height" !== i && "className" !== i && "border" !== i || !(!U[i] || U[i] && U[i]._autoCSS) || (s[i] = t[i], delete t[i]); t.css = s; }(this.vars, e), this.vars)
    if (u = this.vars[a], G[a])
        u && (u instanceof Array || u.push && d(u)) && -1 !== u.join("").indexOf("{self}") && (this.vars[a] = u = this._swapSelfInParams(u, this));
    else if (U[a] && (h = new U[a])._onInitTween(e, this.vars[a], this, n)) {
        for (this._firstPT = _ = { _next: this._firstPT, t: h, p: "setRatio", s: 0, c: 1, f: 1, n: a, pg: 1, pr: h._priority, m: 0 }, o = h._overwriteProps.length; --o > -1;)
            i[h._overwriteProps[o]] = this._firstPT;
        (h._priority || h._onInitAllProps) && (l = !0), (h._onDisable || h._onEnable) && (this._notifyPluginsOfEnabled = !0), _._next && (_._next._prev = _);
    }
    else
        i[a] = Y.call(this, e, a, "get", u, a, 0, null, this.vars.stringFilter, n); return r && this._kill(r, e) ? this._initProps(e, i, s, r, n) : this._overwrite > 1 && this._firstPT && s.length > 1 && tt(e, this, i, this._overwrite, s) ? (this._kill(i, e), this._initProps(e, i, s, r, n)) : (this._firstPT && (!1 !== this.vars.lazy && this._duration || this.vars.lazy && !this._duration) && (F[e._gsTweenID] = !0), l); }, o.render = function (t, e, i) { var s, r, n, a, o = this._time, l = this._duration, h = this._rawPrevTime; if (t >= l - 1e-7 && t >= 0)
    this._totalTime = this._time = l, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, this._reversed || (s = !0, r = "onComplete", i = i || this._timeline.autoRemoveChildren), 0 === l && (this._initted || !this.vars.lazy || i) && (this._startTime === this._timeline._duration && (t = 0), (h < 0 || t <= 0 && t >= -1e-7 || 1e-10 === h && "isPause" !== this.data) && h !== t && (i = !0, h > 1e-10 && (r = "onReverseComplete")), this._rawPrevTime = a = !e || t || h === t ? t : 1e-10);
else if (t < 1e-7)
    this._totalTime = this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== o || 0 === l && h > 0) && (r = "onReverseComplete", s = this._reversed), t < 0 && (this._active = !1, 0 === l && (this._initted || !this.vars.lazy || i) && (h >= 0 && (1e-10 !== h || "isPause" !== this.data) && (i = !0), this._rawPrevTime = a = !e || t || h === t ? t : 1e-10)), (!this._initted || this._startAt && this._startAt.progress()) && (i = !0);
else if (this._totalTime = this._time = t, this._easeType) {
    var _ = t / l, u = this._easeType, c = this._easePower;
    (1 === u || 3 === u && _ >= .5) && (_ = 1 - _), 3 === u && (_ *= 2), 1 === c ? _ *= _ : 2 === c ? _ *= _ * _ : 3 === c ? _ *= _ * _ * _ : 4 === c && (_ *= _ * _ * _ * _), this.ratio = 1 === u ? 1 - _ : 2 === u ? _ : t / l < .5 ? _ / 2 : 1 - _ / 2;
}
else
    this.ratio = this._ease.getRatio(t / l); if (this._time !== o || i) {
    if (!this._initted) {
        if (this._init(), !this._initted || this._gc)
            return;
        if (!i && this._firstPT && (!1 !== this.vars.lazy && this._duration || this.vars.lazy && !this._duration))
            return this._time = this._totalTime = o, this._rawPrevTime = h, z.push(this), void (this._lazy = [t, e]);
        this._time && !s ? this.ratio = this._ease.getRatio(this._time / l) : s && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1));
    }
    for (!1 !== this._lazy && (this._lazy = !1), this._active || !this._paused && this._time !== o && t >= 0 && (this._active = !0), 0 === o && (this._startAt && (t >= 0 ? this._startAt.render(t, !0, i) : r || (r = "_dummyGS")), this.vars.onStart && (0 === this._time && 0 !== l || e || this._callback("onStart"))), n = this._firstPT; n;)
        n.f ? n.t[n.p](n.c * this.ratio + n.s) : n.t[n.p] = n.c * this.ratio + n.s, n = n._next;
    this._onUpdate && (t < 0 && this._startAt && -1e-4 !== t && this._startAt.render(t, !0, i), e || (this._time !== o || s || i) && this._callback("onUpdate")), r && (this._gc && !i || (t < 0 && this._startAt && !this._onUpdate && -1e-4 !== t && this._startAt.render(t, !0, i), s && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[r] && this._callback(r), 0 === l && 1e-10 === this._rawPrevTime && 1e-10 !== a && (this._rawPrevTime = 0)));
} }, o._kill = function (t, e, i) { if ("all" === t && (t = null), null == t && (null == e || e === this.target))
    return this._lazy = !1, this._enabled(!1, !1); e = "string" != typeof e ? e || this._targets || this.target : L.selector(e) || e; var s, r, n, a, o, l, h, _, u, c = i && this._time && i._startTime === this._startTime && this._timeline === i._timeline, f = this._firstPT; if ((d(e) || E(e)) && "number" != typeof e[0])
    for (s = e.length; --s > -1;)
        this._kill(t, e[s], i) && (l = !0);
else {
    if (this._targets) {
        for (s = this._targets.length; --s > -1;)
            if (e === this._targets[s]) {
                o = this._propLookup[s] || {}, this._overwrittenProps = this._overwrittenProps || [], r = this._overwrittenProps[s] = t ? this._overwrittenProps[s] || {} : "all";
                break;
            }
    }
    else {
        if (e !== this.target)
            return !1;
        o = this._propLookup, r = this._overwrittenProps = t ? this._overwrittenProps || {} : "all";
    }
    if (o) {
        if (h = t || o, _ = t !== r && "all" !== r && t !== o && ("object" != typeof t || !t._tempKill), i && (L.onOverwrite || this.vars.onOverwrite)) {
            for (n in h)
                o[n] && (u || (u = []), u.push(n));
            if ((u || !t) && !J(this, i, e, u))
                return !1;
        }
        for (n in h)
            (a = o[n]) && (c && (a.f ? a.t[a.p](a.s) : a.t[a.p] = a.s, l = !0), a.pg && a.t._kill(h) && (l = !0), a.pg && 0 !== a.t._overwriteProps.length || (a._prev ? a._prev._next = a._next : a === this._firstPT && (this._firstPT = a._next), a._next && (a._next._prev = a._prev), a._next = a._prev = null), delete o[n]), _ && (r[n] = 1);
        !this._firstPT && this._initted && f && this._enabled(!1, !1);
    }
} return l; }, o.invalidate = function () { return this._notifyPluginsOfEnabled && L._onPluginEvent("_onDisable", this), this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null, this._notifyPluginsOfEnabled = this._active = this._lazy = !1, this._propLookup = this._targets ? {} : [], C.prototype.invalidate.call(this), this.vars.immediateRender && (this._time = -1e-10, this.render(Math.min(0, -this._delay))), this; }, o._enabled = function (t, e) { if (h || l.wake(), t && this._gc) {
    var i, s = this._targets;
    if (s)
        for (i = s.length; --i > -1;)
            this._siblings[i] = K(s[i], this, !0);
    else
        this._siblings = K(this.target, this, !0);
} return C.prototype._enabled.call(this, t, e), !(!this._notifyPluginsOfEnabled || !this._firstPT) && L._onPluginEvent(t ? "_onEnable" : "_onDisable", this); }, L.to = function (t, e, i) { return new L(t, e, i); }, L.from = function (t, e, i) { return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new L(t, e, i); }, L.fromTo = function (t, e, i, s) { return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, new L(t, e, s); }, L.delayedCall = function (t, e, i, s, r) { return new L(e, 0, { delay: t, onComplete: e, onCompleteParams: i, callbackScope: s, onReverseComplete: e, onReverseCompleteParams: i, immediateRender: !1, lazy: !1, useFrames: r, overwrite: 0 }); }, L.set = function (t, e) { return new L(t, 0, e); }, L.getTweensOf = function (t, e) { if (null == t)
    return []; var i, s, r, n; if (t = "string" != typeof t ? t : L.selector(t) || t, (d(t) || E(t)) && "number" != typeof t[0]) {
    for (i = t.length, s = []; --i > -1;)
        s = s.concat(L.getTweensOf(t[i], e));
    for (i = s.length; --i > -1;)
        for (n = s[i], r = i; --r > -1;)
            n === s[r] && s.splice(i, 1);
}
else if (t._gsTweenID)
    for (i = (s = K(t).concat()).length; --i > -1;)
        (s[i]._gc || e && !s[i].isActive()) && s.splice(i, 1); return s || []; }, L.killTweensOf = L.killDelayedCallsTo = function (t, e, i) { "object" == typeof e && (i = e, e = !1); for (var s = L.getTweensOf(t, e), r = s.length; --r > -1;)
    s[r]._kill(i, t); }; var it = T("plugins.TweenPlugin", function (t, e) { this._overwriteProps = (t || "").split(","), this._propName = this._overwriteProps[0], this._priority = e || 0, this._super = it.prototype; }, !0); if (o = it.prototype, it.version = "1.19.0", it.API = 2, o._firstPT = null, o._addTween = Y, o.setRatio = B, o._kill = function (t) { var e, i = this._overwriteProps, s = this._firstPT; if (null != t[this._propName])
    this._overwriteProps = [];
else
    for (e = i.length; --e > -1;)
        null != t[i[e]] && i.splice(e, 1); for (; s;)
    null != t[s.n] && (s._next && (s._next._prev = s._prev), s._prev ? (s._prev._next = s._next, s._prev = null) : this._firstPT === s && (this._firstPT = s._next)), s = s._next; return !1; }, o._mod = o._roundProps = function (t) { for (var e, i = this._firstPT; i;)
    (e = t[this._propName] || null != i.n && t[i.n.split(this._propName + "_").join("")]) && "function" == typeof e && (2 === i.f ? i.t._applyPT.m = e : i.m = e), i = i._next; }, L._onPluginEvent = function (t, e) { var i, s, r, n, a, o = e._firstPT; if ("_onInitAllProps" === t) {
    for (; o;) {
        for (a = o._next, s = r; s && s.pr > o.pr;)
            s = s._next;
        (o._prev = s ? s._prev : n) ? o._prev._next = o : r = o, (o._next = s) ? s._prev = o : n = o, o = a;
    }
    o = e._firstPT = r;
} for (; o;)
    o.pg && "function" == typeof o.t[t] && o.t[t]() && (i = !0), o = o._next; return i; }, it.activate = function (t) { for (var e = t.length; --e > -1;)
    t[e].API === it.API && (U[(new t[e])._propName] = t[e]); return !0; }, v.plugin = function (t) { if (!(t && t.propName && t.init && t.API))
    throw "illegal plugin definition."; var e, i = t.propName, s = t.priority || 0, r = t.overwriteProps, n = { init: "_onInitTween", set: "setRatio", kill: "_kill", round: "_mod", mod: "_mod", initAll: "_onInitAllProps" }, a = T("plugins." + i.charAt(0).toUpperCase() + i.substr(1) + "Plugin", function () { it.call(this, i, s), this._overwriteProps = r || []; }, !0 === t.global), o = a.prototype = new it(i); for (e in o.constructor = a, a.API = t.API, n)
    "function" == typeof t[e] && (o[n[e]] = t[e]); return a.version = t.version, it.activate([a]), a; }, n = t._gsQueue) {
    for (a = 0; a < n.length; a++)
        n[a]();
    for (o in g)
        g[o].func || t.console.log("GSAP encountered missing dependency: " + o);
} return h = !1, L; }(_gsScope), globals = _gsScope.GreenSockGlobals, nonGlobals = globals.com.greensock, SimpleTimeline = nonGlobals.core.SimpleTimeline, Animation = nonGlobals.core.Animation, Ease = globals.Ease, Linear = globals.Linear, Power1 = globals.Power1, Power2 = globals.Power2, Power3 = globals.Power3, Power4 = globals.Power4, TweenPlugin = globals.TweenPlugin, EventDispatcher = nonGlobals.events.EventDispatcher;
_gsScope._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function () { var t = function (t) { SimpleTimeline.call(this, t), this._labels = {}, this.autoRemoveChildren = !0 === this.vars.autoRemoveChildren, this.smoothChildTiming = !0 === this.vars.smoothChildTiming, this._sortChildren = !0, this._onUpdate = this.vars.onUpdate; var e, i, s = this.vars; for (i in s)
    e = s[i], r(e) && -1 !== e.join("").indexOf("{self}") && (s[i] = this._swapSelfInParams(e)); r(s.tweens) && this.add(s.tweens, 0, s.align, s.stagger); }, e = TweenLite._internals, i = t._internals = {}, s = e.isSelector, r = e.isArray, n = e.lazyTweens, a = e.lazyRender, o = _gsScope._gsDefine.globals, l = function (t) { var e, i = {}; for (e in t)
    i[e] = t[e]; return i; }, h = function (t, e, i) { var s, r, n = t.cycle; for (s in n)
    r = n[s], t[s] = "function" == typeof r ? r(i, e[i]) : r[i % r.length]; delete t.cycle; }, _ = i.pauseCallback = function () { }, u = function (t) { var e, i = [], s = t.length; for (e = 0; e !== s; i.push(t[e++]))
    ; return i; }, c = t.prototype = new SimpleTimeline; return t.version = "2.0.2", c.constructor = t, c.kill()._gc = c._forcingPlayhead = c._hasPause = !1, c.to = function (t, e, i, s) { var r = i.repeat && o.TweenMax || TweenLite; return e ? this.add(new r(t, e, i), s) : this.set(t, i, s); }, c.from = function (t, e, i, s) { return this.add((i.repeat && o.TweenMax || TweenLite).from(t, e, i), s); }, c.fromTo = function (t, e, i, s, r) { var n = s.repeat && o.TweenMax || TweenLite; return e ? this.add(n.fromTo(t, e, i, s), r) : this.set(t, s, r); }, c.staggerTo = function (e, i, r, n, a, o, _, c) { var f, p, m = new t({ onComplete: o, onCompleteParams: _, callbackScope: c, smoothChildTiming: this.smoothChildTiming }), d = r.cycle; for ("string" == typeof e && (e = TweenLite.selector(e) || e), s(e = e || []) && (e = u(e)), (n = n || 0) < 0 && ((e = u(e)).reverse(), n *= -1), p = 0; p < e.length; p++)
    (f = l(r)).startAt && (f.startAt = l(f.startAt), f.startAt.cycle && h(f.startAt, e, p)), d && (h(f, e, p), null != f.duration && (i = f.duration, delete f.duration)), m.to(e[p], i, f, p * n); return this.add(m, a); }, c.staggerFrom = function (t, e, i, s, r, n, a, o) { return i.immediateRender = 0 != i.immediateRender, i.runBackwards = !0, this.staggerTo(t, e, i, s, r, n, a, o); }, c.staggerFromTo = function (t, e, i, s, r, n, a, o, l) { return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, this.staggerTo(t, e, s, r, n, a, o, l); }, c.call = function (t, e, i, s) { return this.add(TweenLite.delayedCall(0, t, e, i), s); }, c.set = function (t, e, i) { return i = this._parseTimeOrLabel(i, 0, !0), null == e.immediateRender && (e.immediateRender = i === this._time && !this._paused), this.add(new TweenLite(t, 0, e), i); }, t.exportRoot = function (e, i) { null == (e = e || {}).smoothChildTiming && (e.smoothChildTiming = !0); var s, r, n, a, o = new t(e), l = o._timeline; for (null == i && (i = !0), l._remove(o, !0), o._startTime = 0, o._rawPrevTime = o._time = o._totalTime = l._time, n = l._first; n;)
    a = n._next, i && n instanceof TweenLite && n.target === n.vars.onComplete || ((r = n._startTime - n._delay) < 0 && (s = 1), o.add(n, r)), n = a; return l.add(o, 0), s && o.totalDuration(), o; }, c.add = function (e, i, s, n) { var a, o, l, h, _, u; if ("number" != typeof i && (i = this._parseTimeOrLabel(i, 0, !0, e)), !(e instanceof Animation)) {
    if (e instanceof Array || e && e.push && r(e)) {
        for (s = s || "normal", n = n || 0, a = i, o = e.length, l = 0; l < o; l++)
            r(h = e[l]) && (h = new t({ tweens: h })), this.add(h, a), "string" != typeof h && "function" != typeof h && ("sequence" === s ? a = h._startTime + h.totalDuration() / h._timeScale : "start" === s && (h._startTime -= h.delay())), a += n;
        return this._uncache(!0);
    }
    if ("string" == typeof e)
        return this.addLabel(e, i);
    if ("function" != typeof e)
        throw "Cannot add " + e + " into the timeline; it is not a tween, timeline, function, or string.";
    e = TweenLite.delayedCall(0, e);
} if (SimpleTimeline.prototype.add.call(this, e, i), e._time && (a = Math.max(0, Math.min(e.totalDuration(), (this.rawTime() - e._startTime) * e._timeScale)), Math.abs(a - e._totalTime) > 1e-5 && e.render(a, !1, !1)), (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())
    for (u = (_ = this).rawTime() > e._startTime; _._timeline;)
        u && _._timeline.smoothChildTiming ? _.totalTime(_._totalTime, !0) : _._gc && _._enabled(!0, !1), _ = _._timeline; return this; }, c.remove = function (t) { if (t instanceof Animation) {
    this._remove(t, !1);
    var e = t._timeline = t.vars.useFrames ? Animation._rootFramesTimeline : Animation._rootTimeline;
    return t._startTime = (t._paused ? t._pauseTime : e._time) - (t._reversed ? t.totalDuration() - t._totalTime : t._totalTime) / t._timeScale, this;
} if (t instanceof Array || t && t.push && r(t)) {
    for (var i = t.length; --i > -1;)
        this.remove(t[i]);
    return this;
} return "string" == typeof t ? this.removeLabel(t) : this.kill(null, t); }, c._remove = function (t, e) { return SimpleTimeline.prototype._remove.call(this, t, e), this._last ? this._time > this.duration() && (this._time = this._duration, this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0, this; }, c.append = function (t, e) { return this.add(t, this._parseTimeOrLabel(null, e, !0, t)); }, c.insert = c.insertMultiple = function (t, e, i, s) { return this.add(t, e || 0, i, s); }, c.appendMultiple = function (t, e, i, s) { return this.add(t, this._parseTimeOrLabel(null, e, !0, t), i, s); }, c.addLabel = function (t, e) { return this._labels[t] = this._parseTimeOrLabel(e), this; }, c.addPause = function (t, e, i, s) { var r = TweenLite.delayedCall(0, _, i, s || this); return r.vars.onComplete = r.vars.onReverseComplete = e, r.data = "isPause", this._hasPause = !0, this.add(r, t); }, c.removeLabel = function (t) { return delete this._labels[t], this; }, c.getLabelTime = function (t) { return null != this._labels[t] ? this._labels[t] : -1; }, c._parseTimeOrLabel = function (t, e, i, s) { var n, a; if (s instanceof Animation && s.timeline === this)
    this.remove(s);
else if (s && (s instanceof Array || s.push && r(s)))
    for (a = s.length; --a > -1;)
        s[a] instanceof Animation && s[a].timeline === this && this.remove(s[a]); if (n = "number" != typeof t || e ? this.duration() > 99999999999 ? this.recent().endTime(!1) : this._duration : 0, "string" == typeof e)
    return this._parseTimeOrLabel(e, i && "number" == typeof t && null == this._labels[e] ? t - n : 0, i); if (e = e || 0, "string" != typeof t || !isNaN(t) && null == this._labels[t])
    null == t && (t = n);
else {
    if (-1 === (a = t.indexOf("=")))
        return null == this._labels[t] ? i ? this._labels[t] = n + e : e : this._labels[t] + e;
    e = parseInt(t.charAt(a - 1) + "1", 10) * Number(t.substr(a + 1)), t = a > 1 ? this._parseTimeOrLabel(t.substr(0, a - 1), 0, i) : n;
} return Number(t) + e; }, c.seek = function (t, e) { return this.totalTime("number" == typeof t ? t : this._parseTimeOrLabel(t), !1 !== e); }, c.stop = function () { return this.paused(!0); }, c.gotoAndPlay = function (t, e) { return this.play(t, e); }, c.gotoAndStop = function (t, e) { return this.pause(t, e); }, c.render = function (t, e, i) { this._gc && this._enabled(!0, !1); var s, r, o, l, h, _, u, c = this._time, f = this._dirty ? this.totalDuration() : this._totalDuration, p = this._startTime, m = this._timeScale, d = this._paused; if (c !== this._time && (t += this._time - c), t >= f - 1e-7 && t >= 0)
    this._totalTime = this._time = f, this._reversed || this._hasPausedChild() || (r = !0, l = "onComplete", h = !!this._timeline.autoRemoveChildren, 0 === this._duration && (t <= 0 && t >= -1e-7 || this._rawPrevTime < 0 || 1e-10 === this._rawPrevTime) && this._rawPrevTime !== t && this._first && (h = !0, this._rawPrevTime > 1e-10 && (l = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : 1e-10, t = f + 1e-4;
else if (t < 1e-7)
    if (this._totalTime = this._time = 0, (0 !== c || 0 === this._duration && 1e-10 !== this._rawPrevTime && (this._rawPrevTime > 0 || t < 0 && this._rawPrevTime >= 0)) && (l = "onReverseComplete", r = this._reversed), t < 0)
        this._active = !1, this._timeline.autoRemoveChildren && this._reversed ? (h = r = !0, l = "onReverseComplete") : this._rawPrevTime >= 0 && this._first && (h = !0), this._rawPrevTime = t;
    else {
        if (this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : 1e-10, 0 === t && r)
            for (s = this._first; s && 0 === s._startTime;)
                s._duration || (r = !1), s = s._next;
        t = 0, this._initted || (h = !0);
    }
else {
    if (this._hasPause && !this._forcingPlayhead && !e) {
        if (t >= c)
            for (s = this._first; s && s._startTime <= t && !_;)
                s._duration || "isPause" !== s.data || s.ratio || 0 === s._startTime && 0 === this._rawPrevTime || (_ = s), s = s._next;
        else
            for (s = this._last; s && s._startTime >= t && !_;)
                s._duration || "isPause" === s.data && s._rawPrevTime > 0 && (_ = s), s = s._prev;
        _ && (this._time = t = _._startTime, this._totalTime = t + this._cycle * (this._totalDuration + this._repeatDelay));
    }
    this._totalTime = this._time = this._rawPrevTime = t;
} if (this._time !== c && this._first || i || h || _) {
    if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== c && t > 0 && (this._active = !0), 0 === c && this.vars.onStart && (0 === this._time && this._duration || e || this._callback("onStart")), (u = this._time) >= c)
        for (s = this._first; s && (o = s._next, u === this._time && (!this._paused || d));)
            (s._active || s._startTime <= u && !s._paused && !s._gc) && (_ === s && this.pause(), s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), s = o;
    else
        for (s = this._last; s && (o = s._prev, u === this._time && (!this._paused || d));) {
            if (s._active || s._startTime <= c && !s._paused && !s._gc) {
                if (_ === s) {
                    for (_ = s._prev; _ && _.endTime() > this._time;)
                        _.render(_._reversed ? _.totalDuration() - (t - _._startTime) * _._timeScale : (t - _._startTime) * _._timeScale, e, i), _ = _._prev;
                    _ = null, this.pause();
                }
                s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i);
            }
            s = o;
        }
    this._onUpdate && (e || (n.length && a(), this._callback("onUpdate"))), l && (this._gc || p !== this._startTime && m === this._timeScale || (0 === this._time || f >= this.totalDuration()) && (r && (n.length && a(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[l] && this._callback(l)));
} }, c._hasPausedChild = function () { for (var e = this._first; e;) {
    if (e._paused || e instanceof t && e._hasPausedChild())
        return !0;
    e = e._next;
} return !1; }, c.getChildren = function (t, e, i, s) { s = s || -9999999999; for (var r = [], n = this._first, a = 0; n;)
    n._startTime < s || (n instanceof TweenLite ? !1 !== e && (r[a++] = n) : (!1 !== i && (r[a++] = n), !1 !== t && (a = (r = r.concat(n.getChildren(!0, e, i))).length))), n = n._next; return r; }, c.getTweensOf = function (t, e) { var i, s, r = this._gc, n = [], a = 0; for (r && this._enabled(!0, !0), s = (i = TweenLite.getTweensOf(t)).length; --s > -1;)
    (i[s].timeline === this || e && this._contains(i[s])) && (n[a++] = i[s]); return r && this._enabled(!1, !0), n; }, c.recent = function () { return this._recent; }, c._contains = function (t) { for (var e = t.timeline; e;) {
    if (e === this)
        return !0;
    e = e.timeline;
} return !1; }, c.shiftChildren = function (t, e, i) { i = i || 0; for (var s, r = this._first, n = this._labels; r;)
    r._startTime >= i && (r._startTime += t), r = r._next; if (e)
    for (s in n)
        n[s] >= i && (n[s] += t); return this._uncache(!0); }, c._kill = function (t, e) { if (!t && !e)
    return this._enabled(!1, !1); for (var i = e ? this.getTweensOf(e) : this.getChildren(!0, !0, !1), s = i.length, r = !1; --s > -1;)
    i[s]._kill(t, e) && (r = !0); return r; }, c.clear = function (t) { var e = this.getChildren(!1, !0, !0), i = e.length; for (this._time = this._totalTime = 0; --i > -1;)
    e[i]._enabled(!1, !1); return !1 !== t && (this._labels = {}), this._uncache(!0); }, c.invalidate = function () { for (var t = this._first; t;)
    t.invalidate(), t = t._next; return Animation.prototype.invalidate.call(this); }, c._enabled = function (t, e) { if (t === this._gc)
    for (var i = this._first; i;)
        i._enabled(t, !0), i = i._next; return SimpleTimeline.prototype._enabled.call(this, t, e); }, c.totalTime = function (t, e, i) { this._forcingPlayhead = !0; var s = Animation.prototype.totalTime.apply(this, arguments); return this._forcingPlayhead = !1, s; }, c.duration = function (t) { return arguments.length ? (0 !== this.duration() && 0 !== t && this.timeScale(this._duration / t), this) : (this._dirty && this.totalDuration(), this._duration); }, c.totalDuration = function (t) { if (!arguments.length) {
    if (this._dirty) {
        for (var e, i, s = 0, r = this._last, n = 999999999999; r;)
            e = r._prev, r._dirty && r.totalDuration(), r._startTime > n && this._sortChildren && !r._paused && !this._calculatingDuration ? (this._calculatingDuration = 1, this.add(r, r._startTime - r._delay), this._calculatingDuration = 0) : n = r._startTime, r._startTime < 0 && !r._paused && (s -= r._startTime, this._timeline.smoothChildTiming && (this._startTime += r._startTime / this._timeScale, this._time -= r._startTime, this._totalTime -= r._startTime, this._rawPrevTime -= r._startTime), this.shiftChildren(-r._startTime, !1, -9999999999), n = 0), (i = r._startTime + r._totalDuration / r._timeScale) > s && (s = i), r = e;
        this._duration = this._totalDuration = s, this._dirty = !1;
    }
    return this._totalDuration;
} return t && this.totalDuration() ? this.timeScale(this._totalDuration / t) : this; }, c.paused = function (t) { if (!t)
    for (var e = this._first, i = this._time; e;)
        e._startTime === i && "isPause" === e.data && (e._rawPrevTime = 0), e = e._next; return Animation.prototype.paused.apply(this, arguments); }, c.usesFrames = function () { for (var t = this._timeline; t._timeline;)
    t = t._timeline; return t === Animation._rootFramesTimeline; }, c.rawTime = function (t) { return t && (this._paused || this._repeat && this.time() > 0 && this.totalProgress() < 1) ? this._totalTime % (this._duration + this._repeatDelay) : this._paused ? this._totalTime : (this._timeline.rawTime(t) - this._startTime) * this._timeScale; }, t; }, !0);
var TimelineLite = globals.TimelineLite;
_gsScope._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function () { var t = function (t) { TimelineLite.call(this, t), this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._cycle = 0, this._yoyo = !0 === this.vars.yoyo, this._dirty = !0; }, e = TweenLite._internals, i = e.lazyTweens, s = e.lazyRender, r = _gsScope._gsDefine.globals, n = new Ease(null, null, 1, 0), a = t.prototype = new TimelineLite; return a.constructor = t, a.kill()._gc = !1, t.version = "2.0.2", a.invalidate = function () { return this._yoyo = !0 === this.vars.yoyo, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), TimelineLite.prototype.invalidate.call(this); }, a.addCallback = function (t, e, i, s) { return this.add(TweenLite.delayedCall(0, t, i, s), e); }, a.removeCallback = function (t, e) { if (t)
    if (null == e)
        this._kill(null, t);
    else
        for (var i = this.getTweensOf(t, !1), s = i.length, r = this._parseTimeOrLabel(e); --s > -1;)
            i[s]._startTime === r && i[s]._enabled(!1, !1); return this; }, a.removePause = function (t) { return this.removeCallback(TimelineLite._internals.pauseCallback, t); }, a.tweenTo = function (t, e) { e = e || {}; var i, s, a, o = { ease: n, useFrames: this.usesFrames(), immediateRender: !1, lazy: !1 }, l = e.repeat && r.TweenMax || TweenLite; for (s in e)
    o[s] = e[s]; return o.time = this._parseTimeOrLabel(t), i = Math.abs(Number(o.time) - this._time) / this._timeScale || .001, a = new l(this, i, o), o.onStart = function () { a.target.paused(!0), a.vars.time === a.target.time() || i !== a.duration() || a.isFromTo || a.duration(Math.abs(a.vars.time - a.target.time()) / a.target._timeScale).render(a.time(), !0, !0), e.onStart && e.onStart.apply(e.onStartScope || e.callbackScope || a, e.onStartParams || []); }, a; }, a.tweenFromTo = function (t, e, i) { i = i || {}, t = this._parseTimeOrLabel(t), i.startAt = { onComplete: this.seek, onCompleteParams: [t], callbackScope: this }, i.immediateRender = !1 !== i.immediateRender; var s = this.tweenTo(e, i); return s.isFromTo = 1, s.duration(Math.abs(s.vars.time - t) / this._timeScale || .001); }, a.render = function (t, e, r) { this._gc && this._enabled(!0, !1); var n, a, o, l, h, _, u, c, f = this._time, p = this._dirty ? this.totalDuration() : this._totalDuration, m = this._duration, d = this._totalTime, g = this._startTime, y = this._timeScale, v = this._rawPrevTime, T = this._paused, x = this._cycle; if (f !== this._time && (t += this._time - f), t >= p - 1e-7 && t >= 0)
    this._locked || (this._totalTime = p, this._cycle = this._repeat), this._reversed || this._hasPausedChild() || (a = !0, l = "onComplete", h = !!this._timeline.autoRemoveChildren, 0 === this._duration && (t <= 0 && t >= -1e-7 || v < 0 || 1e-10 === v) && v !== t && this._first && (h = !0, v > 1e-10 && (l = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : 1e-10, this._yoyo && 0 != (1 & this._cycle) ? this._time = t = 0 : (this._time = m, t = m + 1e-4);
else if (t < 1e-7)
    if (this._locked || (this._totalTime = this._cycle = 0), this._time = 0, (0 !== f || 0 === m && 1e-10 !== v && (v > 0 || t < 0 && v >= 0) && !this._locked) && (l = "onReverseComplete", a = this._reversed), t < 0)
        this._active = !1, this._timeline.autoRemoveChildren && this._reversed ? (h = a = !0, l = "onReverseComplete") : v >= 0 && this._first && (h = !0), this._rawPrevTime = t;
    else {
        if (this._rawPrevTime = m || !e || t || this._rawPrevTime === t ? t : 1e-10, 0 === t && a)
            for (n = this._first; n && 0 === n._startTime;)
                n._duration || (a = !1), n = n._next;
        t = 0, this._initted || (h = !0);
    }
else if (0 === m && v < 0 && (h = !0), this._time = this._rawPrevTime = t, this._locked || (this._totalTime = t, 0 !== this._repeat && (_ = m + this._repeatDelay, this._cycle = this._totalTime / _ >> 0, 0 !== this._cycle && this._cycle === this._totalTime / _ && d <= t && this._cycle--, this._time = this._totalTime - this._cycle * _, this._yoyo && 0 != (1 & this._cycle) && (this._time = m - this._time), this._time > m ? (this._time = m, t = m + 1e-4) : this._time < 0 ? this._time = t = 0 : t = this._time)), this._hasPause && !this._forcingPlayhead && !e) {
    if ((t = this._time) >= f || this._repeat && x !== this._cycle)
        for (n = this._first; n && n._startTime <= t && !u;)
            n._duration || "isPause" !== n.data || n.ratio || 0 === n._startTime && 0 === this._rawPrevTime || (u = n), n = n._next;
    else
        for (n = this._last; n && n._startTime >= t && !u;)
            n._duration || "isPause" === n.data && n._rawPrevTime > 0 && (u = n), n = n._prev;
    u && u._startTime < m && (this._time = t = u._startTime, this._totalTime = t + this._cycle * (this._totalDuration + this._repeatDelay));
} if (this._cycle !== x && !this._locked) {
    var w = this._yoyo && 0 != (1 & x), b = w === (this._yoyo && 0 != (1 & this._cycle)), P = this._totalTime, S = this._cycle, O = this._rawPrevTime, k = this._time;
    if (this._totalTime = x * m, this._cycle < x ? w = !w : this._totalTime += m, this._time = f, this._rawPrevTime = 0 === m ? v - 1e-4 : v, this._cycle = x, this._locked = !0, f = w ? 0 : m, this.render(f, e, 0 === m), e || this._gc || this.vars.onRepeat && (this._cycle = S, this._locked = !1, this._callback("onRepeat")), f !== this._time)
        return;
    if (b && (this._cycle = x, this._locked = !0, f = w ? m + 1e-4 : -1e-4, this.render(f, !0, !1)), this._locked = !1, this._paused && !T)
        return;
    this._time = k, this._totalTime = P, this._cycle = S, this._rawPrevTime = O;
} if (this._time !== f && this._first || r || h || u) {
    if (this._initted || (this._initted = !0), this._active || !this._paused && this._totalTime !== d && t > 0 && (this._active = !0), 0 === d && this.vars.onStart && (0 === this._totalTime && this._totalDuration || e || this._callback("onStart")), (c = this._time) >= f)
        for (n = this._first; n && (o = n._next, c === this._time && (!this._paused || T));)
            (n._active || n._startTime <= this._time && !n._paused && !n._gc) && (u === n && this.pause(), n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, r) : n.render((t - n._startTime) * n._timeScale, e, r)), n = o;
    else
        for (n = this._last; n && (o = n._prev, c === this._time && (!this._paused || T));) {
            if (n._active || n._startTime <= f && !n._paused && !n._gc) {
                if (u === n) {
                    for (u = n._prev; u && u.endTime() > this._time;)
                        u.render(u._reversed ? u.totalDuration() - (t - u._startTime) * u._timeScale : (t - u._startTime) * u._timeScale, e, r), u = u._prev;
                    u = null, this.pause();
                }
                n._reversed ? n.render((n._dirty ? n.totalDuration() : n._totalDuration) - (t - n._startTime) * n._timeScale, e, r) : n.render((t - n._startTime) * n._timeScale, e, r);
            }
            n = o;
        }
    this._onUpdate && (e || (i.length && s(), this._callback("onUpdate"))), l && (this._locked || this._gc || g !== this._startTime && y === this._timeScale || (0 === this._time || p >= this.totalDuration()) && (a && (i.length && s(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[l] && this._callback(l)));
}
else
    d !== this._totalTime && this._onUpdate && (e || this._callback("onUpdate")); }, a.getActive = function (t, e, i) { null == t && (t = !0), null == e && (e = !0), null == i && (i = !1); var s, r, n = [], a = this.getChildren(t, e, i), o = 0, l = a.length; for (s = 0; s < l; s++)
    (r = a[s]).isActive() && (n[o++] = r); return n; }, a.getLabelAfter = function (t) { t || 0 !== t && (t = this._time); var e, i = this.getLabelsArray(), s = i.length; for (e = 0; e < s; e++)
    if (i[e].time > t)
        return i[e].name; return null; }, a.getLabelBefore = function (t) { null == t && (t = this._time); for (var e = this.getLabelsArray(), i = e.length; --i > -1;)
    if (e[i].time < t)
        return e[i].name; return null; }, a.getLabelsArray = function () { var t, e = [], i = 0; for (t in this._labels)
    e[i++] = { time: this._labels[t], name: t }; return e.sort(function (t, e) { return t.time - e.time; }), e; }, a.invalidate = function () { return this._locked = !1, TimelineLite.prototype.invalidate.call(this); }, a.progress = function (t, e) { return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 != (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), e) : this._time / this.duration() || 0; }, a.totalProgress = function (t, e) { return arguments.length ? this.totalTime(this.totalDuration() * t, e) : this._totalTime / this.totalDuration() || 0; }, a.totalDuration = function (t) { return arguments.length ? -1 !== this._repeat && t ? this.timeScale(this.totalDuration() / t) : this : (this._dirty && (TimelineLite.prototype.totalDuration.call(this), this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat), this._totalDuration); }, a.time = function (t, e) { return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), this._yoyo && 0 != (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(t, e)) : this._time; }, a.repeat = function (t) { return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat; }, a.repeatDelay = function (t) { return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay; }, a.yoyo = function (t) { return arguments.length ? (this._yoyo = t, this) : this._yoyo; }, a.currentLabel = function (t) { return arguments.length ? this.seek(t, !0) : this.getLabelBefore(this._time + 1e-8); }, t; }, !0);
var TimelineMax = globals.TimelineMax;
_gsScope._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function () { var t = function (t) { var e, i = [], s = t.length; for (e = 0; e !== s; i.push(t[e++]))
    ; return i; }, e = function (t, e, i) { var s, r, n = t.cycle; for (s in n)
    r = n[s], t[s] = "function" == typeof r ? r(i, e[i]) : r[i % r.length]; delete t.cycle; }, i = function (t, e, s) { TweenLite.call(this, t, e, s), this._cycle = 0, this._yoyo = !0 === this.vars.yoyo || !!this.vars.yoyoEase, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._repeat && this._uncache(!0), this.render = i.prototype.render; }, s = TweenLite._internals, r = s.isSelector, n = s.isArray, a = i.prototype = TweenLite.to({}, .1, {}), o = []; i.version = "2.0.2", a.constructor = i, a.kill()._gc = !1, i.killTweensOf = i.killDelayedCallsTo = TweenLite.killTweensOf, i.getTweensOf = TweenLite.getTweensOf, i.lagSmoothing = TweenLite.lagSmoothing, i.ticker = TweenLite.ticker, i.render = TweenLite.render, a.invalidate = function () { return this._yoyo = !0 === this.vars.yoyo || !!this.vars.yoyoEase, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._yoyoEase = null, this._uncache(!0), TweenLite.prototype.invalidate.call(this); }, a.updateTo = function (t, e) { var i, s = this.ratio, r = this.vars.immediateRender || t.immediateRender; for (i in e && this._startTime < this._timeline._time && (this._startTime = this._timeline._time, this._uncache(!1), this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay)), t)
    this.vars[i] = t[i]; if (this._initted || r)
    if (e)
        this._initted = !1, r && this.render(0, !0, !0);
    else if (this._gc && this._enabled(!0, !1), this._notifyPluginsOfEnabled && this._firstPT && TweenLite._onPluginEvent("_onDisable", this), this._time / this._duration > .998) {
        var n = this._totalTime;
        this.render(0, !0, !1), this._initted = !1, this.render(n, !0, !1);
    }
    else if (this._initted = !1, this._init(), this._time > 0 || r)
        for (var a, o = 1 / (1 - s), l = this._firstPT; l;)
            a = l.s + l.c, l.c *= o, l.s = a - l.c, l = l._next; return this; }, a.render = function (t, e, i) { this._initted || 0 === this._duration && this.vars.repeat && this.invalidate(); var r, n, a, o, l, h, _, u, c, f = this._dirty ? this.totalDuration() : this._totalDuration, p = this._time, m = this._totalTime, d = this._cycle, g = this._duration, y = this._rawPrevTime; if (t >= f - 1e-7 && t >= 0 ? (this._totalTime = f, this._cycle = this._repeat, this._yoyo && 0 != (1 & this._cycle) ? (this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = g, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1), this._reversed || (r = !0, n = "onComplete", i = i || this._timeline.autoRemoveChildren), 0 === g && (this._initted || !this.vars.lazy || i) && (this._startTime === this._timeline._duration && (t = 0), (y < 0 || t <= 0 && t >= -1e-7 || 1e-10 === y && "isPause" !== this.data) && y !== t && (i = !0, y > 1e-10 && (n = "onReverseComplete")), this._rawPrevTime = u = !e || t || y === t ? t : 1e-10)) : t < 1e-7 ? (this._totalTime = this._time = this._cycle = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== m || 0 === g && y > 0) && (n = "onReverseComplete", r = this._reversed), t < 0 && (this._active = !1, 0 === g && (this._initted || !this.vars.lazy || i) && (y >= 0 && (i = !0), this._rawPrevTime = u = !e || t || y === t ? t : 1e-10)), this._initted || (i = !0)) : (this._totalTime = this._time = t, 0 !== this._repeat && (o = g + this._repeatDelay, this._cycle = this._totalTime / o >> 0, 0 !== this._cycle && this._cycle === this._totalTime / o && m <= t && this._cycle--, this._time = this._totalTime - this._cycle * o, this._yoyo && 0 != (1 & this._cycle) && (this._time = g - this._time, (c = this._yoyoEase || this.vars.yoyoEase) && (this._yoyoEase || (!0 !== c || this._initted ? this._yoyoEase = c = !0 === c ? this._ease : c instanceof Ease ? c : Ease.map[c] : (c = this.vars.ease, this._yoyoEase = c = c ? c instanceof Ease ? c : "function" == typeof c ? new Ease(c, this.vars.easeParams) : Ease.map[c] || TweenLite.defaultEase : TweenLite.defaultEase)), this.ratio = c ? 1 - c.getRatio((g - this._time) / g) : 0)), this._time > g ? this._time = g : this._time < 0 && (this._time = 0)), this._easeType && !c ? (l = this._time / g, h = this._easeType, _ = this._easePower, (1 === h || 3 === h && l >= .5) && (l = 1 - l), 3 === h && (l *= 2), 1 === _ ? l *= l : 2 === _ ? l *= l * l : 3 === _ ? l *= l * l * l : 4 === _ && (l *= l * l * l * l), 1 === h ? this.ratio = 1 - l : 2 === h ? this.ratio = l : this._time / g < .5 ? this.ratio = l / 2 : this.ratio = 1 - l / 2) : c || (this.ratio = this._ease.getRatio(this._time / g))), p !== this._time || i || d !== this._cycle) {
    if (!this._initted) {
        if (this._init(), !this._initted || this._gc)
            return;
        if (!i && this._firstPT && (!1 !== this.vars.lazy && this._duration || this.vars.lazy && !this._duration))
            return this._time = p, this._totalTime = m, this._rawPrevTime = y, this._cycle = d, s.lazyTweens.push(this), void (this._lazy = [t, e]);
        !this._time || r || c ? r && this._ease._calcEnd && !c && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1)) : this.ratio = this._ease.getRatio(this._time / g);
    }
    for (!1 !== this._lazy && (this._lazy = !1), this._active || !this._paused && this._time !== p && t >= 0 && (this._active = !0), 0 === m && (2 === this._initted && t > 0 && this._init(), this._startAt && (t >= 0 ? this._startAt.render(t, !0, i) : n || (n = "_dummyGS")), this.vars.onStart && (0 === this._totalTime && 0 !== g || e || this._callback("onStart"))), a = this._firstPT; a;)
        a.f ? a.t[a.p](a.c * this.ratio + a.s) : a.t[a.p] = a.c * this.ratio + a.s, a = a._next;
    this._onUpdate && (t < 0 && this._startAt && this._startTime && this._startAt.render(t, !0, i), e || (this._totalTime !== m || n) && this._callback("onUpdate")), this._cycle !== d && (e || this._gc || this.vars.onRepeat && this._callback("onRepeat")), n && (this._gc && !i || (t < 0 && this._startAt && !this._onUpdate && this._startTime && this._startAt.render(t, !0, i), r && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[n] && this._callback(n), 0 === g && 1e-10 === this._rawPrevTime && 1e-10 !== u && (this._rawPrevTime = 0)));
}
else
    m !== this._totalTime && this._onUpdate && (e || this._callback("onUpdate")); }, i.to = function (t, e, s) { return new i(t, e, s); }, i.from = function (t, e, s) { return s.runBackwards = !0, s.immediateRender = 0 != s.immediateRender, new i(t, e, s); }, i.fromTo = function (t, e, s, r) { return r.startAt = s, r.immediateRender = 0 != r.immediateRender && 0 != s.immediateRender, new i(t, e, r); }, i.staggerTo = i.allTo = function (s, a, l, h, _, u, c) { h = h || 0; var f, p, m, d, g = 0, y = [], v = function () { l.onComplete && l.onComplete.apply(l.onCompleteScope || this, arguments), _.apply(c || l.callbackScope || this, u || o); }, T = l.cycle, x = l.startAt && l.startAt.cycle; for (n(s) || ("string" == typeof s && (s = TweenLite.selector(s) || s), r(s) && (s = t(s))), s = s || [], h < 0 && ((s = t(s)).reverse(), h *= -1), f = s.length - 1, m = 0; m <= f; m++) {
    for (d in p = {}, l)
        p[d] = l[d];
    if (T && (e(p, s, m), null != p.duration && (a = p.duration, delete p.duration)), x) {
        for (d in x = p.startAt = {}, l.startAt)
            x[d] = l.startAt[d];
        e(p.startAt, s, m);
    }
    p.delay = g + (p.delay || 0), m === f && _ && (p.onComplete = v), y[m] = new i(s[m], a, p), g += h;
} return y; }, i.staggerFrom = i.allFrom = function (t, e, s, r, n, a, o) { return s.runBackwards = !0, s.immediateRender = 0 != s.immediateRender, i.staggerTo(t, e, s, r, n, a, o); }, i.staggerFromTo = i.allFromTo = function (t, e, s, r, n, a, o, l) { return r.startAt = s, r.immediateRender = 0 != r.immediateRender && 0 != s.immediateRender, i.staggerTo(t, e, r, n, a, o, l); }, i.delayedCall = function (t, e, s, r, n) { return new i(e, 0, { delay: t, onComplete: e, onCompleteParams: s, callbackScope: r, onReverseComplete: e, onReverseCompleteParams: s, immediateRender: !1, useFrames: n, overwrite: 0 }); }, i.set = function (t, e) { return new i(t, 0, e); }, i.isTweening = function (t) { return TweenLite.getTweensOf(t, !0).length > 0; }; var l = function (t, e) { for (var i = [], s = 0, r = t._first; r;)
    r instanceof TweenLite ? i[s++] = r : (e && (i[s++] = r), s = (i = i.concat(l(r, e))).length), r = r._next; return i; }, h = i.getAllTweens = function (t) { return l(Animation._rootTimeline, t).concat(l(Animation._rootFramesTimeline, t)); }; i.killAll = function (t, e, i, s) { null == e && (e = !0), null == i && (i = !0); var r, n, a, o = h(0 != s), l = o.length, _ = e && i && s; for (a = 0; a < l; a++)
    n = o[a], (_ || n instanceof SimpleTimeline || (r = n.target === n.vars.onComplete) && i || e && !r) && (t ? n.totalTime(n._reversed ? 0 : n.totalDuration()) : n._enabled(!1, !1)); }, i.killChildTweensOf = function (e, a) { if (null != e) {
    var o, l, h, _, u, c = s.tweenLookup;
    if ("string" == typeof e && (e = TweenLite.selector(e) || e), r(e) && (e = t(e)), n(e))
        for (_ = e.length; --_ > -1;)
            i.killChildTweensOf(e[_], a);
    else {
        for (h in o = [], c)
            for (l = c[h].target.parentNode; l;)
                l === e && (o = o.concat(c[h].tweens)), l = l.parentNode;
        for (u = o.length, _ = 0; _ < u; _++)
            a && o[_].totalTime(o[_].totalDuration()), o[_]._enabled(!1, !1);
    }
} }; var _ = function (t, e, i, s) { e = !1 !== e, i = !1 !== i; for (var r, n, a = h(s = !1 !== s), o = e && i && s, l = a.length; --l > -1;)
    n = a[l], (o || n instanceof SimpleTimeline || (r = n.target === n.vars.onComplete) && i || e && !r) && n.paused(t); }; return i.pauseAll = function (t, e, i) { _(!0, t, e, i); }, i.resumeAll = function (t, e, i) { _(!1, t, e, i); }, i.globalTimeScale = function (t) { var e = Animation._rootTimeline, i = TweenLite.ticker.time; return arguments.length ? (t = t || 1e-10, e._startTime = i - (i - e._startTime) * e._timeScale / t, e = Animation._rootFramesTimeline, i = TweenLite.ticker.frame, e._startTime = i - (i - e._startTime) * e._timeScale / t, e._timeScale = Animation._rootTimeline._timeScale = t, t) : e._timeScale; }, a.progress = function (t, e) { return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 != (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), e) : this._time / this.duration(); }, a.totalProgress = function (t, e) { return arguments.length ? this.totalTime(this.totalDuration() * t, e) : this._totalTime / this.totalDuration(); }, a.time = function (t, e) { return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), this._yoyo && 0 != (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(t, e)) : this._time; }, a.duration = function (t) { return arguments.length ? Animation.prototype.duration.call(this, t) : this._duration; }, a.totalDuration = function (t) { return arguments.length ? -1 === this._repeat ? this : this.duration((t - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = -1 === this._repeat ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat, this._dirty = !1), this._totalDuration); }, a.repeat = function (t) { return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat; }, a.repeatDelay = function (t) { return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay; }, a.yoyo = function (t) { return arguments.length ? (this._yoyo = t, this) : this._yoyo; }, i; }, !0);
var TweenMax = globals.TweenMax;
_gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function () { var t, e, i, s, r = function () { TweenPlugin.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = r.prototype.setRatio; }, n = _gsScope._gsDefine.globals, a = {}, o = r.prototype = new TweenPlugin("css"); o.constructor = r, r.version = "2.0.2", r.API = 2, r.defaultTransformPerspective = 0, r.defaultSkewType = "compensated", r.defaultSmoothOrigin = !0, o = "px", r.suffixMap = { top: o, right: o, bottom: o, left: o, width: o, height: o, fontSize: o, padding: o, margin: o, perspective: o, lineHeight: "" }; var l, h, _, u, c, f, p, m, d = /(?:\-|\.|\b)(\d|\.|e\-)+/g, g = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g, y = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi, v = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g, T = /(?:\d|\-|\+|=|#|\.)*/g, x = /opacity *= *([^)]*)/i, w = /opacity:([^;]*)/i, b = /alpha\(opacity *=.+?\)/i, P = /^(rgb|hsl)/, S = /([A-Z])/g, O = /-([a-z])/gi, k = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi, R = function (t, e) { return e.toUpperCase(); }, A = /(?:Left|Right|Width)/i, C = /(M11|M12|M21|M22)=[\d\-\.e]+/gi, M = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i, D = /,(?=[^\)]*(?:\(|$))/gi, L = /[\s,\(]/i, E = Math.PI / 180, z = 180 / Math.PI, F = {}, I = { style: {} }, X = _gsScope.document || { createElement: function () { return I; } }, B = function (t, e) { return X.createElementNS ? X.createElementNS(e || "http://www.w3.org/1999/xhtml", t) : X.createElement(t); }, N = B("div"), Y = B("img"), j = r._internals = { _specialProps: a }, U = (_gsScope.navigator || {}).userAgent || "", V = function () { var t = U.indexOf("Android"), e = B("a"); return _ = -1 !== U.indexOf("Safari") && -1 === U.indexOf("Chrome") && (-1 === t || parseFloat(U.substr(t + 8, 2)) > 3), c = _ && parseFloat(U.substr(U.indexOf("Version/") + 8, 2)) < 6, u = -1 !== U.indexOf("Firefox"), (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(U) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(U)) && (f = parseFloat(RegExp.$1)), !!e && (e.style.cssText = "top:1px;opacity:.55;", /^0.55/.test(e.style.opacity)); }(), q = function (t) { return x.test("string" == typeof t ? t : (t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1; }, G = function (t) { _gsScope.console && console.log(t); }, W = "", $ = "", Z = function (t, e) { var i, s, r = (e = e || N).style; if (void 0 !== r[t])
    return t; for (t = t.charAt(0).toUpperCase() + t.substr(1), i = ["O", "Moz", "ms", "Ms", "Webkit"], s = 5; --s > -1 && void 0 === r[i[s] + t];)
    ; return s >= 0 ? (W = "-" + ($ = 3 === s ? "ms" : i[s]).toLowerCase() + "-", $ + t) : null; }, H = ("undefined" != typeof window ? window : X.defaultView || { getComputedStyle: function () { } }).getComputedStyle, Q = r.getStyle = function (t, e, i, s, r) { var n; return V || "opacity" !== e ? (!s && t.style[e] ? n = t.style[e] : (i = i || H(t)) ? n = i[e] || i.getPropertyValue(e) || i.getPropertyValue(e.replace(S, "-$1").toLowerCase()) : t.currentStyle && (n = t.currentStyle[e]), null == r || n && "none" !== n && "auto" !== n && "auto auto" !== n ? n : r) : q(t); }, K = j.convertToPixels = function (t, e, i, s, n) { if ("px" === s || !s && "lineHeight" !== e)
    return i; if ("auto" === s || !i)
    return 0; var a, o, l, h = A.test(e), _ = t, u = N.style, c = i < 0, f = 1 === i; if (c && (i = -i), f && (i *= 100), "lineHeight" !== e || s)
    if ("%" === s && -1 !== e.indexOf("border"))
        a = i / 100 * (h ? t.clientWidth : t.clientHeight);
    else {
        if (u.cssText = "border:0 solid red;position:" + Q(t, "position") + ";line-height:0;", "%" !== s && _.appendChild && "v" !== s.charAt(0) && "rem" !== s)
            u[h ? "borderLeftWidth" : "borderTopWidth"] = i + s;
        else {
            if (_ = t.parentNode || X.body, -1 !== Q(_, "display").indexOf("flex") && (u.position = "absolute"), o = _._gsCache, l = TweenLite.ticker.frame, o && h && o.time === l)
                return o.width * i / 100;
            u[h ? "width" : "height"] = i + s;
        }
        _.appendChild(N), a = parseFloat(N[h ? "offsetWidth" : "offsetHeight"]), _.removeChild(N), h && "%" === s && !1 !== r.cacheWidths && ((o = _._gsCache = _._gsCache || {}).time = l, o.width = a / i * 100), 0 !== a || n || (a = K(t, e, i, s, !0));
    }
else
    o = H(t).lineHeight, t.style.lineHeight = i, a = parseFloat(H(t).lineHeight), t.style.lineHeight = o; return f && (a /= 100), c ? -a : a; }, J = j.calculateOffset = function (t, e, i) { if ("absolute" !== Q(t, "position", i))
    return 0; var s = "left" === e ? "Left" : "Top", r = Q(t, "margin" + s, i); return t["offset" + s] - (K(t, e, parseFloat(r), r.replace(T, "")) || 0); }, tt = function (t, e) { var i, s, r, n = {}; if (e = e || H(t, null))
    if (i = e.length)
        for (; --i > -1;)
            -1 !== (r = e[i]).indexOf("-transform") && Mt !== r || (n[r.replace(O, R)] = e.getPropertyValue(r));
    else
        for (i in e)
            -1 !== i.indexOf("Transform") && Ct !== i || (n[i] = e[i]);
else if (e = t.currentStyle || t.style)
    for (i in e)
        "string" == typeof i && void 0 === n[i] && (n[i.replace(O, R)] = e[i]); return V || (n.opacity = q(t)), s = Vt(t, e, !1), n.rotation = s.rotation, n.skewX = s.skewX, n.scaleX = s.scaleX, n.scaleY = s.scaleY, n.x = s.x, n.y = s.y, Lt && (n.z = s.z, n.rotationX = s.rotationX, n.rotationY = s.rotationY, n.scaleZ = s.scaleZ), n.filters && delete n.filters, n; }, et = function (t, e, i, s, r) { var n, a, o, l = {}, h = t.style; for (a in i)
    "cssText" !== a && "length" !== a && isNaN(a) && (e[a] !== (n = i[a]) || r && r[a]) && -1 === a.indexOf("Origin") && ("number" != typeof n && "string" != typeof n || (l[a] = "auto" !== n || "left" !== a && "top" !== a ? "" !== n && "auto" !== n && "none" !== n || "string" != typeof e[a] || "" === e[a].replace(v, "") ? n : 0 : J(t, a), void 0 !== h[a] && (o = new dt(h, a, h[a], o)))); if (s)
    for (a in s)
        "className" !== a && (l[a] = s[a]); return { difs: l, firstMPT: o }; }, it = { width: ["Left", "Right"], height: ["Top", "Bottom"] }, st = ["marginLeft", "marginRight", "marginTop", "marginBottom"], rt = function (t, e, i) { if ("svg" === (t.nodeName + "").toLowerCase())
    return (i || H(t))[e] || 0; if (t.getCTM && Yt(t))
    return t.getBBox()[e] || 0; var s = parseFloat("width" === e ? t.offsetWidth : t.offsetHeight), r = it[e], n = r.length; for (i = i || H(t, null); --n > -1;)
    s -= parseFloat(Q(t, "padding" + r[n], i, !0)) || 0, s -= parseFloat(Q(t, "border" + r[n] + "Width", i, !0)) || 0; return s; }, nt = function (t, e) { if ("contain" === t || "auto" === t || "auto auto" === t)
    return t + " "; null != t && "" !== t || (t = "0 0"); var i, s = t.split(" "), r = -1 !== t.indexOf("left") ? "0%" : -1 !== t.indexOf("right") ? "100%" : s[0], n = -1 !== t.indexOf("top") ? "0%" : -1 !== t.indexOf("bottom") ? "100%" : s[1]; if (s.length > 3 && !e) {
    for (s = t.split(", ").join(",").split(","), t = [], i = 0; i < s.length; i++)
        t.push(nt(s[i]));
    return t.join(",");
} return null == n ? n = "center" === r ? "50%" : "0" : "center" === n && (n = "50%"), ("center" === r || isNaN(parseFloat(r)) && -1 === (r + "").indexOf("=")) && (r = "50%"), t = r + " " + n + (s.length > 2 ? " " + s[2] : ""), e && (e.oxp = -1 !== r.indexOf("%"), e.oyp = -1 !== n.indexOf("%"), e.oxr = "=" === r.charAt(1), e.oyr = "=" === n.charAt(1), e.ox = parseFloat(r.replace(v, "")), e.oy = parseFloat(n.replace(v, "")), e.v = t), e || t; }, at = function (t, e) { return "function" == typeof t && (t = t(m, p)), "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) : parseFloat(t) - parseFloat(e) || 0; }, ot = function (t, e) { "function" == typeof t && (t = t(m, p)); var i = "string" == typeof t && "=" === t.charAt(1); return "string" == typeof t && "v" === t.charAt(t.length - 2) && (t = (i ? t.substr(0, 2) : 0) + window["inner" + ("vh" === t.substr(-2) ? "Height" : "Width")] * (parseFloat(i ? t.substr(2) : t) / 100)), null == t ? e : i ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) + e : parseFloat(t) || 0; }, lt = function (t, e, i, s) { var r, n, a, o, l; return "function" == typeof t && (t = t(m, p)), null == t ? o = e : "number" == typeof t ? o = t : (r = 360, n = t.split("_"), a = ((l = "=" === t.charAt(1)) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(n[0].substr(2)) : parseFloat(n[0])) * (-1 === t.indexOf("rad") ? 1 : z) - (l ? 0 : e), n.length && (s && (s[i] = e + a), -1 !== t.indexOf("short") && (a %= r) != a % (r / 2) && (a = a < 0 ? a + r : a - r), -1 !== t.indexOf("_cw") && a < 0 ? a = (a + 9999999999 * r) % r - (a / r | 0) * r : -1 !== t.indexOf("ccw") && a > 0 && (a = (a - 9999999999 * r) % r - (a / r | 0) * r)), o = e + a), o < 1e-6 && o > -1e-6 && (o = 0), o; }, ht = { aqua: [0, 255, 255], lime: [0, 255, 0], silver: [192, 192, 192], black: [0, 0, 0], maroon: [128, 0, 0], teal: [0, 128, 128], blue: [0, 0, 255], navy: [0, 0, 128], white: [255, 255, 255], fuchsia: [255, 0, 255], olive: [128, 128, 0], yellow: [255, 255, 0], orange: [255, 165, 0], gray: [128, 128, 128], purple: [128, 0, 128], green: [0, 128, 0], red: [255, 0, 0], pink: [255, 192, 203], cyan: [0, 255, 255], transparent: [255, 255, 255, 0] }, _t = function (t, e, i) { return 255 * (6 * (t = t < 0 ? t + 1 : t > 1 ? t - 1 : t) < 1 ? e + (i - e) * t * 6 : t < .5 ? i : 3 * t < 2 ? e + (i - e) * (2 / 3 - t) * 6 : e) + .5 | 0; }, ut = r.parseColor = function (t, e) { var i, s, r, n, a, o, l, h, _, u, c; if (t)
    if ("number" == typeof t)
        i = [t >> 16, t >> 8 & 255, 255 & t];
    else {
        if ("," === t.charAt(t.length - 1) && (t = t.substr(0, t.length - 1)), ht[t])
            i = ht[t];
        else if ("#" === t.charAt(0))
            4 === t.length && (t = "#" + (s = t.charAt(1)) + s + (r = t.charAt(2)) + r + (n = t.charAt(3)) + n), i = [(t = parseInt(t.substr(1), 16)) >> 16, t >> 8 & 255, 255 & t];
        else if ("hsl" === t.substr(0, 3))
            if (i = c = t.match(d), e) {
                if (-1 !== t.indexOf("="))
                    return t.match(g);
            }
            else
                a = Number(i[0]) % 360 / 360, o = Number(i[1]) / 100, s = 2 * (l = Number(i[2]) / 100) - (r = l <= .5 ? l * (o + 1) : l + o - l * o), i.length > 3 && (i[3] = Number(i[3])), i[0] = _t(a + 1 / 3, s, r), i[1] = _t(a, s, r), i[2] = _t(a - 1 / 3, s, r);
        else
            i = t.match(d) || ht.transparent;
        i[0] = Number(i[0]), i[1] = Number(i[1]), i[2] = Number(i[2]), i.length > 3 && (i[3] = Number(i[3]));
    }
else
    i = ht.black; return e && !c && (s = i[0] / 255, r = i[1] / 255, n = i[2] / 255, l = ((h = Math.max(s, r, n)) + (_ = Math.min(s, r, n))) / 2, h === _ ? a = o = 0 : (u = h - _, o = l > .5 ? u / (2 - h - _) : u / (h + _), a = h === s ? (r - n) / u + (r < n ? 6 : 0) : h === r ? (n - s) / u + 2 : (s - r) / u + 4, a *= 60), i[0] = a + .5 | 0, i[1] = 100 * o + .5 | 0, i[2] = 100 * l + .5 | 0), i; }, ct = function (t, e) { var i, s, r, n = t.match(ft) || [], a = 0, o = ""; if (!n.length)
    return t; for (i = 0; i < n.length; i++)
    s = n[i], a += (r = t.substr(a, t.indexOf(s, a) - a)).length + s.length, 3 === (s = ut(s, e)).length && s.push(1), o += r + (e ? "hsla(" + s[0] + "," + s[1] + "%," + s[2] + "%," + s[3] : "rgba(" + s.join(",")) + ")"; return o + t.substr(a); }, ft = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b"; for (o in ht)
    ft += "|" + o + "\\b"; ft = new RegExp(ft + ")", "gi"), r.colorStringFilter = function (t) { var e, i = t[0] + " " + t[1]; ft.test(i) && (e = -1 !== i.indexOf("hsl(") || -1 !== i.indexOf("hsla("), t[0] = ct(t[0], e), t[1] = ct(t[1], e)), ft.lastIndex = 0; }, TweenLite.defaultStringFilter || (TweenLite.defaultStringFilter = r.colorStringFilter); var pt = function (t, e, i, s) { if (null == t)
    return function (t) { return t; }; var r, n = e ? (t.match(ft) || [""])[0] : "", a = t.split(n).join("").match(y) || [], o = t.substr(0, t.indexOf(a[0])), l = ")" === t.charAt(t.length - 1) ? ")" : "", h = -1 !== t.indexOf(" ") ? " " : ",", _ = a.length, u = _ > 0 ? a[0].replace(d, "") : ""; return _ ? r = e ? function (t) { var e, c, f, p; if ("number" == typeof t)
    t += u;
else if (s && D.test(t)) {
    for (p = t.replace(D, "|").split("|"), f = 0; f < p.length; f++)
        p[f] = r(p[f]);
    return p.join(",");
} if (e = (t.match(ft) || [n])[0], f = (c = t.split(e).join("").match(y) || []).length, _ > f--)
    for (; ++f < _;)
        c[f] = i ? c[(f - 1) / 2 | 0] : a[f]; return o + c.join(h) + h + e + l + (-1 !== t.indexOf("inset") ? " inset" : ""); } : function (t) { var e, n, c; if ("number" == typeof t)
    t += u;
else if (s && D.test(t)) {
    for (n = t.replace(D, "|").split("|"), c = 0; c < n.length; c++)
        n[c] = r(n[c]);
    return n.join(",");
} if (c = (e = t.match(y) || []).length, _ > c--)
    for (; ++c < _;)
        e[c] = i ? e[(c - 1) / 2 | 0] : a[c]; return o + e.join(h) + l; } : function (t) { return t; }; }, mt = function (t) { return t = t.split(","), function (e, i, s, r, n, a, o) { var l, h = (i + "").split(" "); for (o = {}, l = 0; l < 4; l++)
    o[t[l]] = h[l] = h[l] || h[(l - 1) / 2 >> 0]; return r.parse(e, o, n, a); }; }, dt = (j._setPluginRatio = function (t) { this.plugin.setRatio(t); for (var e, i, s, r, n, a = this.data, o = a.proxy, l = a.firstMPT; l;)
    e = o[l.v], l.r ? e = l.r(e) : e < 1e-6 && e > -1e-6 && (e = 0), l.t[l.p] = e, l = l._next; if (a.autoRotate && (a.autoRotate.rotation = a.mod ? a.mod.call(this._tween, o.rotation, this.t, this._tween) : o.rotation), 1 === t || 0 === t)
    for (l = a.firstMPT, n = 1 === t ? "e" : "b"; l;) {
        if ((i = l.t).type) {
            if (1 === i.type) {
                for (r = i.xs0 + i.s + i.xs1, s = 1; s < i.l; s++)
                    r += i["xn" + s] + i["xs" + (s + 1)];
                i[n] = r;
            }
        }
        else
            i[n] = i.s + i.xs0;
        l = l._next;
    } }, function (t, e, i, s, r) { this.t = t, this.p = e, this.v = i, this.r = r, s && (s._prev = this, this._next = s); }), gt = (j._parseToProxy = function (t, e, i, s, r, n) { var a, o, l, h, _, u = s, c = {}, f = {}, p = i._transform, m = F; for (i._transform = null, F = e, s = _ = i.parse(t, e, s, r), F = m, n && (i._transform = p, u && (u._prev = null, u._prev && (u._prev._next = null))); s && s !== u;) {
    if (s.type <= 1 && (f[o = s.p] = s.s + s.c, c[o] = s.s, n || (h = new dt(s, "s", o, h, s.r), s.c = 0), 1 === s.type))
        for (a = s.l; --a > 0;)
            l = "xn" + a, f[o = s.p + "_" + l] = s.data[l], c[o] = s[l], n || (h = new dt(s, l, o, h, s.rxp[l]));
    s = s._next;
} return { proxy: c, end: f, firstMPT: h, pt: _ }; }, j.CSSPropTween = function (e, i, r, n, a, o, l, h, _, u, c) { this.t = e, this.p = i, this.s = r, this.c = n, this.n = l || i, e instanceof gt || s.push(this.n), this.r = h ? "function" == typeof h ? h : Math.round : h, this.type = o || 0, _ && (this.pr = _, t = !0), this.b = void 0 === u ? r : u, this.e = void 0 === c ? r + n : c, a && (this._next = a, a._prev = this); }), yt = function (t, e, i, s, r, n) { var a = new gt(t, e, i, s - i, r, -1, n); return a.b = i, a.e = a.xs0 = s, a; }, vt = r.parseComplex = function (t, e, i, s, n, a, o, h, _, u) { i = i || a || "", "function" == typeof s && (s = s(m, p)), o = new gt(t, e, 0, 0, o, u ? 2 : 1, null, !1, h, i, s), s += "", n && ft.test(s + i) && (s = [i, s], r.colorStringFilter(s), i = s[0], s = s[1]); var c, f, y, v, T, x, w, b, P, S, O, k, R, A = i.split(", ").join(",").split(" "), C = s.split(", ").join(",").split(" "), M = A.length, L = !1 !== l; for (-1 === s.indexOf(",") && -1 === i.indexOf(",") || (-1 !== (s + i).indexOf("rgb") || -1 !== (s + i).indexOf("hsl") ? (A = A.join(" ").replace(D, ", ").split(" "), C = C.join(" ").replace(D, ", ").split(" ")) : (A = A.join(" ").split(",").join(", ").split(" "), C = C.join(" ").split(",").join(", ").split(" ")), M = A.length), M !== C.length && (M = (A = (a || "").split(" ")).length), o.plugin = _, o.setRatio = u, ft.lastIndex = 0, c = 0; c < M; c++)
    if (v = A[c], T = C[c] + "", (b = parseFloat(v)) || 0 === b)
        o.appendXtra("", b, at(T, b), T.replace(g, ""), !(!L || -1 === T.indexOf("px")) && Math.round, !0);
    else if (n && ft.test(v))
        k = ")" + ((k = T.indexOf(")") + 1) ? T.substr(k) : ""), R = -1 !== T.indexOf("hsl") && V, S = T, v = ut(v, R), T = ut(T, R), (P = v.length + T.length > 6) && !V && 0 === T[3] ? (o["xs" + o.l] += o.l ? " transparent" : "transparent", o.e = o.e.split(C[c]).join("transparent")) : (V || (P = !1), R ? o.appendXtra(S.substr(0, S.indexOf("hsl")) + (P ? "hsla(" : "hsl("), v[0], at(T[0], v[0]), ",", !1, !0).appendXtra("", v[1], at(T[1], v[1]), "%,", !1).appendXtra("", v[2], at(T[2], v[2]), P ? "%," : "%" + k, !1) : o.appendXtra(S.substr(0, S.indexOf("rgb")) + (P ? "rgba(" : "rgb("), v[0], T[0] - v[0], ",", Math.round, !0).appendXtra("", v[1], T[1] - v[1], ",", Math.round).appendXtra("", v[2], T[2] - v[2], P ? "," : k, Math.round), P && (v = v.length < 4 ? 1 : v[3], o.appendXtra("", v, (T.length < 4 ? 1 : T[3]) - v, k, !1))), ft.lastIndex = 0;
    else if (x = v.match(d)) {
        if (!(w = T.match(g)) || w.length !== x.length)
            return o;
        for (y = 0, f = 0; f < x.length; f++)
            O = x[f], S = v.indexOf(O, y), o.appendXtra(v.substr(y, S - y), Number(O), at(w[f], O), "", !(!L || "px" !== v.substr(S + O.length, 2)) && Math.round, 0 === f), y = S + O.length;
        o["xs" + o.l] += v.substr(y);
    }
    else
        o["xs" + o.l] += o.l || o["xs" + o.l] ? " " + T : T; if (-1 !== s.indexOf("=") && o.data) {
    for (k = o.xs0 + o.data.s, c = 1; c < o.l; c++)
        k += o["xs" + c] + o.data["xn" + c];
    o.e = k + o["xs" + c];
} return o.l || (o.type = -1, o.xs0 = o.e), o.xfirst || o; }, Tt = 9; for ((o = gt.prototype).l = o.pr = 0; --Tt > 0;)
    o["xn" + Tt] = 0, o["xs" + Tt] = ""; o.xs0 = "", o._next = o._prev = o.xfirst = o.data = o.plugin = o.setRatio = o.rxp = null, o.appendXtra = function (t, e, i, s, r, n) { var a = this, o = a.l; return a["xs" + o] += n && (o || a["xs" + o]) ? " " + t : t || "", i || 0 === o || a.plugin ? (a.l++, a.type = a.setRatio ? 2 : 1, a["xs" + a.l] = s || "", o > 0 ? (a.data["xn" + o] = e + i, a.rxp["xn" + o] = r, a["xn" + o] = e, a.plugin || (a.xfirst = new gt(a, "xn" + o, e, i, a.xfirst || a, 0, a.n, r, a.pr), a.xfirst.xs0 = 0), a) : (a.data = { s: e + i }, a.rxp = {}, a.s = e, a.c = i, a.r = r, a)) : (a["xs" + o] += e + (s || ""), a); }; var xt = function (t, e) { e = e || {}, this.p = e.prefix && Z(t) || t, a[t] = a[this.p] = this, this.format = e.formatter || pt(e.defaultValue, e.color, e.collapsible, e.multi), e.parser && (this.parse = e.parser), this.clrs = e.color, this.multi = e.multi, this.keyword = e.keyword, this.dflt = e.defaultValue, this.pr = e.priority || 0; }, wt = j._registerComplexSpecialProp = function (t, e, i) { "object" != typeof e && (e = { parser: i }); var s, r = t.split(","), n = e.defaultValue; for (i = i || [n], s = 0; s < r.length; s++)
    e.prefix = 0 === s && e.prefix, e.defaultValue = i[s] || n, new xt(r[s], e); }, bt = j._registerPluginProp = function (t) { if (!a[t]) {
    var e = t.charAt(0).toUpperCase() + t.substr(1) + "Plugin";
    wt(t, { parser: function (t, i, s, r, o, l, h) { var _ = n.com.greensock.plugins[e]; return _ ? (_._cssRegister(), a[s].parse(t, i, s, r, o, l, h)) : (G("Error: " + e + " js file not loaded."), o); } });
} }; (o = xt.prototype).parseComplex = function (t, e, i, s, r, n) { var a, o, l, h, _, u, c = this.keyword; if (this.multi && (D.test(i) || D.test(e) ? (o = e.replace(D, "|").split("|"), l = i.replace(D, "|").split("|")) : c && (o = [e], l = [i])), l) {
    for (h = l.length > o.length ? l.length : o.length, a = 0; a < h; a++)
        e = o[a] = o[a] || this.dflt, i = l[a] = l[a] || this.dflt, c && (_ = e.indexOf(c)) !== (u = i.indexOf(c)) && (-1 === u ? o[a] = o[a].split(c).join("") : -1 === _ && (o[a] += " " + c));
    e = o.join(", "), i = l.join(", ");
} return vt(t, this.p, e, i, this.clrs, this.dflt, s, this.pr, r, n); }, o.parse = function (t, e, s, r, n, a, o) { return this.parseComplex(t.style, this.format(Q(t, this.p, i, !1, this.dflt)), this.format(e), n, a); }, r.registerSpecialProp = function (t, e, i) { wt(t, { parser: function (t, s, r, n, a, o, l) { var h = new gt(t, r, 0, 0, a, 2, r, !1, i); return h.plugin = o, h.setRatio = e(t, s, n._tween, r), h; }, priority: i }); }, r.useSVGTransformAttr = !0; var Pt, St, Ot, kt, Rt, At = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","), Ct = Z("transform"), Mt = W + "transform", Dt = Z("transformOrigin"), Lt = null !== Z("perspective"), Et = j.Transform = function () { this.perspective = parseFloat(r.defaultTransformPerspective) || 0, this.force3D = !(!1 === r.defaultForce3D || !Lt) && (r.defaultForce3D || "auto"); }, zt = _gsScope.SVGElement, Ft = function (t, e, i) { var s, r = X.createElementNS("http://www.w3.org/2000/svg", t), n = /([a-z])([A-Z])/g; for (s in i)
    r.setAttributeNS(null, s.replace(n, "$1-$2").toLowerCase(), i[s]); return e.appendChild(r), r; }, It = X.documentElement || {}, Xt = (Rt = f || /Android/i.test(U) && !_gsScope.chrome, X.createElementNS && !Rt && (kt = (Ot = Ft("rect", St = Ft("svg", It), { width: 100, height: 50, x: 100 })).getBoundingClientRect().width, Ot.style[Dt] = "50% 50%", Ot.style[Ct] = "scaleX(0.5)", Rt = kt === Ot.getBoundingClientRect().width && !(u && Lt), It.removeChild(St)), Rt), Bt = function (t, e, i, s, n, a) { var o, l, h, _, u, c, f, p, m, d, g, y, v, T, x = t._gsTransform, w = Ut(t, !0); x && (v = x.xOrigin, T = x.yOrigin), (!s || (o = s.split(" ")).length < 2) && (0 === (f = t.getBBox()).x && 0 === f.y && f.width + f.height === 0 && (f = { x: parseFloat(t.hasAttribute("x") ? t.getAttribute("x") : t.hasAttribute("cx") ? t.getAttribute("cx") : 0) || 0, y: parseFloat(t.hasAttribute("y") ? t.getAttribute("y") : t.hasAttribute("cy") ? t.getAttribute("cy") : 0) || 0, width: 0, height: 0 }), o = [(-1 !== (e = nt(e).split(" "))[0].indexOf("%") ? parseFloat(e[0]) / 100 * f.width : parseFloat(e[0])) + f.x, (-1 !== e[1].indexOf("%") ? parseFloat(e[1]) / 100 * f.height : parseFloat(e[1])) + f.y]), i.xOrigin = _ = parseFloat(o[0]), i.yOrigin = u = parseFloat(o[1]), s && w !== jt && (c = w[0], f = w[1], p = w[2], m = w[3], d = w[4], g = w[5], (y = c * m - f * p) && (l = _ * (m / y) + u * (-p / y) + (p * g - m * d) / y, h = _ * (-f / y) + u * (c / y) - (c * g - f * d) / y, _ = i.xOrigin = o[0] = l, u = i.yOrigin = o[1] = h)), x && (a && (i.xOffset = x.xOffset, i.yOffset = x.yOffset, x = i), n || !1 !== n && !1 !== r.defaultSmoothOrigin ? (l = _ - v, h = u - T, x.xOffset += l * w[0] + h * w[2] - l, x.yOffset += l * w[1] + h * w[3] - h) : x.xOffset = x.yOffset = 0), a || t.setAttribute("data-svg-origin", o.join(" ")); }, Nt = function (t) { var e, i = B("svg", this.ownerSVGElement && this.ownerSVGElement.getAttribute("xmlns") || "http://www.w3.org/2000/svg"), s = this.parentNode, r = this.nextSibling, n = this.style.cssText; if (It.appendChild(i), i.appendChild(this), this.style.display = "block", t)
    try {
        e = this.getBBox(), this._originalGetBBox = this.getBBox, this.getBBox = Nt;
    }
    catch (t) { }
else
    this._originalGetBBox && (e = this._originalGetBBox()); return r ? s.insertBefore(this, r) : s.appendChild(this), It.removeChild(i), this.style.cssText = n, e; }, Yt = function (t) { return !(!zt || !t.getCTM || t.parentNode && !t.ownerSVGElement || !function (t) { try {
    return t.getBBox();
}
catch (e) {
    return Nt.call(t, !0);
} }(t)); }, jt = [1, 0, 0, 1, 0, 0], Ut = function (t, e) { var i, s, r, n, a, o, l = t._gsTransform || new Et, h = t.style; if (Ct ? s = Q(t, Mt, null, !0) : t.currentStyle && (s = (s = t.currentStyle.filter.match(C)) && 4 === s.length ? [s[0].substr(4), Number(s[2].substr(4)), Number(s[1].substr(4)), s[3].substr(4), l.x || 0, l.y || 0].join(",") : ""), i = !s || "none" === s || "matrix(1, 0, 0, 1, 0, 0)" === s, !Ct || !(o = !H(t) || "none" === H(t).display) && t.parentNode || (o && (n = h.display, h.display = "block"), t.parentNode || (a = 1, It.appendChild(t)), i = !(s = Q(t, Mt, null, !0)) || "none" === s || "matrix(1, 0, 0, 1, 0, 0)" === s, n ? h.display = n : o && $t(h, "display"), a && It.removeChild(t)), (l.svg || t.getCTM && Yt(t)) && (i && -1 !== (h[Ct] + "").indexOf("matrix") && (s = h[Ct], i = 0), r = t.getAttribute("transform"), i && r && (s = "matrix(" + (r = t.transform.baseVal.consolidate().matrix).a + "," + r.b + "," + r.c + "," + r.d + "," + r.e + "," + r.f + ")", i = 0)), i)
    return jt; for (r = (s || "").match(d) || [], Tt = r.length; --Tt > -1;)
    n = Number(r[Tt]), r[Tt] = (a = n - (n |= 0)) ? (1e5 * a + (a < 0 ? -.5 : .5) | 0) / 1e5 + n : n; return e && r.length > 6 ? [r[0], r[1], r[4], r[5], r[12], r[13]] : r; }, Vt = j.getTransform = function (t, e, i, s) { if (t._gsTransform && i && !s)
    return t._gsTransform; var n, a, o, l, h, _, u = i && t._gsTransform || new Et, c = u.scaleX < 0, f = Lt && (parseFloat(Q(t, Dt, e, !1, "0 0 0").split(" ")[2]) || u.zOrigin) || 0, p = parseFloat(r.defaultTransformPerspective) || 0; if (u.svg = !(!t.getCTM || !Yt(t)), u.svg && (Bt(t, Q(t, Dt, e, !1, "50% 50%") + "", u, t.getAttribute("data-svg-origin")), Pt = r.useSVGTransformAttr || Xt), (n = Ut(t)) !== jt) {
    if (16 === n.length) {
        var m, d, g, y, v, T = n[0], x = n[1], w = n[2], b = n[3], P = n[4], S = n[5], O = n[6], k = n[7], R = n[8], A = n[9], C = n[10], M = n[12], D = n[13], L = n[14], E = n[11], F = Math.atan2(O, C);
        u.zOrigin && (M = R * (L = -u.zOrigin) - n[12], D = A * L - n[13], L = C * L + u.zOrigin - n[14]), u.rotationX = F * z, F && (m = P * (y = Math.cos(-F)) + R * (v = Math.sin(-F)), d = S * y + A * v, g = O * y + C * v, R = P * -v + R * y, A = S * -v + A * y, C = O * -v + C * y, E = k * -v + E * y, P = m, S = d, O = g), F = Math.atan2(-w, C), u.rotationY = F * z, F && (d = x * (y = Math.cos(-F)) - A * (v = Math.sin(-F)), g = w * y - C * v, A = x * v + A * y, C = w * v + C * y, E = b * v + E * y, T = m = T * y - R * v, x = d, w = g), F = Math.atan2(x, T), u.rotation = F * z, F && (m = T * (y = Math.cos(F)) + x * (v = Math.sin(F)), d = P * y + S * v, g = R * y + A * v, x = x * y - T * v, S = S * y - P * v, A = A * y - R * v, T = m, P = d, R = g), u.rotationX && Math.abs(u.rotationX) + Math.abs(u.rotation) > 359.9 && (u.rotationX = u.rotation = 0, u.rotationY = 180 - u.rotationY), F = Math.atan2(P, S), u.scaleX = (1e5 * Math.sqrt(T * T + x * x + w * w) + .5 | 0) / 1e5, u.scaleY = (1e5 * Math.sqrt(S * S + O * O) + .5 | 0) / 1e5, u.scaleZ = (1e5 * Math.sqrt(R * R + A * A + C * C) + .5 | 0) / 1e5, T /= u.scaleX, P /= u.scaleY, x /= u.scaleX, S /= u.scaleY, Math.abs(F) > 2e-5 ? (u.skewX = F * z, P = 0, "simple" !== u.skewType && (u.scaleY *= 1 / Math.cos(F))) : u.skewX = 0, u.perspective = E ? 1 / (E < 0 ? -E : E) : 0, u.x = M, u.y = D, u.z = L, u.svg && (u.x -= u.xOrigin - (u.xOrigin * T - u.yOrigin * P), u.y -= u.yOrigin - (u.yOrigin * x - u.xOrigin * S));
    }
    else if (!Lt || s || !n.length || u.x !== n[4] || u.y !== n[5] || !u.rotationX && !u.rotationY) {
        var I = n.length >= 6, X = I ? n[0] : 1, B = n[1] || 0, N = n[2] || 0, Y = I ? n[3] : 1;
        u.x = n[4] || 0, u.y = n[5] || 0, o = Math.sqrt(X * X + B * B), l = Math.sqrt(Y * Y + N * N), h = X || B ? Math.atan2(B, X) * z : u.rotation || 0, _ = N || Y ? Math.atan2(N, Y) * z + h : u.skewX || 0, u.scaleX = o, u.scaleY = l, u.rotation = h, u.skewX = _, Lt && (u.rotationX = u.rotationY = u.z = 0, u.perspective = p, u.scaleZ = 1), u.svg && (u.x -= u.xOrigin - (u.xOrigin * X + u.yOrigin * N), u.y -= u.yOrigin - (u.xOrigin * B + u.yOrigin * Y));
    }
    for (a in Math.abs(u.skewX) > 90 && Math.abs(u.skewX) < 270 && (c ? (u.scaleX *= -1, u.skewX += u.rotation <= 0 ? 180 : -180, u.rotation += u.rotation <= 0 ? 180 : -180) : (u.scaleY *= -1, u.skewX += u.skewX <= 0 ? 180 : -180)), u.zOrigin = f, u)
        u[a] < 2e-5 && u[a] > -2e-5 && (u[a] = 0);
} return i && (t._gsTransform = u, u.svg && (Pt && t.style[Ct] ? TweenLite.delayedCall(.001, function () { $t(t.style, Ct); }) : !Pt && t.getAttribute("transform") && TweenLite.delayedCall(.001, function () { t.removeAttribute("transform"); }))), u; }, qt = function (t) { var e, i, s = this.data, r = -s.rotation * E, n = r + s.skewX * E, a = (Math.cos(r) * s.scaleX * 1e5 | 0) / 1e5, o = (Math.sin(r) * s.scaleX * 1e5 | 0) / 1e5, l = (Math.sin(n) * -s.scaleY * 1e5 | 0) / 1e5, h = (Math.cos(n) * s.scaleY * 1e5 | 0) / 1e5, _ = this.t.style, u = this.t.currentStyle; if (u) {
    i = o, o = -l, l = -i, e = u.filter, _.filter = "";
    var c, p, m = this.t.offsetWidth, d = this.t.offsetHeight, g = "absolute" !== u.position, y = "progid:DXImageTransform.Microsoft.Matrix(M11=" + a + ", M12=" + o + ", M21=" + l + ", M22=" + h, v = s.x + m * s.xPercent / 100, w = s.y + d * s.yPercent / 100;
    if (null != s.ox && (v += (c = (s.oxp ? m * s.ox * .01 : s.ox) - m / 2) - (c * a + (p = (s.oyp ? d * s.oy * .01 : s.oy) - d / 2) * o), w += p - (c * l + p * h)), y += g ? ", Dx=" + ((c = m / 2) - (c * a + (p = d / 2) * o) + v) + ", Dy=" + (p - (c * l + p * h) + w) + ")" : ", sizingMethod='auto expand')", -1 !== e.indexOf("DXImageTransform.Microsoft.Matrix(") ? _.filter = e.replace(M, y) : _.filter = y + " " + e, 0 !== t && 1 !== t || 1 === a && 0 === o && 0 === l && 1 === h && (g && -1 === y.indexOf("Dx=0, Dy=0") || x.test(e) && 100 !== parseFloat(RegExp.$1) || -1 === e.indexOf(e.indexOf("Alpha")) && _.removeAttribute("filter")), !g) {
        var b, P, S, O = f < 8 ? 1 : -1;
        for (c = s.ieOffsetX || 0, p = s.ieOffsetY || 0, s.ieOffsetX = Math.round((m - ((a < 0 ? -a : a) * m + (o < 0 ? -o : o) * d)) / 2 + v), s.ieOffsetY = Math.round((d - ((h < 0 ? -h : h) * d + (l < 0 ? -l : l) * m)) / 2 + w), Tt = 0; Tt < 4; Tt++)
            S = (i = -1 !== (b = u[P = st[Tt]]).indexOf("px") ? parseFloat(b) : K(this.t, P, parseFloat(b), b.replace(T, "")) || 0) !== s[P] ? Tt < 2 ? -s.ieOffsetX : -s.ieOffsetY : Tt < 2 ? c - s.ieOffsetX : p - s.ieOffsetY, _[P] = (s[P] = Math.round(i - S * (0 === Tt || 2 === Tt ? 1 : O))) + "px";
    }
} }, Gt = j.set3DTransformRatio = j.setTransformRatio = function (t) { var e, i, s, r, n, a, o, l, h, _, c, f, p, m, d, g, y, v, T, x, w, b = this.data, P = this.t.style, S = b.rotation, O = b.rotationX, k = b.rotationY, R = b.scaleX, A = b.scaleY, C = b.scaleZ, M = b.x, D = b.y, L = b.z, z = b.svg, F = b.perspective, I = b.force3D, X = b.skewY, B = b.skewX; if (X && (B += X, S += X), !((1 !== t && 0 !== t || "auto" !== I || this.tween._totalTime !== this.tween._totalDuration && this.tween._totalTime) && I || L || F || k || O || 1 !== C) || Pt && z || !Lt)
    S || B || z ? (S *= E, x = B * E, w = 1e5, i = Math.cos(S) * R, n = Math.sin(S) * R, s = Math.sin(S - x) * -A, a = Math.cos(S - x) * A, x && "simple" === b.skewType && (e = Math.tan(x - X * E), s *= e = Math.sqrt(1 + e * e), a *= e, X && (e = Math.tan(X * E), i *= e = Math.sqrt(1 + e * e), n *= e)), z && (M += b.xOrigin - (b.xOrigin * i + b.yOrigin * s) + b.xOffset, D += b.yOrigin - (b.xOrigin * n + b.yOrigin * a) + b.yOffset, Pt && (b.xPercent || b.yPercent) && (d = this.t.getBBox(), M += .01 * b.xPercent * d.width, D += .01 * b.yPercent * d.height), M < (d = 1e-6) && M > -d && (M = 0), D < d && D > -d && (D = 0)), T = (i * w | 0) / w + "," + (n * w | 0) / w + "," + (s * w | 0) / w + "," + (a * w | 0) / w + "," + M + "," + D + ")", z && Pt ? this.t.setAttribute("transform", "matrix(" + T) : P[Ct] = (b.xPercent || b.yPercent ? "translate(" + b.xPercent + "%," + b.yPercent + "%) matrix(" : "matrix(") + T) : P[Ct] = (b.xPercent || b.yPercent ? "translate(" + b.xPercent + "%," + b.yPercent + "%) matrix(" : "matrix(") + R + ",0,0," + A + "," + M + "," + D + ")";
else {
    if (u && (R < (d = 1e-4) && R > -d && (R = C = 2e-5), A < d && A > -d && (A = C = 2e-5), !F || b.z || b.rotationX || b.rotationY || (F = 0)), S || B)
        S *= E, g = i = Math.cos(S), y = n = Math.sin(S), B && (S -= B * E, g = Math.cos(S), y = Math.sin(S), "simple" === b.skewType && (e = Math.tan((B - X) * E), g *= e = Math.sqrt(1 + e * e), y *= e, b.skewY && (e = Math.tan(X * E), i *= e = Math.sqrt(1 + e * e), n *= e))), s = -y, a = g;
    else {
        if (!(k || O || 1 !== C || F || z))
            return void (P[Ct] = (b.xPercent || b.yPercent ? "translate(" + b.xPercent + "%," + b.yPercent + "%) translate3d(" : "translate3d(") + M + "px," + D + "px," + L + "px)" + (1 !== R || 1 !== A ? " scale(" + R + "," + A + ")" : ""));
        i = a = 1, s = n = 0;
    }
    _ = 1, r = o = l = h = c = f = 0, p = F ? -1 / F : 0, m = b.zOrigin, d = 1e-6, (S = k * E) && (g = Math.cos(S), l = -(y = Math.sin(S)), c = p * -y, r = i * y, o = n * y, _ = g, p *= g, i *= g, n *= g), (S = O * E) && (e = s * (g = Math.cos(S)) + r * (y = Math.sin(S)), v = a * g + o * y, h = _ * y, f = p * y, r = s * -y + r * g, o = a * -y + o * g, _ *= g, p *= g, s = e, a = v), 1 !== C && (r *= C, o *= C, _ *= C, p *= C), 1 !== A && (s *= A, a *= A, h *= A, f *= A), 1 !== R && (i *= R, n *= R, l *= R, c *= R), (m || z) && (m && (M += r * -m, D += o * -m, L += _ * -m + m), z && (M += b.xOrigin - (b.xOrigin * i + b.yOrigin * s) + b.xOffset, D += b.yOrigin - (b.xOrigin * n + b.yOrigin * a) + b.yOffset), M < d && M > -d && (M = "0"), D < d && D > -d && (D = "0"), L < d && L > -d && (L = 0)), T = b.xPercent || b.yPercent ? "translate(" + b.xPercent + "%," + b.yPercent + "%) matrix3d(" : "matrix3d(", T += (i < d && i > -d ? "0" : i) + "," + (n < d && n > -d ? "0" : n) + "," + (l < d && l > -d ? "0" : l), T += "," + (c < d && c > -d ? "0" : c) + "," + (s < d && s > -d ? "0" : s) + "," + (a < d && a > -d ? "0" : a), O || k || 1 !== C ? (T += "," + (h < d && h > -d ? "0" : h) + "," + (f < d && f > -d ? "0" : f) + "," + (r < d && r > -d ? "0" : r), T += "," + (o < d && o > -d ? "0" : o) + "," + (_ < d && _ > -d ? "0" : _) + "," + (p < d && p > -d ? "0" : p) + ",") : T += ",0,0,0,0,1,0,", T += M + "," + D + "," + L + "," + (F ? 1 + -L / F : 1) + ")", P[Ct] = T;
} }; (o = Et.prototype).x = o.y = o.z = o.skewX = o.skewY = o.rotation = o.rotationX = o.rotationY = o.zOrigin = o.xPercent = o.yPercent = o.xOffset = o.yOffset = 0, o.scaleX = o.scaleY = o.scaleZ = 1, wt("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin", { parser: function (t, e, s, n, a, o, l) { if (n._lastParsedTransform === l)
        return a; n._lastParsedTransform = l; var h, _ = l.scale && "function" == typeof l.scale ? l.scale : 0; "function" == typeof l[s] && (h = l[s], l[s] = e), _ && (l.scale = _(m, t)); var u, c, f, d, g, y, v, T, x, w = t._gsTransform, b = t.style, P = At.length, S = l, O = {}, k = Vt(t, i, !0, S.parseTransform), R = S.transform && ("function" == typeof S.transform ? S.transform(m, p) : S.transform); if (k.skewType = S.skewType || k.skewType || r.defaultSkewType, n._transform = k, "rotationZ" in S && (S.rotation = S.rotationZ), R && "string" == typeof R && Ct)
        (c = N.style)[Ct] = R, c.display = "block", c.position = "absolute", -1 !== R.indexOf("%") && (c.width = Q(t, "width"), c.height = Q(t, "height")), X.body.appendChild(N), u = Vt(N, null, !1), "simple" === k.skewType && (u.scaleY *= Math.cos(u.skewX * E)), k.svg && (y = k.xOrigin, v = k.yOrigin, u.x -= k.xOffset, u.y -= k.yOffset, (S.transformOrigin || S.svgOrigin) && (R = {}, Bt(t, nt(S.transformOrigin), R, S.svgOrigin, S.smoothOrigin, !0), y = R.xOrigin, v = R.yOrigin, u.x -= R.xOffset - k.xOffset, u.y -= R.yOffset - k.yOffset), (y || v) && (T = Ut(N, !0), u.x -= y - (y * T[0] + v * T[2]), u.y -= v - (y * T[1] + v * T[3]))), X.body.removeChild(N), u.perspective || (u.perspective = k.perspective), null != S.xPercent && (u.xPercent = ot(S.xPercent, k.xPercent)), null != S.yPercent && (u.yPercent = ot(S.yPercent, k.yPercent));
    else if ("object" == typeof S) {
        if (u = { scaleX: ot(null != S.scaleX ? S.scaleX : S.scale, k.scaleX), scaleY: ot(null != S.scaleY ? S.scaleY : S.scale, k.scaleY), scaleZ: ot(S.scaleZ, k.scaleZ), x: ot(S.x, k.x), y: ot(S.y, k.y), z: ot(S.z, k.z), xPercent: ot(S.xPercent, k.xPercent), yPercent: ot(S.yPercent, k.yPercent), perspective: ot(S.transformPerspective, k.perspective) }, null != (g = S.directionalRotation))
            if ("object" == typeof g)
                for (c in g)
                    S[c] = g[c];
            else
                S.rotation = g;
        "string" == typeof S.x && -1 !== S.x.indexOf("%") && (u.x = 0, u.xPercent = ot(S.x, k.xPercent)), "string" == typeof S.y && -1 !== S.y.indexOf("%") && (u.y = 0, u.yPercent = ot(S.y, k.yPercent)), u.rotation = lt("rotation" in S ? S.rotation : "shortRotation" in S ? S.shortRotation + "_short" : k.rotation, k.rotation, "rotation", O), Lt && (u.rotationX = lt("rotationX" in S ? S.rotationX : "shortRotationX" in S ? S.shortRotationX + "_short" : k.rotationX || 0, k.rotationX, "rotationX", O), u.rotationY = lt("rotationY" in S ? S.rotationY : "shortRotationY" in S ? S.shortRotationY + "_short" : k.rotationY || 0, k.rotationY, "rotationY", O)), u.skewX = lt(S.skewX, k.skewX), u.skewY = lt(S.skewY, k.skewY);
    } for (Lt && null != S.force3D && (k.force3D = S.force3D, d = !0), (f = k.force3D || k.z || k.rotationX || k.rotationY || u.z || u.rotationX || u.rotationY || u.perspective) || null == S.scale || (u.scaleZ = 1); --P > -1;)
        ((R = u[x = At[P]] - k[x]) > 1e-6 || R < -1e-6 || null != S[x] || null != F[x]) && (d = !0, a = new gt(k, x, k[x], R, a), x in O && (a.e = O[x]), a.xs0 = 0, a.plugin = o, n._overwriteProps.push(a.n)); return R = S.transformOrigin, k.svg && (R || S.svgOrigin) && (y = k.xOffset, v = k.yOffset, Bt(t, nt(R), u, S.svgOrigin, S.smoothOrigin), a = yt(k, "xOrigin", (w ? k : u).xOrigin, u.xOrigin, a, "transformOrigin"), a = yt(k, "yOrigin", (w ? k : u).yOrigin, u.yOrigin, a, "transformOrigin"), y === k.xOffset && v === k.yOffset || (a = yt(k, "xOffset", w ? y : k.xOffset, k.xOffset, a, "transformOrigin"), a = yt(k, "yOffset", w ? v : k.yOffset, k.yOffset, a, "transformOrigin")), R = "0px 0px"), (R || Lt && f && k.zOrigin) && (Ct ? (d = !0, x = Dt, R = (R || Q(t, x, i, !1, "50% 50%")) + "", (a = new gt(b, x, 0, 0, a, -1, "transformOrigin")).b = b[x], a.plugin = o, Lt ? (c = k.zOrigin, R = R.split(" "), k.zOrigin = (R.length > 2 && (0 === c || "0px" !== R[2]) ? parseFloat(R[2]) : c) || 0, a.xs0 = a.e = R[0] + " " + (R[1] || "50%") + " 0px", (a = new gt(k, "zOrigin", 0, 0, a, -1, a.n)).b = c, a.xs0 = a.e = k.zOrigin) : a.xs0 = a.e = R) : nt(R + "", k)), d && (n._transformType = k.svg && Pt || !f && 3 !== this._transformType ? 2 : 3), h && (l[s] = h), _ && (l.scale = _), a; }, prefix: !0 }), wt("boxShadow", { defaultValue: "0px 0px 0px 0px #999", prefix: !0, color: !0, multi: !0, keyword: "inset" }), wt("borderRadius", { defaultValue: "0px", parser: function (t, s, r, n, a, o) { s = this.format(s); var l, h, _, u, c, f, p, m, d, g, y, v, T, x, w, b, P = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"], S = t.style; for (d = parseFloat(t.offsetWidth), g = parseFloat(t.offsetHeight), l = s.split(" "), h = 0; h < P.length; h++)
        this.p.indexOf("border") && (P[h] = Z(P[h])), -1 !== (c = u = Q(t, P[h], i, !1, "0px")).indexOf(" ") && (c = (u = c.split(" "))[0], u = u[1]), f = _ = l[h], p = parseFloat(c), v = c.substr((p + "").length), (T = "=" === f.charAt(1)) ? (m = parseInt(f.charAt(0) + "1", 10), f = f.substr(2), m *= parseFloat(f), y = f.substr((m + "").length - (m < 0 ? 1 : 0)) || "") : (m = parseFloat(f), y = f.substr((m + "").length)), "" === y && (y = e[r] || v), y !== v && (x = K(t, "borderLeft", p, v), w = K(t, "borderTop", p, v), "%" === y ? (c = x / d * 100 + "%", u = w / g * 100 + "%") : "em" === y ? (c = x / (b = K(t, "borderLeft", 1, "em")) + "em", u = w / b + "em") : (c = x + "px", u = w + "px"), T && (f = parseFloat(c) + m + y, _ = parseFloat(u) + m + y)), a = vt(S, P[h], c + " " + u, f + " " + _, !1, "0px", a); return a; }, prefix: !0, formatter: pt("0px 0px 0px 0px", !1, !0) }), wt("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius", { defaultValue: "0px", parser: function (t, e, s, r, n, a) { return vt(t.style, s, this.format(Q(t, s, i, !1, "0px 0px")), this.format(e), !1, "0px", n); }, prefix: !0, formatter: pt("0px 0px", !1, !0) }), wt("backgroundPosition", { defaultValue: "0 0", parser: function (t, e, s, r, n, a) { var o, l, h, _, u, c, p = "background-position", m = i || H(t, null), d = this.format((m ? f ? m.getPropertyValue(p + "-x") + " " + m.getPropertyValue(p + "-y") : m.getPropertyValue(p) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"), g = this.format(e); if (-1 !== d.indexOf("%") != (-1 !== g.indexOf("%")) && g.split(",").length < 2 && (c = Q(t, "backgroundImage").replace(k, "")) && "none" !== c) {
        for (o = d.split(" "), l = g.split(" "), Y.setAttribute("src", c), h = 2; --h > -1;)
            (_ = -1 !== (d = o[h]).indexOf("%")) != (-1 !== l[h].indexOf("%")) && (u = 0 === h ? t.offsetWidth - Y.width : t.offsetHeight - Y.height, o[h] = _ ? parseFloat(d) / 100 * u + "px" : parseFloat(d) / u * 100 + "%");
        d = o.join(" ");
    } return this.parseComplex(t.style, d, g, n, a); }, formatter: nt }), wt("backgroundSize", { defaultValue: "0 0", formatter: function (t) { return "co" === (t += "").substr(0, 2) ? t : nt(-1 === t.indexOf(" ") ? t + " " + t : t); } }), wt("perspective", { defaultValue: "0px", prefix: !0 }), wt("perspectiveOrigin", { defaultValue: "50% 50%", prefix: !0 }), wt("transformStyle", { prefix: !0 }), wt("backfaceVisibility", { prefix: !0 }), wt("userSelect", { prefix: !0 }), wt("margin", { parser: mt("marginTop,marginRight,marginBottom,marginLeft") }), wt("padding", { parser: mt("paddingTop,paddingRight,paddingBottom,paddingLeft") }), wt("clip", { defaultValue: "rect(0px,0px,0px,0px)", parser: function (t, e, s, r, n, a) { var o, l, h; return f < 9 ? (l = t.currentStyle, h = f < 8 ? " " : ",", o = "rect(" + l.clipTop + h + l.clipRight + h + l.clipBottom + h + l.clipLeft + ")", e = this.format(e).split(",").join(h)) : (o = this.format(Q(t, this.p, i, !1, this.dflt)), e = this.format(e)), this.parseComplex(t.style, o, e, n, a); } }), wt("textShadow", { defaultValue: "0px 0px 0px #999", color: !0, multi: !0 }), wt("autoRound,strictUnits", { parser: function (t, e, i, s, r) { return r; } }), wt("border", { defaultValue: "0px solid #000", parser: function (t, e, s, r, n, a) { var o = Q(t, "borderTopWidth", i, !1, "0px"), l = this.format(e).split(" "), h = l[0].replace(T, ""); return "px" !== h && (o = parseFloat(o) / K(t, "borderTopWidth", 1, h) + h), this.parseComplex(t.style, this.format(o + " " + Q(t, "borderTopStyle", i, !1, "solid") + " " + Q(t, "borderTopColor", i, !1, "#000")), l.join(" "), n, a); }, color: !0, formatter: function (t) { var e = t.split(" "); return e[0] + " " + (e[1] || "solid") + " " + (t.match(ft) || ["#000"])[0]; } }), wt("borderWidth", { parser: mt("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth") }), wt("float,cssFloat,styleFloat", { parser: function (t, e, i, s, r, n) { var a = t.style, o = "cssFloat" in a ? "cssFloat" : "styleFloat"; return new gt(a, o, 0, 0, r, -1, i, !1, 0, a[o], e); } }); var Wt = function (t) { var e, i = this.t, s = i.filter || Q(this.data, "filter") || "", r = this.s + this.c * t | 0; 100 === r && (-1 === s.indexOf("atrix(") && -1 === s.indexOf("radient(") && -1 === s.indexOf("oader(") ? (i.removeAttribute("filter"), e = !Q(this.data, "filter")) : (i.filter = s.replace(b, ""), e = !0)), e || (this.xn1 && (i.filter = s = s || "alpha(opacity=" + r + ")"), -1 === s.indexOf("pacity") ? 0 === r && this.xn1 || (i.filter = s + " alpha(opacity=" + r + ")") : i.filter = s.replace(x, "opacity=" + r)); }; wt("opacity,alpha,autoAlpha", { defaultValue: "1", parser: function (t, e, s, r, n, a) { var o = parseFloat(Q(t, "opacity", i, !1, "1")), l = t.style, h = "autoAlpha" === s; return "string" == typeof e && "=" === e.charAt(1) && (e = ("-" === e.charAt(0) ? -1 : 1) * parseFloat(e.substr(2)) + o), h && 1 === o && "hidden" === Q(t, "visibility", i) && 0 !== e && (o = 0), V ? n = new gt(l, "opacity", o, e - o, n) : ((n = new gt(l, "opacity", 100 * o, 100 * (e - o), n)).xn1 = h ? 1 : 0, l.zoom = 1, n.type = 2, n.b = "alpha(opacity=" + n.s + ")", n.e = "alpha(opacity=" + (n.s + n.c) + ")", n.data = t, n.plugin = a, n.setRatio = Wt), h && ((n = new gt(l, "visibility", 0, 0, n, -1, null, !1, 0, 0 !== o ? "inherit" : "hidden", 0 === e ? "hidden" : "inherit")).xs0 = "inherit", r._overwriteProps.push(n.n), r._overwriteProps.push(s)), n; } }); var $t = function (t, e) { e && (t.removeProperty ? ("ms" !== e.substr(0, 2) && "webkit" !== e.substr(0, 6) || (e = "-" + e), t.removeProperty(e.replace(S, "-$1").toLowerCase())) : t.removeAttribute(e)); }, Zt = function (t) { if (this.t._gsClassPT = this, 1 === t || 0 === t) {
    this.t.setAttribute("class", 0 === t ? this.b : this.e);
    for (var e = this.data, i = this.t.style; e;)
        e.v ? i[e.p] = e.v : $t(i, e.p), e = e._next;
    1 === t && this.t._gsClassPT === this && (this.t._gsClassPT = null);
}
else
    this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e); }; wt("className", { parser: function (e, s, r, n, a, o, l) { var h, _, u, c, f, p = e.getAttribute("class") || "", m = e.style.cssText; if ((a = n._classNamePT = new gt(e, r, 0, 0, a, 2)).setRatio = Zt, a.pr = -11, t = !0, a.b = p, _ = tt(e, i), u = e._gsClassPT) {
        for (c = {}, f = u.data; f;)
            c[f.p] = 1, f = f._next;
        u.setRatio(1);
    } return e._gsClassPT = a, a.e = "=" !== s.charAt(1) ? s : p.replace(new RegExp("(?:\\s|^)" + s.substr(2) + "(?![\\w-])"), "") + ("+" === s.charAt(0) ? " " + s.substr(2) : ""), e.setAttribute("class", a.e), h = et(e, _, tt(e), l, c), e.setAttribute("class", p), a.data = h.firstMPT, e.style.cssText = m, a.xfirst = n.parse(e, h.difs, a, o); } }); var Ht = function (t) { if ((1 === t || 0 === t) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
    var e, i, s, r, n, o = this.t.style, l = a.transform.parse;
    if ("all" === this.e)
        o.cssText = "", r = !0;
    else
        for (s = (e = this.e.split(" ").join("").split(",")).length; --s > -1;)
            i = e[s], a[i] && (a[i].parse === l ? r = !0 : i = "transformOrigin" === i ? Dt : a[i].p), $t(o, i);
    r && ($t(o, Ct), (n = this.t._gsTransform) && (n.svg && (this.t.removeAttribute("data-svg-origin"), this.t.removeAttribute("transform")), delete this.t._gsTransform));
} }; for (wt("clearProps", { parser: function (e, i, s, r, n) { return (n = new gt(e, s, 0, 0, n, 2)).setRatio = Ht, n.e = i, n.pr = -10, n.data = r._tween, t = !0, n; } }), o = "bezier,throwProps,physicsProps,physics2D".split(","), Tt = o.length; Tt--;)
    bt(o[Tt]); (o = r.prototype)._firstPT = o._lastParsedTransform = o._transform = null, o._onInitTween = function (n, o, u, f) { if (!n.nodeType)
    return !1; this._target = p = n, this._tween = u, this._vars = o, m = f, l = o.autoRound, t = !1, e = o.suffixMap || r.suffixMap, i = H(n, ""), s = this._overwriteProps; var d, g, y, v, T, x, b, P, S, O = n.style; if (h && "" === O.zIndex && ("auto" !== (d = Q(n, "zIndex", i)) && "" !== d || this._addLazySet(O, "zIndex", 0)), "string" == typeof o && (v = O.cssText, d = tt(n, i), O.cssText = v + ";" + o, d = et(n, d, tt(n)).difs, !V && w.test(o) && (d.opacity = parseFloat(RegExp.$1)), o = d, O.cssText = v), o.className ? this._firstPT = g = a.className.parse(n, o.className, "className", this, null, null, o) : this._firstPT = g = this.parse(n, o, null), this._transformType) {
    for (S = 3 === this._transformType, Ct ? _ && (h = !0, "" === O.zIndex && ("auto" !== (b = Q(n, "zIndex", i)) && "" !== b || this._addLazySet(O, "zIndex", 0)), c && this._addLazySet(O, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (S ? "visible" : "hidden"))) : O.zoom = 1, y = g; y && y._next;)
        y = y._next;
    P = new gt(n, "transform", 0, 0, null, 2), this._linkCSSP(P, null, y), P.setRatio = Ct ? Gt : qt, P.data = this._transform || Vt(n, i, !0), P.tween = u, P.pr = -1, s.pop();
} if (t) {
    for (; g;) {
        for (x = g._next, y = v; y && y.pr > g.pr;)
            y = y._next;
        (g._prev = y ? y._prev : T) ? g._prev._next = g : v = g, (g._next = y) ? y._prev = g : T = g, g = x;
    }
    this._firstPT = v;
} return !0; }, o.parse = function (t, s, r, n) { var o, h, _, u, c, f, d, g, y, v, x = t.style; for (o in s) {
    if ("function" == typeof (f = s[o]) && (f = f(m, p)), h = a[o])
        r = h.parse(t, f, o, this, r, n, s);
    else {
        if ("--" === o.substr(0, 2)) {
            this._tween._propLookup[o] = this._addTween.call(this._tween, t.style, "setProperty", H(t).getPropertyValue(o) + "", f + "", o, !1, o);
            continue;
        }
        c = Q(t, o, i) + "", y = "string" == typeof f, "color" === o || "fill" === o || "stroke" === o || -1 !== o.indexOf("Color") || y && P.test(f) ? (y || (f = ((f = ut(f)).length > 3 ? "rgba(" : "rgb(") + f.join(",") + ")"), r = vt(x, o, c, f, !0, "transparent", r, 0, n)) : y && L.test(f) ? r = vt(x, o, c, f, !0, null, r, 0, n) : (d = (_ = parseFloat(c)) || 0 === _ ? c.substr((_ + "").length) : "", "" !== c && "auto" !== c || ("width" === o || "height" === o ? (_ = rt(t, o, i), d = "px") : "left" === o || "top" === o ? (_ = J(t, o, i), d = "px") : (_ = "opacity" !== o ? 0 : 1, d = "")), (v = y && "=" === f.charAt(1)) ? (u = parseInt(f.charAt(0) + "1", 10), f = f.substr(2), u *= parseFloat(f), g = f.replace(T, "")) : (u = parseFloat(f), g = y ? f.replace(T, "") : ""), "" === g && (g = o in e ? e[o] : d), f = u || 0 === u ? (v ? u + _ : u) + g : s[o], d !== g && ("" === g && "lineHeight" !== o || (u || 0 === u) && _ && (_ = K(t, o, _, d), "%" === g ? (_ /= K(t, o, 100, "%") / 100, !0 !== s.strictUnits && (c = _ + "%")) : "em" === g || "rem" === g || "vw" === g || "vh" === g ? _ /= K(t, o, 1, g) : "px" !== g && (u = K(t, o, u, g), g = "px"), v && (u || 0 === u) && (f = u + _ + g))), v && (u += _), !_ && 0 !== _ || !u && 0 !== u ? void 0 !== x[o] && (f || f + "" != "NaN" && null != f) ? (r = new gt(x, o, u || _ || 0, 0, r, -1, o, !1, 0, c, f)).xs0 = "none" !== f || "display" !== o && -1 === o.indexOf("Style") ? f : c : G("invalid " + o + " tween value: " + s[o]) : (r = new gt(x, o, _, u - _, r, 0, o, !1 !== l && ("px" === g || "zIndex" === o), 0, c, f)).xs0 = g);
    }
    n && r && !r.plugin && (r.plugin = n);
} return r; }, o.setRatio = function (t) { var e, i, s, r = this._firstPT; if (1 !== t || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
    if (t || this._tween._time !== this._tween._duration && 0 !== this._tween._time || -1e-6 === this._tween._rawPrevTime)
        for (; r;) {
            if (e = r.c * t + r.s, r.r ? e = r.r(e) : e < 1e-6 && e > -1e-6 && (e = 0), r.type)
                if (1 === r.type)
                    if (2 === (s = r.l))
                        r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2;
                    else if (3 === s)
                        r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3;
                    else if (4 === s)
                        r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4;
                    else if (5 === s)
                        r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4 + r.xn4 + r.xs5;
                    else {
                        for (i = r.xs0 + e + r.xs1, s = 1; s < r.l; s++)
                            i += r["xn" + s] + r["xs" + (s + 1)];
                        r.t[r.p] = i;
                    }
                else
                    -1 === r.type ? r.t[r.p] = r.xs0 : r.setRatio && r.setRatio(t);
            else
                r.t[r.p] = e + r.xs0;
            r = r._next;
        }
    else
        for (; r;)
            2 !== r.type ? r.t[r.p] = r.b : r.setRatio(t), r = r._next;
else
    for (; r;) {
        if (2 !== r.type)
            if (r.r && -1 !== r.type)
                if (e = r.r(r.s + r.c), r.type) {
                    if (1 === r.type) {
                        for (s = r.l, i = r.xs0 + e + r.xs1, s = 1; s < r.l; s++)
                            i += r["xn" + s] + r["xs" + (s + 1)];
                        r.t[r.p] = i;
                    }
                }
                else
                    r.t[r.p] = e + r.xs0;
            else
                r.t[r.p] = r.e;
        else
            r.setRatio(t);
        r = r._next;
    } }, o._enableTransforms = function (t) { this._transform = this._transform || Vt(this._target, i, !0), this._transformType = this._transform.svg && Pt || !t && 3 !== this._transformType ? 2 : 3; }; var Qt = function (t) { this.t[this.p] = this.e, this.data._linkCSSP(this, this._next, null, !0); }; o._addLazySet = function (t, e, i) { var s = this._firstPT = new gt(t, e, 0, 0, this._firstPT, 2); s.e = i, s.setRatio = Qt, s.data = this; }, o._linkCSSP = function (t, e, i, s) { return t && (e && (e._prev = t), t._next && (t._next._prev = t._prev), t._prev ? t._prev._next = t._next : this._firstPT === t && (this._firstPT = t._next, s = !0), i ? i._next = t : s || null !== this._firstPT || (this._firstPT = t), t._next = e, t._prev = i), t; }, o._mod = function (t) { for (var e = this._firstPT; e;)
    "function" == typeof t[e.p] && (e.r = t[e.p]), e = e._next; }, o._kill = function (t) { var e, i, s, r = t; if (t.autoAlpha || t.alpha) {
    for (i in r = {}, t)
        r[i] = t[i];
    r.opacity = 1, r.autoAlpha && (r.visibility = 1);
} for (t.className && (e = this._classNamePT) && ((s = e.xfirst) && s._prev ? this._linkCSSP(s._prev, e._next, s._prev._prev) : s === this._firstPT && (this._firstPT = e._next), e._next && this._linkCSSP(e._next, e._next._next, s._prev), this._classNamePT = null), e = this._firstPT; e;)
    e.plugin && e.plugin !== i && e.plugin._kill && (e.plugin._kill(t), i = e.plugin), e = e._next; return TweenPlugin.prototype._kill.call(this, r); }; var Kt = function (t, e, i) { var s, r, n, a; if (t.slice)
    for (r = t.length; --r > -1;)
        Kt(t[r], e, i);
else
    for (r = (s = t.childNodes).length; --r > -1;)
        a = (n = s[r]).type, n.style && (e.push(tt(n)), i && i.push(n)), 1 !== a && 9 !== a && 11 !== a || !n.childNodes.length || Kt(n, e, i); }; return r.cascadeTo = function (t, e, i) { var s, r, n, a, o = TweenLite.to(t, e, i), l = [o], h = [], _ = [], u = [], c = TweenLite._internals.reservedProps; for (t = o._targets || o.target, Kt(t, h, u), o.render(e, !0, !0), Kt(t, _), o.render(0, !0, !0), o._enabled(!0), s = u.length; --s > -1;)
    if ((r = et(u[s], h[s], _[s])).firstMPT) {
        for (n in r = r.difs, i)
            c[n] && (r[n] = i[n]);
        for (n in a = {}, r)
            a[n] = h[s][n];
        l.push(TweenLite.fromTo(u[s], e, a, r));
    } return l; }, TweenPlugin.activate([r]), r; }, !0);
var CSSPlugin = globals.CSSPlugin, AttrPlugin = _gsScope._gsDefine.plugin({ propName: "attr", API: 2, version: "0.6.1", init: function (t, e, i, s) { var r, n; if ("function" != typeof t.setAttribute)
        return !1; for (r in e)
        "function" == typeof (n = e[r]) && (n = n(s, t)), this._addTween(t, "setAttribute", t.getAttribute(r) + "", n + "", r, !1, r), this._overwriteProps.push(r); return !0; } }), RoundPropsPlugin = _gsScope._gsDefine.plugin({ propName: "roundProps", version: "1.7.0", priority: -1, API: 2, init: function (t, e, i) { return this._tween = i, !0; } }), _getRoundFunc = function (t) { var e = t < 1 ? Math.pow(10, (t + "").length - 2) : 1; return function (i) { return (Math.round(i / t) * t * e | 0) / e; }; }, _roundLinkedList = function (t, e) { for (; t;)
    t.f || t.blob || (t.m = e || Math.round), t = t._next; }, p = RoundPropsPlugin.prototype;
p._onInitAllProps = function () { var t, e, i, s, r = this._tween, n = r.vars.roundProps, a = {}, o = r._propLookup.roundProps; if ("object" != typeof n || n.push)
    for ("string" == typeof n && (n = n.split(",")), i = n.length; --i > -1;)
        a[n[i]] = Math.round;
else
    for (s in n)
        a[s] = _getRoundFunc(n[s]); for (s in a)
    for (t = r._firstPT; t;)
        e = t._next, t.pg ? t.t._mod(a) : t.n === s && (2 === t.f && t.t ? _roundLinkedList(t.t._firstPT, a[s]) : (this._add(t.t, s, t.s, t.c, a[s]), e && (e._prev = t._prev), t._prev ? t._prev._next = e : r._firstPT === t && (r._firstPT = e), t._next = t._prev = null, r._propLookup[s] = o)), t = e; return !1; }, p._add = function (t, e, i, s, r) { this._addTween(t, e, i, i + s, e, r || Math.round), this._overwriteProps.push(e); };
var DirectionalRotationPlugin = _gsScope._gsDefine.plugin({ propName: "directionalRotation", version: "0.3.1", API: 2, init: function (t, e, i, s) { "object" != typeof e && (e = { rotation: e }), this.finals = {}; var r, n, a, o, l, h, _ = !0 === e.useRadians ? 2 * Math.PI : 360; for (r in e)
        "useRadians" !== r && ("function" == typeof (o = e[r]) && (o = o(s, t)), n = (h = (o + "").split("_"))[0], a = parseFloat("function" != typeof t[r] ? t[r] : t[r.indexOf("set") || "function" != typeof t["get" + r.substr(3)] ? r : "get" + r.substr(3)]()), l = (o = this.finals[r] = "string" == typeof n && "=" === n.charAt(1) ? a + parseInt(n.charAt(0) + "1", 10) * Number(n.substr(2)) : Number(n) || 0) - a, h.length && (-1 !== (n = h.join("_")).indexOf("short") && (l %= _) != l % (_ / 2) && (l = l < 0 ? l + _ : l - _), -1 !== n.indexOf("_cw") && l < 0 ? l = (l + 9999999999 * _) % _ - (l / _ | 0) * _ : -1 !== n.indexOf("ccw") && l > 0 && (l = (l - 9999999999 * _) % _ - (l / _ | 0) * _)), (l > 1e-6 || l < -1e-6) && (this._addTween(t, r, a, a + l, r), this._overwriteProps.push(r))); return !0; }, set: function (t) { var e; if (1 !== t)
        this._super.setRatio.call(this, t);
    else
        for (e = this._firstPT; e;)
            e.f ? e.t[e.p](this.finals[e.p]) : e.t[e.p] = this.finals[e.p], e = e._next; } });
DirectionalRotationPlugin._autoCSS = !0;
var _RAD2DEG = 180 / Math.PI, _r1 = [], _r2 = [], _r3 = [], _corProps = {}, _globals = _gsScope._gsDefine.globals, Segment = function (t, e, i, s) { i === s && (i = s - (s - e) / 1e6), t === e && (e = t + (i - t) / 1e6), this.a = t, this.b = e, this.c = i, this.d = s, this.da = s - t, this.ca = i - t, this.ba = e - t; }, _correlate = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,", cubicToQuadratic = function (t, e, i, s) { var r = { a: t }, n = {}, a = {}, o = { c: s }, l = (t + e) / 2, h = (e + i) / 2, _ = (i + s) / 2, u = (l + h) / 2, c = (h + _) / 2, f = (c - u) / 8; return r.b = l + (t - l) / 4, n.b = u + f, r.c = n.a = (r.b + n.b) / 2, n.c = a.a = (u + c) / 2, a.b = c - f, o.b = _ + (s - _) / 4, a.c = o.a = (a.b + o.b) / 2, [r, n, a, o]; }, _calculateControlPoints = function (t, e, i, s, r) { var n, a, o, l, h, _, u, c, f, p, m, d, g, y = t.length - 1, v = 0, T = t[0].a; for (n = 0; n < y; n++)
    a = (h = t[v]).a, o = h.d, l = t[v + 1].d, r ? (m = _r1[n], g = ((d = _r2[n]) + m) * e * .25 / (s ? .5 : _r3[n] || .5), c = o - ((_ = o - (o - a) * (s ? .5 * e : 0 !== m ? g / m : 0)) + (((u = o + (l - o) * (s ? .5 * e : 0 !== d ? g / d : 0)) - _) * (3 * m / (m + d) + .5) / 4 || 0))) : c = o - ((_ = o - (o - a) * e * .5) + (u = o + (l - o) * e * .5)) / 2, _ += c, u += c, h.c = f = _, h.b = 0 !== n ? T : T = h.a + .6 * (h.c - h.a), h.da = o - a, h.ca = f - a, h.ba = T - a, i ? (p = cubicToQuadratic(a, T, f, o), t.splice(v, 1, p[0], p[1], p[2], p[3]), v += 4) : v++, T = u; (h = t[v]).b = T, h.c = T + .4 * (h.d - T), h.da = h.d - h.a, h.ca = h.c - h.a, h.ba = T - h.a, i && (p = cubicToQuadratic(h.a, T, h.c, h.d), t.splice(v, 1, p[0], p[1], p[2], p[3])); }, _parseAnchors = function (t, e, i, s) { var r, n, a, o, l, h, _ = []; if (s)
    for (n = (t = [s].concat(t)).length; --n > -1;)
        "string" == typeof (h = t[n][e]) && "=" === h.charAt(1) && (t[n][e] = s[e] + Number(h.charAt(0) + h.substr(2))); if ((r = t.length - 2) < 0)
    return _[0] = new Segment(t[0][e], 0, 0, t[0][e]), _; for (n = 0; n < r; n++)
    a = t[n][e], o = t[n + 1][e], _[n] = new Segment(a, 0, 0, o), i && (l = t[n + 2][e], _r1[n] = (_r1[n] || 0) + (o - a) * (o - a), _r2[n] = (_r2[n] || 0) + (l - o) * (l - o)); return _[n] = new Segment(t[n][e], 0, 0, t[n + 1][e]), _; }, bezierThrough = function (t, e, i, s, r, n) { var a, o, l, h, _, u, c, f, p = {}, m = [], d = n || t[0]; for (o in r = "string" == typeof r ? "," + r + "," : _correlate, null == e && (e = 1), t[0])
    m.push(o); if (t.length > 1) {
    for (f = t[t.length - 1], c = !0, a = m.length; --a > -1;)
        if (o = m[a], Math.abs(d[o] - f[o]) > .05) {
            c = !1;
            break;
        }
    c && (t = t.concat(), n && t.unshift(n), t.push(t[1]), n = t[t.length - 3]);
} for (_r1.length = _r2.length = _r3.length = 0, a = m.length; --a > -1;)
    o = m[a], _corProps[o] = -1 !== r.indexOf("," + o + ","), p[o] = _parseAnchors(t, o, _corProps[o], n); for (a = _r1.length; --a > -1;)
    _r1[a] = Math.sqrt(_r1[a]), _r2[a] = Math.sqrt(_r2[a]); if (!s) {
    for (a = m.length; --a > -1;)
        if (_corProps[o])
            for (u = (l = p[m[a]]).length - 1, h = 0; h < u; h++)
                _ = l[h + 1].da / _r2[h] + l[h].da / _r1[h] || 0, _r3[h] = (_r3[h] || 0) + _ * _;
    for (a = _r3.length; --a > -1;)
        _r3[a] = Math.sqrt(_r3[a]);
} for (a = m.length, h = i ? 4 : 1; --a > -1;)
    l = p[o = m[a]], _calculateControlPoints(l, e, i, s, _corProps[o]), c && (l.splice(0, h), l.splice(l.length - h, h)); return p; }, _parseBezierData = function (t, e, i) { var s, r, n, a, o, l, h, _, u, c, f, p = {}, m = "cubic" === (e = e || "soft") ? 3 : 2, d = "soft" === e, g = []; if (d && i && (t = [i].concat(t)), null == t || t.length < m + 1)
    throw "invalid Bezier data"; for (u in t[0])
    g.push(u); for (l = g.length; --l > -1;) {
    for (p[u = g[l]] = o = [], c = 0, _ = t.length, h = 0; h < _; h++)
        s = null == i ? t[h][u] : "string" == typeof (f = t[h][u]) && "=" === f.charAt(1) ? i[u] + Number(f.charAt(0) + f.substr(2)) : Number(f), d && h > 1 && h < _ - 1 && (o[c++] = (s + o[c - 2]) / 2), o[c++] = s;
    for (_ = c - m + 1, c = 0, h = 0; h < _; h += m)
        s = o[h], r = o[h + 1], n = o[h + 2], a = 2 === m ? 0 : o[h + 3], o[c++] = f = 3 === m ? new Segment(s, r, n, a) : new Segment(s, (2 * r + s) / 3, (2 * r + n) / 3, n);
    o.length = c;
} return p; }, _addCubicLengths = function (t, e, i) { for (var s, r, n, a, o, l, h, _, u, c, f, p = 1 / i, m = t.length; --m > -1;)
    for (n = (c = t[m]).a, a = c.d - n, o = c.c - n, l = c.b - n, s = r = 0, _ = 1; _ <= i; _++)
        s = r - (r = ((h = p * _) * h * a + 3 * (u = 1 - h) * (h * o + u * l)) * h), e[f = m * i + _ - 1] = (e[f] || 0) + s * s; }, _parseLengthData = function (t, e) { var i, s, r, n, a = [], o = [], l = 0, h = 0, _ = (e = e >> 0 || 6) - 1, u = [], c = []; for (i in t)
    _addCubicLengths(t[i], a, e); for (r = a.length, s = 0; s < r; s++)
    l += Math.sqrt(a[s]), c[n = s % e] = l, n === _ && (h += l, u[n = s / e >> 0] = c, o[n] = h, l = 0, c = []); return { length: h, lengths: o, segments: u }; }, BezierPlugin = _gsScope._gsDefine.plugin({ propName: "bezier", priority: -1, version: "1.3.8", API: 2, global: !0, init: function (t, e, i) { this._target = t, e instanceof Array && (e = { values: e }), this._func = {}, this._mod = {}, this._props = [], this._timeRes = null == e.timeResolution ? 6 : parseInt(e.timeResolution, 10); var s, r, n, a, o, l = e.values || [], h = {}, _ = l[0], u = e.autoRotate || i.vars.orientToBezier; for (s in this._autoRotate = u ? u instanceof Array ? u : [["x", "y", "rotation", !0 === u ? 0 : Number(u) || 0]] : null, _)
        this._props.push(s); for (n = this._props.length; --n > -1;)
        s = this._props[n], this._overwriteProps.push(s), r = this._func[s] = "function" == typeof t[s], h[s] = r ? t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)]() : parseFloat(t[s]), o || h[s] !== l[0][s] && (o = h); if (this._beziers = "cubic" !== e.type && "quadratic" !== e.type && "soft" !== e.type ? bezierThrough(l, isNaN(e.curviness) ? 1 : e.curviness, !1, "thruBasic" === e.type, e.correlate, o) : _parseBezierData(l, e.type, h), this._segCount = this._beziers[s].length, this._timeRes) {
        var c = _parseLengthData(this._beziers, this._timeRes);
        this._length = c.length, this._lengths = c.lengths, this._segments = c.segments, this._l1 = this._li = this._s1 = this._si = 0, this._l2 = this._lengths[0], this._curSeg = this._segments[0], this._s2 = this._curSeg[0], this._prec = 1 / this._curSeg.length;
    } if (u = this._autoRotate)
        for (this._initialRotations = [], u[0] instanceof Array || (this._autoRotate = u = [u]), n = u.length; --n > -1;) {
            for (a = 0; a < 3; a++)
                s = u[n][a], this._func[s] = "function" == typeof t[s] && t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)];
            s = u[n][2], this._initialRotations[n] = (this._func[s] ? this._func[s].call(this._target) : this._target[s]) || 0, this._overwriteProps.push(s);
        } return this._startRatio = i.vars.runBackwards ? 1 : 0, !0; }, set: function (t) { var e, i, s, r, n, a, o, l, h, _, u = this._segCount, c = this._func, f = this._target, p = t !== this._startRatio; if (this._timeRes) {
        if (h = this._lengths, _ = this._curSeg, t *= this._length, s = this._li, t > this._l2 && s < u - 1) {
            for (l = u - 1; s < l && (this._l2 = h[++s]) <= t;)
                ;
            this._l1 = h[s - 1], this._li = s, this._curSeg = _ = this._segments[s], this._s2 = _[this._s1 = this._si = 0];
        }
        else if (t < this._l1 && s > 0) {
            for (; s > 0 && (this._l1 = h[--s]) >= t;)
                ;
            0 === s && t < this._l1 ? this._l1 = 0 : s++, this._l2 = h[s], this._li = s, this._curSeg = _ = this._segments[s], this._s1 = _[(this._si = _.length - 1) - 1] || 0, this._s2 = _[this._si];
        }
        if (e = s, t -= this._l1, s = this._si, t > this._s2 && s < _.length - 1) {
            for (l = _.length - 1; s < l && (this._s2 = _[++s]) <= t;)
                ;
            this._s1 = _[s - 1], this._si = s;
        }
        else if (t < this._s1 && s > 0) {
            for (; s > 0 && (this._s1 = _[--s]) >= t;)
                ;
            0 === s && t < this._s1 ? this._s1 = 0 : s++, this._s2 = _[s], this._si = s;
        }
        a = (s + (t - this._s1) / (this._s2 - this._s1)) * this._prec || 0;
    }
    else
        a = (t - (e = t < 0 ? 0 : t >= 1 ? u - 1 : u * t >> 0) * (1 / u)) * u; for (i = 1 - a, s = this._props.length; --s > -1;)
        r = this._props[s], o = (a * a * (n = this._beziers[r][e]).da + 3 * i * (a * n.ca + i * n.ba)) * a + n.a, this._mod[r] && (o = this._mod[r](o, f)), c[r] ? f[r](o) : f[r] = o; if (this._autoRotate) {
        var m, d, g, y, v, T, x, w = this._autoRotate;
        for (s = w.length; --s > -1;)
            r = w[s][2], T = w[s][3] || 0, x = !0 === w[s][4] ? 1 : _RAD2DEG, n = this._beziers[w[s][0]], m = this._beziers[w[s][1]], n && m && (n = n[e], m = m[e], d = n.a + (n.b - n.a) * a, d += ((y = n.b + (n.c - n.b) * a) - d) * a, y += (n.c + (n.d - n.c) * a - y) * a, g = m.a + (m.b - m.a) * a, g += ((v = m.b + (m.c - m.b) * a) - g) * a, v += (m.c + (m.d - m.c) * a - v) * a, o = p ? Math.atan2(v - g, y - d) * x + T : this._initialRotations[s], this._mod[r] && (o = this._mod[r](o, f)), c[r] ? f[r](o) : f[r] = o);
    } } }), p$1 = BezierPlugin.prototype;
BezierPlugin.bezierThrough = bezierThrough, BezierPlugin.cubicToQuadratic = cubicToQuadratic, BezierPlugin._autoCSS = !0, BezierPlugin.quadraticToCubic = function (t, e, i) { return new Segment(t, (2 * e + t) / 3, (2 * e + i) / 3, i); }, BezierPlugin._cssRegister = function () { var t = _globals.CSSPlugin; if (t) {
    var e = t._internals, i = e._parseToProxy, s = e._setPluginRatio, r = e.CSSPropTween;
    e._registerComplexSpecialProp("bezier", { parser: function (t, e, n, a, o, l) { e instanceof Array && (e = { values: e }), l = new BezierPlugin; var h, _, u, c = e.values, f = c.length - 1, p = [], m = {}; if (f < 0)
            return o; for (h = 0; h <= f; h++)
            u = i(t, c[h], a, o, l, f !== h), p[h] = u.end; for (_ in e)
            m[_] = e[_]; return m.values = p, (o = new r(t, "bezier", 0, 0, u.pt, 2)).data = u, o.plugin = l, o.setRatio = s, 0 === m.autoRotate && (m.autoRotate = !0), !m.autoRotate || m.autoRotate instanceof Array || (h = !0 === m.autoRotate ? 0 : Number(m.autoRotate), m.autoRotate = null != u.end.left ? [["left", "top", "rotation", h, !1]] : null != u.end.x && [["x", "y", "rotation", h, !1]]), m.autoRotate && (a._transform || a._enableTransforms(!1), u.autoRotate = a._target._gsTransform, u.proxy.rotation = u.autoRotate.rotation || 0, a._overwriteProps.push("rotation")), l._onInitTween(u.proxy, m, a._tween), o; } });
} }, p$1._mod = function (t) { for (var e, i = this._overwriteProps, s = i.length; --s > -1;)
    (e = t[i[s]]) && "function" == typeof e && (this._mod[i[s]] = e); }, p$1._kill = function (t) { var e, i, s = this._props; for (e in this._beziers)
    if (e in t)
        for (delete this._beziers[e], delete this._func[e], i = s.length; --i > -1;)
            s[i] === e && s.splice(i, 1); if (s = this._autoRotate)
    for (i = s.length; --i > -1;)
        t[s[i][2]] && s.splice(i, 1); return this._super._kill.call(this, t); }, _gsScope._gsDefine("easing.Back", ["easing.Ease"], function () { var t, e, i, s, r = _gsScope.GreenSockGlobals || _gsScope, n = r.com.greensock, a = 2 * Math.PI, o = Math.PI / 2, l = n._class, h = function (t, e) { var i = l("easing." + t, function () { }, !0), s = i.prototype = new Ease; return s.constructor = i, s.getRatio = e, i; }, _ = Ease.register || function () { }, u = function (t, e, i, s, r) { var n = l("easing." + t, { easeOut: new e, easeIn: new i, easeInOut: new s }, !0); return _(n, t), n; }, c = function (t, e, i) { this.t = t, this.v = e, i && (this.next = i, i.prev = this, this.c = i.v - e, this.gap = i.t - t); }, f = function (t, e) { var i = l("easing." + t, function (t) { this._p1 = t || 0 === t ? t : 1.70158, this._p2 = 1.525 * this._p1; }, !0), s = i.prototype = new Ease; return s.constructor = i, s.getRatio = e, s.config = function (t) { return new i(t); }, i; }, p = u("Back", f("BackOut", function (t) { return (t -= 1) * t * ((this._p1 + 1) * t + this._p1) + 1; }), f("BackIn", function (t) { return t * t * ((this._p1 + 1) * t - this._p1); }), f("BackInOut", function (t) { return (t *= 2) < 1 ? .5 * t * t * ((this._p2 + 1) * t - this._p2) : .5 * ((t -= 2) * t * ((this._p2 + 1) * t + this._p2) + 2); })), m = l("easing.SlowMo", function (t, e, i) { e = e || 0 === e ? e : .7, null == t ? t = .7 : t > 1 && (t = 1), this._p = 1 !== t ? e : 0, this._p1 = (1 - t) / 2, this._p2 = t, this._p3 = this._p1 + this._p2, this._calcEnd = !0 === i; }, !0), d = m.prototype = new Ease; return d.constructor = m, d.getRatio = function (t) { var e = t + (.5 - t) * this._p; return t < this._p1 ? this._calcEnd ? 1 - (t = 1 - t / this._p1) * t : e - (t = 1 - t / this._p1) * t * t * t * e : t > this._p3 ? this._calcEnd ? 1 === t ? 0 : 1 - (t = (t - this._p3) / this._p1) * t : e + (t - e) * (t = (t - this._p3) / this._p1) * t * t * t : this._calcEnd ? 1 : e; }, m.ease = new m(.7, .7), d.config = m.config = function (t, e, i) { return new m(t, e, i); }, (d = (t = l("easing.SteppedEase", function (t, e) { t = t || 1, this._p1 = 1 / t, this._p2 = t + (e ? 0 : 1), this._p3 = e ? 1 : 0; }, !0)).prototype = new Ease).constructor = t, d.getRatio = function (t) { return t < 0 ? t = 0 : t >= 1 && (t = .999999999), ((this._p2 * t | 0) + this._p3) * this._p1; }, d.config = t.config = function (e, i) { return new t(e, i); }, (d = (e = l("easing.ExpoScaleEase", function (t, e, i) { this._p1 = Math.log(e / t), this._p2 = e - t, this._p3 = t, this._ease = i; }, !0)).prototype = new Ease).constructor = e, d.getRatio = function (t) { return this._ease && (t = this._ease.getRatio(t)), (this._p3 * Math.exp(this._p1 * t) - this._p3) / this._p2; }, d.config = e.config = function (t, i, s) { return new e(t, i, s); }, (d = (i = l("easing.RoughEase", function (t) { for (var e, i, s, r, n, a, o = (t = t || {}).taper || "none", l = [], h = 0, _ = 0 | (t.points || 20), u = _, f = !1 !== t.randomize, p = !0 === t.clamp, m = t.template instanceof Ease ? t.template : null, d = "number" == typeof t.strength ? .4 * t.strength : .4; --u > -1;)
    e = f ? Math.random() : 1 / _ * u, i = m ? m.getRatio(e) : e, s = "none" === o ? d : "out" === o ? (r = 1 - e) * r * d : "in" === o ? e * e * d : e < .5 ? (r = 2 * e) * r * .5 * d : (r = 2 * (1 - e)) * r * .5 * d, f ? i += Math.random() * s - .5 * s : u % 2 ? i += .5 * s : i -= .5 * s, p && (i > 1 ? i = 1 : i < 0 && (i = 0)), l[h++] = { x: e, y: i }; for (l.sort(function (t, e) { return t.x - e.x; }), a = new c(1, 1, null), u = _; --u > -1;)
    n = l[u], a = new c(n.x, n.y, a); this._prev = new c(0, 0, 0 !== a.t ? a : a.next); }, !0)).prototype = new Ease).constructor = i, d.getRatio = function (t) { var e = this._prev; if (t > e.t) {
    for (; e.next && t >= e.t;)
        e = e.next;
    e = e.prev;
}
else
    for (; e.prev && t <= e.t;)
        e = e.prev; return this._prev = e, e.v + (t - e.t) / e.gap * e.c; }, d.config = function (t) { return new i(t); }, i.ease = new i, u("Bounce", h("BounceOut", function (t) { return t < 1 / 2.75 ? 7.5625 * t * t : t < 2 / 2.75 ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : t < 2.5 / 2.75 ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375; }), h("BounceIn", function (t) { return (t = 1 - t) < 1 / 2.75 ? 1 - 7.5625 * t * t : t < 2 / 2.75 ? 1 - (7.5625 * (t -= 1.5 / 2.75) * t + .75) : t < 2.5 / 2.75 ? 1 - (7.5625 * (t -= 2.25 / 2.75) * t + .9375) : 1 - (7.5625 * (t -= 2.625 / 2.75) * t + .984375); }), h("BounceInOut", function (t) { var e = t < .5; return (t = e ? 1 - 2 * t : 2 * t - 1) < 1 / 2.75 ? t *= 7.5625 * t : t = t < 2 / 2.75 ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : t < 2.5 / 2.75 ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375, e ? .5 * (1 - t) : .5 * t + .5; })), u("Circ", h("CircOut", function (t) { return Math.sqrt(1 - (t -= 1) * t); }), h("CircIn", function (t) { return -(Math.sqrt(1 - t * t) - 1); }), h("CircInOut", function (t) { return (t *= 2) < 1 ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1); })), u("Elastic", (s = function (t, e, i) { var s = l("easing." + t, function (t, e) { this._p1 = t >= 1 ? t : 1, this._p2 = (e || i) / (t < 1 ? t : 1), this._p3 = this._p2 / a * (Math.asin(1 / this._p1) || 0), this._p2 = a / this._p2; }, !0), r = s.prototype = new Ease; return r.constructor = s, r.getRatio = e, r.config = function (t, e) { return new s(t, e); }, s; })("ElasticOut", function (t) { return this._p1 * Math.pow(2, -10 * t) * Math.sin((t - this._p3) * this._p2) + 1; }, .3), s("ElasticIn", function (t) { return -this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * this._p2); }, .3), s("ElasticInOut", function (t) { return (t *= 2) < 1 ? this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * this._p2) * -.5 : this._p1 * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - this._p3) * this._p2) * .5 + 1; }, .45)), u("Expo", h("ExpoOut", function (t) { return 1 - Math.pow(2, -10 * t); }), h("ExpoIn", function (t) { return Math.pow(2, 10 * (t - 1)) - .001; }), h("ExpoInOut", function (t) { return (t *= 2) < 1 ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (2 - Math.pow(2, -10 * (t - 1))); })), u("Sine", h("SineOut", function (t) { return Math.sin(t * o); }), h("SineIn", function (t) { return 1 - Math.cos(t * o); }), h("SineInOut", function (t) { return -.5 * (Math.cos(Math.PI * t) - 1); })), l("easing.EaseLookup", { find: function (t) { return Ease.map[t]; } }, !0), _(r.SlowMo, "SlowMo", "ease,"), _(i, "RoughEase", "ease,"), _(t, "SteppedEase", "ease,"), p; }, !0);
var Back = globals.Back, Elastic = globals.Elastic, Bounce = globals.Bounce, RoughEase = globals.RoughEase, SlowMo = globals.SlowMo, SteppedEase = globals.SteppedEase, Circ = globals.Circ, Expo = globals.Expo, Sine = globals.Sine, ExpoScaleEase = globals.ExpoScaleEase, TweenMax$1 = TweenMax;
TweenMax$1._autoActivated = [TimelineLite, TimelineMax, CSSPlugin, AttrPlugin, BezierPlugin, RoundPropsPlugin, DirectionalRotationPlugin, Back, Elastic, Bounce, RoughEase, SlowMo, SteppedEase, Circ, Expo, Sine, ExpoScaleEase];
export { TweenLite as a, Power1 as b, Power4 as c, TimelineLite as d, TweenMax$1 as e };
