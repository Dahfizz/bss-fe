// webcomponents: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './webcomponents.core.js';
import {
  Accordion,
  Arrow,
  Body,
  Checkbox,
  Dummy,
  Header,
  Item,
  MenuFlyout,
  MenuFlyoutList,
  MenuFlyoutListItem,
  MenuFlyoutToggle,
  ProgressFull,
  ProgressFullStep,
  Ribbon,
  Section
} from './webcomponents.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    Accordion,
    Arrow,
    Body,
    Checkbox,
    Dummy,
    Header,
    Item,
    MenuFlyout,
    MenuFlyoutList,
    MenuFlyoutListItem,
    MenuFlyoutToggle,
    ProgressFull,
    ProgressFullStep,
    Ribbon,
    Section
  ], opts);
}