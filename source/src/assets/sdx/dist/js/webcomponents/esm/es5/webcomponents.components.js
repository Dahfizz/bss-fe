// webcomponents: Host Data, ES Module/ES5 Target

export var Accordion = ["sdx-accordion",function(o){return(o.scoped?import("./y9yrwccm.sc.js"):import("./y9yrwccm.js")).then(function(m){return m.SdxAccordion})},1,[["arrowPosition",1,0,"arrow-position",2],["close",6],["closeAll",6],["el",7],["keepOpen",1,0,"keep-open",3],["open",6],["openAll",6],["toggle",6]],1];

export var Arrow = ["sdx-accordion-arrow",function(o){return(o.scoped?import("./udfbdpq7.sc.js"):import("./udfbdpq7.js")).then(function(m){return m.SdxAccordionArrow})},1,[["arrowPosition",1,0,"arrow-position",2],["direction",1,0,1,2],["hover",1,0,1,3]],1];

export var Item = ["sdx-accordion-item",function(o){return(o.scoped?import("./uzi5zxn5.sc.js"):import("./uzi5zxn5.js")).then(function(m){return m.SdxAccordionItem})},1,[["el",7],["open",1,0,1,3]],1];

export var Body = ["sdx-accordion-item-body",function(o){return(o.scoped?import("./mcebidee.sc.js"):import("./mcebidee.js")).then(function(m){return m.SdxAccordionItemBody})},1,[["arrowPosition",1,0,"arrow-position",2],["el",7],["toggle",6]],1];

export var Header = ["sdx-accordion-item-header",function(o){return(o.scoped?import("./udfbdpq7.sc.js"):import("./udfbdpq7.js")).then(function(m){return m.SdxAccordionItemHeader})},1,[["arrowPosition",1,0,"arrow-position",2],["closeItem",6],["el",7],["expand",1,0,1,3],["openItem",6],["toggle",1]],1,[["click","onClick"],["mouseover","onMouseOver",0,1],["mouseout","onMouseOut",0,1]]];

export var Section = ["sdx-accordion-item-section",function(o){return(o.scoped?import("./evne3ybj.sc.js"):import("./evne3ybj.js")).then(function(m){return m.SdxAccordionItemSection})},1,0,1];

export var Checkbox = ["sdx-checkbox",function(o){return(o.scoped?import("./ta0kgmyw.sc.js"):import("./ta0kgmyw.js")).then(function(m){return m.SdxCheckbox})},1,[["checked",1,0,1,3]],1];

export var Dummy = ["sdx-dummy",function(o){return(o.scoped?import("./kbfumsrw.sc.js"):import("./kbfumsrw.js")).then(function(m){return m.SdxDummy})},0,0,1];

export var MenuFlyout = ["sdx-menu-flyout",function(o){return(o.scoped?import("./co7fatfd.sc.js"):import("./co7fatfd.js")).then(function(m){return m.SdxMenuFlyout})},1,[["close",6],["closeOnClick",1,0,"close-on-click",3],["direction",1,0,1,2],["el",7],["open",6],["toggle",6]],1,[["click","onClick"],["touchend","onClick",0,1],["window:click","onWindowClick"],["window:touchend","onWindowClick",0,1]]];

export var MenuFlyoutList = ["sdx-menu-flyout-list",function(o){return(o.scoped?import("./bhvinzit.sc.js"):import("./bhvinzit.js")).then(function(m){return m.SdxMenuFlyoutList})},1,0,1];

export var MenuFlyoutListItem = ["sdx-menu-flyout-list-item",function(o){return(o.scoped?import("./tmoynarq.sc.js"):import("./tmoynarq.js")).then(function(m){return m.SdxMenuFlyoutListItem})},1,[["direction",1,0,1,2],["selectable",1,0,1,3]],1];

export var MenuFlyoutToggle = ["sdx-menu-flyout-toggle",function(o){return(o.scoped?import("./rfxsmmvs.sc.js"):import("./rfxsmmvs.js")).then(function(m){return m.SdxMenuFlyoutToggle})},1,[["toggle",1]],1,[["click","onClick"]]];

export var ProgressFull = ["sdx-progress-full",function(o){return(o.scoped?import("./qkqxmxdi.sc.js"):import("./qkqxmxdi.js")).then(function(m){return m.SdxProgressFull})},1,[["activeStep",5],["animated",1,0,1,3],["el",7],["getActiveStep",6],["nextStep",6],["onStepChange",1,0,"on-step-change",2],["previousActiveStep",5],["previousStep",6],["setActiveStep",6],["step",1,0,1,4],["stepsLabel",1,0,"steps-label",2]],1,[["window:resize","resizePerformed",0,1]]];

export var ProgressFullStep = ["sdx-progress-full-step",function(o){return(o.scoped?import("./znlpgjq2.sc.js"):import("./znlpgjq2.js")).then(function(m){return m.SdxProgressFullStep})},1,[["el",7],["onStepClick",1],["position",1,0,1,2],["status",1,0,1,2],["value",1,0,1,4]],1];

export var Ribbon = ["sdx-ribbon",function(o){return(o.scoped?import("./gpomao4x.sc.js"):import("./gpomao4x.js")).then(function(m){return m.SdxRibbon})},1,[["design",1,0,1,2],["label",1,0,1,2],["position",1,0,1,2],["size",1,0,1,2]],1];