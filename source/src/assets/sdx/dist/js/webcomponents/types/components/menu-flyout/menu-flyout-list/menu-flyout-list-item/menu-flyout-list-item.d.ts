import '../../../../stencil.core';
import { Direction } from "../../types";
export declare class MenuFlyoutListItem {
    /**
     * If an item is not selectable, it is neither highlighted nor has it cursor: pointer.
     */
    selectable: boolean;
    /**
     * @private
     */
    direction: Direction;
    hostData(): {
        class: {
            [x: string]: boolean;
            selectable: boolean;
        };
    };
    render(): JSX.Element;
}
