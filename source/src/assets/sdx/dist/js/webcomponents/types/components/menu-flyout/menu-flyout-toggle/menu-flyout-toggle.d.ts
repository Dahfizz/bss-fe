import '../../../stencil.core';
export declare class MenuFlyoutToggle {
    /**
     * Callback fired on toggle element click (will be set internally).
     */
    toggle: () => void;
    onClick(): void;
    render(): JSX.Element;
}
