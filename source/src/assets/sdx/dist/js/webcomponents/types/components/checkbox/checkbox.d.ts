import '../../stencil.core';
export declare class Checkbox {
    checked: boolean;
    hostData(): {
        class: {
            checked: boolean;
        };
    };
    render(): JSX.Element;
}
