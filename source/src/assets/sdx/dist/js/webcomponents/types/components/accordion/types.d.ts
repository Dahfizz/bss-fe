export declare type ArrowPosition = "none" | "right" | "left" | "center";
export declare type ArrowDirection = "up" | "down";
