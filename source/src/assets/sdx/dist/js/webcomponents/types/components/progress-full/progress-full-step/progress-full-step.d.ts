import '../../../stencil.core';
import { Status, StepPosition } from "../types";
export declare class ProgressFullStep {
    el: HTMLSdxProgressFullStepElement;
    /**
     * @Private
     */
    value: number;
    /**
     * @Private
     */
    status: Status;
    /**
     * @Private
     */
    position: StepPosition;
    /**
     * Triggered when a user clicks on the button or description of a completed step.
     */
    onStepClick: () => void;
    /**
     * Trigger click event when completed state.
     */
    private clicked;
    render(): JSX.Element;
}
