import '../../stencil.core';
import { Direction } from "./types";
export declare class MenuFlyout {
    private toggleEl?;
    private listEl?;
    private listItemEls;
    private toggleElChild?;
    private animationDuration;
    private arrowOffset;
    private animationOffset;
    private isOpen;
    private listPosition;
    private isClicking;
    el: HTMLSdxMenuFlyoutElement;
    /**
     * In which direction the flyout opens.
     */
    direction: Direction;
    /**
     * Close if the user clicks on the flyout.
     */
    closeOnClick: boolean;
    directionChanged(): void;
    componentDidLoad(): void;
    onClick(): void;
    onWindowClick(): void;
    /**
     * Fired by the MutationObserver whenever children change.
     */
    onChildrenChange(): void;
    /**
     * Toggles the flyout.
     */
    toggle(): Promise<void>;
    /**
     * Opens the flyout.
     */
    open(): Promise<void>;
    /**
     * Closes the flyout.
     */
    close(): Promise<void>;
    /**
     * Traverse through child components, keep references and pass props to them.
     */
    private setChildReferences;
    /**
     * Return the position where the flyout will appear.
     * @param direction Desired direction.
     */
    private getListPosition;
    /**
     * Generates a string "translated3d(...)" that can be passed to the transition style property.
     * @param position Position to be transformed into a string.
     * @param offset Will be added to the position.
     */
    private positionToTranslate3d;
    /**
     * Inform the children about the their props.
     */
    private assignPropsToChildren;
    render(): JSX.Element;
}
