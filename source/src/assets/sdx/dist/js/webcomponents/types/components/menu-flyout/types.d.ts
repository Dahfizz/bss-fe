export declare type Direction = "bottom-right" | "bottom-left" | "top-right" | "top-left";
export declare type Position = [number, number];
