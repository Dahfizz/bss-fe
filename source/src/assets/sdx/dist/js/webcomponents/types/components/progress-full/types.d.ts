/**
 * @private
 */
export declare type Status = "completed" | "active" | "none" | "disabled";
/**
 * @private
 */
export declare type StepPosition = "none" | "first" | "middle-left" | "middle" | "middle-right" | "last";
