import * as tslib_1 from "tslib";
import { searchAndInitialize, clamp, preventDefault, remove } from "../Utils";
import DomElement from "../DomElement";
import * as Inputs from "../Inputs";
import * as Dom from "../DomFunctions";
var QUERY_SLIDER = ".carousel__container";
var QUERY_SLIDE_AREA = ".carousel__slider";
var QUERY_WRAPPER = ".carousel__slider-wrapper";
var QUERY_PAGINATION = ".carousel__pagination";
var CLASS_ACTIVE = "slide--active";
var CLASS_PREV = "slide--prev";
var CLASS_NEXT = "slide--next";
var CLASS_BULLET = "pagination-bullet";
var CLASS_BULLET_ACTIVE = "pagination-bullet--active";
var QUERY_BTN_PREV = ".carousel__button-prev";
var QUERY_BTN_NEXT = ".carousel__button-next";
var QUERY_BTN_WRAPPER = ".carousel__button-wrapper";
var ATTRIBUTE_INDEX = "js-index";
var ANIMATION_DURATION = 350;
var ANIMATION_EASING = "ease-in-out";
var TOUCH_DURATION = 300;
var TOUCH_DELTA_MIN = 25;
/**
 * The carousel component definition.
 */
var Carousel = /** @class */ (function (_super) {
    tslib_1.__extends(Carousel, _super);
    /**
     * Creates and initializes the carousel component.
     * @param {DomElement} element - The root element of the Carousel component.
     * @param {Number} index - The initial index.
     */
    function Carousel(element, index) {
        if (index === void 0) { index = 0; }
        var _this = _super.call(this, element) || this;
        _this._slider = _this.element.querySelector(QUERY_SLIDER);
        _this._wrapper = _this._slider.querySelector(QUERY_WRAPPER);
        _this._pagination = _this._slider.querySelector(QUERY_PAGINATION);
        _this._slideArea = _this._slider.querySelector(QUERY_SLIDE_AREA);
        _this._btnWrapper = _this.element.querySelector(QUERY_BTN_WRAPPER);
        _this._prevCtrl = _this.element.querySelector(QUERY_BTN_PREV);
        _this._nextCtrl = _this.element.querySelector(QUERY_BTN_NEXT);
        _this._slides = [];
        _this._index = index || 0;
        _this._slidesPerGroup = 1;
        _this._sliderWrapper = new SliderWrapper(_this._wrapper, _this._slideArea, _this.element);
        _this._sliderWrapper.index = _this._index;
        _this._additionalSlideMargin = 0;
        _this._resizeHandler = _this._onresize.bind(_this);
        _this._prevHandler = _this.prev.bind(_this);
        _this._nextHandler = _this.next.bind(_this);
        _this._paginationClickHandler = _this._handlePaginationClick.bind(_this);
        _this._keydownHandler = _this._handleKeydown.bind(_this);
        _this._handleTouchstart = _this._onTouchstart.bind(_this);
        _this._handleTouchmove = _this._onTouchmove.bind(_this);
        _this._handleTouchend = _this._onTouchend.bind(_this);
        _this._initialize();
        _this.slide(_this._index, 0, false);
        _this._updateCtrlOffsets();
        return _this;
    }
    /**
     * Initializes the carousel component.
     * @private
     */
    Carousel.prototype._initialize = function () {
        // responsive helpers
        this._breakpointPhone = new DomElement("div")
            .addClass("js-phone")
            .element;
        this._breakpointTablet = new DomElement("div")
            .addClass("js-tablet")
            .element;
        this._breakpointDesktop = new DomElement("div")
            .addClass("js-desktop")
            .element;
        this.element.appendChild(this._breakpointPhone);
        this.element.appendChild(this._breakpointTablet);
        this.element.appendChild(this._breakpointDesktop);
        if (this._prevCtrl && this._nextCtrl) {
            this._prevCtrl.addEventListener("click", this._prevHandler);
            this._nextCtrl.addEventListener("click", this._nextHandler);
        }
        if (this._pagination) {
            this._pagination.addEventListener("click", this._paginationClickHandler);
        }
        this._slides = Array.from(this._wrapper.children);
        if (this._slides.length === 0) {
            throw Error("Provide at least one slide to the slider");
        }
        for (var i = 0; i < this._slides.length; i++) {
            var slide = this._slides[i];
            slide.setAttribute(ATTRIBUTE_INDEX, String(i));
        }
        this._updateResponsiveOptions();
        this._sliderWrapper.initialize();
        this.reset();
        this.element.addEventListener("keydown", this._keydownHandler);
        this._slideArea.addEventListener("mousedown", this._handleTouchstart);
        this._slideArea.addEventListener("touchstart", this._handleTouchstart);
        window.addEventListener("resize", this._resizeHandler);
        window.addEventListener("orientationchange", this._resizeHandler);
    };
    Carousel.prototype._isBreakpointActive = function (breakpoint) {
        var style = window.getComputedStyle(breakpoint);
        return style.visibility === "visible";
    };
    Carousel.prototype._onresize = function () {
        this.reset();
        this._updateCtrlOffsets();
    };
    /**
     * Makes sure the index is always in the range of available slide
     * In case it's to high or to low it is wrapped around
     * @param {Number} index - The index to adjust and sanitize
     * @returns {Number} index - The adjusted index
     * @private
     */
    Carousel.prototype._adjustIndex = function (index) {
        if (typeof index !== "number") {
            index = 0;
        }
        if (index < 0) {
            index = this._wrapround(index, 0, this._slides.length);
        }
        else if (index >= this._slides.length) {
            index %= this._slides.length;
        }
        return Math.floor(index / this._slidesPerGroup) * this._slidesPerGroup;
    };
    Carousel.prototype._wrapround = function (n, min, max) {
        if (n >= max) {
            return min;
        }
        if (n < min) {
            return max - 1;
        }
        return n;
    };
    Carousel.prototype._wraproundCount = function (a, b, min, max, direction) {
        if (direction === 0) {
            return 0;
        }
        if (a < min || a >= max) {
            throw new Error("Argument 'a' is out of range, Value: " + a + " Min: " + min + ", Max: " + max);
        }
        if (b < min || b >= max) {
            throw new Error("Argument 'b' is out of range, Value: " + b + " Min: " + min + ", Max: " + max);
        }
        var i = 0;
        while (a !== b) {
            i++;
            a = this._wrapround(a + direction, min, max);
        }
        return i;
    };
    Carousel.prototype._updateCtrlOffsets = function () {
        if (!this._nextCtrl || !this._prevCtrl || !this._btnWrapper) {
            return;
        }
        var prevCtrlMargin = 0;
        var nextCtrlMargin = 0;
        if (this._slidesPerGroup > 1) {
            var wrapperRect = this._btnWrapper.getBoundingClientRect();
            var prevSlideCount = Math.floor(0.5 * this._slidesPerGroup);
            var rightIndex = this._sliderWrapper.index + prevSlideCount + 1;
            var leftIndex = this._sliderWrapper.index - 1;
            if (this._slidesPerGroup % 2 !== 0) {
                leftIndex -= prevSlideCount;
            }
            if ((leftIndex >= 0 && leftIndex < this._wrapper.children.length) &&
                (rightIndex >= 0 && rightIndex < this._wrapper.children.length)) {
                var leftSlide = this._sliderWrapper.getSlideProperties(leftIndex);
                var rightSlide = this._sliderWrapper.getSlideProperties(rightIndex);
                var btnWidth = this._prevCtrl.offsetWidth;
                if (btnWidth <= 0) {
                    btnWidth = 60;
                }
                prevCtrlMargin = leftSlide.right - wrapperRect.left - btnWidth;
                nextCtrlMargin = wrapperRect.right - rightSlide.left - btnWidth;
            }
        }
        var left = prevCtrlMargin !== 0 ? prevCtrlMargin + "px" : "";
        this._prevCtrl.style.left = left;
        var right = nextCtrlMargin !== 0 ? nextCtrlMargin + "px" : "";
        this._nextCtrl.style.right = right;
    };
    Carousel.prototype._updateActiveSlides = function (nextIndex) {
        var prevSlideCount = Math.floor(0.5 * (this._slidesPerGroup - 1));
        var evenGroup = this._slidesPerGroup % 2 === 0;
        for (var i = 0; i < this._wrapper.children.length; i++) {
            var slide = this._wrapper.children[i];
            if (i === nextIndex || (evenGroup && i === nextIndex + 1)) {
                Dom.addClass(slide, CLASS_ACTIVE);
            }
            else {
                Dom.removeClass(slide, CLASS_ACTIVE);
            }
            if (i < nextIndex && i >= nextIndex - prevSlideCount) {
                Dom.addClass(slide, CLASS_PREV);
            }
            else {
                Dom.removeClass(slide, CLASS_PREV);
            }
            if (i > nextIndex && (i <= nextIndex + prevSlideCount || (evenGroup && i <= nextIndex + 1 + prevSlideCount))) {
                Dom.addClass(slide, CLASS_NEXT);
            }
            else {
                Dom.removeClass(slide, CLASS_NEXT);
            }
        }
    };
    /**
     * Updates and creates the pagination bullets.
     * @private
     */
    Carousel.prototype._updatePagination = function () {
        if (!this._pagination) {
            return;
        }
        var to = this._index;
        var bullets = this._pagination.children;
        var totalItems = Math.max(this._slides.length, bullets.length);
        var slideCount = Math.ceil(this._slides.length / this._slidesPerGroup);
        var activeSlideIndex = Math.floor(to / this._slidesPerGroup);
        for (var i = 0; i < totalItems; i++) {
            var bullet = void 0;
            if (bullets.length > i) {
                if (bullets.length <= slideCount) {
                    bullet = bullets[i];
                }
                else {
                    remove(bullets[i]);
                }
            }
            else if (i < slideCount) {
                bullet = new DomElement("div")
                    .addClass(CLASS_BULLET)
                    .element;
                this._pagination.appendChild(bullet);
            }
            if (bullet && i < slideCount) {
                if (i === activeSlideIndex) {
                    Dom.addClass(bullet, CLASS_BULLET_ACTIVE);
                }
                else {
                    Dom.removeClass(bullet, CLASS_BULLET_ACTIVE);
                }
            }
        }
    };
    Carousel.prototype._handlePaginationClick = function (e) {
        if (!Dom.hasClass(e.target, CLASS_BULLET)) {
            return;
        }
        var index = Array.from(this._pagination.children).indexOf(e.target);
        var slideNumber = index * this._slidesPerGroup;
        this.slideTo(slideNumber);
    };
    Carousel.prototype._handleKeydown = function (event) {
        var keycode = event.which || event.keyCode;
        switch (keycode) {
            case Inputs.KEY_ARROW_LEFT:
                this.prev();
                break;
            case Inputs.KEY_ARROW_RIGHT:
                this.next();
                break;
            case Inputs.KEY_ESCAPE:
                this.element.blur();
                break;
            default:
        }
    };
    Carousel.prototype._onTouchstart = function (event) {
        var touch = event.touches ? event.touches[0] : event;
        this._slideArea.removeEventListener("mousedown", this._handleTouchstart);
        this._slideArea.removeEventListener("touchstart", this._handleTouchstart);
        this._sliderWrapper.beginDrag();
        var pageX = touch.pageX;
        this._touchOffset = {
            x: pageX,
            time: Date.now()
        };
        this._delta = {
            x: 0,
            lastMove: pageX
        };
        document.addEventListener("mousemove", this._handleTouchmove);
        document.addEventListener("touchmove", this._handleTouchmove);
        document.addEventListener("mouseup", this._handleTouchend);
        document.addEventListener("mouseleave", this._handleTouchend);
        document.addEventListener("touchend", this._handleTouchend);
    };
    Carousel.prototype._onTouchmove = function (event) {
        var touch = event.touches ? event.touches[0] : event;
        var pageX = touch.pageX;
        var deltaMove = pageX - this._delta.lastMove;
        this._delta = {
            x: pageX - this._touchOffset.x,
            lastMove: pageX
        };
        if (this._touchOffset) {
            preventDefault(event);
            this._sliderWrapper.move(deltaMove);
            this._cloneSlidesToFitWrapper(false, deltaMove);
        }
    };
    Carousel.prototype._onTouchend = function () {
        var duration = this._touchOffset ? Date.now() - this._touchOffset.time : undefined;
        var isValid = Number(duration) < TOUCH_DURATION &&
            Math.abs(this._delta.x) > TOUCH_DELTA_MIN ||
            Math.abs(this._delta.x) > this._frameWidth / 3;
        if (isValid) {
            var direction = clamp(this._delta.x, -1, 1) * -1;
            this.slide(false, direction, true);
            this._sliderWrapper.endDrag();
        }
        else {
            // Slide back to the starting point of the drag operation
            this._sliderWrapper.cancelDrag();
        }
        this._touchOffset = undefined;
        this._slideArea.addEventListener("mousedown", this._handleTouchstart);
        this._slideArea.addEventListener("touchstart", this._handleTouchstart);
        document.removeEventListener("mousemove", this._handleTouchmove);
        document.removeEventListener("mouseup", this._handleTouchend);
        document.removeEventListener("mouseleave", this._handleTouchend);
        document.removeEventListener("touchmove", this._handleTouchmove);
        document.removeEventListener("touchend", this._handleTouchend);
    };
    /**
     * Updated parameters in regard to the currently active responsive
     * breakpoint.
     * @private
     */
    Carousel.prototype._updateResponsiveOptions = function () {
        if (this._isBreakpointActive(this._breakpointPhone)) {
            this._slidesPerGroup = 1;
        }
        if (this._isBreakpointActive(this._breakpointTablet)) {
            this._slidesPerGroup = 2;
        }
        if (this._isBreakpointActive(this._breakpointDesktop)) {
            this._slidesPerGroup = 3;
        }
        this._sliderWrapper.slidesPerGroup = this._slidesPerGroup;
    };
    /**
     * Clones the requested slide and adds it to the slider.
     * @param {Number} index - The original slide index of the template slide
     * @param {Number} direction - The direction in which to add the slides, -1 for left, 1 for right
     * @private
     */
    Carousel.prototype._cloneSlide = function (index, direction) {
        var clone = this._slides[index].cloneNode(true);
        Dom.removeClass(clone, CLASS_ACTIVE);
        Dom.removeClass(clone, CLASS_PREV);
        Dom.removeClass(clone, CLASS_NEXT);
        this._sliderWrapper.addSlide(clone, direction);
        var slideMargin = this._additionalSlideMargin > 0 ? this._additionalSlideMargin + "px" : "";
        clone.style.marginLeft = slideMargin;
        clone.style.marginRight = slideMargin;
        return clone.offsetWidth;
    };
    /**
     * Clones and adds the requested ammount of slides.
     * @param {Number} slideCount - The number of slides to add
     * @param {Number} direction - The direction in which to add the slides, -1 for left, 1 for right
     * @private
     */
    Carousel.prototype._cloneSlidesByCount = function (slideCount, direction) {
        var originalIndex = direction < 0 ? 0 : this._wrapper.children.length - 1;
        var index = parseInt(this._wrapper.children[originalIndex].getAttribute(ATTRIBUTE_INDEX), 10);
        while (slideCount > 0) {
            index = this._wrapround(index + direction, 0, this._slides.length);
            this._cloneSlide(index, direction);
            slideCount--;
        }
    };
    /**
     * Calculates the scroll clount and inserts the required ammount of slides
     * in the apropriate direction.
     * @param {Number} nextIndex - The slide to scroll to
     * @param {Number} direction - The direction of the scroll
     * @private
     */
    Carousel.prototype._cloneSlidesByScrollCount = function (nextIndex, direction) {
        var scrollCount = this._wraproundCount(this._index, nextIndex, 0, this._slides.length, direction);
        var outerSlideProps = this._sliderWrapper.getSlideProperties(direction > 0 ? this._wrapper.children.length - 1 : 0);
        var indexToOuterSlideCount = this._wraproundCount(this._index, outerSlideProps.index, 0, this._slides.length, direction);
        var slidesToInsert = scrollCount - indexToOuterSlideCount;
        if (slidesToInsert > 0) {
            this._cloneSlidesByCount(slidesToInsert, direction);
        }
    };
    Carousel.prototype._cloneSlidesByToFill = function (spaceToFill, direction) {
        var originalIndex = direction < 0 ? 0 : this._wrapper.children.length - 1;
        var index = parseInt(this._wrapper.children[originalIndex].getAttribute(ATTRIBUTE_INDEX), 10);
        while (spaceToFill > 0) {
            index = this._wrapround(index + direction, 0, this._slides.length);
            spaceToFill -= this._cloneSlide(index, direction);
        }
    };
    Carousel.prototype._cloneSlidesToFitWrapper = function (cleanup, slideDelta) {
        if (cleanup === void 0) { cleanup = true; }
        if (slideDelta === void 0) { slideDelta = 0; }
        var realIndex = this._sliderWrapper.index;
        var first;
        var last;
        if (cleanup === false) {
            first = this._sliderWrapper.getSlideProperties(0);
            last = this._sliderWrapper.getSlideProperties(this._wrapper.children.length - 1);
        }
        else {
            var result = this._sliderWrapper.getRemovableSlides(slideDelta);
            first = result.first;
            last = result.last;
            // Remove the slides from view
            for (var i = result.slides.length - 1; i >= 0; i--) {
                if (result.slides[i] === true) {
                    this._sliderWrapper.removeSlide(i);
                }
            }
        }
        var spaceToFill = this._sliderWrapper.getEmptySpace(first.left, last.right);
        // Check if additional slides are required on the left
        if (first.visible === true && spaceToFill.left > 0) {
            this._cloneSlidesByToFill(spaceToFill.left, -1);
        }
        // Check if additional slides are required on the right
        if (last.visible === true && spaceToFill.right > 0) {
            this._cloneSlidesByToFill(spaceToFill.right, 1);
        }
        return realIndex - this._sliderWrapper.index;
    };
    /**
     * Gets the real (wrapper) index for the slide with the given original index
     * @param {Number} index - The index to search for
     * @param {Number} direction - The direction in which to search
     * @returns {Number} The wrapper index
     * @private
     */
    Carousel.prototype._getRealIndexFor = function (index, direction) {
        var i = this._sliderWrapper.index;
        while (i >= 0 && i < this._wrapper.children.length) {
            var slideIndex = parseInt(this._wrapper.children[i].getAttribute(ATTRIBUTE_INDEX), 10);
            if (slideIndex === index) {
                return i;
            }
            i += direction;
        }
        throw new Error("Cloud not find real index for slide " + index + " in direction " + direction);
    };
    Object.defineProperty(Carousel.prototype, "index", {
        /**
         * Gets the index of the current active slide. If the slides are grouped evenly
         * the active slide is always the first in the group.
         * @returns {Number} The index of the active slide.
         */
        get: function () {
            return this._index;
        },
        enumerable: true,
        configurable: true
    });
    Carousel.prototype.reset = function () {
        this._frameWidth = this._slider.getBoundingClientRect()
            .width || this._slider.offsetWidth;
        this._updateResponsiveOptions();
        if (this._nextCtrl) {
            this._nextCtrl.disabled = false;
        }
        if (this._prevCtrl) {
            this._prevCtrl.disabled = false;
        }
        if (this._slidesPerGroup === 1) {
            var style = window.getComputedStyle(this._slider.parentElement);
            var parentWidth = this._slider.parentElement.clientWidth + (parseFloat(style.marginLeft) || 0) + (parseFloat(style.marginRight) || 0);
            var outerMargin = Math.ceil(parentWidth - this._frameWidth);
            this._additionalSlideMargin = Math.ceil(outerMargin * 0.5) + 1;
        }
        else {
            this._additionalSlideMargin = 0;
        }
        var slideMargin = this._additionalSlideMargin > 0 ? this._additionalSlideMargin + "px" : "";
        for (var i = 0; i < this._wrapper.children.length; i++) {
            var slide = this._wrapper.children[i];
            slide.style.marginLeft = slideMargin;
            slide.style.marginRight = slideMargin;
        }
        this._sliderWrapper.onresize();
        this._cloneSlidesToFitWrapper(false);
        this._sliderWrapper.moveTo(this._sliderWrapper.index);
        this._updatePagination();
        this._updateActiveSlides(this._sliderWrapper.index);
    };
    /**
     * Moves the slider to the next item.
     */
    Carousel.prototype.prev = function () {
        this.slide(false, -1);
    };
    /**
     * Moves the slider to the previous item.
     */
    Carousel.prototype.next = function () {
        this.slide(false, 1);
    };
    Carousel.prototype.slide = function (nextIndex, direction, animate) {
        if (animate === void 0) { animate = true; }
        if (typeof nextIndex !== "number") {
            if (direction > 0) {
                nextIndex = this._index + this._slidesPerGroup;
                direction = 1;
            }
            else {
                nextIndex = this._index - this._slidesPerGroup;
                direction = -1;
            }
        }
        nextIndex = this._adjustIndex(nextIndex);
        if (!direction) {
            direction = clamp(nextIndex - this._index, -1, 1);
        }
        // Make sure there are enought slides on screen
        this._cloneSlidesToFitWrapper(false);
        // Make sure there are enough slides for the scroll operation
        this._cloneSlidesByScrollCount(nextIndex, direction);
        var realIndex = this._getRealIndexFor(nextIndex, direction);
        var slideDelta = this._sliderWrapper.getSlideDelta(realIndex);
        realIndex = Math.max(realIndex - this._cloneSlidesToFitWrapper(true, slideDelta), 0);
        this._sliderWrapper.moveTo(realIndex, undefined, animate);
        // Update the active index
        this._index = nextIndex;
        // Mark slides as active
        this._updatePagination();
        this._updateActiveSlides(realIndex);
        // console.log(`Performed slide to ${this._index}, realIndex: ${this._sliderWrapper.index}`)
    };
    /**
     * Moves the slider to the selected slide.
     * @param {Number} index - The index of the slide to slide to.
     * @param {Boolean} animate - `True` if the slide should be animated; otherwise `false`. Defaults to `true`.
     */
    Carousel.prototype.slideTo = function (index, animate) {
        if (animate === void 0) { animate = true; }
        this.slide(index, undefined, animate);
    };
    /**
     * Destroys the components and frees all references.
     */
    Carousel.prototype.destroy = function () {
        window.removeEventListener("resize", this._resizeHandler);
        window.removeEventListener("orientationchange", this._resizeHandler);
        this.element.removeEventListener("keydown", this._keydownHandler);
        this._slideArea.removeEventListener("mousedown", this._handleTouchstart);
        this._slideArea.removeEventListener("touchstart", this._handleTouchstart);
        this._breakpointPhone.remove();
        this._breakpointTablet.remove();
        this._breakpointDesktop.remove();
        if (this._prevCtrl && this._nextCtrl) {
            this._prevCtrl.removeEventListener("click", this._prevHandler);
            this._nextCtrl.removeEventListener("click", this._nextHandler);
        }
        this._prevCtrl = undefined;
        this._nextCtrl = undefined;
        if (this._pagination) {
            this._pagination.removeEventListener("click", this._paginationClickHandler);
            this._pagination = undefined;
        }
        this._sliderWrapper.destroy();
        this._sliderWrapper = undefined;
    };
    return Carousel;
}(DomElement));
var TRANSFORM = "transform";
var DURATION = "transitionDuration";
var TIMING = "transitionTimingFunction";
var SliderWrapper = /** @class */ (function () {
    function SliderWrapper(wrapperElement, slideAreaElement, carouselElement) {
        this._wrapperElement = wrapperElement;
        this._slideAreaElement = slideAreaElement;
        this._carouselElement = carouselElement;
        this._position = 0;
        this._index = 0;
        this._isdragging = false;
    }
    SliderWrapper.prototype._getSlide = function (index) {
        if (index < 0 || index >= this._wrapperElement.children.length) {
            throw new Error("Argument 'index' is out of range, Value: " + index + " Min: 0, Max: " + (this._wrapperElement.children.length - 1));
        }
        return this._wrapperElement.children[index];
    };
    SliderWrapper.prototype._setTransform = function (targetPosition, animated, duration, ease) {
        if (animated === void 0) { animated = false; }
        if (duration === void 0) { duration = ANIMATION_DURATION; }
        if (ease === void 0) { ease = ANIMATION_EASING; }
        if (animated === false) {
            duration = 0;
        }
        var style = this._wrapperElement.style;
        if (style) {
            style[DURATION] = duration + "ms";
            style[TIMING] = ease;
            // No sub pixel transitions.
            targetPosition = Math.floor(targetPosition);
            style[TRANSFORM] = "translate(" + targetPosition + "px, 0)";
            this._position = targetPosition;
        }
    };
    SliderWrapper.prototype._getWrapperSlidePosition = function (index) {
        var wrapperCenter = (0.5 * this._wrapperElement.offsetWidth);
        var slide = this._getSlide(index);
        var result = 0;
        // Calculate the position of the slide (centered)
        if (this._slidesPerGroup % 2 === 0) {
            var slideStyle = window.getComputedStyle(slide);
            var slideMargin = slideStyle ? parseInt(slideStyle.marginRight, 10) : 0;
            // Centered to the space between the two center slides of the group
            result = -slide.offsetLeft - (slide.clientWidth) + wrapperCenter - slideMargin;
        }
        else {
            result = -slide.offsetLeft - (0.5 * slide.clientWidth) + wrapperCenter;
        }
        return result;
    };
    Object.defineProperty(SliderWrapper.prototype, "position", {
        get: function () {
            return this._position;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SliderWrapper.prototype, "index", {
        get: function () {
            return this._index;
        },
        set: function (index) {
            this._index = index;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SliderWrapper.prototype, "slidesPerGroup", {
        set: function (value) {
            this._slidesPerGroup = value;
        },
        enumerable: true,
        configurable: true
    });
    SliderWrapper.prototype.initialize = function () {
        this.onresize();
    };
    SliderWrapper.prototype.onresize = function () {
        // update the area offset for slide position calculation
        this._areaOffset = this._slideAreaElement.getBoundingClientRect().left;
        // Get the container dimensions
        var containerRect = this._carouselElement.getBoundingClientRect();
        this._containerMin = containerRect.left;
        this._containerMax = containerRect.right;
    };
    SliderWrapper.prototype.beginDrag = function () {
        this._isdragging = true;
        this._dragStartPosition = this._position;
    };
    SliderWrapper.prototype.cancelDrag = function () {
        this._isdragging = false;
        this._setTransform(this._dragStartPosition, true, ANIMATION_DURATION, ANIMATION_EASING);
        this._dragStartPosition = undefined;
    };
    SliderWrapper.prototype.endDrag = function () {
        this._isdragging = false;
        this._dragStartPosition = undefined;
    };
    SliderWrapper.prototype.move = function (delta, animated, duration, ease) {
        if (animated === void 0) { animated = false; }
        if (duration === void 0) { duration = ANIMATION_DURATION; }
        if (ease === void 0) { ease = ANIMATION_EASING; }
        delta = Math.trunc(delta);
        if (Math.abs(delta) <= 0) {
            return;
        }
        var targetPosition = this._position += delta;
        this._setTransform(targetPosition, animated, duration, ease);
    };
    SliderWrapper.prototype.moveTo = function (index, delta, animated) {
        if (animated === void 0) { animated = false; }
        var newPosition = 0;
        if (!delta) {
            newPosition = this._getWrapperSlidePosition(index);
        }
        else {
            newPosition = this._position += delta;
        }
        this._index = index;
        this._setTransform(newPosition, animated);
    };
    SliderWrapper.prototype.addSlide = function (slide, position) {
        if (!slide) {
            throw new Error("Cannot add an undefined slide");
        }
        if (position !== -1 && position !== 1) {
            throw new Error("Argument out of range, 'position' must be either 1 or -1. Value " + position);
        }
        if (position > 0) {
            this._wrapperElement.appendChild(slide);
        }
        else {
            this._wrapperElement.insertBefore(slide, this._wrapperElement.children[0]);
            this._index++;
        }
        if (position < 0) {
            var width = slide.offsetWidth;
            var style = window.getComputedStyle(slide);
            var marginLeft = style ? parseInt(style.marginLeft, 10) : 0;
            var marginRight = style ? parseInt(style.marginRight, 10) : 0;
            this.move(-(width + marginLeft + marginRight));
        }
    };
    SliderWrapper.prototype.removeSlide = function (index) {
        var slide = this._getSlide(index);
        var width = slide.offsetWidth;
        if (index <= this._index) {
            width *= -1;
            this._index--;
        }
        remove(slide);
        if (width < 0) {
            this.move(-width);
        }
    };
    SliderWrapper.prototype.getSlideDelta = function (index) {
        var currentPosition = this._position;
        if (this._isdragging === true) {
            currentPosition = this._dragStartPosition - this._position;
        }
        var newPosition = this._getWrapperSlidePosition(index);
        return newPosition - currentPosition;
    };
    SliderWrapper.prototype.getSlideProperties = function (index, delta) {
        if (delta === void 0) { delta = 0; }
        var currentOffset = this._areaOffset + this._position + delta;
        var currentLeft = currentOffset;
        var currentRight = currentOffset;
        var _a = tslib_1.__read([0, 0], 2), currentMarginLeft = _a[0], currentMarginRight = _a[1];
        var slide = this._getSlide(index);
        var slideIndex = parseInt(slide.getAttribute(ATTRIBUTE_INDEX), 10);
        for (var i = 0; i <= index; i++) {
            slide = this._getSlide(i);
            var slideStyle = window.getComputedStyle(slide);
            currentMarginLeft = parseInt(slideStyle.marginLeft, 10);
            currentMarginRight = parseInt(slideStyle.marginRight, 10);
            currentOffset += currentMarginLeft;
            currentLeft = currentOffset;
            currentRight = currentLeft + slide.offsetWidth;
            if (i < index) {
                currentOffset = currentRight + currentMarginRight;
            }
        }
        var visible = false;
        if ((currentLeft > this._containerMin && currentLeft < this._containerMax) ||
            (currentRight > this._containerMin && currentRight < this._containerMax)) {
            visible = true;
        }
        return {
            visible: visible,
            index: slideIndex,
            left: currentLeft,
            right: currentRight,
            width: currentRight - currentLeft,
            marginLeft: currentMarginLeft,
            marginRight: currentMarginRight
        };
    };
    SliderWrapper.prototype.getRemovableSlides = function (delta) {
        var slides = [];
        var first;
        var last;
        var index = this._wrapperElement.children.length;
        while (index > 0) {
            index--;
            var propsNow = this.getSlideProperties(index);
            var propsNew = this.getSlideProperties(index, delta);
            if (index === this._wrapperElement.children.length - 1) {
                last = propsNew;
            }
            if (index === 0) {
                first = propsNew;
            }
            if (propsNow.visible === false && propsNew.visible === false &&
                index !== this._index && this._isdragging === false) {
                slides.push(true);
            }
            else {
                slides.push(false);
            }
        }
        slides.reverse();
        var firstToKeep = slides.indexOf(false);
        var lastToKeep = slides.lastIndexOf(false);
        for (var i = firstToKeep; i < lastToKeep; i++) {
            slides[i] = false;
        }
        return {
            slides: slides,
            first: first,
            last: last
        };
    };
    SliderWrapper.prototype.getEmptySpace = function (left, right) {
        return {
            left: Math.max(Math.ceil(left - this._containerMin), 0),
            right: Math.max(Math.ceil(this._containerMax - right), 0)
        };
    };
    SliderWrapper.prototype.destroy = function () {
        this._wrapperElement = null;
        this._slideAreaElement = null;
        this._carouselElement = null;
    };
    /**
     * @deprecated use destroy() instead.
     * @todo remove in version 2.0.0
     */
    SliderWrapper.prototype.destory = function () {
        this.destroy();
    };
    return SliderWrapper;
}());
export function init() {
    searchAndInitialize(".carousel", function (e) {
        new Carousel(e);
    });
}
export default Carousel;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2Nhcm91c2VsL0Nhcm91c2VsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsTUFBTSxVQUFVLENBQUE7QUFDN0UsT0FBTyxVQUFVLE1BQU0sZUFBZSxDQUFBO0FBQ3RDLE9BQU8sS0FBSyxNQUFNLE1BQU0sV0FBVyxDQUFBO0FBQ25DLE9BQU8sS0FBSyxHQUFHLE1BQU0saUJBQWlCLENBQUE7QUFFdEMsSUFBTSxZQUFZLEdBQUcsc0JBQXNCLENBQUE7QUFDM0MsSUFBTSxnQkFBZ0IsR0FBRyxtQkFBbUIsQ0FBQTtBQUM1QyxJQUFNLGFBQWEsR0FBRywyQkFBMkIsQ0FBQTtBQUVqRCxJQUFNLGdCQUFnQixHQUFHLHVCQUF1QixDQUFBO0FBRWhELElBQU0sWUFBWSxHQUFHLGVBQWUsQ0FBQTtBQUNwQyxJQUFNLFVBQVUsR0FBRyxhQUFhLENBQUE7QUFDaEMsSUFBTSxVQUFVLEdBQUcsYUFBYSxDQUFBO0FBRWhDLElBQU0sWUFBWSxHQUFHLG1CQUFtQixDQUFBO0FBQ3hDLElBQU0sbUJBQW1CLEdBQUcsMkJBQTJCLENBQUE7QUFFdkQsSUFBTSxjQUFjLEdBQUcsd0JBQXdCLENBQUE7QUFDL0MsSUFBTSxjQUFjLEdBQUcsd0JBQXdCLENBQUE7QUFDL0MsSUFBTSxpQkFBaUIsR0FBRywyQkFBMkIsQ0FBQTtBQUVyRCxJQUFNLGVBQWUsR0FBRyxVQUFVLENBQUE7QUFFbEMsSUFBTSxrQkFBa0IsR0FBRyxHQUFHLENBQUE7QUFDOUIsSUFBTSxnQkFBZ0IsR0FBRyxhQUFhLENBQUE7QUFFdEMsSUFBTSxjQUFjLEdBQUcsR0FBRyxDQUFBO0FBQzFCLElBQU0sZUFBZSxHQUFHLEVBQUUsQ0FBQTtBQWMxQjs7R0FFRztBQUNIO0lBQXVCLG9DQUF1QjtJQTZDNUM7Ozs7T0FJRztJQUNILGtCQUFZLE9BQW9CLEVBQUUsS0FBUztRQUFULHNCQUFBLEVBQUEsU0FBUztRQUEzQyxZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQWtDZjtRQWhDQyxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBaUIsQ0FBQTtRQUN2RSxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBaUIsQ0FBQTtRQUN6RSxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFnQixDQUFBO1FBQzlFLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQWlCLENBQUE7UUFFOUUsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBaUIsQ0FBQTtRQUNoRixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBaUIsQ0FBQTtRQUMzRSxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBaUIsQ0FBQTtRQUUzRSxLQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTtRQUVqQixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUE7UUFDeEIsS0FBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUE7UUFFeEIsS0FBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLGFBQWEsQ0FBQyxLQUFJLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQ3JGLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUE7UUFDdkMsS0FBSSxDQUFDLHNCQUFzQixHQUFHLENBQUMsQ0FBQTtRQUUvQixLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQy9DLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDeEMsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUN4QyxLQUFJLENBQUMsdUJBQXVCLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUNyRSxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBRXJELEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUN0RCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDcEQsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUVsRCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDbEIsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUVqQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTs7SUFDM0IsQ0FBQztJQUVEOzs7T0FHRztJQUNPLDhCQUFXLEdBQXJCO1FBQ0UscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLFVBQVUsQ0FBaUIsS0FBSyxDQUFDO2FBQzFELFFBQVEsQ0FBQyxVQUFVLENBQUM7YUFDcEIsT0FBTyxDQUFBO1FBRVYsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksVUFBVSxDQUFpQixLQUFLLENBQUM7YUFDM0QsUUFBUSxDQUFDLFdBQVcsQ0FBQzthQUNyQixPQUFPLENBQUE7UUFFVixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxVQUFVLENBQWlCLEtBQUssQ0FBQzthQUM1RCxRQUFRLENBQUMsWUFBWSxDQUFDO2FBQ3RCLE9BQU8sQ0FBQTtRQUVWLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBRWpELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUMzRCxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7U0FDNUQ7UUFFRCxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUE7U0FDekU7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQWtCLENBQUE7UUFDbEUsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDN0IsTUFBTSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQTtTQUN4RDtRQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQzNCLEtBQUssQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQy9DO1FBRUQsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUE7UUFDL0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtRQUVoQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7UUFFWixJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFFOUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFDckUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFFdEUsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7UUFDdEQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQTtJQUNuRSxDQUFDO0lBRVMsc0NBQW1CLEdBQTdCLFVBQThCLFVBQTBCO1FBQ3RELElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUMvQyxPQUFPLEtBQUssQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFBO0lBQ3ZDLENBQUM7SUFFUyw0QkFBUyxHQUFuQjtRQUNFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtRQUNaLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFBO0lBQzNCLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDTywrQkFBWSxHQUF0QixVQUF1QixLQUFhO1FBQ2xDLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLEtBQUssR0FBRyxDQUFDLENBQUE7U0FDVjtRQUVELElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtZQUNiLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUN2RDthQUFNLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO1lBQ3ZDLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQTtTQUM3QjtRQUVELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7SUFDeEUsQ0FBQztJQUVTLDZCQUFVLEdBQXBCLFVBQXFCLENBQVMsRUFBRSxHQUFXLEVBQUUsR0FBVztRQUN0RCxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUU7WUFDWixPQUFPLEdBQUcsQ0FBQTtTQUNYO1FBRUQsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFO1lBQ1gsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFBO1NBQ2Y7UUFFRCxPQUFPLENBQUMsQ0FBQTtJQUNWLENBQUM7SUFFUyxrQ0FBZSxHQUF6QixVQUEwQixDQUFTLEVBQUUsQ0FBUyxFQUFFLEdBQVcsRUFBRSxHQUFXLEVBQUUsU0FBb0I7UUFDNUYsSUFBSSxTQUFTLEtBQUssQ0FBQyxFQUFFO1lBQ25CLE9BQU8sQ0FBQyxDQUFBO1NBQ1Q7UUFFRCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRTtZQUN2QixNQUFNLElBQUksS0FBSyxDQUFDLDBDQUF3QyxDQUFDLGNBQVMsR0FBRyxlQUFVLEdBQUssQ0FBQyxDQUFBO1NBQ3RGO1FBRUQsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUU7WUFDdkIsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBd0MsQ0FBQyxjQUFTLEdBQUcsZUFBVSxHQUFLLENBQUMsQ0FBQTtTQUN0RjtRQUVELElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNULE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNkLENBQUMsRUFBRSxDQUFBO1lBQ0gsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLFNBQVMsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUE7U0FDN0M7UUFFRCxPQUFPLENBQUMsQ0FBQTtJQUNWLENBQUM7SUFFUyxxQ0FBa0IsR0FBNUI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQzNELE9BQU07U0FDUDtRQUVELElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQTtRQUN0QixJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUE7UUFFdEIsSUFBSSxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsRUFBRTtZQUM1QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixFQUFFLENBQUE7WUFFMUQsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1lBQzdELElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxHQUFHLGNBQWMsR0FBRyxDQUFDLENBQUE7WUFFakUsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO1lBQzdDLElBQUksSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNsQyxTQUFTLElBQUksY0FBYyxDQUFBO2FBQzVCO1lBRUQsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDL0QsQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDakUsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQTtnQkFDakUsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQTtnQkFFbkUsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUE7Z0JBQ3pDLElBQUksUUFBUSxJQUFJLENBQUMsRUFBRTtvQkFDakIsUUFBUSxHQUFHLEVBQUUsQ0FBQTtpQkFDZDtnQkFFRCxjQUFjLEdBQUcsU0FBUyxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQTtnQkFDOUQsY0FBYyxHQUFHLFdBQVcsQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUE7YUFDaEU7U0FDRjtRQUVELElBQUksSUFBSSxHQUFHLGNBQWMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFJLGNBQWMsT0FBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7UUFDNUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQTtRQUVoQyxJQUFJLEtBQUssR0FBRyxjQUFjLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBSSxjQUFjLE9BQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO1FBQzdELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7SUFDcEMsQ0FBQztJQUVTLHNDQUFtQixHQUE3QixVQUE4QixTQUFpQjtRQUM3QyxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNuRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFaEQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUVyQyxJQUFJLENBQUMsS0FBSyxTQUFTLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxLQUFLLFNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDekQsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUE7YUFDbEM7aUJBQU07Z0JBQ0wsR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUE7YUFDckM7WUFFRCxJQUFJLENBQUMsR0FBRyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsR0FBRyxjQUFjLEVBQUU7Z0JBQ3BELEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFBO2FBQ2hDO2lCQUFNO2dCQUNMLEdBQUcsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFBO2FBQ25DO1lBRUQsSUFBSSxDQUFDLEdBQUcsU0FBUyxJQUFJLENBQUMsQ0FBQyxJQUFJLFNBQVMsR0FBRyxjQUFjLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsR0FBRyxDQUFDLEdBQUcsY0FBYyxDQUFDLENBQUMsRUFBRTtnQkFDNUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUE7YUFDaEM7aUJBQU07Z0JBQ0wsR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUE7YUFDbkM7U0FDRjtJQUNILENBQUM7SUFFRDs7O09BR0c7SUFDTyxvQ0FBaUIsR0FBM0I7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixPQUFNO1NBQ1A7UUFFRCxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRXBCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFBO1FBQ3ZDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQzlELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ3RFLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBRTVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkMsSUFBSSxNQUFNLFNBQUEsQ0FBQTtZQUVWLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxVQUFVLEVBQUU7b0JBQ2hDLE1BQU0sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQ3BCO3FCQUFNO29CQUNMLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtpQkFDbkI7YUFDRjtpQkFBTSxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUU7Z0JBQ3pCLE1BQU0sR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7cUJBQzNCLFFBQVEsQ0FBQyxZQUFZLENBQUM7cUJBQ3RCLE9BQU8sQ0FBQTtnQkFDVixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTthQUNyQztZQUVELElBQUksTUFBTSxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxLQUFLLGdCQUFnQixFQUFFO29CQUMxQixHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO2lCQUMxQztxQkFBTTtvQkFDTCxHQUFHLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO2lCQUM3QzthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBRVMseUNBQXNCLEdBQWhDLFVBQWlDLENBQWE7UUFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQWlCLEVBQUUsWUFBWSxDQUFDLEVBQUU7WUFDcEQsT0FBTTtTQUNQO1FBRUQsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBaUIsQ0FBQyxDQUFBO1FBQy9FLElBQUksV0FBVyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFBO1FBRTlDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7SUFDM0IsQ0FBQztJQUVTLGlDQUFjLEdBQXhCLFVBQXlCLEtBQW9CO1FBQzNDLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQTtRQUUxQyxRQUFRLE9BQU8sRUFBRTtZQUNmLEtBQUssTUFBTSxDQUFDLGNBQWM7Z0JBQ3hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtnQkFDWCxNQUFLO1lBQ1AsS0FBSyxNQUFNLENBQUMsZUFBZTtnQkFDekIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO2dCQUNYLE1BQUs7WUFDUCxLQUFLLE1BQU0sQ0FBQyxVQUFVO2dCQUNwQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFBO2dCQUNuQixNQUFLO1lBQ1AsUUFBUTtTQUNUO0lBQ0gsQ0FBQztJQUVTLGdDQUFhLEdBQXZCLFVBQXdCLEtBQThCO1FBQ3BELElBQU0sS0FBSyxHQUFJLEtBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBRSxLQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBbUIsQ0FBQTtRQUVwRyxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtRQUN4RSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtRQUV6RSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxDQUFBO1FBQ3ZCLElBQUEsbUJBQUssQ0FBVTtRQUV2QixJQUFJLENBQUMsWUFBWSxHQUFHO1lBQ2xCLENBQUMsRUFBRSxLQUFLO1lBQ1IsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUU7U0FDakIsQ0FBQTtRQUVELElBQUksQ0FBQyxNQUFNLEdBQUc7WUFDWixDQUFDLEVBQUUsQ0FBQztZQUNKLFFBQVEsRUFBRSxLQUFLO1NBQ2hCLENBQUE7UUFFRCxRQUFRLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO1FBQzdELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFFN0QsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDMUQsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDN0QsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7SUFDN0QsQ0FBQztJQUVTLCtCQUFZLEdBQXRCLFVBQXVCLEtBQThCO1FBQ25ELElBQU0sS0FBSyxHQUFJLEtBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBRSxLQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBbUIsQ0FBQTtRQUM1RixJQUFBLG1CQUFLLENBQVU7UUFFdkIsSUFBSSxTQUFTLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFPLENBQUMsUUFBUSxDQUFBO1FBRTdDLElBQUksQ0FBQyxNQUFNLEdBQUc7WUFDWixDQUFDLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFhLENBQUMsQ0FBQztZQUMvQixRQUFRLEVBQUUsS0FBSztTQUNoQixDQUFBO1FBRUQsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUVyQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtZQUNuQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFBO1NBQ2hEO0lBQ0gsQ0FBQztJQUVTLDhCQUFXLEdBQXJCO1FBQ0UsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUE7UUFFcEYsSUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLGNBQWM7WUFDL0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLGVBQWU7WUFDMUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFZLEdBQUcsQ0FBQyxDQUFBO1FBRWxELElBQUksT0FBTyxFQUFFO1lBQ1gsSUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBYyxDQUFBO1lBQ2hFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUVsQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFBO1NBQzlCO2FBQU07WUFDTCx5REFBeUQ7WUFDekQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtTQUNqQztRQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFBO1FBRTdCLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBQ3JFLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBRXRFLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDaEUsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDN0QsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDaEUsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUNoRSxRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtJQUNoRSxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNPLDJDQUF3QixHQUFsQztRQUNFLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO1lBQ25ELElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFBO1NBQ3pCO1FBRUQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDcEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUE7U0FDekI7UUFFRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsRUFBRTtZQUNyRCxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQTtTQUN6QjtRQUVELElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7SUFDM0QsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ08sOEJBQVcsR0FBckIsVUFBc0IsS0FBYSxFQUFFLFNBQWlCO1FBQ3BELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBZ0IsQ0FBQTtRQUM5RCxHQUFHLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQTtRQUNwQyxHQUFHLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQTtRQUNsQyxHQUFHLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQTtRQUVsQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUE7UUFFOUMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUksSUFBSSxDQUFDLHNCQUFzQixPQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQTtRQUMzRixLQUFLLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUE7UUFDcEMsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFBO1FBRXJDLE9BQU8sS0FBSyxDQUFDLFdBQVcsQ0FBQTtJQUMxQixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDTyxzQ0FBbUIsR0FBN0IsVUFBOEIsVUFBa0IsRUFBRSxTQUFvQjtRQUNwRSxJQUFJLGFBQWEsR0FBRyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUE7UUFDekUsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQTtRQUU5RixPQUFPLFVBQVUsR0FBRyxDQUFDLEVBQUU7WUFDckIsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLFNBQVMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUNsRSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQTtZQUNsQyxVQUFVLEVBQUUsQ0FBQTtTQUNiO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNPLDRDQUF5QixHQUFuQyxVQUFvQyxTQUFpQixFQUFFLFNBQW9CO1FBQ3pFLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFBO1FBRW5HLElBQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDckgsSUFBTSxzQkFBc0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUE7UUFFMUgsSUFBTSxjQUFjLEdBQUcsV0FBVyxHQUFHLHNCQUFzQixDQUFBO1FBQzNELElBQUksY0FBYyxHQUFHLENBQUMsRUFBRTtZQUN0QixJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxFQUFFLFNBQVMsQ0FBQyxDQUFBO1NBQ3BEO0lBQ0gsQ0FBQztJQUVTLHVDQUFvQixHQUE5QixVQUErQixXQUFtQixFQUFFLFNBQW9CO1FBQ3RFLElBQUksYUFBYSxHQUFHLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQTtRQUN6RSxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBRSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBRTlGLE9BQU8sV0FBVyxHQUFHLENBQUMsRUFBRTtZQUN0QixLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEdBQUcsU0FBUyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ2xFLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQTtTQUNsRDtJQUNILENBQUM7SUFFUywyQ0FBd0IsR0FBbEMsVUFBbUMsT0FBYyxFQUFFLFVBQWM7UUFBOUIsd0JBQUEsRUFBQSxjQUFjO1FBQUUsMkJBQUEsRUFBQSxjQUFjO1FBQy9ELElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFBO1FBQzNDLElBQUksS0FBc0IsQ0FBQTtRQUMxQixJQUFJLElBQXFCLENBQUE7UUFFekIsSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO1lBQ3JCLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ2pELElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQTtTQUNqRjthQUFNO1lBQ0wsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQTtZQUMvRCxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQU0sQ0FBQTtZQUNyQixJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUssQ0FBQTtZQUVuQiw4QkFBOEI7WUFDOUIsS0FBSyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbEQsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBRTtvQkFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQ25DO2FBQ0Y7U0FDRjtRQUVELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRTNFLHNEQUFzRDtRQUN0RCxJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssSUFBSSxJQUFJLFdBQVcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO1lBQ2xELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDaEQ7UUFFRCx1REFBdUQ7UUFDdkQsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxXQUFXLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRTtZQUNsRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQTtTQUNoRDtRQUVELE9BQU8sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFBO0lBQzlDLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDTyxtQ0FBZ0IsR0FBMUIsVUFBMkIsS0FBYSxFQUFFLFNBQW9CO1FBQzVELElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFBO1FBQ2pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO1lBQ2xELElBQUksVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFFLEVBQUUsRUFBRSxDQUFDLENBQUE7WUFDdkYsSUFBSSxVQUFVLEtBQUssS0FBSyxFQUFFO2dCQUN4QixPQUFPLENBQUMsQ0FBQTthQUNUO1lBRUQsQ0FBQyxJQUFJLFNBQVMsQ0FBQTtTQUNmO1FBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyx5Q0FBdUMsS0FBSyxzQkFBaUIsU0FBVyxDQUFDLENBQUE7SUFDM0YsQ0FBQztJQU9ELHNCQUFJLDJCQUFLO1FBTFQ7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBQ3BCLENBQUM7OztPQUFBO0lBRUQsd0JBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRTthQUNwRCxLQUFLLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUE7UUFFcEMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUE7UUFFL0IsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxTQUFpQixDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7U0FDekM7UUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDakIsSUFBSSxDQUFDLFNBQWlCLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTtTQUN6QztRQUVELElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxDQUFDLEVBQUU7WUFDOUIsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYyxDQUFDLENBQUE7WUFDaEUsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFjLENBQUMsV0FBVyxHQUFHLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxVQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsV0FBWSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7WUFFeEksSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1lBQzNELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7U0FDL0Q7YUFBTTtZQUNMLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxDQUFDLENBQUE7U0FDaEM7UUFFRCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBSSxJQUFJLENBQUMsc0JBQXNCLE9BQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO1FBQzNGLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdEQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFnQixDQUFBO1lBQ3BELEtBQUssQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQTtZQUNwQyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUE7U0FDdEM7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFBO1FBQzlCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNwQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRXJELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFBO0lBQ3JELENBQUM7SUFFRDs7T0FFRztJQUNILHVCQUFJLEdBQUo7UUFDRSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3ZCLENBQUM7SUFFRDs7T0FFRztJQUNILHVCQUFJLEdBQUo7UUFDRSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQTtJQUN0QixDQUFDO0lBRUQsd0JBQUssR0FBTCxVQUFNLFNBQXlCLEVBQUUsU0FBcUIsRUFBRSxPQUFjO1FBQWQsd0JBQUEsRUFBQSxjQUFjO1FBQ3BFLElBQUksT0FBTyxTQUFTLEtBQUssUUFBUSxFQUFFO1lBQ2pDLElBQUksU0FBVSxHQUFHLENBQUMsRUFBRTtnQkFDbEIsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQTtnQkFDOUMsU0FBUyxHQUFHLENBQUMsQ0FBQTthQUNkO2lCQUFNO2dCQUNMLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7Z0JBQzlDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQTthQUNmO1NBQ0Y7UUFFRCxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUV4QyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2QsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQWMsQ0FBQTtTQUMvRDtRQUVELCtDQUErQztRQUMvQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFcEMsNkRBQTZEO1FBQzdELElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUE7UUFFcEQsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQTtRQUMzRCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUM3RCxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUVwRixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBRXpELDBCQUEwQjtRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQTtRQUV2Qix3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7UUFDeEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBRW5DLDRGQUE0RjtJQUM5RixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILDBCQUFPLEdBQVAsVUFBUSxLQUFhLEVBQUUsT0FBYztRQUFkLHdCQUFBLEVBQUEsY0FBYztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUE7SUFDdkMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsMEJBQU8sR0FBUDtRQUNFLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3pELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFcEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ2pFLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBQ3hFLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBRXpFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtRQUM5QixJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLENBQUE7UUFDL0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxDQUFBO1FBRWhDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUM5RCxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7U0FDL0Q7UUFFQSxJQUFZLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUNuQyxJQUFZLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTtRQUVuQyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDM0UsSUFBWSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUE7U0FDdEM7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzdCLElBQVksQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFBO0lBQzFDLENBQUM7SUFDSCxlQUFDO0FBQUQsQ0Fsc0JBLEFBa3NCQyxDQWxzQnNCLFVBQVUsR0Frc0JoQztBQUVELElBQU0sU0FBUyxHQUFHLFdBQVcsQ0FBQTtBQUM3QixJQUFNLFFBQVEsR0FBRyxvQkFBb0IsQ0FBQTtBQUNyQyxJQUFNLE1BQU0sR0FBRywwQkFBMEIsQ0FBQTtBQUV6QztJQWtCRSx1QkFBWSxjQUEyQixFQUFFLGdCQUE2QixFQUFFLGVBQTRCO1FBQ2xHLElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFBO1FBQ3JDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxnQkFBZ0IsQ0FBQTtRQUN6QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFBO1FBRXZDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBO1FBQ2YsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUE7SUFDMUIsQ0FBQztJQUVTLGlDQUFTLEdBQW5CLFVBQW9CLEtBQWE7UUFDL0IsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUU7WUFDOUQsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBNEMsS0FBSyx1QkFBaUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBRSxDQUFDLENBQUE7U0FDOUg7UUFFRCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBZ0IsQ0FBQTtJQUM1RCxDQUFDO0lBRVMscUNBQWEsR0FBdkIsVUFBd0IsY0FBc0IsRUFBRSxRQUFnQixFQUFFLFFBQTZCLEVBQUUsSUFBdUI7UUFBeEUseUJBQUEsRUFBQSxnQkFBZ0I7UUFBRSx5QkFBQSxFQUFBLDZCQUE2QjtRQUFFLHFCQUFBLEVBQUEsdUJBQXVCO1FBQ3RILElBQUksUUFBUSxLQUFLLEtBQUssRUFBRTtZQUN0QixRQUFRLEdBQUcsQ0FBQyxDQUFBO1NBQ2I7UUFFRCxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQTtRQUN4QyxJQUFJLEtBQUssRUFBRTtZQUNULEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBTSxRQUFRLE9BQUksQ0FBQTtZQUNqQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFBO1lBRXBCLDRCQUE0QjtZQUM1QixjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQTtZQUUzQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsZUFBYSxjQUFjLFdBQVEsQ0FBQTtZQUN0RCxJQUFJLENBQUMsU0FBUyxHQUFHLGNBQWMsQ0FBQTtTQUNoQztJQUNILENBQUM7SUFFUyxnREFBd0IsR0FBbEMsVUFBbUMsS0FBYTtRQUM5QyxJQUFNLGFBQWEsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBQzlELElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFbkMsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFBO1FBQ2QsaURBQWlEO1FBQ2pELElBQUksSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2xDLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUMvQyxJQUFJLFdBQVcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDeEUsbUVBQW1FO1lBQ25FLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEdBQUcsYUFBYSxHQUFHLFdBQVcsQ0FBQTtTQUMvRTthQUFNO1lBQ0wsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLEdBQUcsYUFBYSxDQUFBO1NBQ3ZFO1FBRUQsT0FBTyxNQUFNLENBQUE7SUFDZixDQUFDO0lBRUQsc0JBQUksbUNBQVE7YUFBWjtZQUNFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQTtRQUN2QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdDQUFLO2FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUE7UUFDcEIsQ0FBQzthQUVELFVBQVUsS0FBYTtZQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQTtRQUNyQixDQUFDOzs7T0FKQTtJQU1ELHNCQUFJLHlDQUFjO2FBQWxCLFVBQW1CLEtBQWE7WUFDOUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUE7UUFDOUIsQ0FBQzs7O09BQUE7SUFFRCxrQ0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBQ2pCLENBQUM7SUFFRCxnQ0FBUSxHQUFSO1FBQ0Usd0RBQXdEO1FBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxDQUFBO1FBRXRFLCtCQUErQjtRQUMvQixJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLEVBQUUsQ0FBQTtRQUNuRSxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUE7UUFDdkMsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFBO0lBQzFDLENBQUM7SUFFRCxpQ0FBUyxHQUFUO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7UUFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUE7SUFDMUMsQ0FBQztJQUVELGtDQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQTtRQUN4QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxrQkFBbUIsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTtRQUV4RixJQUFJLENBQUMsa0JBQWtCLEdBQUcsU0FBUyxDQUFBO0lBQ3JDLENBQUM7SUFFRCwrQkFBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUE7UUFDeEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFNBQVMsQ0FBQTtJQUNyQyxDQUFDO0lBRUQsNEJBQUksR0FBSixVQUFLLEtBQWEsRUFBRSxRQUFnQixFQUFFLFFBQTZCLEVBQUUsSUFBdUI7UUFBeEUseUJBQUEsRUFBQSxnQkFBZ0I7UUFBRSx5QkFBQSxFQUFBLDZCQUE2QjtRQUFFLHFCQUFBLEVBQUEsdUJBQXVCO1FBQzFGLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3pCLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDeEIsT0FBTTtTQUNQO1FBRUQsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLENBQUE7UUFDNUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUM5RCxDQUFDO0lBRUQsOEJBQU0sR0FBTixVQUFPLEtBQWEsRUFBRSxLQUFjLEVBQUUsUUFBZ0I7UUFBaEIseUJBQUEsRUFBQSxnQkFBZ0I7UUFDcEQsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFBO1FBQ25CLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDVixXQUFXLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ25EO2FBQU07WUFDTCxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxLQUFLLENBQUE7U0FDdEM7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQTtRQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQTtJQUMzQyxDQUFDO0lBRUQsZ0NBQVEsR0FBUixVQUFTLEtBQWtCLEVBQUUsUUFBZ0I7UUFDM0MsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQTtTQUNqRDtRQUVELElBQUksUUFBUSxLQUFLLENBQUMsQ0FBQyxJQUFJLFFBQVEsS0FBSyxDQUFDLEVBQUU7WUFDckMsTUFBTSxJQUFJLEtBQUssQ0FBQyxxRUFBbUUsUUFBVSxDQUFDLENBQUE7U0FDL0Y7UUFFRCxJQUFJLFFBQVEsR0FBRyxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDeEM7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQzFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNkO1FBRUQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxFQUFFO1lBQ2hCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUE7WUFFN0IsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQzFDLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUM1RCxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFOUQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLFVBQVUsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFBO1NBQy9DO0lBQ0gsQ0FBQztJQUVELG1DQUFXLEdBQVgsVUFBWSxLQUFhO1FBQ3ZCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDbkMsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQTtRQUU3QixJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3hCLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQTtZQUNYLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNkO1FBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRWIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ2xCO0lBQ0gsQ0FBQztJQUVELHFDQUFhLEdBQWIsVUFBYyxLQUFhO1FBQ3pCLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUE7UUFDcEMsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLElBQUksRUFBRTtZQUM3QixlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFtQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUE7U0FDNUQ7UUFFRCxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDeEQsT0FBTyxXQUFXLEdBQUcsZUFBZSxDQUFBO0lBQ3RDLENBQUM7SUFFRCwwQ0FBa0IsR0FBbEIsVUFBbUIsS0FBYSxFQUFFLEtBQVM7UUFBVCxzQkFBQSxFQUFBLFNBQVM7UUFDekMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtRQUM5RCxJQUFJLFdBQVcsR0FBRyxhQUFhLENBQUE7UUFDL0IsSUFBSSxZQUFZLEdBQUcsYUFBYSxDQUFBO1FBQzVCLElBQUEsOEJBQW9ELEVBQWxELHlCQUFpQixFQUFFLDBCQUFrQixDQUFhO1FBRXhELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDakMsSUFBSSxVQUFVLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFFLEVBQUUsRUFBRSxDQUFDLENBQUE7UUFFbkUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQixLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN6QixJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUE7WUFFL0MsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFXLEVBQUUsRUFBRSxDQUFDLENBQUE7WUFDeEQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFZLEVBQUUsRUFBRSxDQUFDLENBQUE7WUFFMUQsYUFBYSxJQUFJLGlCQUFpQixDQUFBO1lBQ2xDLFdBQVcsR0FBRyxhQUFhLENBQUE7WUFDM0IsWUFBWSxHQUFHLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFBO1lBRTlDLElBQUksQ0FBQyxHQUFHLEtBQUssRUFBRTtnQkFDYixhQUFhLEdBQUcsWUFBWSxHQUFHLGtCQUFrQixDQUFBO2FBQ2xEO1NBQ0Y7UUFFRCxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUE7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ3hFLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUMxRSxPQUFPLEdBQUcsSUFBSSxDQUFBO1NBQ2Y7UUFFRCxPQUFPO1lBQ0wsT0FBTyxTQUFBO1lBQ1AsS0FBSyxFQUFFLFVBQVU7WUFDakIsSUFBSSxFQUFFLFdBQVc7WUFDakIsS0FBSyxFQUFFLFlBQVk7WUFDbkIsS0FBSyxFQUFFLFlBQVksR0FBRyxXQUFXO1lBQ2pDLFVBQVUsRUFBRSxpQkFBaUI7WUFDN0IsV0FBVyxFQUFFLGtCQUFrQjtTQUNoQyxDQUFBO0lBQ0gsQ0FBQztJQUVELDBDQUFrQixHQUFsQixVQUFtQixLQUFhO1FBQzlCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQTtRQUNmLElBQUksS0FBa0MsQ0FBQTtRQUN0QyxJQUFJLElBQWlDLENBQUE7UUFFckMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFBO1FBQ2hELE9BQU8sS0FBSyxHQUFHLENBQUMsRUFBRTtZQUNoQixLQUFLLEVBQUUsQ0FBQTtZQUVQLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUM3QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFBO1lBRXBELElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3RELElBQUksR0FBRyxRQUFRLENBQUE7YUFDaEI7WUFFRCxJQUFJLEtBQUssS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsS0FBSyxHQUFHLFFBQVEsQ0FBQTthQUNqQjtZQUVELElBQUksUUFBUSxDQUFDLE9BQU8sS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLE9BQU8sS0FBSyxLQUFLO2dCQUMxRCxLQUFLLEtBQUssSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLEtBQUssRUFBRTtnQkFDckQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUNsQjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO2FBQ25CO1NBQ0Y7UUFFRCxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUE7UUFFaEIsSUFBSSxXQUFXLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUN2QyxJQUFJLFVBQVUsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRTFDLEtBQUssSUFBSSxDQUFDLEdBQUcsV0FBVyxFQUFFLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDN0MsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQTtTQUNsQjtRQUVELE9BQU87WUFDTCxNQUFNLFFBQUE7WUFDTixLQUFLLEVBQUUsS0FBd0I7WUFDL0IsSUFBSSxFQUFFLElBQXVCO1NBQzlCLENBQUE7SUFDSCxDQUFDO0lBRUQscUNBQWEsR0FBYixVQUFjLElBQVksRUFBRSxLQUFhO1FBQ3ZDLE9BQU87WUFDTCxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZELEtBQUssRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDMUQsQ0FBQTtJQUNILENBQUM7SUFFRCwrQkFBTyxHQUFQO1FBQ0csSUFBWSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBWSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUN0QyxJQUFZLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFBO0lBQ3ZDLENBQUM7SUFFRDs7O09BR0c7SUFDSCwrQkFBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO0lBQ2hCLENBQUM7SUFDSCxvQkFBQztBQUFELENBNVNBLEFBNFNDLElBQUE7QUFFRCxNQUFNLFVBQVUsSUFBSTtJQUNsQixtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsVUFBQyxDQUFDO1FBQ2pDLElBQUksUUFBUSxDQUFDLENBQWdCLENBQUMsQ0FBQTtJQUNoQyxDQUFDLENBQUMsQ0FBQTtBQUNKLENBQUM7QUFFRCxlQUFlLFFBQVEsQ0FBQSIsImZpbGUiOiJtYWluL3NyYy9jYXJvdXNlbC9DYXJvdXNlbC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHNlYXJjaEFuZEluaXRpYWxpemUsIGNsYW1wLCBwcmV2ZW50RGVmYXVsdCwgcmVtb3ZlIH0gZnJvbSBcIi4uL1V0aWxzXCJcbmltcG9ydCBEb21FbGVtZW50IGZyb20gXCIuLi9Eb21FbGVtZW50XCJcbmltcG9ydCAqIGFzIElucHV0cyBmcm9tIFwiLi4vSW5wdXRzXCJcbmltcG9ydCAqIGFzIERvbSBmcm9tIFwiLi4vRG9tRnVuY3Rpb25zXCJcblxuY29uc3QgUVVFUllfU0xJREVSID0gXCIuY2Fyb3VzZWxfX2NvbnRhaW5lclwiXG5jb25zdCBRVUVSWV9TTElERV9BUkVBID0gXCIuY2Fyb3VzZWxfX3NsaWRlclwiXG5jb25zdCBRVUVSWV9XUkFQUEVSID0gXCIuY2Fyb3VzZWxfX3NsaWRlci13cmFwcGVyXCJcblxuY29uc3QgUVVFUllfUEFHSU5BVElPTiA9IFwiLmNhcm91c2VsX19wYWdpbmF0aW9uXCJcblxuY29uc3QgQ0xBU1NfQUNUSVZFID0gXCJzbGlkZS0tYWN0aXZlXCJcbmNvbnN0IENMQVNTX1BSRVYgPSBcInNsaWRlLS1wcmV2XCJcbmNvbnN0IENMQVNTX05FWFQgPSBcInNsaWRlLS1uZXh0XCJcblxuY29uc3QgQ0xBU1NfQlVMTEVUID0gXCJwYWdpbmF0aW9uLWJ1bGxldFwiXG5jb25zdCBDTEFTU19CVUxMRVRfQUNUSVZFID0gXCJwYWdpbmF0aW9uLWJ1bGxldC0tYWN0aXZlXCJcblxuY29uc3QgUVVFUllfQlROX1BSRVYgPSBcIi5jYXJvdXNlbF9fYnV0dG9uLXByZXZcIlxuY29uc3QgUVVFUllfQlROX05FWFQgPSBcIi5jYXJvdXNlbF9fYnV0dG9uLW5leHRcIlxuY29uc3QgUVVFUllfQlROX1dSQVBQRVIgPSBcIi5jYXJvdXNlbF9fYnV0dG9uLXdyYXBwZXJcIlxuXG5jb25zdCBBVFRSSUJVVEVfSU5ERVggPSBcImpzLWluZGV4XCJcblxuY29uc3QgQU5JTUFUSU9OX0RVUkFUSU9OID0gMzUwXG5jb25zdCBBTklNQVRJT05fRUFTSU5HID0gXCJlYXNlLWluLW91dFwiXG5cbmNvbnN0IFRPVUNIX0RVUkFUSU9OID0gMzAwXG5jb25zdCBUT1VDSF9ERUxUQV9NSU4gPSAyNVxuXG5leHBvcnQgaW50ZXJmYWNlIFNsaWRlUHJvcGVydGllcyB7XG4gIHJpZ2h0OiBudW1iZXJcbiAgbGVmdDogbnVtYmVyXG4gIHZpc2libGU6IGJvb2xlYW5cbiAgaW5kZXg6IG51bWJlclxuICB3aWR0aDogbnVtYmVyXG4gIG1hcmdpbkxlZnQ6IG51bWJlclxuICBtYXJnaW5SaWdodDogbnVtYmVyXG59XG5cbmV4cG9ydCB0eXBlIERpcmVjdGlvbiA9IDAgfCAtMSB8IDFcblxuLyoqXG4gKiBUaGUgY2Fyb3VzZWwgY29tcG9uZW50IGRlZmluaXRpb24uXG4gKi9cbmNsYXNzIENhcm91c2VsIGV4dGVuZHMgRG9tRWxlbWVudDxIVE1MRWxlbWVudD4ge1xuICBwcml2YXRlIF9zbGlkZXI6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX3dyYXBwZXI6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX3BhZ2luYXRpb24/OiBIVE1MRWxlbWVudFxuICBwcml2YXRlIF9zbGlkZUFyZWE6IEhUTUxFbGVtZW50XG5cbiAgcHJpdmF0ZSBfYnRuV3JhcHBlcjogSFRNTEVsZW1lbnRcbiAgcHJpdmF0ZSBfcHJldkN0cmw6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX25leHRDdHJsOiBIVE1MRWxlbWVudFxuXG4gIHByaXZhdGUgX3NsaWRlczogSFRNTEVsZW1lbnRbXVxuXG4gIHByaXZhdGUgX2luZGV4OiBudW1iZXJcbiAgcHJpdmF0ZSBfc2xpZGVzUGVyR3JvdXA6IG51bWJlclxuXG4gIHByaXZhdGUgX3NsaWRlcldyYXBwZXI6IFNsaWRlcldyYXBwZXJcblxuICBwcml2YXRlIF9hZGRpdGlvbmFsU2xpZGVNYXJnaW46IG51bWJlclxuXG4gIHByaXZhdGUgX3Jlc2l6ZUhhbmRsZXI6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfcHJldkhhbmRsZXI6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfbmV4dEhhbmRsZXI6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfcGFnaW5hdGlvbkNsaWNrSGFuZGxlcjogKGV2ZW50OiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9rZXlkb3duSGFuZGxlcjogKGV2ZW50OiBFdmVudCkgPT4gdm9pZFxuXG4gIHByaXZhdGUgX2hhbmRsZVRvdWNoc3RhcnQ6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfaGFuZGxlVG91Y2htb3ZlOiAoZXZlbnQ6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2hhbmRsZVRvdWNoZW5kOiAoZXZlbnQ6IEV2ZW50KSA9PiB2b2lkXG5cbiAgcHJpdmF0ZSBfYnJlYWtwb2ludFBob25lITogSFRNTERpdkVsZW1lbnRcbiAgcHJpdmF0ZSBfYnJlYWtwb2ludFRhYmxldCE6IEhUTUxEaXZFbGVtZW50XG4gIHByaXZhdGUgX2JyZWFrcG9pbnREZXNrdG9wITogSFRNTERpdkVsZW1lbnRcblxuICBwcml2YXRlIF90b3VjaE9mZnNldD86IHtcbiAgICB4OiBudW1iZXI7XG4gICAgdGltZTogbnVtYmVyO1xuICB9XG5cbiAgcHJpdmF0ZSBfZGVsdGE/OiB7XG4gICAgeDogbnVtYmVyO1xuICAgIGxhc3RNb3ZlOiBudW1iZXI7XG4gIH1cblxuICBwcml2YXRlIF9mcmFtZVdpZHRoPzogbnVtYmVyXG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYW5kIGluaXRpYWxpemVzIHRoZSBjYXJvdXNlbCBjb21wb25lbnQuXG4gICAqIEBwYXJhbSB7RG9tRWxlbWVudH0gZWxlbWVudCAtIFRoZSByb290IGVsZW1lbnQgb2YgdGhlIENhcm91c2VsIGNvbXBvbmVudC5cbiAgICogQHBhcmFtIHtOdW1iZXJ9IGluZGV4IC0gVGhlIGluaXRpYWwgaW5kZXguXG4gICAqL1xuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBIVE1MRWxlbWVudCwgaW5kZXggPSAwKSB7XG4gICAgc3VwZXIoZWxlbWVudClcblxuICAgIHRoaXMuX3NsaWRlciA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX1NMSURFUikhIGFzIEhUTUxFbGVtZW50XG4gICAgdGhpcy5fd3JhcHBlciA9IHRoaXMuX3NsaWRlci5xdWVyeVNlbGVjdG9yKFFVRVJZX1dSQVBQRVIpISBhcyBIVE1MRWxlbWVudFxuICAgIHRoaXMuX3BhZ2luYXRpb24gPSB0aGlzLl9zbGlkZXIucXVlcnlTZWxlY3RvcihRVUVSWV9QQUdJTkFUSU9OKSBhcyBIVE1MRWxlbWVudFxuICAgIHRoaXMuX3NsaWRlQXJlYSA9IHRoaXMuX3NsaWRlci5xdWVyeVNlbGVjdG9yKFFVRVJZX1NMSURFX0FSRUEpISBhcyBIVE1MRWxlbWVudFxuXG4gICAgdGhpcy5fYnRuV3JhcHBlciA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX0JUTl9XUkFQUEVSKSEgYXMgSFRNTEVsZW1lbnRcbiAgICB0aGlzLl9wcmV2Q3RybCA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX0JUTl9QUkVWKSEgYXMgSFRNTEVsZW1lbnRcbiAgICB0aGlzLl9uZXh0Q3RybCA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX0JUTl9ORVhUKSEgYXMgSFRNTEVsZW1lbnRcblxuICAgIHRoaXMuX3NsaWRlcyA9IFtdXG5cbiAgICB0aGlzLl9pbmRleCA9IGluZGV4IHx8IDBcbiAgICB0aGlzLl9zbGlkZXNQZXJHcm91cCA9IDFcblxuICAgIHRoaXMuX3NsaWRlcldyYXBwZXIgPSBuZXcgU2xpZGVyV3JhcHBlcih0aGlzLl93cmFwcGVyLCB0aGlzLl9zbGlkZUFyZWEsIHRoaXMuZWxlbWVudClcbiAgICB0aGlzLl9zbGlkZXJXcmFwcGVyLmluZGV4ID0gdGhpcy5faW5kZXhcbiAgICB0aGlzLl9hZGRpdGlvbmFsU2xpZGVNYXJnaW4gPSAwXG5cbiAgICB0aGlzLl9yZXNpemVIYW5kbGVyID0gdGhpcy5fb25yZXNpemUuYmluZCh0aGlzKVxuICAgIHRoaXMuX3ByZXZIYW5kbGVyID0gdGhpcy5wcmV2LmJpbmQodGhpcylcbiAgICB0aGlzLl9uZXh0SGFuZGxlciA9IHRoaXMubmV4dC5iaW5kKHRoaXMpXG4gICAgdGhpcy5fcGFnaW5hdGlvbkNsaWNrSGFuZGxlciA9IHRoaXMuX2hhbmRsZVBhZ2luYXRpb25DbGljay5iaW5kKHRoaXMpXG4gICAgdGhpcy5fa2V5ZG93bkhhbmRsZXIgPSB0aGlzLl9oYW5kbGVLZXlkb3duLmJpbmQodGhpcylcblxuICAgIHRoaXMuX2hhbmRsZVRvdWNoc3RhcnQgPSB0aGlzLl9vblRvdWNoc3RhcnQuYmluZCh0aGlzKVxuICAgIHRoaXMuX2hhbmRsZVRvdWNobW92ZSA9IHRoaXMuX29uVG91Y2htb3ZlLmJpbmQodGhpcylcbiAgICB0aGlzLl9oYW5kbGVUb3VjaGVuZCA9IHRoaXMuX29uVG91Y2hlbmQuYmluZCh0aGlzKVxuXG4gICAgdGhpcy5faW5pdGlhbGl6ZSgpXG4gICAgdGhpcy5zbGlkZSh0aGlzLl9pbmRleCwgMCwgZmFsc2UpXG5cbiAgICB0aGlzLl91cGRhdGVDdHJsT2Zmc2V0cygpXG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZXMgdGhlIGNhcm91c2VsIGNvbXBvbmVudC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICAvLyByZXNwb25zaXZlIGhlbHBlcnNcbiAgICB0aGlzLl9icmVha3BvaW50UGhvbmUgPSBuZXcgRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD4oXCJkaXZcIilcbiAgICAgIC5hZGRDbGFzcyhcImpzLXBob25lXCIpXG4gICAgICAuZWxlbWVudFxuXG4gICAgdGhpcy5fYnJlYWtwb2ludFRhYmxldCA9IG5ldyBEb21FbGVtZW50PEhUTUxEaXZFbGVtZW50PihcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKFwianMtdGFibGV0XCIpXG4gICAgICAuZWxlbWVudFxuXG4gICAgdGhpcy5fYnJlYWtwb2ludERlc2t0b3AgPSBuZXcgRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD4oXCJkaXZcIilcbiAgICAgIC5hZGRDbGFzcyhcImpzLWRlc2t0b3BcIilcbiAgICAgIC5lbGVtZW50XG5cbiAgICB0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQodGhpcy5fYnJlYWtwb2ludFBob25lKVxuICAgIHRoaXMuZWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLl9icmVha3BvaW50VGFibGV0KVxuICAgIHRoaXMuZWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLl9icmVha3BvaW50RGVza3RvcClcblxuICAgIGlmICh0aGlzLl9wcmV2Q3RybCAmJiB0aGlzLl9uZXh0Q3RybCkge1xuICAgICAgdGhpcy5fcHJldkN0cmwuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX3ByZXZIYW5kbGVyKVxuICAgICAgdGhpcy5fbmV4dEN0cmwuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX25leHRIYW5kbGVyKVxuICAgIH1cblxuICAgIGlmICh0aGlzLl9wYWdpbmF0aW9uKSB7XG4gICAgICB0aGlzLl9wYWdpbmF0aW9uLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9wYWdpbmF0aW9uQ2xpY2tIYW5kbGVyKVxuICAgIH1cblxuICAgIHRoaXMuX3NsaWRlcyA9IEFycmF5LmZyb20odGhpcy5fd3JhcHBlci5jaGlsZHJlbikgYXMgSFRNTEVsZW1lbnRbXVxuICAgIGlmICh0aGlzLl9zbGlkZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICB0aHJvdyBFcnJvcihcIlByb3ZpZGUgYXQgbGVhc3Qgb25lIHNsaWRlIHRvIHRoZSBzbGlkZXJcIilcbiAgICB9XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuX3NsaWRlcy5sZW5ndGg7IGkrKykge1xuICAgICAgbGV0IHNsaWRlID0gdGhpcy5fc2xpZGVzW2ldXG4gICAgICBzbGlkZS5zZXRBdHRyaWJ1dGUoQVRUUklCVVRFX0lOREVYLCBTdHJpbmcoaSkpXG4gICAgfVxuXG4gICAgdGhpcy5fdXBkYXRlUmVzcG9uc2l2ZU9wdGlvbnMoKVxuICAgIHRoaXMuX3NsaWRlcldyYXBwZXIuaW5pdGlhbGl6ZSgpXG5cbiAgICB0aGlzLnJlc2V0KClcblxuICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLl9rZXlkb3duSGFuZGxlcilcblxuICAgIHRoaXMuX3NsaWRlQXJlYS5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIHRoaXMuX2hhbmRsZVRvdWNoc3RhcnQpXG4gICAgdGhpcy5fc2xpZGVBcmVhLmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaHN0YXJ0XCIsIHRoaXMuX2hhbmRsZVRvdWNoc3RhcnQpXG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCB0aGlzLl9yZXNpemVIYW5kbGVyKVxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwib3JpZW50YXRpb25jaGFuZ2VcIiwgdGhpcy5fcmVzaXplSGFuZGxlcilcbiAgfVxuXG4gIHByb3RlY3RlZCBfaXNCcmVha3BvaW50QWN0aXZlKGJyZWFrcG9pbnQ6IEhUTUxEaXZFbGVtZW50KSB7XG4gICAgbGV0IHN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoYnJlYWtwb2ludClcbiAgICByZXR1cm4gc3R5bGUudmlzaWJpbGl0eSA9PT0gXCJ2aXNpYmxlXCJcbiAgfVxuXG4gIHByb3RlY3RlZCBfb25yZXNpemUoKSB7XG4gICAgdGhpcy5yZXNldCgpXG4gICAgdGhpcy5fdXBkYXRlQ3RybE9mZnNldHMoKVxuICB9XG5cbiAgLyoqXG4gICAqIE1ha2VzIHN1cmUgdGhlIGluZGV4IGlzIGFsd2F5cyBpbiB0aGUgcmFuZ2Ugb2YgYXZhaWxhYmxlIHNsaWRlXG4gICAqIEluIGNhc2UgaXQncyB0byBoaWdoIG9yIHRvIGxvdyBpdCBpcyB3cmFwcGVkIGFyb3VuZFxuICAgKiBAcGFyYW0ge051bWJlcn0gaW5kZXggLSBUaGUgaW5kZXggdG8gYWRqdXN0IGFuZCBzYW5pdGl6ZVxuICAgKiBAcmV0dXJucyB7TnVtYmVyfSBpbmRleCAtIFRoZSBhZGp1c3RlZCBpbmRleFxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9hZGp1c3RJbmRleChpbmRleDogbnVtYmVyKSB7XG4gICAgaWYgKHR5cGVvZiBpbmRleCAhPT0gXCJudW1iZXJcIikge1xuICAgICAgaW5kZXggPSAwXG4gICAgfVxuXG4gICAgaWYgKGluZGV4IDwgMCkge1xuICAgICAgaW5kZXggPSB0aGlzLl93cmFwcm91bmQoaW5kZXgsIDAsIHRoaXMuX3NsaWRlcy5sZW5ndGgpXG4gICAgfSBlbHNlIGlmIChpbmRleCA+PSB0aGlzLl9zbGlkZXMubGVuZ3RoKSB7XG4gICAgICBpbmRleCAlPSB0aGlzLl9zbGlkZXMubGVuZ3RoXG4gICAgfVxuXG4gICAgcmV0dXJuIE1hdGguZmxvb3IoaW5kZXggLyB0aGlzLl9zbGlkZXNQZXJHcm91cCkgKiB0aGlzLl9zbGlkZXNQZXJHcm91cFxuICB9XG5cbiAgcHJvdGVjdGVkIF93cmFwcm91bmQobjogbnVtYmVyLCBtaW46IG51bWJlciwgbWF4OiBudW1iZXIpIHtcbiAgICBpZiAobiA+PSBtYXgpIHtcbiAgICAgIHJldHVybiBtaW5cbiAgICB9XG5cbiAgICBpZiAobiA8IG1pbikge1xuICAgICAgcmV0dXJuIG1heCAtIDFcbiAgICB9XG5cbiAgICByZXR1cm4gblxuICB9XG5cbiAgcHJvdGVjdGVkIF93cmFwcm91bmRDb3VudChhOiBudW1iZXIsIGI6IG51bWJlciwgbWluOiBudW1iZXIsIG1heDogbnVtYmVyLCBkaXJlY3Rpb246IERpcmVjdGlvbikge1xuICAgIGlmIChkaXJlY3Rpb24gPT09IDApIHtcbiAgICAgIHJldHVybiAwXG4gICAgfVxuXG4gICAgaWYgKGEgPCBtaW4gfHwgYSA+PSBtYXgpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihgQXJndW1lbnQgJ2EnIGlzIG91dCBvZiByYW5nZSwgVmFsdWU6ICR7YX0gTWluOiAke21pbn0sIE1heDogJHttYXh9YClcbiAgICB9XG5cbiAgICBpZiAoYiA8IG1pbiB8fCBiID49IG1heCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBBcmd1bWVudCAnYicgaXMgb3V0IG9mIHJhbmdlLCBWYWx1ZTogJHtifSBNaW46ICR7bWlufSwgTWF4OiAke21heH1gKVxuICAgIH1cblxuICAgIGxldCBpID0gMFxuICAgIHdoaWxlIChhICE9PSBiKSB7XG4gICAgICBpKytcbiAgICAgIGEgPSB0aGlzLl93cmFwcm91bmQoYSArIGRpcmVjdGlvbiwgbWluLCBtYXgpXG4gICAgfVxuXG4gICAgcmV0dXJuIGlcbiAgfVxuXG4gIHByb3RlY3RlZCBfdXBkYXRlQ3RybE9mZnNldHMoKSB7XG4gICAgaWYgKCF0aGlzLl9uZXh0Q3RybCB8fCAhdGhpcy5fcHJldkN0cmwgfHwgIXRoaXMuX2J0bldyYXBwZXIpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGxldCBwcmV2Q3RybE1hcmdpbiA9IDBcbiAgICBsZXQgbmV4dEN0cmxNYXJnaW4gPSAwXG5cbiAgICBpZiAodGhpcy5fc2xpZGVzUGVyR3JvdXAgPiAxKSB7XG4gICAgICBsZXQgd3JhcHBlclJlY3QgPSB0aGlzLl9idG5XcmFwcGVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpXG5cbiAgICAgIGNvbnN0IHByZXZTbGlkZUNvdW50ID0gTWF0aC5mbG9vcigwLjUgKiB0aGlzLl9zbGlkZXNQZXJHcm91cClcbiAgICAgIGNvbnN0IHJpZ2h0SW5kZXggPSB0aGlzLl9zbGlkZXJXcmFwcGVyLmluZGV4ICsgcHJldlNsaWRlQ291bnQgKyAxXG5cbiAgICAgIGxldCBsZWZ0SW5kZXggPSB0aGlzLl9zbGlkZXJXcmFwcGVyLmluZGV4IC0gMVxuICAgICAgaWYgKHRoaXMuX3NsaWRlc1Blckdyb3VwICUgMiAhPT0gMCkge1xuICAgICAgICBsZWZ0SW5kZXggLT0gcHJldlNsaWRlQ291bnRcbiAgICAgIH1cblxuICAgICAgaWYgKChsZWZ0SW5kZXggPj0gMCAmJiBsZWZ0SW5kZXggPCB0aGlzLl93cmFwcGVyLmNoaWxkcmVuLmxlbmd0aCkgJiZcbiAgICAgICAgKHJpZ2h0SW5kZXggPj0gMCAmJiByaWdodEluZGV4IDwgdGhpcy5fd3JhcHBlci5jaGlsZHJlbi5sZW5ndGgpKSB7XG4gICAgICAgIGxldCBsZWZ0U2xpZGUgPSB0aGlzLl9zbGlkZXJXcmFwcGVyLmdldFNsaWRlUHJvcGVydGllcyhsZWZ0SW5kZXgpXG4gICAgICAgIGxldCByaWdodFNsaWRlID0gdGhpcy5fc2xpZGVyV3JhcHBlci5nZXRTbGlkZVByb3BlcnRpZXMocmlnaHRJbmRleClcblxuICAgICAgICBsZXQgYnRuV2lkdGggPSB0aGlzLl9wcmV2Q3RybC5vZmZzZXRXaWR0aFxuICAgICAgICBpZiAoYnRuV2lkdGggPD0gMCkge1xuICAgICAgICAgIGJ0bldpZHRoID0gNjBcbiAgICAgICAgfVxuXG4gICAgICAgIHByZXZDdHJsTWFyZ2luID0gbGVmdFNsaWRlLnJpZ2h0IC0gd3JhcHBlclJlY3QubGVmdCAtIGJ0bldpZHRoXG4gICAgICAgIG5leHRDdHJsTWFyZ2luID0gd3JhcHBlclJlY3QucmlnaHQgLSByaWdodFNsaWRlLmxlZnQgLSBidG5XaWR0aFxuICAgICAgfVxuICAgIH1cblxuICAgIGxldCBsZWZ0ID0gcHJldkN0cmxNYXJnaW4gIT09IDAgPyBgJHtwcmV2Q3RybE1hcmdpbn1weGAgOiBcIlwiXG4gICAgdGhpcy5fcHJldkN0cmwuc3R5bGUubGVmdCA9IGxlZnRcblxuICAgIGxldCByaWdodCA9IG5leHRDdHJsTWFyZ2luICE9PSAwID8gYCR7bmV4dEN0cmxNYXJnaW59cHhgIDogXCJcIlxuICAgIHRoaXMuX25leHRDdHJsLnN0eWxlLnJpZ2h0ID0gcmlnaHRcbiAgfVxuXG4gIHByb3RlY3RlZCBfdXBkYXRlQWN0aXZlU2xpZGVzKG5leHRJbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3QgcHJldlNsaWRlQ291bnQgPSBNYXRoLmZsb29yKDAuNSAqICh0aGlzLl9zbGlkZXNQZXJHcm91cCAtIDEpKVxuICAgIGNvbnN0IGV2ZW5Hcm91cCA9IHRoaXMuX3NsaWRlc1Blckdyb3VwICUgMiA9PT0gMFxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLl93cmFwcGVyLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICBsZXQgc2xpZGUgPSB0aGlzLl93cmFwcGVyLmNoaWxkcmVuW2ldXG5cbiAgICAgIGlmIChpID09PSBuZXh0SW5kZXggfHwgKGV2ZW5Hcm91cCAmJiBpID09PSBuZXh0SW5kZXggKyAxKSkge1xuICAgICAgICBEb20uYWRkQ2xhc3Moc2xpZGUsIENMQVNTX0FDVElWRSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIERvbS5yZW1vdmVDbGFzcyhzbGlkZSwgQ0xBU1NfQUNUSVZFKVxuICAgICAgfVxuXG4gICAgICBpZiAoaSA8IG5leHRJbmRleCAmJiBpID49IG5leHRJbmRleCAtIHByZXZTbGlkZUNvdW50KSB7XG4gICAgICAgIERvbS5hZGRDbGFzcyhzbGlkZSwgQ0xBU1NfUFJFVilcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIERvbS5yZW1vdmVDbGFzcyhzbGlkZSwgQ0xBU1NfUFJFVilcbiAgICAgIH1cblxuICAgICAgaWYgKGkgPiBuZXh0SW5kZXggJiYgKGkgPD0gbmV4dEluZGV4ICsgcHJldlNsaWRlQ291bnQgfHwgKGV2ZW5Hcm91cCAmJiBpIDw9IG5leHRJbmRleCArIDEgKyBwcmV2U2xpZGVDb3VudCkpKSB7XG4gICAgICAgIERvbS5hZGRDbGFzcyhzbGlkZSwgQ0xBU1NfTkVYVClcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIERvbS5yZW1vdmVDbGFzcyhzbGlkZSwgQ0xBU1NfTkVYVClcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlcyBhbmQgY3JlYXRlcyB0aGUgcGFnaW5hdGlvbiBidWxsZXRzLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF91cGRhdGVQYWdpbmF0aW9uKCkge1xuICAgIGlmICghdGhpcy5fcGFnaW5hdGlvbikge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgbGV0IHRvID0gdGhpcy5faW5kZXhcblxuICAgIGxldCBidWxsZXRzID0gdGhpcy5fcGFnaW5hdGlvbi5jaGlsZHJlblxuICAgIGxldCB0b3RhbEl0ZW1zID0gTWF0aC5tYXgodGhpcy5fc2xpZGVzLmxlbmd0aCwgYnVsbGV0cy5sZW5ndGgpXG4gICAgbGV0IHNsaWRlQ291bnQgPSBNYXRoLmNlaWwodGhpcy5fc2xpZGVzLmxlbmd0aCAvIHRoaXMuX3NsaWRlc1Blckdyb3VwKVxuICAgIGxldCBhY3RpdmVTbGlkZUluZGV4ID0gTWF0aC5mbG9vcih0byAvIHRoaXMuX3NsaWRlc1Blckdyb3VwKVxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0b3RhbEl0ZW1zOyBpKyspIHtcbiAgICAgIGxldCBidWxsZXRcblxuICAgICAgaWYgKGJ1bGxldHMubGVuZ3RoID4gaSkge1xuICAgICAgICBpZiAoYnVsbGV0cy5sZW5ndGggPD0gc2xpZGVDb3VudCkge1xuICAgICAgICAgIGJ1bGxldCA9IGJ1bGxldHNbaV1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZW1vdmUoYnVsbGV0c1tpXSlcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChpIDwgc2xpZGVDb3VudCkge1xuICAgICAgICBidWxsZXQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgICAgIC5hZGRDbGFzcyhDTEFTU19CVUxMRVQpXG4gICAgICAgICAgLmVsZW1lbnRcbiAgICAgICAgdGhpcy5fcGFnaW5hdGlvbi5hcHBlbmRDaGlsZChidWxsZXQpXG4gICAgICB9XG5cbiAgICAgIGlmIChidWxsZXQgJiYgaSA8IHNsaWRlQ291bnQpIHtcbiAgICAgICAgaWYgKGkgPT09IGFjdGl2ZVNsaWRlSW5kZXgpIHtcbiAgICAgICAgICBEb20uYWRkQ2xhc3MoYnVsbGV0LCBDTEFTU19CVUxMRVRfQUNUSVZFKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIERvbS5yZW1vdmVDbGFzcyhidWxsZXQsIENMQVNTX0JVTExFVF9BQ1RJVkUpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZVBhZ2luYXRpb25DbGljayhlOiBNb3VzZUV2ZW50KSB7XG4gICAgaWYgKCFEb20uaGFzQ2xhc3MoZS50YXJnZXQgYXMgRWxlbWVudCwgQ0xBU1NfQlVMTEVUKSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgbGV0IGluZGV4ID0gQXJyYXkuZnJvbSh0aGlzLl9wYWdpbmF0aW9uIS5jaGlsZHJlbikuaW5kZXhPZihlLnRhcmdldCBhcyBFbGVtZW50KVxuICAgIGxldCBzbGlkZU51bWJlciA9IGluZGV4ICogdGhpcy5fc2xpZGVzUGVyR3JvdXBcblxuICAgIHRoaXMuc2xpZGVUbyhzbGlkZU51bWJlcilcbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlS2V5ZG93bihldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgIGxldCBrZXljb2RlID0gZXZlbnQud2hpY2ggfHwgZXZlbnQua2V5Q29kZVxuXG4gICAgc3dpdGNoIChrZXljb2RlKSB7XG4gICAgICBjYXNlIElucHV0cy5LRVlfQVJST1dfTEVGVDpcbiAgICAgICAgdGhpcy5wcmV2KClcbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgSW5wdXRzLktFWV9BUlJPV19SSUdIVDpcbiAgICAgICAgdGhpcy5uZXh0KClcbiAgICAgICAgYnJlYWtcbiAgICAgIGNhc2UgSW5wdXRzLktFWV9FU0NBUEU6XG4gICAgICAgIHRoaXMuZWxlbWVudC5ibHVyKClcbiAgICAgICAgYnJlYWtcbiAgICAgIGRlZmF1bHQ6XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9vblRvdWNoc3RhcnQoZXZlbnQ6IFRvdWNoRXZlbnQgfCBNb3VzZUV2ZW50KSB7XG4gICAgY29uc3QgdG91Y2ggPSAoZXZlbnQgYXMgVG91Y2hFdmVudCkudG91Y2hlcyA/IChldmVudCBhcyBUb3VjaEV2ZW50KS50b3VjaGVzWzBdIDogZXZlbnQgYXMgTW91c2VFdmVudFxuXG4gICAgdGhpcy5fc2xpZGVBcmVhLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgdGhpcy5faGFuZGxlVG91Y2hzdGFydClcbiAgICB0aGlzLl9zbGlkZUFyZWEucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInRvdWNoc3RhcnRcIiwgdGhpcy5faGFuZGxlVG91Y2hzdGFydClcblxuICAgIHRoaXMuX3NsaWRlcldyYXBwZXIuYmVnaW5EcmFnKClcbiAgICBjb25zdCB7IHBhZ2VYIH0gPSB0b3VjaFxuXG4gICAgdGhpcy5fdG91Y2hPZmZzZXQgPSB7XG4gICAgICB4OiBwYWdlWCxcbiAgICAgIHRpbWU6IERhdGUubm93KClcbiAgICB9XG5cbiAgICB0aGlzLl9kZWx0YSA9IHtcbiAgICAgIHg6IDAsXG4gICAgICBsYXN0TW92ZTogcGFnZVhcbiAgICB9XG5cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIHRoaXMuX2hhbmRsZVRvdWNobW92ZSlcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwidG91Y2htb3ZlXCIsIHRoaXMuX2hhbmRsZVRvdWNobW92ZSlcblxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIHRoaXMuX2hhbmRsZVRvdWNoZW5kKVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIHRoaXMuX2hhbmRsZVRvdWNoZW5kKVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaGVuZFwiLCB0aGlzLl9oYW5kbGVUb3VjaGVuZClcbiAgfVxuXG4gIHByb3RlY3RlZCBfb25Ub3VjaG1vdmUoZXZlbnQ6IFRvdWNoRXZlbnQgfCBNb3VzZUV2ZW50KSB7XG4gICAgY29uc3QgdG91Y2ggPSAoZXZlbnQgYXMgVG91Y2hFdmVudCkudG91Y2hlcyA/IChldmVudCBhcyBUb3VjaEV2ZW50KS50b3VjaGVzWzBdIDogZXZlbnQgYXMgTW91c2VFdmVudFxuICAgIGNvbnN0IHsgcGFnZVggfSA9IHRvdWNoXG5cbiAgICBsZXQgZGVsdGFNb3ZlID0gcGFnZVggLSB0aGlzLl9kZWx0YSEubGFzdE1vdmVcblxuICAgIHRoaXMuX2RlbHRhID0ge1xuICAgICAgeDogcGFnZVggLSB0aGlzLl90b3VjaE9mZnNldCEueCxcbiAgICAgIGxhc3RNb3ZlOiBwYWdlWFxuICAgIH1cblxuICAgIGlmICh0aGlzLl90b3VjaE9mZnNldCkge1xuICAgICAgcHJldmVudERlZmF1bHQoZXZlbnQpXG5cbiAgICAgIHRoaXMuX3NsaWRlcldyYXBwZXIubW92ZShkZWx0YU1vdmUpXG4gICAgICB0aGlzLl9jbG9uZVNsaWRlc1RvRml0V3JhcHBlcihmYWxzZSwgZGVsdGFNb3ZlKVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfb25Ub3VjaGVuZCgpIHtcbiAgICBjb25zdCBkdXJhdGlvbiA9IHRoaXMuX3RvdWNoT2Zmc2V0ID8gRGF0ZS5ub3coKSAtIHRoaXMuX3RvdWNoT2Zmc2V0LnRpbWUgOiB1bmRlZmluZWRcblxuICAgIGNvbnN0IGlzVmFsaWQgPSBOdW1iZXIoZHVyYXRpb24pIDwgVE9VQ0hfRFVSQVRJT04gJiZcbiAgICAgIE1hdGguYWJzKHRoaXMuX2RlbHRhIS54KSA+IFRPVUNIX0RFTFRBX01JTiB8fFxuICAgICAgTWF0aC5hYnModGhpcy5fZGVsdGEhLngpID4gdGhpcy5fZnJhbWVXaWR0aCEgLyAzXG5cbiAgICBpZiAoaXNWYWxpZCkge1xuICAgICAgY29uc3QgZGlyZWN0aW9uID0gY2xhbXAodGhpcy5fZGVsdGEhLngsIC0xLCAxKSAqIC0xIGFzIERpcmVjdGlvblxuICAgICAgdGhpcy5zbGlkZShmYWxzZSwgZGlyZWN0aW9uLCB0cnVlKVxuXG4gICAgICB0aGlzLl9zbGlkZXJXcmFwcGVyLmVuZERyYWcoKVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBTbGlkZSBiYWNrIHRvIHRoZSBzdGFydGluZyBwb2ludCBvZiB0aGUgZHJhZyBvcGVyYXRpb25cbiAgICAgIHRoaXMuX3NsaWRlcldyYXBwZXIuY2FuY2VsRHJhZygpXG4gICAgfVxuXG4gICAgdGhpcy5fdG91Y2hPZmZzZXQgPSB1bmRlZmluZWRcblxuICAgIHRoaXMuX3NsaWRlQXJlYS5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIHRoaXMuX2hhbmRsZVRvdWNoc3RhcnQpXG4gICAgdGhpcy5fc2xpZGVBcmVhLmFkZEV2ZW50TGlzdGVuZXIoXCJ0b3VjaHN0YXJ0XCIsIHRoaXMuX2hhbmRsZVRvdWNoc3RhcnQpXG5cbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIHRoaXMuX2hhbmRsZVRvdWNobW92ZSlcbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCB0aGlzLl9oYW5kbGVUb3VjaGVuZClcbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2VsZWF2ZVwiLCB0aGlzLl9oYW5kbGVUb3VjaGVuZClcbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwidG91Y2htb3ZlXCIsIHRoaXMuX2hhbmRsZVRvdWNobW92ZSlcbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwidG91Y2hlbmRcIiwgdGhpcy5faGFuZGxlVG91Y2hlbmQpXG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlZCBwYXJhbWV0ZXJzIGluIHJlZ2FyZCB0byB0aGUgY3VycmVudGx5IGFjdGl2ZSByZXNwb25zaXZlXG4gICAqIGJyZWFrcG9pbnQuXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBwcm90ZWN0ZWQgX3VwZGF0ZVJlc3BvbnNpdmVPcHRpb25zKCkge1xuICAgIGlmICh0aGlzLl9pc0JyZWFrcG9pbnRBY3RpdmUodGhpcy5fYnJlYWtwb2ludFBob25lKSkge1xuICAgICAgdGhpcy5fc2xpZGVzUGVyR3JvdXAgPSAxXG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX2lzQnJlYWtwb2ludEFjdGl2ZSh0aGlzLl9icmVha3BvaW50VGFibGV0KSkge1xuICAgICAgdGhpcy5fc2xpZGVzUGVyR3JvdXAgPSAyXG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX2lzQnJlYWtwb2ludEFjdGl2ZSh0aGlzLl9icmVha3BvaW50RGVza3RvcCkpIHtcbiAgICAgIHRoaXMuX3NsaWRlc1Blckdyb3VwID0gM1xuICAgIH1cblxuICAgIHRoaXMuX3NsaWRlcldyYXBwZXIuc2xpZGVzUGVyR3JvdXAgPSB0aGlzLl9zbGlkZXNQZXJHcm91cFxuICB9XG5cbiAgLyoqXG4gICAqIENsb25lcyB0aGUgcmVxdWVzdGVkIHNsaWRlIGFuZCBhZGRzIGl0IHRvIHRoZSBzbGlkZXIuXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBpbmRleCAtIFRoZSBvcmlnaW5hbCBzbGlkZSBpbmRleCBvZiB0aGUgdGVtcGxhdGUgc2xpZGVcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGRpcmVjdGlvbiAtIFRoZSBkaXJlY3Rpb24gaW4gd2hpY2ggdG8gYWRkIHRoZSBzbGlkZXMsIC0xIGZvciBsZWZ0LCAxIGZvciByaWdodFxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9jbG9uZVNsaWRlKGluZGV4OiBudW1iZXIsIGRpcmVjdGlvbjogbnVtYmVyKSB7XG4gICAgbGV0IGNsb25lID0gdGhpcy5fc2xpZGVzW2luZGV4XS5jbG9uZU5vZGUodHJ1ZSkgYXMgSFRNTEVsZW1lbnRcbiAgICBEb20ucmVtb3ZlQ2xhc3MoY2xvbmUsIENMQVNTX0FDVElWRSlcbiAgICBEb20ucmVtb3ZlQ2xhc3MoY2xvbmUsIENMQVNTX1BSRVYpXG4gICAgRG9tLnJlbW92ZUNsYXNzKGNsb25lLCBDTEFTU19ORVhUKVxuXG4gICAgdGhpcy5fc2xpZGVyV3JhcHBlci5hZGRTbGlkZShjbG9uZSwgZGlyZWN0aW9uKVxuXG4gICAgbGV0IHNsaWRlTWFyZ2luID0gdGhpcy5fYWRkaXRpb25hbFNsaWRlTWFyZ2luID4gMCA/IGAke3RoaXMuX2FkZGl0aW9uYWxTbGlkZU1hcmdpbn1weGAgOiBcIlwiXG4gICAgY2xvbmUuc3R5bGUubWFyZ2luTGVmdCA9IHNsaWRlTWFyZ2luXG4gICAgY2xvbmUuc3R5bGUubWFyZ2luUmlnaHQgPSBzbGlkZU1hcmdpblxuXG4gICAgcmV0dXJuIGNsb25lLm9mZnNldFdpZHRoXG4gIH1cblxuICAvKipcbiAgICogQ2xvbmVzIGFuZCBhZGRzIHRoZSByZXF1ZXN0ZWQgYW1tb3VudCBvZiBzbGlkZXMuXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBzbGlkZUNvdW50IC0gVGhlIG51bWJlciBvZiBzbGlkZXMgdG8gYWRkXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBkaXJlY3Rpb24gLSBUaGUgZGlyZWN0aW9uIGluIHdoaWNoIHRvIGFkZCB0aGUgc2xpZGVzLCAtMSBmb3IgbGVmdCwgMSBmb3IgcmlnaHRcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfY2xvbmVTbGlkZXNCeUNvdW50KHNsaWRlQ291bnQ6IG51bWJlciwgZGlyZWN0aW9uOiBEaXJlY3Rpb24pIHtcbiAgICBsZXQgb3JpZ2luYWxJbmRleCA9IGRpcmVjdGlvbiA8IDAgPyAwIDogdGhpcy5fd3JhcHBlci5jaGlsZHJlbi5sZW5ndGggLSAxXG4gICAgbGV0IGluZGV4ID0gcGFyc2VJbnQodGhpcy5fd3JhcHBlci5jaGlsZHJlbltvcmlnaW5hbEluZGV4XS5nZXRBdHRyaWJ1dGUoQVRUUklCVVRFX0lOREVYKSEsIDEwKVxuXG4gICAgd2hpbGUgKHNsaWRlQ291bnQgPiAwKSB7XG4gICAgICBpbmRleCA9IHRoaXMuX3dyYXByb3VuZChpbmRleCArIGRpcmVjdGlvbiwgMCwgdGhpcy5fc2xpZGVzLmxlbmd0aClcbiAgICAgIHRoaXMuX2Nsb25lU2xpZGUoaW5kZXgsIGRpcmVjdGlvbilcbiAgICAgIHNsaWRlQ291bnQtLVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDYWxjdWxhdGVzIHRoZSBzY3JvbGwgY2xvdW50IGFuZCBpbnNlcnRzIHRoZSByZXF1aXJlZCBhbW1vdW50IG9mIHNsaWRlc1xuICAgKiBpbiB0aGUgYXByb3ByaWF0ZSBkaXJlY3Rpb24uXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBuZXh0SW5kZXggLSBUaGUgc2xpZGUgdG8gc2Nyb2xsIHRvXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBkaXJlY3Rpb24gLSBUaGUgZGlyZWN0aW9uIG9mIHRoZSBzY3JvbGxcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfY2xvbmVTbGlkZXNCeVNjcm9sbENvdW50KG5leHRJbmRleDogbnVtYmVyLCBkaXJlY3Rpb246IERpcmVjdGlvbikge1xuICAgIGNvbnN0IHNjcm9sbENvdW50ID0gdGhpcy5fd3JhcHJvdW5kQ291bnQodGhpcy5faW5kZXgsIG5leHRJbmRleCwgMCwgdGhpcy5fc2xpZGVzLmxlbmd0aCwgZGlyZWN0aW9uKVxuXG4gICAgY29uc3Qgb3V0ZXJTbGlkZVByb3BzID0gdGhpcy5fc2xpZGVyV3JhcHBlci5nZXRTbGlkZVByb3BlcnRpZXMoZGlyZWN0aW9uID4gMCA/IHRoaXMuX3dyYXBwZXIuY2hpbGRyZW4ubGVuZ3RoIC0gMSA6IDApXG4gICAgY29uc3QgaW5kZXhUb091dGVyU2xpZGVDb3VudCA9IHRoaXMuX3dyYXByb3VuZENvdW50KHRoaXMuX2luZGV4LCBvdXRlclNsaWRlUHJvcHMuaW5kZXgsIDAsIHRoaXMuX3NsaWRlcy5sZW5ndGgsIGRpcmVjdGlvbilcblxuICAgIGNvbnN0IHNsaWRlc1RvSW5zZXJ0ID0gc2Nyb2xsQ291bnQgLSBpbmRleFRvT3V0ZXJTbGlkZUNvdW50XG4gICAgaWYgKHNsaWRlc1RvSW5zZXJ0ID4gMCkge1xuICAgICAgdGhpcy5fY2xvbmVTbGlkZXNCeUNvdW50KHNsaWRlc1RvSW5zZXJ0LCBkaXJlY3Rpb24pXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9jbG9uZVNsaWRlc0J5VG9GaWxsKHNwYWNlVG9GaWxsOiBudW1iZXIsIGRpcmVjdGlvbjogRGlyZWN0aW9uKSB7XG4gICAgbGV0IG9yaWdpbmFsSW5kZXggPSBkaXJlY3Rpb24gPCAwID8gMCA6IHRoaXMuX3dyYXBwZXIuY2hpbGRyZW4ubGVuZ3RoIC0gMVxuICAgIGxldCBpbmRleCA9IHBhcnNlSW50KHRoaXMuX3dyYXBwZXIuY2hpbGRyZW5bb3JpZ2luYWxJbmRleF0uZ2V0QXR0cmlidXRlKEFUVFJJQlVURV9JTkRFWCkhLCAxMClcblxuICAgIHdoaWxlIChzcGFjZVRvRmlsbCA+IDApIHtcbiAgICAgIGluZGV4ID0gdGhpcy5fd3JhcHJvdW5kKGluZGV4ICsgZGlyZWN0aW9uLCAwLCB0aGlzLl9zbGlkZXMubGVuZ3RoKVxuICAgICAgc3BhY2VUb0ZpbGwgLT0gdGhpcy5fY2xvbmVTbGlkZShpbmRleCwgZGlyZWN0aW9uKVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfY2xvbmVTbGlkZXNUb0ZpdFdyYXBwZXIoY2xlYW51cCA9IHRydWUsIHNsaWRlRGVsdGEgPSAwKSB7XG4gICAgY29uc3QgcmVhbEluZGV4ID0gdGhpcy5fc2xpZGVyV3JhcHBlci5pbmRleFxuICAgIGxldCBmaXJzdDogU2xpZGVQcm9wZXJ0aWVzXG4gICAgbGV0IGxhc3Q6IFNsaWRlUHJvcGVydGllc1xuXG4gICAgaWYgKGNsZWFudXAgPT09IGZhbHNlKSB7XG4gICAgICBmaXJzdCA9IHRoaXMuX3NsaWRlcldyYXBwZXIuZ2V0U2xpZGVQcm9wZXJ0aWVzKDApXG4gICAgICBsYXN0ID0gdGhpcy5fc2xpZGVyV3JhcHBlci5nZXRTbGlkZVByb3BlcnRpZXModGhpcy5fd3JhcHBlci5jaGlsZHJlbi5sZW5ndGggLSAxKVxuICAgIH0gZWxzZSB7XG4gICAgICBsZXQgcmVzdWx0ID0gdGhpcy5fc2xpZGVyV3JhcHBlci5nZXRSZW1vdmFibGVTbGlkZXMoc2xpZGVEZWx0YSlcbiAgICAgIGZpcnN0ID0gcmVzdWx0LmZpcnN0IVxuICAgICAgbGFzdCA9IHJlc3VsdC5sYXN0IVxuXG4gICAgICAvLyBSZW1vdmUgdGhlIHNsaWRlcyBmcm9tIHZpZXdcbiAgICAgIGZvciAobGV0IGkgPSByZXN1bHQuc2xpZGVzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgIGlmIChyZXN1bHQuc2xpZGVzW2ldID09PSB0cnVlKSB7XG4gICAgICAgICAgdGhpcy5fc2xpZGVyV3JhcHBlci5yZW1vdmVTbGlkZShpKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgbGV0IHNwYWNlVG9GaWxsID0gdGhpcy5fc2xpZGVyV3JhcHBlci5nZXRFbXB0eVNwYWNlKGZpcnN0LmxlZnQsIGxhc3QucmlnaHQpXG5cbiAgICAvLyBDaGVjayBpZiBhZGRpdGlvbmFsIHNsaWRlcyBhcmUgcmVxdWlyZWQgb24gdGhlIGxlZnRcbiAgICBpZiAoZmlyc3QudmlzaWJsZSA9PT0gdHJ1ZSAmJiBzcGFjZVRvRmlsbC5sZWZ0ID4gMCkge1xuICAgICAgdGhpcy5fY2xvbmVTbGlkZXNCeVRvRmlsbChzcGFjZVRvRmlsbC5sZWZ0LCAtMSlcbiAgICB9XG5cbiAgICAvLyBDaGVjayBpZiBhZGRpdGlvbmFsIHNsaWRlcyBhcmUgcmVxdWlyZWQgb24gdGhlIHJpZ2h0XG4gICAgaWYgKGxhc3QudmlzaWJsZSA9PT0gdHJ1ZSAmJiBzcGFjZVRvRmlsbC5yaWdodCA+IDApIHtcbiAgICAgIHRoaXMuX2Nsb25lU2xpZGVzQnlUb0ZpbGwoc3BhY2VUb0ZpbGwucmlnaHQsIDEpXG4gICAgfVxuXG4gICAgcmV0dXJuIHJlYWxJbmRleCAtIHRoaXMuX3NsaWRlcldyYXBwZXIuaW5kZXhcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSByZWFsICh3cmFwcGVyKSBpbmRleCBmb3IgdGhlIHNsaWRlIHdpdGggdGhlIGdpdmVuIG9yaWdpbmFsIGluZGV4XG4gICAqIEBwYXJhbSB7TnVtYmVyfSBpbmRleCAtIFRoZSBpbmRleCB0byBzZWFyY2ggZm9yXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBkaXJlY3Rpb24gLSBUaGUgZGlyZWN0aW9uIGluIHdoaWNoIHRvIHNlYXJjaFxuICAgKiBAcmV0dXJucyB7TnVtYmVyfSBUaGUgd3JhcHBlciBpbmRleFxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9nZXRSZWFsSW5kZXhGb3IoaW5kZXg6IG51bWJlciwgZGlyZWN0aW9uOiBEaXJlY3Rpb24pIHtcbiAgICBsZXQgaSA9IHRoaXMuX3NsaWRlcldyYXBwZXIuaW5kZXhcbiAgICB3aGlsZSAoaSA+PSAwICYmIGkgPCB0aGlzLl93cmFwcGVyLmNoaWxkcmVuLmxlbmd0aCkge1xuICAgICAgbGV0IHNsaWRlSW5kZXggPSBwYXJzZUludCh0aGlzLl93cmFwcGVyLmNoaWxkcmVuW2ldLmdldEF0dHJpYnV0ZShBVFRSSUJVVEVfSU5ERVgpISwgMTApXG4gICAgICBpZiAoc2xpZGVJbmRleCA9PT0gaW5kZXgpIHtcbiAgICAgICAgcmV0dXJuIGlcbiAgICAgIH1cblxuICAgICAgaSArPSBkaXJlY3Rpb25cbiAgICB9XG5cbiAgICB0aHJvdyBuZXcgRXJyb3IoYENsb3VkIG5vdCBmaW5kIHJlYWwgaW5kZXggZm9yIHNsaWRlICR7aW5kZXh9IGluIGRpcmVjdGlvbiAke2RpcmVjdGlvbn1gKVxuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGluZGV4IG9mIHRoZSBjdXJyZW50IGFjdGl2ZSBzbGlkZS4gSWYgdGhlIHNsaWRlcyBhcmUgZ3JvdXBlZCBldmVubHlcbiAgICogdGhlIGFjdGl2ZSBzbGlkZSBpcyBhbHdheXMgdGhlIGZpcnN0IGluIHRoZSBncm91cC5cbiAgICogQHJldHVybnMge051bWJlcn0gVGhlIGluZGV4IG9mIHRoZSBhY3RpdmUgc2xpZGUuXG4gICAqL1xuICBnZXQgaW5kZXgoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2luZGV4XG4gIH1cblxuICByZXNldCgpIHtcbiAgICB0aGlzLl9mcmFtZVdpZHRoID0gdGhpcy5fc2xpZGVyLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpXG4gICAgICAud2lkdGggfHwgdGhpcy5fc2xpZGVyLm9mZnNldFdpZHRoXG5cbiAgICB0aGlzLl91cGRhdGVSZXNwb25zaXZlT3B0aW9ucygpXG5cbiAgICBpZiAodGhpcy5fbmV4dEN0cmwpIHtcbiAgICAgICh0aGlzLl9uZXh0Q3RybCBhcyBhbnkpLmRpc2FibGVkID0gZmFsc2VcbiAgICB9XG5cbiAgICBpZiAodGhpcy5fcHJldkN0cmwpIHtcbiAgICAgICh0aGlzLl9wcmV2Q3RybCBhcyBhbnkpLmRpc2FibGVkID0gZmFsc2VcbiAgICB9XG5cbiAgICBpZiAodGhpcy5fc2xpZGVzUGVyR3JvdXAgPT09IDEpIHtcbiAgICAgIGxldCBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRoaXMuX3NsaWRlci5wYXJlbnRFbGVtZW50ISlcbiAgICAgIGxldCBwYXJlbnRXaWR0aCA9IHRoaXMuX3NsaWRlci5wYXJlbnRFbGVtZW50IS5jbGllbnRXaWR0aCArIChwYXJzZUZsb2F0KHN0eWxlLm1hcmdpbkxlZnQhKSB8fCAwKSArIChwYXJzZUZsb2F0KHN0eWxlLm1hcmdpblJpZ2h0ISkgfHwgMClcblxuICAgICAgbGV0IG91dGVyTWFyZ2luID0gTWF0aC5jZWlsKHBhcmVudFdpZHRoIC0gdGhpcy5fZnJhbWVXaWR0aClcbiAgICAgIHRoaXMuX2FkZGl0aW9uYWxTbGlkZU1hcmdpbiA9IE1hdGguY2VpbChvdXRlck1hcmdpbiAqIDAuNSkgKyAxXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuX2FkZGl0aW9uYWxTbGlkZU1hcmdpbiA9IDBcbiAgICB9XG5cbiAgICBsZXQgc2xpZGVNYXJnaW4gPSB0aGlzLl9hZGRpdGlvbmFsU2xpZGVNYXJnaW4gPiAwID8gYCR7dGhpcy5fYWRkaXRpb25hbFNsaWRlTWFyZ2lufXB4YCA6IFwiXCJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuX3dyYXBwZXIuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxldCBzbGlkZSA9IHRoaXMuX3dyYXBwZXIuY2hpbGRyZW5baV0gYXMgSFRNTEVsZW1lbnRcbiAgICAgIHNsaWRlLnN0eWxlLm1hcmdpbkxlZnQgPSBzbGlkZU1hcmdpblxuICAgICAgc2xpZGUuc3R5bGUubWFyZ2luUmlnaHQgPSBzbGlkZU1hcmdpblxuICAgIH1cblxuICAgIHRoaXMuX3NsaWRlcldyYXBwZXIub25yZXNpemUoKVxuICAgIHRoaXMuX2Nsb25lU2xpZGVzVG9GaXRXcmFwcGVyKGZhbHNlKVxuICAgIHRoaXMuX3NsaWRlcldyYXBwZXIubW92ZVRvKHRoaXMuX3NsaWRlcldyYXBwZXIuaW5kZXgpXG5cbiAgICB0aGlzLl91cGRhdGVQYWdpbmF0aW9uKClcbiAgICB0aGlzLl91cGRhdGVBY3RpdmVTbGlkZXModGhpcy5fc2xpZGVyV3JhcHBlci5pbmRleClcbiAgfVxuXG4gIC8qKlxuICAgKiBNb3ZlcyB0aGUgc2xpZGVyIHRvIHRoZSBuZXh0IGl0ZW0uXG4gICAqL1xuICBwcmV2KCkge1xuICAgIHRoaXMuc2xpZGUoZmFsc2UsIC0xKVxuICB9XG5cbiAgLyoqXG4gICAqIE1vdmVzIHRoZSBzbGlkZXIgdG8gdGhlIHByZXZpb3VzIGl0ZW0uXG4gICAqL1xuICBuZXh0KCkge1xuICAgIHRoaXMuc2xpZGUoZmFsc2UsIDEpXG4gIH1cblxuICBzbGlkZShuZXh0SW5kZXg6IG51bWJlciB8IGZhbHNlLCBkaXJlY3Rpb24/OiBEaXJlY3Rpb24sIGFuaW1hdGUgPSB0cnVlKSB7XG4gICAgaWYgKHR5cGVvZiBuZXh0SW5kZXggIT09IFwibnVtYmVyXCIpIHtcbiAgICAgIGlmIChkaXJlY3Rpb24hID4gMCkge1xuICAgICAgICBuZXh0SW5kZXggPSB0aGlzLl9pbmRleCArIHRoaXMuX3NsaWRlc1Blckdyb3VwXG4gICAgICAgIGRpcmVjdGlvbiA9IDFcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG5leHRJbmRleCA9IHRoaXMuX2luZGV4IC0gdGhpcy5fc2xpZGVzUGVyR3JvdXBcbiAgICAgICAgZGlyZWN0aW9uID0gLTFcbiAgICAgIH1cbiAgICB9XG5cbiAgICBuZXh0SW5kZXggPSB0aGlzLl9hZGp1c3RJbmRleChuZXh0SW5kZXgpXG5cbiAgICBpZiAoIWRpcmVjdGlvbikge1xuICAgICAgZGlyZWN0aW9uID0gY2xhbXAobmV4dEluZGV4IC0gdGhpcy5faW5kZXgsIC0xLCAxKSBhcyBEaXJlY3Rpb25cbiAgICB9XG5cbiAgICAvLyBNYWtlIHN1cmUgdGhlcmUgYXJlIGVub3VnaHQgc2xpZGVzIG9uIHNjcmVlblxuICAgIHRoaXMuX2Nsb25lU2xpZGVzVG9GaXRXcmFwcGVyKGZhbHNlKVxuXG4gICAgLy8gTWFrZSBzdXJlIHRoZXJlIGFyZSBlbm91Z2ggc2xpZGVzIGZvciB0aGUgc2Nyb2xsIG9wZXJhdGlvblxuICAgIHRoaXMuX2Nsb25lU2xpZGVzQnlTY3JvbGxDb3VudChuZXh0SW5kZXgsIGRpcmVjdGlvbilcblxuICAgIGxldCByZWFsSW5kZXggPSB0aGlzLl9nZXRSZWFsSW5kZXhGb3IobmV4dEluZGV4LCBkaXJlY3Rpb24pXG4gICAgbGV0IHNsaWRlRGVsdGEgPSB0aGlzLl9zbGlkZXJXcmFwcGVyLmdldFNsaWRlRGVsdGEocmVhbEluZGV4KVxuICAgIHJlYWxJbmRleCA9IE1hdGgubWF4KHJlYWxJbmRleCAtIHRoaXMuX2Nsb25lU2xpZGVzVG9GaXRXcmFwcGVyKHRydWUsIHNsaWRlRGVsdGEpLCAwKVxuXG4gICAgdGhpcy5fc2xpZGVyV3JhcHBlci5tb3ZlVG8ocmVhbEluZGV4LCB1bmRlZmluZWQsIGFuaW1hdGUpXG5cbiAgICAvLyBVcGRhdGUgdGhlIGFjdGl2ZSBpbmRleFxuICAgIHRoaXMuX2luZGV4ID0gbmV4dEluZGV4XG5cbiAgICAvLyBNYXJrIHNsaWRlcyBhcyBhY3RpdmVcbiAgICB0aGlzLl91cGRhdGVQYWdpbmF0aW9uKClcbiAgICB0aGlzLl91cGRhdGVBY3RpdmVTbGlkZXMocmVhbEluZGV4KVxuXG4gICAgLy8gY29uc29sZS5sb2coYFBlcmZvcm1lZCBzbGlkZSB0byAke3RoaXMuX2luZGV4fSwgcmVhbEluZGV4OiAke3RoaXMuX3NsaWRlcldyYXBwZXIuaW5kZXh9YClcbiAgfVxuXG4gIC8qKlxuICAgKiBNb3ZlcyB0aGUgc2xpZGVyIHRvIHRoZSBzZWxlY3RlZCBzbGlkZS5cbiAgICogQHBhcmFtIHtOdW1iZXJ9IGluZGV4IC0gVGhlIGluZGV4IG9mIHRoZSBzbGlkZSB0byBzbGlkZSB0by5cbiAgICogQHBhcmFtIHtCb29sZWFufSBhbmltYXRlIC0gYFRydWVgIGlmIHRoZSBzbGlkZSBzaG91bGQgYmUgYW5pbWF0ZWQ7IG90aGVyd2lzZSBgZmFsc2VgLiBEZWZhdWx0cyB0byBgdHJ1ZWAuXG4gICAqL1xuICBzbGlkZVRvKGluZGV4OiBudW1iZXIsIGFuaW1hdGUgPSB0cnVlKSB7XG4gICAgdGhpcy5zbGlkZShpbmRleCwgdW5kZWZpbmVkLCBhbmltYXRlKVxuICB9XG5cbiAgLyoqXG4gICAqIERlc3Ryb3lzIHRoZSBjb21wb25lbnRzIGFuZCBmcmVlcyBhbGwgcmVmZXJlbmNlcy5cbiAgICovXG4gIGRlc3Ryb3koKSB7XG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgdGhpcy5fcmVzaXplSGFuZGxlcilcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm9yaWVudGF0aW9uY2hhbmdlXCIsIHRoaXMuX3Jlc2l6ZUhhbmRsZXIpXG5cbiAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgdGhpcy5fa2V5ZG93bkhhbmRsZXIpXG4gICAgdGhpcy5fc2xpZGVBcmVhLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZWRvd25cIiwgdGhpcy5faGFuZGxlVG91Y2hzdGFydClcbiAgICB0aGlzLl9zbGlkZUFyZWEucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInRvdWNoc3RhcnRcIiwgdGhpcy5faGFuZGxlVG91Y2hzdGFydClcblxuICAgIHRoaXMuX2JyZWFrcG9pbnRQaG9uZS5yZW1vdmUoKVxuICAgIHRoaXMuX2JyZWFrcG9pbnRUYWJsZXQucmVtb3ZlKClcbiAgICB0aGlzLl9icmVha3BvaW50RGVza3RvcC5yZW1vdmUoKVxuXG4gICAgaWYgKHRoaXMuX3ByZXZDdHJsICYmIHRoaXMuX25leHRDdHJsKSB7XG4gICAgICB0aGlzLl9wcmV2Q3RybC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fcHJldkhhbmRsZXIpXG4gICAgICB0aGlzLl9uZXh0Q3RybC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fbmV4dEhhbmRsZXIpXG4gICAgfVxuXG4gICAgKHRoaXMgYXMgYW55KS5fcHJldkN0cmwgPSB1bmRlZmluZWQ7XG4gICAgKHRoaXMgYXMgYW55KS5fbmV4dEN0cmwgPSB1bmRlZmluZWRcblxuICAgIGlmICh0aGlzLl9wYWdpbmF0aW9uKSB7XG4gICAgICB0aGlzLl9wYWdpbmF0aW9uLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9wYWdpbmF0aW9uQ2xpY2tIYW5kbGVyKTtcbiAgICAgICh0aGlzIGFzIGFueSkuX3BhZ2luYXRpb24gPSB1bmRlZmluZWRcbiAgICB9XG5cbiAgICB0aGlzLl9zbGlkZXJXcmFwcGVyLmRlc3Ryb3koKTtcbiAgICAodGhpcyBhcyBhbnkpLl9zbGlkZXJXcmFwcGVyID0gdW5kZWZpbmVkXG4gIH1cbn1cblxuY29uc3QgVFJBTlNGT1JNID0gXCJ0cmFuc2Zvcm1cIlxuY29uc3QgRFVSQVRJT04gPSBcInRyYW5zaXRpb25EdXJhdGlvblwiXG5jb25zdCBUSU1JTkcgPSBcInRyYW5zaXRpb25UaW1pbmdGdW5jdGlvblwiXG5cbmNsYXNzIFNsaWRlcldyYXBwZXIge1xuICBwcml2YXRlIF93cmFwcGVyRWxlbWVudDogSFRNTEVsZW1lbnRcbiAgcHJpdmF0ZSBfc2xpZGVBcmVhRWxlbWVudDogSFRNTEVsZW1lbnRcbiAgcHJpdmF0ZSBfY2Fyb3VzZWxFbGVtZW50OiBIVE1MRWxlbWVudFxuXG4gIHByaXZhdGUgX3Bvc2l0aW9uOiBudW1iZXJcbiAgcHJpdmF0ZSBfaW5kZXg6IG51bWJlclxuXG4gIHByaXZhdGUgX2lzZHJhZ2dpbmc6IGJvb2xlYW5cbiAgcHJpdmF0ZSBfZHJhZ1N0YXJ0UG9zaXRpb24/OiBudW1iZXJcblxuICBwcml2YXRlIF9hcmVhT2Zmc2V0PzogbnVtYmVyXG5cbiAgcHJpdmF0ZSBfc2xpZGVzUGVyR3JvdXAhOiBudW1iZXJcblxuICBwcml2YXRlIF9jb250YWluZXJNaW4hOiBudW1iZXJcbiAgcHJpdmF0ZSBfY29udGFpbmVyTWF4ITogbnVtYmVyXG5cbiAgY29uc3RydWN0b3Iod3JhcHBlckVsZW1lbnQ6IEhUTUxFbGVtZW50LCBzbGlkZUFyZWFFbGVtZW50OiBIVE1MRWxlbWVudCwgY2Fyb3VzZWxFbGVtZW50OiBIVE1MRWxlbWVudCkge1xuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50ID0gd3JhcHBlckVsZW1lbnRcbiAgICB0aGlzLl9zbGlkZUFyZWFFbGVtZW50ID0gc2xpZGVBcmVhRWxlbWVudFxuICAgIHRoaXMuX2Nhcm91c2VsRWxlbWVudCA9IGNhcm91c2VsRWxlbWVudFxuXG4gICAgdGhpcy5fcG9zaXRpb24gPSAwXG4gICAgdGhpcy5faW5kZXggPSAwXG4gICAgdGhpcy5faXNkcmFnZ2luZyA9IGZhbHNlXG4gIH1cblxuICBwcm90ZWN0ZWQgX2dldFNsaWRlKGluZGV4OiBudW1iZXIpIHtcbiAgICBpZiAoaW5kZXggPCAwIHx8IGluZGV4ID49IHRoaXMuX3dyYXBwZXJFbGVtZW50LmNoaWxkcmVuLmxlbmd0aCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBBcmd1bWVudCAnaW5kZXgnIGlzIG91dCBvZiByYW5nZSwgVmFsdWU6ICR7aW5kZXh9IE1pbjogMCwgTWF4OiAke3RoaXMuX3dyYXBwZXJFbGVtZW50LmNoaWxkcmVuLmxlbmd0aCAtIDF9YClcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5fd3JhcHBlckVsZW1lbnQuY2hpbGRyZW5baW5kZXhdIGFzIEhUTUxFbGVtZW50XG4gIH1cblxuICBwcm90ZWN0ZWQgX3NldFRyYW5zZm9ybSh0YXJnZXRQb3NpdGlvbjogbnVtYmVyLCBhbmltYXRlZCA9IGZhbHNlLCBkdXJhdGlvbiA9IEFOSU1BVElPTl9EVVJBVElPTiwgZWFzZSA9IEFOSU1BVElPTl9FQVNJTkcpIHtcbiAgICBpZiAoYW5pbWF0ZWQgPT09IGZhbHNlKSB7XG4gICAgICBkdXJhdGlvbiA9IDBcbiAgICB9XG5cbiAgICBjb25zdCBzdHlsZSA9IHRoaXMuX3dyYXBwZXJFbGVtZW50LnN0eWxlXG4gICAgaWYgKHN0eWxlKSB7XG4gICAgICBzdHlsZVtEVVJBVElPTl0gPSBgJHtkdXJhdGlvbn1tc2BcbiAgICAgIHN0eWxlW1RJTUlOR10gPSBlYXNlXG5cbiAgICAgIC8vIE5vIHN1YiBwaXhlbCB0cmFuc2l0aW9ucy5cbiAgICAgIHRhcmdldFBvc2l0aW9uID0gTWF0aC5mbG9vcih0YXJnZXRQb3NpdGlvbilcblxuICAgICAgc3R5bGVbVFJBTlNGT1JNXSA9IGB0cmFuc2xhdGUoJHt0YXJnZXRQb3NpdGlvbn1weCwgMClgXG4gICAgICB0aGlzLl9wb3NpdGlvbiA9IHRhcmdldFBvc2l0aW9uXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9nZXRXcmFwcGVyU2xpZGVQb3NpdGlvbihpbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3Qgd3JhcHBlckNlbnRlciA9ICgwLjUgKiB0aGlzLl93cmFwcGVyRWxlbWVudC5vZmZzZXRXaWR0aClcbiAgICBjb25zdCBzbGlkZSA9IHRoaXMuX2dldFNsaWRlKGluZGV4KVxuXG4gICAgbGV0IHJlc3VsdCA9IDBcbiAgICAvLyBDYWxjdWxhdGUgdGhlIHBvc2l0aW9uIG9mIHRoZSBzbGlkZSAoY2VudGVyZWQpXG4gICAgaWYgKHRoaXMuX3NsaWRlc1Blckdyb3VwICUgMiA9PT0gMCkge1xuICAgICAgbGV0IHNsaWRlU3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShzbGlkZSlcbiAgICAgIGxldCBzbGlkZU1hcmdpbiA9IHNsaWRlU3R5bGUgPyBwYXJzZUludChzbGlkZVN0eWxlLm1hcmdpblJpZ2h0ISwgMTApIDogMFxuICAgICAgLy8gQ2VudGVyZWQgdG8gdGhlIHNwYWNlIGJldHdlZW4gdGhlIHR3byBjZW50ZXIgc2xpZGVzIG9mIHRoZSBncm91cFxuICAgICAgcmVzdWx0ID0gLXNsaWRlLm9mZnNldExlZnQgLSAoc2xpZGUuY2xpZW50V2lkdGgpICsgd3JhcHBlckNlbnRlciAtIHNsaWRlTWFyZ2luXG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3VsdCA9IC1zbGlkZS5vZmZzZXRMZWZ0IC0gKDAuNSAqIHNsaWRlLmNsaWVudFdpZHRoKSArIHdyYXBwZXJDZW50ZXJcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0XG4gIH1cblxuICBnZXQgcG9zaXRpb24oKSB7XG4gICAgcmV0dXJuIHRoaXMuX3Bvc2l0aW9uXG4gIH1cblxuICBnZXQgaW5kZXgoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2luZGV4XG4gIH1cblxuICBzZXQgaW5kZXgoaW5kZXg6IG51bWJlcikge1xuICAgIHRoaXMuX2luZGV4ID0gaW5kZXhcbiAgfVxuXG4gIHNldCBzbGlkZXNQZXJHcm91cCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgdGhpcy5fc2xpZGVzUGVyR3JvdXAgPSB2YWx1ZVxuICB9XG5cbiAgaW5pdGlhbGl6ZSgpIHtcbiAgICB0aGlzLm9ucmVzaXplKClcbiAgfVxuXG4gIG9ucmVzaXplKCkge1xuICAgIC8vIHVwZGF0ZSB0aGUgYXJlYSBvZmZzZXQgZm9yIHNsaWRlIHBvc2l0aW9uIGNhbGN1bGF0aW9uXG4gICAgdGhpcy5fYXJlYU9mZnNldCA9IHRoaXMuX3NsaWRlQXJlYUVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdFxuXG4gICAgLy8gR2V0IHRoZSBjb250YWluZXIgZGltZW5zaW9uc1xuICAgIGNvbnN0IGNvbnRhaW5lclJlY3QgPSB0aGlzLl9jYXJvdXNlbEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KClcbiAgICB0aGlzLl9jb250YWluZXJNaW4gPSBjb250YWluZXJSZWN0LmxlZnRcbiAgICB0aGlzLl9jb250YWluZXJNYXggPSBjb250YWluZXJSZWN0LnJpZ2h0XG4gIH1cblxuICBiZWdpbkRyYWcoKSB7XG4gICAgdGhpcy5faXNkcmFnZ2luZyA9IHRydWVcbiAgICB0aGlzLl9kcmFnU3RhcnRQb3NpdGlvbiA9IHRoaXMuX3Bvc2l0aW9uXG4gIH1cblxuICBjYW5jZWxEcmFnKCkge1xuICAgIHRoaXMuX2lzZHJhZ2dpbmcgPSBmYWxzZVxuICAgIHRoaXMuX3NldFRyYW5zZm9ybSh0aGlzLl9kcmFnU3RhcnRQb3NpdGlvbiEsIHRydWUsIEFOSU1BVElPTl9EVVJBVElPTiwgQU5JTUFUSU9OX0VBU0lORylcblxuICAgIHRoaXMuX2RyYWdTdGFydFBvc2l0aW9uID0gdW5kZWZpbmVkXG4gIH1cblxuICBlbmREcmFnKCkge1xuICAgIHRoaXMuX2lzZHJhZ2dpbmcgPSBmYWxzZVxuICAgIHRoaXMuX2RyYWdTdGFydFBvc2l0aW9uID0gdW5kZWZpbmVkXG4gIH1cblxuICBtb3ZlKGRlbHRhOiBudW1iZXIsIGFuaW1hdGVkID0gZmFsc2UsIGR1cmF0aW9uID0gQU5JTUFUSU9OX0RVUkFUSU9OLCBlYXNlID0gQU5JTUFUSU9OX0VBU0lORykge1xuICAgIGRlbHRhID0gTWF0aC50cnVuYyhkZWx0YSlcbiAgICBpZiAoTWF0aC5hYnMoZGVsdGEpIDw9IDApIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGxldCB0YXJnZXRQb3NpdGlvbiA9IHRoaXMuX3Bvc2l0aW9uICs9IGRlbHRhXG4gICAgdGhpcy5fc2V0VHJhbnNmb3JtKHRhcmdldFBvc2l0aW9uLCBhbmltYXRlZCwgZHVyYXRpb24sIGVhc2UpXG4gIH1cblxuICBtb3ZlVG8oaW5kZXg6IG51bWJlciwgZGVsdGE/OiBudW1iZXIsIGFuaW1hdGVkID0gZmFsc2UpIHtcbiAgICBsZXQgbmV3UG9zaXRpb24gPSAwXG4gICAgaWYgKCFkZWx0YSkge1xuICAgICAgbmV3UG9zaXRpb24gPSB0aGlzLl9nZXRXcmFwcGVyU2xpZGVQb3NpdGlvbihpbmRleClcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3UG9zaXRpb24gPSB0aGlzLl9wb3NpdGlvbiArPSBkZWx0YVxuICAgIH1cblxuICAgIHRoaXMuX2luZGV4ID0gaW5kZXhcbiAgICB0aGlzLl9zZXRUcmFuc2Zvcm0obmV3UG9zaXRpb24sIGFuaW1hdGVkKVxuICB9XG5cbiAgYWRkU2xpZGUoc2xpZGU6IEhUTUxFbGVtZW50LCBwb3NpdGlvbjogbnVtYmVyKSB7XG4gICAgaWYgKCFzbGlkZSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGFkZCBhbiB1bmRlZmluZWQgc2xpZGVcIilcbiAgICB9XG5cbiAgICBpZiAocG9zaXRpb24gIT09IC0xICYmIHBvc2l0aW9uICE9PSAxKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYEFyZ3VtZW50IG91dCBvZiByYW5nZSwgJ3Bvc2l0aW9uJyBtdXN0IGJlIGVpdGhlciAxIG9yIC0xLiBWYWx1ZSAke3Bvc2l0aW9ufWApXG4gICAgfVxuXG4gICAgaWYgKHBvc2l0aW9uID4gMCkge1xuICAgICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuYXBwZW5kQ2hpbGQoc2xpZGUpXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50Lmluc2VydEJlZm9yZShzbGlkZSwgdGhpcy5fd3JhcHBlckVsZW1lbnQuY2hpbGRyZW5bMF0pXG4gICAgICB0aGlzLl9pbmRleCsrXG4gICAgfVxuXG4gICAgaWYgKHBvc2l0aW9uIDwgMCkge1xuICAgICAgbGV0IHdpZHRoID0gc2xpZGUub2Zmc2V0V2lkdGhcblxuICAgICAgbGV0IHN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoc2xpZGUpXG4gICAgICBsZXQgbWFyZ2luTGVmdCA9IHN0eWxlID8gcGFyc2VJbnQoc3R5bGUubWFyZ2luTGVmdCEsIDEwKSA6IDBcbiAgICAgIGxldCBtYXJnaW5SaWdodCA9IHN0eWxlID8gcGFyc2VJbnQoc3R5bGUubWFyZ2luUmlnaHQhLCAxMCkgOiAwXG5cbiAgICAgIHRoaXMubW92ZSgtKHdpZHRoICsgbWFyZ2luTGVmdCArIG1hcmdpblJpZ2h0KSlcbiAgICB9XG4gIH1cblxuICByZW1vdmVTbGlkZShpbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3Qgc2xpZGUgPSB0aGlzLl9nZXRTbGlkZShpbmRleClcbiAgICBsZXQgd2lkdGggPSBzbGlkZS5vZmZzZXRXaWR0aFxuXG4gICAgaWYgKGluZGV4IDw9IHRoaXMuX2luZGV4KSB7XG4gICAgICB3aWR0aCAqPSAtMVxuICAgICAgdGhpcy5faW5kZXgtLVxuICAgIH1cblxuICAgIHJlbW92ZShzbGlkZSlcblxuICAgIGlmICh3aWR0aCA8IDApIHtcbiAgICAgIHRoaXMubW92ZSgtd2lkdGgpXG4gICAgfVxuICB9XG5cbiAgZ2V0U2xpZGVEZWx0YShpbmRleDogbnVtYmVyKSB7XG4gICAgbGV0IGN1cnJlbnRQb3NpdGlvbiA9IHRoaXMuX3Bvc2l0aW9uXG4gICAgaWYgKHRoaXMuX2lzZHJhZ2dpbmcgPT09IHRydWUpIHtcbiAgICAgIGN1cnJlbnRQb3NpdGlvbiA9IHRoaXMuX2RyYWdTdGFydFBvc2l0aW9uISAtIHRoaXMuX3Bvc2l0aW9uXG4gICAgfVxuXG4gICAgY29uc3QgbmV3UG9zaXRpb24gPSB0aGlzLl9nZXRXcmFwcGVyU2xpZGVQb3NpdGlvbihpbmRleClcbiAgICByZXR1cm4gbmV3UG9zaXRpb24gLSBjdXJyZW50UG9zaXRpb25cbiAgfVxuXG4gIGdldFNsaWRlUHJvcGVydGllcyhpbmRleDogbnVtYmVyLCBkZWx0YSA9IDApOiBTbGlkZVByb3BlcnRpZXMge1xuICAgIGxldCBjdXJyZW50T2Zmc2V0ID0gdGhpcy5fYXJlYU9mZnNldCEgKyB0aGlzLl9wb3NpdGlvbiArIGRlbHRhXG4gICAgbGV0IGN1cnJlbnRMZWZ0ID0gY3VycmVudE9mZnNldFxuICAgIGxldCBjdXJyZW50UmlnaHQgPSBjdXJyZW50T2Zmc2V0XG4gICAgbGV0IFsgY3VycmVudE1hcmdpbkxlZnQsIGN1cnJlbnRNYXJnaW5SaWdodCBdID0gWyAwLCAwIF1cblxuICAgIGxldCBzbGlkZSA9IHRoaXMuX2dldFNsaWRlKGluZGV4KVxuICAgIGxldCBzbGlkZUluZGV4ID0gcGFyc2VJbnQoc2xpZGUuZ2V0QXR0cmlidXRlKEFUVFJJQlVURV9JTkRFWCkhLCAxMClcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDw9IGluZGV4OyBpKyspIHtcbiAgICAgIHNsaWRlID0gdGhpcy5fZ2V0U2xpZGUoaSlcbiAgICAgIGxldCBzbGlkZVN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoc2xpZGUpXG5cbiAgICAgIGN1cnJlbnRNYXJnaW5MZWZ0ID0gcGFyc2VJbnQoc2xpZGVTdHlsZS5tYXJnaW5MZWZ0ISwgMTApXG4gICAgICBjdXJyZW50TWFyZ2luUmlnaHQgPSBwYXJzZUludChzbGlkZVN0eWxlLm1hcmdpblJpZ2h0ISwgMTApXG5cbiAgICAgIGN1cnJlbnRPZmZzZXQgKz0gY3VycmVudE1hcmdpbkxlZnRcbiAgICAgIGN1cnJlbnRMZWZ0ID0gY3VycmVudE9mZnNldFxuICAgICAgY3VycmVudFJpZ2h0ID0gY3VycmVudExlZnQgKyBzbGlkZS5vZmZzZXRXaWR0aFxuXG4gICAgICBpZiAoaSA8IGluZGV4KSB7XG4gICAgICAgIGN1cnJlbnRPZmZzZXQgPSBjdXJyZW50UmlnaHQgKyBjdXJyZW50TWFyZ2luUmlnaHRcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsZXQgdmlzaWJsZSA9IGZhbHNlXG4gICAgaWYgKChjdXJyZW50TGVmdCA+IHRoaXMuX2NvbnRhaW5lck1pbiAmJiBjdXJyZW50TGVmdCA8IHRoaXMuX2NvbnRhaW5lck1heCkgfHxcbiAgICAgIChjdXJyZW50UmlnaHQgPiB0aGlzLl9jb250YWluZXJNaW4gJiYgY3VycmVudFJpZ2h0IDwgdGhpcy5fY29udGFpbmVyTWF4KSkge1xuICAgICAgdmlzaWJsZSA9IHRydWVcbiAgICB9XG5cbiAgICByZXR1cm4ge1xuICAgICAgdmlzaWJsZSxcbiAgICAgIGluZGV4OiBzbGlkZUluZGV4LFxuICAgICAgbGVmdDogY3VycmVudExlZnQsXG4gICAgICByaWdodDogY3VycmVudFJpZ2h0LFxuICAgICAgd2lkdGg6IGN1cnJlbnRSaWdodCAtIGN1cnJlbnRMZWZ0LFxuICAgICAgbWFyZ2luTGVmdDogY3VycmVudE1hcmdpbkxlZnQsXG4gICAgICBtYXJnaW5SaWdodDogY3VycmVudE1hcmdpblJpZ2h0XG4gICAgfVxuICB9XG5cbiAgZ2V0UmVtb3ZhYmxlU2xpZGVzKGRlbHRhOiBudW1iZXIpIHtcbiAgICBsZXQgc2xpZGVzID0gW11cbiAgICBsZXQgZmlyc3Q6IFNsaWRlUHJvcGVydGllcyB8IHVuZGVmaW5lZFxuICAgIGxldCBsYXN0OiBTbGlkZVByb3BlcnRpZXMgfCB1bmRlZmluZWRcblxuICAgIGxldCBpbmRleCA9IHRoaXMuX3dyYXBwZXJFbGVtZW50LmNoaWxkcmVuLmxlbmd0aFxuICAgIHdoaWxlIChpbmRleCA+IDApIHtcbiAgICAgIGluZGV4LS1cblxuICAgICAgbGV0IHByb3BzTm93ID0gdGhpcy5nZXRTbGlkZVByb3BlcnRpZXMoaW5kZXgpXG4gICAgICBsZXQgcHJvcHNOZXcgPSB0aGlzLmdldFNsaWRlUHJvcGVydGllcyhpbmRleCwgZGVsdGEpXG5cbiAgICAgIGlmIChpbmRleCA9PT0gdGhpcy5fd3JhcHBlckVsZW1lbnQuY2hpbGRyZW4ubGVuZ3RoIC0gMSkge1xuICAgICAgICBsYXN0ID0gcHJvcHNOZXdcbiAgICAgIH1cblxuICAgICAgaWYgKGluZGV4ID09PSAwKSB7XG4gICAgICAgIGZpcnN0ID0gcHJvcHNOZXdcbiAgICAgIH1cblxuICAgICAgaWYgKHByb3BzTm93LnZpc2libGUgPT09IGZhbHNlICYmIHByb3BzTmV3LnZpc2libGUgPT09IGZhbHNlICYmXG4gICAgICAgIGluZGV4ICE9PSB0aGlzLl9pbmRleCAmJiB0aGlzLl9pc2RyYWdnaW5nID09PSBmYWxzZSkge1xuICAgICAgICBzbGlkZXMucHVzaCh0cnVlKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc2xpZGVzLnB1c2goZmFsc2UpXG4gICAgICB9XG4gICAgfVxuXG4gICAgc2xpZGVzLnJldmVyc2UoKVxuXG4gICAgbGV0IGZpcnN0VG9LZWVwID0gc2xpZGVzLmluZGV4T2YoZmFsc2UpXG4gICAgbGV0IGxhc3RUb0tlZXAgPSBzbGlkZXMubGFzdEluZGV4T2YoZmFsc2UpXG5cbiAgICBmb3IgKGxldCBpID0gZmlyc3RUb0tlZXA7IGkgPCBsYXN0VG9LZWVwOyBpKyspIHtcbiAgICAgIHNsaWRlc1tpXSA9IGZhbHNlXG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgIHNsaWRlcyxcbiAgICAgIGZpcnN0OiBmaXJzdCBhcyBTbGlkZVByb3BlcnRpZXMsXG4gICAgICBsYXN0OiBsYXN0IGFzIFNsaWRlUHJvcGVydGllc1xuICAgIH1cbiAgfVxuXG4gIGdldEVtcHR5U3BhY2UobGVmdDogbnVtYmVyLCByaWdodDogbnVtYmVyKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxlZnQ6IE1hdGgubWF4KE1hdGguY2VpbChsZWZ0IC0gdGhpcy5fY29udGFpbmVyTWluKSwgMCksXG4gICAgICByaWdodDogTWF0aC5tYXgoTWF0aC5jZWlsKHRoaXMuX2NvbnRhaW5lck1heCAtIHJpZ2h0KSwgMClcbiAgICB9XG4gIH1cblxuICBkZXN0cm95KCkge1xuICAgICh0aGlzIGFzIGFueSkuX3dyYXBwZXJFbGVtZW50ID0gbnVsbDtcbiAgICAodGhpcyBhcyBhbnkpLl9zbGlkZUFyZWFFbGVtZW50ID0gbnVsbDtcbiAgICAodGhpcyBhcyBhbnkpLl9jYXJvdXNlbEVsZW1lbnQgPSBudWxsXG4gIH1cblxuICAvKipcbiAgICogQGRlcHJlY2F0ZWQgdXNlIGRlc3Ryb3koKSBpbnN0ZWFkLlxuICAgKiBAdG9kbyByZW1vdmUgaW4gdmVyc2lvbiAyLjAuMFxuICAgKi9cbiAgZGVzdG9yeSgpIHtcbiAgICB0aGlzLmRlc3Ryb3koKVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0KCkge1xuICBzZWFyY2hBbmRJbml0aWFsaXplKFwiLmNhcm91c2VsXCIsIChlKSA9PiB7XG4gICAgbmV3IENhcm91c2VsKGUgYXMgSFRNTEVsZW1lbnQpXG4gIH0pXG59XG5cbmV4cG9ydCBkZWZhdWx0IENhcm91c2VsXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
