import * as tslib_1 from "tslib";
import { preventDefault } from "../Utils";
import DomElement from "../DomElement";
var CLASS_NOTIFICATION = "notification-header";
var CLASS_OPEN = "notification--open";
var CLASS_BUTTON_CLOSE = "notification__close";
/**
 * Creates and shows a notification with the specified message.
 * @memberof Notification
 * @param {String} containerId - The id of the container on where to show the notification.
 * @param {String} message - The message to show.
 * @param {Notification~Click} messageClickCallback - The callback that gets called when the user clicks on the notification message text.
 * @param {Notification~Cancel} cancelCallback - The callback that gets called when the user cancels the notification by closing it.
 * @param {String} modifierClass - The css modifier class for the notification; this is an optional parameter
 * @returns {NotificationHeader} The notification header item instance.
 */
export function showOnHeader(containerId, message, messageClickCallback, cancelCallback, modifierClass) {
    var containerE = document.querySelector("#" + containerId);
    if (!containerE) {
        throw new Error("Could not find the container with id " + containerId);
    }
    var containerElement = new DomElement(containerE);
    var notificationElement = new NotificationHeader();
    if (modifierClass) {
        notificationElement.addClass(modifierClass);
    }
    notificationElement.message = message;
    notificationElement.messageClickCallback = messageClickCallback;
    notificationElement.cancelCallback = cancelCallback;
    containerElement.appendChild(notificationElement);
    notificationElement._open();
    return notificationElement;
}
/**
 * A component for displaying notifications on the page-header.
 * @inner
 * @memberof Notification
 */
var NotificationHeader = /** @class */ (function (_super) {
    tslib_1.__extends(NotificationHeader, _super);
    function NotificationHeader() {
        var _this = _super.call(this, "div") || this;
        _this._closeHandler = _this._handleClose.bind(_this);
        _this._clickHandler = _this._handleClick.bind(_this);
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the range modal component.
     * @private
     */
    NotificationHeader.prototype._initialize = function () {
        this.addClass(CLASS_NOTIFICATION);
        this.addClass(CLASS_OPEN);
        var notificationContent = new DomElement("div")
            .addClass("notification__content");
        this.appendChild(notificationContent);
        this._notificationBody = new DomElement("div")
            .addClass("notification__body");
        notificationContent.appendChild(this._notificationBody);
        this._closeButton = new DomElement("button")
            .addClass(CLASS_BUTTON_CLOSE)
            .addClass("notification-cancel")
            .setAttribute("aria-label", "Close");
        var closeIcon = new DomElement("i")
            .addClass("icon")
            .addClass("icon-022-close")
            .setAttribute("aria-hidden", "true");
        this._closeButton.appendChild(closeIcon);
        notificationContent.appendChild(this._closeButton);
        this.element.addEventListener("click", this._clickHandler);
    };
    NotificationHeader.prototype._handleClick = function (event) {
        preventDefault(event);
        var closeNotification = true;
        if (this._callback) {
            if (this._callback(this) === false) {
                closeNotification = false;
            }
        }
        if (closeNotification === true) {
            this.close();
        }
    };
    NotificationHeader.prototype._handleClose = function (event) {
        preventDefault(event);
        event.stopPropagation();
        if (this._cancelCallback) {
            this._cancelCallback(this);
        }
        this.close();
    };
    NotificationHeader.prototype._close = function () {
        this.removeClass(CLASS_OPEN);
        this._closeButton.element.removeEventListener("click", this._closeHandler);
        var el = this.element;
        setTimeout(function () {
            // remove the element from the dom
            if (el && el.parentElement) {
                el.parentElement.removeChild(el);
            }
        }, 300);
    };
    // called by showOnHeader
    NotificationHeader.prototype._open = function () {
        this.addClass(CLASS_OPEN);
        this._closeButton.element.addEventListener("click", this._closeHandler);
        this.dispatchEvent("opened");
    };
    Object.defineProperty(NotificationHeader.prototype, "messageClickCallback", {
        set: function (callback) {
            this._callback = callback;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NotificationHeader.prototype, "cancelCallback", {
        /**
         * Sets the cancel callback function.
         * @param {function} - The callback function to call.
         */
        set: function (callback) {
            this._cancelCallback = callback;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NotificationHeader.prototype, "message", {
        /**
         * Sets the notification message.
         * @param {String} - The message to set.
         */
        set: function (value) {
            this._notificationBody.setHtml(value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Closes the notification.
     */
    NotificationHeader.prototype.close = function () {
        this._close();
        this.dispatchEvent("closed");
    };
    return NotificationHeader;
}(DomElement));
export { NotificationHeader };

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL25vdGlmaWNhdGlvbi9Ob3RpZmljYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxVQUFVLENBQUE7QUFDekMsT0FBTyxVQUFVLE1BQU0sZUFBZSxDQUFBO0FBRXRDLElBQU0sa0JBQWtCLEdBQUcscUJBQXFCLENBQUE7QUFFaEQsSUFBTSxVQUFVLEdBQUcsb0JBQW9CLENBQUE7QUFDdkMsSUFBTSxrQkFBa0IsR0FBRyxxQkFBcUIsQ0FBQTtBQTZCaEQ7Ozs7Ozs7OztHQVNHO0FBQ0gsTUFBTSxVQUFVLFlBQVksQ0FDMUIsV0FBbUIsRUFDbkIsT0FBZSxFQUNmLG9CQUEyQyxFQUMzQyxjQUErQixFQUMvQixhQUFzQjtJQUd0QixJQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQUksV0FBYSxDQUFDLENBQUE7SUFDNUQsSUFBSSxDQUFDLFVBQVUsRUFBRTtRQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQXdDLFdBQWEsQ0FBQyxDQUFBO0tBQ3ZFO0lBRUQsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQTtJQUNuRCxJQUFNLG1CQUFtQixHQUFHLElBQUksa0JBQWtCLEVBQUUsQ0FBQTtJQUVwRCxJQUFJLGFBQWEsRUFBRTtRQUNqQixtQkFBbUIsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUE7S0FDNUM7SUFFRCxtQkFBbUIsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBO0lBQ3JDLG1CQUFtQixDQUFDLG9CQUFvQixHQUFHLG9CQUFvQixDQUFBO0lBQy9ELG1CQUFtQixDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUE7SUFFbkQsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUE7SUFDakQsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUE7SUFFM0IsT0FBTyxtQkFBbUIsQ0FBQTtBQUM1QixDQUFDO0FBRUQ7Ozs7R0FJRztBQUNIO0lBQXdDLDhDQUFVO0lBVWhEO1FBQUEsWUFDRSxrQkFBTSxLQUFLLENBQUMsU0FNYjtRQUpDLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDakQsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUVqRCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7O0lBQ3BCLENBQUM7SUFFRDs7O09BR0c7SUFDTyx3Q0FBVyxHQUFyQjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtRQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRXpCLElBQU0sbUJBQW1CLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO2FBQzlDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO1FBRXBDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUVyQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO2FBQzNDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1FBRWpDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtRQUV2RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQzthQUN6QyxRQUFRLENBQUMsa0JBQWtCLENBQUM7YUFDNUIsUUFBUSxDQUFDLHFCQUFxQixDQUFDO2FBQy9CLFlBQVksQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFFdEMsSUFBTSxTQUFTLEdBQUcsSUFBSSxVQUFVLENBQUMsR0FBRyxDQUFDO2FBQ2xDLFFBQVEsQ0FBQyxNQUFNLENBQUM7YUFDaEIsUUFBUSxDQUFDLGdCQUFnQixDQUFDO2FBQzFCLFlBQVksQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFdEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDeEMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUVsRCxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7SUFDNUQsQ0FBQztJQUVTLHlDQUFZLEdBQXRCLFVBQXVCLEtBQWlCO1FBQ3RDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUVyQixJQUFJLGlCQUFpQixHQUFHLElBQUksQ0FBQTtRQUM1QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssRUFBRTtnQkFDbEMsaUJBQWlCLEdBQUcsS0FBSyxDQUFBO2FBQzFCO1NBQ0Y7UUFFRCxJQUFJLGlCQUFpQixLQUFLLElBQUksRUFBRTtZQUM5QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7U0FDYjtJQUNILENBQUM7SUFFUyx5Q0FBWSxHQUF0QixVQUF1QixLQUFZO1FBQ2pDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNyQixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUE7UUFFdkIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUE7U0FDM0I7UUFFRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7SUFDZCxDQUFDO0lBRVMsbUNBQU0sR0FBaEI7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBQzVCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFFMUUsSUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtRQUN2QixVQUFVLENBQUM7WUFDVCxrQ0FBa0M7WUFDbEMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLGFBQWEsRUFBRTtnQkFDMUIsRUFBRSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUE7YUFDakM7UUFDSCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUE7SUFDVCxDQUFDO0lBRUQseUJBQXlCO0lBQ3pCLGtDQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRXpCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDdkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBRUQsc0JBQUksb0RBQW9CO2FBQXhCLFVBQXlCLFFBQTBDO1lBQ2pFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFBO1FBQzNCLENBQUM7OztPQUFBO0lBTUQsc0JBQUksOENBQWM7UUFKbEI7OztXQUdHO2FBQ0gsVUFBbUIsUUFBb0M7WUFDckQsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUE7UUFDakMsQ0FBQzs7O09BQUE7SUFNRCxzQkFBSSx1Q0FBTztRQUpYOzs7V0FHRzthQUNILFVBQVksS0FBYTtZQUN2QixJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3ZDLENBQUM7OztPQUFBO0lBRUQ7O09BRUc7SUFDSCxrQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO1FBQ2IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBQ0gseUJBQUM7QUFBRCxDQS9IQSxBQStIQyxDQS9IdUMsVUFBVSxHQStIakQiLCJmaWxlIjoibWFpbi9zcmMvbm90aWZpY2F0aW9uL05vdGlmaWNhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHByZXZlbnREZWZhdWx0IH0gZnJvbSBcIi4uL1V0aWxzXCJcbmltcG9ydCBEb21FbGVtZW50IGZyb20gXCIuLi9Eb21FbGVtZW50XCJcblxuY29uc3QgQ0xBU1NfTk9USUZJQ0FUSU9OID0gXCJub3RpZmljYXRpb24taGVhZGVyXCJcblxuY29uc3QgQ0xBU1NfT1BFTiA9IFwibm90aWZpY2F0aW9uLS1vcGVuXCJcbmNvbnN0IENMQVNTX0JVVFRPTl9DTE9TRSA9IFwibm90aWZpY2F0aW9uX19jbG9zZVwiXG5cbi8qKlxuICogTm90aWZpY2F0aW9uIGNvbXBvbmVudC5cbiAqIEBuYW1lc3BhY2UgTm90aWZpY2F0aW9uXG4gKi9cblxuLyoqXG4gKiBUaGUgbWVzc2FnZSBjbGljayBjYWxsYmFjayBmdW5jdGlvbi5cbiAqIEBtZW1iZXJvZiBOb3RpZmljYXRpb25cbiAqIEBjYWxsYmFjayBOb3RpZmljYXRpb25+Q2xpY2tcbiAqIEBwcm9wZXJ0eSB7Tm90aWZpY2F0aW9uSGVhZGVyfSBpdGVtIC0gVGhlIGN1cnJlbnQgbm90aWZpY2F0aW9uIGhlYWRlciBpbnN0YW5jZS5cbiAqIEByZXR1cm5zIHtib29sZWFufSBSZXR1cm4gdHJ1ZSBpZiB0aGUgbm90aWZpY2F0aW9uIHNob3VsZCBiZSBjbG9zZWQ7IHJldHVybiBmYWxzZSBpZiB0aGVcbiAqICAgICAgICAgICAgICAgICAgICBub3RpZmljYXRpb24gc2hvdWxkIHJlbWFpbiBvcGVuLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIE1lc3NhZ2VDbGlja0NhbGxiYWNrIHtcbiAgKGhlYWRlcjogTm90aWZpY2F0aW9uSGVhZGVyKTogYm9vbGVhbiB8IHVuZGVmaW5lZFxufVxuXG4vKipcbiAqIFRoZSBjYW5jZWwgY2FsbGJhY2sgZnVuY3Rpb24uXG4gKiBAbWVtYmVyb2YgTm90aWZpY2F0aW9uXG4gKiBAY2FsbGJhY2sgTm90aWZpY2F0aW9ufkNhbmNlbFxuICogQHByb3BlcnR5IHtOb3RpZmljYXRpb25IZWFkZXJ9IGl0ZW0gLSBUaGUgY3VycmVudCBub3RpZmljYXRpb24gaGVhZGVyIGluc3RhbmNlLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIENhbmNlbENhbGxiYWNrIHtcbiAgKGhlYWRlcjogTm90aWZpY2F0aW9uSGVhZGVyKTogdm9pZFxufVxuXG4vKipcbiAqIENyZWF0ZXMgYW5kIHNob3dzIGEgbm90aWZpY2F0aW9uIHdpdGggdGhlIHNwZWNpZmllZCBtZXNzYWdlLlxuICogQG1lbWJlcm9mIE5vdGlmaWNhdGlvblxuICogQHBhcmFtIHtTdHJpbmd9IGNvbnRhaW5lcklkIC0gVGhlIGlkIG9mIHRoZSBjb250YWluZXIgb24gd2hlcmUgdG8gc2hvdyB0aGUgbm90aWZpY2F0aW9uLlxuICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgLSBUaGUgbWVzc2FnZSB0byBzaG93LlxuICogQHBhcmFtIHtOb3RpZmljYXRpb25+Q2xpY2t9IG1lc3NhZ2VDbGlja0NhbGxiYWNrIC0gVGhlIGNhbGxiYWNrIHRoYXQgZ2V0cyBjYWxsZWQgd2hlbiB0aGUgdXNlciBjbGlja3Mgb24gdGhlIG5vdGlmaWNhdGlvbiBtZXNzYWdlIHRleHQuXG4gKiBAcGFyYW0ge05vdGlmaWNhdGlvbn5DYW5jZWx9IGNhbmNlbENhbGxiYWNrIC0gVGhlIGNhbGxiYWNrIHRoYXQgZ2V0cyBjYWxsZWQgd2hlbiB0aGUgdXNlciBjYW5jZWxzIHRoZSBub3RpZmljYXRpb24gYnkgY2xvc2luZyBpdC5cbiAqIEBwYXJhbSB7U3RyaW5nfSBtb2RpZmllckNsYXNzIC0gVGhlIGNzcyBtb2RpZmllciBjbGFzcyBmb3IgdGhlIG5vdGlmaWNhdGlvbjsgdGhpcyBpcyBhbiBvcHRpb25hbCBwYXJhbWV0ZXJcbiAqIEByZXR1cm5zIHtOb3RpZmljYXRpb25IZWFkZXJ9IFRoZSBub3RpZmljYXRpb24gaGVhZGVyIGl0ZW0gaW5zdGFuY2UuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzaG93T25IZWFkZXIoXG4gIGNvbnRhaW5lcklkOiBzdHJpbmcsXG4gIG1lc3NhZ2U6IHN0cmluZyxcbiAgbWVzc2FnZUNsaWNrQ2FsbGJhY2s/OiBNZXNzYWdlQ2xpY2tDYWxsYmFjayxcbiAgY2FuY2VsQ2FsbGJhY2s/OiBDYW5jZWxDYWxsYmFjayxcbiAgbW9kaWZpZXJDbGFzcz86IHN0cmluZ1xuKSB7XG5cbiAgY29uc3QgY29udGFpbmVyRSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCMke2NvbnRhaW5lcklkfWApXG4gIGlmICghY29udGFpbmVyRSkge1xuICAgIHRocm93IG5ldyBFcnJvcihgQ291bGQgbm90IGZpbmQgdGhlIGNvbnRhaW5lciB3aXRoIGlkICR7Y29udGFpbmVySWR9YClcbiAgfVxuXG4gIGNvbnN0IGNvbnRhaW5lckVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChjb250YWluZXJFKVxuICBjb25zdCBub3RpZmljYXRpb25FbGVtZW50ID0gbmV3IE5vdGlmaWNhdGlvbkhlYWRlcigpXG5cbiAgaWYgKG1vZGlmaWVyQ2xhc3MpIHtcbiAgICBub3RpZmljYXRpb25FbGVtZW50LmFkZENsYXNzKG1vZGlmaWVyQ2xhc3MpXG4gIH1cblxuICBub3RpZmljYXRpb25FbGVtZW50Lm1lc3NhZ2UgPSBtZXNzYWdlXG4gIG5vdGlmaWNhdGlvbkVsZW1lbnQubWVzc2FnZUNsaWNrQ2FsbGJhY2sgPSBtZXNzYWdlQ2xpY2tDYWxsYmFja1xuICBub3RpZmljYXRpb25FbGVtZW50LmNhbmNlbENhbGxiYWNrID0gY2FuY2VsQ2FsbGJhY2tcblxuICBjb250YWluZXJFbGVtZW50LmFwcGVuZENoaWxkKG5vdGlmaWNhdGlvbkVsZW1lbnQpXG4gIG5vdGlmaWNhdGlvbkVsZW1lbnQuX29wZW4oKVxuXG4gIHJldHVybiBub3RpZmljYXRpb25FbGVtZW50XG59XG5cbi8qKlxuICogQSBjb21wb25lbnQgZm9yIGRpc3BsYXlpbmcgbm90aWZpY2F0aW9ucyBvbiB0aGUgcGFnZS1oZWFkZXIuXG4gKiBAaW5uZXJcbiAqIEBtZW1iZXJvZiBOb3RpZmljYXRpb25cbiAqL1xuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbkhlYWRlciBleHRlbmRzIERvbUVsZW1lbnQge1xuICBwcml2YXRlIF9jbG9zZUhhbmRsZXI6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfY2xpY2tIYW5kbGVyOiAoZXZlbnQ6IEV2ZW50KSA9PiB2b2lkXG5cbiAgcHJpdmF0ZSBfY2FsbGJhY2s/OiBNZXNzYWdlQ2xpY2tDYWxsYmFja1xuICBwcml2YXRlIF9jYW5jZWxDYWxsYmFjaz86IENhbmNlbENhbGxiYWNrXG5cbiAgcHJpdmF0ZSBfY2xvc2VCdXR0b24hOiBEb21FbGVtZW50PEVsZW1lbnQ+XG4gIHByaXZhdGUgX25vdGlmaWNhdGlvbkJvZHkhOiBEb21FbGVtZW50PEVsZW1lbnQ+XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgc3VwZXIoXCJkaXZcIilcblxuICAgIHRoaXMuX2Nsb3NlSGFuZGxlciA9IHRoaXMuX2hhbmRsZUNsb3NlLmJpbmQodGhpcylcbiAgICB0aGlzLl9jbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVDbGljay5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgcmFuZ2UgbW9kYWwgY29tcG9uZW50LlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9pbml0aWFsaXplKCkge1xuICAgIHRoaXMuYWRkQ2xhc3MoQ0xBU1NfTk9USUZJQ0FUSU9OKVxuICAgIHRoaXMuYWRkQ2xhc3MoQ0xBU1NfT1BFTilcblxuICAgIGNvbnN0IG5vdGlmaWNhdGlvbkNvbnRlbnQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKFwibm90aWZpY2F0aW9uX19jb250ZW50XCIpXG5cbiAgICB0aGlzLmFwcGVuZENoaWxkKG5vdGlmaWNhdGlvbkNvbnRlbnQpXG5cbiAgICB0aGlzLl9ub3RpZmljYXRpb25Cb2R5ID0gbmV3IERvbUVsZW1lbnQoXCJkaXZcIilcbiAgICAgIC5hZGRDbGFzcyhcIm5vdGlmaWNhdGlvbl9fYm9keVwiKVxuXG4gICAgbm90aWZpY2F0aW9uQ29udGVudC5hcHBlbmRDaGlsZCh0aGlzLl9ub3RpZmljYXRpb25Cb2R5KVxuXG4gICAgdGhpcy5fY2xvc2VCdXR0b24gPSBuZXcgRG9tRWxlbWVudChcImJ1dHRvblwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX0JVVFRPTl9DTE9TRSlcbiAgICAgIC5hZGRDbGFzcyhcIm5vdGlmaWNhdGlvbi1jYW5jZWxcIilcbiAgICAgIC5zZXRBdHRyaWJ1dGUoXCJhcmlhLWxhYmVsXCIsIFwiQ2xvc2VcIilcblxuICAgIGNvbnN0IGNsb3NlSWNvbiA9IG5ldyBEb21FbGVtZW50KFwiaVwiKVxuICAgICAgLmFkZENsYXNzKFwiaWNvblwiKVxuICAgICAgLmFkZENsYXNzKFwiaWNvbi0wMjItY2xvc2VcIilcbiAgICAgIC5zZXRBdHRyaWJ1dGUoXCJhcmlhLWhpZGRlblwiLCBcInRydWVcIilcblxuICAgIHRoaXMuX2Nsb3NlQnV0dG9uLmFwcGVuZENoaWxkKGNsb3NlSWNvbilcbiAgICBub3RpZmljYXRpb25Db250ZW50LmFwcGVuZENoaWxkKHRoaXMuX2Nsb3NlQnV0dG9uKVxuXG4gICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9jbGlja0hhbmRsZXIpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgcHJldmVudERlZmF1bHQoZXZlbnQpXG5cbiAgICBsZXQgY2xvc2VOb3RpZmljYXRpb24gPSB0cnVlXG4gICAgaWYgKHRoaXMuX2NhbGxiYWNrKSB7XG4gICAgICBpZiAodGhpcy5fY2FsbGJhY2sodGhpcykgPT09IGZhbHNlKSB7XG4gICAgICAgIGNsb3NlTm90aWZpY2F0aW9uID0gZmFsc2VcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoY2xvc2VOb3RpZmljYXRpb24gPT09IHRydWUpIHtcbiAgICAgIHRoaXMuY2xvc2UoKVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlQ2xvc2UoZXZlbnQ6IEV2ZW50KSB7XG4gICAgcHJldmVudERlZmF1bHQoZXZlbnQpXG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcblxuICAgIGlmICh0aGlzLl9jYW5jZWxDYWxsYmFjaykge1xuICAgICAgdGhpcy5fY2FuY2VsQ2FsbGJhY2sodGhpcylcbiAgICB9XG5cbiAgICB0aGlzLmNsb3NlKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfY2xvc2UoKSB7XG4gICAgdGhpcy5yZW1vdmVDbGFzcyhDTEFTU19PUEVOKVxuICAgIHRoaXMuX2Nsb3NlQnV0dG9uLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2Nsb3NlSGFuZGxlcilcblxuICAgIGNvbnN0IGVsID0gdGhpcy5lbGVtZW50XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAvLyByZW1vdmUgdGhlIGVsZW1lbnQgZnJvbSB0aGUgZG9tXG4gICAgICBpZiAoZWwgJiYgZWwucGFyZW50RWxlbWVudCkge1xuICAgICAgICBlbC5wYXJlbnRFbGVtZW50LnJlbW92ZUNoaWxkKGVsKVxuICAgICAgfVxuICAgIH0sIDMwMClcbiAgfVxuXG4gIC8vIGNhbGxlZCBieSBzaG93T25IZWFkZXJcbiAgX29wZW4oKSB7XG4gICAgdGhpcy5hZGRDbGFzcyhDTEFTU19PUEVOKVxuXG4gICAgdGhpcy5fY2xvc2VCdXR0b24uZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2xvc2VIYW5kbGVyKVxuICAgIHRoaXMuZGlzcGF0Y2hFdmVudChcIm9wZW5lZFwiKVxuICB9XG5cbiAgc2V0IG1lc3NhZ2VDbGlja0NhbGxiYWNrKGNhbGxiYWNrOiBNZXNzYWdlQ2xpY2tDYWxsYmFjayB8IHVuZGVmaW5lZCkge1xuICAgIHRoaXMuX2NhbGxiYWNrID0gY2FsbGJhY2tcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBjYW5jZWwgY2FsbGJhY2sgZnVuY3Rpb24uXG4gICAqIEBwYXJhbSB7ZnVuY3Rpb259IC0gVGhlIGNhbGxiYWNrIGZ1bmN0aW9uIHRvIGNhbGwuXG4gICAqL1xuICBzZXQgY2FuY2VsQ2FsbGJhY2soY2FsbGJhY2s6IENhbmNlbENhbGxiYWNrIHwgdW5kZWZpbmVkKSB7XG4gICAgdGhpcy5fY2FuY2VsQ2FsbGJhY2sgPSBjYWxsYmFja1xuICB9XG5cbiAgLyoqXG4gICAqIFNldHMgdGhlIG5vdGlmaWNhdGlvbiBtZXNzYWdlLlxuICAgKiBAcGFyYW0ge1N0cmluZ30gLSBUaGUgbWVzc2FnZSB0byBzZXQuXG4gICAqL1xuICBzZXQgbWVzc2FnZSh2YWx1ZTogc3RyaW5nKSB7XG4gICAgdGhpcy5fbm90aWZpY2F0aW9uQm9keS5zZXRIdG1sKHZhbHVlKVxuICB9XG5cbiAgLyoqXG4gICAqIENsb3NlcyB0aGUgbm90aWZpY2F0aW9uLlxuICAgKi9cbiAgY2xvc2UoKSB7XG4gICAgdGhpcy5fY2xvc2UoKVxuICAgIHRoaXMuZGlzcGF0Y2hFdmVudChcImNsb3NlZFwiKVxuICB9XG59XG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
