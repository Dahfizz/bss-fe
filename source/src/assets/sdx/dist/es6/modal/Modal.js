import * as tslib_1 from "tslib";
import { searchAndInitialize, preventDefault } from "../Utils";
import * as Inputs from "../Inputs";
import DomElement from "../DomElement";
import { getRootElement, addClass, removeClass } from "../DomFunctions";
var CLASS_BACKDROP = "backdrop";
var CLASS_BACKDROP_OPEN = "backdrop--open";
var CLASS_OPEN = "modal--open";
var CLASS_TRIGGER = "modal-trigger";
var CLASS_MODAL_OPEN = "js-modal-open";
var CLASS_BUTTONS_OKAY = ".modal-close";
var CLASS_BUTTONS_CLOSE = ".modal-cancel";
/**
 * A component to open and close modal dialogs. It also handles cancellation and makes
 * sure that the modal background is present in the DOM.
 */
var Modal = /** @class */ (function (_super) {
    tslib_1.__extends(Modal, _super);
    function Modal(element) {
        var _this = _super.call(this, element) || this;
        _this._okayHandler = _this.close.bind(_this);
        _this._cancelHandler = _this._handleClick.bind(_this);
        _this._keydownHandler = _this._handleKeydown.bind(_this);
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the range modal component.
     * @private
     */
    Modal.prototype._initialize = function () {
        // Create the backdrop
        this._backdrop = new DomElement("div")
            .addClass(CLASS_BACKDROP);
        this._backdropParent = getRootElement();
        this._subscribeToTrigger();
    };
    Modal.prototype._subscribeToTrigger = function () {
        var e_1, _a;
        var triggerId = this.element.id;
        if (!triggerId) {
            return;
        }
        this._triggerClickHandler = this.open.bind(this);
        var triggerElements = document.querySelectorAll("." + CLASS_TRIGGER + "[href=" + triggerId + "]");
        try {
            for (var triggerElements_1 = tslib_1.__values(triggerElements), triggerElements_1_1 = triggerElements_1.next(); !triggerElements_1_1.done; triggerElements_1_1 = triggerElements_1.next()) {
                var triggerElement = triggerElements_1_1.value;
                triggerElement.addEventListener("click", this._triggerClickHandler);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (triggerElements_1_1 && !triggerElements_1_1.done && (_a = triggerElements_1.return)) _a.call(triggerElements_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    Modal.prototype._unsubscribeFromTrigger = function () {
        var e_2, _a;
        var triggerId = this.element.id;
        if (!triggerId) {
            return;
        }
        var triggerElements = document.querySelectorAll("." + CLASS_TRIGGER + "[href=" + triggerId + "]");
        try {
            for (var triggerElements_2 = tslib_1.__values(triggerElements), triggerElements_2_1 = triggerElements_2.next(); !triggerElements_2_1.done; triggerElements_2_1 = triggerElements_2.next()) {
                var triggerElement = triggerElements_2_1.value;
                triggerElement.removeEventListener("click", this._windowClickHandler);
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (triggerElements_2_1 && !triggerElements_2_1.done && (_a = triggerElements_2.return)) _a.call(triggerElements_2);
            }
            finally { if (e_2) throw e_2.error; }
        }
        this._triggerClickHandler = undefined;
    };
    Modal.prototype._handleKeydown = function (event) {
        var keycode = event.which || event.keyCode;
        if (keycode === Inputs.KEY_ESCAPE) {
            // handle Escape key (ESC)
            this.cancel();
            return;
        }
    };
    Modal.prototype._handleClick = function (event) {
        preventDefault(event);
        this.cancel();
    };
    Modal.prototype._close = function () {
        var _this = this;
        var e_3, _a, e_4, _b;
        document.removeEventListener("keydown", this._keydownHandler);
        this._backdrop.element.removeEventListener("click", this._cancelHandler);
        removeClass(this._backdropParent, CLASS_MODAL_OPEN);
        this._backdrop.removeClass(CLASS_BACKDROP_OPEN);
        this.removeClass(CLASS_OPEN);
        try {
            for (var _c = tslib_1.__values(this.element.querySelectorAll(CLASS_BUTTONS_CLOSE)), _d = _c.next(); !_d.done; _d = _c.next()) {
                var closeButton = _d.value;
                closeButton.removeEventListener("click", this._cancelHandler);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
            }
            finally { if (e_3) throw e_3.error; }
        }
        try {
            for (var _e = tslib_1.__values(this.element.querySelectorAll(CLASS_BUTTONS_OKAY)), _f = _e.next(); !_f.done; _f = _e.next()) {
                var okayButton = _f.value;
                okayButton.removeEventListener("click", this._okayHandler);
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
            }
            finally { if (e_4) throw e_4.error; }
        }
        setTimeout(function () {
            // remove the backdrop from the body
            _this._backdropParent.removeChild(_this._backdrop.element);
        }, 300);
    };
    /**
     * Opens the modal dialog.
     * @fires Modal#opened
     */
    Modal.prototype.open = function () {
        var _this = this;
        // add the backdrop to the body
        this._backdropParent.appendChild(this._backdrop.element);
        // set the element to flex as it is initially hidden
        this.element.style.display = "flex";
        // remove the style after the animation completes
        setTimeout(function () {
            _this.element.style.display = "";
        }, 800);
        // wait a bit to allow the browser to catch up and show the animation
        setTimeout(function () {
            var e_5, _a, e_6, _b;
            _this.addClass(CLASS_OPEN);
            _this._backdrop.addClass(CLASS_BACKDROP_OPEN);
            addClass(_this._backdropParent, CLASS_MODAL_OPEN);
            document.addEventListener("keydown", _this._keydownHandler);
            _this._backdrop.element.addEventListener("click", _this._cancelHandler);
            try {
                for (var _c = tslib_1.__values(_this.element.querySelectorAll(CLASS_BUTTONS_CLOSE)), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var closeButton = _d.value;
                    closeButton.addEventListener("click", _this._cancelHandler);
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_5) throw e_5.error; }
            }
            try {
                for (var _e = tslib_1.__values(_this.element.querySelectorAll(CLASS_BUTTONS_OKAY)), _f = _e.next(); !_f.done; _f = _e.next()) {
                    var okayButton = _f.value;
                    okayButton.addEventListener("click", _this._okayHandler);
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                }
                finally { if (e_6) throw e_6.error; }
            }
            _this.dispatchEvent("opened");
        }, 50);
    };
    /**
     * Cancels (and closes) the modal dialog.
     * @fires Modal#cancelled
     * @fires Modal#closed
     */
    Modal.prototype.cancel = function () {
        this.dispatchEvent("cancelled");
        this._close();
    };
    /**
     * Closes the modal dialog.
     * @fires Modal#closed
     */
    Modal.prototype.close = function () {
        this._close();
        this.dispatchEvent("closed");
    };
    /**
     * Destroys the component and frees all references.
     */
    Modal.prototype.destroy = function () {
        this.cancel();
        this._unsubscribeFromTrigger();
    };
    return Modal;
}(DomElement));
export function init() {
    searchAndInitialize(".modal", function (e) {
        new Modal(e);
    });
}
export default Modal;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL21vZGFsL01vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLE1BQU0sVUFBVSxDQUFBO0FBQzlELE9BQU8sS0FBSyxNQUFNLE1BQU0sV0FBVyxDQUFBO0FBQ25DLE9BQU8sVUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUN0QyxPQUFPLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUV2RSxJQUFNLGNBQWMsR0FBRyxVQUFVLENBQUE7QUFDakMsSUFBTSxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQTtBQUU1QyxJQUFNLFVBQVUsR0FBRyxhQUFhLENBQUE7QUFDaEMsSUFBTSxhQUFhLEdBQUcsZUFBZSxDQUFBO0FBRXJDLElBQU0sZ0JBQWdCLEdBQUcsZUFBZSxDQUFBO0FBRXhDLElBQU0sa0JBQWtCLEdBQUcsY0FBYyxDQUFBO0FBQ3pDLElBQU0sbUJBQW1CLEdBQUcsZUFBZSxDQUFBO0FBRTNDOzs7R0FHRztBQUNIO0lBQW9CLGlDQUF1QjtJQVV6QyxlQUFZLE9BQW9CO1FBQWhDLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBT2Y7UUFMQyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQ3pDLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDbEQsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUVyRCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7O0lBQ3BCLENBQUM7SUFFRDs7O09BR0c7SUFDTywyQkFBVyxHQUFyQjtRQUVFLHNCQUFzQjtRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksVUFBVSxDQUFpQixLQUFLLENBQUM7YUFDbkQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBRTNCLElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxFQUFFLENBQUE7UUFDdkMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUE7SUFDNUIsQ0FBQztJQUVTLG1DQUFtQixHQUE3Qjs7UUFDRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQTtRQUNqQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2QsT0FBTTtTQUNQO1FBRUQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBRWhELElBQUksZUFBZSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFJLGFBQWEsY0FBUyxTQUFTLE1BQUcsQ0FBQyxDQUFBOztZQUN2RixLQUEyQixJQUFBLG9CQUFBLGlCQUFBLGVBQWUsQ0FBQSxnREFBQSw2RUFBRTtnQkFBdkMsSUFBSSxjQUFjLDRCQUFBO2dCQUNyQixjQUFjLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxvQkFBcUIsQ0FBQyxDQUFBO2FBQ3JFOzs7Ozs7Ozs7SUFDSCxDQUFDO0lBRVMsdUNBQXVCLEdBQWpDOztRQUNFLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFBO1FBQ2pDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDZCxPQUFNO1NBQ1A7UUFFRCxJQUFJLGVBQWUsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsTUFBSSxhQUFhLGNBQVMsU0FBUyxNQUFHLENBQUMsQ0FBQTs7WUFDdkYsS0FBMkIsSUFBQSxvQkFBQSxpQkFBQSxlQUFlLENBQUEsZ0RBQUEsNkVBQUU7Z0JBQXZDLElBQUksY0FBYyw0QkFBQTtnQkFDckIsY0FBYyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW9CLENBQUMsQ0FBQTthQUN2RTs7Ozs7Ozs7O1FBRUQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFNBQVMsQ0FBQTtJQUN2QyxDQUFDO0lBRVMsOEJBQWMsR0FBeEIsVUFBeUIsS0FBb0I7UUFDM0MsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFBO1FBRTFDLElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxVQUFVLEVBQUU7WUFDakMsMEJBQTBCO1lBQzFCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtZQUNiLE9BQU07U0FDUDtJQUNILENBQUM7SUFFUyw0QkFBWSxHQUF0QixVQUF1QixLQUFpQjtRQUN0QyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDckIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQ2YsQ0FBQztJQUVTLHNCQUFNLEdBQWhCO1FBQUEsaUJBb0JDOztRQW5CQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUM3RCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBRXhFLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLENBQUE7UUFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUMvQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFBOztZQUU1QixLQUF3QixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO2dCQUF2RSxJQUFJLFdBQVcsV0FBQTtnQkFDbEIsV0FBVyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7YUFDOUQ7Ozs7Ozs7Ozs7WUFFRCxLQUF1QixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO2dCQUFyRSxJQUFJLFVBQVUsV0FBQTtnQkFDakIsVUFBVSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7YUFDM0Q7Ozs7Ozs7OztRQUVELFVBQVUsQ0FBQztZQUNULG9DQUFvQztZQUNwQyxLQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQzFELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtJQUNULENBQUM7SUFFRDs7O09BR0c7SUFDSCxvQkFBSSxHQUFKO1FBQUEsaUJBZ0NDO1FBL0JDLCtCQUErQjtRQUMvQixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBRXhELG9EQUFvRDtRQUNwRCxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFBO1FBRW5DLGlEQUFpRDtRQUNqRCxVQUFVLENBQUM7WUFDVCxLQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFBO1FBQ2pDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtRQUVQLHFFQUFxRTtRQUNyRSxVQUFVLENBQUM7O1lBQ1QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQTtZQUN6QixLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1lBQzVDLFFBQVEsQ0FBQyxLQUFJLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLENBQUE7WUFFaEQsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7WUFFMUQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQTs7Z0JBRXJFLEtBQXdCLElBQUEsS0FBQSxpQkFBQSxLQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUEsZ0JBQUEsNEJBQUU7b0JBQXZFLElBQUksV0FBVyxXQUFBO29CQUNsQixXQUFXLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQTtpQkFDM0Q7Ozs7Ozs7Ozs7Z0JBRUQsS0FBdUIsSUFBQSxLQUFBLGlCQUFBLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBckUsSUFBSSxVQUFVLFdBQUE7b0JBQ2pCLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO2lCQUN4RDs7Ozs7Ozs7O1lBRUQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUM5QixDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUE7SUFDUixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILHNCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBQy9CLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtJQUNmLENBQUM7SUFFRDs7O09BR0c7SUFDSCxxQkFBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO1FBQ2IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBRUQ7O09BRUc7SUFDSCx1QkFBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO1FBQ2IsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUE7SUFDaEMsQ0FBQztJQXNCSCxZQUFDO0FBQUQsQ0F4TEEsQUF3TEMsQ0F4TG1CLFVBQVUsR0F3TDdCO0FBRUQsTUFBTSxVQUFVLElBQUk7SUFDbEIsbUJBQW1CLENBQWMsUUFBUSxFQUFFLFVBQUMsQ0FBQztRQUMzQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNkLENBQUMsQ0FBQyxDQUFBO0FBQ0osQ0FBQztBQUVELGVBQWUsS0FBSyxDQUFBIiwiZmlsZSI6Im1haW4vc3JjL21vZGFsL01vZGFsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgc2VhcmNoQW5kSW5pdGlhbGl6ZSwgcHJldmVudERlZmF1bHQgfSBmcm9tIFwiLi4vVXRpbHNcIlxuaW1wb3J0ICogYXMgSW5wdXRzIGZyb20gXCIuLi9JbnB1dHNcIlxuaW1wb3J0IERvbUVsZW1lbnQgZnJvbSBcIi4uL0RvbUVsZW1lbnRcIlxuaW1wb3J0IHsgZ2V0Um9vdEVsZW1lbnQsIGFkZENsYXNzLCByZW1vdmVDbGFzcyB9IGZyb20gXCIuLi9Eb21GdW5jdGlvbnNcIlxuXG5jb25zdCBDTEFTU19CQUNLRFJPUCA9IFwiYmFja2Ryb3BcIlxuY29uc3QgQ0xBU1NfQkFDS0RST1BfT1BFTiA9IFwiYmFja2Ryb3AtLW9wZW5cIlxuXG5jb25zdCBDTEFTU19PUEVOID0gXCJtb2RhbC0tb3BlblwiXG5jb25zdCBDTEFTU19UUklHR0VSID0gXCJtb2RhbC10cmlnZ2VyXCJcblxuY29uc3QgQ0xBU1NfTU9EQUxfT1BFTiA9IFwianMtbW9kYWwtb3BlblwiXG5cbmNvbnN0IENMQVNTX0JVVFRPTlNfT0tBWSA9IFwiLm1vZGFsLWNsb3NlXCJcbmNvbnN0IENMQVNTX0JVVFRPTlNfQ0xPU0UgPSBcIi5tb2RhbC1jYW5jZWxcIlxuXG4vKipcbiAqIEEgY29tcG9uZW50IHRvIG9wZW4gYW5kIGNsb3NlIG1vZGFsIGRpYWxvZ3MuIEl0IGFsc28gaGFuZGxlcyBjYW5jZWxsYXRpb24gYW5kIG1ha2VzXG4gKiBzdXJlIHRoYXQgdGhlIG1vZGFsIGJhY2tncm91bmQgaXMgcHJlc2VudCBpbiB0aGUgRE9NLlxuICovXG5jbGFzcyBNb2RhbCBleHRlbmRzIERvbUVsZW1lbnQ8SFRNTEVsZW1lbnQ+IHtcbiAgcHJpdmF0ZSBfb2theUhhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9jYW5jZWxIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfa2V5ZG93bkhhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF93aW5kb3dDbGlja0hhbmRsZXI/OiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfdHJpZ2dlckNsaWNrSGFuZGxlcj86IChlOiBFdmVudCkgPT4gdm9pZFxuXG4gIHByaXZhdGUgX2JhY2tkcm9wITogRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD5cbiAgcHJpdmF0ZSBfYmFja2Ryb3BQYXJlbnQhOiBFbGVtZW50XG5cbiAgY29uc3RydWN0b3IoZWxlbWVudDogSFRNTEVsZW1lbnQpIHtcbiAgICBzdXBlcihlbGVtZW50KVxuXG4gICAgdGhpcy5fb2theUhhbmRsZXIgPSB0aGlzLmNsb3NlLmJpbmQodGhpcylcbiAgICB0aGlzLl9jYW5jZWxIYW5kbGVyID0gdGhpcy5faGFuZGxlQ2xpY2suYmluZCh0aGlzKVxuICAgIHRoaXMuX2tleWRvd25IYW5kbGVyID0gdGhpcy5faGFuZGxlS2V5ZG93bi5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgcmFuZ2UgbW9kYWwgY29tcG9uZW50LlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9pbml0aWFsaXplKCkge1xuXG4gICAgLy8gQ3JlYXRlIHRoZSBiYWNrZHJvcFxuICAgIHRoaXMuX2JhY2tkcm9wID0gbmV3IERvbUVsZW1lbnQ8SFRNTERpdkVsZW1lbnQ+KFwiZGl2XCIpXG4gICAgICAuYWRkQ2xhc3MoQ0xBU1NfQkFDS0RST1ApXG5cbiAgICB0aGlzLl9iYWNrZHJvcFBhcmVudCA9IGdldFJvb3RFbGVtZW50KClcbiAgICB0aGlzLl9zdWJzY3JpYmVUb1RyaWdnZXIoKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9zdWJzY3JpYmVUb1RyaWdnZXIoKSB7XG4gICAgY29uc3QgdHJpZ2dlcklkID0gdGhpcy5lbGVtZW50LmlkXG4gICAgaWYgKCF0cmlnZ2VySWQpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIHRoaXMuX3RyaWdnZXJDbGlja0hhbmRsZXIgPSB0aGlzLm9wZW4uYmluZCh0aGlzKVxuXG4gICAgbGV0IHRyaWdnZXJFbGVtZW50cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC4ke0NMQVNTX1RSSUdHRVJ9W2hyZWY9JHt0cmlnZ2VySWR9XWApXG4gICAgZm9yIChsZXQgdHJpZ2dlckVsZW1lbnQgb2YgdHJpZ2dlckVsZW1lbnRzKSB7XG4gICAgICB0cmlnZ2VyRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fdHJpZ2dlckNsaWNrSGFuZGxlciEpXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF91bnN1YnNjcmliZUZyb21UcmlnZ2VyKCkge1xuICAgIGNvbnN0IHRyaWdnZXJJZCA9IHRoaXMuZWxlbWVudC5pZFxuICAgIGlmICghdHJpZ2dlcklkKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBsZXQgdHJpZ2dlckVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgLiR7Q0xBU1NfVFJJR0dFUn1baHJlZj0ke3RyaWdnZXJJZH1dYClcbiAgICBmb3IgKGxldCB0cmlnZ2VyRWxlbWVudCBvZiB0cmlnZ2VyRWxlbWVudHMpIHtcbiAgICAgIHRyaWdnZXJFbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIhKVxuICAgIH1cblxuICAgIHRoaXMuX3RyaWdnZXJDbGlja0hhbmRsZXIgPSB1bmRlZmluZWRcbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlS2V5ZG93bihldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgIGxldCBrZXljb2RlID0gZXZlbnQud2hpY2ggfHwgZXZlbnQua2V5Q29kZVxuXG4gICAgaWYgKGtleWNvZGUgPT09IElucHV0cy5LRVlfRVNDQVBFKSB7XG4gICAgICAvLyBoYW5kbGUgRXNjYXBlIGtleSAoRVNDKVxuICAgICAgdGhpcy5jYW5jZWwoKVxuICAgICAgcmV0dXJuXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVDbGljayhldmVudDogTW91c2VFdmVudCkge1xuICAgIHByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgIHRoaXMuY2FuY2VsKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfY2xvc2UoKSB7XG4gICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgdGhpcy5fa2V5ZG93bkhhbmRsZXIpXG4gICAgdGhpcy5fYmFja2Ryb3AuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2FuY2VsSGFuZGxlcilcblxuICAgIHJlbW92ZUNsYXNzKHRoaXMuX2JhY2tkcm9wUGFyZW50LCBDTEFTU19NT0RBTF9PUEVOKVxuICAgIHRoaXMuX2JhY2tkcm9wLnJlbW92ZUNsYXNzKENMQVNTX0JBQ0tEUk9QX09QRU4pXG4gICAgdGhpcy5yZW1vdmVDbGFzcyhDTEFTU19PUEVOKVxuXG4gICAgZm9yIChsZXQgY2xvc2VCdXR0b24gb2YgdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoQ0xBU1NfQlVUVE9OU19DTE9TRSkpIHtcbiAgICAgIGNsb3NlQnV0dG9uLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9jYW5jZWxIYW5kbGVyKVxuICAgIH1cblxuICAgIGZvciAobGV0IG9rYXlCdXR0b24gb2YgdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoQ0xBU1NfQlVUVE9OU19PS0FZKSkge1xuICAgICAgb2theUJ1dHRvbi5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fb2theUhhbmRsZXIpXG4gICAgfVxuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAvLyByZW1vdmUgdGhlIGJhY2tkcm9wIGZyb20gdGhlIGJvZHlcbiAgICAgIHRoaXMuX2JhY2tkcm9wUGFyZW50LnJlbW92ZUNoaWxkKHRoaXMuX2JhY2tkcm9wLmVsZW1lbnQpXG4gICAgfSwgMzAwKVxuICB9XG5cbiAgLyoqXG4gICAqIE9wZW5zIHRoZSBtb2RhbCBkaWFsb2cuXG4gICAqIEBmaXJlcyBNb2RhbCNvcGVuZWRcbiAgICovXG4gIG9wZW4oKSB7XG4gICAgLy8gYWRkIHRoZSBiYWNrZHJvcCB0byB0aGUgYm9keVxuICAgIHRoaXMuX2JhY2tkcm9wUGFyZW50LmFwcGVuZENoaWxkKHRoaXMuX2JhY2tkcm9wLmVsZW1lbnQpXG5cbiAgICAvLyBzZXQgdGhlIGVsZW1lbnQgdG8gZmxleCBhcyBpdCBpcyBpbml0aWFsbHkgaGlkZGVuXG4gICAgdGhpcy5lbGVtZW50LnN0eWxlLmRpc3BsYXkgPSBcImZsZXhcIlxuXG4gICAgLy8gcmVtb3ZlIHRoZSBzdHlsZSBhZnRlciB0aGUgYW5pbWF0aW9uIGNvbXBsZXRlc1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5lbGVtZW50LnN0eWxlLmRpc3BsYXkgPSBcIlwiXG4gICAgfSwgODAwKVxuXG4gICAgLy8gd2FpdCBhIGJpdCB0byBhbGxvdyB0aGUgYnJvd3NlciB0byBjYXRjaCB1cCBhbmQgc2hvdyB0aGUgYW5pbWF0aW9uXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLmFkZENsYXNzKENMQVNTX09QRU4pXG4gICAgICB0aGlzLl9iYWNrZHJvcC5hZGRDbGFzcyhDTEFTU19CQUNLRFJPUF9PUEVOKVxuICAgICAgYWRkQ2xhc3ModGhpcy5fYmFja2Ryb3BQYXJlbnQsIENMQVNTX01PREFMX09QRU4pXG5cbiAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIHRoaXMuX2tleWRvd25IYW5kbGVyKVxuXG4gICAgICB0aGlzLl9iYWNrZHJvcC5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9jYW5jZWxIYW5kbGVyKVxuXG4gICAgICBmb3IgKGxldCBjbG9zZUJ1dHRvbiBvZiB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChDTEFTU19CVVRUT05TX0NMT1NFKSkge1xuICAgICAgICBjbG9zZUJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2FuY2VsSGFuZGxlcilcbiAgICAgIH1cblxuICAgICAgZm9yIChsZXQgb2theUJ1dHRvbiBvZiB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChDTEFTU19CVVRUT05TX09LQVkpKSB7XG4gICAgICAgIG9rYXlCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX29rYXlIYW5kbGVyKVxuICAgICAgfVxuXG4gICAgICB0aGlzLmRpc3BhdGNoRXZlbnQoXCJvcGVuZWRcIilcbiAgICB9LCA1MClcbiAgfVxuXG4gIC8qKlxuICAgKiBDYW5jZWxzIChhbmQgY2xvc2VzKSB0aGUgbW9kYWwgZGlhbG9nLlxuICAgKiBAZmlyZXMgTW9kYWwjY2FuY2VsbGVkXG4gICAqIEBmaXJlcyBNb2RhbCNjbG9zZWRcbiAgICovXG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmRpc3BhdGNoRXZlbnQoXCJjYW5jZWxsZWRcIilcbiAgICB0aGlzLl9jbG9zZSgpXG4gIH1cblxuICAvKipcbiAgICogQ2xvc2VzIHRoZSBtb2RhbCBkaWFsb2cuXG4gICAqIEBmaXJlcyBNb2RhbCNjbG9zZWRcbiAgICovXG4gIGNsb3NlKCkge1xuICAgIHRoaXMuX2Nsb3NlKClcbiAgICB0aGlzLmRpc3BhdGNoRXZlbnQoXCJjbG9zZWRcIilcbiAgfVxuXG4gIC8qKlxuICAgKiBEZXN0cm95cyB0aGUgY29tcG9uZW50IGFuZCBmcmVlcyBhbGwgcmVmZXJlbmNlcy5cbiAgICovXG4gIGRlc3Ryb3koKSB7XG4gICAgdGhpcy5jYW5jZWwoKVxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlRnJvbVRyaWdnZXIoKVxuICB9XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdGhlIG1vZGFsIGRpYWxvZyBpcyBvcGVuZWQgYnkgdGhlIGFuY2hvciBsaW5rIG9yIHVzaW5nIHRoZVxuICAgKiB7QGxpbmsgTW9kYWwjb3Blbn0gbWV0aG9kLlxuICAgKiBAZXZlbnQgTW9kYWwjb3BlbmVkXG4gICAqIEB0eXBlIHtvYmplY3R9XG4gICAqL1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIHRoZSBtb2RhbCBkaWFsb2cgaXMgY2xvc2VkIGJ5IHRoZSB1c2VyIG9yIHVzaW5nIHRoZVxuICAgKiB7QGxpbmsgTW9kYWwjY2xvc2V9IG1ldGhvZC5cbiAgICogQGV2ZW50IE1vZGFsI2Nsb3NlZFxuICAgKiBAdHlwZSB7b2JqZWN0fVxuICAgKi9cblxuICAvKipcbiAgICogRmlyZWQgd2hlbiB0aGUgbW9kYWwgZGlhbG9nIGlzIGNhbmNlbGxlZCBieSB0aGUgdXNlciBvciB1c2luZyB0aGVcbiAgICoge0BsaW5rIE1vZGFsI2NhbmNlbH0gbWV0aG9kLlxuICAgKiBAZXZlbnQgTW9kYWwjY2FuY2VsbGVkXG4gICAqIEB0eXBlIHtvYmplY3R9XG4gICAqL1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgc2VhcmNoQW5kSW5pdGlhbGl6ZTxIVE1MRWxlbWVudD4oXCIubW9kYWxcIiwgKGUpID0+IHtcbiAgICBuZXcgTW9kYWwoZSlcbiAgfSlcbn1cblxuZXhwb3J0IGRlZmF1bHQgTW9kYWxcbiJdLCJzb3VyY2VSb290IjoiLi4vLi4vLi4vLi4vLi4ifQ==
