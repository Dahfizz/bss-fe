import * as tslib_1 from "tslib";
import DomElement from "../DomElement";
import { searchAndInitialize, remove } from "../Utils";
import { getAttributeReference } from "../DomFunctions";
import { tryGetData, createLegendItem, isColor, removeAllChildren } from "./ChartFunctions";
import { TimelineLite, Power4 } from "gsap";
var QUERY_DETAIL_RIGHT = ".detail-right";
var QUERY_DETAIL_BOTTOM = ".detail-bottom";
var QUERY_PROGRESS = ".bar-chart__progress";
var CLASS_UNLIMITED = "bar-chart-horizontal--unlimited";
var CLASS_LIMITED = "bar-chart-horizontal--limited";
var CLASS_DETAIL_VALUE = "value";
var CLASS_DETAIL_UNIT = "unit";
var CLASS_INDICATOR = "indicator";
var CLASS_INDICATOR_WRAPPER = "indicator-wrapper";
var CLASS_TOOLTIP = "tooltip";
var CLASS_TOOLTIP_MULTILINE = "tooltip--multiline";
var ANIMATION_DURATION = 0.5;
/**
 * Bar Chart Horizontal Component.
 */
var BarChartHorizontal = /** @class */ (function (_super) {
    tslib_1.__extends(BarChartHorizontal, _super);
    /**
     * Creates and initializes the bar chart horizontal component.
     * @param {DomElement} - root element of the chart.
     */
    function BarChartHorizontal(element, data) {
        var _this = _super.call(this, element) || this;
        if (data) {
            _this._data = data;
        }
        _this._legendItems = [];
        _this._initialize();
        return _this;
    }
    BarChartHorizontal.prototype._initialize = function () {
        this._unit = this.getAttribute("data-unit") || "";
        this._maxValue = parseFloat(this.getAttribute("data-max"));
        this._precision = parseInt(this.getAttribute("data-precision"), 10) || 0;
        this._isUnlimited = this.hasClass(CLASS_UNLIMITED);
        this._isLimited = this.hasClass(CLASS_LIMITED);
        this._progessWrapper = this.element.querySelector(QUERY_PROGRESS);
        if (this._isLimited === true) {
            this._detailRight = this.element.querySelector(QUERY_DETAIL_BOTTOM);
        }
        else {
            this._detailRight = this.element.querySelector(QUERY_DETAIL_RIGHT);
        }
        if (this._isUnlimited === false && this._isLimited === false) {
            this._legend = getAttributeReference(this.element, "data-legend");
        }
        if (!this._data) {
            this._data = tryGetData(this.element);
        }
        this._render();
    };
    BarChartHorizontal.prototype._render = function () {
        var e_1, _a;
        var dataOne = this._data[0];
        var dataTwo = this._data[1];
        var tl = new TimelineLite();
        var tooltip = this._isLimited === false ? this._getTooltipContent(this._data) : undefined;
        var animatedValueElement;
        // Cleanup
        removeAllChildren(this._detailRight);
        removeAllChildren(this._progessWrapper);
        try {
            // Clear only own legend items
            for (var _b = tslib_1.__values(this._legendItems), _c = _b.next(); !_c.done; _c = _b.next()) {
                var item = _c.value;
                remove(item);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this._legendItems = [];
        if (dataOne) {
            if (this._isUnlimited === false || (this._isUnlimited === true && !dataTwo)) {
                var valElement = animatedValueElement = this._createValueElement(dataOne);
                this._detailRight.appendChild(valElement);
                if (this._isLimited === false) {
                    var separatorElement = new DomElement("div")
                        .addClass(CLASS_DETAIL_UNIT)
                        .setHtml(" " + this._unit)
                        .element;
                    this._detailRight.appendChild(separatorElement);
                }
            }
            // Add the indicator
            var indicator = this._addIndicator(dataOne, tooltip);
            tl.from(indicator, ANIMATION_DURATION, {
                width: 0,
                ease: Power4.easeInOut
            });
            // Animate the value if required
            if (animatedValueElement && this._isLimited === true) {
                var counter_1 = { var: 0 };
                tl.to(counter_1, ANIMATION_DURATION, {
                    var: dataOne.value,
                    roundProps: "var",
                    onUpdate: function () {
                        animatedValueElement.innerHTML = "" + counter_1.var;
                    },
                    ease: Power4.easeOut
                }, "-=" + ANIMATION_DURATION);
            }
            // Add the legend
            if (this._legend) {
                var legendItem = createLegendItem(dataOne);
                this._legend.appendChild(legendItem);
                this._legendItems.push(legendItem);
                tl.from(legendItem, ANIMATION_DURATION, {
                    opacity: 0,
                    ease: Power4.easeInOut
                }, "-=" + ANIMATION_DURATION);
            }
        }
        if (dataTwo) {
            var valElement = this._createValueElement(dataTwo);
            var unitElement = new DomElement("div")
                .addClass(CLASS_DETAIL_UNIT)
                .setHtml(" " + this._unit)
                .element;
            this._detailRight.appendChild(valElement);
            this._detailRight.appendChild(unitElement);
            // Add the indicator
            var indicator = this._addIndicator(dataTwo, tooltip);
            tl.from(indicator, ANIMATION_DURATION, {
                width: 0,
                ease: Power4.easeInOut
            });
            // Add the legend
            if (this._legend) {
                var legendItem = createLegendItem(dataTwo);
                this._legend.appendChild(legendItem);
                this._legendItems.push(legendItem);
                tl.from(legendItem, ANIMATION_DURATION, {
                    opacity: 0,
                    ease: Power4.easeInOut
                }, "-=" + ANIMATION_DURATION);
            }
        }
        if (this._isLimited === true) {
            var valElement = this._createValueElement({ value: this._maxValue });
            var unitElement = new DomElement("div")
                .addClass(CLASS_DETAIL_UNIT)
                .setHtml(" " + this._unit)
                .element;
            this._detailRight.appendChild(valElement);
            this._detailRight.appendChild(unitElement);
        }
    };
    BarChartHorizontal.prototype._createValueElement = function (data) {
        var unlimitedPrefix = "";
        if (this._isUnlimited === true) {
            unlimitedPrefix = "+";
        }
        var value = parseFloat(data.value);
        if (value <= 0) {
            value = ".00";
        }
        else {
            value = value.toFixed(this._precision);
        }
        return new DomElement("div")
            .addClass(CLASS_DETAIL_VALUE)
            .setHtml("" + unlimitedPrefix + value)
            .element;
    };
    BarChartHorizontal.prototype._addIndicator = function (data, tooltip) {
        var width = ((99.8 / this._maxValue) * data.value);
        var indicator = new DomElement("div")
            .addClass(CLASS_INDICATOR);
        if (isColor(data.color) === true) {
            indicator.setAttribute("style", "background-color: " + data.color + ";");
        }
        else {
            indicator.addClass(data.color);
        }
        var indicatorWrapper = new DomElement("div")
            .addClass(CLASS_INDICATOR_WRAPPER)
            .setAttribute("style", "width: " + width + "%")
            .appendChild(indicator)
            .setAttribute("onclick", "void(0)");
        if (tooltip && tooltip !== "") {
            indicatorWrapper
                .addClass(CLASS_TOOLTIP)
                .addClass(CLASS_TOOLTIP_MULTILINE)
                .setAttribute("aria-label", tooltip);
        }
        this._progessWrapper.appendChild(indicatorWrapper.element);
        return indicatorWrapper.element;
    };
    BarChartHorizontal.prototype._getTooltipContent = function (dataList) {
        var e_2, _a;
        var tooltip = "";
        try {
            for (var dataList_1 = tslib_1.__values(dataList), dataList_1_1 = dataList_1.next(); !dataList_1_1.done; dataList_1_1 = dataList_1.next()) {
                var data = dataList_1_1.value;
                tooltip += data.title + ": " + data.value + " " + this._unit + "\n";
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (dataList_1_1 && !dataList_1_1.done && (_a = dataList_1.return)) _a.call(dataList_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return tooltip.trim();
    };
    /**
     * Updates the bar chart with the specified data definitions.
     * @param {Array} - bar chart data definitions.
     */
    BarChartHorizontal.prototype.update = function (data) {
        if (data) {
            this._data = data;
        }
        this._render();
    };
    /**
     * Removes all event handlers and clears references.
     */
    BarChartHorizontal.prototype.destroy = function () {
        var e_3, _a;
        this._data = undefined;
        removeAllChildren(this._detailRight);
        removeAllChildren(this._progessWrapper);
        this._detailRight = undefined;
        this._progessWrapper = undefined;
        try {
            for (var _b = tslib_1.__values(this._legendItems), _c = _b.next(); !_c.done; _c = _b.next()) {
                var item = _c.value;
                remove(item);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_3) throw e_3.error; }
        }
        this._legendItems = undefined;
        this._legend = undefined;
    };
    /**
     * @deprecated use destroy() instead.
     * @todo remove in version 2.0.0
     */
    BarChartHorizontal.prototype.destory = function () {
        this.destroy();
    };
    return BarChartHorizontal;
}(DomElement));
export function init() {
    searchAndInitialize(".bar-chart-horizontal", function (e) {
        new BarChartHorizontal(e);
    });
}
export default BarChartHorizontal;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2NoYXJ0cy9CYXJDaGFydEhvcml6b250YWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sVUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUN0QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxFQUFFLE1BQU0sVUFBVSxDQUFBO0FBQ3RELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQ3ZELE9BQU8sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUF3QixNQUFNLGtCQUFrQixDQUFBO0FBRWpILE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sTUFBTSxDQUFBO0FBRTNDLElBQU0sa0JBQWtCLEdBQUcsZUFBZSxDQUFBO0FBQzFDLElBQU0sbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUE7QUFDNUMsSUFBTSxjQUFjLEdBQUcsc0JBQXNCLENBQUE7QUFFN0MsSUFBTSxlQUFlLEdBQUcsaUNBQWlDLENBQUE7QUFDekQsSUFBTSxhQUFhLEdBQUcsK0JBQStCLENBQUE7QUFFckQsSUFBTSxrQkFBa0IsR0FBRyxPQUFPLENBQUE7QUFDbEMsSUFBTSxpQkFBaUIsR0FBRyxNQUFNLENBQUE7QUFFaEMsSUFBTSxlQUFlLEdBQUcsV0FBVyxDQUFBO0FBQ25DLElBQU0sdUJBQXVCLEdBQUcsbUJBQW1CLENBQUE7QUFFbkQsSUFBTSxhQUFhLEdBQUcsU0FBUyxDQUFBO0FBQy9CLElBQU0sdUJBQXVCLEdBQUcsb0JBQW9CLENBQUE7QUFFcEQsSUFBTSxrQkFBa0IsR0FBRyxHQUFHLENBQUE7QUFFOUI7O0dBRUc7QUFDSDtJQUFpQyw4Q0FBdUI7SUFnQnREOzs7T0FHRztJQUNILDRCQUFZLE9BQW9CLEVBQUUsSUFBZ0I7UUFBbEQsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FTZjtRQVBDLElBQUksSUFBSSxFQUFFO1lBQ1IsS0FBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7U0FDbEI7UUFFRCxLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQTtRQUV0QixLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7O0lBQ3BCLENBQUM7SUFFUyx3Q0FBVyxHQUFyQjtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDakQsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUUsQ0FBQyxDQUFBO1FBQzNELElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUUsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFekUsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ2xELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUU5QyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBaUIsQ0FBQTtRQUVqRixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFO1lBQzVCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQWlCLENBQUE7U0FDcEY7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQWlCLENBQUE7U0FDbkY7UUFFRCxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO1lBQzVELElBQUksQ0FBQyxPQUFPLEdBQUcscUJBQXFCLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUUsQ0FBQTtTQUNuRTtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQ3RDO1FBRUQsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO0lBQ2hCLENBQUM7SUFFUyxvQ0FBTyxHQUFqQjs7UUFDRSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQzNCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFFM0IsSUFBSSxFQUFFLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQTtRQUMzQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFBO1FBRXpGLElBQUksb0JBQXlDLENBQUE7UUFFN0MsVUFBVTtRQUNWLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUNwQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7O1lBRXZDLDhCQUE4QjtZQUM5QixLQUFpQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLFlBQVksQ0FBQSxnQkFBQSw0QkFBRTtnQkFBL0IsSUFBSSxJQUFJLFdBQUE7Z0JBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFBO2FBQ2I7Ozs7Ozs7OztRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFBO1FBRXRCLElBQUksT0FBTyxFQUFFO1lBQ1gsSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBRTNFLElBQUksVUFBVSxHQUFHLG9CQUFvQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQTtnQkFDekUsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUE7Z0JBRXpDLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7b0JBQzdCLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO3lCQUN6QyxRQUFRLENBQUMsaUJBQWlCLENBQUM7eUJBQzNCLE9BQU8sQ0FBQyxNQUFJLElBQUksQ0FBQyxLQUFPLENBQUM7eUJBQ3pCLE9BQU8sQ0FBQTtvQkFFVixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO2lCQUNoRDthQUNGO1lBRUQsb0JBQW9CO1lBQ3BCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1lBRXBELEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGtCQUFrQixFQUFFO2dCQUNyQyxLQUFLLEVBQUUsQ0FBQztnQkFDUixJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVM7YUFDdkIsQ0FBQyxDQUFBO1lBRUYsZ0NBQWdDO1lBQ2hDLElBQUksb0JBQW9CLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ3BELElBQUksU0FBTyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFBO2dCQUN4QixFQUFFLENBQUMsRUFBRSxDQUFDLFNBQU8sRUFBRSxrQkFBa0IsRUFBRTtvQkFDakMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxLQUFLO29CQUNsQixVQUFVLEVBQUUsS0FBSztvQkFDakIsUUFBUSxFQUFFO3dCQUNSLG9CQUFxQixDQUFDLFNBQVMsR0FBRyxLQUFHLFNBQU8sQ0FBQyxHQUFLLENBQUE7b0JBQ3BELENBQUM7b0JBQ0QsSUFBSSxFQUFFLE1BQU0sQ0FBQyxPQUFPO2lCQUNyQixFQUFFLE9BQUssa0JBQW9CLENBQUMsQ0FBQTthQUM5QjtZQUVELGlCQUFpQjtZQUNqQixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2hCLElBQU0sVUFBVSxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFBO2dCQUM1QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtnQkFDcEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7Z0JBRWxDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGtCQUFrQixFQUFFO29CQUN0QyxPQUFPLEVBQUUsQ0FBQztvQkFDVixJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVM7aUJBQ3ZCLEVBQUUsT0FBSyxrQkFBb0IsQ0FBQyxDQUFBO2FBQzlCO1NBQ0Y7UUFFRCxJQUFJLE9BQU8sRUFBRTtZQUNYLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQTtZQUVsRCxJQUFJLFdBQVcsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7aUJBQ3BDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDM0IsT0FBTyxDQUFDLE1BQUksSUFBSSxDQUFDLEtBQU8sQ0FBQztpQkFDekIsT0FBTyxDQUFBO1lBRVYsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUE7WUFDekMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUE7WUFFMUMsb0JBQW9CO1lBQ3BCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1lBQ3BELEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGtCQUFrQixFQUFFO2dCQUNyQyxLQUFLLEVBQUUsQ0FBQztnQkFDUixJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVM7YUFDdkIsQ0FBQyxDQUFBO1lBRUYsaUJBQWlCO1lBQ2pCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEIsSUFBTSxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUE7Z0JBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFBO2dCQUNwQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTtnQkFFbEMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLEVBQUU7b0JBQ3RDLE9BQU8sRUFBRSxDQUFDO29CQUNWLElBQUksRUFBRSxNQUFNLENBQUMsU0FBUztpQkFDdkIsRUFBRSxPQUFLLGtCQUFvQixDQUFDLENBQUE7YUFDOUI7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7WUFDNUIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFBO1lBRXBFLElBQUksV0FBVyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQztpQkFDcEMsUUFBUSxDQUFDLGlCQUFpQixDQUFDO2lCQUMzQixPQUFPLENBQUMsTUFBSSxJQUFJLENBQUMsS0FBTyxDQUFDO2lCQUN6QixPQUFPLENBQUE7WUFFVixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtZQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtTQUMzQztJQUNILENBQUM7SUFFUyxnREFBbUIsR0FBN0IsVUFBOEIsSUFBZ0M7UUFDNUQsSUFBSSxlQUFlLEdBQUcsRUFBRSxDQUFBO1FBRXhCLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLEVBQUU7WUFDOUIsZUFBZSxHQUFHLEdBQUcsQ0FBQTtTQUN0QjtRQUVELElBQUksS0FBSyxHQUFvQixVQUFVLENBQUUsSUFBSSxDQUFDLEtBQWdCLENBQUMsQ0FBQTtRQUMvRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxLQUFLLEdBQUcsS0FBSyxDQUFBO1NBQ2Q7YUFBTTtZQUNMLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTtTQUN2QztRQUVELE9BQU8sSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO2FBQ3pCLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQzthQUM1QixPQUFPLENBQUMsS0FBRyxlQUFlLEdBQUcsS0FBTyxDQUFDO2FBQ3JDLE9BQU8sQ0FBQTtJQUNaLENBQUM7SUFFUywwQ0FBYSxHQUF2QixVQUF3QixJQUFlLEVBQUUsT0FBZ0I7UUFDdkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBRWxELElBQUksU0FBUyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQzthQUNsQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUE7UUFFNUIsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksRUFBRTtZQUNoQyxTQUFTLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSx1QkFBcUIsSUFBSSxDQUFDLEtBQUssTUFBRyxDQUFDLENBQUE7U0FDcEU7YUFBTTtZQUNMLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQy9CO1FBRUQsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7YUFDekMsUUFBUSxDQUFDLHVCQUF1QixDQUFDO2FBQ2pDLFlBQVksQ0FBQyxPQUFPLEVBQUUsWUFBVSxLQUFLLE1BQUcsQ0FBQzthQUN6QyxXQUFXLENBQUMsU0FBUyxDQUFDO2FBQ3RCLFlBQVksQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUE7UUFFckMsSUFBSSxPQUFPLElBQUksT0FBTyxLQUFLLEVBQUUsRUFBRTtZQUM3QixnQkFBZ0I7aUJBQ2IsUUFBUSxDQUFDLGFBQWEsQ0FBQztpQkFDdkIsUUFBUSxDQUFDLHVCQUF1QixDQUFDO2lCQUNqQyxZQUFZLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFBO1NBQ3ZDO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDMUQsT0FBTyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUE7SUFDakMsQ0FBQztJQUVTLCtDQUFrQixHQUE1QixVQUE2QixRQUFtQjs7UUFDOUMsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFBOztZQUNoQixLQUFpQixJQUFBLGFBQUEsaUJBQUEsUUFBUSxDQUFBLGtDQUFBLHdEQUFFO2dCQUF0QixJQUFJLElBQUkscUJBQUE7Z0JBQ1gsT0FBTyxJQUFPLElBQUksQ0FBQyxLQUFLLFVBQUssSUFBSSxDQUFDLEtBQUssU0FBSSxJQUFJLENBQUMsS0FBSyxPQUFJLENBQUE7YUFDMUQ7Ozs7Ozs7OztRQUVELE9BQU8sT0FBTyxDQUFDLElBQUksRUFBRSxDQUFBO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxtQ0FBTSxHQUFOLFVBQU8sSUFBZTtRQUNwQixJQUFJLElBQUksRUFBRTtZQUNSLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO1NBQ2xCO1FBRUQsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNILG9DQUFPLEdBQVA7O1FBQ0csSUFBWSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUE7UUFFL0IsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBQ3BDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUV2QyxJQUFZLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQztRQUN0QyxJQUFZLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQTs7WUFFekMsS0FBaUIsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxZQUFZLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQS9CLElBQUksSUFBSSxXQUFBO2dCQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUNiOzs7Ozs7Ozs7UUFFQSxJQUFZLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQztRQUN0QyxJQUFZLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQTtJQUNuQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsb0NBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtJQUNoQixDQUFDO0lBQ0gseUJBQUM7QUFBRCxDQTdRQSxBQTZRQyxDQTdRZ0MsVUFBVSxHQTZRMUM7QUFFRCxNQUFNLFVBQVUsSUFBSTtJQUNsQixtQkFBbUIsQ0FBYyx1QkFBdUIsRUFBRSxVQUFDLENBQUM7UUFDMUQsSUFBSSxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUMzQixDQUFDLENBQUMsQ0FBQTtBQUNKLENBQUM7QUFFRCxlQUFlLGtCQUFrQixDQUFBIiwiZmlsZSI6Im1haW4vc3JjL2NoYXJ0cy9CYXJDaGFydEhvcml6b250YWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRG9tRWxlbWVudCBmcm9tIFwiLi4vRG9tRWxlbWVudFwiXG5pbXBvcnQgeyBzZWFyY2hBbmRJbml0aWFsaXplLCByZW1vdmUgfSBmcm9tIFwiLi4vVXRpbHNcIlxuaW1wb3J0IHsgZ2V0QXR0cmlidXRlUmVmZXJlbmNlIH0gZnJvbSBcIi4uL0RvbUZ1bmN0aW9uc1wiXG5pbXBvcnQgeyB0cnlHZXREYXRhLCBjcmVhdGVMZWdlbmRJdGVtLCBpc0NvbG9yLCByZW1vdmVBbGxDaGlsZHJlbiwgQ2hhcnREYXRhLCBDaGFydEF4aXMgfSBmcm9tIFwiLi9DaGFydEZ1bmN0aW9uc1wiXG5cbmltcG9ydCB7IFRpbWVsaW5lTGl0ZSwgUG93ZXI0IH0gZnJvbSBcImdzYXBcIlxuXG5jb25zdCBRVUVSWV9ERVRBSUxfUklHSFQgPSBcIi5kZXRhaWwtcmlnaHRcIlxuY29uc3QgUVVFUllfREVUQUlMX0JPVFRPTSA9IFwiLmRldGFpbC1ib3R0b21cIlxuY29uc3QgUVVFUllfUFJPR1JFU1MgPSBcIi5iYXItY2hhcnRfX3Byb2dyZXNzXCJcblxuY29uc3QgQ0xBU1NfVU5MSU1JVEVEID0gXCJiYXItY2hhcnQtaG9yaXpvbnRhbC0tdW5saW1pdGVkXCJcbmNvbnN0IENMQVNTX0xJTUlURUQgPSBcImJhci1jaGFydC1ob3Jpem9udGFsLS1saW1pdGVkXCJcblxuY29uc3QgQ0xBU1NfREVUQUlMX1ZBTFVFID0gXCJ2YWx1ZVwiXG5jb25zdCBDTEFTU19ERVRBSUxfVU5JVCA9IFwidW5pdFwiXG5cbmNvbnN0IENMQVNTX0lORElDQVRPUiA9IFwiaW5kaWNhdG9yXCJcbmNvbnN0IENMQVNTX0lORElDQVRPUl9XUkFQUEVSID0gXCJpbmRpY2F0b3Itd3JhcHBlclwiXG5cbmNvbnN0IENMQVNTX1RPT0xUSVAgPSBcInRvb2x0aXBcIlxuY29uc3QgQ0xBU1NfVE9PTFRJUF9NVUxUSUxJTkUgPSBcInRvb2x0aXAtLW11bHRpbGluZVwiXG5cbmNvbnN0IEFOSU1BVElPTl9EVVJBVElPTiA9IDAuNVxuXG4vKipcbiAqIEJhciBDaGFydCBIb3Jpem9udGFsIENvbXBvbmVudC5cbiAqL1xuY2xhc3MgQmFyQ2hhcnRIb3Jpem9udGFsIGV4dGVuZHMgRG9tRWxlbWVudDxIVE1MRWxlbWVudD4ge1xuICBwcml2YXRlIF9kYXRhITogQ2hhcnREYXRhXG5cbiAgcHJpdmF0ZSBfbGVnZW5kSXRlbXM6IEhUTUxFbGVtZW50W11cbiAgcHJpdmF0ZSBfcHJvZ2Vzc1dyYXBwZXIhOiBIVE1MRWxlbWVudFxuXG4gIHByaXZhdGUgX3VuaXQhOiBzdHJpbmdcbiAgcHJpdmF0ZSBfbWF4VmFsdWUhOiBudW1iZXJcbiAgcHJpdmF0ZSBfcHJlY2lzaW9uITogbnVtYmVyXG5cbiAgcHJpdmF0ZSBfaXNVbmxpbWl0ZWQhOiBib29sZWFuXG4gIHByaXZhdGUgX2lzTGltaXRlZCE6IGJvb2xlYW5cblxuICBwcml2YXRlIF9kZXRhaWxSaWdodCE6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX2xlZ2VuZCE6IEhUTUxFbGVtZW50XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYW5kIGluaXRpYWxpemVzIHRoZSBiYXIgY2hhcnQgaG9yaXpvbnRhbCBjb21wb25lbnQuXG4gICAqIEBwYXJhbSB7RG9tRWxlbWVudH0gLSByb290IGVsZW1lbnQgb2YgdGhlIGNoYXJ0LlxuICAgKi9cbiAgY29uc3RydWN0b3IoZWxlbWVudDogSFRNTEVsZW1lbnQsIGRhdGE/OiBDaGFydERhdGEpIHtcbiAgICBzdXBlcihlbGVtZW50KVxuXG4gICAgaWYgKGRhdGEpIHtcbiAgICAgIHRoaXMuX2RhdGEgPSBkYXRhXG4gICAgfVxuXG4gICAgdGhpcy5fbGVnZW5kSXRlbXMgPSBbXVxuXG4gICAgdGhpcy5faW5pdGlhbGl6ZSgpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2luaXRpYWxpemUoKSB7XG4gICAgdGhpcy5fdW5pdCA9IHRoaXMuZ2V0QXR0cmlidXRlKFwiZGF0YS11bml0XCIpIHx8IFwiXCJcbiAgICB0aGlzLl9tYXhWYWx1ZSA9IHBhcnNlRmxvYXQodGhpcy5nZXRBdHRyaWJ1dGUoXCJkYXRhLW1heFwiKSEpXG4gICAgdGhpcy5fcHJlY2lzaW9uID0gcGFyc2VJbnQodGhpcy5nZXRBdHRyaWJ1dGUoXCJkYXRhLXByZWNpc2lvblwiKSEsIDEwKSB8fCAwXG5cbiAgICB0aGlzLl9pc1VubGltaXRlZCA9IHRoaXMuaGFzQ2xhc3MoQ0xBU1NfVU5MSU1JVEVEKVxuICAgIHRoaXMuX2lzTGltaXRlZCA9IHRoaXMuaGFzQ2xhc3MoQ0xBU1NfTElNSVRFRClcblxuICAgIHRoaXMuX3Byb2dlc3NXcmFwcGVyID0gdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoUVVFUllfUFJPR1JFU1MpISBhcyBIVE1MRWxlbWVudFxuXG4gICAgaWYgKHRoaXMuX2lzTGltaXRlZCA9PT0gdHJ1ZSkge1xuICAgICAgdGhpcy5fZGV0YWlsUmlnaHQgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihRVUVSWV9ERVRBSUxfQk9UVE9NKSEgYXMgSFRNTEVsZW1lbnRcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5fZGV0YWlsUmlnaHQgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihRVUVSWV9ERVRBSUxfUklHSFQpISBhcyBIVE1MRWxlbWVudFxuICAgIH1cblxuICAgIGlmICh0aGlzLl9pc1VubGltaXRlZCA9PT0gZmFsc2UgJiYgdGhpcy5faXNMaW1pdGVkID09PSBmYWxzZSkge1xuICAgICAgdGhpcy5fbGVnZW5kID0gZ2V0QXR0cmlidXRlUmVmZXJlbmNlKHRoaXMuZWxlbWVudCwgXCJkYXRhLWxlZ2VuZFwiKSFcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMuX2RhdGEpIHtcbiAgICAgIHRoaXMuX2RhdGEgPSB0cnlHZXREYXRhKHRoaXMuZWxlbWVudClcbiAgICB9XG5cbiAgICB0aGlzLl9yZW5kZXIoKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9yZW5kZXIoKSB7XG4gICAgbGV0IGRhdGFPbmUgPSB0aGlzLl9kYXRhWzBdXG4gICAgbGV0IGRhdGFUd28gPSB0aGlzLl9kYXRhWzFdXG5cbiAgICBsZXQgdGwgPSBuZXcgVGltZWxpbmVMaXRlKClcbiAgICBsZXQgdG9vbHRpcCA9IHRoaXMuX2lzTGltaXRlZCA9PT0gZmFsc2UgPyB0aGlzLl9nZXRUb29sdGlwQ29udGVudCh0aGlzLl9kYXRhKSA6IHVuZGVmaW5lZFxuXG4gICAgbGV0IGFuaW1hdGVkVmFsdWVFbGVtZW50OiBFbGVtZW50IHwgdW5kZWZpbmVkXG5cbiAgICAvLyBDbGVhbnVwXG4gICAgcmVtb3ZlQWxsQ2hpbGRyZW4odGhpcy5fZGV0YWlsUmlnaHQpXG4gICAgcmVtb3ZlQWxsQ2hpbGRyZW4odGhpcy5fcHJvZ2Vzc1dyYXBwZXIpXG5cbiAgICAvLyBDbGVhciBvbmx5IG93biBsZWdlbmQgaXRlbXNcbiAgICBmb3IgKGxldCBpdGVtIG9mIHRoaXMuX2xlZ2VuZEl0ZW1zKSB7XG4gICAgICByZW1vdmUoaXRlbSlcbiAgICB9XG4gICAgdGhpcy5fbGVnZW5kSXRlbXMgPSBbXVxuXG4gICAgaWYgKGRhdGFPbmUpIHtcbiAgICAgIGlmICh0aGlzLl9pc1VubGltaXRlZCA9PT0gZmFsc2UgfHwgKHRoaXMuX2lzVW5saW1pdGVkID09PSB0cnVlICYmICFkYXRhVHdvKSkge1xuXG4gICAgICAgIGxldCB2YWxFbGVtZW50ID0gYW5pbWF0ZWRWYWx1ZUVsZW1lbnQgPSB0aGlzLl9jcmVhdGVWYWx1ZUVsZW1lbnQoZGF0YU9uZSlcbiAgICAgICAgdGhpcy5fZGV0YWlsUmlnaHQuYXBwZW5kQ2hpbGQodmFsRWxlbWVudClcblxuICAgICAgICBpZiAodGhpcy5faXNMaW1pdGVkID09PSBmYWxzZSkge1xuICAgICAgICAgIGxldCBzZXBhcmF0b3JFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQoXCJkaXZcIilcbiAgICAgICAgICAgIC5hZGRDbGFzcyhDTEFTU19ERVRBSUxfVU5JVClcbiAgICAgICAgICAgIC5zZXRIdG1sKGAgJHt0aGlzLl91bml0fWApXG4gICAgICAgICAgICAuZWxlbWVudFxuXG4gICAgICAgICAgdGhpcy5fZGV0YWlsUmlnaHQuYXBwZW5kQ2hpbGQoc2VwYXJhdG9yRWxlbWVudClcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBBZGQgdGhlIGluZGljYXRvclxuICAgICAgbGV0IGluZGljYXRvciA9IHRoaXMuX2FkZEluZGljYXRvcihkYXRhT25lLCB0b29sdGlwKVxuXG4gICAgICB0bC5mcm9tKGluZGljYXRvciwgQU5JTUFUSU9OX0RVUkFUSU9OLCB7XG4gICAgICAgIHdpZHRoOiAwLFxuICAgICAgICBlYXNlOiBQb3dlcjQuZWFzZUluT3V0XG4gICAgICB9KVxuXG4gICAgICAvLyBBbmltYXRlIHRoZSB2YWx1ZSBpZiByZXF1aXJlZFxuICAgICAgaWYgKGFuaW1hdGVkVmFsdWVFbGVtZW50ICYmIHRoaXMuX2lzTGltaXRlZCA9PT0gdHJ1ZSkge1xuICAgICAgICBsZXQgY291bnRlciA9IHsgdmFyOiAwIH1cbiAgICAgICAgdGwudG8oY291bnRlciwgQU5JTUFUSU9OX0RVUkFUSU9OLCB7XG4gICAgICAgICAgdmFyOiBkYXRhT25lLnZhbHVlLFxuICAgICAgICAgIHJvdW5kUHJvcHM6IFwidmFyXCIsXG4gICAgICAgICAgb25VcGRhdGU6ICgpID0+IHtcbiAgICAgICAgICAgIGFuaW1hdGVkVmFsdWVFbGVtZW50IS5pbm5lckhUTUwgPSBgJHtjb3VudGVyLnZhcn1gXG4gICAgICAgICAgfSxcbiAgICAgICAgICBlYXNlOiBQb3dlcjQuZWFzZU91dFxuICAgICAgICB9LCBgLT0ke0FOSU1BVElPTl9EVVJBVElPTn1gKVxuICAgICAgfVxuXG4gICAgICAvLyBBZGQgdGhlIGxlZ2VuZFxuICAgICAgaWYgKHRoaXMuX2xlZ2VuZCkge1xuICAgICAgICBjb25zdCBsZWdlbmRJdGVtID0gY3JlYXRlTGVnZW5kSXRlbShkYXRhT25lKVxuICAgICAgICB0aGlzLl9sZWdlbmQuYXBwZW5kQ2hpbGQobGVnZW5kSXRlbSlcbiAgICAgICAgdGhpcy5fbGVnZW5kSXRlbXMucHVzaChsZWdlbmRJdGVtKVxuXG4gICAgICAgIHRsLmZyb20obGVnZW5kSXRlbSwgQU5JTUFUSU9OX0RVUkFUSU9OLCB7XG4gICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICBlYXNlOiBQb3dlcjQuZWFzZUluT3V0XG4gICAgICAgIH0sIGAtPSR7QU5JTUFUSU9OX0RVUkFUSU9OfWApXG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGRhdGFUd28pIHtcbiAgICAgIGxldCB2YWxFbGVtZW50ID0gdGhpcy5fY3JlYXRlVmFsdWVFbGVtZW50KGRhdGFUd28pXG5cbiAgICAgIGxldCB1bml0RWxlbWVudCA9IG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAgIC5hZGRDbGFzcyhDTEFTU19ERVRBSUxfVU5JVClcbiAgICAgICAgLnNldEh0bWwoYCAke3RoaXMuX3VuaXR9YClcbiAgICAgICAgLmVsZW1lbnRcblxuICAgICAgdGhpcy5fZGV0YWlsUmlnaHQuYXBwZW5kQ2hpbGQodmFsRWxlbWVudClcbiAgICAgIHRoaXMuX2RldGFpbFJpZ2h0LmFwcGVuZENoaWxkKHVuaXRFbGVtZW50KVxuXG4gICAgICAvLyBBZGQgdGhlIGluZGljYXRvclxuICAgICAgbGV0IGluZGljYXRvciA9IHRoaXMuX2FkZEluZGljYXRvcihkYXRhVHdvLCB0b29sdGlwKVxuICAgICAgdGwuZnJvbShpbmRpY2F0b3IsIEFOSU1BVElPTl9EVVJBVElPTiwge1xuICAgICAgICB3aWR0aDogMCxcbiAgICAgICAgZWFzZTogUG93ZXI0LmVhc2VJbk91dFxuICAgICAgfSlcblxuICAgICAgLy8gQWRkIHRoZSBsZWdlbmRcbiAgICAgIGlmICh0aGlzLl9sZWdlbmQpIHtcbiAgICAgICAgY29uc3QgbGVnZW5kSXRlbSA9IGNyZWF0ZUxlZ2VuZEl0ZW0oZGF0YVR3bylcbiAgICAgICAgdGhpcy5fbGVnZW5kLmFwcGVuZENoaWxkKGxlZ2VuZEl0ZW0pXG4gICAgICAgIHRoaXMuX2xlZ2VuZEl0ZW1zLnB1c2gobGVnZW5kSXRlbSlcblxuICAgICAgICB0bC5mcm9tKGxlZ2VuZEl0ZW0sIEFOSU1BVElPTl9EVVJBVElPTiwge1xuICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgZWFzZTogUG93ZXI0LmVhc2VJbk91dFxuICAgICAgICB9LCBgLT0ke0FOSU1BVElPTl9EVVJBVElPTn1gKVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLl9pc0xpbWl0ZWQgPT09IHRydWUpIHtcbiAgICAgIGxldCB2YWxFbGVtZW50ID0gdGhpcy5fY3JlYXRlVmFsdWVFbGVtZW50KHsgdmFsdWU6IHRoaXMuX21heFZhbHVlIH0pXG5cbiAgICAgIGxldCB1bml0RWxlbWVudCA9IG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAgIC5hZGRDbGFzcyhDTEFTU19ERVRBSUxfVU5JVClcbiAgICAgICAgLnNldEh0bWwoYCAke3RoaXMuX3VuaXR9YClcbiAgICAgICAgLmVsZW1lbnRcblxuICAgICAgdGhpcy5fZGV0YWlsUmlnaHQuYXBwZW5kQ2hpbGQodmFsRWxlbWVudClcbiAgICAgIHRoaXMuX2RldGFpbFJpZ2h0LmFwcGVuZENoaWxkKHVuaXRFbGVtZW50KVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfY3JlYXRlVmFsdWVFbGVtZW50KGRhdGE6IHsgdmFsdWU6IG51bWJlciB8IHN0cmluZyB9KSB7XG4gICAgbGV0IHVubGltaXRlZFByZWZpeCA9IFwiXCJcblxuICAgIGlmICh0aGlzLl9pc1VubGltaXRlZCA9PT0gdHJ1ZSkge1xuICAgICAgdW5saW1pdGVkUHJlZml4ID0gXCIrXCJcbiAgICB9XG5cbiAgICBsZXQgdmFsdWU6IHN0cmluZyB8IG51bWJlciA9IHBhcnNlRmxvYXQoKGRhdGEudmFsdWUgYXMgc3RyaW5nKSlcbiAgICBpZiAodmFsdWUgPD0gMCkge1xuICAgICAgdmFsdWUgPSBcIi4wMFwiXG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlID0gdmFsdWUudG9GaXhlZCh0aGlzLl9wcmVjaXNpb24pXG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAuYWRkQ2xhc3MoQ0xBU1NfREVUQUlMX1ZBTFVFKVxuICAgICAgLnNldEh0bWwoYCR7dW5saW1pdGVkUHJlZml4fSR7dmFsdWV9YClcbiAgICAgIC5lbGVtZW50XG4gIH1cblxuICBwcm90ZWN0ZWQgX2FkZEluZGljYXRvcihkYXRhOiBDaGFydEF4aXMsIHRvb2x0aXA/OiBzdHJpbmcpIHtcbiAgICBsZXQgd2lkdGggPSAoKDk5LjggLyB0aGlzLl9tYXhWYWx1ZSkgKiBkYXRhLnZhbHVlKVxuXG4gICAgbGV0IGluZGljYXRvciA9IG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAuYWRkQ2xhc3MoQ0xBU1NfSU5ESUNBVE9SKVxuXG4gICAgaWYgKGlzQ29sb3IoZGF0YS5jb2xvcikgPT09IHRydWUpIHtcbiAgICAgIGluZGljYXRvci5zZXRBdHRyaWJ1dGUoXCJzdHlsZVwiLCBgYmFja2dyb3VuZC1jb2xvcjogJHtkYXRhLmNvbG9yfTtgKVxuICAgIH0gZWxzZSB7XG4gICAgICBpbmRpY2F0b3IuYWRkQ2xhc3MoZGF0YS5jb2xvcilcbiAgICB9XG5cbiAgICBsZXQgaW5kaWNhdG9yV3JhcHBlciA9IG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAuYWRkQ2xhc3MoQ0xBU1NfSU5ESUNBVE9SX1dSQVBQRVIpXG4gICAgICAuc2V0QXR0cmlidXRlKFwic3R5bGVcIiwgYHdpZHRoOiAke3dpZHRofSVgKVxuICAgICAgLmFwcGVuZENoaWxkKGluZGljYXRvcilcbiAgICAgIC5zZXRBdHRyaWJ1dGUoXCJvbmNsaWNrXCIsIFwidm9pZCgwKVwiKVxuXG4gICAgaWYgKHRvb2x0aXAgJiYgdG9vbHRpcCAhPT0gXCJcIikge1xuICAgICAgaW5kaWNhdG9yV3JhcHBlclxuICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfVE9PTFRJUClcbiAgICAgICAgLmFkZENsYXNzKENMQVNTX1RPT0xUSVBfTVVMVElMSU5FKVxuICAgICAgICAuc2V0QXR0cmlidXRlKFwiYXJpYS1sYWJlbFwiLCB0b29sdGlwKVxuICAgIH1cblxuICAgIHRoaXMuX3Byb2dlc3NXcmFwcGVyLmFwcGVuZENoaWxkKGluZGljYXRvcldyYXBwZXIuZWxlbWVudClcbiAgICByZXR1cm4gaW5kaWNhdG9yV3JhcHBlci5lbGVtZW50XG4gIH1cblxuICBwcm90ZWN0ZWQgX2dldFRvb2x0aXBDb250ZW50KGRhdGFMaXN0OiBDaGFydERhdGEpIHtcbiAgICBsZXQgdG9vbHRpcCA9IFwiXCJcbiAgICBmb3IgKGxldCBkYXRhIG9mIGRhdGFMaXN0KSB7XG4gICAgICB0b29sdGlwICs9IGAke2RhdGEudGl0bGV9OiAke2RhdGEudmFsdWV9ICR7dGhpcy5fdW5pdH1cXG5gXG4gICAgfVxuXG4gICAgcmV0dXJuIHRvb2x0aXAudHJpbSgpXG4gIH1cblxuICAvKipcbiAgICogVXBkYXRlcyB0aGUgYmFyIGNoYXJ0IHdpdGggdGhlIHNwZWNpZmllZCBkYXRhIGRlZmluaXRpb25zLlxuICAgKiBAcGFyYW0ge0FycmF5fSAtIGJhciBjaGFydCBkYXRhIGRlZmluaXRpb25zLlxuICAgKi9cbiAgdXBkYXRlKGRhdGE6IENoYXJ0RGF0YSkge1xuICAgIGlmIChkYXRhKSB7XG4gICAgICB0aGlzLl9kYXRhID0gZGF0YVxuICAgIH1cblxuICAgIHRoaXMuX3JlbmRlcigpXG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbGwgZXZlbnQgaGFuZGxlcnMgYW5kIGNsZWFycyByZWZlcmVuY2VzLlxuICAgKi9cbiAgZGVzdHJveSgpIHtcbiAgICAodGhpcyBhcyBhbnkpLl9kYXRhID0gdW5kZWZpbmVkXG5cbiAgICByZW1vdmVBbGxDaGlsZHJlbih0aGlzLl9kZXRhaWxSaWdodClcbiAgICByZW1vdmVBbGxDaGlsZHJlbih0aGlzLl9wcm9nZXNzV3JhcHBlcik7XG5cbiAgICAodGhpcyBhcyBhbnkpLl9kZXRhaWxSaWdodCA9IHVuZGVmaW5lZDtcbiAgICAodGhpcyBhcyBhbnkpLl9wcm9nZXNzV3JhcHBlciA9IHVuZGVmaW5lZFxuXG4gICAgZm9yIChsZXQgaXRlbSBvZiB0aGlzLl9sZWdlbmRJdGVtcykge1xuICAgICAgcmVtb3ZlKGl0ZW0pXG4gICAgfVxuXG4gICAgKHRoaXMgYXMgYW55KS5fbGVnZW5kSXRlbXMgPSB1bmRlZmluZWQ7XG4gICAgKHRoaXMgYXMgYW55KS5fbGVnZW5kID0gdW5kZWZpbmVkXG4gIH1cblxuICAvKipcbiAgICogQGRlcHJlY2F0ZWQgdXNlIGRlc3Ryb3koKSBpbnN0ZWFkLlxuICAgKiBAdG9kbyByZW1vdmUgaW4gdmVyc2lvbiAyLjAuMFxuICAgKi9cbiAgZGVzdG9yeSgpIHtcbiAgICB0aGlzLmRlc3Ryb3koKVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0KCkge1xuICBzZWFyY2hBbmRJbml0aWFsaXplPEhUTUxFbGVtZW50PihcIi5iYXItY2hhcnQtaG9yaXpvbnRhbFwiLCAoZSkgPT4ge1xuICAgIG5ldyBCYXJDaGFydEhvcml6b250YWwoZSlcbiAgfSlcbn1cblxuZXhwb3J0IGRlZmF1bHQgQmFyQ2hhcnRIb3Jpem9udGFsXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
