import * as tslib_1 from "tslib";
import DomElement from "../DomElement";
import { searchAndInitialize } from "../Utils";
import { text } from "../DomFunctions";
import { createLegendItem, isColor, removeAllChildren } from "./ChartFunctions";
import { TimelineLite, Power4 } from "gsap";
var QUERY_DATA_CATEGORIES = ".js-data-list .js-category";
var QUERY_DATA_ITEMS = ".js-data-list .js-data";
var QUERY_CHART = ".js-chart";
var QUERY_LEGEND = ".bar-chart__legend";
var CLASS_INDICATOR = "indicator";
var CLASS_LABEL_X = "axis-x-label";
var CLASS_INDICATOR_WRAPPER = "indicator-wrapper";
var CLASS_INDICATOR_INNER_WRAPPER = "indicator-wrapper-inner";
var CLASS_INDICATOR_EMPTY = "empty";
var CLASS_TOOLTIP = "tooltip";
var CLASS_TOOLTIP_RIGHT = "tooltip--right";
var CLASS_TOOLTIP_MULTILINE = "tooltip--multiline";
var ANIMATION_DURATION = 0.5;
/**
 * Bar Chart Horizontal Component.
 */
var BarChartVertical = /** @class */ (function (_super) {
    tslib_1.__extends(BarChartVertical, _super);
    /**
     * Creates and initializes the bar chart horizontal component.
     * @param {DomElement} - root element of the chart.
     */
    function BarChartVertical(element, data) {
        var _this = _super.call(this, element) || this;
        if (data) {
            _this._data = data;
        }
        _this._initialize();
        return _this;
    }
    BarChartVertical.prototype._initialize = function () {
        this._unit = this.getAttribute("data-unit") || "";
        this._maxValue = parseFloat(this.getAttribute("data-max")) || 100;
        this._chart = this.element.querySelector(QUERY_CHART);
        this._legend = this.element.querySelector(QUERY_LEGEND);
        if (!this._data) {
            this._data = this._tryGetData(this.element);
        }
        this._render();
    };
    BarChartVertical.prototype._tryGetData = function (element) {
        var e_1, _a, e_2, _b, e_3, _c;
        var data = {
            categories: [],
            items: []
        };
        var categories = element.querySelectorAll(QUERY_DATA_CATEGORIES);
        var items = element.querySelectorAll(QUERY_DATA_ITEMS);
        try {
            for (var categories_1 = tslib_1.__values(categories), categories_1_1 = categories_1.next(); !categories_1_1.done; categories_1_1 = categories_1.next()) {
                var category = categories_1_1.value;
                data.categories.push({
                    title: text(category),
                    color: category.getAttribute("data-color")
                });
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (categories_1_1 && !categories_1_1.done && (_a = categories_1.return)) _a.call(categories_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        try {
            for (var items_1 = tslib_1.__values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                var item = items_1_1.value;
                var dataEnty = {
                    title: text(item),
                    class: item.getAttribute("data-class"),
                    values: []
                };
                var vals = item.getAttribute("data-value");
                if (vals) {
                    try {
                        for (var _d = tslib_1.__values(vals.split(",")), _e = _d.next(); !_e.done; _e = _d.next()) {
                            var val = _e.value;
                            dataEnty.values.push(parseFloat(val));
                        }
                    }
                    catch (e_3_1) { e_3 = { error: e_3_1 }; }
                    finally {
                        try {
                            if (_e && !_e.done && (_c = _d.return)) _c.call(_d);
                        }
                        finally { if (e_3) throw e_3.error; }
                    }
                }
                data.items.push(dataEnty);
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (items_1_1 && !items_1_1.done && (_b = items_1.return)) _b.call(items_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return data;
    };
    BarChartVertical.prototype._getTooltipContent = function (entry, categories) {
        var tooltip = "";
        for (var i = 0; i < entry.values.length; i++) {
            tooltip += categories[i].title + ": " + entry.values[i] + " " + this._unit + "\n";
        }
        return tooltip.trim();
    };
    BarChartVertical.prototype._render = function () {
        var e_4, _a, e_5, _b;
        if (this._legend) {
            removeAllChildren(this._legend);
            try {
                for (var _c = tslib_1.__values(this._data.categories), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var category = _d.value;
                    var legendItem = createLegendItem(category);
                    this._legend.appendChild(legendItem);
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_4) throw e_4.error; }
            }
        }
        removeAllChildren(this._chart);
        var animationStages = [];
        try {
            for (var _e = tslib_1.__values(this._data.items), _f = _e.next(); !_f.done; _f = _e.next()) {
                var item = _f.value;
                var element = new DomElement("li");
                if (item.class && item.class !== "") {
                    element.addClass(item.class);
                }
                var listElement = new DomElement("ul")
                    .addClass(CLASS_INDICATOR_WRAPPER);
                var wrapper = new DomElement("div")
                    .addClass(CLASS_INDICATOR_INNER_WRAPPER);
                listElement.appendChild(wrapper);
                element.appendChild(listElement);
                var tooltip = this._getTooltipContent(item, this._data.categories);
                if (tooltip && tooltip !== "") {
                    wrapper
                        .addClass(CLASS_TOOLTIP)
                        .addClass(CLASS_TOOLTIP_RIGHT)
                        .setAttribute("aria-label", tooltip);
                    if (item.values.length > 1) {
                        wrapper.addClass(CLASS_TOOLTIP_MULTILINE);
                    }
                }
                for (var i = 0; i < item.values.length; i++) {
                    var height = (this._chart.offsetHeight / this._maxValue) * item.values[i];
                    var indicator = new DomElement("li")
                        .addClass(CLASS_INDICATOR)
                        .setAttribute("style", "height: " + height + "px;");
                    if (height > 0) {
                        var color = this._data.categories[i].color;
                        if (isColor(color) === true) {
                            indicator.setAttribute("style", "background-color: " + color + ";");
                        }
                        else {
                            indicator.addClass(color);
                        }
                        if (animationStages.length <= i) {
                            animationStages.push([]);
                        }
                        animationStages[i].push(indicator.element);
                    }
                    else {
                        indicator.addClass(CLASS_INDICATOR_EMPTY);
                    }
                    wrapper.appendChild(indicator);
                }
                element.appendChild(new DomElement("div")
                    .addClass(CLASS_LABEL_X)
                    .setHtml(item.title));
                this._chart.appendChild(element.element);
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
            }
            finally { if (e_5) throw e_5.error; }
        }
        var tl = new TimelineLite();
        for (var i = 0; i < animationStages.length; i++) {
            tl.from(animationStages[i], ANIMATION_DURATION, {
                height: 0,
                ease: Power4.easeInOut,
                autoRound: false
            });
            if (this._legend) {
                tl.from(this._legend.children[i], ANIMATION_DURATION, {
                    opacity: 0,
                    ease: Power4.easeInOut
                }, "-=" + ANIMATION_DURATION);
            }
        }
    };
    /**
     * Updates the bar chart with the specified data definitions.
     * @param {Array} - bar chart data definitions.
     */
    BarChartVertical.prototype.update = function (data) {
        if (data) {
            this._data = data;
        }
        this._render();
    };
    /**
     * Removes all event handlers and clears references.
     */
    BarChartVertical.prototype.destroy = function () {
        this._data = undefined;
        if (this._legend) {
            removeAllChildren(this._legend);
            this._legend = undefined;
        }
    };
    /**
     * @deprecated use destroy() instead.
     * @todo remove in version 2.0.0
     */
    BarChartVertical.prototype.destory = function () {
        this.destroy();
    };
    return BarChartVertical;
}(DomElement));
export function init() {
    searchAndInitialize(".bar-chart-vertical", function (e) {
        new BarChartVertical(e);
    });
}
export default BarChartVertical;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2NoYXJ0cy9CYXJDaGFydFZlcnRpY2FsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sVUFBVSxDQUFBO0FBQzlDLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFjLE1BQU0sa0JBQWtCLENBQUE7QUFFM0YsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxNQUFNLENBQUE7QUFFM0MsSUFBTSxxQkFBcUIsR0FBRyw0QkFBNEIsQ0FBQTtBQUMxRCxJQUFNLGdCQUFnQixHQUFHLHdCQUF3QixDQUFBO0FBQ2pELElBQU0sV0FBVyxHQUFHLFdBQVcsQ0FBQTtBQUMvQixJQUFNLFlBQVksR0FBRyxvQkFBb0IsQ0FBQTtBQUV6QyxJQUFNLGVBQWUsR0FBRyxXQUFXLENBQUE7QUFDbkMsSUFBTSxhQUFhLEdBQUcsY0FBYyxDQUFBO0FBQ3BDLElBQU0sdUJBQXVCLEdBQUcsbUJBQW1CLENBQUE7QUFDbkQsSUFBTSw2QkFBNkIsR0FBRyx5QkFBeUIsQ0FBQTtBQUMvRCxJQUFNLHFCQUFxQixHQUFHLE9BQU8sQ0FBQTtBQUVyQyxJQUFNLGFBQWEsR0FBRyxTQUFTLENBQUE7QUFDL0IsSUFBTSxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQTtBQUM1QyxJQUFNLHVCQUF1QixHQUFHLG9CQUFvQixDQUFBO0FBRXBELElBQU0sa0JBQWtCLEdBQUcsR0FBRyxDQUFBO0FBZ0I5Qjs7R0FFRztBQUNIO0lBQStCLDRDQUF1QjtJQVNwRDs7O09BR0c7SUFDSCwwQkFBWSxPQUFvQixFQUFFLElBQWdCO1FBQWxELFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBT2Y7UUFMQyxJQUFJLElBQUksRUFBRTtZQUNSLEtBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO1NBQ2xCO1FBRUQsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFBOztJQUNwQixDQUFDO0lBRVMsc0NBQVcsR0FBckI7UUFDRSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFBO1FBRWpELElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFFLENBQUMsSUFBSSxHQUFHLENBQUE7UUFFbEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQWlCLENBQUE7UUFDckUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQWlCLENBQUE7UUFFdkUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQzVDO1FBRUQsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO0lBQ2hCLENBQUM7SUFFUyxzQ0FBVyxHQUFyQixVQUFzQixPQUFvQjs7UUFDeEMsSUFBSSxJQUFJLEdBQWM7WUFDcEIsVUFBVSxFQUFFLEVBQUU7WUFDZCxLQUFLLEVBQUUsRUFBRTtTQUNWLENBQUE7UUFFRCxJQUFJLFVBQVUsR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsQ0FBQTtRQUNoRSxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTs7WUFFdEQsS0FBcUIsSUFBQSxlQUFBLGlCQUFBLFVBQVUsQ0FBQSxzQ0FBQSw4REFBRTtnQkFBNUIsSUFBSSxRQUFRLHVCQUFBO2dCQUNmLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUNsQjtvQkFDRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQztvQkFDckIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFFO2lCQUM1QyxDQUNGLENBQUE7YUFDRjs7Ozs7Ozs7OztZQUVELEtBQWlCLElBQUEsVUFBQSxpQkFBQSxLQUFLLENBQUEsNEJBQUEsK0NBQUU7Z0JBQW5CLElBQUksSUFBSSxrQkFBQTtnQkFDWCxJQUFJLFFBQVEsR0FBYztvQkFDeEIsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2pCLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBRTtvQkFDdkMsTUFBTSxFQUFFLEVBQUU7aUJBQ1gsQ0FBQTtnQkFFRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFBO2dCQUMxQyxJQUFJLElBQUksRUFBRTs7d0JBQ1IsS0FBZ0IsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7NEJBQTVCLElBQUksR0FBRyxXQUFBOzRCQUNWLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO3lCQUN0Qzs7Ozs7Ozs7O2lCQUNGO2dCQUVELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO2FBQzFCOzs7Ozs7Ozs7UUFFRCxPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFUyw2Q0FBa0IsR0FBNUIsVUFBNkIsS0FBZ0IsRUFBRSxVQUFzQjtRQUNuRSxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUE7UUFDaEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzVDLE9BQU8sSUFBTyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxVQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQUksSUFBSSxDQUFDLEtBQUssT0FBSSxDQUFBO1NBQ3hFO1FBRUQsT0FBTyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUE7SUFDdkIsQ0FBQztJQUVTLGtDQUFPLEdBQWpCOztRQUNFLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7O2dCQUUvQixLQUFxQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUEsZ0JBQUEsNEJBQUU7b0JBQXZDLElBQUksUUFBUSxXQUFBO29CQUNmLElBQU0sVUFBVSxHQUFHLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFBO29CQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtpQkFDckM7Ozs7Ozs7OztTQUNGO1FBRUQsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBRTlCLElBQUksZUFBZSxHQUFnQixFQUFFLENBQUE7O1lBRXJDLEtBQWlCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQSxnQkFBQSw0QkFBRTtnQkFBOUIsSUFBSSxJQUFJLFdBQUE7Z0JBQ1gsSUFBSSxPQUFPLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBRWxDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLEVBQUUsRUFBRTtvQkFDbkMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7aUJBQzdCO2dCQUVELElBQUksV0FBVyxHQUFHLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQztxQkFDbkMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLENBQUE7Z0JBRXBDLElBQUksT0FBTyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQztxQkFDaEMsUUFBUSxDQUFDLDZCQUE2QixDQUFDLENBQUE7Z0JBQzFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUE7Z0JBRWhDLE9BQU8sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUE7Z0JBRWhDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQTtnQkFDbEUsSUFBSSxPQUFPLElBQUksT0FBTyxLQUFLLEVBQUUsRUFBRTtvQkFDN0IsT0FBTzt5QkFDSixRQUFRLENBQUMsYUFBYSxDQUFDO3lCQUN2QixRQUFRLENBQUMsbUJBQW1CLENBQUM7eUJBQzdCLFlBQVksQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUE7b0JBRXRDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMxQixPQUFPLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLENBQUE7cUJBQzFDO2lCQUNGO2dCQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDM0MsSUFBTSxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFFM0UsSUFBSSxTQUFTLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDO3lCQUNqQyxRQUFRLENBQUMsZUFBZSxDQUFDO3lCQUN6QixZQUFZLENBQUMsT0FBTyxFQUFFLGFBQVcsTUFBTSxRQUFLLENBQUMsQ0FBQTtvQkFFaEQsSUFBSSxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNkLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTt3QkFDMUMsSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFOzRCQUMzQixTQUFTLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSx1QkFBcUIsS0FBSyxNQUFHLENBQUMsQ0FBQTt5QkFDL0Q7NkJBQU07NEJBQ0wsU0FBUyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQTt5QkFDMUI7d0JBRUQsSUFBSSxlQUFlLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTs0QkFDL0IsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTt5QkFDekI7d0JBRUQsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7cUJBQzNDO3lCQUFNO3dCQUNMLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQTtxQkFDMUM7b0JBRUQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQTtpQkFDL0I7Z0JBRUQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7cUJBQ3RDLFFBQVEsQ0FBQyxhQUFhLENBQUM7cUJBQ3ZCLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtnQkFFdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO2FBQ3pDOzs7Ozs7Ozs7UUFFRCxJQUFJLEVBQUUsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFBO1FBQzNCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLEVBQUUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFLGtCQUFrQixFQUFFO2dCQUM5QyxNQUFNLEVBQUUsQ0FBQztnQkFDVCxJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVM7Z0JBQ3RCLFNBQVMsRUFBRSxLQUFLO2FBQ2pCLENBQUMsQ0FBQTtZQUVGLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEIsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxrQkFBa0IsRUFBRTtvQkFDcEQsT0FBTyxFQUFFLENBQUM7b0JBQ1YsSUFBSSxFQUFFLE1BQU0sQ0FBQyxTQUFTO2lCQUN2QixFQUFFLE9BQUssa0JBQW9CLENBQUMsQ0FBQTthQUM5QjtTQUNGO0lBQ0gsQ0FBQztJQUVEOzs7T0FHRztJQUNILGlDQUFNLEdBQU4sVUFBTyxJQUFlO1FBQ3BCLElBQUksSUFBSSxFQUFFO1lBQ1IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7U0FDbEI7UUFFRCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUE7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsa0NBQU8sR0FBUDtRQUNHLElBQVksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFBO1FBRS9CLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDL0IsSUFBWSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUE7U0FDbEM7SUFDSCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsa0NBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtJQUNoQixDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQWpOQSxBQWlOQyxDQWpOOEIsVUFBVSxHQWlOeEM7QUFFRCxNQUFNLFVBQVUsSUFBSTtJQUNsQixtQkFBbUIsQ0FBYyxxQkFBcUIsRUFBRSxVQUFDLENBQUM7UUFDeEQsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUN6QixDQUFDLENBQUMsQ0FBQTtBQUNKLENBQUM7QUFFRCxlQUFlLGdCQUFnQixDQUFBIiwiZmlsZSI6Im1haW4vc3JjL2NoYXJ0cy9CYXJDaGFydFZlcnRpY2FsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IERvbUVsZW1lbnQgZnJvbSBcIi4uL0RvbUVsZW1lbnRcIlxuaW1wb3J0IHsgc2VhcmNoQW5kSW5pdGlhbGl6ZSB9IGZyb20gXCIuLi9VdGlsc1wiXG5pbXBvcnQgeyB0ZXh0IH0gZnJvbSBcIi4uL0RvbUZ1bmN0aW9uc1wiXG5pbXBvcnQgeyBjcmVhdGVMZWdlbmRJdGVtLCBpc0NvbG9yLCByZW1vdmVBbGxDaGlsZHJlbiwgQ2hhcnRMYWJlbCB9IGZyb20gXCIuL0NoYXJ0RnVuY3Rpb25zXCJcblxuaW1wb3J0IHsgVGltZWxpbmVMaXRlLCBQb3dlcjQgfSBmcm9tIFwiZ3NhcFwiXG5cbmNvbnN0IFFVRVJZX0RBVEFfQ0FURUdPUklFUyA9IFwiLmpzLWRhdGEtbGlzdCAuanMtY2F0ZWdvcnlcIlxuY29uc3QgUVVFUllfREFUQV9JVEVNUyA9IFwiLmpzLWRhdGEtbGlzdCAuanMtZGF0YVwiXG5jb25zdCBRVUVSWV9DSEFSVCA9IFwiLmpzLWNoYXJ0XCJcbmNvbnN0IFFVRVJZX0xFR0VORCA9IFwiLmJhci1jaGFydF9fbGVnZW5kXCJcblxuY29uc3QgQ0xBU1NfSU5ESUNBVE9SID0gXCJpbmRpY2F0b3JcIlxuY29uc3QgQ0xBU1NfTEFCRUxfWCA9IFwiYXhpcy14LWxhYmVsXCJcbmNvbnN0IENMQVNTX0lORElDQVRPUl9XUkFQUEVSID0gXCJpbmRpY2F0b3Itd3JhcHBlclwiXG5jb25zdCBDTEFTU19JTkRJQ0FUT1JfSU5ORVJfV1JBUFBFUiA9IFwiaW5kaWNhdG9yLXdyYXBwZXItaW5uZXJcIlxuY29uc3QgQ0xBU1NfSU5ESUNBVE9SX0VNUFRZID0gXCJlbXB0eVwiXG5cbmNvbnN0IENMQVNTX1RPT0xUSVAgPSBcInRvb2x0aXBcIlxuY29uc3QgQ0xBU1NfVE9PTFRJUF9SSUdIVCA9IFwidG9vbHRpcC0tcmlnaHRcIlxuY29uc3QgQ0xBU1NfVE9PTFRJUF9NVUxUSUxJTkUgPSBcInRvb2x0aXAtLW11bHRpbGluZVwiXG5cbmNvbnN0IEFOSU1BVElPTl9EVVJBVElPTiA9IDAuNVxuXG5leHBvcnQgaW50ZXJmYWNlIENhdGVnb3J5IGV4dGVuZHMgQ2hhcnRMYWJlbCB7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgRGF0YUVudHJ5IHtcbiAgdGl0bGU6IHN0cmluZ1xuICBjbGFzczogc3RyaW5nXG4gIHZhbHVlczogbnVtYmVyW11cbn1cblxuZXhwb3J0IGludGVyZmFjZSBDaGFydERhdGEge1xuICBjYXRlZ29yaWVzOiBDYXRlZ29yeVtdXG4gIGl0ZW1zOiBEYXRhRW50cnlbXVxufVxuXG4vKipcbiAqIEJhciBDaGFydCBIb3Jpem9udGFsIENvbXBvbmVudC5cbiAqL1xuY2xhc3MgQmFyQ2hhcnRWZXJ0aWNhbCBleHRlbmRzIERvbUVsZW1lbnQ8SFRNTEVsZW1lbnQ+IHtcbiAgcHJpdmF0ZSBfZGF0YSE6IENoYXJ0RGF0YVxuXG4gIHByaXZhdGUgX3VuaXQhOiBzdHJpbmdcbiAgcHJpdmF0ZSBfbWF4VmFsdWUhOiBudW1iZXJcblxuICBwcml2YXRlIF9jaGFydCE6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX2xlZ2VuZCE6IEhUTUxFbGVtZW50XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYW5kIGluaXRpYWxpemVzIHRoZSBiYXIgY2hhcnQgaG9yaXpvbnRhbCBjb21wb25lbnQuXG4gICAqIEBwYXJhbSB7RG9tRWxlbWVudH0gLSByb290IGVsZW1lbnQgb2YgdGhlIGNoYXJ0LlxuICAgKi9cbiAgY29uc3RydWN0b3IoZWxlbWVudDogSFRNTEVsZW1lbnQsIGRhdGE/OiBDaGFydERhdGEpIHtcbiAgICBzdXBlcihlbGVtZW50KVxuXG4gICAgaWYgKGRhdGEpIHtcbiAgICAgIHRoaXMuX2RhdGEgPSBkYXRhXG4gICAgfVxuXG4gICAgdGhpcy5faW5pdGlhbGl6ZSgpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2luaXRpYWxpemUoKSB7XG4gICAgdGhpcy5fdW5pdCA9IHRoaXMuZ2V0QXR0cmlidXRlKFwiZGF0YS11bml0XCIpIHx8IFwiXCJcblxuICAgIHRoaXMuX21heFZhbHVlID0gcGFyc2VGbG9hdCh0aGlzLmdldEF0dHJpYnV0ZShcImRhdGEtbWF4XCIpISkgfHwgMTAwXG5cbiAgICB0aGlzLl9jaGFydCA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX0NIQVJUKSEgYXMgSFRNTEVsZW1lbnRcbiAgICB0aGlzLl9sZWdlbmQgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihRVUVSWV9MRUdFTkQpISBhcyBIVE1MRWxlbWVudFxuXG4gICAgaWYgKCF0aGlzLl9kYXRhKSB7XG4gICAgICB0aGlzLl9kYXRhID0gdGhpcy5fdHJ5R2V0RGF0YSh0aGlzLmVsZW1lbnQpXG4gICAgfVxuXG4gICAgdGhpcy5fcmVuZGVyKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfdHJ5R2V0RGF0YShlbGVtZW50OiBIVE1MRWxlbWVudCk6IENoYXJ0RGF0YSB7XG4gICAgbGV0IGRhdGE6IENoYXJ0RGF0YSA9IHtcbiAgICAgIGNhdGVnb3JpZXM6IFtdLFxuICAgICAgaXRlbXM6IFtdXG4gICAgfVxuXG4gICAgbGV0IGNhdGVnb3JpZXMgPSBlbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoUVVFUllfREFUQV9DQVRFR09SSUVTKVxuICAgIGxldCBpdGVtcyA9IGVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChRVUVSWV9EQVRBX0lURU1TKVxuXG4gICAgZm9yIChsZXQgY2F0ZWdvcnkgb2YgY2F0ZWdvcmllcykge1xuICAgICAgZGF0YS5jYXRlZ29yaWVzLnB1c2goXG4gICAgICAgIHtcbiAgICAgICAgICB0aXRsZTogdGV4dChjYXRlZ29yeSksXG4gICAgICAgICAgY29sb3I6IGNhdGVnb3J5LmdldEF0dHJpYnV0ZShcImRhdGEtY29sb3JcIikhXG4gICAgICAgIH1cbiAgICAgIClcbiAgICB9XG5cbiAgICBmb3IgKGxldCBpdGVtIG9mIGl0ZW1zKSB7XG4gICAgICBsZXQgZGF0YUVudHk6IERhdGFFbnRyeSA9IHtcbiAgICAgICAgdGl0bGU6IHRleHQoaXRlbSksXG4gICAgICAgIGNsYXNzOiBpdGVtLmdldEF0dHJpYnV0ZShcImRhdGEtY2xhc3NcIikhLFxuICAgICAgICB2YWx1ZXM6IFtdXG4gICAgICB9XG5cbiAgICAgIGxldCB2YWxzID0gaXRlbS5nZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIpXG4gICAgICBpZiAodmFscykge1xuICAgICAgICBmb3IgKGxldCB2YWwgb2YgdmFscy5zcGxpdChcIixcIikpIHtcbiAgICAgICAgICBkYXRhRW50eS52YWx1ZXMucHVzaChwYXJzZUZsb2F0KHZhbCkpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgZGF0YS5pdGVtcy5wdXNoKGRhdGFFbnR5KVxuICAgIH1cblxuICAgIHJldHVybiBkYXRhXG4gIH1cblxuICBwcm90ZWN0ZWQgX2dldFRvb2x0aXBDb250ZW50KGVudHJ5OiBEYXRhRW50cnksIGNhdGVnb3JpZXM6IENhdGVnb3J5W10pIHtcbiAgICBsZXQgdG9vbHRpcCA9IFwiXCJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGVudHJ5LnZhbHVlcy5sZW5ndGg7IGkrKykge1xuICAgICAgdG9vbHRpcCArPSBgJHtjYXRlZ29yaWVzW2ldLnRpdGxlfTogJHtlbnRyeS52YWx1ZXNbaV19ICR7dGhpcy5fdW5pdH1cXG5gXG4gICAgfVxuXG4gICAgcmV0dXJuIHRvb2x0aXAudHJpbSgpXG4gIH1cblxuICBwcm90ZWN0ZWQgX3JlbmRlcigpIHtcbiAgICBpZiAodGhpcy5fbGVnZW5kKSB7XG4gICAgICByZW1vdmVBbGxDaGlsZHJlbih0aGlzLl9sZWdlbmQpXG5cbiAgICAgIGZvciAobGV0IGNhdGVnb3J5IG9mIHRoaXMuX2RhdGEuY2F0ZWdvcmllcykge1xuICAgICAgICBjb25zdCBsZWdlbmRJdGVtID0gY3JlYXRlTGVnZW5kSXRlbShjYXRlZ29yeSlcbiAgICAgICAgdGhpcy5fbGVnZW5kLmFwcGVuZENoaWxkKGxlZ2VuZEl0ZW0pXG4gICAgICB9XG4gICAgfVxuXG4gICAgcmVtb3ZlQWxsQ2hpbGRyZW4odGhpcy5fY2hhcnQpXG5cbiAgICBsZXQgYW5pbWF0aW9uU3RhZ2VzOiBFbGVtZW50W11bXSA9IFtdXG5cbiAgICBmb3IgKGxldCBpdGVtIG9mIHRoaXMuX2RhdGEuaXRlbXMpIHtcbiAgICAgIGxldCBlbGVtZW50ID0gbmV3IERvbUVsZW1lbnQoXCJsaVwiKVxuXG4gICAgICBpZiAoaXRlbS5jbGFzcyAmJiBpdGVtLmNsYXNzICE9PSBcIlwiKSB7XG4gICAgICAgIGVsZW1lbnQuYWRkQ2xhc3MoaXRlbS5jbGFzcylcbiAgICAgIH1cblxuICAgICAgbGV0IGxpc3RFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQoXCJ1bFwiKVxuICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfSU5ESUNBVE9SX1dSQVBQRVIpXG5cbiAgICAgIGxldCB3cmFwcGVyID0gbmV3IERvbUVsZW1lbnQoXCJkaXZcIilcbiAgICAgICAgLmFkZENsYXNzKENMQVNTX0lORElDQVRPUl9JTk5FUl9XUkFQUEVSKVxuICAgICAgbGlzdEVsZW1lbnQuYXBwZW5kQ2hpbGQod3JhcHBlcilcblxuICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZChsaXN0RWxlbWVudClcblxuICAgICAgbGV0IHRvb2x0aXAgPSB0aGlzLl9nZXRUb29sdGlwQ29udGVudChpdGVtLCB0aGlzLl9kYXRhLmNhdGVnb3JpZXMpXG4gICAgICBpZiAodG9vbHRpcCAmJiB0b29sdGlwICE9PSBcIlwiKSB7XG4gICAgICAgIHdyYXBwZXJcbiAgICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfVE9PTFRJUClcbiAgICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfVE9PTFRJUF9SSUdIVClcbiAgICAgICAgICAuc2V0QXR0cmlidXRlKFwiYXJpYS1sYWJlbFwiLCB0b29sdGlwKVxuXG4gICAgICAgIGlmIChpdGVtLnZhbHVlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgd3JhcHBlci5hZGRDbGFzcyhDTEFTU19UT09MVElQX01VTFRJTElORSlcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGl0ZW0udmFsdWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGNvbnN0IGhlaWdodCA9ICh0aGlzLl9jaGFydC5vZmZzZXRIZWlnaHQgLyB0aGlzLl9tYXhWYWx1ZSkgKiBpdGVtLnZhbHVlc1tpXVxuXG4gICAgICAgIGxldCBpbmRpY2F0b3IgPSBuZXcgRG9tRWxlbWVudChcImxpXCIpXG4gICAgICAgICAgLmFkZENsYXNzKENMQVNTX0lORElDQVRPUilcbiAgICAgICAgICAuc2V0QXR0cmlidXRlKFwic3R5bGVcIiwgYGhlaWdodDogJHtoZWlnaHR9cHg7YClcblxuICAgICAgICBpZiAoaGVpZ2h0ID4gMCkge1xuICAgICAgICAgIGxldCBjb2xvciA9IHRoaXMuX2RhdGEuY2F0ZWdvcmllc1tpXS5jb2xvclxuICAgICAgICAgIGlmIChpc0NvbG9yKGNvbG9yKSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgaW5kaWNhdG9yLnNldEF0dHJpYnV0ZShcInN0eWxlXCIsIGBiYWNrZ3JvdW5kLWNvbG9yOiAke2NvbG9yfTtgKVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpbmRpY2F0b3IuYWRkQ2xhc3MoY29sb3IpXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGFuaW1hdGlvblN0YWdlcy5sZW5ndGggPD0gaSkge1xuICAgICAgICAgICAgYW5pbWF0aW9uU3RhZ2VzLnB1c2goW10pXG4gICAgICAgICAgfVxuXG4gICAgICAgICAgYW5pbWF0aW9uU3RhZ2VzW2ldLnB1c2goaW5kaWNhdG9yLmVsZW1lbnQpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaW5kaWNhdG9yLmFkZENsYXNzKENMQVNTX0lORElDQVRPUl9FTVBUWSlcbiAgICAgICAgfVxuXG4gICAgICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQoaW5kaWNhdG9yKVxuICAgICAgfVxuXG4gICAgICBlbGVtZW50LmFwcGVuZENoaWxkKG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAgIC5hZGRDbGFzcyhDTEFTU19MQUJFTF9YKVxuICAgICAgICAuc2V0SHRtbChpdGVtLnRpdGxlKSlcblxuICAgICAgdGhpcy5fY2hhcnQuYXBwZW5kQ2hpbGQoZWxlbWVudC5lbGVtZW50KVxuICAgIH1cblxuICAgIGxldCB0bCA9IG5ldyBUaW1lbGluZUxpdGUoKVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYW5pbWF0aW9uU3RhZ2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB0bC5mcm9tKGFuaW1hdGlvblN0YWdlc1tpXSwgQU5JTUFUSU9OX0RVUkFUSU9OLCB7XG4gICAgICAgIGhlaWdodDogMCxcbiAgICAgICAgZWFzZTogUG93ZXI0LmVhc2VJbk91dCxcbiAgICAgICAgYXV0b1JvdW5kOiBmYWxzZVxuICAgICAgfSlcblxuICAgICAgaWYgKHRoaXMuX2xlZ2VuZCkge1xuICAgICAgICB0bC5mcm9tKHRoaXMuX2xlZ2VuZC5jaGlsZHJlbltpXSwgQU5JTUFUSU9OX0RVUkFUSU9OLCB7XG4gICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICBlYXNlOiBQb3dlcjQuZWFzZUluT3V0XG4gICAgICAgIH0sIGAtPSR7QU5JTUFUSU9OX0RVUkFUSU9OfWApXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZXMgdGhlIGJhciBjaGFydCB3aXRoIHRoZSBzcGVjaWZpZWQgZGF0YSBkZWZpbml0aW9ucy5cbiAgICogQHBhcmFtIHtBcnJheX0gLSBiYXIgY2hhcnQgZGF0YSBkZWZpbml0aW9ucy5cbiAgICovXG4gIHVwZGF0ZShkYXRhOiBDaGFydERhdGEpIHtcbiAgICBpZiAoZGF0YSkge1xuICAgICAgdGhpcy5fZGF0YSA9IGRhdGFcbiAgICB9XG5cbiAgICB0aGlzLl9yZW5kZXIoKVxuICB9XG5cbiAgLyoqXG4gICAqIFJlbW92ZXMgYWxsIGV2ZW50IGhhbmRsZXJzIGFuZCBjbGVhcnMgcmVmZXJlbmNlcy5cbiAgICovXG4gIGRlc3Ryb3koKSB7XG4gICAgKHRoaXMgYXMgYW55KS5fZGF0YSA9IHVuZGVmaW5lZFxuXG4gICAgaWYgKHRoaXMuX2xlZ2VuZCkge1xuICAgICAgcmVtb3ZlQWxsQ2hpbGRyZW4odGhpcy5fbGVnZW5kKTtcbiAgICAgICh0aGlzIGFzIGFueSkuX2xlZ2VuZCA9IHVuZGVmaW5lZFxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAZGVwcmVjYXRlZCB1c2UgZGVzdHJveSgpIGluc3RlYWQuXG4gICAqIEB0b2RvIHJlbW92ZSBpbiB2ZXJzaW9uIDIuMC4wXG4gICAqL1xuICBkZXN0b3J5KCkge1xuICAgIHRoaXMuZGVzdHJveSgpXG4gIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXQoKSB7XG4gIHNlYXJjaEFuZEluaXRpYWxpemU8SFRNTEVsZW1lbnQ+KFwiLmJhci1jaGFydC12ZXJ0aWNhbFwiLCAoZSkgPT4ge1xuICAgIG5ldyBCYXJDaGFydFZlcnRpY2FsKGUpXG4gIH0pXG59XG5cbmV4cG9ydCBkZWZhdWx0IEJhckNoYXJ0VmVydGljYWxcbiJdLCJzb3VyY2VSb290IjoiLi4vLi4vLi4vLi4vLi4ifQ==
