import * as tslib_1 from "tslib";
import DomElement from "../DomElement";
import { text } from "../DomFunctions";
var QUERY_DATA = ".js-data";
export function tryGetData(element) {
    var e_1, _a;
    var data = [];
    var elements = element.querySelectorAll(QUERY_DATA);
    try {
        for (var elements_1 = tslib_1.__values(elements), elements_1_1 = elements_1.next(); !elements_1_1.done; elements_1_1 = elements_1.next()) {
            var entry = elements_1_1.value;
            var value = parseFloat(entry.getAttribute("data-value"));
            var color = entry.getAttribute("data-color");
            var title = text(entry);
            var item = {
                title: title,
                value: value,
                color: color
            };
            data.push(item);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (elements_1_1 && !elements_1_1.done && (_a = elements_1.return)) _a.call(elements_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return data;
}
export function removeAllChildren(node) {
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}
export function createLegendItem(data) {
    var bullet = new DomElement("span")
        .addClass("bullet");
    if (isColor(data.color) === true) {
        bullet.setAttribute("style", "background-color: " + data.color + ";");
    }
    else {
        bullet.addClass(data.color);
    }
    var caption = new DomElement("span")
        .setHtml(data.title);
    return new DomElement("li")
        .appendChild(bullet)
        .appendChild(caption)
        .element;
}
export function isColor(str) {
    var pattern = /^#/i;
    return pattern.test(str);
}

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2NoYXJ0cy9DaGFydEZ1bmN0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxVQUFVLE1BQU0sZUFBZSxDQUFBO0FBQ3RDLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQWF0QyxJQUFNLFVBQVUsR0FBRyxVQUFVLENBQUE7QUFFN0IsTUFBTSxVQUFVLFVBQVUsQ0FBQyxPQUFvQjs7SUFDN0MsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFBO0lBQ2IsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFBOztRQUVuRCxLQUFrQixJQUFBLGFBQUEsaUJBQUEsUUFBUSxDQUFBLGtDQUFBLHdEQUFFO1lBQXZCLElBQUksS0FBSyxxQkFBQTtZQUNaLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBRSxDQUFDLENBQUE7WUFDekQsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUUsQ0FBQTtZQUM3QyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7WUFFdkIsSUFBSSxJQUFJLEdBQUc7Z0JBQ1QsS0FBSyxPQUFBO2dCQUNMLEtBQUssT0FBQTtnQkFDTCxLQUFLLE9BQUE7YUFDTixDQUFBO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtTQUNoQjs7Ozs7Ozs7O0lBRUQsT0FBTyxJQUFJLENBQUE7QUFDYixDQUFDO0FBRUQsTUFBTSxVQUFVLGlCQUFpQixDQUFDLElBQVU7SUFDMUMsT0FBTyxJQUFJLENBQUMsVUFBVSxFQUFFO1FBQ3RCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0tBQ2xDO0FBQ0gsQ0FBQztBQUVELE1BQU0sVUFBVSxnQkFBZ0IsQ0FBQyxJQUFnQjtJQUMvQyxJQUFNLE1BQU0sR0FBRyxJQUFJLFVBQVUsQ0FBa0IsTUFBTSxDQUFDO1NBQ25ELFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUVyQixJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFO1FBQ2hDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLHVCQUFxQixJQUFJLENBQUMsS0FBSyxNQUFHLENBQUMsQ0FBQTtLQUNqRTtTQUFNO1FBQ0wsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7S0FDNUI7SUFFRCxJQUFNLE9BQU8sR0FBRyxJQUFJLFVBQVUsQ0FBa0IsTUFBTSxDQUFDO1NBQ3BELE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7SUFFdEIsT0FBTyxJQUFJLFVBQVUsQ0FBZ0IsSUFBSSxDQUFDO1NBQ3ZDLFdBQVcsQ0FBQyxNQUFNLENBQUM7U0FDbkIsV0FBVyxDQUFDLE9BQU8sQ0FBQztTQUNwQixPQUFPLENBQUE7QUFDWixDQUFDO0FBRUQsTUFBTSxVQUFVLE9BQU8sQ0FBQyxHQUFXO0lBQ2pDLElBQU0sT0FBTyxHQUFHLEtBQUssQ0FBQTtJQUNyQixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDMUIsQ0FBQyIsImZpbGUiOiJtYWluL3NyYy9jaGFydHMvQ2hhcnRGdW5jdGlvbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRG9tRWxlbWVudCBmcm9tIFwiLi4vRG9tRWxlbWVudFwiXG5pbXBvcnQgeyB0ZXh0IH0gZnJvbSBcIi4uL0RvbUZ1bmN0aW9uc1wiXG5cbmV4cG9ydCBpbnRlcmZhY2UgQ2hhcnRMYWJlbCB7XG4gIHRpdGxlOiBzdHJpbmdcbiAgY29sb3I6IHN0cmluZ1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIENoYXJ0QXhpcyBleHRlbmRzIENoYXJ0TGFiZWwge1xuICB2YWx1ZTogbnVtYmVyXG59XG5cbmV4cG9ydCB0eXBlIENoYXJ0RGF0YSA9IENoYXJ0QXhpc1tdXG5cbmNvbnN0IFFVRVJZX0RBVEEgPSBcIi5qcy1kYXRhXCJcblxuZXhwb3J0IGZ1bmN0aW9uIHRyeUdldERhdGEoZWxlbWVudDogSFRNTEVsZW1lbnQpOiBDaGFydERhdGEge1xuICBsZXQgZGF0YSA9IFtdXG4gIGxldCBlbGVtZW50cyA9IGVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChRVUVSWV9EQVRBKVxuXG4gIGZvciAobGV0IGVudHJ5IG9mIGVsZW1lbnRzKSB7XG4gICAgbGV0IHZhbHVlID0gcGFyc2VGbG9hdChlbnRyeS5nZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIpISlcbiAgICBsZXQgY29sb3IgPSBlbnRyeS5nZXRBdHRyaWJ1dGUoXCJkYXRhLWNvbG9yXCIpIVxuICAgIGxldCB0aXRsZSA9IHRleHQoZW50cnkpXG5cbiAgICBsZXQgaXRlbSA9IHtcbiAgICAgIHRpdGxlLFxuICAgICAgdmFsdWUsXG4gICAgICBjb2xvclxuICAgIH1cblxuICAgIGRhdGEucHVzaChpdGVtKVxuICB9XG5cbiAgcmV0dXJuIGRhdGFcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZUFsbENoaWxkcmVuKG5vZGU6IE5vZGUpIHtcbiAgd2hpbGUgKG5vZGUuZmlyc3RDaGlsZCkge1xuICAgIG5vZGUucmVtb3ZlQ2hpbGQobm9kZS5maXJzdENoaWxkKVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVMZWdlbmRJdGVtKGRhdGE6IENoYXJ0TGFiZWwpIHtcbiAgY29uc3QgYnVsbGV0ID0gbmV3IERvbUVsZW1lbnQ8SFRNTFNwYW5FbGVtZW50PihcInNwYW5cIilcbiAgICAuYWRkQ2xhc3MoXCJidWxsZXRcIilcblxuICBpZiAoaXNDb2xvcihkYXRhLmNvbG9yKSA9PT0gdHJ1ZSkge1xuICAgIGJ1bGxldC5zZXRBdHRyaWJ1dGUoXCJzdHlsZVwiLCBgYmFja2dyb3VuZC1jb2xvcjogJHtkYXRhLmNvbG9yfTtgKVxuICB9IGVsc2Uge1xuICAgIGJ1bGxldC5hZGRDbGFzcyhkYXRhLmNvbG9yKVxuICB9XG5cbiAgY29uc3QgY2FwdGlvbiA9IG5ldyBEb21FbGVtZW50PEhUTUxTcGFuRWxlbWVudD4oXCJzcGFuXCIpXG4gICAgLnNldEh0bWwoZGF0YS50aXRsZSlcblxuICByZXR1cm4gbmV3IERvbUVsZW1lbnQ8SFRNTExJRWxlbWVudD4oXCJsaVwiKVxuICAgIC5hcHBlbmRDaGlsZChidWxsZXQpXG4gICAgLmFwcGVuZENoaWxkKGNhcHRpb24pXG4gICAgLmVsZW1lbnRcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzQ29sb3Ioc3RyOiBzdHJpbmcpIHtcbiAgY29uc3QgcGF0dGVybiA9IC9eIy9pXG4gIHJldHVybiBwYXR0ZXJuLnRlc3Qoc3RyKVxufVxuIl0sInNvdXJjZVJvb3QiOiIuLi8uLi8uLi8uLi8uLiJ9
