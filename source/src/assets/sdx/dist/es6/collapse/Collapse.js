import * as tslib_1 from "tslib";
import { preventDefault } from "../Utils";
import { TweenLite, Power1, Power4 } from "gsap";
import DomElement from "../DomElement";
import { addClass, hasClass, removeClass, isHidden } from "../DomFunctions";
var CLASS_OPEN = "is-open";
var ANIMATION_OPEN = 0.3;
/**
 * The Collapse component.
 */
var Collapse = /** @class */ (function (_super) {
    tslib_1.__extends(Collapse, _super);
    /**
     * Creates and initializes the Collapse component.
     * @param {DomElement} - The root element of the Collapse component.
     */
    function Collapse(element) {
        var _this = _super.call(this, element) || this;
        _this._clickHandler = _this._handleClick.bind(_this);
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the Collapse component.
     * @private
     */
    Collapse.prototype._initialize = function () {
        var dataTarget = this.element.getAttribute("data-target");
        if (dataTarget === null || dataTarget === "") {
            /* tslint:disable:no-console */
            console.error("A collapsible element requires a 'data-target' that specifies the element to collapse");
            console.info(this.element);
            /* tslint:enable:no-console */
            return;
        }
        var hiddenTarget = this.element.getAttribute("data-hidden");
        if (hiddenTarget !== null && hiddenTarget !== "") {
            this._hiddenIndicator = document.querySelector(hiddenTarget);
        }
        this._collapsibleElements = document.querySelectorAll(dataTarget);
        this.element.addEventListener("click", this._clickHandler);
    };
    Collapse.prototype._handleClick = function (event) {
        preventDefault(event);
        this.toggle();
    };
    /**
     * Toggles the collapseible.
     */
    Collapse.prototype.toggle = function () {
        var e_1, _a, e_2, _b;
        if (this._hiddenIndicator && isHidden(this._hiddenIndicator, false) === true) {
            return;
        }
        if (hasClass(this.element, CLASS_OPEN) === false) {
            addClass(this.element, CLASS_OPEN);
            try {
                for (var _c = tslib_1.__values(this._collapsibleElements), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var s = _d.value;
                    this._openCollapse(s);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            removeClass(this.element, CLASS_OPEN);
            try {
                for (var _e = tslib_1.__values(this._collapsibleElements), _f = _e.next(); !_f.done; _f = _e.next()) {
                    var s = _f.value;
                    this._closeCollapse(s);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
    };
    Collapse.prototype._openCollapse = function (el) {
        TweenLite.killTweensOf(el);
        TweenLite.set(el, {
            display: "block"
        });
        TweenLite.to(el, ANIMATION_OPEN, {
            className: "+=" + CLASS_OPEN,
            ease: [
                Power1.easeIn, Power4.easeOut
            ]
        });
        // set aria expanded
        el.setAttribute("aria-expanded", "true");
    };
    Collapse.prototype._closeCollapse = function (el) {
        TweenLite.killTweensOf(el);
        TweenLite.to(el, ANIMATION_OPEN, {
            className: "-=" + CLASS_OPEN,
            ease: [
                Power1.easeIn, Power4.easeOut
            ],
            onComplete: function () {
                TweenLite.set(el, {
                    clearProps: "display"
                });
            }
        });
        // set aria expanded
        el.setAttribute("aria-expanded", "false");
    };
    /**
     * Removes all event handlers and clears references.
     */
    Collapse.prototype.destroy = function () {
        this._collapsibleElements = null;
        if (this._clickHandler) {
            this.element.removeEventListener("click", this._clickHandler);
        }
        this.element = null;
    };
    return Collapse;
}(DomElement));
export function init() {
    var e_3, _a;
    var elements = document.querySelectorAll("[data-toggle='collapse']");
    try {
        for (var elements_1 = tslib_1.__values(elements), elements_1_1 = elements_1.next(); !elements_1_1.done; elements_1_1 = elements_1.next()) {
            var e = elements_1_1.value;
            if (e.getAttribute("data-init") === "auto") {
                new Collapse(e);
            }
        }
    }
    catch (e_3_1) { e_3 = { error: e_3_1 }; }
    finally {
        try {
            if (elements_1_1 && !elements_1_1.done && (_a = elements_1.return)) _a.call(elements_1);
        }
        finally { if (e_3) throw e_3.error; }
    }
}
export default Collapse;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2NvbGxhcHNlL0NvbGxhcHNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sVUFBVSxDQUFBO0FBQ3pDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxNQUFNLE1BQU0sQ0FBQTtBQUNoRCxPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBRTNFLElBQU0sVUFBVSxHQUFHLFNBQVMsQ0FBQTtBQUU1QixJQUFNLGNBQWMsR0FBRyxHQUFHLENBQUE7QUFFMUI7O0dBRUc7QUFDSDtJQUF1QixvQ0FBVTtJQU0vQjs7O09BR0c7SUFDSCxrQkFBWSxPQUFvQjtRQUFoQyxZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1FBRkMsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUNqRCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7O0lBQ3BCLENBQUM7SUFFRDs7O09BR0c7SUFDTyw4QkFBVyxHQUFyQjtRQUNFLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQ3pELElBQUksVUFBVSxLQUFLLElBQUksSUFBSSxVQUFVLEtBQUssRUFBRSxFQUFFO1lBRTVDLCtCQUErQjtZQUMvQixPQUFPLENBQUMsS0FBSyxDQUFDLHVGQUF1RixDQUFDLENBQUE7WUFDdEcsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDMUIsOEJBQThCO1lBRTlCLE9BQU07U0FDUDtRQUVELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzNELElBQUksWUFBWSxLQUFLLElBQUksSUFBSSxZQUFZLEtBQUssRUFBRSxFQUFFO1lBQ2hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBaUIsQ0FBQTtTQUM3RTtRQUVELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDakUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0lBQzVELENBQUM7SUFFUywrQkFBWSxHQUF0QixVQUF1QixLQUFpQjtRQUN0QyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDckIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQ2YsQ0FBQztJQUVEOztPQUVHO0lBQ0gseUJBQU0sR0FBTjs7UUFDRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxLQUFLLElBQUksRUFBRTtZQUM1RSxPQUFNO1NBQ1A7UUFFRCxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUNoRCxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQTs7Z0JBRWxDLEtBQWMsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBcEMsSUFBSSxDQUFDLFdBQUE7b0JBQ1IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtpQkFDdEI7Ozs7Ozs7OztTQUNGO2FBQU07WUFDTCxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQTs7Z0JBRXJDLEtBQWMsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBcEMsSUFBSSxDQUFDLFdBQUE7b0JBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtpQkFDdkI7Ozs7Ozs7OztTQUNGO0lBQ0gsQ0FBQztJQUVTLGdDQUFhLEdBQXZCLFVBQXdCLEVBQWU7UUFDckMsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUUxQixTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRTtZQUNoQixPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUE7UUFFRixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxjQUFjLEVBQUU7WUFDL0IsU0FBUyxFQUFFLE9BQUssVUFBWTtZQUM1QixJQUFJLEVBQUU7Z0JBQ0osTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsT0FBTzthQUM5QjtTQUNGLENBQUMsQ0FBQTtRQUVGLG9CQUFvQjtRQUNwQixFQUFFLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxNQUFNLENBQUMsQ0FBQTtJQUMxQyxDQUFDO0lBRVMsaUNBQWMsR0FBeEIsVUFBeUIsRUFBZTtRQUN0QyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1FBRTFCLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLGNBQWMsRUFBRTtZQUMvQixTQUFTLEVBQUUsT0FBSyxVQUFZO1lBQzVCLElBQUksRUFBRTtnQkFDSixNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxPQUFPO2FBQzlCO1lBQ0QsVUFBVSxFQUFFO2dCQUNWLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFO29CQUNoQixVQUFVLEVBQUUsU0FBUztpQkFDdEIsQ0FBQyxDQUFBO1lBQ0osQ0FBQztTQUNGLENBQUMsQ0FBQTtRQUVGLG9CQUFvQjtRQUNwQixFQUFFLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsQ0FBQTtJQUMzQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCwwQkFBTyxHQUFQO1FBQ0csSUFBWSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQTtRQUV6QyxJQUFLLElBQVksQ0FBQyxhQUFhLEVBQUU7WUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQzlEO1FBRUEsSUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7SUFDOUIsQ0FBQztJQUNILGVBQUM7QUFBRCxDQXZIQSxBQXVIQyxDQXZIc0IsVUFBVSxHQXVIaEM7QUFFRCxNQUFNLFVBQVUsSUFBSTs7SUFDbEIsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDLDBCQUEwQixDQUFDLENBQUE7O1FBQ3BFLEtBQWMsSUFBQSxhQUFBLGlCQUFBLFFBQVEsQ0FBQSxrQ0FBQSx3REFBRTtZQUFuQixJQUFJLENBQUMscUJBQUE7WUFDUixJQUFJLENBQUMsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLEtBQUssTUFBTSxFQUFFO2dCQUMxQyxJQUFJLFFBQVEsQ0FBQyxDQUFnQixDQUFDLENBQUE7YUFDL0I7U0FDRjs7Ozs7Ozs7O0FBQ0gsQ0FBQztBQUVELGVBQWUsUUFBUSxDQUFBIiwiZmlsZSI6Im1haW4vc3JjL2NvbGxhcHNlL0NvbGxhcHNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgcHJldmVudERlZmF1bHQgfSBmcm9tIFwiLi4vVXRpbHNcIlxuaW1wb3J0IHsgVHdlZW5MaXRlLCBQb3dlcjEsIFBvd2VyNCB9IGZyb20gXCJnc2FwXCJcbmltcG9ydCBEb21FbGVtZW50IGZyb20gXCIuLi9Eb21FbGVtZW50XCJcbmltcG9ydCB7IGFkZENsYXNzLCBoYXNDbGFzcywgcmVtb3ZlQ2xhc3MsIGlzSGlkZGVuIH0gZnJvbSBcIi4uL0RvbUZ1bmN0aW9uc1wiXG5cbmNvbnN0IENMQVNTX09QRU4gPSBcImlzLW9wZW5cIlxuXG5jb25zdCBBTklNQVRJT05fT1BFTiA9IDAuM1xuXG4vKipcbiAqIFRoZSBDb2xsYXBzZSBjb21wb25lbnQuXG4gKi9cbmNsYXNzIENvbGxhcHNlIGV4dGVuZHMgRG9tRWxlbWVudCB7XG4gIHByaXZhdGUgX2hpZGRlbkluZGljYXRvciE6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX2NvbGxhcHNpYmxlRWxlbWVudHMhOiBOb2RlTGlzdE9mPEhUTUxFbGVtZW50PlxuXG4gIHByaXZhdGUgX2NsaWNrSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYW5kIGluaXRpYWxpemVzIHRoZSBDb2xsYXBzZSBjb21wb25lbnQuXG4gICAqIEBwYXJhbSB7RG9tRWxlbWVudH0gLSBUaGUgcm9vdCBlbGVtZW50IG9mIHRoZSBDb2xsYXBzZSBjb21wb25lbnQuXG4gICAqL1xuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBIVE1MRWxlbWVudCkge1xuICAgIHN1cGVyKGVsZW1lbnQpXG5cbiAgICB0aGlzLl9jbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVDbGljay5iaW5kKHRoaXMpXG4gICAgdGhpcy5faW5pdGlhbGl6ZSgpXG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZXMgdGhlIENvbGxhcHNlIGNvbXBvbmVudC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICBsZXQgZGF0YVRhcmdldCA9IHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKVxuICAgIGlmIChkYXRhVGFyZ2V0ID09PSBudWxsIHx8IGRhdGFUYXJnZXQgPT09IFwiXCIpIHtcblxuICAgICAgLyogdHNsaW50OmRpc2FibGU6bm8tY29uc29sZSAqL1xuICAgICAgY29uc29sZS5lcnJvcihcIkEgY29sbGFwc2libGUgZWxlbWVudCByZXF1aXJlcyBhICdkYXRhLXRhcmdldCcgdGhhdCBzcGVjaWZpZXMgdGhlIGVsZW1lbnQgdG8gY29sbGFwc2VcIilcbiAgICAgIGNvbnNvbGUuaW5mbyh0aGlzLmVsZW1lbnQpXG4gICAgICAvKiB0c2xpbnQ6ZW5hYmxlOm5vLWNvbnNvbGUgKi9cblxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgbGV0IGhpZGRlblRhcmdldCA9IHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJkYXRhLWhpZGRlblwiKVxuICAgIGlmIChoaWRkZW5UYXJnZXQgIT09IG51bGwgJiYgaGlkZGVuVGFyZ2V0ICE9PSBcIlwiKSB7XG4gICAgICB0aGlzLl9oaWRkZW5JbmRpY2F0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGhpZGRlblRhcmdldCkhIGFzIEhUTUxFbGVtZW50XG4gICAgfVxuXG4gICAgdGhpcy5fY29sbGFwc2libGVFbGVtZW50cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZGF0YVRhcmdldClcbiAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2NsaWNrSGFuZGxlcilcbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlQ2xpY2soZXZlbnQ6IE1vdXNlRXZlbnQpIHtcbiAgICBwcmV2ZW50RGVmYXVsdChldmVudClcbiAgICB0aGlzLnRvZ2dsZSgpXG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlcyB0aGUgY29sbGFwc2VpYmxlLlxuICAgKi9cbiAgdG9nZ2xlKCkge1xuICAgIGlmICh0aGlzLl9oaWRkZW5JbmRpY2F0b3IgJiYgaXNIaWRkZW4odGhpcy5faGlkZGVuSW5kaWNhdG9yLCBmYWxzZSkgPT09IHRydWUpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChoYXNDbGFzcyh0aGlzLmVsZW1lbnQsIENMQVNTX09QRU4pID09PSBmYWxzZSkge1xuICAgICAgYWRkQ2xhc3ModGhpcy5lbGVtZW50LCBDTEFTU19PUEVOKVxuXG4gICAgICBmb3IgKGxldCBzIG9mIHRoaXMuX2NvbGxhcHNpYmxlRWxlbWVudHMpIHtcbiAgICAgICAgdGhpcy5fb3BlbkNvbGxhcHNlKHMpXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlbW92ZUNsYXNzKHRoaXMuZWxlbWVudCwgQ0xBU1NfT1BFTilcblxuICAgICAgZm9yIChsZXQgcyBvZiB0aGlzLl9jb2xsYXBzaWJsZUVsZW1lbnRzKSB7XG4gICAgICAgIHRoaXMuX2Nsb3NlQ29sbGFwc2UocylcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX29wZW5Db2xsYXBzZShlbDogSFRNTEVsZW1lbnQpIHtcbiAgICBUd2VlbkxpdGUua2lsbFR3ZWVuc09mKGVsKVxuXG4gICAgVHdlZW5MaXRlLnNldChlbCwge1xuICAgICAgZGlzcGxheTogXCJibG9ja1wiXG4gICAgfSlcblxuICAgIFR3ZWVuTGl0ZS50byhlbCwgQU5JTUFUSU9OX09QRU4sIHtcbiAgICAgIGNsYXNzTmFtZTogYCs9JHtDTEFTU19PUEVOfWAsXG4gICAgICBlYXNlOiBbXG4gICAgICAgIFBvd2VyMS5lYXNlSW4sIFBvd2VyNC5lYXNlT3V0XG4gICAgICBdXG4gICAgfSlcblxuICAgIC8vIHNldCBhcmlhIGV4cGFuZGVkXG4gICAgZWwuc2V0QXR0cmlidXRlKFwiYXJpYS1leHBhbmRlZFwiLCBcInRydWVcIilcbiAgfVxuXG4gIHByb3RlY3RlZCBfY2xvc2VDb2xsYXBzZShlbDogSFRNTEVsZW1lbnQpIHtcbiAgICBUd2VlbkxpdGUua2lsbFR3ZWVuc09mKGVsKVxuXG4gICAgVHdlZW5MaXRlLnRvKGVsLCBBTklNQVRJT05fT1BFTiwge1xuICAgICAgY2xhc3NOYW1lOiBgLT0ke0NMQVNTX09QRU59YCxcbiAgICAgIGVhc2U6IFtcbiAgICAgICAgUG93ZXIxLmVhc2VJbiwgUG93ZXI0LmVhc2VPdXRcbiAgICAgIF0sXG4gICAgICBvbkNvbXBsZXRlOiAoKSA9PiB7XG4gICAgICAgIFR3ZWVuTGl0ZS5zZXQoZWwsIHtcbiAgICAgICAgICBjbGVhclByb3BzOiBcImRpc3BsYXlcIlxuICAgICAgICB9KVxuICAgICAgfVxuICAgIH0pXG5cbiAgICAvLyBzZXQgYXJpYSBleHBhbmRlZFxuICAgIGVsLnNldEF0dHJpYnV0ZShcImFyaWEtZXhwYW5kZWRcIiwgXCJmYWxzZVwiKVxuICB9XG5cbiAgLyoqXG4gICAqIFJlbW92ZXMgYWxsIGV2ZW50IGhhbmRsZXJzIGFuZCBjbGVhcnMgcmVmZXJlbmNlcy5cbiAgICovXG4gIGRlc3Ryb3koKSB7XG4gICAgKHRoaXMgYXMgYW55KS5fY29sbGFwc2libGVFbGVtZW50cyA9IG51bGxcblxuICAgIGlmICgodGhpcyBhcyBhbnkpLl9jbGlja0hhbmRsZXIpIHtcbiAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2xpY2tIYW5kbGVyKVxuICAgIH1cblxuICAgICh0aGlzIGFzIGFueSkuZWxlbWVudCA9IG51bGxcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgbGV0IGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltkYXRhLXRvZ2dsZT0nY29sbGFwc2UnXVwiKVxuICBmb3IgKGxldCBlIG9mIGVsZW1lbnRzKSB7XG4gICAgaWYgKGUuZ2V0QXR0cmlidXRlKFwiZGF0YS1pbml0XCIpID09PSBcImF1dG9cIikge1xuICAgICAgbmV3IENvbGxhcHNlKGUgYXMgSFRNTEVsZW1lbnQpXG4gICAgfVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENvbGxhcHNlXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
