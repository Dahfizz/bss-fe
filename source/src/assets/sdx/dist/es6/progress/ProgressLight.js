import * as tslib_1 from "tslib";
import { TweenLite, Power4 } from "gsap";
import { searchAndInitialize, clamp } from "../Utils";
import DomElement from "../DomElement";
var CLASS_BAR = ".progress-light__bar";
var CLASS_PROGRESS = ".bar__progress";
var CLASS_PROGRESS_COMPLETED = "bar__progress--complete";
var CLASS_TICK = "bar__tick";
var CLASS_PAGE_CURRENT = ".detail__currentpage";
var CLASS_PAGE_TOTAL = ".detail__totalpage";
var CLASS_DISABLED = "arrow--disabled";
var CLASS_BUTTON_LEFT = ".arrow--left";
var CLASS_BUTTON_RIGHT = ".arrow--right";
/**
 * Light progress bar component
 */
var ProgressLight = /** @class */ (function (_super) {
    tslib_1.__extends(ProgressLight, _super);
    /**
     * Creates and initializes the ProgressLight component.
     * @param {DomElement} - The root element of the ProgressLight component.
     */
    function ProgressLight(element) {
        var _this = _super.call(this, element) || this;
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the loader bar component.
     * @private
     */
    ProgressLight.prototype._initialize = function () {
        this._buttonClickHandler = this._handleButtonClick.bind(this);
        this._animationCompletedHandler = this._handleAnimationCompleted.bind(this);
        this._barElement = this.find(CLASS_BAR);
        this._progressElement = this.find(CLASS_PROGRESS);
        this._pageCurrentElement = this.find(CLASS_PAGE_CURRENT);
        this._pageTotalElement = this.find(CLASS_PAGE_TOTAL);
        this._buttonLeft = this.find(CLASS_BUTTON_LEFT);
        this._buttonRight = this.find(CLASS_BUTTON_RIGHT);
        this._minValue = 1;
        this._total = Math.max(parseInt(this.getAttribute("total") || "100", 10), this._minValue);
        this._value = clamp(parseInt(this.getAttribute("value") || "1", 10), this._minValue, this._total);
        this._layout();
        this._addTicks();
        this._update(false);
        this.enable();
    };
    ProgressLight.prototype._addTicks = function () {
        for (var i = 1; i < this._total; i++) {
            var position = this._itemWidth * i;
            var tickElement = new DomElement("div")
                .addClass(CLASS_TICK)
                .setAttribute("style", "left: " + position + "%");
            this._barElement.prependChild(tickElement);
        }
    };
    ProgressLight.prototype._update = function (animate) {
        if (animate === void 0) { animate = true; }
        this._pageCurrentElement.setHtml(this._value.toString());
        this._pageTotalElement.setHtml(this._total.toString());
        var position = this._value * this._itemWidth;
        // Add additional width to the last element to make sure
        // the rounded border on the left is filled as well
        if (this._value === this._total) {
            position += 5;
        }
        if (this._value >= this._total) {
            this._buttonRight.addClass(CLASS_DISABLED);
        }
        else {
            this._buttonRight.removeClass(CLASS_DISABLED);
        }
        if (this._value <= this._minValue) {
            this._buttonLeft.addClass(CLASS_DISABLED);
        }
        else {
            this._buttonLeft.removeClass(CLASS_DISABLED);
        }
        if (animate) {
            TweenLite.to(this._progressElement.element, 0.2, {
                width: position + "%",
                ease: Power4.easeInOut,
                onComplete: this._animationCompletedHandler
            });
        }
        else {
            TweenLite.set(this._progressElement.element, {
                width: position + "%",
                onComplete: this._animationCompletedHandler
            });
        }
    };
    ProgressLight.prototype._layout = function () {
        this._itemWidth = Math.floor(100 / this._total);
    };
    ProgressLight.prototype._handleButtonClick = function (event) {
        if (event.target === this._buttonLeft.element) {
            this.value = this._value - 1;
        }
        else if (event.target === this._buttonRight.element) {
            this.value = this._value + 1;
        }
    };
    ProgressLight.prototype._handleAnimationCompleted = function () {
        if (this._value === this._total) {
            this._progressElement.addClass(CLASS_PROGRESS_COMPLETED);
        }
        else {
            this._progressElement.removeClass(CLASS_PROGRESS_COMPLETED);
        }
    };
    Object.defineProperty(ProgressLight.prototype, "value", {
        /**
         * Gets the current progress value in the range of 1..total.
         */
        get: function () {
            return this._value;
        },
        /**
         * Sets the current progress.
         * @param {number} - The progress in the range of 1..total.
         */
        set: function (val) {
            this._value = clamp(val, this._minValue, this._total);
            this._update(true);
            this.dispatchEvent("changed");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProgressLight.prototype, "total", {
        /**
         * Gets the total progress value.
         */
        get: function () {
            return this._total;
        },
        /**
         * Sets the total progress value and updates the UI accordingly.
         * @param {number} - The total progress positive integer value.
         */
        set: function (value) {
            var e_1, _a;
            if (this._total === value) {
                return;
            }
            this._total = Math.max(value, this._minValue);
            this._value = clamp(this._value, this._minValue, this._total);
            try {
                // Clear the ticks
                for (var _b = tslib_1.__values(this.element.querySelectorAll("." + CLASS_TICK)), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var tick = _c.value;
                    this._barElement.element.removeChild(tick);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            this._layout();
            this._addTicks();
            this._update(false);
            this.dispatchEvent("totalchanged");
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Enables the component.
     */
    ProgressLight.prototype.enable = function () {
        this._buttonLeft.element.addEventListener("click", this._buttonClickHandler);
        this._buttonRight.element.addEventListener("click", this._buttonClickHandler);
    };
    /**
     * Disables the component.
     */
    ProgressLight.prototype.disable = function () {
        this._buttonLeft.element.removeEventListener("click", this._buttonClickHandler);
        this._buttonRight.element.removeEventListener("click", this._buttonClickHandler);
    };
    return ProgressLight;
}(DomElement));
export function init() {
    searchAndInitialize(".progress-light", function (e) {
        new ProgressLight(e);
    });
}
export default ProgressLight;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL3Byb2dyZXNzL1Byb2dyZXNzTGlnaHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sTUFBTSxDQUFBO0FBQ3hDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxLQUFLLEVBQUUsTUFBTSxVQUFVLENBQUE7QUFDckQsT0FBTyxVQUFVLE1BQU0sZUFBZSxDQUFBO0FBRXRDLElBQU0sU0FBUyxHQUFHLHNCQUFzQixDQUFBO0FBQ3hDLElBQU0sY0FBYyxHQUFHLGdCQUFnQixDQUFBO0FBQ3ZDLElBQU0sd0JBQXdCLEdBQUcseUJBQXlCLENBQUE7QUFDMUQsSUFBTSxVQUFVLEdBQUcsV0FBVyxDQUFBO0FBQzlCLElBQU0sa0JBQWtCLEdBQUcsc0JBQXNCLENBQUE7QUFDakQsSUFBTSxnQkFBZ0IsR0FBRyxvQkFBb0IsQ0FBQTtBQUU3QyxJQUFNLGNBQWMsR0FBRyxpQkFBaUIsQ0FBQTtBQUN4QyxJQUFNLGlCQUFpQixHQUFHLGNBQWMsQ0FBQTtBQUN4QyxJQUFNLGtCQUFrQixHQUFHLGVBQWUsQ0FBQTtBQUUxQzs7R0FFRztBQUNIO0lBQTRCLHlDQUFVO0lBaUJwQzs7O09BR0c7SUFDSCx1QkFBWSxPQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUVmO1FBREMsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFBOztJQUNwQixDQUFDO0lBRUQ7OztPQUdHO0lBQ08sbUNBQVcsR0FBckI7UUFFRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUM3RCxJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUUzRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFFLENBQUE7UUFDeEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFFLENBQUE7UUFDbEQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUUsQ0FBQTtRQUN6RCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBRSxDQUFBO1FBQ3JELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBRSxDQUFBO1FBQ2hELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBRSxDQUFBO1FBRWxELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3pGLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUVqRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUE7UUFFZCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7UUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUVuQixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7SUFDZixDQUFDO0lBRVMsaUNBQVMsR0FBbkI7UUFDRSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwQyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVyxHQUFHLENBQUMsQ0FBQTtZQUVyQyxJQUFJLFdBQVcsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7aUJBQ3BDLFFBQVEsQ0FBQyxVQUFVLENBQUM7aUJBQ3BCLFlBQVksQ0FBQyxPQUFPLEVBQUUsV0FBUyxRQUFRLE1BQUcsQ0FBQyxDQUFBO1lBRTlDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1NBQzNDO0lBQ0gsQ0FBQztJQUVTLCtCQUFPLEdBQWpCLFVBQWtCLE9BQWM7UUFBZCx3QkFBQSxFQUFBLGNBQWM7UUFDOUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUE7UUFDeEQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUE7UUFFdEQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVyxDQUFBO1FBRTdDLHdEQUF3RDtRQUN4RCxtREFBbUQ7UUFDbkQsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDL0IsUUFBUSxJQUFJLENBQUMsQ0FBQTtTQUNkO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUE7U0FDM0M7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFBO1NBQzlDO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUE7U0FDMUM7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFBO1NBQzdDO1FBRUQsSUFBSSxPQUFPLEVBQUU7WUFDWCxTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFO2dCQUMvQyxLQUFLLEVBQUssUUFBUSxNQUFHO2dCQUNyQixJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVM7Z0JBQ3RCLFVBQVUsRUFBRSxJQUFJLENBQUMsMEJBQTBCO2FBQzVDLENBQUMsQ0FBQTtTQUNIO2FBQU07WUFDTCxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7Z0JBQzNDLEtBQUssRUFBSyxRQUFRLE1BQUc7Z0JBQ3JCLFVBQVUsRUFBRSxJQUFJLENBQUMsMEJBQTBCO2FBQzVDLENBQUMsQ0FBQTtTQUNIO0lBQ0gsQ0FBQztJQUVTLCtCQUFPLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7SUFDakQsQ0FBQztJQUVTLDBDQUFrQixHQUE1QixVQUE2QixLQUFpQjtRQUM1QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDN0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQTtTQUM3QjthQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRTtZQUNyRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBO1NBQzdCO0lBQ0gsQ0FBQztJQUVTLGlEQUF5QixHQUFuQztRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQy9CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsd0JBQXdCLENBQUMsQ0FBQTtTQUN6RDthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO1NBQzVEO0lBQ0gsQ0FBQztJQUtELHNCQUFJLGdDQUFLO1FBSFQ7O1dBRUc7YUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQTtRQUNwQixDQUFDO1FBRUQ7OztXQUdHO2FBQ0gsVUFBVSxHQUFHO1lBQ1gsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ3JELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7WUFFbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUMvQixDQUFDOzs7T0FYQTtJQWdCRCxzQkFBSSxnQ0FBSztRQUhUOztXQUVHO2FBQ0g7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUE7UUFDcEIsQ0FBQztRQUVEOzs7V0FHRzthQUNILFVBQVUsS0FBSzs7WUFDYixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO2dCQUN6QixPQUFNO2FBQ1A7WUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtZQUM3QyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBOztnQkFFN0Qsa0JBQWtCO2dCQUNsQixLQUFpQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFJLFVBQVksQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO29CQUE3RCxJQUFJLElBQUksV0FBQTtvQkFDWCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUE7aUJBQzNDOzs7Ozs7Ozs7WUFFRCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUE7WUFDZCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7WUFFaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUVuQixJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3BDLENBQUM7OztPQXpCQTtJQTJCRDs7T0FFRztJQUNILDhCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFDNUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO0lBQy9FLENBQUM7SUFFRDs7T0FFRztJQUNILCtCQUFPLEdBQVA7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFDL0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO0lBQ2xGLENBQUM7SUFDSCxvQkFBQztBQUFELENBN0xBLEFBNkxDLENBN0wyQixVQUFVLEdBNkxyQztBQUVELE1BQU0sVUFBVSxJQUFJO0lBQ2xCLG1CQUFtQixDQUFDLGlCQUFpQixFQUFFLFVBQUMsQ0FBQztRQUN2QyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUN0QixDQUFDLENBQUMsQ0FBQTtBQUNKLENBQUM7QUFFRCxlQUFlLGFBQWEsQ0FBQSIsImZpbGUiOiJtYWluL3NyYy9wcm9ncmVzcy9Qcm9ncmVzc0xpZ2h0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVHdlZW5MaXRlLCBQb3dlcjQgfSBmcm9tIFwiZ3NhcFwiXG5pbXBvcnQgeyBzZWFyY2hBbmRJbml0aWFsaXplLCBjbGFtcCB9IGZyb20gXCIuLi9VdGlsc1wiXG5pbXBvcnQgRG9tRWxlbWVudCBmcm9tIFwiLi4vRG9tRWxlbWVudFwiXG5cbmNvbnN0IENMQVNTX0JBUiA9IFwiLnByb2dyZXNzLWxpZ2h0X19iYXJcIlxuY29uc3QgQ0xBU1NfUFJPR1JFU1MgPSBcIi5iYXJfX3Byb2dyZXNzXCJcbmNvbnN0IENMQVNTX1BST0dSRVNTX0NPTVBMRVRFRCA9IFwiYmFyX19wcm9ncmVzcy0tY29tcGxldGVcIlxuY29uc3QgQ0xBU1NfVElDSyA9IFwiYmFyX190aWNrXCJcbmNvbnN0IENMQVNTX1BBR0VfQ1VSUkVOVCA9IFwiLmRldGFpbF9fY3VycmVudHBhZ2VcIlxuY29uc3QgQ0xBU1NfUEFHRV9UT1RBTCA9IFwiLmRldGFpbF9fdG90YWxwYWdlXCJcblxuY29uc3QgQ0xBU1NfRElTQUJMRUQgPSBcImFycm93LS1kaXNhYmxlZFwiXG5jb25zdCBDTEFTU19CVVRUT05fTEVGVCA9IFwiLmFycm93LS1sZWZ0XCJcbmNvbnN0IENMQVNTX0JVVFRPTl9SSUdIVCA9IFwiLmFycm93LS1yaWdodFwiXG5cbi8qKlxuICogTGlnaHQgcHJvZ3Jlc3MgYmFyIGNvbXBvbmVudFxuICovXG5jbGFzcyBQcm9ncmVzc0xpZ2h0IGV4dGVuZHMgRG9tRWxlbWVudCB7XG4gIHByaXZhdGUgX2J1dHRvbkNsaWNrSGFuZGxlciE6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfYW5pbWF0aW9uQ29tcGxldGVkSGFuZGxlciE6IChldmVudDogRXZlbnQpID0+IHZvaWRcblxuICBwcml2YXRlIF9iYXJFbGVtZW50ITogRG9tRWxlbWVudDxFbGVtZW50PlxuICBwcml2YXRlIF9wcm9ncmVzc0VsZW1lbnQhOiBEb21FbGVtZW50PEVsZW1lbnQ+XG4gIHByaXZhdGUgX3BhZ2VDdXJyZW50RWxlbWVudCE6IERvbUVsZW1lbnQ8RWxlbWVudD5cbiAgcHJpdmF0ZSBfcGFnZVRvdGFsRWxlbWVudCE6IERvbUVsZW1lbnQ8RWxlbWVudD5cbiAgcHJpdmF0ZSBfYnV0dG9uTGVmdCE6IERvbUVsZW1lbnQ8RWxlbWVudD5cbiAgcHJpdmF0ZSBfYnV0dG9uUmlnaHQhOiBEb21FbGVtZW50PEVsZW1lbnQ+XG5cbiAgcHJpdmF0ZSBfbWluVmFsdWUhOiBudW1iZXJcbiAgcHJpdmF0ZSBfdG90YWwhOiBudW1iZXJcbiAgcHJpdmF0ZSBfdmFsdWUhOiBudW1iZXJcblxuICBwcml2YXRlIF9pdGVtV2lkdGg/OiBudW1iZXJcblxuICAvKipcbiAgICogQ3JlYXRlcyBhbmQgaW5pdGlhbGl6ZXMgdGhlIFByb2dyZXNzTGlnaHQgY29tcG9uZW50LlxuICAgKiBAcGFyYW0ge0RvbUVsZW1lbnR9IC0gVGhlIHJvb3QgZWxlbWVudCBvZiB0aGUgUHJvZ3Jlc3NMaWdodCBjb21wb25lbnQuXG4gICAqL1xuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBFbGVtZW50KSB7XG4gICAgc3VwZXIoZWxlbWVudClcbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgbG9hZGVyIGJhciBjb21wb25lbnQuXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBwcm90ZWN0ZWQgX2luaXRpYWxpemUoKSB7XG5cbiAgICB0aGlzLl9idXR0b25DbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVCdXR0b25DbGljay5iaW5kKHRoaXMpXG4gICAgdGhpcy5fYW5pbWF0aW9uQ29tcGxldGVkSGFuZGxlciA9IHRoaXMuX2hhbmRsZUFuaW1hdGlvbkNvbXBsZXRlZC5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9iYXJFbGVtZW50ID0gdGhpcy5maW5kKENMQVNTX0JBUikhXG4gICAgdGhpcy5fcHJvZ3Jlc3NFbGVtZW50ID0gdGhpcy5maW5kKENMQVNTX1BST0dSRVNTKSFcbiAgICB0aGlzLl9wYWdlQ3VycmVudEVsZW1lbnQgPSB0aGlzLmZpbmQoQ0xBU1NfUEFHRV9DVVJSRU5UKSFcbiAgICB0aGlzLl9wYWdlVG90YWxFbGVtZW50ID0gdGhpcy5maW5kKENMQVNTX1BBR0VfVE9UQUwpIVxuICAgIHRoaXMuX2J1dHRvbkxlZnQgPSB0aGlzLmZpbmQoQ0xBU1NfQlVUVE9OX0xFRlQpIVxuICAgIHRoaXMuX2J1dHRvblJpZ2h0ID0gdGhpcy5maW5kKENMQVNTX0JVVFRPTl9SSUdIVCkhXG5cbiAgICB0aGlzLl9taW5WYWx1ZSA9IDFcbiAgICB0aGlzLl90b3RhbCA9IE1hdGgubWF4KHBhcnNlSW50KHRoaXMuZ2V0QXR0cmlidXRlKFwidG90YWxcIikgfHwgXCIxMDBcIiwgMTApLCB0aGlzLl9taW5WYWx1ZSlcbiAgICB0aGlzLl92YWx1ZSA9IGNsYW1wKHBhcnNlSW50KHRoaXMuZ2V0QXR0cmlidXRlKFwidmFsdWVcIikgfHwgXCIxXCIsIDEwKSwgdGhpcy5fbWluVmFsdWUsIHRoaXMuX3RvdGFsKVxuXG4gICAgdGhpcy5fbGF5b3V0KClcblxuICAgIHRoaXMuX2FkZFRpY2tzKClcbiAgICB0aGlzLl91cGRhdGUoZmFsc2UpXG5cbiAgICB0aGlzLmVuYWJsZSgpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2FkZFRpY2tzKCkge1xuICAgIGZvciAobGV0IGkgPSAxOyBpIDwgdGhpcy5fdG90YWw7IGkrKykge1xuICAgICAgY29uc3QgcG9zaXRpb24gPSB0aGlzLl9pdGVtV2lkdGghICogaVxuXG4gICAgICBsZXQgdGlja0VsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfVElDSylcbiAgICAgICAgLnNldEF0dHJpYnV0ZShcInN0eWxlXCIsIGBsZWZ0OiAke3Bvc2l0aW9ufSVgKVxuXG4gICAgICB0aGlzLl9iYXJFbGVtZW50LnByZXBlbmRDaGlsZCh0aWNrRWxlbWVudClcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX3VwZGF0ZShhbmltYXRlID0gdHJ1ZSkge1xuICAgIHRoaXMuX3BhZ2VDdXJyZW50RWxlbWVudC5zZXRIdG1sKHRoaXMuX3ZhbHVlLnRvU3RyaW5nKCkpXG4gICAgdGhpcy5fcGFnZVRvdGFsRWxlbWVudC5zZXRIdG1sKHRoaXMuX3RvdGFsLnRvU3RyaW5nKCkpXG5cbiAgICBsZXQgcG9zaXRpb24gPSB0aGlzLl92YWx1ZSAqIHRoaXMuX2l0ZW1XaWR0aCFcblxuICAgIC8vIEFkZCBhZGRpdGlvbmFsIHdpZHRoIHRvIHRoZSBsYXN0IGVsZW1lbnQgdG8gbWFrZSBzdXJlXG4gICAgLy8gdGhlIHJvdW5kZWQgYm9yZGVyIG9uIHRoZSBsZWZ0IGlzIGZpbGxlZCBhcyB3ZWxsXG4gICAgaWYgKHRoaXMuX3ZhbHVlID09PSB0aGlzLl90b3RhbCkge1xuICAgICAgcG9zaXRpb24gKz0gNVxuICAgIH1cblxuICAgIGlmICh0aGlzLl92YWx1ZSA+PSB0aGlzLl90b3RhbCkge1xuICAgICAgdGhpcy5fYnV0dG9uUmlnaHQuYWRkQ2xhc3MoQ0xBU1NfRElTQUJMRUQpXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuX2J1dHRvblJpZ2h0LnJlbW92ZUNsYXNzKENMQVNTX0RJU0FCTEVEKVxuICAgIH1cblxuICAgIGlmICh0aGlzLl92YWx1ZSA8PSB0aGlzLl9taW5WYWx1ZSkge1xuICAgICAgdGhpcy5fYnV0dG9uTGVmdC5hZGRDbGFzcyhDTEFTU19ESVNBQkxFRClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5fYnV0dG9uTGVmdC5yZW1vdmVDbGFzcyhDTEFTU19ESVNBQkxFRClcbiAgICB9XG5cbiAgICBpZiAoYW5pbWF0ZSkge1xuICAgICAgVHdlZW5MaXRlLnRvKHRoaXMuX3Byb2dyZXNzRWxlbWVudC5lbGVtZW50LCAwLjIsIHtcbiAgICAgICAgd2lkdGg6IGAke3Bvc2l0aW9ufSVgLFxuICAgICAgICBlYXNlOiBQb3dlcjQuZWFzZUluT3V0LFxuICAgICAgICBvbkNvbXBsZXRlOiB0aGlzLl9hbmltYXRpb25Db21wbGV0ZWRIYW5kbGVyXG4gICAgICB9KVxuICAgIH0gZWxzZSB7XG4gICAgICBUd2VlbkxpdGUuc2V0KHRoaXMuX3Byb2dyZXNzRWxlbWVudC5lbGVtZW50LCB7XG4gICAgICAgIHdpZHRoOiBgJHtwb3NpdGlvbn0lYCxcbiAgICAgICAgb25Db21wbGV0ZTogdGhpcy5fYW5pbWF0aW9uQ29tcGxldGVkSGFuZGxlclxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX2xheW91dCgpIHtcbiAgICB0aGlzLl9pdGVtV2lkdGggPSBNYXRoLmZsb29yKDEwMCAvIHRoaXMuX3RvdGFsKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVCdXR0b25DbGljayhldmVudDogTW91c2VFdmVudCkge1xuICAgIGlmIChldmVudC50YXJnZXQgPT09IHRoaXMuX2J1dHRvbkxlZnQuZWxlbWVudCkge1xuICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuX3ZhbHVlIC0gMVxuICAgIH0gZWxzZSBpZiAoZXZlbnQudGFyZ2V0ID09PSB0aGlzLl9idXR0b25SaWdodC5lbGVtZW50KSB7XG4gICAgICB0aGlzLnZhbHVlID0gdGhpcy5fdmFsdWUgKyAxXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVBbmltYXRpb25Db21wbGV0ZWQoKSB7XG4gICAgaWYgKHRoaXMuX3ZhbHVlID09PSB0aGlzLl90b3RhbCkge1xuICAgICAgdGhpcy5fcHJvZ3Jlc3NFbGVtZW50LmFkZENsYXNzKENMQVNTX1BST0dSRVNTX0NPTVBMRVRFRClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5fcHJvZ3Jlc3NFbGVtZW50LnJlbW92ZUNsYXNzKENMQVNTX1BST0dSRVNTX0NPTVBMRVRFRClcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgY3VycmVudCBwcm9ncmVzcyB2YWx1ZSBpbiB0aGUgcmFuZ2Ugb2YgMS4udG90YWwuXG4gICAqL1xuICBnZXQgdmFsdWUoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlXG4gIH1cblxuICAvKipcbiAgICogU2V0cyB0aGUgY3VycmVudCBwcm9ncmVzcy5cbiAgICogQHBhcmFtIHtudW1iZXJ9IC0gVGhlIHByb2dyZXNzIGluIHRoZSByYW5nZSBvZiAxLi50b3RhbC5cbiAgICovXG4gIHNldCB2YWx1ZSh2YWwpIHtcbiAgICB0aGlzLl92YWx1ZSA9IGNsYW1wKHZhbCwgdGhpcy5fbWluVmFsdWUsIHRoaXMuX3RvdGFsKVxuICAgIHRoaXMuX3VwZGF0ZSh0cnVlKVxuXG4gICAgdGhpcy5kaXNwYXRjaEV2ZW50KFwiY2hhbmdlZFwiKVxuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIHRvdGFsIHByb2dyZXNzIHZhbHVlLlxuICAgKi9cbiAgZ2V0IHRvdGFsKCkge1xuICAgIHJldHVybiB0aGlzLl90b3RhbFxuICB9XG5cbiAgLyoqXG4gICAqIFNldHMgdGhlIHRvdGFsIHByb2dyZXNzIHZhbHVlIGFuZCB1cGRhdGVzIHRoZSBVSSBhY2NvcmRpbmdseS5cbiAgICogQHBhcmFtIHtudW1iZXJ9IC0gVGhlIHRvdGFsIHByb2dyZXNzIHBvc2l0aXZlIGludGVnZXIgdmFsdWUuXG4gICAqL1xuICBzZXQgdG90YWwodmFsdWUpIHtcbiAgICBpZiAodGhpcy5fdG90YWwgPT09IHZhbHVlKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICB0aGlzLl90b3RhbCA9IE1hdGgubWF4KHZhbHVlLCB0aGlzLl9taW5WYWx1ZSlcbiAgICB0aGlzLl92YWx1ZSA9IGNsYW1wKHRoaXMuX3ZhbHVlLCB0aGlzLl9taW5WYWx1ZSwgdGhpcy5fdG90YWwpXG5cbiAgICAvLyBDbGVhciB0aGUgdGlja3NcbiAgICBmb3IgKGxldCB0aWNrIG9mIHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKGAuJHtDTEFTU19USUNLfWApKSB7XG4gICAgICB0aGlzLl9iYXJFbGVtZW50LmVsZW1lbnQucmVtb3ZlQ2hpbGQodGljaylcbiAgICB9XG5cbiAgICB0aGlzLl9sYXlvdXQoKVxuICAgIHRoaXMuX2FkZFRpY2tzKClcblxuICAgIHRoaXMuX3VwZGF0ZShmYWxzZSlcblxuICAgIHRoaXMuZGlzcGF0Y2hFdmVudChcInRvdGFsY2hhbmdlZFwiKVxuICB9XG5cbiAgLyoqXG4gICAqIEVuYWJsZXMgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGVuYWJsZSgpIHtcbiAgICB0aGlzLl9idXR0b25MZWZ0LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2J1dHRvbkNsaWNrSGFuZGxlcilcbiAgICB0aGlzLl9idXR0b25SaWdodC5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9idXR0b25DbGlja0hhbmRsZXIpXG4gIH1cblxuICAvKipcbiAgICogRGlzYWJsZXMgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGRpc2FibGUoKSB7XG4gICAgdGhpcy5fYnV0dG9uTGVmdC5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9idXR0b25DbGlja0hhbmRsZXIpXG4gICAgdGhpcy5fYnV0dG9uUmlnaHQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fYnV0dG9uQ2xpY2tIYW5kbGVyKVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0KCkge1xuICBzZWFyY2hBbmRJbml0aWFsaXplKFwiLnByb2dyZXNzLWxpZ2h0XCIsIChlKSA9PiB7XG4gICAgbmV3IFByb2dyZXNzTGlnaHQoZSlcbiAgfSlcbn1cblxuZXhwb3J0IGRlZmF1bHQgUHJvZ3Jlc3NMaWdodFxuIl0sInNvdXJjZVJvb3QiOiIuLi8uLi8uLi8uLi8uLiJ9
