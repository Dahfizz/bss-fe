import DomElement from "../DomElement";
/**
 * Loader bar component
 */
declare class LoaderBar extends DomElement {
    private progressElement;
    private fileNameElement;
    private progressLabelElement;
    private totalProgressElement;
    private value;
    /**
     * Creates and initializes the LoaderBar component.
     * @param {Element} - The root element of the LoaderBar component.
     */
    constructor(element: Element);
    /**
     * Initializes the loader bar component.
     * @private
     */
    protected _initialize(): void;
    /**
     * Gets the current progress value in the range of 0..1.
     */
    /**
    * Sets the current progress.
    * @param {number} - The progress in the range of 0..1.
    */
    progress: number;
    /**
     * Gets the filename.
     * @returns {string} - The filename.
     */
    /**
    * Sets the filename.
    */
    filename: string | undefined;
    /**
     * Sets the file size label.
     */
    fileSize: string;
}
export declare function init(): void;
export default LoaderBar;
