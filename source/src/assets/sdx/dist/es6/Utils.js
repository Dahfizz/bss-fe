import * as tslib_1 from "tslib";
/**
 * Calls the callback function when the document has been completely parsed.
 * @param {callback} value The callback function to execute.
 */
export function onDocumentReady(callback) {
    function completed() {
        document.removeEventListener("DOMContentLoaded", completed, false);
        window.removeEventListener("load", completed, false);
        callback();
    }
    if (document.readyState === "complete") {
        setTimeout(callback);
    }
    else {
        document.addEventListener("DOMContentLoaded", completed, false);
        // A fallback to window.onload, that will always work
        window.addEventListener("load", completed, false);
    }
}
export function searchAndInitialize(selector, callback, initSelector) {
    var e_1, _a;
    if (!callback) {
        throw new Error("The callback cannot be undefined");
    }
    var elements = document.querySelectorAll(selector);
    try {
        for (var elements_1 = tslib_1.__values(elements), elements_1_1 = elements_1.next(); !elements_1_1.done; elements_1_1 = elements_1.next()) {
            var e = elements_1_1.value;
            var initElement = e;
            if (initSelector) {
                initElement = initSelector(e);
            }
            if (initElement.getAttribute("data-init") === "auto") {
                callback(e);
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (elements_1_1 && !elements_1_1.done && (_a = elements_1.return)) _a.call(elements_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
}
/**
 * Returns a number whose value is limited to the given range.
 *
 * Example: limit the output of this computation to between 0 and 255
 * Utils.clamp(number, 0, 255)
 *
 * @param {Number} value The number to clamp
 * @param {Number} min The lower boundary of the output range
 * @param {Number} max The upper boundary of the output range
 * @returns A number in the range [min, max]
 * @type Number
 */
export function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
}
/**
 * A polyfill for Event.preventDefault().
 * @param {Event} event - The event to prevent the default action.
 */
export function preventDefault(event) {
    if (event.preventDefault) {
        event.preventDefault();
    }
    else {
        event.returnValue = false;
    }
}
/**
 * A polyfill for Node.remove().
 * @param {Node} node - The node to remove.
 */
export function remove(node) {
    if (!node || !node.parentNode) {
        return;
    }
    node.parentNode.removeChild(node);
}
/**
 * A simple polyfill for the Array.find() method.
 * @param {Array} array - The array to search in.
 * @param {function} expression - The expression to evaluate. Must return true if the element matches.
 */
export function find(array, expression) {
    for (var i = 0; i < array.length; i++) {
        var item = array[i];
        if (expression(item) === true) {
            return item;
        }
    }
    return undefined;
}
/**
 * Checks the useragent and returns the Microsoft Internet Explorer / Edge version.
 * If another browser is detected 0 is returned.
 */
export function msIEVersion(ua) {
    if (ua === void 0) { ua = window.navigator.userAgent; }
    // see http://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery
    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }
    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf("rv:");
        return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }
    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }
    // other browser
    return 0;
}
/**
 * Tries to move a child element to the top by scrolling the parent element, if it is not already fully visible.
 */
export function scrollIntoView(child) {
    var parent = child.parentNode;
    var parentRect = parent.getBoundingClientRect();
    var childRect = child.getBoundingClientRect();
    var isFullyVisible = childRect.top >= parentRect.top && childRect.bottom <= parentRect.top + parent.clientHeight;
    if (!isFullyVisible) {
        parent.scrollTop = childRect.top + parent.scrollTop - parentRect.top;
    }
}

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL1V0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTs7O0dBR0c7QUFDSCxNQUFNLFVBQVUsZUFBZSxDQUFDLFFBQTZCO0lBQzNELFNBQVMsU0FBUztRQUNoQixRQUFRLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ2xFLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ3BELFFBQVEsRUFBRSxDQUFBO0lBQ1osQ0FBQztJQUVELElBQUksUUFBUSxDQUFDLFVBQVUsS0FBSyxVQUFVLEVBQUU7UUFDdEMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0tBQ3JCO1NBQU07UUFFTCxRQUFRLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBRS9ELHFEQUFxRDtRQUNyRCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQTtLQUNsRDtBQUNILENBQUM7QUF1QkQsTUFBTSxVQUFVLG1CQUFtQixDQUNqQyxRQUFnQixFQUNoQixRQUErQixFQUMvQixZQUF1Qzs7SUFFdkMsSUFBSSxDQUFDLFFBQVEsRUFBRTtRQUNiLE1BQU0sSUFBSSxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQTtLQUNwRDtJQUVELElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQXdCLENBQUE7O1FBRXpFLEtBQWMsSUFBQSxhQUFBLGlCQUFBLFFBQVEsQ0FBQSxrQ0FBQSx3REFBRTtZQUFuQixJQUFJLENBQUMscUJBQUE7WUFFUixJQUFJLFdBQVcsR0FBWSxDQUFDLENBQUE7WUFFNUIsSUFBSSxZQUFZLEVBQUU7Z0JBQ2hCLFdBQVcsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDOUI7WUFFRCxJQUFJLFdBQVcsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLEtBQUssTUFBTSxFQUFFO2dCQUNwRCxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDWjtTQUNGOzs7Ozs7Ozs7QUFDSCxDQUFDO0FBRUQ7Ozs7Ozs7Ozs7O0dBV0c7QUFDSCxNQUFNLFVBQVUsS0FBSyxDQUFDLEtBQWEsRUFBRSxHQUFXLEVBQUUsR0FBVztJQUMzRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUE7QUFDNUMsQ0FBQztBQUVEOzs7R0FHRztBQUNILE1BQU0sVUFBVSxjQUFjLENBQUMsS0FBWTtJQUN6QyxJQUFJLEtBQUssQ0FBQyxjQUFjLEVBQUU7UUFDeEIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFBO0tBQ3ZCO1NBQU07UUFDTCxLQUFLLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQTtLQUMxQjtBQUNILENBQUM7QUFFRDs7O0dBR0c7QUFDSCxNQUFNLFVBQVUsTUFBTSxDQUFDLElBQVU7SUFDL0IsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7UUFDN0IsT0FBTTtLQUNQO0lBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDbkMsQ0FBQztBQUVEOzs7O0dBSUc7QUFDSCxNQUFNLFVBQVUsSUFBSSxDQUNsQixLQUErQyxFQUMvQyxVQUFnQztJQUVoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUNyQyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFDbkIsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQzdCLE9BQU8sSUFBSSxDQUFBO1NBQ1o7S0FDRjtJQUVELE9BQU8sU0FBUyxDQUFBO0FBQ2xCLENBQUM7QUFFRDs7O0dBR0c7QUFDSCxNQUFNLFVBQVUsV0FBVyxDQUFDLEVBQXVDO0lBQXZDLG1CQUFBLEVBQUEsS0FBYSxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVM7SUFDakUsd0ZBQXdGO0lBQ3hGLElBQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDaEMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFO1FBQ1osMENBQTBDO1FBQzFDLE9BQU8sUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFBO0tBQ25FO0lBRUQsSUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQTtJQUN0QyxJQUFJLE9BQU8sR0FBRyxDQUFDLEVBQUU7UUFDZixpQ0FBaUM7UUFDakMsSUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUM1QixPQUFPLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQTtLQUMvRDtJQUVELElBQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDaEMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFO1FBQ1oseUNBQXlDO1FBQ3pDLE9BQU8sUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFBO0tBQ25FO0lBRUQsZ0JBQWdCO0lBQ2hCLE9BQU8sQ0FBQyxDQUFBO0FBQ1YsQ0FBQztBQUVEOztHQUVHO0FBQ0gsTUFBTSxVQUFVLGNBQWMsQ0FBQyxLQUFrQjtJQUMvQyxJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsVUFBeUIsQ0FBQTtJQUM5QyxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMscUJBQXFCLEVBQUUsQ0FBQTtJQUNqRCxJQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMscUJBQXFCLEVBQUUsQ0FBQTtJQUMvQyxJQUFNLGNBQWMsR0FBRyxTQUFTLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBQyxHQUFHLElBQUksU0FBUyxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUE7SUFFbEgsSUFBSSxDQUFDLGNBQWMsRUFBRTtRQUNuQixNQUFNLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFBO0tBQ3JFO0FBQ0gsQ0FBQyIsImZpbGUiOiJtYWluL3NyYy9VdGlscy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ2FsbHMgdGhlIGNhbGxiYWNrIGZ1bmN0aW9uIHdoZW4gdGhlIGRvY3VtZW50IGhhcyBiZWVuIGNvbXBsZXRlbHkgcGFyc2VkLlxuICogQHBhcmFtIHtjYWxsYmFja30gdmFsdWUgVGhlIGNhbGxiYWNrIGZ1bmN0aW9uIHRvIGV4ZWN1dGUuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBvbkRvY3VtZW50UmVhZHkoY2FsbGJhY2s6IChlPzogRXZlbnQpID0+IHZvaWQpIHtcbiAgZnVuY3Rpb24gY29tcGxldGVkKCkge1xuICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGNvbXBsZXRlZCwgZmFsc2UpXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsIGNvbXBsZXRlZCwgZmFsc2UpXG4gICAgY2FsbGJhY2soKVxuICB9XG5cbiAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgPT09IFwiY29tcGxldGVcIikge1xuICAgIHNldFRpbWVvdXQoY2FsbGJhY2spXG4gIH0gZWxzZSB7XG5cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCBjb21wbGV0ZWQsIGZhbHNlKVxuXG4gICAgLy8gQSBmYWxsYmFjayB0byB3aW5kb3cub25sb2FkLCB0aGF0IHdpbGwgYWx3YXlzIHdvcmtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImxvYWRcIiwgY29tcGxldGVkLCBmYWxzZSlcbiAgfVxufVxuXG4vKipcbiAqIFNlYXJjaGVzIGZvciBlbGVtZW50cyB3aXRoIHRoZSBnaXZlbiBzZWxlY3RvciBhbmQgY2FsbHMgdGhlIGNhbGxiYWNrXG4gKiBmdW5jdGlvbiBpZiB0aGUgYGRhdGEtaW5pdGAgYXR0cmlidXRlIGlzIHByZXNlbnQgb24gdGhlIGVsZW1lbnQuXG4gKiBAcGFyYW0ge3NlbGVjdG9yfSB2YWx1ZSBUaGUgcXVlcnkuXG4gKiBAcGFyYW0ge2NhbGxiYWNrfSB2YWx1ZSBUaGUgY2FsbGJhY2sgZnVuY3Rpb24gdG8gaW5pdGlhbGl6ZSB0aGUgZWxlbWVudC5cbiAqIEBwYXJhbSB7ZnVuY3Rpb259IGluaXRTZWxlY3RvciBUaGUgaW5pdGl0YWxpemF0aW9uIGVsZW1lbnQgc2VsZWN0b3IgZnVuY3Rpb24uXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hBbmRJbml0aWFsaXplPFxuICBLIGV4dGVuZHMga2V5b2YgSFRNTEVsZW1lbnRUYWdOYW1lTWFwXG4gID4oXG4gICAgc2VsZWN0b3I6IEssXG4gICAgY2FsbGJhY2s6IChlbDogSFRNTEVsZW1lbnRUYWdOYW1lTWFwW0tdKSA9PiB2b2lkLFxuICAgIGluaXRTZWxlY3Rvcj86IChlbDogSFRNTEVsZW1lbnRUYWdOYW1lTWFwW0tdKSA9PiBFbGVtZW50XG4gICk6IHZvaWRcbmV4cG9ydCBmdW5jdGlvbiBzZWFyY2hBbmRJbml0aWFsaXplPFxuICBFIGV4dGVuZHMgRWxlbWVudFxuICA+KFxuICAgIHNlbGVjdG9yOiBzdHJpbmcsXG4gICAgY2FsbGJhY2s6IChlbDogRSkgPT4gdm9pZCxcbiAgICBpbml0U2VsZWN0b3I/OiAoZWw6IEUpID0+IEVsZW1lbnRcbiAgKTogdm9pZFxuZXhwb3J0IGZ1bmN0aW9uIHNlYXJjaEFuZEluaXRpYWxpemUoXG4gIHNlbGVjdG9yOiBzdHJpbmcsXG4gIGNhbGxiYWNrOiAoZWw6IEVsZW1lbnQpID0+IHZvaWQsXG4gIGluaXRTZWxlY3Rvcj86IChlbDogRWxlbWVudCkgPT4gRWxlbWVudFxuKTogdm9pZCB7XG4gIGlmICghY2FsbGJhY2spIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJUaGUgY2FsbGJhY2sgY2Fubm90IGJlIHVuZGVmaW5lZFwiKVxuICB9XG5cbiAgbGV0IGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChzZWxlY3RvcikgYXMgTm9kZUxpc3RPZjxFbGVtZW50PlxuXG4gIGZvciAobGV0IGUgb2YgZWxlbWVudHMpIHtcblxuICAgIGxldCBpbml0RWxlbWVudDogRWxlbWVudCA9IGVcblxuICAgIGlmIChpbml0U2VsZWN0b3IpIHtcbiAgICAgIGluaXRFbGVtZW50ID0gaW5pdFNlbGVjdG9yKGUpXG4gICAgfVxuXG4gICAgaWYgKGluaXRFbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtaW5pdFwiKSA9PT0gXCJhdXRvXCIpIHtcbiAgICAgIGNhbGxiYWNrKGUpXG4gICAgfVxuICB9XG59XG5cbi8qKlxuICogUmV0dXJucyBhIG51bWJlciB3aG9zZSB2YWx1ZSBpcyBsaW1pdGVkIHRvIHRoZSBnaXZlbiByYW5nZS5cbiAqXG4gKiBFeGFtcGxlOiBsaW1pdCB0aGUgb3V0cHV0IG9mIHRoaXMgY29tcHV0YXRpb24gdG8gYmV0d2VlbiAwIGFuZCAyNTVcbiAqIFV0aWxzLmNsYW1wKG51bWJlciwgMCwgMjU1KVxuICpcbiAqIEBwYXJhbSB7TnVtYmVyfSB2YWx1ZSBUaGUgbnVtYmVyIHRvIGNsYW1wXG4gKiBAcGFyYW0ge051bWJlcn0gbWluIFRoZSBsb3dlciBib3VuZGFyeSBvZiB0aGUgb3V0cHV0IHJhbmdlXG4gKiBAcGFyYW0ge051bWJlcn0gbWF4IFRoZSB1cHBlciBib3VuZGFyeSBvZiB0aGUgb3V0cHV0IHJhbmdlXG4gKiBAcmV0dXJucyBBIG51bWJlciBpbiB0aGUgcmFuZ2UgW21pbiwgbWF4XVxuICogQHR5cGUgTnVtYmVyXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBjbGFtcCh2YWx1ZTogbnVtYmVyLCBtaW46IG51bWJlciwgbWF4OiBudW1iZXIpIHtcbiAgcmV0dXJuIE1hdGgubWluKE1hdGgubWF4KHZhbHVlLCBtaW4pLCBtYXgpXG59XG5cbi8qKlxuICogQSBwb2x5ZmlsbCBmb3IgRXZlbnQucHJldmVudERlZmF1bHQoKS5cbiAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50IC0gVGhlIGV2ZW50IHRvIHByZXZlbnQgdGhlIGRlZmF1bHQgYWN0aW9uLlxuICovXG5leHBvcnQgZnVuY3Rpb24gcHJldmVudERlZmF1bHQoZXZlbnQ6IEV2ZW50KSB7XG4gIGlmIChldmVudC5wcmV2ZW50RGVmYXVsdCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgfSBlbHNlIHtcbiAgICBldmVudC5yZXR1cm5WYWx1ZSA9IGZhbHNlXG4gIH1cbn1cblxuLyoqXG4gKiBBIHBvbHlmaWxsIGZvciBOb2RlLnJlbW92ZSgpLlxuICogQHBhcmFtIHtOb2RlfSBub2RlIC0gVGhlIG5vZGUgdG8gcmVtb3ZlLlxuICovXG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlKG5vZGU6IE5vZGUpIHtcbiAgaWYgKCFub2RlIHx8ICFub2RlLnBhcmVudE5vZGUpIHtcbiAgICByZXR1cm5cbiAgfVxuXG4gIG5vZGUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChub2RlKVxufVxuXG4vKipcbiAqIEEgc2ltcGxlIHBvbHlmaWxsIGZvciB0aGUgQXJyYXkuZmluZCgpIG1ldGhvZC5cbiAqIEBwYXJhbSB7QXJyYXl9IGFycmF5IC0gVGhlIGFycmF5IHRvIHNlYXJjaCBpbi5cbiAqIEBwYXJhbSB7ZnVuY3Rpb259IGV4cHJlc3Npb24gLSBUaGUgZXhwcmVzc2lvbiB0byBldmFsdWF0ZS4gTXVzdCByZXR1cm4gdHJ1ZSBpZiB0aGUgZWxlbWVudCBtYXRjaGVzLlxuICovXG5leHBvcnQgZnVuY3Rpb24gZmluZDxUID0gYW55PihcbiAgYXJyYXk6IFRbXSB8IHsgbGVuZ3RoOiBudW1iZXIsIFtpOiBudW1iZXJdOiBUIH0sXG4gIGV4cHJlc3Npb246IChpdGVtOiBUKSA9PiBib29sZWFuXG4pIHtcbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBhcnJheS5sZW5ndGg7IGkrKykge1xuICAgIGxldCBpdGVtID0gYXJyYXlbaV1cbiAgICBpZiAoZXhwcmVzc2lvbihpdGVtKSA9PT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuIGl0ZW1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gdW5kZWZpbmVkXG59XG5cbi8qKlxuICogQ2hlY2tzIHRoZSB1c2VyYWdlbnQgYW5kIHJldHVybnMgdGhlIE1pY3Jvc29mdCBJbnRlcm5ldCBFeHBsb3JlciAvIEVkZ2UgdmVyc2lvbi5cbiAqIElmIGFub3RoZXIgYnJvd3NlciBpcyBkZXRlY3RlZCAwIGlzIHJldHVybmVkLlxuICovXG5leHBvcnQgZnVuY3Rpb24gbXNJRVZlcnNpb24odWE6IHN0cmluZyA9IHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50KSB7XG4gIC8vIHNlZSBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzE5OTk5Mzg4L2NoZWNrLWlmLXVzZXItaXMtdXNpbmctaWUtd2l0aC1qcXVlcnlcbiAgY29uc3QgbXNpZSA9IHVhLmluZGV4T2YoXCJNU0lFIFwiKVxuICBpZiAobXNpZSA+IDApIHtcbiAgICAvLyBJRSAxMCBvciBvbGRlciA9PiByZXR1cm4gdmVyc2lvbiBudW1iZXJcbiAgICByZXR1cm4gcGFyc2VJbnQodWEuc3Vic3RyaW5nKG1zaWUgKyA1LCB1YS5pbmRleE9mKFwiLlwiLCBtc2llKSksIDEwKVxuICB9XG5cbiAgY29uc3QgdHJpZGVudCA9IHVhLmluZGV4T2YoXCJUcmlkZW50L1wiKVxuICBpZiAodHJpZGVudCA+IDApIHtcbiAgICAvLyBJRSAxMSA9PiByZXR1cm4gdmVyc2lvbiBudW1iZXJcbiAgICBjb25zdCBydiA9IHVhLmluZGV4T2YoXCJydjpcIilcbiAgICByZXR1cm4gcGFyc2VJbnQodWEuc3Vic3RyaW5nKHJ2ICsgMywgdWEuaW5kZXhPZihcIi5cIiwgcnYpKSwgMTApXG4gIH1cblxuICBjb25zdCBlZGdlID0gdWEuaW5kZXhPZihcIkVkZ2UvXCIpXG4gIGlmIChlZGdlID4gMCkge1xuICAgIC8vIEVkZ2UgKElFIDEyKykgPT4gcmV0dXJuIHZlcnNpb24gbnVtYmVyXG4gICAgcmV0dXJuIHBhcnNlSW50KHVhLnN1YnN0cmluZyhlZGdlICsgNSwgdWEuaW5kZXhPZihcIi5cIiwgZWRnZSkpLCAxMClcbiAgfVxuXG4gIC8vIG90aGVyIGJyb3dzZXJcbiAgcmV0dXJuIDBcbn1cblxuLyoqXG4gKiBUcmllcyB0byBtb3ZlIGEgY2hpbGQgZWxlbWVudCB0byB0aGUgdG9wIGJ5IHNjcm9sbGluZyB0aGUgcGFyZW50IGVsZW1lbnQsIGlmIGl0IGlzIG5vdCBhbHJlYWR5IGZ1bGx5IHZpc2libGUuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzY3JvbGxJbnRvVmlldyhjaGlsZDogSFRNTEVsZW1lbnQpIHtcbiAgY29uc3QgcGFyZW50ID0gY2hpbGQucGFyZW50Tm9kZSBhcyBIVE1MRWxlbWVudFxuICBjb25zdCBwYXJlbnRSZWN0ID0gcGFyZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpXG4gIGNvbnN0IGNoaWxkUmVjdCA9IGNoaWxkLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpXG4gIGNvbnN0IGlzRnVsbHlWaXNpYmxlID0gY2hpbGRSZWN0LnRvcCA+PSBwYXJlbnRSZWN0LnRvcCAmJiBjaGlsZFJlY3QuYm90dG9tIDw9IHBhcmVudFJlY3QudG9wICsgcGFyZW50LmNsaWVudEhlaWdodFxuXG4gIGlmICghaXNGdWxseVZpc2libGUpIHtcbiAgICBwYXJlbnQuc2Nyb2xsVG9wID0gY2hpbGRSZWN0LnRvcCArIHBhcmVudC5zY3JvbGxUb3AgLSBwYXJlbnRSZWN0LnRvcFxuICB9XG59XG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uIn0=
