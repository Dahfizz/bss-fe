import * as Dom from "./DomFunctions";
/**
 * A wrapper class for DOM Elements.
 */
var DomElement = /** @class */ (function () {
    /**
     * Creates a new instance.
     * @param {Element} - The element to wrap.
     * @param {String} - The DOM element to create.
     */
    function DomElement(element) {
        if (typeof element === "string") {
            this.element = document.createElement(element);
        }
        else {
            this.element = element;
        }
    }
    /**
     * Adds the specified CSS class to the element.
     * @param {String} - The class name to add.
     * @return {DomElement} Returns the current instance for fluent chaining of calls.
     */
    DomElement.prototype.addClass = function (name) {
        Dom.addClass(this.element, name);
        return this;
    };
    /**
     * Removes the specified CSS class from the element.
     * @param {String} - The class name to remove.
     * @return {DomElement} Returns the current instance for fluent chaining of calls.
     */
    DomElement.prototype.removeClass = function (name) {
        Dom.removeClass(this.element, name);
        return this;
    };
    DomElement.prototype.hasClass = function (name) {
        return Dom.hasClass(this.element, name);
    };
    DomElement.prototype.toggleClass = function (name) {
        Dom.toggleClass(this.element, name);
        return this;
    };
    Object.defineProperty(DomElement.prototype, "classes", {
        get: function () {
            return this.element.classList;
        },
        enumerable: true,
        configurable: true
    });
    DomElement.prototype.setId = function (id) {
        this.element.setAttribute("id", id);
        return this;
    };
    Object.defineProperty(DomElement.prototype, "innerText", {
        get: function () {
            return Dom.text(this.element);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DomElement.prototype, "innerHtml", {
        get: function () {
            return this.element.innerHTML;
        },
        enumerable: true,
        configurable: true
    });
    DomElement.prototype.setHtml = function (value) {
        if (typeof value !== "string") {
            throw new Error("Expected HTML string");
        }
        this.element.innerHTML = value;
        return this;
    };
    DomElement.prototype.getAttribute = function (name) {
        return this.element.getAttribute(name);
    };
    DomElement.prototype.setAttribute = function (name, value) {
        this.element.setAttribute(name, value);
        return this;
    };
    /**
     * Registers an event listener.
     */
    DomElement.prototype.addEventListener = function (type, listener) {
        this.element.addEventListener(type, listener);
    };
    /**
     * Unregisters an event listener on the component.
     */
    DomElement.prototype.removeEventListener = function (type, listener) {
        this.element.removeEventListener(type, listener);
    };
    DomElement.prototype.appendChild = function (newChild) {
        if (!(newChild instanceof DomElement)) {
            throw new Error("Only other DomElements can be added as children");
        }
        this.element.appendChild(newChild.element);
        return this;
    };
    DomElement.prototype.prependChild = function (newChild) {
        if (!(newChild instanceof DomElement)) {
            throw new Error("Only other DomElements can be added as children");
        }
        this.element.insertBefore(newChild.element, this.element.firstChild);
        return this;
    };
    DomElement.prototype.insertBefore = function (newChild) {
        if (!(newChild instanceof DomElement)) {
            throw new Error("Only other DomElements can be added as children");
        }
        if (!this.element.parentNode) {
            throw new Error("Element is not attached");
        }
        this.element.parentNode.insertBefore(newChild.element, this.element);
        return this;
    };
    DomElement.prototype.insertAfter = function (newChild) {
        if (!(newChild instanceof DomElement)) {
            throw new Error("Only other DomElements can be added as children");
        }
        if (!this.element.parentNode) {
            throw new Error("Element is not attached");
        }
        this.element.parentNode.insertBefore(newChild.element, this.element.nextSibling);
        return this;
    };
    DomElement.prototype.removeChild = function (oldChild) {
        if (!(oldChild instanceof DomElement)) {
            throw new Error("Only a DomElements child can be removed");
        }
        this.element.removeChild(oldChild.element);
    };
    DomElement.prototype.find = function (selectors) {
        var e = this.element.querySelector(selectors);
        if (e) {
            return new DomElement(e);
        }
        return undefined;
    };
    DomElement.prototype.wrapWithElement = function (wrapperElement) {
        if (!this.element.parentNode) {
            throw new Error("Element is not attached");
        }
        this.element.parentNode.replaceChild(wrapperElement.element, this.element);
        wrapperElement.element.appendChild(this.element);
        return this;
    };
    DomElement.prototype.dispatchEvent = function (eventName) {
        var event;
        var el = this.element;
        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            event.initEvent(eventName, true, true);
        }
        else if (document.createEventObject) { // IE < 9
            event = document.createEventObject();
            event.eventType = eventName;
        }
        event.eventName = eventName;
        if (el.dispatchEvent) {
            el.dispatchEvent(event);
        }
        else if (el.fireEvent && htmlEvents["on" + eventName]) { // IE < 9
            el.fireEvent("on" + event.eventType, event); // can trigger only real event (e.g. 'click')
        }
        else if (el[eventName]) {
            el[eventName]();
        }
        else if (el["on" + eventName]) {
            el["on" + eventName]();
        }
    };
    DomElement.prototype.css = function (property) {
        return Dom.css(this.element, property);
    };
    /**
     * Removes all child nodes of the current DomElement.
     */
    DomElement.prototype.empty = function () {
        Dom.empty(this.element);
    };
    return DomElement;
}());
export default DomElement;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL0RvbUVsZW1lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLEdBQUcsTUFBTSxnQkFBZ0IsQ0FBQTtBQVFyQzs7R0FFRztBQUNIO0lBRUU7Ozs7T0FJRztJQUNILG9CQUFZLE9BQW9DO1FBQzlDLElBQUksT0FBTyxPQUFPLEtBQUssUUFBUSxFQUFFO1lBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQWlCLENBQUE7U0FDL0Q7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBO1NBQ3ZCO0lBQ0gsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCw2QkFBUSxHQUFSLFVBQVMsSUFBWTtRQUNuQixHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDaEMsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILGdDQUFXLEdBQVgsVUFBWSxJQUFZO1FBQ3RCLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUNuQyxPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFRCw2QkFBUSxHQUFSLFVBQVMsSUFBWTtRQUNuQixPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUN6QyxDQUFDO0lBRUQsZ0NBQVcsR0FBWCxVQUFZLElBQVk7UUFDdEIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ25DLE9BQU8sSUFBSSxDQUFBO0lBQ2IsQ0FBQztJQUVELHNCQUFJLCtCQUFPO2FBQVg7WUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFBO1FBQy9CLENBQUM7OztPQUFBO0lBRUQsMEJBQUssR0FBTCxVQUFNLEVBQVU7UUFDZCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUE7UUFDbkMsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBRUQsc0JBQUksaUNBQVM7YUFBYjtZQUNFLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDL0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxpQ0FBUzthQUFiO1lBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQTtRQUMvQixDQUFDOzs7T0FBQTtJQUVELDRCQUFPLEdBQVAsVUFBUSxLQUFhO1FBQ25CLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLE1BQU0sSUFBSSxLQUFLLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtTQUN4QztRQUVELElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtRQUM5QixPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsSUFBWTtRQUN2QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQ3hDLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsSUFBWSxFQUFFLEtBQWE7UUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQ3RDLE9BQU8sSUFBSSxDQUFBO0lBQ2IsQ0FBQztJQUVEOztPQUVHO0lBQ0gscUNBQWdCLEdBQWhCLFVBQXNELElBQU8sRUFBRSxRQUE2QztRQUMxRyxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQTtJQUMvQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCx3Q0FBbUIsR0FBbkIsVUFBeUQsSUFBTyxFQUFFLFFBQTZDO1FBQzdHLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFBO0lBQ2xELENBQUM7SUFFRCxnQ0FBVyxHQUFYLFVBQVksUUFBb0I7UUFDOUIsSUFBSSxDQUFDLENBQUMsUUFBUSxZQUFZLFVBQVUsQ0FBQyxFQUFFO1lBQ3JDLE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQTtTQUNuRTtRQUVELElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUMxQyxPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsUUFBb0I7UUFDL0IsSUFBSSxDQUFDLENBQUMsUUFBUSxZQUFZLFVBQVUsQ0FBQyxFQUFFO1lBQ3JDLE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQTtTQUNuRTtRQUVELElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUNwRSxPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsUUFBb0I7UUFDL0IsSUFBSSxDQUFDLENBQUMsUUFBUSxZQUFZLFVBQVUsQ0FBQyxFQUFFO1lBQ3JDLE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQTtTQUNuRTtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUM1QixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUE7U0FDM0M7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDcEUsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBRUQsZ0NBQVcsR0FBWCxVQUFZLFFBQW9CO1FBQzlCLElBQUksQ0FBQyxDQUFDLFFBQVEsWUFBWSxVQUFVLENBQUMsRUFBRTtZQUNyQyxNQUFNLElBQUksS0FBSyxDQUFDLGlEQUFpRCxDQUFDLENBQUE7U0FDbkU7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUU7WUFDNUIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBO1NBQzNDO1FBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUNoRixPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFRCxnQ0FBVyxHQUFYLFVBQVksUUFBb0I7UUFDOUIsSUFBSSxDQUFDLENBQUMsUUFBUSxZQUFZLFVBQVUsQ0FBQyxFQUFFO1lBQ3JDLE1BQU0sSUFBSSxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQTtTQUMzRDtRQUVELElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUM1QyxDQUFDO0lBRUQseUJBQUksR0FBSixVQUFLLFNBQWlCO1FBQ3BCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQzdDLElBQUksQ0FBQyxFQUFFO1lBQ0wsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFZLENBQUMsQ0FBQTtTQUNwQztRQUVELE9BQU8sU0FBUyxDQUFBO0lBQ2xCLENBQUM7SUFFRCxvQ0FBZSxHQUFmLFVBQWdCLGNBQTBCO1FBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUM1QixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUE7U0FDM0M7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDMUUsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBRWhELE9BQU8sSUFBSSxDQUFBO0lBQ2IsQ0FBQztJQUVELGtDQUFhLEdBQWIsVUFBYyxTQUFpQjtRQUM3QixJQUFJLEtBQUssQ0FBQTtRQUNULElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUE7UUFFckIsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO1lBQ3hCLEtBQUssR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO1lBQzFDLEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQTtTQUN2QzthQUFNLElBQUssUUFBZ0IsQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLFNBQVM7WUFDekQsS0FBSyxHQUFJLFFBQWdCLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTtZQUM3QyxLQUFLLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTtTQUM1QjtRQUNELEtBQUssQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBO1FBQzNCLElBQUksRUFBRSxDQUFDLGFBQWEsRUFBRTtZQUNwQixFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3hCO2FBQU0sSUFBSyxFQUFVLENBQUMsU0FBUyxJQUFJLFVBQVUsQ0FBQyxPQUFLLFNBQVcsQ0FBQyxFQUFFLEVBQUUsU0FBUztZQUMxRSxFQUFVLENBQUMsU0FBUyxDQUFDLE9BQUssS0FBSyxDQUFDLFNBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQSxDQUFDLDZDQUE2QztTQUNuRzthQUFNLElBQUksRUFBRSxDQUFDLFNBQTBCLENBQUMsRUFBRTtZQUN4QyxFQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQTtTQUN6QjthQUFNLElBQUksRUFBRSxDQUFDLE9BQUssU0FBNEIsQ0FBQyxFQUFFO1lBQy9DLEVBQVUsQ0FBQyxPQUFLLFNBQVcsQ0FBQyxFQUFFLENBQUE7U0FDaEM7SUFDSCxDQUFDO0lBRUQsd0JBQUcsR0FBSCxVQUFJLFFBQWdCO1FBQ2xCLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFBO0lBQ3hDLENBQUM7SUFFRDs7T0FFRztJQUNILDBCQUFLLEdBQUw7UUFDRSxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUN6QixDQUFDO0lBQ0gsaUJBQUM7QUFBRCxDQW5NQSxBQW1NQyxJQUFBO0FBRUQsZUFBZSxVQUFVLENBQUEiLCJmaWxlIjoibWFpbi9zcmMvRG9tRWxlbWVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIERvbSBmcm9tIFwiLi9Eb21GdW5jdGlvbnNcIlxuXG5kZWNsYXJlIGdsb2JhbCB7XG4gIGxldCBodG1sRXZlbnRzOiB7XG4gICAgW2V2ZW50TmFtZTogc3RyaW5nXTogKCkgPT4gdm9pZDtcbiAgfVxufVxuXG4vKipcbiAqIEEgd3JhcHBlciBjbGFzcyBmb3IgRE9NIEVsZW1lbnRzLlxuICovXG5jbGFzcyBEb21FbGVtZW50PFQgZXh0ZW5kcyBFbGVtZW50ID0gRWxlbWVudD4ge1xuICBwdWJsaWMgZWxlbWVudDogVFxuICAvKipcbiAgICogQ3JlYXRlcyBhIG5ldyBpbnN0YW5jZS5cbiAgICogQHBhcmFtIHtFbGVtZW50fSAtIFRoZSBlbGVtZW50IHRvIHdyYXAuXG4gICAqIEBwYXJhbSB7U3RyaW5nfSAtIFRoZSBET00gZWxlbWVudCB0byBjcmVhdGUuXG4gICAqL1xuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBUIHwga2V5b2YgRWxlbWVudFRhZ05hbWVNYXApIHtcbiAgICBpZiAodHlwZW9mIGVsZW1lbnQgPT09IFwic3RyaW5nXCIpIHtcbiAgICAgIHRoaXMuZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoZWxlbWVudCkgYXMgRWxlbWVudCBhcyBUXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZWxlbWVudCA9IGVsZW1lbnRcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQWRkcyB0aGUgc3BlY2lmaWVkIENTUyBjbGFzcyB0byB0aGUgZWxlbWVudC5cbiAgICogQHBhcmFtIHtTdHJpbmd9IC0gVGhlIGNsYXNzIG5hbWUgdG8gYWRkLlxuICAgKiBAcmV0dXJuIHtEb21FbGVtZW50fSBSZXR1cm5zIHRoZSBjdXJyZW50IGluc3RhbmNlIGZvciBmbHVlbnQgY2hhaW5pbmcgb2YgY2FsbHMuXG4gICAqL1xuICBhZGRDbGFzcyhuYW1lOiBzdHJpbmcpIHtcbiAgICBEb20uYWRkQ2xhc3ModGhpcy5lbGVtZW50LCBuYW1lKVxuICAgIHJldHVybiB0aGlzXG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyB0aGUgc3BlY2lmaWVkIENTUyBjbGFzcyBmcm9tIHRoZSBlbGVtZW50LlxuICAgKiBAcGFyYW0ge1N0cmluZ30gLSBUaGUgY2xhc3MgbmFtZSB0byByZW1vdmUuXG4gICAqIEByZXR1cm4ge0RvbUVsZW1lbnR9IFJldHVybnMgdGhlIGN1cnJlbnQgaW5zdGFuY2UgZm9yIGZsdWVudCBjaGFpbmluZyBvZiBjYWxscy5cbiAgICovXG4gIHJlbW92ZUNsYXNzKG5hbWU6IHN0cmluZykge1xuICAgIERvbS5yZW1vdmVDbGFzcyh0aGlzLmVsZW1lbnQsIG5hbWUpXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIGhhc0NsYXNzKG5hbWU6IHN0cmluZykge1xuICAgIHJldHVybiBEb20uaGFzQ2xhc3ModGhpcy5lbGVtZW50LCBuYW1lKVxuICB9XG5cbiAgdG9nZ2xlQ2xhc3MobmFtZTogc3RyaW5nKSB7XG4gICAgRG9tLnRvZ2dsZUNsYXNzKHRoaXMuZWxlbWVudCwgbmFtZSlcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgZ2V0IGNsYXNzZXMoKSB7XG4gICAgcmV0dXJuIHRoaXMuZWxlbWVudC5jbGFzc0xpc3RcbiAgfVxuXG4gIHNldElkKGlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwiaWRcIiwgaWQpXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIGdldCBpbm5lclRleHQoKSB7XG4gICAgcmV0dXJuIERvbS50ZXh0KHRoaXMuZWxlbWVudClcbiAgfVxuXG4gIGdldCBpbm5lckh0bWwoKSB7XG4gICAgcmV0dXJuIHRoaXMuZWxlbWVudC5pbm5lckhUTUxcbiAgfVxuXG4gIHNldEh0bWwodmFsdWU6IHN0cmluZykge1xuICAgIGlmICh0eXBlb2YgdmFsdWUgIT09IFwic3RyaW5nXCIpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkV4cGVjdGVkIEhUTUwgc3RyaW5nXCIpXG4gICAgfVxuXG4gICAgdGhpcy5lbGVtZW50LmlubmVySFRNTCA9IHZhbHVlXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIGdldEF0dHJpYnV0ZShuYW1lOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShuYW1lKVxuICB9XG5cbiAgc2V0QXR0cmlidXRlKG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZykge1xuICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGUobmFtZSwgdmFsdWUpXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIC8qKlxuICAgKiBSZWdpc3RlcnMgYW4gZXZlbnQgbGlzdGVuZXIuXG4gICAqL1xuICBhZGRFdmVudExpc3RlbmVyPFQgZXh0ZW5kcyBrZXlvZiBIVE1MRWxlbWVudEV2ZW50TWFwPih0eXBlOiBULCBsaXN0ZW5lcjogKGU6IEhUTUxFbGVtZW50RXZlbnRNYXBbVF0pID0+IHZvaWQpIHtcbiAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcilcbiAgfVxuXG4gIC8qKlxuICAgKiBVbnJlZ2lzdGVycyBhbiBldmVudCBsaXN0ZW5lciBvbiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgcmVtb3ZlRXZlbnRMaXN0ZW5lcjxUIGV4dGVuZHMga2V5b2YgSFRNTEVsZW1lbnRFdmVudE1hcD4odHlwZTogVCwgbGlzdGVuZXI6IChlOiBIVE1MRWxlbWVudEV2ZW50TWFwW1RdKSA9PiB2b2lkKSB7XG4gICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpXG4gIH1cblxuICBhcHBlbmRDaGlsZChuZXdDaGlsZDogRG9tRWxlbWVudCkge1xuICAgIGlmICghKG5ld0NoaWxkIGluc3RhbmNlb2YgRG9tRWxlbWVudCkpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIk9ubHkgb3RoZXIgRG9tRWxlbWVudHMgY2FuIGJlIGFkZGVkIGFzIGNoaWxkcmVuXCIpXG4gICAgfVxuXG4gICAgdGhpcy5lbGVtZW50LmFwcGVuZENoaWxkKG5ld0NoaWxkLmVsZW1lbnQpXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIHByZXBlbmRDaGlsZChuZXdDaGlsZDogRG9tRWxlbWVudCkge1xuICAgIGlmICghKG5ld0NoaWxkIGluc3RhbmNlb2YgRG9tRWxlbWVudCkpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIk9ubHkgb3RoZXIgRG9tRWxlbWVudHMgY2FuIGJlIGFkZGVkIGFzIGNoaWxkcmVuXCIpXG4gICAgfVxuXG4gICAgdGhpcy5lbGVtZW50Lmluc2VydEJlZm9yZShuZXdDaGlsZC5lbGVtZW50LCB0aGlzLmVsZW1lbnQuZmlyc3RDaGlsZClcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgaW5zZXJ0QmVmb3JlKG5ld0NoaWxkOiBEb21FbGVtZW50KSB7XG4gICAgaWYgKCEobmV3Q2hpbGQgaW5zdGFuY2VvZiBEb21FbGVtZW50KSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiT25seSBvdGhlciBEb21FbGVtZW50cyBjYW4gYmUgYWRkZWQgYXMgY2hpbGRyZW5cIilcbiAgICB9XG4gICAgaWYgKCF0aGlzLmVsZW1lbnQucGFyZW50Tm9kZSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiRWxlbWVudCBpcyBub3QgYXR0YWNoZWRcIilcbiAgICB9XG5cbiAgICB0aGlzLmVsZW1lbnQucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUobmV3Q2hpbGQuZWxlbWVudCwgdGhpcy5lbGVtZW50KVxuICAgIHJldHVybiB0aGlzXG4gIH1cblxuICBpbnNlcnRBZnRlcihuZXdDaGlsZDogRG9tRWxlbWVudCkge1xuICAgIGlmICghKG5ld0NoaWxkIGluc3RhbmNlb2YgRG9tRWxlbWVudCkpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIk9ubHkgb3RoZXIgRG9tRWxlbWVudHMgY2FuIGJlIGFkZGVkIGFzIGNoaWxkcmVuXCIpXG4gICAgfVxuICAgIGlmICghdGhpcy5lbGVtZW50LnBhcmVudE5vZGUpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkVsZW1lbnQgaXMgbm90IGF0dGFjaGVkXCIpXG4gICAgfVxuXG4gICAgdGhpcy5lbGVtZW50LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKG5ld0NoaWxkLmVsZW1lbnQsIHRoaXMuZWxlbWVudC5uZXh0U2libGluZylcbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgcmVtb3ZlQ2hpbGQob2xkQ2hpbGQ6IERvbUVsZW1lbnQpIHtcbiAgICBpZiAoIShvbGRDaGlsZCBpbnN0YW5jZW9mIERvbUVsZW1lbnQpKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJPbmx5IGEgRG9tRWxlbWVudHMgY2hpbGQgY2FuIGJlIHJlbW92ZWRcIilcbiAgICB9XG5cbiAgICB0aGlzLmVsZW1lbnQucmVtb3ZlQ2hpbGQob2xkQ2hpbGQuZWxlbWVudClcbiAgfVxuXG4gIGZpbmQoc2VsZWN0b3JzOiBzdHJpbmcpIHtcbiAgICBsZXQgZSA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9ycylcbiAgICBpZiAoZSkge1xuICAgICAgcmV0dXJuIG5ldyBEb21FbGVtZW50KGUgYXMgRWxlbWVudClcbiAgICB9XG5cbiAgICByZXR1cm4gdW5kZWZpbmVkXG4gIH1cblxuICB3cmFwV2l0aEVsZW1lbnQod3JhcHBlckVsZW1lbnQ6IERvbUVsZW1lbnQpIHtcbiAgICBpZiAoIXRoaXMuZWxlbWVudC5wYXJlbnROb2RlKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJFbGVtZW50IGlzIG5vdCBhdHRhY2hlZFwiKVxuICAgIH1cbiAgICB0aGlzLmVsZW1lbnQucGFyZW50Tm9kZS5yZXBsYWNlQ2hpbGQod3JhcHBlckVsZW1lbnQuZWxlbWVudCwgdGhpcy5lbGVtZW50KVxuICAgIHdyYXBwZXJFbGVtZW50LmVsZW1lbnQuYXBwZW5kQ2hpbGQodGhpcy5lbGVtZW50KVxuXG4gICAgcmV0dXJuIHRoaXNcbiAgfVxuXG4gIGRpc3BhdGNoRXZlbnQoZXZlbnROYW1lOiBzdHJpbmcpIHtcbiAgICBsZXQgZXZlbnRcbiAgICBsZXQgZWwgPSB0aGlzLmVsZW1lbnRcblxuICAgIGlmIChkb2N1bWVudC5jcmVhdGVFdmVudCkge1xuICAgICAgZXZlbnQgPSBkb2N1bWVudC5jcmVhdGVFdmVudChcIkhUTUxFdmVudHNcIilcbiAgICAgIGV2ZW50LmluaXRFdmVudChldmVudE5hbWUsIHRydWUsIHRydWUpXG4gICAgfSBlbHNlIGlmICgoZG9jdW1lbnQgYXMgYW55KS5jcmVhdGVFdmVudE9iamVjdCkgeyAvLyBJRSA8IDlcbiAgICAgIGV2ZW50ID0gKGRvY3VtZW50IGFzIGFueSkuY3JlYXRlRXZlbnRPYmplY3QoKVxuICAgICAgZXZlbnQuZXZlbnRUeXBlID0gZXZlbnROYW1lXG4gICAgfVxuICAgIGV2ZW50LmV2ZW50TmFtZSA9IGV2ZW50TmFtZVxuICAgIGlmIChlbC5kaXNwYXRjaEV2ZW50KSB7XG4gICAgICBlbC5kaXNwYXRjaEV2ZW50KGV2ZW50KVxuICAgIH0gZWxzZSBpZiAoKGVsIGFzIGFueSkuZmlyZUV2ZW50ICYmIGh0bWxFdmVudHNbYG9uJHtldmVudE5hbWV9YF0pIHsgLy8gSUUgPCA5XG4gICAgICAoZWwgYXMgYW55KS5maXJlRXZlbnQoYG9uJHtldmVudC5ldmVudFR5cGV9YCwgZXZlbnQpIC8vIGNhbiB0cmlnZ2VyIG9ubHkgcmVhbCBldmVudCAoZS5nLiAnY2xpY2snKVxuICAgIH0gZWxzZSBpZiAoZWxbZXZlbnROYW1lIGFzIGtleW9mIEVsZW1lbnRdKSB7XG4gICAgICAoZWwgYXMgYW55KVtldmVudE5hbWVdKClcbiAgICB9IGVsc2UgaWYgKGVsW2BvbiR7ZXZlbnROYW1lfWAgYXMga2V5b2YgRWxlbWVudF0pIHtcbiAgICAgIChlbCBhcyBhbnkpW2BvbiR7ZXZlbnROYW1lfWBdKClcbiAgICB9XG4gIH1cblxuICBjc3MocHJvcGVydHk6IHN0cmluZykge1xuICAgIHJldHVybiBEb20uY3NzKHRoaXMuZWxlbWVudCwgcHJvcGVydHkpXG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbGwgY2hpbGQgbm9kZXMgb2YgdGhlIGN1cnJlbnQgRG9tRWxlbWVudC5cbiAgICovXG4gIGVtcHR5KCkge1xuICAgIERvbS5lbXB0eSh0aGlzLmVsZW1lbnQpXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRG9tRWxlbWVudFxuIl0sInNvdXJjZVJvb3QiOiIuLi8uLi8uLi8uLiJ9
