import * as tslib_1 from "tslib";
import { searchAndInitialize } from "../Utils";
import DomElement from "../DomElement";
import * as Dom from "../DomFunctions";
var QUERY_HEADER = "thead th";
var CLASS_SORTED_ASCENDING = "js-ascending";
var CLASS_SORTED_DESCENDING = "js-descending";
var CLASS_ARROW = "arrow-icon";
/**
 * The Table component. Adds additional capabilities to standard HTML 5 tables.
 */
var Table = /** @class */ (function (_super) {
    tslib_1.__extends(Table, _super);
    /**
     * Creates a new instance of the table component.
     */
    function Table(element) {
        var _this = _super.call(this, element) || this;
        _this._headerClickHandler = _this._handleHeaderClick.bind(_this);
        _this._body = _this.element.querySelector("tbody");
        _this._rows = _this._body.getElementsByTagName("tr");
        _this._initialize();
        return _this;
    }
    Table.prototype._initialize = function () {
        var e_1, _a;
        try {
            for (var _b = tslib_1.__values(this.element.querySelectorAll(QUERY_HEADER)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var header = _c.value;
                if (header.getAttribute("data-type")) {
                    header.addEventListener("click", this._headerClickHandler);
                    var arrowElement = new DomElement("div")
                        .addClass(CLASS_ARROW)
                        .element;
                    header.appendChild(arrowElement);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    Table.prototype._handleHeaderClick = function (e) {
        var th = e.target;
        this.sort(th);
    };
    /**
     * Sorts the table according to the specified table header element.
     * The column is sorted ascending by default if no direction is specified and no
     * existing sort order class is found in the markup.
     *
     * If the displayed data is not suitable for sorting `<td/>` elements can define a `data-value` attribute
     * which is then used for the data-source.
     *
     * @param {TableHeader} tableHeader The header element of the row to sort by.
     * @param {Number} direction The direction to sort, `1` for ascending, `-1` for descending order. This parameter is optional.
     * @param {function} equalityComparer The equiality comparer function to compare individual cell values.
     */
    Table.prototype.sort = function (tableHeader, direction, equalityComparer) {
        var e_2, _a;
        if (!tableHeader || tableHeader.tagName !== "TH") {
            throw new Error("The parameter 'tableHeader' must be a valid column header node");
        }
        if (direction !== 1 && direction !== -1 && direction) {
            throw new Error("Parameter out of range, parameter 'direction' with value '" + direction + "' must be either -1, 1 or undefined");
        }
        var columnIndex = tableHeader.cellIndex;
        if (!equalityComparer) {
            var dataType = tableHeader.getAttribute("data-type");
            equalityComparer = this._getComparer(dataType);
        }
        if (columnIndex >= this.element.querySelectorAll(QUERY_HEADER).length) {
            throw new Error("Column out of range");
        }
        try {
            for (var _b = tslib_1.__values(this.element.querySelectorAll(QUERY_HEADER)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var header = _c.value;
                if (header !== tableHeader) {
                    Dom.removeClass(header, CLASS_SORTED_ASCENDING);
                    Dom.removeClass(header, CLASS_SORTED_DESCENDING);
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_2) throw e_2.error; }
        }
        if (Dom.hasClass(tableHeader, CLASS_SORTED_ASCENDING)) {
            Dom.removeClass(tableHeader, CLASS_SORTED_ASCENDING);
            Dom.addClass(tableHeader, CLASS_SORTED_DESCENDING);
            direction = direction || -1;
        }
        else {
            Dom.removeClass(tableHeader, CLASS_SORTED_DESCENDING);
            Dom.addClass(tableHeader, CLASS_SORTED_ASCENDING);
            direction = direction || 1;
        }
        this._quicksort(columnIndex, 0, this._rows.length - 1, direction, equalityComparer);
    };
    Table.prototype._getCell = function (column, row) {
        return this._rows[row].cells[column];
    };
    Table.prototype._getRow = function (row) {
        return this._rows[row];
    };
    Table.prototype._getComparer = function (dataType) {
        switch (dataType) {
            case "number": {
                // parse the string as a number
                return function (a, b) { return parseFloat(a) - parseFloat(b); };
            }
            default: {
                // compare strings
                return function (a, b) {
                    if (a < b) {
                        return -1;
                    }
                    if (a > b) {
                        return 1;
                    }
                    return 0;
                };
            }
        }
    };
    Table.prototype._quicksort = function (column, left, right, direction, equalityComparer) {
        if (direction === void 0) { direction = 1; }
        if (right - left > 1) {
            var partition = this._partition(column, left, right, direction, equalityComparer);
            if (left < partition - 1) {
                this._quicksort(column, left, partition - 1, direction, equalityComparer);
            }
            if (partition < right) {
                this._quicksort(column, partition, right, direction, equalityComparer);
            }
        }
    };
    Table.prototype._partition = function (column, left, right, direction, equalityComparer) {
        if (direction === void 0) { direction = 1; }
        var pivot = this._getCell(column, Math.floor((right + left) / 2));
        var i = left;
        var j = right;
        while (i <= j) {
            while (this._equals(this._getCell(column, i), pivot, equalityComparer) * direction < 0) {
                i++;
            }
            while (this._equals(this._getCell(column, j), pivot, equalityComparer) * direction > 0) {
                j--;
            }
            if (i <= j) {
                this._swap(i, j);
                i++;
                j--;
            }
        }
        return i;
    };
    Table.prototype._equals = function (a, b, equalityComparer) {
        var dataA = a.getAttribute("data-value");
        var dataB = b.getAttribute("data-value");
        dataA = dataA || a.textContent || a.innerText;
        dataB = dataB || b.textContent || b.innerText;
        return equalityComparer(dataA, dataB);
    };
    Table.prototype._swap = function (i, j) {
        var tmpNode = this._body.replaceChild(this._getRow(i), this._getRow(j));
        var referenceRow = this._getRow(i);
        if (!referenceRow) {
            this._body.appendChild(tmpNode);
        }
        else {
            this._body.insertBefore(tmpNode, referenceRow);
        }
    };
    /**
     * Destroys the component and clears all references.
     */
    Table.prototype.destroy = function () {
        var e_3, _a;
        try {
            for (var _b = tslib_1.__values(this.element.querySelectorAll(QUERY_HEADER)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var header = _c.value;
                header.removeEventListener("click", this._headerClickHandler);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_3) throw e_3.error; }
        }
        this._headerClickHandler = null;
        this._body = null;
        this._rows = null;
    };
    return Table;
}(DomElement));
export function init() {
    searchAndInitialize("table", function (e) {
        new Table(e);
    });
}
export default Table;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL3RhYmxlL1RhYmxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxVQUFVLENBQUE7QUFDOUMsT0FBTyxVQUFVLE1BQU0sZUFBZSxDQUFBO0FBQ3RDLE9BQU8sS0FBSyxHQUFHLE1BQU0saUJBQWlCLENBQUE7QUFFdEMsSUFBTSxZQUFZLEdBQUcsVUFBVSxDQUFBO0FBRS9CLElBQU0sc0JBQXNCLEdBQUcsY0FBYyxDQUFBO0FBQzdDLElBQU0sdUJBQXVCLEdBQUcsZUFBZSxDQUFBO0FBQy9DLElBQU0sV0FBVyxHQUFHLFlBQVksQ0FBQTtBQU1oQzs7R0FFRztBQUNIO0lBQW9CLGlDQUFVO0lBSzVCOztPQUVHO0lBQ0gsZUFBWSxPQUF5QjtRQUFyQyxZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQVFmO1FBTkMsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFFN0QsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQTRCLENBQUE7UUFDM0UsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFBO1FBRWxELEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTs7SUFDcEIsQ0FBQztJQUVTLDJCQUFXLEdBQXJCOzs7WUFDRSxLQUFtQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBM0QsSUFBSSxNQUFNLFdBQUE7Z0JBQ2IsSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxFQUFFO29CQUNwQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO29CQUUxRCxJQUFJLFlBQVksR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7eUJBQ3JDLFFBQVEsQ0FBQyxXQUFXLENBQUM7eUJBQ3JCLE9BQU8sQ0FBQTtvQkFFVixNQUFNLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO2lCQUNqQzthQUNGOzs7Ozs7Ozs7SUFDSCxDQUFDO0lBRVMsa0NBQWtCLEdBQTVCLFVBQTZCLENBQVE7UUFDbkMsSUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDLE1BQW9DLENBQUE7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUNmLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7T0FXRztJQUNILG9CQUFJLEdBQUosVUFDRSxXQUF1QyxFQUN2QyxTQUFrQixFQUNsQixnQkFBMkI7O1FBRTNCLElBQUksQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDLE9BQU8sS0FBSyxJQUFJLEVBQUU7WUFDaEQsTUFBTSxJQUFJLEtBQUssQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFBO1NBQ2xGO1FBRUQsSUFBSSxTQUFTLEtBQUssQ0FBQyxJQUFJLFNBQVMsS0FBSyxDQUFDLENBQUMsSUFBSSxTQUFTLEVBQUU7WUFDcEQsTUFBTSxJQUFJLEtBQUssQ0FBQywrREFBNkQsU0FBUyx3Q0FBcUMsQ0FBQyxDQUFBO1NBQzdIO1FBRUQsSUFBTSxXQUFXLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQTtRQUV6QyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDckIsSUFBSSxRQUFRLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUNwRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVMsQ0FBQyxDQUFBO1NBQ2hEO1FBRUQsSUFBSSxXQUFXLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLEVBQUU7WUFDckUsTUFBTSxJQUFJLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBO1NBQ3ZDOztZQUVELEtBQW1CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO2dCQUEzRCxJQUFJLE1BQU0sV0FBQTtnQkFDYixJQUFJLE1BQU0sS0FBSyxXQUFXLEVBQUU7b0JBQzFCLEdBQUcsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLHNCQUFzQixDQUFDLENBQUE7b0JBQy9DLEdBQUcsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLHVCQUF1QixDQUFDLENBQUE7aUJBQ2pEO2FBQ0Y7Ozs7Ozs7OztRQUVELElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsc0JBQXNCLENBQUMsRUFBRTtZQUNyRCxHQUFHLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxzQkFBc0IsQ0FBQyxDQUFBO1lBQ3BELEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLHVCQUF1QixDQUFDLENBQUE7WUFFbEQsU0FBUyxHQUFHLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQTtTQUM1QjthQUFNO1lBQ0wsR0FBRyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsdUJBQXVCLENBQUMsQ0FBQTtZQUNyRCxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxzQkFBc0IsQ0FBQyxDQUFBO1lBQ2pELFNBQVMsR0FBRyxTQUFTLElBQUksQ0FBQyxDQUFBO1NBQzNCO1FBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTtJQUNyRixDQUFDO0lBRVMsd0JBQVEsR0FBbEIsVUFBbUIsTUFBYyxFQUFFLEdBQVc7UUFDNUMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQTtJQUN0QyxDQUFDO0lBRVMsdUJBQU8sR0FBakIsVUFBa0IsR0FBVztRQUMzQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUE7SUFDeEIsQ0FBQztJQUVTLDRCQUFZLEdBQXRCLFVBQXVCLFFBQWdCO1FBQ3JDLFFBQVEsUUFBUSxFQUFFO1lBQ2hCLEtBQUssUUFBUSxDQUFDLENBQUM7Z0JBQ2IsK0JBQStCO2dCQUMvQixPQUFPLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQTdCLENBQTZCLENBQUE7YUFDL0M7WUFDRCxPQUFPLENBQUMsQ0FBQztnQkFDUCxrQkFBa0I7Z0JBQ2xCLE9BQU8sVUFBQyxDQUFDLEVBQUUsQ0FBQztvQkFDVixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ1QsT0FBTyxDQUFDLENBQUMsQ0FBQTtxQkFDVjtvQkFDRCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ1QsT0FBTyxDQUFDLENBQUE7cUJBQ1Q7b0JBRUQsT0FBTyxDQUFDLENBQUE7Z0JBQ1YsQ0FBQyxDQUFBO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFUywwQkFBVSxHQUFwQixVQUNFLE1BQWMsRUFDZCxJQUFZLEVBQ1osS0FBYSxFQUNiLFNBQXFCLEVBQ3JCLGdCQUFrQztRQURsQywwQkFBQSxFQUFBLGFBQXFCO1FBR3JCLElBQUksS0FBSyxHQUFHLElBQUksR0FBRyxDQUFDLEVBQUU7WUFFcEIsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTtZQUVqRixJQUFJLElBQUksR0FBRyxTQUFTLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxHQUFHLENBQUMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTthQUMxRTtZQUVELElBQUksU0FBUyxHQUFHLEtBQUssRUFBRTtnQkFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTthQUN2RTtTQUNGO0lBQ0gsQ0FBQztJQUVTLDBCQUFVLEdBQXBCLFVBQ0UsTUFBYyxFQUNkLElBQVksRUFDWixLQUFhLEVBQ2IsU0FBcUIsRUFDckIsZ0JBQWtDO1FBRGxDLDBCQUFBLEVBQUEsYUFBcUI7UUFHckIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2pFLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQTtRQUNaLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQTtRQUViLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNiLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsZ0JBQWdCLENBQUMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxFQUFFO2dCQUN0RixDQUFDLEVBQUUsQ0FBQTthQUNKO1lBRUQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxnQkFBZ0IsQ0FBQyxHQUFHLFNBQVMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3RGLENBQUMsRUFBRSxDQUFBO2FBQ0o7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7Z0JBQ2hCLENBQUMsRUFBRSxDQUFBO2dCQUNILENBQUMsRUFBRSxDQUFBO2FBQ0o7U0FDRjtRQUVELE9BQU8sQ0FBQyxDQUFBO0lBQ1YsQ0FBQztJQUVTLHVCQUFPLEdBQWpCLFVBQ0UsQ0FBYyxFQUNkLENBQWMsRUFDZCxnQkFBa0M7UUFFbEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUN4QyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBRXhDLEtBQUssR0FBRyxLQUFLLElBQUksQ0FBQyxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFBO1FBQzdDLEtBQUssR0FBRyxLQUFLLElBQUksQ0FBQyxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFBO1FBRTdDLE9BQU8sZ0JBQWdCLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFBO0lBQ3ZDLENBQUM7SUFFUyxxQkFBSyxHQUFmLFVBQWdCLENBQVMsRUFBRSxDQUFTO1FBQ2xDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3ZFLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFFcEMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQyxDQUFBO1NBQy9DO0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0gsdUJBQU8sR0FBUDs7O1lBQ0UsS0FBbUIsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQTNELElBQUksTUFBTSxXQUFBO2dCQUNiLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7YUFDOUQ7Ozs7Ozs7OztRQUVBLElBQVksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDeEMsSUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7SUFDNUIsQ0FBQztJQUNILFlBQUM7QUFBRCxDQXJOQSxBQXFOQyxDQXJObUIsVUFBVSxHQXFON0I7QUFFRCxNQUFNLFVBQVUsSUFBSTtJQUNsQixtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsVUFBQyxDQUFDO1FBQzdCLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ2QsQ0FBQyxDQUFDLENBQUE7QUFDSixDQUFDO0FBRUQsZUFBZSxLQUFLLENBQUEiLCJmaWxlIjoibWFpbi9zcmMvdGFibGUvVGFibGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzZWFyY2hBbmRJbml0aWFsaXplIH0gZnJvbSBcIi4uL1V0aWxzXCJcbmltcG9ydCBEb21FbGVtZW50IGZyb20gXCIuLi9Eb21FbGVtZW50XCJcbmltcG9ydCAqIGFzIERvbSBmcm9tIFwiLi4vRG9tRnVuY3Rpb25zXCJcblxuY29uc3QgUVVFUllfSEVBREVSID0gXCJ0aGVhZCB0aFwiXG5cbmNvbnN0IENMQVNTX1NPUlRFRF9BU0NFTkRJTkcgPSBcImpzLWFzY2VuZGluZ1wiXG5jb25zdCBDTEFTU19TT1JURURfREVTQ0VORElORyA9IFwianMtZGVzY2VuZGluZ1wiXG5jb25zdCBDTEFTU19BUlJPVyA9IFwiYXJyb3ctaWNvblwiXG5cbmV4cG9ydCBpbnRlcmZhY2UgQ29tcGFyZXI8VCA9IGFueT4ge1xuICAoaXRlbTE6IFQsIGl0ZW0yOiBUKTogbnVtYmVyXG59XG5cbi8qKlxuICogVGhlIFRhYmxlIGNvbXBvbmVudC4gQWRkcyBhZGRpdGlvbmFsIGNhcGFiaWxpdGllcyB0byBzdGFuZGFyZCBIVE1MIDUgdGFibGVzLlxuICovXG5jbGFzcyBUYWJsZSBleHRlbmRzIERvbUVsZW1lbnQge1xuICBwcml2YXRlIF9oZWFkZXJDbGlja0hhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9ib2R5OiBIVE1MVGFibGVTZWN0aW9uRWxlbWVudFxuICBwcml2YXRlIF9yb3dzOiBOb2RlTGlzdE9mPEhUTUxUYWJsZVJvd0VsZW1lbnQ+XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYSBuZXcgaW5zdGFuY2Ugb2YgdGhlIHRhYmxlIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbnN0cnVjdG9yKGVsZW1lbnQ6IEhUTUxUYWJsZUVsZW1lbnQpIHtcbiAgICBzdXBlcihlbGVtZW50KVxuXG4gICAgdGhpcy5faGVhZGVyQ2xpY2tIYW5kbGVyID0gdGhpcy5faGFuZGxlSGVhZGVyQ2xpY2suYmluZCh0aGlzKVxuXG4gICAgdGhpcy5fYm9keSA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFwidGJvZHlcIikgYXMgSFRNTFRhYmxlU2VjdGlvbkVsZW1lbnRcbiAgICB0aGlzLl9yb3dzID0gdGhpcy5fYm9keS5nZXRFbGVtZW50c0J5VGFnTmFtZShcInRyXCIpXG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICBmb3IgKGxldCBoZWFkZXIgb2YgdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoUVVFUllfSEVBREVSKSkge1xuICAgICAgaWYgKGhlYWRlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXR5cGVcIikpIHtcbiAgICAgICAgaGVhZGVyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9oZWFkZXJDbGlja0hhbmRsZXIpXG5cbiAgICAgICAgbGV0IGFycm93RWxlbWVudCA9IG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAgICAgLmFkZENsYXNzKENMQVNTX0FSUk9XKVxuICAgICAgICAgIC5lbGVtZW50XG5cbiAgICAgICAgaGVhZGVyLmFwcGVuZENoaWxkKGFycm93RWxlbWVudClcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUhlYWRlckNsaWNrKGU6IEV2ZW50KSB7XG4gICAgY29uc3QgdGggPSBlLnRhcmdldCBhcyBIVE1MVGFibGVIZWFkZXJDZWxsRWxlbWVudFxuICAgIHRoaXMuc29ydCh0aClcbiAgfVxuXG4gIC8qKlxuICAgKiBTb3J0cyB0aGUgdGFibGUgYWNjb3JkaW5nIHRvIHRoZSBzcGVjaWZpZWQgdGFibGUgaGVhZGVyIGVsZW1lbnQuXG4gICAqIFRoZSBjb2x1bW4gaXMgc29ydGVkIGFzY2VuZGluZyBieSBkZWZhdWx0IGlmIG5vIGRpcmVjdGlvbiBpcyBzcGVjaWZpZWQgYW5kIG5vXG4gICAqIGV4aXN0aW5nIHNvcnQgb3JkZXIgY2xhc3MgaXMgZm91bmQgaW4gdGhlIG1hcmt1cC5cbiAgICpcbiAgICogSWYgdGhlIGRpc3BsYXllZCBkYXRhIGlzIG5vdCBzdWl0YWJsZSBmb3Igc29ydGluZyBgPHRkLz5gIGVsZW1lbnRzIGNhbiBkZWZpbmUgYSBgZGF0YS12YWx1ZWAgYXR0cmlidXRlXG4gICAqIHdoaWNoIGlzIHRoZW4gdXNlZCBmb3IgdGhlIGRhdGEtc291cmNlLlxuICAgKlxuICAgKiBAcGFyYW0ge1RhYmxlSGVhZGVyfSB0YWJsZUhlYWRlciBUaGUgaGVhZGVyIGVsZW1lbnQgb2YgdGhlIHJvdyB0byBzb3J0IGJ5LlxuICAgKiBAcGFyYW0ge051bWJlcn0gZGlyZWN0aW9uIFRoZSBkaXJlY3Rpb24gdG8gc29ydCwgYDFgIGZvciBhc2NlbmRpbmcsIGAtMWAgZm9yIGRlc2NlbmRpbmcgb3JkZXIuIFRoaXMgcGFyYW1ldGVyIGlzIG9wdGlvbmFsLlxuICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBlcXVhbGl0eUNvbXBhcmVyIFRoZSBlcXVpYWxpdHkgY29tcGFyZXIgZnVuY3Rpb24gdG8gY29tcGFyZSBpbmRpdmlkdWFsIGNlbGwgdmFsdWVzLlxuICAgKi9cbiAgc29ydChcbiAgICB0YWJsZUhlYWRlcjogSFRNTFRhYmxlSGVhZGVyQ2VsbEVsZW1lbnQsXG4gICAgZGlyZWN0aW9uPzogLTEgfCAxLFxuICAgIGVxdWFsaXR5Q29tcGFyZXI/OiBDb21wYXJlclxuICApIHtcbiAgICBpZiAoIXRhYmxlSGVhZGVyIHx8IHRhYmxlSGVhZGVyLnRhZ05hbWUgIT09IFwiVEhcIikge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiVGhlIHBhcmFtZXRlciAndGFibGVIZWFkZXInIG11c3QgYmUgYSB2YWxpZCBjb2x1bW4gaGVhZGVyIG5vZGVcIilcbiAgICB9XG5cbiAgICBpZiAoZGlyZWN0aW9uICE9PSAxICYmIGRpcmVjdGlvbiAhPT0gLTEgJiYgZGlyZWN0aW9uKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYFBhcmFtZXRlciBvdXQgb2YgcmFuZ2UsIHBhcmFtZXRlciAnZGlyZWN0aW9uJyB3aXRoIHZhbHVlICcke2RpcmVjdGlvbn0nIG11c3QgYmUgZWl0aGVyIC0xLCAxIG9yIHVuZGVmaW5lZGApXG4gICAgfVxuXG4gICAgY29uc3QgY29sdW1uSW5kZXggPSB0YWJsZUhlYWRlci5jZWxsSW5kZXhcblxuICAgIGlmICghZXF1YWxpdHlDb21wYXJlcikge1xuICAgICAgbGV0IGRhdGFUeXBlID0gdGFibGVIZWFkZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS10eXBlXCIpXG4gICAgICBlcXVhbGl0eUNvbXBhcmVyID0gdGhpcy5fZ2V0Q29tcGFyZXIoZGF0YVR5cGUhKVxuICAgIH1cblxuICAgIGlmIChjb2x1bW5JbmRleCA+PSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChRVUVSWV9IRUFERVIpLmxlbmd0aCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ29sdW1uIG91dCBvZiByYW5nZVwiKVxuICAgIH1cblxuICAgIGZvciAobGV0IGhlYWRlciBvZiB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChRVUVSWV9IRUFERVIpKSB7XG4gICAgICBpZiAoaGVhZGVyICE9PSB0YWJsZUhlYWRlcikge1xuICAgICAgICBEb20ucmVtb3ZlQ2xhc3MoaGVhZGVyLCBDTEFTU19TT1JURURfQVNDRU5ESU5HKVxuICAgICAgICBEb20ucmVtb3ZlQ2xhc3MoaGVhZGVyLCBDTEFTU19TT1JURURfREVTQ0VORElORylcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoRG9tLmhhc0NsYXNzKHRhYmxlSGVhZGVyLCBDTEFTU19TT1JURURfQVNDRU5ESU5HKSkge1xuICAgICAgRG9tLnJlbW92ZUNsYXNzKHRhYmxlSGVhZGVyLCBDTEFTU19TT1JURURfQVNDRU5ESU5HKVxuICAgICAgRG9tLmFkZENsYXNzKHRhYmxlSGVhZGVyLCBDTEFTU19TT1JURURfREVTQ0VORElORylcblxuICAgICAgZGlyZWN0aW9uID0gZGlyZWN0aW9uIHx8IC0xXG4gICAgfSBlbHNlIHtcbiAgICAgIERvbS5yZW1vdmVDbGFzcyh0YWJsZUhlYWRlciwgQ0xBU1NfU09SVEVEX0RFU0NFTkRJTkcpXG4gICAgICBEb20uYWRkQ2xhc3ModGFibGVIZWFkZXIsIENMQVNTX1NPUlRFRF9BU0NFTkRJTkcpXG4gICAgICBkaXJlY3Rpb24gPSBkaXJlY3Rpb24gfHwgMVxuICAgIH1cblxuICAgIHRoaXMuX3F1aWNrc29ydChjb2x1bW5JbmRleCwgMCwgdGhpcy5fcm93cy5sZW5ndGggLSAxLCBkaXJlY3Rpb24sIGVxdWFsaXR5Q29tcGFyZXIpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2dldENlbGwoY29sdW1uOiBudW1iZXIsIHJvdzogbnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMuX3Jvd3Nbcm93XS5jZWxsc1tjb2x1bW5dXG4gIH1cblxuICBwcm90ZWN0ZWQgX2dldFJvdyhyb3c6IG51bWJlcikge1xuICAgIHJldHVybiB0aGlzLl9yb3dzW3Jvd11cbiAgfVxuXG4gIHByb3RlY3RlZCBfZ2V0Q29tcGFyZXIoZGF0YVR5cGU6IHN0cmluZyk6IENvbXBhcmVyPHN0cmluZz4ge1xuICAgIHN3aXRjaCAoZGF0YVR5cGUpIHtcbiAgICAgIGNhc2UgXCJudW1iZXJcIjoge1xuICAgICAgICAvLyBwYXJzZSB0aGUgc3RyaW5nIGFzIGEgbnVtYmVyXG4gICAgICAgIHJldHVybiAoYSwgYikgPT4gcGFyc2VGbG9hdChhKSAtIHBhcnNlRmxvYXQoYilcbiAgICAgIH1cbiAgICAgIGRlZmF1bHQ6IHtcbiAgICAgICAgLy8gY29tcGFyZSBzdHJpbmdzXG4gICAgICAgIHJldHVybiAoYSwgYikgPT4ge1xuICAgICAgICAgIGlmIChhIDwgYikge1xuICAgICAgICAgICAgcmV0dXJuIC0xXG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChhID4gYikge1xuICAgICAgICAgICAgcmV0dXJuIDFcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4gMFxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9xdWlja3NvcnQoXG4gICAgY29sdW1uOiBudW1iZXIsXG4gICAgbGVmdDogbnVtYmVyLFxuICAgIHJpZ2h0OiBudW1iZXIsXG4gICAgZGlyZWN0aW9uOiAtMSB8IDEgPSAxLFxuICAgIGVxdWFsaXR5Q29tcGFyZXI6IENvbXBhcmVyPHN0cmluZz5cbiAgKSB7XG4gICAgaWYgKHJpZ2h0IC0gbGVmdCA+IDEpIHtcblxuICAgICAgbGV0IHBhcnRpdGlvbiA9IHRoaXMuX3BhcnRpdGlvbihjb2x1bW4sIGxlZnQsIHJpZ2h0LCBkaXJlY3Rpb24sIGVxdWFsaXR5Q29tcGFyZXIpXG5cbiAgICAgIGlmIChsZWZ0IDwgcGFydGl0aW9uIC0gMSkge1xuICAgICAgICB0aGlzLl9xdWlja3NvcnQoY29sdW1uLCBsZWZ0LCBwYXJ0aXRpb24gLSAxLCBkaXJlY3Rpb24sIGVxdWFsaXR5Q29tcGFyZXIpXG4gICAgICB9XG5cbiAgICAgIGlmIChwYXJ0aXRpb24gPCByaWdodCkge1xuICAgICAgICB0aGlzLl9xdWlja3NvcnQoY29sdW1uLCBwYXJ0aXRpb24sIHJpZ2h0LCBkaXJlY3Rpb24sIGVxdWFsaXR5Q29tcGFyZXIpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9wYXJ0aXRpb24oXG4gICAgY29sdW1uOiBudW1iZXIsXG4gICAgbGVmdDogbnVtYmVyLFxuICAgIHJpZ2h0OiBudW1iZXIsXG4gICAgZGlyZWN0aW9uOiAtMSB8IDEgPSAxLFxuICAgIGVxdWFsaXR5Q29tcGFyZXI6IENvbXBhcmVyPHN0cmluZz5cbiAgKSB7XG4gICAgbGV0IHBpdm90ID0gdGhpcy5fZ2V0Q2VsbChjb2x1bW4sIE1hdGguZmxvb3IoKHJpZ2h0ICsgbGVmdCkgLyAyKSlcbiAgICBsZXQgaSA9IGxlZnRcbiAgICBsZXQgaiA9IHJpZ2h0XG5cbiAgICB3aGlsZSAoaSA8PSBqKSB7XG4gICAgICB3aGlsZSAodGhpcy5fZXF1YWxzKHRoaXMuX2dldENlbGwoY29sdW1uLCBpKSwgcGl2b3QsIGVxdWFsaXR5Q29tcGFyZXIpICogZGlyZWN0aW9uIDwgMCkge1xuICAgICAgICBpKytcbiAgICAgIH1cblxuICAgICAgd2hpbGUgKHRoaXMuX2VxdWFscyh0aGlzLl9nZXRDZWxsKGNvbHVtbiwgaiksIHBpdm90LCBlcXVhbGl0eUNvbXBhcmVyKSAqIGRpcmVjdGlvbiA+IDApIHtcbiAgICAgICAgai0tXG4gICAgICB9XG5cbiAgICAgIGlmIChpIDw9IGopIHtcbiAgICAgICAgdGhpcy5fc3dhcChpLCBqKVxuICAgICAgICBpKytcbiAgICAgICAgai0tXG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGlcbiAgfVxuXG4gIHByb3RlY3RlZCBfZXF1YWxzKFxuICAgIGE6IEhUTUxFbGVtZW50LFxuICAgIGI6IEhUTUxFbGVtZW50LFxuICAgIGVxdWFsaXR5Q29tcGFyZXI6IENvbXBhcmVyPHN0cmluZz5cbiAgKSB7XG4gICAgbGV0IGRhdGFBID0gYS5nZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIpXG4gICAgbGV0IGRhdGFCID0gYi5nZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIpXG5cbiAgICBkYXRhQSA9IGRhdGFBIHx8IGEudGV4dENvbnRlbnQgfHwgYS5pbm5lclRleHRcbiAgICBkYXRhQiA9IGRhdGFCIHx8IGIudGV4dENvbnRlbnQgfHwgYi5pbm5lclRleHRcblxuICAgIHJldHVybiBlcXVhbGl0eUNvbXBhcmVyKGRhdGFBLCBkYXRhQilcbiAgfVxuXG4gIHByb3RlY3RlZCBfc3dhcChpOiBudW1iZXIsIGo6IG51bWJlcikge1xuICAgIGxldCB0bXBOb2RlID0gdGhpcy5fYm9keS5yZXBsYWNlQ2hpbGQodGhpcy5fZ2V0Um93KGkpLCB0aGlzLl9nZXRSb3coaikpXG4gICAgY29uc3QgcmVmZXJlbmNlUm93ID0gdGhpcy5fZ2V0Um93KGkpXG5cbiAgICBpZiAoIXJlZmVyZW5jZVJvdykge1xuICAgICAgdGhpcy5fYm9keS5hcHBlbmRDaGlsZCh0bXBOb2RlKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9ib2R5Lmluc2VydEJlZm9yZSh0bXBOb2RlLCByZWZlcmVuY2VSb3cpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIERlc3Ryb3lzIHRoZSBjb21wb25lbnQgYW5kIGNsZWFycyBhbGwgcmVmZXJlbmNlcy5cbiAgICovXG4gIGRlc3Ryb3koKSB7XG4gICAgZm9yIChsZXQgaGVhZGVyIG9mIHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFFVRVJZX0hFQURFUikpIHtcbiAgICAgIGhlYWRlci5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5faGVhZGVyQ2xpY2tIYW5kbGVyKVxuICAgIH1cblxuICAgICh0aGlzIGFzIGFueSkuX2hlYWRlckNsaWNrSGFuZGxlciA9IG51bGw7XG4gICAgKHRoaXMgYXMgYW55KS5fYm9keSA9IG51bGw7XG4gICAgKHRoaXMgYXMgYW55KS5fcm93cyA9IG51bGxcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgc2VhcmNoQW5kSW5pdGlhbGl6ZShcInRhYmxlXCIsIChlKSA9PiB7XG4gICAgbmV3IFRhYmxlKGUpXG4gIH0pXG59XG5cbmV4cG9ydCBkZWZhdWx0IFRhYmxlXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
