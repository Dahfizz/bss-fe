import * as tslib_1 from "tslib";
import { TweenLite, Power1, Power4 } from "gsap";
import Popper from "popper.js";
import DomElement from "../DomElement";
import { addClass, hasClass, removeClass, isHidden, parentWithClass } from "../DomFunctions";
var CLASS_OPEN = "is-open";
var CLASS_MENU = "js-flyout";
var CLASS_TABS = "tabs";
var ANIMATION_OPEN = 0.3;
/**
 * A component for the flyout menu.
 */
var MenuFlyout = /** @class */ (function (_super) {
    tslib_1.__extends(MenuFlyout, _super);
    /**
     * Creates and initializes the flyout component.
     * @param {DomElement} - The root element of the flyout menu component.
     */
    function MenuFlyout(element) {
        var _this = _super.call(this, element) || this;
        _this._animationDuration = ANIMATION_OPEN;
        _this._dynamicPlacement = false;
        _this._clickHandler = _this._handleClick.bind(_this);
        _this._windowClickHandler = _this._handleWindowClick.bind(_this);
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the flyout component.
     * @private
     */
    MenuFlyout.prototype._initialize = function () {
        var dataTarget = this.element.getAttribute("data-target");
        if (dataTarget === null || dataTarget === "") {
            /* tslint:disable:no-console */
            console.error("A flyout menu element requires a 'data-target' that specifies the element to collapse");
            console.info(this.element);
            /* tslint:enable:no-console */
            return;
        }
        if (this._useDynamicPlacement()) {
            this._dynamicPlacement = true;
        }
        var hiddenTarget = this.element.getAttribute("data-hidden");
        if (hiddenTarget !== null && hiddenTarget !== "") {
            this._hiddenIndicator = document.querySelector(hiddenTarget) || undefined;
        }
        this._flyoutElement = document.querySelector(dataTarget);
        this.element.addEventListener("click", this._clickHandler);
    };
    MenuFlyout.prototype._handleClick = function () {
        this.toggle();
    };
    MenuFlyout.prototype._handleWindowClick = function (event) {
        var target = event.target;
        if (parentWithClass(target, CLASS_MENU) === this._flyoutElement) {
            return false;
        }
        while (target !== this.element && target.parentElement) {
            target = target.parentElement;
        }
        if (target !== this.element) {
            this.close();
            return false;
        }
        return true;
    };
    MenuFlyout.prototype._useDynamicPlacement = function () {
        return parentWithClass(this.element, CLASS_TABS);
    };
    MenuFlyout.prototype._openMenu = function (el) {
        TweenLite.killTweensOf(el);
        TweenLite.set(el, {
            display: "block"
        });
        if (this._dynamicPlacement === true) {
            var popperOptions = {
                placement: "bottom",
                modifiers: {
                    flip: {
                        enabled: false
                    }
                },
                eventsEnabled: false
            };
            this._popperInstance = new Popper(this.element, this._flyoutElement, popperOptions);
        }
        TweenLite.to(el, this._animationDuration, {
            className: "+=" + CLASS_OPEN,
            ease: [
                Power1.easeIn, Power4.easeOut
            ]
        });
        // set aria expanded
        el.setAttribute("aria-expanded", "true");
        this.dispatchEvent("opened");
    };
    MenuFlyout.prototype._closeMenu = function (el) {
        TweenLite.killTweensOf(el);
        if (this._popperInstance) {
            this._popperInstance.destroy();
            this._popperInstance = undefined;
        }
        TweenLite.to(el, ANIMATION_OPEN, {
            className: "-=" + CLASS_OPEN,
            ease: [
                Power1.easeIn, Power4.easeOut
            ],
            onComplete: function () {
                TweenLite.set(el, {
                    clearProps: "display"
                });
            }
        });
        // set aria expanded
        el.setAttribute("aria-expanded", "false");
        this.dispatchEvent("closed");
    };
    Object.defineProperty(MenuFlyout.prototype, "animationDuration", {
        /**
         * Sets the opening animation duration.
         * @param {durationInSeconds} - The animation duration in seconds.
         */
        set: function (durationInSeconds) {
            this._animationDuration = durationInSeconds;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Opens the flyout menu.
     * @fires Modal#opened
     */
    MenuFlyout.prototype.open = function () {
        var _this = this;
        if (this._hiddenIndicator && isHidden(this._hiddenIndicator, false) === true) {
            return;
        }
        if (hasClass(this.element, CLASS_OPEN) === true) {
            return;
        }
        addClass(this.element, CLASS_OPEN);
        this._openMenu(this._flyoutElement);
        setTimeout(function () {
            window.addEventListener("click", _this._windowClickHandler);
            window.addEventListener("touchend", _this._windowClickHandler);
        }, 50);
    };
    /**
     * Closes the flyout menu.
     * @fires Modal#closed
     */
    MenuFlyout.prototype.close = function () {
        if (this._hiddenIndicator && isHidden(this._hiddenIndicator, false) === true) {
            return;
        }
        if (hasClass(this.element, CLASS_OPEN) === false) {
            return;
        }
        removeClass(this.element, CLASS_OPEN);
        window.removeEventListener("click", this._windowClickHandler);
        window.removeEventListener("touchend", this._windowClickHandler);
        this._closeMenu(this._flyoutElement);
    };
    /**
     * Toggles the flyout menu.
     * @fires Modal#opened
     * @fires Modal#closed
     */
    MenuFlyout.prototype.toggle = function () {
        if (hasClass(this.element, CLASS_OPEN) === false) {
            this.open();
        }
        else {
            this.close();
        }
    };
    /**
     * Removes all event handlers and clears references.
     */
    MenuFlyout.prototype.destroy = function () {
        this._flyoutElement = null;
        window.removeEventListener("click", this._windowClickHandler);
        window.removeEventListener("touchend", this._windowClickHandler);
        if (this._clickHandler) {
            this.element.removeEventListener("click", this._clickHandler);
        }
        if (this._popperInstance) {
            this._popperInstance.destroy();
            this._popperInstance = undefined;
        }
        this._clickHandler = null;
        this._windowClickHandler = null;
        this.element = null;
    };
    return MenuFlyout;
}(DomElement));
export function init() {
    var e_1, _a;
    var elements = document.querySelectorAll("[data-toggle='flyout']");
    try {
        for (var elements_1 = tslib_1.__values(elements), elements_1_1 = elements_1.next(); !elements_1_1.done; elements_1_1 = elements_1.next()) {
            var e = elements_1_1.value;
            if (e.getAttribute("data-init") === "auto") {
                new MenuFlyout(e);
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (elements_1_1 && !elements_1_1.done && (_a = elements_1.return)) _a.call(elements_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
}
export default MenuFlyout;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL21lbnUvTWVudUZseW91dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE1BQU0sTUFBTSxDQUFBO0FBQ2hELE9BQU8sTUFBTSxNQUFNLFdBQVcsQ0FBQTtBQUU5QixPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUU1RixJQUFNLFVBQVUsR0FBRyxTQUFTLENBQUE7QUFDNUIsSUFBTSxVQUFVLEdBQUcsV0FBVyxDQUFBO0FBQzlCLElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQTtBQUV6QixJQUFNLGNBQWMsR0FBRyxHQUFHLENBQUE7QUFFMUI7O0dBRUc7QUFDSDtJQUF5QixzQ0FBVTtJQWFqQzs7O09BR0c7SUFDSCxvQkFBWSxPQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQU1mO1FBcEJPLHdCQUFrQixHQUFHLGNBQWMsQ0FBQTtRQUVuQyx1QkFBaUIsR0FBRyxLQUFLLENBQUE7UUFjL0IsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUNqRCxLQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUU3RCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7O0lBQ3BCLENBQUM7SUFFRDs7O09BR0c7SUFDTyxnQ0FBVyxHQUFyQjtRQUNFLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQ3pELElBQUksVUFBVSxLQUFLLElBQUksSUFBSSxVQUFVLEtBQUssRUFBRSxFQUFFO1lBRTVDLCtCQUErQjtZQUMvQixPQUFPLENBQUMsS0FBSyxDQUFDLHVGQUF1RixDQUFDLENBQUE7WUFDdEcsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDMUIsOEJBQThCO1lBRTlCLE9BQU07U0FDUDtRQUVELElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQTtTQUM5QjtRQUVELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzNELElBQUksWUFBWSxLQUFLLElBQUksSUFBSSxZQUFZLEtBQUssRUFBRSxFQUFFO1lBQ2hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBZ0IsSUFBSSxTQUFTLENBQUE7U0FDekY7UUFFRCxJQUFJLENBQUMsY0FBYyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFpQixDQUFBO1FBQ3hFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtJQUU1RCxDQUFDO0lBRVMsaUNBQVksR0FBdEI7UUFDRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7SUFDZixDQUFDO0lBRVMsdUNBQWtCLEdBQTVCLFVBQTZCLEtBQWlCO1FBQzVDLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFxQixDQUFBO1FBRXhDLElBQUksZUFBZSxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQy9ELE9BQU8sS0FBSyxDQUFBO1NBQ2I7UUFFRCxPQUFPLE1BQU0sS0FBSyxJQUFJLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxhQUFhLEVBQUU7WUFDdEQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUE7U0FDOUI7UUFFRCxJQUFJLE1BQU0sS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQzNCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUNaLE9BQU8sS0FBSyxDQUFBO1NBQ2I7UUFFRCxPQUFPLElBQUksQ0FBQTtJQUNiLENBQUM7SUFFUyx5Q0FBb0IsR0FBOUI7UUFDRSxPQUFPLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFBO0lBQ2xELENBQUM7SUFFUyw4QkFBUyxHQUFuQixVQUFvQixFQUFXO1FBQzdCLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUE7UUFFMUIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUU7WUFDaEIsT0FBTyxFQUFFLE9BQU87U0FDakIsQ0FBQyxDQUFBO1FBRUYsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssSUFBSSxFQUFFO1lBQ25DLElBQU0sYUFBYSxHQUF5QjtnQkFDMUMsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLFNBQVMsRUFBRTtvQkFDVCxJQUFJLEVBQUU7d0JBQ0osT0FBTyxFQUFFLEtBQUs7cUJBQ2Y7aUJBQ0Y7Z0JBQ0QsYUFBYSxFQUFFLEtBQUs7YUFDckIsQ0FBQTtZQUVELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLGFBQWEsQ0FBQyxDQUFBO1NBQ3BGO1FBRUQsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3hDLFNBQVMsRUFBRSxPQUFLLFVBQVk7WUFDNUIsSUFBSSxFQUFFO2dCQUNKLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87YUFDOUI7U0FDRixDQUFDLENBQUE7UUFFRixvQkFBb0I7UUFDcEIsRUFBRSxDQUFDLFlBQVksQ0FBQyxlQUFlLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBRVMsK0JBQVUsR0FBcEIsVUFBcUIsRUFBVztRQUM5QixTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1FBRTFCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFBO1lBQzlCLElBQUksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFBO1NBQ2pDO1FBRUQsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsY0FBYyxFQUFFO1lBQy9CLFNBQVMsRUFBRSxPQUFLLFVBQVk7WUFDNUIsSUFBSSxFQUFFO2dCQUNKLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87YUFDOUI7WUFDRCxVQUFVLEVBQUU7Z0JBQ1YsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUU7b0JBQ2hCLFVBQVUsRUFBRSxTQUFTO2lCQUN0QixDQUFDLENBQUE7WUFDSixDQUFDO1NBQ0YsQ0FBQyxDQUFBO1FBRUYsb0JBQW9CO1FBQ3BCLEVBQUUsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBRXpDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUE7SUFDOUIsQ0FBQztJQU1ELHNCQUFJLHlDQUFpQjtRQUpyQjs7O1dBR0c7YUFDSCxVQUFzQixpQkFBeUI7WUFDN0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGlCQUFpQixDQUFBO1FBQzdDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0gseUJBQUksR0FBSjtRQUFBLGlCQWdCQztRQWZDLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQzVFLE9BQU07U0FDUDtRQUVELElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQy9DLE9BQU07U0FDUDtRQUVELFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBRW5DLFVBQVUsQ0FBQztZQUNULE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7WUFDMUQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUMvRCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUE7SUFDUixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsMEJBQUssR0FBTDtRQUNFLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFO1lBQzVFLE9BQU07U0FDUDtRQUVELElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLEtBQUssS0FBSyxFQUFFO1lBQ2hELE9BQU07U0FDUDtRQUVELFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBRXJDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFDN0QsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUVoRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQTtJQUN0QyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILDJCQUFNLEdBQU47UUFDRSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUNoRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7U0FDWjthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1NBQ2I7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCw0QkFBTyxHQUFQO1FBQ0csSUFBWSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUE7UUFFbkMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUM3RCxNQUFNLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1FBRWhFLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDOUQ7UUFFRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtZQUM5QixJQUFJLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQTtTQUNqQztRQUVBLElBQVksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLElBQVksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDeEMsSUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7SUFDOUIsQ0FBQztJQWVILGlCQUFDO0FBQUQsQ0FuUEEsQUFtUEMsQ0FuUHdCLFVBQVUsR0FtUGxDO0FBRUQsTUFBTSxVQUFVLElBQUk7O0lBQ2xCLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBOztRQUNsRSxLQUFjLElBQUEsYUFBQSxpQkFBQSxRQUFRLENBQUEsa0NBQUEsd0RBQUU7WUFBbkIsSUFBSSxDQUFDLHFCQUFBO1lBQ1IsSUFBSSxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxLQUFLLE1BQU0sRUFBRTtnQkFDMUMsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDbEI7U0FDRjs7Ozs7Ozs7O0FBQ0gsQ0FBQztBQUVELGVBQWUsVUFBVSxDQUFBIiwiZmlsZSI6Im1haW4vc3JjL21lbnUvTWVudUZseW91dC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFR3ZWVuTGl0ZSwgUG93ZXIxLCBQb3dlcjQgfSBmcm9tIFwiZ3NhcFwiXG5pbXBvcnQgUG9wcGVyIGZyb20gXCJwb3BwZXIuanNcIlxuXG5pbXBvcnQgRG9tRWxlbWVudCBmcm9tIFwiLi4vRG9tRWxlbWVudFwiXG5pbXBvcnQgeyBhZGRDbGFzcywgaGFzQ2xhc3MsIHJlbW92ZUNsYXNzLCBpc0hpZGRlbiwgcGFyZW50V2l0aENsYXNzIH0gZnJvbSBcIi4uL0RvbUZ1bmN0aW9uc1wiXG5cbmNvbnN0IENMQVNTX09QRU4gPSBcImlzLW9wZW5cIlxuY29uc3QgQ0xBU1NfTUVOVSA9IFwianMtZmx5b3V0XCJcbmNvbnN0IENMQVNTX1RBQlMgPSBcInRhYnNcIlxuXG5jb25zdCBBTklNQVRJT05fT1BFTiA9IDAuM1xuXG4vKipcbiAqIEEgY29tcG9uZW50IGZvciB0aGUgZmx5b3V0IG1lbnUuXG4gKi9cbmNsYXNzIE1lbnVGbHlvdXQgZXh0ZW5kcyBEb21FbGVtZW50IHtcbiAgcHJpdmF0ZSBfY2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfd2luZG93Q2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcblxuICBwcml2YXRlIF9hbmltYXRpb25EdXJhdGlvbiA9IEFOSU1BVElPTl9PUEVOXG5cbiAgcHJpdmF0ZSBfZHluYW1pY1BsYWNlbWVudCA9IGZhbHNlXG5cbiAgcHJpdmF0ZSBfaGlkZGVuSW5kaWNhdG9yPzogSFRNTEVsZW1lbnRcbiAgcHJpdmF0ZSBfZmx5b3V0RWxlbWVudCE6IEhUTUxFbGVtZW50XG5cbiAgcHJpdmF0ZSBfcG9wcGVySW5zdGFuY2U/OiBQb3BwZXJcblxuICAvKipcbiAgICogQ3JlYXRlcyBhbmQgaW5pdGlhbGl6ZXMgdGhlIGZseW91dCBjb21wb25lbnQuXG4gICAqIEBwYXJhbSB7RG9tRWxlbWVudH0gLSBUaGUgcm9vdCBlbGVtZW50IG9mIHRoZSBmbHlvdXQgbWVudSBjb21wb25lbnQuXG4gICAqL1xuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBFbGVtZW50KSB7XG4gICAgc3VwZXIoZWxlbWVudClcblxuICAgIHRoaXMuX2NsaWNrSGFuZGxlciA9IHRoaXMuX2hhbmRsZUNsaWNrLmJpbmQodGhpcylcbiAgICB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVXaW5kb3dDbGljay5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgZmx5b3V0IGNvbXBvbmVudC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICBsZXQgZGF0YVRhcmdldCA9IHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKVxuICAgIGlmIChkYXRhVGFyZ2V0ID09PSBudWxsIHx8IGRhdGFUYXJnZXQgPT09IFwiXCIpIHtcblxuICAgICAgLyogdHNsaW50OmRpc2FibGU6bm8tY29uc29sZSAqL1xuICAgICAgY29uc29sZS5lcnJvcihcIkEgZmx5b3V0IG1lbnUgZWxlbWVudCByZXF1aXJlcyBhICdkYXRhLXRhcmdldCcgdGhhdCBzcGVjaWZpZXMgdGhlIGVsZW1lbnQgdG8gY29sbGFwc2VcIilcbiAgICAgIGNvbnNvbGUuaW5mbyh0aGlzLmVsZW1lbnQpXG4gICAgICAvKiB0c2xpbnQ6ZW5hYmxlOm5vLWNvbnNvbGUgKi9cblxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX3VzZUR5bmFtaWNQbGFjZW1lbnQoKSkge1xuICAgICAgdGhpcy5fZHluYW1pY1BsYWNlbWVudCA9IHRydWVcbiAgICB9XG5cbiAgICBsZXQgaGlkZGVuVGFyZ2V0ID0gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtaGlkZGVuXCIpXG4gICAgaWYgKGhpZGRlblRhcmdldCAhPT0gbnVsbCAmJiBoaWRkZW5UYXJnZXQgIT09IFwiXCIpIHtcbiAgICAgIHRoaXMuX2hpZGRlbkluZGljYXRvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaGlkZGVuVGFyZ2V0KSBhcyBIVE1MRWxlbWVudCB8fCB1bmRlZmluZWRcbiAgICB9XG5cbiAgICB0aGlzLl9mbHlvdXRFbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihkYXRhVGFyZ2V0KSEgYXMgSFRNTEVsZW1lbnRcbiAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2NsaWNrSGFuZGxlcilcblxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVDbGljaygpIHtcbiAgICB0aGlzLnRvZ2dsZSgpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZVdpbmRvd0NsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgbGV0IHRhcmdldCA9IGV2ZW50LnRhcmdldCBhcyBIVE1MRWxlbWVudFxuXG4gICAgaWYgKHBhcmVudFdpdGhDbGFzcyh0YXJnZXQsIENMQVNTX01FTlUpID09PSB0aGlzLl9mbHlvdXRFbGVtZW50KSB7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG5cbiAgICB3aGlsZSAodGFyZ2V0ICE9PSB0aGlzLmVsZW1lbnQgJiYgdGFyZ2V0LnBhcmVudEVsZW1lbnQpIHtcbiAgICAgIHRhcmdldCA9IHRhcmdldC5wYXJlbnRFbGVtZW50XG4gICAgfVxuXG4gICAgaWYgKHRhcmdldCAhPT0gdGhpcy5lbGVtZW50KSB7XG4gICAgICB0aGlzLmNsb3NlKClcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cblxuICAgIHJldHVybiB0cnVlXG4gIH1cblxuICBwcm90ZWN0ZWQgX3VzZUR5bmFtaWNQbGFjZW1lbnQoKSB7XG4gICAgcmV0dXJuIHBhcmVudFdpdGhDbGFzcyh0aGlzLmVsZW1lbnQsIENMQVNTX1RBQlMpXG4gIH1cblxuICBwcm90ZWN0ZWQgX29wZW5NZW51KGVsOiBFbGVtZW50KSB7XG4gICAgVHdlZW5MaXRlLmtpbGxUd2VlbnNPZihlbClcblxuICAgIFR3ZWVuTGl0ZS5zZXQoZWwsIHtcbiAgICAgIGRpc3BsYXk6IFwiYmxvY2tcIlxuICAgIH0pXG5cbiAgICBpZiAodGhpcy5fZHluYW1pY1BsYWNlbWVudCA9PT0gdHJ1ZSkge1xuICAgICAgY29uc3QgcG9wcGVyT3B0aW9uczogUG9wcGVyLlBvcHBlck9wdGlvbnMgPSB7XG4gICAgICAgIHBsYWNlbWVudDogXCJib3R0b21cIixcbiAgICAgICAgbW9kaWZpZXJzOiB7XG4gICAgICAgICAgZmxpcDoge1xuICAgICAgICAgICAgZW5hYmxlZDogZmFsc2VcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGV2ZW50c0VuYWJsZWQ6IGZhbHNlXG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3BvcHBlckluc3RhbmNlID0gbmV3IFBvcHBlcih0aGlzLmVsZW1lbnQsIHRoaXMuX2ZseW91dEVsZW1lbnQsIHBvcHBlck9wdGlvbnMpXG4gICAgfVxuXG4gICAgVHdlZW5MaXRlLnRvKGVsLCB0aGlzLl9hbmltYXRpb25EdXJhdGlvbiwge1xuICAgICAgY2xhc3NOYW1lOiBgKz0ke0NMQVNTX09QRU59YCxcbiAgICAgIGVhc2U6IFtcbiAgICAgICAgUG93ZXIxLmVhc2VJbiwgUG93ZXI0LmVhc2VPdXRcbiAgICAgIF1cbiAgICB9KVxuXG4gICAgLy8gc2V0IGFyaWEgZXhwYW5kZWRcbiAgICBlbC5zZXRBdHRyaWJ1dGUoXCJhcmlhLWV4cGFuZGVkXCIsIFwidHJ1ZVwiKVxuXG4gICAgdGhpcy5kaXNwYXRjaEV2ZW50KFwib3BlbmVkXCIpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2Nsb3NlTWVudShlbDogRWxlbWVudCkge1xuICAgIFR3ZWVuTGl0ZS5raWxsVHdlZW5zT2YoZWwpXG5cbiAgICBpZiAodGhpcy5fcG9wcGVySW5zdGFuY2UpIHtcbiAgICAgIHRoaXMuX3BvcHBlckluc3RhbmNlLmRlc3Ryb3koKVxuICAgICAgdGhpcy5fcG9wcGVySW5zdGFuY2UgPSB1bmRlZmluZWRcbiAgICB9XG5cbiAgICBUd2VlbkxpdGUudG8oZWwsIEFOSU1BVElPTl9PUEVOLCB7XG4gICAgICBjbGFzc05hbWU6IGAtPSR7Q0xBU1NfT1BFTn1gLFxuICAgICAgZWFzZTogW1xuICAgICAgICBQb3dlcjEuZWFzZUluLCBQb3dlcjQuZWFzZU91dFxuICAgICAgXSxcbiAgICAgIG9uQ29tcGxldGU6ICgpID0+IHtcbiAgICAgICAgVHdlZW5MaXRlLnNldChlbCwge1xuICAgICAgICAgIGNsZWFyUHJvcHM6IFwiZGlzcGxheVwiXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfSlcblxuICAgIC8vIHNldCBhcmlhIGV4cGFuZGVkXG4gICAgZWwuc2V0QXR0cmlidXRlKFwiYXJpYS1leHBhbmRlZFwiLCBcImZhbHNlXCIpXG5cbiAgICB0aGlzLmRpc3BhdGNoRXZlbnQoXCJjbG9zZWRcIilcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBvcGVuaW5nIGFuaW1hdGlvbiBkdXJhdGlvbi5cbiAgICogQHBhcmFtIHtkdXJhdGlvbkluU2Vjb25kc30gLSBUaGUgYW5pbWF0aW9uIGR1cmF0aW9uIGluIHNlY29uZHMuXG4gICAqL1xuICBzZXQgYW5pbWF0aW9uRHVyYXRpb24oZHVyYXRpb25JblNlY29uZHM6IG51bWJlcikge1xuICAgIHRoaXMuX2FuaW1hdGlvbkR1cmF0aW9uID0gZHVyYXRpb25JblNlY29uZHNcbiAgfVxuXG4gIC8qKlxuICAgKiBPcGVucyB0aGUgZmx5b3V0IG1lbnUuXG4gICAqIEBmaXJlcyBNb2RhbCNvcGVuZWRcbiAgICovXG4gIG9wZW4oKSB7XG4gICAgaWYgKHRoaXMuX2hpZGRlbkluZGljYXRvciAmJiBpc0hpZGRlbih0aGlzLl9oaWRkZW5JbmRpY2F0b3IsIGZhbHNlKSA9PT0gdHJ1ZSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKGhhc0NsYXNzKHRoaXMuZWxlbWVudCwgQ0xBU1NfT1BFTikgPT09IHRydWUpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGFkZENsYXNzKHRoaXMuZWxlbWVudCwgQ0xBU1NfT1BFTilcbiAgICB0aGlzLl9vcGVuTWVudSh0aGlzLl9mbHlvdXRFbGVtZW50KVxuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX3dpbmRvd0NsaWNrSGFuZGxlcilcbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwidG91Y2hlbmRcIiwgdGhpcy5fd2luZG93Q2xpY2tIYW5kbGVyKVxuICAgIH0sIDUwKVxuICB9XG5cbiAgLyoqXG4gICAqIENsb3NlcyB0aGUgZmx5b3V0IG1lbnUuXG4gICAqIEBmaXJlcyBNb2RhbCNjbG9zZWRcbiAgICovXG4gIGNsb3NlKCkge1xuICAgIGlmICh0aGlzLl9oaWRkZW5JbmRpY2F0b3IgJiYgaXNIaWRkZW4odGhpcy5faGlkZGVuSW5kaWNhdG9yLCBmYWxzZSkgPT09IHRydWUpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChoYXNDbGFzcyh0aGlzLmVsZW1lbnQsIENMQVNTX09QRU4pID09PSBmYWxzZSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgcmVtb3ZlQ2xhc3ModGhpcy5lbGVtZW50LCBDTEFTU19PUEVOKVxuXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ0b3VjaGVuZFwiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG5cbiAgICB0aGlzLl9jbG9zZU1lbnUodGhpcy5fZmx5b3V0RWxlbWVudClcbiAgfVxuXG4gIC8qKlxuICAgKiBUb2dnbGVzIHRoZSBmbHlvdXQgbWVudS5cbiAgICogQGZpcmVzIE1vZGFsI29wZW5lZFxuICAgKiBAZmlyZXMgTW9kYWwjY2xvc2VkXG4gICAqL1xuICB0b2dnbGUoKSB7XG4gICAgaWYgKGhhc0NsYXNzKHRoaXMuZWxlbWVudCwgQ0xBU1NfT1BFTikgPT09IGZhbHNlKSB7XG4gICAgICB0aGlzLm9wZW4oKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmNsb3NlKClcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVtb3ZlcyBhbGwgZXZlbnQgaGFuZGxlcnMgYW5kIGNsZWFycyByZWZlcmVuY2VzLlxuICAgKi9cbiAgZGVzdHJveSgpIHtcbiAgICAodGhpcyBhcyBhbnkpLl9mbHlvdXRFbGVtZW50ID0gbnVsbFxuXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ0b3VjaGVuZFwiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG5cbiAgICBpZiAodGhpcy5fY2xpY2tIYW5kbGVyKSB7XG4gICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2NsaWNrSGFuZGxlcilcbiAgICB9XG5cbiAgICBpZiAodGhpcy5fcG9wcGVySW5zdGFuY2UpIHtcbiAgICAgIHRoaXMuX3BvcHBlckluc3RhbmNlLmRlc3Ryb3koKVxuICAgICAgdGhpcy5fcG9wcGVySW5zdGFuY2UgPSB1bmRlZmluZWRcbiAgICB9XG5cbiAgICAodGhpcyBhcyBhbnkpLl9jbGlja0hhbmRsZXIgPSBudWxsO1xuICAgICh0aGlzIGFzIGFueSkuX3dpbmRvd0NsaWNrSGFuZGxlciA9IG51bGw7XG4gICAgKHRoaXMgYXMgYW55KS5lbGVtZW50ID0gbnVsbFxuICB9XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdGhlIGZseW91dCBtZW51IGlzIG9wZW5lZCBieSB0aGUgYW5jaG9yIGxpbmsgb3IgdXNpbmcgdGhlXG4gICAqIHtAbGluayBNZW51Rmx5b3V0I29wZW59IG1ldGhvZC5cbiAgICogQGV2ZW50IE1lbnVGbHlvdXQjb3BlbmVkXG4gICAqIEB0eXBlIHtvYmplY3R9XG4gICAqL1xuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIHRoZSBmbHlvdXQgbWVudSBpcyBjbG9zZWQgYnkgdGhlIHVzZXIgb3IgdXNpbmcgdGhlXG4gICAqIHtAbGluayBNZW51Rmx5b3V0I2Nsb3NlfSBtZXRob2QuXG4gICAqIEBldmVudCBNZW51Rmx5b3V0I2Nsb3NlZFxuICAgKiBAdHlwZSB7b2JqZWN0fVxuICAgKi9cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXQoKSB7XG4gIGxldCBlbGVtZW50cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS10b2dnbGU9J2ZseW91dCddXCIpXG4gIGZvciAobGV0IGUgb2YgZWxlbWVudHMpIHtcbiAgICBpZiAoZS5nZXRBdHRyaWJ1dGUoXCJkYXRhLWluaXRcIikgPT09IFwiYXV0b1wiKSB7XG4gICAgICBuZXcgTWVudUZseW91dChlKVxuICAgIH1cbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBNZW51Rmx5b3V0XG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
