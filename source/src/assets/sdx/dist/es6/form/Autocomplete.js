import * as tslib_1 from "tslib";
import { searchAndInitialize, preventDefault } from "../Utils";
import { empty, addClass, removeClass, hasClass } from "../DomFunctions";
import DomElement from "../DomElement";
import * as Inputs from "../Inputs";
var QUERY_DROPDOWN = ".js-autocomplete";
var CLASS_RESULT = "autocomplete__result";
var CLASS_OPEN = "is-open";
var CLASS_HOVER = "js-hover";
var ATTRIBUTE_VALUE = "data-value";
var TIMEOUT_BLUR = 400;
/**
 * Autocomplete component
 * @fires Autocomplete#change
 */
var Autocomplete = /** @class */ (function (_super) {
    tslib_1.__extends(Autocomplete, _super);
    function Autocomplete(element, configuration) {
        var _this = _super.call(this, element) || this;
        _this._input = _this.element.querySelector("input");
        _this._dropdown = _this.element.querySelector(QUERY_DROPDOWN);
        // Setup event context
        _this._clickHandler = _this._handleClick.bind(_this);
        _this._windowClickHandler = _this._handleWindowClick.bind(_this);
        _this._keyUpHandler = _this._handleKeyUp.bind(_this);
        _this._keyDownHandler = _this._handleKeyDown.bind(_this);
        _this._blurHandler = _this._handleBlur.bind(_this);
        if (configuration) {
            _this._minChars = configuration.minChars;
            _this._source = configuration.source;
        }
        if (!_this._minChars || _this._minChars < 0) {
            _this._minChars = 2;
        }
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the Autocomplete component.
     * @private
     */
    Autocomplete.prototype._initialize = function () {
        this._clearSuggestions();
        if (this._input.getAttribute("disabled")) {
            this.disable();
        }
        else {
            this.enable();
        }
        // Disable browser autofill
        this._input.setAttribute("autocomplete", "off");
    };
    /**
     * The Autocomplete component configuration object
     * @callback Autocomplete~Suggest
     * @property {String} term - The current search term.
     * @property {String[]} matches - The list of matching strings.
     */
    /**
     * The Autocomplete component configuration object
     * @callback Autocomplete~Source
     * @property {String} term - The current search term.
     * @property {Autocomplete~Suggest} suggest - The autocomplete callback function to report the results.
     */
    /**
     * The Autocomplete component configuration object
     * @typedef {Object} Autocomplete~Config
     * @property {Number} minChars - The minimal required characters to start querying for autocomplete matches.
     * @property {Autocomplete~Source} source - The autocomplete source function.
     */
    /**
     * Updates the autocomplete component configuration for the current instance
     * @param {Autocomplete~Config} configuration The configuration object
     */
    Autocomplete.prototype.configure = function (configuration) {
        if (!configuration) {
            return;
        }
        if (configuration.minChars) {
            this._minChars = Math.min(configuration.minChars, 1);
        }
        if (configuration.source) {
            this._source = configuration.source;
        }
        this._clearSuggestions();
    };
    /**
     * Sets the select control to the enabled state.
     */
    Autocomplete.prototype.enable = function () {
        if (!this._input) {
            return;
        }
        this._input.removeAttribute("disabled");
        this._input.addEventListener("keyup", this._keyUpHandler);
        this._input.addEventListener("keydown", this._keyDownHandler);
        this._input.addEventListener("blur", this._blurHandler);
    };
    /**
     * Sets the select control to the disabled state.
     */
    Autocomplete.prototype.disable = function () {
        if (!this._input) {
            return;
        }
        this._input.setAttribute("disabled", "true");
        this._input.removeEventListener("keyup", this._keyUpHandler);
        this._input.removeEventListener("keydown", this._keyDownHandler);
        this._input.removeEventListener("blur", this._blurHandler);
        this.close();
    };
    /**
     * Destroys the component and frees all references.
     */
    Autocomplete.prototype.destroy = function () {
        this.disable();
        this._keyUpHandler = undefined;
        this._keyDownHandler = undefined;
        this._windowClickHandler = undefined;
        this._blurHandler = undefined;
        this._input = undefined;
    };
    /**
     * Closes the suggestions dropdown.
     */
    Autocomplete.prototype.open = function () {
        this._dropdown.addEventListener("click", this._clickHandler);
        window.addEventListener("click", this._windowClickHandler);
        this.addClass(CLASS_OPEN);
    };
    /**
     * Opens the suggestions dropdown.
     */
    Autocomplete.prototype.close = function () {
        this._dropdown.removeEventListener("click", this._clickHandler);
        window.removeEventListener("click", this._windowClickHandler);
        this.removeClass(CLASS_OPEN);
    };
    Object.defineProperty(Autocomplete.prototype, "value", {
        /**
         * Gets the value of the input field.
         * @returns {String} The value of the input field.
         */
        get: function () {
            return this._input.value;
        },
        enumerable: true,
        configurable: true
    });
    Autocomplete.prototype._handleClick = function (event) {
        if (!this._isDropdownTarget(event.target)) {
            return;
        }
        var current = event.target;
        while (current.nodeName !== "LI" && current.parentNode) {
            current = current.parentNode;
        }
        if (current.nodeName === "LI") {
            preventDefault(event);
            this._selectItem(current);
        }
    };
    Autocomplete.prototype._handleBlur = function () {
        var _this = this;
        setTimeout(function () {
            _this.close();
        }, TIMEOUT_BLUR);
    };
    Autocomplete.prototype._handleKeyUp = function (evt) {
        var keycode = evt.which || evt.keyCode;
        if (Inputs.containsKey(keycode, [Inputs.KEY_ARROW_UP, Inputs.KEY_ARROW_DOWN, Inputs.KEY_ENTER, Inputs.KEY_TAB])) {
            // Do not handle these events on keyup
            preventDefault(evt);
            return;
        }
        var target = evt.currentTarget;
        if (evt.currentTarget && target.value && target.value.length >= this._minChars) {
            this._getSuggestion(target.value);
        }
        else {
            this.close();
        }
    };
    Autocomplete.prototype._handleKeyDown = function (evt) {
        var keycode = evt.which || evt.keyCode;
        var isOpen = hasClass(this.element, CLASS_OPEN);
        if (keycode === Inputs.KEY_ESCAPE && isOpen === true) {
            // handle Escape key (ESC)
            this.close();
            preventDefault(evt);
            return;
        }
        if (isOpen === true && Inputs.containsKey(keycode, [Inputs.KEY_ENTER, Inputs.KEY_TAB])) {
            var focusedElement = this._suggestionList.querySelector("." + CLASS_HOVER);
            preventDefault(evt);
            this._selectItem(focusedElement);
            return;
        }
        if (isOpen === true && Inputs.containsKey(keycode, [Inputs.KEY_ARROW_UP, Inputs.KEY_ARROW_DOWN])) {
            // Up and down arrows
            var focusedElement = this._suggestionList.querySelector("." + CLASS_HOVER);
            if (focusedElement) {
                removeClass(focusedElement, CLASS_HOVER);
                var children = Array.prototype.slice.call(this._suggestionList.childNodes);
                var totalNodes = children.length - 1;
                var direction = keycode === Inputs.KEY_ARROW_UP ? -1 : 1;
                var index = children.indexOf(focusedElement);
                index = Math.max(Math.min(index + direction, totalNodes), 0);
                focusedElement = this._suggestionList.childNodes[index];
            }
            else {
                focusedElement = this._suggestionList.querySelector("li");
            }
            addClass(focusedElement, CLASS_HOVER);
            preventDefault(evt);
            return;
        }
    };
    Autocomplete.prototype._handleWindowClick = function (event) {
        if (this._isDropdownTarget(event.target)) {
            return;
        }
        this.close();
    };
    Autocomplete.prototype._selectItem = function (item) {
        if (!item) {
            return;
        }
        var text = item.getAttribute(ATTRIBUTE_VALUE);
        if (text) {
            this._input.value = text;
            // Dispatch the changed event
            this.dispatchEvent("change");
        }
        this.close();
    };
    Autocomplete.prototype._isDropdownTarget = function (target) {
        var current = target;
        while (current !== this._dropdown && current.parentNode) {
            current = current.parentNode;
        }
        return current === this._dropdown;
    };
    Autocomplete.prototype._clearSuggestions = function () {
        // Clear the dropdown item
        empty(this._dropdown);
        this._suggestionList = document.createElement("ul");
        this._dropdown.appendChild(this._suggestionList);
    };
    Autocomplete.prototype._addSuggestion = function (text, term) {
        var sanitizedTerm = term.replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&");
        var html = text.replace(new RegExp("(" + sanitizedTerm + ")", "gi"), "<strong>$1</strong>");
        var textElement = new DomElement("span")
            .setHtml(html);
        var innerElement = new DomElement("div")
            .addClass(CLASS_RESULT)
            .appendChild(textElement);
        var liElement = new DomElement("li")
            .setAttribute(ATTRIBUTE_VALUE, text)
            .appendChild(innerElement);
        this._suggestionList.appendChild(liElement.element);
    };
    Autocomplete.prototype._getSuggestion = function (term) {
        var _this = this;
        if (!this._source) {
            throw new Error("The source function is undefined, cannot load suggestions");
        }
        this._source(term, function (matches, termused) {
            _this._onMatchesReceived(matches, termused);
        });
    };
    Autocomplete.prototype._onMatchesReceived = function (matches, term) {
        var e_1, _a;
        this._clearSuggestions();
        if (!matches || matches.length === 0) {
            this.close();
        }
        else {
            // Clear the dropdown item
            empty(this._suggestionList);
            try {
                for (var matches_1 = tslib_1.__values(matches), matches_1_1 = matches_1.next(); !matches_1_1.done; matches_1_1 = matches_1.next()) {
                    var match = matches_1_1.value;
                    this._addSuggestion(match, term);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (matches_1_1 && !matches_1_1.done && (_a = matches_1.return)) _a.call(matches_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            this.open();
        }
    };
    return Autocomplete;
}(DomElement));
/**
 * Change event
 *
 * @event Autocomplete#change
 * @type {object}
 */
export function init() {
    searchAndInitialize(".input-field--autocomplete", function (e) {
        new Autocomplete(e);
    });
}
export default Autocomplete;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2Zvcm0vQXV0b2NvbXBsZXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLE1BQU0sVUFBVSxDQUFBO0FBQzlELE9BQU8sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN4RSxPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxLQUFLLE1BQU0sTUFBTSxXQUFXLENBQUE7QUFFbkMsSUFBTSxjQUFjLEdBQUcsa0JBQWtCLENBQUE7QUFDekMsSUFBTSxZQUFZLEdBQUcsc0JBQXNCLENBQUE7QUFDM0MsSUFBTSxVQUFVLEdBQUcsU0FBUyxDQUFBO0FBQzVCLElBQU0sV0FBVyxHQUFHLFVBQVUsQ0FBQTtBQUM5QixJQUFNLGVBQWUsR0FBRyxZQUFZLENBQUE7QUFFcEMsSUFBTSxZQUFZLEdBQUcsR0FBRyxDQUFBO0FBY3hCOzs7R0FHRztBQUNIO0lBQTJCLHdDQUF1QjtJQWNoRCxzQkFBWSxPQUFvQixFQUFFLGFBQWtDO1FBQXBFLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBc0JmO1FBcEJDLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFFLENBQUE7UUFDbEQsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQWlCLENBQUE7UUFFM0Usc0JBQXNCO1FBQ3RCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDakQsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDN0QsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUNqRCxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQ3JELEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFFL0MsSUFBSSxhQUFhLEVBQUU7WUFDakIsS0FBSSxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUMsUUFBUSxDQUFBO1lBQ3ZDLEtBQUksQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQTtTQUNwQztRQUVELElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxJQUFJLEtBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxFQUFFO1lBQ3pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO1NBQ25CO1FBRUQsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFBOztJQUNwQixDQUFDO0lBRUQ7OztPQUdHO0lBQ08sa0NBQVcsR0FBckI7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTtRQUV4QixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtTQUNmO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7U0FDZDtRQUVELDJCQUEyQjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUE7SUFDakQsQ0FBQztJQUVEOzs7OztPQUtHO0lBRUg7Ozs7O09BS0c7SUFFSDs7Ozs7T0FLRztJQUVIOzs7T0FHRztJQUNILGdDQUFTLEdBQVQsVUFBVSxhQUFrQztRQUMxQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ2xCLE9BQU07U0FDUDtRQUVELElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTtZQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQTtTQUNyRDtRQUVELElBQUksYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUE7U0FDcEM7UUFFRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTtJQUMxQixDQUFDO0lBRUQ7O09BRUc7SUFDSCw2QkFBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTTtTQUNQO1FBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUE7UUFFdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQ3pELElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUM3RCxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7SUFDekQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsOEJBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU07U0FDUDtRQUVELElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUU1QyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDNUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ2hFLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUUxRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7SUFDZCxDQUFDO0lBRUQ7O09BRUc7SUFDSCw4QkFBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRWQsSUFBWSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7UUFDdkMsSUFBWSxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUM7UUFDekMsSUFBWSxDQUFDLG1CQUFtQixHQUFHLFNBQVMsQ0FBQztRQUM3QyxJQUFZLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQztRQUV0QyxJQUFZLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQTtJQUNsQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCwyQkFBSSxHQUFKO1FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzVELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFFMUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQTtJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFDSCw0QkFBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQy9ELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFFN0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtJQUM5QixDQUFDO0lBTUQsc0JBQUksK0JBQUs7UUFKVDs7O1dBR0c7YUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUE7UUFDMUIsQ0FBQzs7O09BQUE7SUFFUyxtQ0FBWSxHQUF0QixVQUF1QixLQUFpQjtRQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxNQUFjLENBQUMsRUFBRTtZQUNqRCxPQUFNO1NBQ1A7UUFFRCxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBcUIsQ0FBQTtRQUN6QyxPQUFPLE9BQU8sQ0FBQyxRQUFRLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxVQUFVLEVBQUU7WUFDdEQsT0FBTyxHQUFHLE9BQU8sQ0FBQyxVQUF5QixDQUFBO1NBQzVDO1FBRUQsSUFBSSxPQUFPLENBQUMsUUFBUSxLQUFLLElBQUksRUFBRTtZQUM3QixjQUFjLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUMxQjtJQUNILENBQUM7SUFFUyxrQ0FBVyxHQUFyQjtRQUFBLGlCQUlDO1FBSEMsVUFBVSxDQUFDO1lBQ1QsS0FBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1FBQ2QsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFBO0lBQ2xCLENBQUM7SUFFUyxtQ0FBWSxHQUF0QixVQUF1QixHQUFrQjtRQUN2QyxJQUFJLE9BQU8sR0FBRyxHQUFHLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUE7UUFFdEMsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFFLE1BQU0sQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUUsQ0FBQyxFQUFFO1lBQ2pILHNDQUFzQztZQUN0QyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDbkIsT0FBTTtTQUNQO1FBRUQsSUFBTSxNQUFNLEdBQUcsR0FBRyxDQUFDLGFBQWlDLENBQUE7UUFFcEQsSUFBSSxHQUFHLENBQUMsYUFBYSxJQUFJLE1BQU0sQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUM5RSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNsQzthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1NBQ2I7SUFDSCxDQUFDO0lBRVMscUNBQWMsR0FBeEIsVUFBeUIsR0FBa0I7UUFDekMsSUFBSSxPQUFPLEdBQUcsR0FBRyxDQUFDLEtBQUssSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFBO1FBQ3RDLElBQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBRWpELElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxVQUFVLElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtZQUNwRCwwQkFBMEI7WUFDMUIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1lBQ1osY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1lBQ25CLE9BQU07U0FDUDtRQUVELElBQUksTUFBTSxLQUFLLElBQUksSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFFLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBRSxDQUFDLEVBQUU7WUFDeEYsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsTUFBSSxXQUFhLENBQUMsQ0FBQTtZQUUxRSxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQTtZQUNoQyxPQUFNO1NBQ1A7UUFFRCxJQUFJLE1BQU0sS0FBSyxJQUFJLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBRSxNQUFNLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUUsQ0FBQyxFQUFFO1lBQ2xHLHFCQUFxQjtZQUVyQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxNQUFJLFdBQWEsQ0FBRSxDQUFBO1lBQzNFLElBQUksY0FBYyxFQUFFO2dCQUNsQixXQUFXLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxDQUFBO2dCQUV4QyxJQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQWMsQ0FBQTtnQkFFekYsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUE7Z0JBQ3RDLElBQU0sU0FBUyxHQUFHLE9BQU8sS0FBSyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUUxRCxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFBO2dCQUU1QyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxTQUFTLEVBQUUsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7Z0JBQzVELGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQVksQ0FBQTthQUVuRTtpQkFBTTtnQkFDTCxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFZLENBQUE7YUFDckU7WUFFRCxRQUFRLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxDQUFBO1lBQ3JDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNuQixPQUFNO1NBQ1A7SUFDSCxDQUFDO0lBRVMseUNBQWtCLEdBQTVCLFVBQTZCLEtBQWlCO1FBQzVDLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxNQUFjLENBQUMsRUFBRTtZQUNoRCxPQUFNO1NBQ1A7UUFFRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7SUFDZCxDQUFDO0lBRVMsa0NBQVcsR0FBckIsVUFBc0IsSUFBcUI7UUFDekMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULE9BQU07U0FDUDtRQUVELElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDL0MsSUFBSSxJQUFJLEVBQUU7WUFDUixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7WUFFeEIsNkJBQTZCO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUE7U0FDN0I7UUFFRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7SUFDZCxDQUFDO0lBRVMsd0NBQWlCLEdBQTNCLFVBQTRCLE1BQVk7UUFDdEMsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFBO1FBQ3BCLE9BQU8sT0FBTyxLQUFLLElBQUksQ0FBQyxTQUFTLElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUN2RCxPQUFPLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQTtTQUM3QjtRQUVELE9BQU8sT0FBTyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUE7SUFDbkMsQ0FBQztJQUVTLHdDQUFpQixHQUEzQjtRQUNFLDBCQUEwQjtRQUMxQixLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBRXJCLElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUVTLHFDQUFjLEdBQXhCLFVBQXlCLElBQVksRUFBRSxJQUFZO1FBQ2pELElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFDbEUsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFJLGFBQWEsTUFBRyxFQUFFLElBQUksQ0FBQyxFQUFFLHFCQUFxQixDQUFDLENBQUE7UUFFeEYsSUFBTSxXQUFXLEdBQUcsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDO2FBQ3ZDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVoQixJQUFNLFlBQVksR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7YUFDdkMsUUFBUSxDQUFDLFlBQVksQ0FBQzthQUN0QixXQUFXLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFM0IsSUFBTSxTQUFTLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDO2FBQ25DLFlBQVksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDO2FBQ25DLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUU1QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7SUFDckQsQ0FBQztJQUVTLHFDQUFjLEdBQXhCLFVBQXlCLElBQVk7UUFBckMsaUJBUUM7UUFQQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixNQUFNLElBQUksS0FBSyxDQUFDLDJEQUEyRCxDQUFDLENBQUE7U0FDN0U7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFDLE9BQU8sRUFBRSxRQUFRO1lBQ25DLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUE7UUFDNUMsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRVMseUNBQWtCLEdBQTVCLFVBQTZCLE9BQWlCLEVBQUUsSUFBWTs7UUFDMUQsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7UUFFeEIsSUFBSSxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUNwQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7U0FDYjthQUFNO1lBQ0wsMEJBQTBCO1lBQzFCLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7O2dCQUUzQixLQUFrQixJQUFBLFlBQUEsaUJBQUEsT0FBTyxDQUFBLGdDQUFBLHFEQUFFO29CQUF0QixJQUFJLEtBQUssb0JBQUE7b0JBQ1osSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUE7aUJBQ2pDOzs7Ozs7Ozs7WUFFRCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7U0FDWjtJQUNILENBQUM7SUFDSCxtQkFBQztBQUFELENBdFZBLEFBc1ZDLENBdFYwQixVQUFVLEdBc1ZwQztBQUVEOzs7OztHQUtHO0FBRUgsTUFBTSxVQUFVLElBQUk7SUFDbEIsbUJBQW1CLENBQWMsNEJBQTRCLEVBQUUsVUFBQyxDQUFDO1FBQy9ELElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3JCLENBQUMsQ0FBQyxDQUFBO0FBQ0osQ0FBQztBQUVELGVBQWUsWUFBWSxDQUFBIiwiZmlsZSI6Im1haW4vc3JjL2Zvcm0vQXV0b2NvbXBsZXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgc2VhcmNoQW5kSW5pdGlhbGl6ZSwgcHJldmVudERlZmF1bHQgfSBmcm9tIFwiLi4vVXRpbHNcIlxuaW1wb3J0IHsgZW1wdHksIGFkZENsYXNzLCByZW1vdmVDbGFzcywgaGFzQ2xhc3MgfSBmcm9tIFwiLi4vRG9tRnVuY3Rpb25zXCJcbmltcG9ydCBEb21FbGVtZW50IGZyb20gXCIuLi9Eb21FbGVtZW50XCJcbmltcG9ydCAqIGFzIElucHV0cyBmcm9tIFwiLi4vSW5wdXRzXCJcblxuY29uc3QgUVVFUllfRFJPUERPV04gPSBcIi5qcy1hdXRvY29tcGxldGVcIlxuY29uc3QgQ0xBU1NfUkVTVUxUID0gXCJhdXRvY29tcGxldGVfX3Jlc3VsdFwiXG5jb25zdCBDTEFTU19PUEVOID0gXCJpcy1vcGVuXCJcbmNvbnN0IENMQVNTX0hPVkVSID0gXCJqcy1ob3ZlclwiXG5jb25zdCBBVFRSSUJVVEVfVkFMVUUgPSBcImRhdGEtdmFsdWVcIlxuXG5jb25zdCBUSU1FT1VUX0JMVVIgPSA0MDBcblxuZXhwb3J0IGludGVyZmFjZSBTb3VyY2Uge1xuICAoXG4gICAgdGVybTogc3RyaW5nLFxuICAgIGNhbGxiYWNrOiAobWF0Y2hlczogc3RyaW5nW10sIHRlcm11c2VkOiBzdHJpbmcpID0+IHZvaWRcbiAgKTogdm9pZFxufVxuXG5leHBvcnQgaW50ZXJmYWNlIEF1dG9jb21wbGV0ZUNvbmZpZyB7XG4gIG1pbkNoYXJzOiBudW1iZXJcbiAgc291cmNlOiBTb3VyY2Vcbn1cblxuLyoqXG4gKiBBdXRvY29tcGxldGUgY29tcG9uZW50XG4gKiBAZmlyZXMgQXV0b2NvbXBsZXRlI2NoYW5nZVxuICovXG5jbGFzcyBBdXRvY29tcGxldGUgZXh0ZW5kcyBEb21FbGVtZW50PEhUTUxFbGVtZW50PiB7XG4gIHByaXZhdGUgX3NvdXJjZSE6IFNvdXJjZVxuICBwcml2YXRlIF9taW5DaGFycyE6IG51bWJlclxuXG4gIHByaXZhdGUgX2lucHV0OiBIVE1MSW5wdXRFbGVtZW50XG4gIHByaXZhdGUgX3N1Z2dlc3Rpb25MaXN0ITogSFRNTFVMaXN0RWxlbWVudFxuICBwcml2YXRlIF9kcm9wZG93bjogSFRNTEVsZW1lbnRcblxuICBwcml2YXRlIF9jbGlja0hhbmRsZXI6IChldmVudDogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfd2luZG93Q2xpY2tIYW5kbGVyOiAoZXZlbnQ6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2tleVVwSGFuZGxlcjogKGV2ZW50OiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9rZXlEb3duSGFuZGxlcjogKGV2ZW50OiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9ibHVySGFuZGxlcjogKGV2ZW50OiBFdmVudCkgPT4gdm9pZFxuXG4gIGNvbnN0cnVjdG9yKGVsZW1lbnQ6IEhUTUxFbGVtZW50LCBjb25maWd1cmF0aW9uPzogQXV0b2NvbXBsZXRlQ29uZmlnKSB7XG4gICAgc3VwZXIoZWxlbWVudClcblxuICAgIHRoaXMuX2lucHV0ID0gdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCJpbnB1dFwiKSFcbiAgICB0aGlzLl9kcm9wZG93biA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX0RST1BET1dOKSEgYXMgSFRNTEVsZW1lbnRcblxuICAgIC8vIFNldHVwIGV2ZW50IGNvbnRleHRcbiAgICB0aGlzLl9jbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVDbGljay5iaW5kKHRoaXMpXG4gICAgdGhpcy5fd2luZG93Q2xpY2tIYW5kbGVyID0gdGhpcy5faGFuZGxlV2luZG93Q2xpY2suYmluZCh0aGlzKVxuICAgIHRoaXMuX2tleVVwSGFuZGxlciA9IHRoaXMuX2hhbmRsZUtleVVwLmJpbmQodGhpcylcbiAgICB0aGlzLl9rZXlEb3duSGFuZGxlciA9IHRoaXMuX2hhbmRsZUtleURvd24uYmluZCh0aGlzKVxuICAgIHRoaXMuX2JsdXJIYW5kbGVyID0gdGhpcy5faGFuZGxlQmx1ci5iaW5kKHRoaXMpXG5cbiAgICBpZiAoY29uZmlndXJhdGlvbikge1xuICAgICAgdGhpcy5fbWluQ2hhcnMgPSBjb25maWd1cmF0aW9uLm1pbkNoYXJzXG4gICAgICB0aGlzLl9zb3VyY2UgPSBjb25maWd1cmF0aW9uLnNvdXJjZVxuICAgIH1cblxuICAgIGlmICghdGhpcy5fbWluQ2hhcnMgfHwgdGhpcy5fbWluQ2hhcnMgPCAwKSB7XG4gICAgICB0aGlzLl9taW5DaGFycyA9IDJcbiAgICB9XG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgQXV0b2NvbXBsZXRlIGNvbXBvbmVudC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICB0aGlzLl9jbGVhclN1Z2dlc3Rpb25zKClcblxuICAgIGlmICh0aGlzLl9pbnB1dC5nZXRBdHRyaWJ1dGUoXCJkaXNhYmxlZFwiKSkge1xuICAgICAgdGhpcy5kaXNhYmxlKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lbmFibGUoKVxuICAgIH1cblxuICAgIC8vIERpc2FibGUgYnJvd3NlciBhdXRvZmlsbFxuICAgIHRoaXMuX2lucHV0LnNldEF0dHJpYnV0ZShcImF1dG9jb21wbGV0ZVwiLCBcIm9mZlwiKVxuICB9XG5cbiAgLyoqXG4gICAqIFRoZSBBdXRvY29tcGxldGUgY29tcG9uZW50IGNvbmZpZ3VyYXRpb24gb2JqZWN0XG4gICAqIEBjYWxsYmFjayBBdXRvY29tcGxldGV+U3VnZ2VzdFxuICAgKiBAcHJvcGVydHkge1N0cmluZ30gdGVybSAtIFRoZSBjdXJyZW50IHNlYXJjaCB0ZXJtLlxuICAgKiBAcHJvcGVydHkge1N0cmluZ1tdfSBtYXRjaGVzIC0gVGhlIGxpc3Qgb2YgbWF0Y2hpbmcgc3RyaW5ncy5cbiAgICovXG5cbiAgLyoqXG4gICAqIFRoZSBBdXRvY29tcGxldGUgY29tcG9uZW50IGNvbmZpZ3VyYXRpb24gb2JqZWN0XG4gICAqIEBjYWxsYmFjayBBdXRvY29tcGxldGV+U291cmNlXG4gICAqIEBwcm9wZXJ0eSB7U3RyaW5nfSB0ZXJtIC0gVGhlIGN1cnJlbnQgc2VhcmNoIHRlcm0uXG4gICAqIEBwcm9wZXJ0eSB7QXV0b2NvbXBsZXRlflN1Z2dlc3R9IHN1Z2dlc3QgLSBUaGUgYXV0b2NvbXBsZXRlIGNhbGxiYWNrIGZ1bmN0aW9uIHRvIHJlcG9ydCB0aGUgcmVzdWx0cy5cbiAgICovXG5cbiAgLyoqXG4gICAqIFRoZSBBdXRvY29tcGxldGUgY29tcG9uZW50IGNvbmZpZ3VyYXRpb24gb2JqZWN0XG4gICAqIEB0eXBlZGVmIHtPYmplY3R9IEF1dG9jb21wbGV0ZX5Db25maWdcbiAgICogQHByb3BlcnR5IHtOdW1iZXJ9IG1pbkNoYXJzIC0gVGhlIG1pbmltYWwgcmVxdWlyZWQgY2hhcmFjdGVycyB0byBzdGFydCBxdWVyeWluZyBmb3IgYXV0b2NvbXBsZXRlIG1hdGNoZXMuXG4gICAqIEBwcm9wZXJ0eSB7QXV0b2NvbXBsZXRlflNvdXJjZX0gc291cmNlIC0gVGhlIGF1dG9jb21wbGV0ZSBzb3VyY2UgZnVuY3Rpb24uXG4gICAqL1xuXG4gIC8qKlxuICAgKiBVcGRhdGVzIHRoZSBhdXRvY29tcGxldGUgY29tcG9uZW50IGNvbmZpZ3VyYXRpb24gZm9yIHRoZSBjdXJyZW50IGluc3RhbmNlXG4gICAqIEBwYXJhbSB7QXV0b2NvbXBsZXRlfkNvbmZpZ30gY29uZmlndXJhdGlvbiBUaGUgY29uZmlndXJhdGlvbiBvYmplY3RcbiAgICovXG4gIGNvbmZpZ3VyZShjb25maWd1cmF0aW9uPzogQXV0b2NvbXBsZXRlQ29uZmlnKSB7XG4gICAgaWYgKCFjb25maWd1cmF0aW9uKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAoY29uZmlndXJhdGlvbi5taW5DaGFycykge1xuICAgICAgdGhpcy5fbWluQ2hhcnMgPSBNYXRoLm1pbihjb25maWd1cmF0aW9uLm1pbkNoYXJzLCAxKVxuICAgIH1cblxuICAgIGlmIChjb25maWd1cmF0aW9uLnNvdXJjZSkge1xuICAgICAgdGhpcy5fc291cmNlID0gY29uZmlndXJhdGlvbi5zb3VyY2VcbiAgICB9XG5cbiAgICB0aGlzLl9jbGVhclN1Z2dlc3Rpb25zKClcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBzZWxlY3QgY29udHJvbCB0byB0aGUgZW5hYmxlZCBzdGF0ZS5cbiAgICovXG4gIGVuYWJsZSgpIHtcbiAgICBpZiAoIXRoaXMuX2lucHV0KSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICB0aGlzLl9pbnB1dC5yZW1vdmVBdHRyaWJ1dGUoXCJkaXNhYmxlZFwiKVxuXG4gICAgdGhpcy5faW5wdXQuYWRkRXZlbnRMaXN0ZW5lcihcImtleXVwXCIsIHRoaXMuX2tleVVwSGFuZGxlcilcbiAgICB0aGlzLl9pbnB1dC5hZGRFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLl9rZXlEb3duSGFuZGxlcilcbiAgICB0aGlzLl9pbnB1dC5hZGRFdmVudExpc3RlbmVyKFwiYmx1clwiLCB0aGlzLl9ibHVySGFuZGxlcilcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBzZWxlY3QgY29udHJvbCB0byB0aGUgZGlzYWJsZWQgc3RhdGUuXG4gICAqL1xuICBkaXNhYmxlKCkge1xuICAgIGlmICghdGhpcy5faW5wdXQpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIHRoaXMuX2lucHV0LnNldEF0dHJpYnV0ZShcImRpc2FibGVkXCIsIFwidHJ1ZVwiKVxuXG4gICAgdGhpcy5faW5wdXQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleXVwXCIsIHRoaXMuX2tleVVwSGFuZGxlcilcbiAgICB0aGlzLl9pbnB1dC5yZW1vdmVFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLl9rZXlEb3duSGFuZGxlcilcbiAgICB0aGlzLl9pbnB1dC5yZW1vdmVFdmVudExpc3RlbmVyKFwiYmx1clwiLCB0aGlzLl9ibHVySGFuZGxlcilcblxuICAgIHRoaXMuY2xvc2UoKVxuICB9XG5cbiAgLyoqXG4gICAqIERlc3Ryb3lzIHRoZSBjb21wb25lbnQgYW5kIGZyZWVzIGFsbCByZWZlcmVuY2VzLlxuICAgKi9cbiAgZGVzdHJveSgpIHtcbiAgICB0aGlzLmRpc2FibGUoKTtcblxuICAgICh0aGlzIGFzIGFueSkuX2tleVVwSGFuZGxlciA9IHVuZGVmaW5lZDtcbiAgICAodGhpcyBhcyBhbnkpLl9rZXlEb3duSGFuZGxlciA9IHVuZGVmaW5lZDtcbiAgICAodGhpcyBhcyBhbnkpLl93aW5kb3dDbGlja0hhbmRsZXIgPSB1bmRlZmluZWQ7XG4gICAgKHRoaXMgYXMgYW55KS5fYmx1ckhhbmRsZXIgPSB1bmRlZmluZWQ7XG5cbiAgICAodGhpcyBhcyBhbnkpLl9pbnB1dCA9IHVuZGVmaW5lZFxuICB9XG5cbiAgLyoqXG4gICAqIENsb3NlcyB0aGUgc3VnZ2VzdGlvbnMgZHJvcGRvd24uXG4gICAqL1xuICBvcGVuKCkge1xuICAgIHRoaXMuX2Ryb3Bkb3duLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9jbGlja0hhbmRsZXIpXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG5cbiAgICB0aGlzLmFkZENsYXNzKENMQVNTX09QRU4pXG4gIH1cblxuICAvKipcbiAgICogT3BlbnMgdGhlIHN1Z2dlc3Rpb25zIGRyb3Bkb3duLlxuICAgKi9cbiAgY2xvc2UoKSB7XG4gICAgdGhpcy5fZHJvcGRvd24ucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2NsaWNrSGFuZGxlcilcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX3dpbmRvd0NsaWNrSGFuZGxlcilcblxuICAgIHRoaXMucmVtb3ZlQ2xhc3MoQ0xBU1NfT1BFTilcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSB2YWx1ZSBvZiB0aGUgaW5wdXQgZmllbGQuXG4gICAqIEByZXR1cm5zIHtTdHJpbmd9IFRoZSB2YWx1ZSBvZiB0aGUgaW5wdXQgZmllbGQuXG4gICAqL1xuICBnZXQgdmFsdWUoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2lucHV0LnZhbHVlXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgaWYgKCF0aGlzLl9pc0Ryb3Bkb3duVGFyZ2V0KGV2ZW50LnRhcmdldCBhcyBOb2RlKSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgbGV0IGN1cnJlbnQgPSBldmVudC50YXJnZXQgYXMgSFRNTEVsZW1lbnRcbiAgICB3aGlsZSAoY3VycmVudC5ub2RlTmFtZSAhPT0gXCJMSVwiICYmIGN1cnJlbnQucGFyZW50Tm9kZSkge1xuICAgICAgY3VycmVudCA9IGN1cnJlbnQucGFyZW50Tm9kZSBhcyBIVE1MRWxlbWVudFxuICAgIH1cblxuICAgIGlmIChjdXJyZW50Lm5vZGVOYW1lID09PSBcIkxJXCIpIHtcbiAgICAgIHByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgICAgdGhpcy5fc2VsZWN0SXRlbShjdXJyZW50KVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlQmx1cigpIHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMuY2xvc2UoKVxuICAgIH0sIFRJTUVPVVRfQkxVUilcbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlS2V5VXAoZXZ0OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgbGV0IGtleWNvZGUgPSBldnQud2hpY2ggfHwgZXZ0LmtleUNvZGVcblxuICAgIGlmIChJbnB1dHMuY29udGFpbnNLZXkoa2V5Y29kZSwgWyBJbnB1dHMuS0VZX0FSUk9XX1VQLCBJbnB1dHMuS0VZX0FSUk9XX0RPV04sIElucHV0cy5LRVlfRU5URVIsIElucHV0cy5LRVlfVEFCIF0pKSB7XG4gICAgICAvLyBEbyBub3QgaGFuZGxlIHRoZXNlIGV2ZW50cyBvbiBrZXl1cFxuICAgICAgcHJldmVudERlZmF1bHQoZXZ0KVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgY29uc3QgdGFyZ2V0ID0gZXZ0LmN1cnJlbnRUYXJnZXQgYXMgSFRNTElucHV0RWxlbWVudFxuXG4gICAgaWYgKGV2dC5jdXJyZW50VGFyZ2V0ICYmIHRhcmdldC52YWx1ZSAmJiB0YXJnZXQudmFsdWUubGVuZ3RoID49IHRoaXMuX21pbkNoYXJzKSB7XG4gICAgICB0aGlzLl9nZXRTdWdnZXN0aW9uKHRhcmdldC52YWx1ZSlcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jbG9zZSgpXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVLZXlEb3duKGV2dDogS2V5Ym9hcmRFdmVudCkge1xuICAgIGxldCBrZXljb2RlID0gZXZ0LndoaWNoIHx8IGV2dC5rZXlDb2RlXG4gICAgY29uc3QgaXNPcGVuID0gaGFzQ2xhc3ModGhpcy5lbGVtZW50LCBDTEFTU19PUEVOKVxuXG4gICAgaWYgKGtleWNvZGUgPT09IElucHV0cy5LRVlfRVNDQVBFICYmIGlzT3BlbiA9PT0gdHJ1ZSkge1xuICAgICAgLy8gaGFuZGxlIEVzY2FwZSBrZXkgKEVTQylcbiAgICAgIHRoaXMuY2xvc2UoKVxuICAgICAgcHJldmVudERlZmF1bHQoZXZ0KVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKGlzT3BlbiA9PT0gdHJ1ZSAmJiBJbnB1dHMuY29udGFpbnNLZXkoa2V5Y29kZSwgWyBJbnB1dHMuS0VZX0VOVEVSLCBJbnB1dHMuS0VZX1RBQiBdKSkge1xuICAgICAgbGV0IGZvY3VzZWRFbGVtZW50ID0gdGhpcy5fc3VnZ2VzdGlvbkxpc3QucXVlcnlTZWxlY3RvcihgLiR7Q0xBU1NfSE9WRVJ9YClcblxuICAgICAgcHJldmVudERlZmF1bHQoZXZ0KVxuICAgICAgdGhpcy5fc2VsZWN0SXRlbShmb2N1c2VkRWxlbWVudClcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChpc09wZW4gPT09IHRydWUgJiYgSW5wdXRzLmNvbnRhaW5zS2V5KGtleWNvZGUsIFsgSW5wdXRzLktFWV9BUlJPV19VUCwgSW5wdXRzLktFWV9BUlJPV19ET1dOIF0pKSB7XG4gICAgICAvLyBVcCBhbmQgZG93biBhcnJvd3NcblxuICAgICAgbGV0IGZvY3VzZWRFbGVtZW50ID0gdGhpcy5fc3VnZ2VzdGlvbkxpc3QucXVlcnlTZWxlY3RvcihgLiR7Q0xBU1NfSE9WRVJ9YCkhXG4gICAgICBpZiAoZm9jdXNlZEVsZW1lbnQpIHtcbiAgICAgICAgcmVtb3ZlQ2xhc3MoZm9jdXNlZEVsZW1lbnQsIENMQVNTX0hPVkVSKVxuXG4gICAgICAgIGNvbnN0IGNoaWxkcmVuID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwodGhpcy5fc3VnZ2VzdGlvbkxpc3QuY2hpbGROb2RlcykgYXMgRWxlbWVudFtdXG5cbiAgICAgICAgY29uc3QgdG90YWxOb2RlcyA9IGNoaWxkcmVuLmxlbmd0aCAtIDFcbiAgICAgICAgY29uc3QgZGlyZWN0aW9uID0ga2V5Y29kZSA9PT0gSW5wdXRzLktFWV9BUlJPV19VUCA/IC0xIDogMVxuXG4gICAgICAgIGxldCBpbmRleCA9IGNoaWxkcmVuLmluZGV4T2YoZm9jdXNlZEVsZW1lbnQpXG5cbiAgICAgICAgaW5kZXggPSBNYXRoLm1heChNYXRoLm1pbihpbmRleCArIGRpcmVjdGlvbiwgdG90YWxOb2RlcyksIDApXG4gICAgICAgIGZvY3VzZWRFbGVtZW50ID0gdGhpcy5fc3VnZ2VzdGlvbkxpc3QuY2hpbGROb2Rlc1tpbmRleF0gYXMgRWxlbWVudFxuXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb2N1c2VkRWxlbWVudCA9IHRoaXMuX3N1Z2dlc3Rpb25MaXN0LnF1ZXJ5U2VsZWN0b3IoXCJsaVwiKSBhcyBFbGVtZW50XG4gICAgICB9XG5cbiAgICAgIGFkZENsYXNzKGZvY3VzZWRFbGVtZW50LCBDTEFTU19IT1ZFUilcbiAgICAgIHByZXZlbnREZWZhdWx0KGV2dClcbiAgICAgIHJldHVyblxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfaGFuZGxlV2luZG93Q2xpY2soZXZlbnQ6IE1vdXNlRXZlbnQpIHtcbiAgICBpZiAodGhpcy5faXNEcm9wZG93blRhcmdldChldmVudC50YXJnZXQgYXMgTm9kZSkpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIHRoaXMuY2xvc2UoKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9zZWxlY3RJdGVtKGl0ZW0/OiBFbGVtZW50IHwgbnVsbCkge1xuICAgIGlmICghaXRlbSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgY29uc3QgdGV4dCA9IGl0ZW0uZ2V0QXR0cmlidXRlKEFUVFJJQlVURV9WQUxVRSlcbiAgICBpZiAodGV4dCkge1xuICAgICAgdGhpcy5faW5wdXQudmFsdWUgPSB0ZXh0XG5cbiAgICAgIC8vIERpc3BhdGNoIHRoZSBjaGFuZ2VkIGV2ZW50XG4gICAgICB0aGlzLmRpc3BhdGNoRXZlbnQoXCJjaGFuZ2VcIilcbiAgICB9XG5cbiAgICB0aGlzLmNsb3NlKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfaXNEcm9wZG93blRhcmdldCh0YXJnZXQ6IE5vZGUpIHtcbiAgICBsZXQgY3VycmVudCA9IHRhcmdldFxuICAgIHdoaWxlIChjdXJyZW50ICE9PSB0aGlzLl9kcm9wZG93biAmJiBjdXJyZW50LnBhcmVudE5vZGUpIHtcbiAgICAgIGN1cnJlbnQgPSBjdXJyZW50LnBhcmVudE5vZGVcbiAgICB9XG5cbiAgICByZXR1cm4gY3VycmVudCA9PT0gdGhpcy5fZHJvcGRvd25cbiAgfVxuXG4gIHByb3RlY3RlZCBfY2xlYXJTdWdnZXN0aW9ucygpIHtcbiAgICAvLyBDbGVhciB0aGUgZHJvcGRvd24gaXRlbVxuICAgIGVtcHR5KHRoaXMuX2Ryb3Bkb3duKVxuXG4gICAgdGhpcy5fc3VnZ2VzdGlvbkxpc3QgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwidWxcIilcbiAgICB0aGlzLl9kcm9wZG93bi5hcHBlbmRDaGlsZCh0aGlzLl9zdWdnZXN0aW9uTGlzdClcbiAgfVxuXG4gIHByb3RlY3RlZCBfYWRkU3VnZ2VzdGlvbih0ZXh0OiBzdHJpbmcsIHRlcm06IHN0cmluZykge1xuICAgIGNvbnN0IHNhbml0aXplZFRlcm0gPSB0ZXJtLnJlcGxhY2UoL1stXFxcXF4kKis/LigpfFtcXF17fV0vZywgXCJcXFxcJCZcIilcbiAgICBjb25zdCBodG1sID0gdGV4dC5yZXBsYWNlKG5ldyBSZWdFeHAoYCgke3Nhbml0aXplZFRlcm19KWAsIFwiZ2lcIiksIFwiPHN0cm9uZz4kMTwvc3Ryb25nPlwiKVxuXG4gICAgY29uc3QgdGV4dEVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChcInNwYW5cIilcbiAgICAgIC5zZXRIdG1sKGh0bWwpXG5cbiAgICBjb25zdCBpbm5lckVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX1JFU1VMVClcbiAgICAgIC5hcHBlbmRDaGlsZCh0ZXh0RWxlbWVudClcblxuICAgIGNvbnN0IGxpRWxlbWVudCA9IG5ldyBEb21FbGVtZW50KFwibGlcIilcbiAgICAgIC5zZXRBdHRyaWJ1dGUoQVRUUklCVVRFX1ZBTFVFLCB0ZXh0KVxuICAgICAgLmFwcGVuZENoaWxkKGlubmVyRWxlbWVudClcblxuICAgIHRoaXMuX3N1Z2dlc3Rpb25MaXN0LmFwcGVuZENoaWxkKGxpRWxlbWVudC5lbGVtZW50KVxuICB9XG5cbiAgcHJvdGVjdGVkIF9nZXRTdWdnZXN0aW9uKHRlcm06IHN0cmluZykge1xuICAgIGlmICghdGhpcy5fc291cmNlKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJUaGUgc291cmNlIGZ1bmN0aW9uIGlzIHVuZGVmaW5lZCwgY2Fubm90IGxvYWQgc3VnZ2VzdGlvbnNcIilcbiAgICB9XG5cbiAgICB0aGlzLl9zb3VyY2UodGVybSwgKG1hdGNoZXMsIHRlcm11c2VkKSA9PiB7XG4gICAgICB0aGlzLl9vbk1hdGNoZXNSZWNlaXZlZChtYXRjaGVzLCB0ZXJtdXNlZClcbiAgICB9KVxuICB9XG5cbiAgcHJvdGVjdGVkIF9vbk1hdGNoZXNSZWNlaXZlZChtYXRjaGVzOiBzdHJpbmdbXSwgdGVybTogc3RyaW5nKSB7XG4gICAgdGhpcy5fY2xlYXJTdWdnZXN0aW9ucygpXG5cbiAgICBpZiAoIW1hdGNoZXMgfHwgbWF0Y2hlcy5sZW5ndGggPT09IDApIHtcbiAgICAgIHRoaXMuY2xvc2UoKVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBDbGVhciB0aGUgZHJvcGRvd24gaXRlbVxuICAgICAgZW1wdHkodGhpcy5fc3VnZ2VzdGlvbkxpc3QpXG5cbiAgICAgIGZvciAobGV0IG1hdGNoIG9mIG1hdGNoZXMpIHtcbiAgICAgICAgdGhpcy5fYWRkU3VnZ2VzdGlvbihtYXRjaCwgdGVybSlcbiAgICAgIH1cblxuICAgICAgdGhpcy5vcGVuKClcbiAgICB9XG4gIH1cbn1cblxuLyoqXG4gKiBDaGFuZ2UgZXZlbnRcbiAqXG4gKiBAZXZlbnQgQXV0b2NvbXBsZXRlI2NoYW5nZVxuICogQHR5cGUge29iamVjdH1cbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgc2VhcmNoQW5kSW5pdGlhbGl6ZTxIVE1MRWxlbWVudD4oXCIuaW5wdXQtZmllbGQtLWF1dG9jb21wbGV0ZVwiLCAoZSkgPT4ge1xuICAgIG5ldyBBdXRvY29tcGxldGUoZSlcbiAgfSlcbn1cblxuZXhwb3J0IGRlZmF1bHQgQXV0b2NvbXBsZXRlXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
