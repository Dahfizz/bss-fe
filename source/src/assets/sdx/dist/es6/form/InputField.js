import * as tslib_1 from "tslib";
import { searchAndInitialize, remove } from "../Utils";
import DomElement from "../DomElement";
import flatpickr from "flatpickr";
import { Italian } from "flatpickr/dist/l10n/it.js";
import { French } from "flatpickr/dist/l10n/fr.js";
import { German } from "flatpickr/dist/l10n/de.js";
flatpickr.localize(Italian);
flatpickr.localize(French);
flatpickr.localize(German);
var DEFAULTS_FLATPICKR = {
    wrap: true,
    allowInput: true,
    locale: "de",
    dateFormat: "d.m.Y",
    time_24hr: true
};
var CLASS_HAS_VALUE = "is-fixed";
var CLASS_MESSAGE = ".message";
/**
 * Input field component
 */
var InputField = /** @class */ (function (_super) {
    tslib_1.__extends(InputField, _super);
    function InputField(element, datePickerOptions) {
        var _this = _super.call(this, element) || this;
        _this._changedHandler = _this.onValueChanged.bind(_this);
        _this._animationStartHandler = _this._onAnimationStart.bind(_this);
        _this._datePickerOptions = datePickerOptions;
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the input field component.
     * @private
     */
    InputField.prototype._initialize = function () {
        this.element.addEventListener("input", this._changedHandler);
        if (this.element.getAttribute("type") === "password") {
            this.element.addEventListener("animationstart", this._animationStartHandler);
        }
        this._initializeDatePicker();
        this.onValueChanged();
    };
    InputField.prototype._initializeDatePicker = function () {
        var picker = this.element.parentElement;
        if (!picker || !picker.classList.contains("flatpickr")) {
            return;
        }
        if (!this._datePickerOptions) {
            try {
                this._datePickerOptions = JSON.parse(picker.dataset.options || "{}");
            }
            catch (e) {
                this._datePickerOptions = {};
                // tslint:disable-next-line:no-console
                console.warn("_initializeDatePicker JSON.parse failed", picker.dataset.options, e);
            }
        }
        this._flatpickrInstance = flatpickr(picker, Object.assign({}, DEFAULTS_FLATPICKR, this._datePickerOptions));
    };
    InputField.prototype._destroyDatePicker = function () {
        if (this._flatpickrInstance) {
            this._flatpickrInstance.destroy();
        }
    };
    InputField.prototype._onAnimationStart = function (e) {
        if (e.animationName === "onAutoFillStart") {
            this.onValueChanged(true);
        }
    };
    /**
     * Notifies the input field component that it's value has been changed.
     */
    InputField.prototype.onValueChanged = function (force) {
        if (force === void 0) { force = false; }
        if (this.element.value && this.element.value !== "" || force === true) {
            this.addClass(CLASS_HAS_VALUE);
        }
        else {
            this.removeClass(CLASS_HAS_VALUE);
            this.element.value = "";
        }
    };
    /**
     * Destroys the component and frees all references.
     */
    InputField.prototype.destroy = function () {
        this.element.removeEventListener("input", this._changedHandler);
        if (this.element.getAttribute("type") === "password") {
            this.element.removeEventListener("animationstart", this._animationStartHandler);
        }
        this._changedHandler = undefined;
        this._animationStartHandler = undefined;
        this._destroyDatePicker();
    };
    /**
     * Displays the specified error text underneath the input field.
     * @param {text} text The error text/html to display; or undefined to hide the message.
     */
    InputField.prototype.showError = function (text) {
        var message;
        if (this.element.parentElement) {
            var msg_1 = this.element.parentElement.querySelector(CLASS_MESSAGE);
            if (msg_1) {
                message = new DomElement(msg_1);
            }
        }
        if (!text || text === "") {
            if (message) {
                remove(message.element);
            }
            this.removeClass("invalid");
            return;
        }
        this.addClass("invalid");
        if (!message) {
            message = new DomElement("div")
                .addClass("message");
            this.element.parentElement.appendChild(message.element);
        }
        else {
            message.empty();
        }
        var icon = new DomElement("i")
            .addClass("icon")
            .addClass("icon-026-exclamation-mark-circle")
            .setAttribute("aria-hidden", "true");
        var msg = new DomElement("span")
            .setHtml(text);
        message.appendChild(icon);
        message.appendChild(msg);
    };
    return InputField;
}(DomElement));
export function init() {
    searchAndInitialize(".input-field input", function (e) {
        new InputField(e);
    }, function (e) { return e.parentElement; });
}
export default InputField;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2Zvcm0vSW5wdXRGaWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sRUFBRSxNQUFNLFVBQVUsQ0FBQTtBQUN0RCxPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxTQUFTLE1BQU0sV0FBVyxDQUFBO0FBRWpDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQTtBQUNuRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sMkJBQTJCLENBQUE7QUFDbEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLDJCQUEyQixDQUFBO0FBRWxELFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDM0IsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUMxQixTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBRTFCLElBQU0sa0JBQWtCLEdBQUc7SUFDekIsSUFBSSxFQUFFLElBQUk7SUFDVixVQUFVLEVBQUUsSUFBSTtJQUNoQixNQUFNLEVBQUUsSUFBSTtJQUNaLFVBQVUsRUFBRSxPQUFPO0lBQ25CLFNBQVMsRUFBRSxJQUFJO0NBQ2hCLENBQUE7QUFFRCxJQUFNLGVBQWUsR0FBRyxVQUFVLENBQUE7QUFDbEMsSUFBTSxhQUFhLEdBQUcsVUFBVSxDQUFBO0FBRWhDOztHQUVHO0FBQ0g7SUFBeUIsc0NBQTRCO0lBTW5ELG9CQUFZLE9BQXlCLEVBQUUsaUJBQXVCO1FBQTlELFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBTWY7UUFKQyxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQ3JELEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQy9ELEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxpQkFBaUIsQ0FBQTtRQUMzQyxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7O0lBQ3BCLENBQUM7SUFFRDs7O09BR0c7SUFDTyxnQ0FBVyxHQUFyQjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUU1RCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLFVBQVUsRUFBRTtZQUNwRCxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBO1NBQzdFO1FBRUQsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUE7UUFDNUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO0lBQ3ZCLENBQUM7SUFFUywwQ0FBcUIsR0FBL0I7UUFDRSxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQTtRQUN6QyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDdEQsT0FBTTtTQUNQO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixJQUFJO2dCQUNGLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxDQUFBO2FBQ3JFO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQTtnQkFDNUIsc0NBQXNDO2dCQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDLHlDQUF5QyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFBO2FBQ25GO1NBQ0Y7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxrQkFBa0IsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFBO0lBQzdHLENBQUM7SUFFUyx1Q0FBa0IsR0FBNUI7UUFDRSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUMzQixJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFLENBQUE7U0FDbEM7SUFDSCxDQUFDO0lBRVMsc0NBQWlCLEdBQTNCLFVBQTRCLENBQWlCO1FBQzNDLElBQUksQ0FBQyxDQUFDLGFBQWEsS0FBSyxpQkFBaUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFBO1NBQzFCO0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0gsbUNBQWMsR0FBZCxVQUFlLEtBQWE7UUFBYixzQkFBQSxFQUFBLGFBQWE7UUFDMUIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxFQUFFLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtZQUNyRSxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1NBQy9CO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFBO1lBQ2pDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQTtTQUN4QjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNILDRCQUFPLEdBQVA7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFFL0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxVQUFVLEVBQUU7WUFDcEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtTQUNoRjtRQUVBLElBQVksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDO1FBQ3pDLElBQVksQ0FBQyxzQkFBc0IsR0FBRyxTQUFTLENBQUE7UUFFaEQsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUE7SUFDM0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILDhCQUFTLEdBQVQsVUFBVSxJQUFZO1FBQ3BCLElBQUksT0FBTyxDQUFBO1FBQ1gsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRTtZQUM5QixJQUFJLEtBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUE7WUFFakUsSUFBSSxLQUFHLEVBQUU7Z0JBQ1AsT0FBTyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUcsQ0FBQyxDQUFBO2FBQzlCO1NBQ0Y7UUFFRCxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksS0FBSyxFQUFFLEVBQUU7WUFDeEIsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQTthQUN4QjtZQUVELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDM0IsT0FBTTtTQUNQO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUV4QixJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ1osT0FBTyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQztpQkFDNUIsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBRXRCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDekQ7YUFBTTtZQUNMLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtTQUNoQjtRQUVELElBQU0sSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEdBQUcsQ0FBQzthQUM3QixRQUFRLENBQUMsTUFBTSxDQUFDO2FBQ2hCLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQzthQUM1QyxZQUFZLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFBO1FBRXRDLElBQU0sR0FBRyxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQzthQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFaEIsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUN6QixPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQzFCLENBQUM7SUFDSCxpQkFBQztBQUFELENBcElBLEFBb0lDLENBcEl3QixVQUFVLEdBb0lsQztBQUVELE1BQU0sVUFBVSxJQUFJO0lBQ2xCLG1CQUFtQixDQUFtQixvQkFBb0IsRUFBRSxVQUFDLENBQUM7UUFDNUQsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDbkIsQ0FBQyxFQUFFLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLGFBQWMsRUFBaEIsQ0FBZ0IsQ0FBQyxDQUFBO0FBQzdCLENBQUM7QUFFRCxlQUFlLFVBQVUsQ0FBQSIsImZpbGUiOiJtYWluL3NyYy9mb3JtL0lucHV0RmllbGQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzZWFyY2hBbmRJbml0aWFsaXplLCByZW1vdmUgfSBmcm9tIFwiLi4vVXRpbHNcIlxuaW1wb3J0IERvbUVsZW1lbnQgZnJvbSBcIi4uL0RvbUVsZW1lbnRcIlxuaW1wb3J0IGZsYXRwaWNrciBmcm9tIFwiZmxhdHBpY2tyXCJcblxuaW1wb3J0IHsgSXRhbGlhbiB9IGZyb20gXCJmbGF0cGlja3IvZGlzdC9sMTBuL2l0LmpzXCJcbmltcG9ydCB7IEZyZW5jaCB9IGZyb20gXCJmbGF0cGlja3IvZGlzdC9sMTBuL2ZyLmpzXCJcbmltcG9ydCB7IEdlcm1hbiB9IGZyb20gXCJmbGF0cGlja3IvZGlzdC9sMTBuL2RlLmpzXCJcblxuZmxhdHBpY2tyLmxvY2FsaXplKEl0YWxpYW4pXG5mbGF0cGlja3IubG9jYWxpemUoRnJlbmNoKVxuZmxhdHBpY2tyLmxvY2FsaXplKEdlcm1hbilcblxuY29uc3QgREVGQVVMVFNfRkxBVFBJQ0tSID0ge1xuICB3cmFwOiB0cnVlLCAvLyBlbmFibGUgY2FsZW5kYXIgdG9nZ2xlIGljb25cbiAgYWxsb3dJbnB1dDogdHJ1ZSwgLy8gZG9uJ3Qgc2V0IGlucHV0IHRvIHJlYWRvbmx5XG4gIGxvY2FsZTogXCJkZVwiLCAvLyBHZXJtYW4gaXMgZGVmYXVsdFxuICBkYXRlRm9ybWF0OiBcImQubS5ZXCIsIC8vIDE1LjAxLjIwMTdcbiAgdGltZV8yNGhyOiB0cnVlXG59XG5cbmNvbnN0IENMQVNTX0hBU19WQUxVRSA9IFwiaXMtZml4ZWRcIlxuY29uc3QgQ0xBU1NfTUVTU0FHRSA9IFwiLm1lc3NhZ2VcIlxuXG4vKipcbiAqIElucHV0IGZpZWxkIGNvbXBvbmVudFxuICovXG5jbGFzcyBJbnB1dEZpZWxkIGV4dGVuZHMgRG9tRWxlbWVudDxIVE1MSW5wdXRFbGVtZW50PiB7XG4gIHByaXZhdGUgX2NoYW5nZWRIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfYW5pbWF0aW9uU3RhcnRIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfZmxhdHBpY2tySW5zdGFuY2U6IGFueVxuICBwcml2YXRlIF9kYXRlUGlja2VyT3B0aW9uczogYW55XG5cbiAgY29uc3RydWN0b3IoZWxlbWVudDogSFRNTElucHV0RWxlbWVudCwgZGF0ZVBpY2tlck9wdGlvbnM/OiBhbnkpIHtcbiAgICBzdXBlcihlbGVtZW50KVxuXG4gICAgdGhpcy5fY2hhbmdlZEhhbmRsZXIgPSB0aGlzLm9uVmFsdWVDaGFuZ2VkLmJpbmQodGhpcylcbiAgICB0aGlzLl9hbmltYXRpb25TdGFydEhhbmRsZXIgPSB0aGlzLl9vbkFuaW1hdGlvblN0YXJ0LmJpbmQodGhpcylcbiAgICB0aGlzLl9kYXRlUGlja2VyT3B0aW9ucyA9IGRhdGVQaWNrZXJPcHRpb25zXG4gICAgdGhpcy5faW5pdGlhbGl6ZSgpXG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZXMgdGhlIGlucHV0IGZpZWxkIGNvbXBvbmVudC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImlucHV0XCIsIHRoaXMuX2NoYW5nZWRIYW5kbGVyKVxuXG4gICAgaWYgKHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJ0eXBlXCIpID09PSBcInBhc3N3b3JkXCIpIHtcbiAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiYW5pbWF0aW9uc3RhcnRcIiwgdGhpcy5fYW5pbWF0aW9uU3RhcnRIYW5kbGVyKVxuICAgIH1cblxuICAgIHRoaXMuX2luaXRpYWxpemVEYXRlUGlja2VyKClcbiAgICB0aGlzLm9uVmFsdWVDaGFuZ2VkKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZURhdGVQaWNrZXIoKSB7XG4gICAgY29uc3QgcGlja2VyID0gdGhpcy5lbGVtZW50LnBhcmVudEVsZW1lbnRcbiAgICBpZiAoIXBpY2tlciB8fCAhcGlja2VyLmNsYXNzTGlzdC5jb250YWlucyhcImZsYXRwaWNrclwiKSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGlmICghdGhpcy5fZGF0ZVBpY2tlck9wdGlvbnMpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHRoaXMuX2RhdGVQaWNrZXJPcHRpb25zID0gSlNPTi5wYXJzZShwaWNrZXIuZGF0YXNldC5vcHRpb25zIHx8IFwie31cIilcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgdGhpcy5fZGF0ZVBpY2tlck9wdGlvbnMgPSB7fVxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tY29uc29sZVxuICAgICAgICBjb25zb2xlLndhcm4oXCJfaW5pdGlhbGl6ZURhdGVQaWNrZXIgSlNPTi5wYXJzZSBmYWlsZWRcIiwgcGlja2VyLmRhdGFzZXQub3B0aW9ucywgZSlcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5fZmxhdHBpY2tySW5zdGFuY2UgPSBmbGF0cGlja3IocGlja2VyLCBPYmplY3QuYXNzaWduKHt9LCBERUZBVUxUU19GTEFUUElDS1IsIHRoaXMuX2RhdGVQaWNrZXJPcHRpb25zKSlcbiAgfVxuXG4gIHByb3RlY3RlZCBfZGVzdHJveURhdGVQaWNrZXIoKSB7XG4gICAgaWYgKHRoaXMuX2ZsYXRwaWNrckluc3RhbmNlKSB7XG4gICAgICB0aGlzLl9mbGF0cGlja3JJbnN0YW5jZS5kZXN0cm95KClcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX29uQW5pbWF0aW9uU3RhcnQoZTogQW5pbWF0aW9uRXZlbnQpIHtcbiAgICBpZiAoZS5hbmltYXRpb25OYW1lID09PSBcIm9uQXV0b0ZpbGxTdGFydFwiKSB7XG4gICAgICB0aGlzLm9uVmFsdWVDaGFuZ2VkKHRydWUpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE5vdGlmaWVzIHRoZSBpbnB1dCBmaWVsZCBjb21wb25lbnQgdGhhdCBpdCdzIHZhbHVlIGhhcyBiZWVuIGNoYW5nZWQuXG4gICAqL1xuICBvblZhbHVlQ2hhbmdlZChmb3JjZSA9IGZhbHNlKSB7XG4gICAgaWYgKHRoaXMuZWxlbWVudC52YWx1ZSAmJiB0aGlzLmVsZW1lbnQudmFsdWUgIT09IFwiXCIgfHwgZm9yY2UgPT09IHRydWUpIHtcbiAgICAgIHRoaXMuYWRkQ2xhc3MoQ0xBU1NfSEFTX1ZBTFVFKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJlbW92ZUNsYXNzKENMQVNTX0hBU19WQUxVRSlcbiAgICAgIHRoaXMuZWxlbWVudC52YWx1ZSA9IFwiXCJcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogRGVzdHJveXMgdGhlIGNvbXBvbmVudCBhbmQgZnJlZXMgYWxsIHJlZmVyZW5jZXMuXG4gICAqL1xuICBkZXN0cm95KCkge1xuICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiaW5wdXRcIiwgdGhpcy5fY2hhbmdlZEhhbmRsZXIpXG5cbiAgICBpZiAodGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcInR5cGVcIikgPT09IFwicGFzc3dvcmRcIikge1xuICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJhbmltYXRpb25zdGFydFwiLCB0aGlzLl9hbmltYXRpb25TdGFydEhhbmRsZXIpXG4gICAgfVxuXG4gICAgKHRoaXMgYXMgYW55KS5fY2hhbmdlZEhhbmRsZXIgPSB1bmRlZmluZWQ7XG4gICAgKHRoaXMgYXMgYW55KS5fYW5pbWF0aW9uU3RhcnRIYW5kbGVyID0gdW5kZWZpbmVkXG5cbiAgICB0aGlzLl9kZXN0cm95RGF0ZVBpY2tlcigpXG4gIH1cblxuICAvKipcbiAgICogRGlzcGxheXMgdGhlIHNwZWNpZmllZCBlcnJvciB0ZXh0IHVuZGVybmVhdGggdGhlIGlucHV0IGZpZWxkLlxuICAgKiBAcGFyYW0ge3RleHR9IHRleHQgVGhlIGVycm9yIHRleHQvaHRtbCB0byBkaXNwbGF5OyBvciB1bmRlZmluZWQgdG8gaGlkZSB0aGUgbWVzc2FnZS5cbiAgICovXG4gIHNob3dFcnJvcih0ZXh0OiBzdHJpbmcpIHtcbiAgICBsZXQgbWVzc2FnZVxuICAgIGlmICh0aGlzLmVsZW1lbnQucGFyZW50RWxlbWVudCkge1xuICAgICAgbGV0IG1zZyA9IHRoaXMuZWxlbWVudC5wYXJlbnRFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoQ0xBU1NfTUVTU0FHRSlcblxuICAgICAgaWYgKG1zZykge1xuICAgICAgICBtZXNzYWdlID0gbmV3IERvbUVsZW1lbnQobXNnKVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmICghdGV4dCB8fCB0ZXh0ID09PSBcIlwiKSB7XG4gICAgICBpZiAobWVzc2FnZSkge1xuICAgICAgICByZW1vdmUobWVzc2FnZS5lbGVtZW50KVxuICAgICAgfVxuXG4gICAgICB0aGlzLnJlbW92ZUNsYXNzKFwiaW52YWxpZFwiKVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgdGhpcy5hZGRDbGFzcyhcImludmFsaWRcIilcblxuICAgIGlmICghbWVzc2FnZSkge1xuICAgICAgbWVzc2FnZSA9IG5ldyBEb21FbGVtZW50KFwiZGl2XCIpXG4gICAgICAgIC5hZGRDbGFzcyhcIm1lc3NhZ2VcIilcblxuICAgICAgdGhpcy5lbGVtZW50LnBhcmVudEVsZW1lbnQhLmFwcGVuZENoaWxkKG1lc3NhZ2UuZWxlbWVudClcbiAgICB9IGVsc2Uge1xuICAgICAgbWVzc2FnZS5lbXB0eSgpXG4gICAgfVxuXG4gICAgY29uc3QgaWNvbiA9IG5ldyBEb21FbGVtZW50KFwiaVwiKVxuICAgICAgLmFkZENsYXNzKFwiaWNvblwiKVxuICAgICAgLmFkZENsYXNzKFwiaWNvbi0wMjYtZXhjbGFtYXRpb24tbWFyay1jaXJjbGVcIilcbiAgICAgIC5zZXRBdHRyaWJ1dGUoXCJhcmlhLWhpZGRlblwiLCBcInRydWVcIilcblxuICAgIGNvbnN0IG1zZyA9IG5ldyBEb21FbGVtZW50KFwic3BhblwiKVxuICAgICAgLnNldEh0bWwodGV4dClcblxuICAgIG1lc3NhZ2UuYXBwZW5kQ2hpbGQoaWNvbilcbiAgICBtZXNzYWdlLmFwcGVuZENoaWxkKG1zZylcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgc2VhcmNoQW5kSW5pdGlhbGl6ZTxIVE1MSW5wdXRFbGVtZW50PihcIi5pbnB1dC1maWVsZCBpbnB1dFwiLCAoZSkgPT4ge1xuICAgIG5ldyBJbnB1dEZpZWxkKGUpXG4gIH0sIChlKSA9PiBlLnBhcmVudEVsZW1lbnQhKVxufVxuXG5leHBvcnQgZGVmYXVsdCBJbnB1dEZpZWxkXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
