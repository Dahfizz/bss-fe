import * as tslib_1 from "tslib";
import { searchAndInitialize, preventDefault, find, remove, msIEVersion, scrollIntoView } from "../Utils";
import DomElement from "../DomElement";
import * as Inputs from "../Inputs";
import * as Dom from "../DomFunctions";
var CLASS_PLACEHOLDER = "select__placeholder";
var CLASS_THUMB = "select__thumb";
var CLASS_BUTTON = "select__button";
var CLASS_DROPDOWN = "select__dropdown";
var CLASS_OPEN = "select--open";
var CLASS_CLOSED = "select--closed";
var CLASS_DISABLED = "select--disabled";
var CLASS_FILTERABLE = "select--filterable";
var CLASS_ITEM = "dropdown-item";
var CLASS_ITEM_SELECTED = "dropdown-item--selected";
var CLASS_ITEM_FOCUSED = "dropdown-item--focused";
var CLASS_ITEM_DISABLED = "dropdown-item--disabled";
var CLASS_GROUP_ITEM = "dropdown-group";
var CLASS_GROUP_HEADER = "dropdown-group__item";
var QUERY_MESSAGE = ".message";
var TIMEOUT_CLOSE = 150;
var TIMEOUT_BLUR = 400;
/**
 * The select component API.
 */
var Select = /** @class */ (function (_super) {
    tslib_1.__extends(Select, _super);
    function Select(element) {
        var _this = _super.call(this, element) || this;
        // Minimum filter length
        _this._minFilterLength = 2;
        // The options the Select was initially created upon
        // These will be used as a basis for filtering
        _this._initialOptions = Array.prototype.slice.call(_this.element.children);
        _this._openByFocus = false;
        // Check for multi-selection
        _this._multiselection = _this.element.hasAttribute("multiple") === true;
        // Setup event context
        _this._clickHandler = _this._handleClick.bind(_this);
        _this._handleDropdownClick = _this._handleClick.bind(_this);
        _this._keydownHandler = _this._handleKeydown.bind(_this);
        _this._focusHandler = _this._handleFocus.bind(_this);
        _this._blurHandler = _this._handleBlur.bind(_this);
        _this._windowClickHandler = _this._handleWindowClick.bind(_this);
        _this._filterKeydownHandler = _this._handleFilterKeydown.bind(_this);
        _this._filterKeyupHandler = _this._handleFilterKeyup.bind(_this);
        _this._filterFocusHandler = _this._handleFilterFocus.bind(_this);
        _this._initialize();
        return _this;
    }
    /**
     * Initializes the select component.
     *
     * This method inspects the select definition and its options and
     * generates new stylable DOM elements around the original select-element
     * definitions.
     * @private
     */
    Select.prototype._initialize = function () {
        var e_1, _a;
        var selectedOption = this.element.querySelector("option[selected]");
        var firstOption = this.element.querySelector("option");
        // Per default, set the last selected option to either the option with a "selected" attribute,
        // or, if not found, to the first available option
        this._lastSelectedOption = selectedOption || firstOption;
        this._wrapperElement = new DomElement(this.element.parentElement)
            .addClass(CLASS_CLOSED);
        try {
            for (var _b = tslib_1.__values(this.classes), _c = _b.next(); !_c.done; _c = _b.next()) {
                var cls = _c.value;
                this._wrapperElement.addClass(cls);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this._dropdownElement = new DomElement("div")
            .addClass(CLASS_DROPDOWN);
        if (msIEVersion() > 0 && msIEVersion() < 12) {
            // This is a workaround for IE browsers 11 and earlier where focusing
            // a scrollable dropdown list will close the dropdown prematurely.
            this._dropdownElement.element.addEventListener("mousedown", function (event) { return event.preventDefault(); });
        }
        this._setupTarget();
        this._setupPlaceholder();
        this._wrapperElement.appendChild(this._dropdownElement);
        this._createOptions(this.element);
        this._updateSize();
        this._updateMessage();
        if (this.element.disabled) {
            this.disable();
        }
        else {
            this.enable();
        }
    };
    Select.prototype._setupTarget = function () {
        // move the id from the select element to the wrapper
        var id = this.element.getAttribute("id");
        if (id) {
            this.element.removeAttribute("id");
            this._wrapperElement.setAttribute("id", id);
        }
        // Apply the tab index
        var tabIndex = this.element.getAttribute("tabindex");
        if (tabIndex) {
            this._wrapperElement.setAttribute("tabIndex", tabIndex);
        }
    };
    Select.prototype._setupPlaceholder = function () {
        var _this = this;
        if (!this._selectButtonElement) {
            this._selectButtonElement = new DomElement("div")
                .addClass(CLASS_BUTTON);
            this._wrapperElement.appendChild(this._selectButtonElement);
        }
        if (!this._thumbElement) {
            this._thumbElement = new DomElement("div")
                .addClass(CLASS_THUMB);
            var thumbIcon = new DomElement("div")
                .addClass("thumb-icon");
            var loader = new DomElement("div")
                .addClass("loader-spinner")
                .addClass("loader-spinner--small");
            this._thumbElement.appendChild(loader);
            this._thumbElement.appendChild(thumbIcon);
            this._selectButtonElement.appendChild(this._thumbElement);
        }
        var placeholderText = "";
        this._placeholderOption = this.element.querySelector("option[selected][disabled]") || undefined;
        if (this._placeholderOption) {
            placeholderText = Dom.text(this._placeholderOption);
            if (this._multiselection === true) {
                this._placeholderOption.selected = false;
            }
        }
        var selectedOption = this.element.querySelector("option[selected]:not([disabled])");
        if (selectedOption) {
            placeholderText = Dom.text(selectedOption);
        }
        if (!this._placeholderElement) {
            // When the Select is filterable, create an "input" as the placeholder element, otherwise a "span"
            if (this._isFilterable()) {
                this._placeholderElement = new DomElement("input");
                this._placeholderElement.addEventListener("keyup", function (e) { return _this._handleFilterKeyup(e); });
                this._placeholderElement.addEventListener("keydown", function (e) { return _this._handleFilterKeydown(e); });
                this._placeholderElement.addEventListener("focus", function (e) { return _this._handleFilterFocus(e); });
            }
            else {
                this._placeholderElement = new DomElement("span");
            }
            this._placeholderElement.addClass(CLASS_PLACEHOLDER);
            this._selectButtonElement.appendChild(this._placeholderElement);
        }
        this._setPlaceholder(placeholderText);
        this._placeholderText = placeholderText;
        if (selectedOption && selectedOption !== this._placeholderOption) {
            this._updatePlaceholder(true);
        }
    };
    Select.prototype._updateMessage = function () {
        var messageNode = this._wrapperElement.element.querySelector(QUERY_MESSAGE);
        if (messageNode !== null) {
            this._wrapperElement.appendChild(new DomElement(messageNode));
        }
    };
    Select.prototype._isOptGroup = function (element) {
        return element.tagName.toUpperCase() === "OPTGROUP";
    };
    Select.prototype._isOption = function (element) {
        return element.tagName.toUpperCase() === "OPTION";
    };
    Select.prototype._createOptions = function (element) {
        for (var i = 0; i < element.children.length; i++) {
            var child = element.children[i];
            if (this._isOptGroup(child)) {
                this._appendGroup(child);
            }
            if (this._isOption(child)) {
                var option = this._createOption(child);
                if (option) {
                    this._dropdownElement.appendChild(option);
                }
            }
        }
    };
    Select.prototype._createOption = function (option) {
        var html = option.innerHTML;
        if (this._activeFilter) {
            var sanitizedActiveFilter = this._activeFilter.replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&");
            html = html.replace(new RegExp("(" + sanitizedActiveFilter + ")", "gi"), "<strong>$1</strong>");
        }
        var opt = new DomElement("div")
            .addClass(CLASS_ITEM)
            .setHtml(html);
        if (option.selected) {
            opt.addClass(CLASS_ITEM_SELECTED);
        }
        if (option.disabled) {
            opt.addClass(CLASS_ITEM_DISABLED);
        }
        if (!this._isPlaceholder(option)) {
            opt.setAttribute("data-value", option.value);
            return opt;
        }
        return undefined;
    };
    Select.prototype._appendGroup = function (optgroup) {
        var e_2, _a;
        var label = optgroup.getAttribute("label");
        var group = new DomElement("div")
            .addClass(CLASS_GROUP_ITEM);
        var groupHeader = new DomElement("div")
            .addClass(CLASS_GROUP_HEADER)
            .setHtml(label);
        group.appendChild(groupHeader);
        var options = optgroup.querySelectorAll("option");
        try {
            for (var options_1 = tslib_1.__values(options), options_1_1 = options_1.next(); !options_1_1.done; options_1_1 = options_1.next()) {
                var entry = options_1_1.value;
                var option = this._createOption(entry);
                if (option) {
                    group.appendChild(option);
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (options_1_1 && !options_1_1.done && (_a = options_1.return)) _a.call(options_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        this._dropdownElement.appendChild(group);
        return group;
    };
    Select.prototype._updateSize = function () {
        var e_3, _a;
        // Note: Mirroring the DOM and measuring the items using their clientWidth was very
        // unreliable, therefore measuring was switched to the new HTML5 measureText method
        // margins and paddings arround the text are copied from the original placeholder items
        // dimension
        var placeholderStyle = window.getComputedStyle(this._placeholderElement.element);
        var paddingRight = parseFloat(placeholderStyle.paddingRight);
        var paddingLeft = parseFloat(placeholderStyle.paddingLeft);
        var font = this._placeholderElement.css("font");
        var textWidth = Dom.textWidth(this._placeholderText, font);
        var maxWidth = paddingLeft + paddingRight + textWidth;
        var options = this._wrapperElement.element.querySelectorAll("." + CLASS_ITEM);
        try {
            for (var options_2 = tslib_1.__values(options), options_2_1 = options_2.next(); !options_2_1.done; options_2_1 = options_2.next()) {
                var entry = options_2_1.value;
                var width = Dom.textWidth(Dom.text(entry), font) + paddingLeft + paddingRight;
                if (width > maxWidth) {
                    maxWidth = width;
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (options_2_1 && !options_2_1.done && (_a = options_2.return)) _a.call(options_2);
            }
            finally { if (e_3) throw e_3.error; }
        }
    };
    Select.prototype._isButtonTarget = function (target) {
        return (target === this._wrapperElement.element ||
            target === this._placeholderElement.element ||
            target === this._selectButtonElement.element ||
            target === this._thumbElement.element);
    };
    Select.prototype._isDropdownTarget = function (target) {
        var current = target;
        while (current !== this._dropdownElement.element && current.parentElement) {
            current = current.parentElement;
        }
        return current === this._dropdownElement.element;
    };
    /**
     * Updates the UI if the selection has changed and makes sure the
     * select control and the generated markup are synchronized.
     * @private
     */
    Select.prototype._selectedItemChanged = function (newItem, autoClose, multiselect) {
        var _this = this;
        if (autoClose === void 0) { autoClose = true; }
        if (multiselect === void 0) { multiselect = false; }
        var oldItems = this._dropdownElement.element.querySelectorAll("." + CLASS_ITEM_SELECTED);
        if (!newItem) {
            setTimeout(function () { return _this.close(); }, TIMEOUT_CLOSE);
            return;
        }
        if (Dom.hasClass(newItem, CLASS_ITEM_DISABLED)) {
            return;
        }
        if ((oldItems.length === 0) && !newItem) {
            throw new Error("Can not select undefined elements");
        }
        var oldItem = oldItems[0];
        if (multiselect === true) {
            oldItem = find(oldItems, function (x) { return x.getAttribute("data-value") === newItem.getAttribute("data-value"); });
        }
        var isDeselect = false;
        if (newItem && oldItem && oldItem === newItem) {
            // Click on a previously selected element -> deselect
            isDeselect = true;
            if (!this._placeholderOption && !multiselect) {
                // If there is no placeholder option, non multiselect options cannot be deselected
                return;
            }
            delete this._lastSelectedOption;
        }
        if (oldItem) {
            // Remove selection on the element
            var oldValue_1 = oldItem.getAttribute("data-value");
            var optElement = find(this.element.options, function (x) { return !x.disabled && x.value === oldValue_1; });
            if (!optElement) {
                throw new Error("The option with value " + oldValue_1 + " does not exist");
            }
            // Unset Select value
            optElement.selected = false;
            Dom.removeClass(oldItem, CLASS_ITEM_SELECTED);
        }
        if (!isDeselect) { // Select an option
            // Select a new item
            var newValue_1 = newItem.getAttribute("data-value");
            var optElement = find(this.element.options, function (x) { return !x.disabled && x.value === newValue_1; });
            if (!optElement) {
                throw new Error("The option with value " + newValue_1 + " does not exist");
            }
            // Set Select value
            optElement.selected = true;
            Dom.addClass(newItem, CLASS_ITEM_SELECTED);
            // Preserve selection
            this._lastSelectedOption = optElement;
        }
        else { // Deselect an option
            // Keep track of falling back to the placeholder (if any)
            if (this._placeholderOption) {
                this._lastSelectedOption = this._placeholderOption;
            }
        }
        var hasSelectedItems = true;
        if (this._multiselection === false && isDeselect) {
            // Handle no selection for non-multiselect states
            this._placeholderOption.selected = true;
            hasSelectedItems = false;
        }
        if (this._multiselection === true && this._getSelectedOptions().length === 0) {
            hasSelectedItems = false;
        }
        // Reset the filter if filterable
        if (this._activeFilter) {
            this._clearFilter();
        }
        this._updatePlaceholder(hasSelectedItems);
        // Dispatch the changed event
        this.dispatchEvent("change");
        if (autoClose && !multiselect) {
            setTimeout(function () {
                _this.close();
            }, TIMEOUT_CLOSE);
        }
    };
    Select.prototype._updatePlaceholder = function (hasSelectedItems) {
        var e_4, _a;
        var text = this._placeholderOption ? Dom.text(this._placeholderOption) : " ";
        if (hasSelectedItems === true) {
            var selectedItems = this._getSelectedOptions();
            if (selectedItems.length > 0) {
                text = "";
                try {
                    for (var selectedItems_1 = tslib_1.__values(selectedItems), selectedItems_1_1 = selectedItems_1.next(); !selectedItems_1_1.done; selectedItems_1_1 = selectedItems_1.next()) {
                        var item = selectedItems_1_1.value;
                        text += Dom.text(item) + ", ";
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (selectedItems_1_1 && !selectedItems_1_1.done && (_a = selectedItems_1.return)) _a.call(selectedItems_1);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
                text = text.substring(0, text.length - 2);
            }
        }
        this._setPlaceholder(text);
    };
    Select.prototype._getSelectedOptions = function () {
        var selectedOptions = [];
        if (this.element.options) {
            [].forEach.call(this.element.options, (function (option) {
                if (option.selected && !option.disabled) {
                    selectedOptions.push(option);
                }
            }));
        }
        return selectedOptions;
    };
    /**
     * Clone all of the initially set options (and optgroups) and returns them in a new array.
     * This serves as the basis for filtering. If a filter is present, it will be respected.
     */
    Select.prototype.getInitialOptions = function () {
        var filter = this._activeFilter || "";
        var filtered = [];
        var initialOptions = this._initialOptions;
        for (var i = 0; i < initialOptions.length; i++) {
            var child = initialOptions[i];
            if (this._isOptGroup(child)) { // handle <optgroup>
                var optGroupClone = child.cloneNode(false);
                var found = false;
                for (var j = 0; j < child.children.length; j++) {
                    var optionClone = child.children[j].cloneNode(true);
                    // Append on match
                    if (this._containsWord(optionClone.innerHTML, filter)) {
                        optGroupClone.appendChild(optionClone);
                        found = true;
                    }
                }
                // Push if any matches found
                if (found) {
                    filtered.push(optGroupClone);
                }
            }
            else if (this._isOption(child)) { // handle <option>
                var optionClone = child.cloneNode(true);
                // Push on match
                if (this._containsWord(optionClone.innerHTML, filter)) {
                    filtered.push(optionClone);
                }
            }
        }
        return filtered;
    };
    /**
     * Returns true if a text contains a given keyword, e.g. in "ca" in "Car"
     */
    Select.prototype._containsWord = function (text, keyword) {
        return text.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
    };
    Select.prototype._handleFocus = function () {
        var _this = this;
        this.open();
        this._openByFocus = true;
        setTimeout(function () {
            _this._openByFocus = false;
        }, TIMEOUT_BLUR);
    };
    Select.prototype._handleBlur = function () {
        this.close();
    };
    Select.prototype._handleClick = function (event) {
        var handled = false;
        if (this._lastHandledEvent === event) {
            this._lastHandledEvent = undefined;
            return;
        }
        if (this._isButtonTarget(event.target) && this._openByFocus === false) {
            // handle header item clicks and toggle dropdown
            this.toggle();
            handled = true;
        }
        var newItem = event.target;
        if (!handled && Dom.hasClass(newItem, CLASS_ITEM)) {
            // handle clicks on dropdown items
            this._selectedItemChanged(newItem, true, this._multiselection);
            handled = true;
        }
        if (handled) {
            this._lastHandledEvent = event;
            preventDefault(event);
        }
    };
    Select.prototype._handleWindowClick = function (event) {
        if (this._isDropdownTarget(event.target) || this._isButtonTarget(event.target)) {
            return;
        }
        this.close();
    };
    Select.prototype._focusOptionStartingWith = function (keycode, startIndex, options) {
        for (var index = startIndex; index < options.length; index++) {
            var item = new DomElement(options[index]);
            var value = item.innerText.toLowerCase();
            if (index > options.length) {
                index = 0;
            }
            if (value.startsWith(Inputs.getKeyValue(keycode))) {
                var newOption = new DomElement(options[index]);
                if (!newOption.hasClass(CLASS_ITEM_DISABLED)) {
                    scrollIntoView(options[index]);
                    newOption.addClass(CLASS_ITEM_FOCUSED);
                    return newOption;
                }
            }
        }
        return undefined;
    };
    Select.prototype._handleKeydown = function (event) {
        var evt = event || window.event;
        var keycode = event.which || event.keyCode;
        if (keycode === Inputs.KEY_ESCAPE) {
            // handle Escape key (ESC)
            if (this.isOpen()) {
                this.close();
            }
            evt.preventDefault();
            return;
        }
        if (keycode === Inputs.KEY_ARROW_UP || keycode === Inputs.KEY_ARROW_DOWN) {
            // Up and down arrows
            var options = this._wrapperElement.element.querySelectorAll("." + CLASS_ITEM);
            if (options.length > 0) {
                var newIndex = 0;
                var oldOption = void 0;
                var focusedElement = this._wrapperElement.find("." + CLASS_ITEM_FOCUSED);
                var searchFor = focusedElement ? CLASS_ITEM_FOCUSED : CLASS_ITEM_SELECTED;
                var newElement = void 0;
                for (var index = 0; index < options.length; index++) {
                    var direction = keycode === Inputs.KEY_ARROW_DOWN ? 1 : -1;
                    var item = new DomElement(options[index]);
                    // search for selected or focusedElement elements
                    if (item.hasClass(searchFor)) {
                        oldOption = item;
                        newIndex = index;
                        // get the next not disabled element in the appropriate direction
                        for (var count = 0; count < options.length; count++) {
                            newIndex += direction;
                            newIndex %= options.length;
                            if (newIndex < 0) {
                                newIndex = options.length - 1;
                            }
                            newElement = new DomElement(options[newIndex]);
                            if (!newElement.hasClass(CLASS_ITEM_DISABLED)) {
                                break;
                            }
                        }
                    }
                }
                // set the new element focused
                scrollIntoView(options[newIndex]);
                var newOption = new DomElement(options[newIndex]);
                newOption.addClass(CLASS_ITEM_FOCUSED);
                if (oldOption) {
                    oldOption.removeClass(CLASS_ITEM_FOCUSED);
                }
            }
            evt.preventDefault();
            return;
        }
        if (Inputs.getKeyValue(keycode) && !this._isFilterable()) {
            // Keyboard keys
            var options = this._wrapperElement.element.querySelectorAll("." + CLASS_ITEM);
            if (options.length > 0) {
                var oldFocusIndex = 0;
                var hasFocusedOption = false;
                for (var index = 0; index < options.length; index++) {
                    var item = new DomElement(options[index]);
                    if (item.hasClass(CLASS_ITEM_FOCUSED)) {
                        item.removeClass(CLASS_ITEM_FOCUSED);
                        var value = item.innerText.toLowerCase();
                        if (value.startsWith(Inputs.getKeyValue(keycode))) {
                            hasFocusedOption = true;
                            oldFocusIndex = index;
                        }
                    }
                }
                var newOption = this._focusOptionStartingWith(keycode, hasFocusedOption ? oldFocusIndex + 1 : 0, options);
                if (newOption === undefined) {
                    this._focusOptionStartingWith(keycode, 0, options);
                }
            }
            evt.preventDefault();
            return;
        }
        if (keycode === Inputs.KEY_ENTER || keycode === Inputs.KEY_TAB) {
            // Handle enter and tab key by selecting the currently focused element
            var newItem = this._dropdownElement.element.querySelector("." + CLASS_ITEM_FOCUSED);
            this._selectedItemChanged(newItem, true, this._multiselection);
        }
    };
    /**
     * Fired when the user presses a key in the filter field
     */
    Select.prototype._handleFilterKeydown = function (e) {
        var keycode = e.which || e.keyCode;
        // If the user hits the enter key while filtering and there's a single match, select it
        if (keycode === Inputs.KEY_ENTER) {
            var dropdownElements = this._dropdownElement.element.querySelectorAll("." + CLASS_ITEM);
            if (dropdownElements.length === 1) {
                this._selectedItemChanged(dropdownElements[0], true, this._multiselection);
                e.stopPropagation();
            }
        }
    };
    /**
     * Fired when the user releases a key in the filter field
     */
    Select.prototype._handleFilterKeyup = function (e) {
        var target = e.target;
        // Filter has changed
        if (target.value !== this._activeFilter && target.value !== this._placeholderText && target.value !== this._lastSelectedOption.innerHTML) {
            this._setFilter(target.value);
        }
    };
    /**
     * Fired when the user focusses the filter input field
     */
    Select.prototype._handleFilterFocus = function (e) {
        var target = e.target;
        setTimeout(function () {
            target.select();
        });
    };
    /**
     * Filters the Select by a given filter keyword
     * @param filter Keyword to filter by
     */
    Select.prototype._setFilter = function (filter) {
        if (filter === void 0) { filter = ""; }
        this._activeFilter = (filter.length >= this._minFilterLength) ? filter : "";
        this.setOptions(this.getInitialOptions());
    };
    /**
     * Resets the filter
     */
    Select.prototype._clearFilter = function () {
        delete this._activeFilter;
        this.setOptions(this.getInitialOptions());
    };
    /**
     * Set new content and reload the Select
     * @param elements Array of new option (or optgroup) elements to display
     */
    Select.prototype.setOptions = function (options) {
        var _this = this;
        this._emptyNode(this.element);
        options.forEach(function (option) {
            _this.element.appendChild(option);
        });
        // Preserve selected value if the selected
        this.element.value = this._lastSelectedOption.value;
        this.reload();
    };
    /**
     * Clear all children of a given node
     * @param node Node
     */
    Select.prototype._emptyNode = function (node) {
        while (node.firstChild) {
            node.removeChild(node.firstChild);
        }
    };
    /**
     * Returns whether an option is a placeholder option
     */
    Select.prototype._isPlaceholder = function (option) {
        return option.hasAttribute("disabled") && option.hasAttribute("selected");
    };
    /**
     * Update placeholder value
     * @param text Content of the placeholder
     */
    Select.prototype._setPlaceholder = function (text) {
        if (this._placeholderElement) {
            if (this._isFilterable()) {
                this._placeholderElement.element.value = text;
            }
            else {
                this._placeholderElement.setHtml(text);
            }
        }
    };
    Object.defineProperty(Select.prototype, "value", {
        /**
         * Gets the value of the currently selected option.
         * If multiple selection is enabled this property returns an array of values.
         */
        get: function () {
            if (this._multiselection) {
                return this._getSelectedOptions().map(function (x) { return x.value; });
            }
            if (this.element.value === "") {
                return null;
            }
            return this.element.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Select.prototype, "disabled", {
        /**
         * Enables or disables the select component depending on the
         * 'value' parameter.
         * @param {value} If true disables the control; false enables it.
         */
        set: function (value) {
            if (value) {
                this.disable();
            }
            else {
                this.enable();
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Reloads the dropdown's option data definitions from the DOM and updates
     * the generated dropdown display items.
     */
    Select.prototype.reload = function () {
        // Remove all existing child elements
        this._emptyNode(this._dropdownElement.element);
        if (this._activeFilter === undefined) { // If the user is filtering, let the placeholder "input" alive
            this._setupPlaceholder();
        }
        this._createOptions(this.element);
        this._updateSize();
        this._updateMessage();
        if (!this._isFilterable()) {
            this._updatePlaceholder(!!this.value);
        }
    };
    /**
     * Sets the select control to the enabled state.
     */
    Select.prototype.enable = function () {
        this.element.removeAttribute("disabled");
        this._wrapperElement.removeClass(CLASS_DISABLED);
        window.addEventListener("click", this._windowClickHandler);
        this._wrapperElement.element.addEventListener("click", this._clickHandler);
        this._wrapperElement.element.addEventListener("keydown", this._keydownHandler);
        this._wrapperElement.element.addEventListener("focus", this._focusHandler);
        this._wrapperElement.element.addEventListener("blur", this._blurHandler);
    };
    /**
     * Sets the select control to the disabled state.
     */
    Select.prototype.disable = function () {
        this.element.setAttribute("disabled", "");
        this._wrapperElement.addClass(CLASS_DISABLED);
        window.removeEventListener("click", this._windowClickHandler);
        this._wrapperElement.element.removeEventListener("click", this._clickHandler);
        this._wrapperElement.element.removeEventListener("keydown", this._keydownHandler);
        this._wrapperElement.element.removeEventListener("focus", this._focusHandler);
        this._wrapperElement.element.removeEventListener("blur", this._blurHandler);
        this.close();
    };
    /**
     * Toggles the open/closed state of the select dropdown.
     */
    Select.prototype.toggle = function () {
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    };
    /**
     * Gets if the select dropdown is open or closed.
     * @return {boolean} True if open; otherwise false.
     */
    Select.prototype.isOpen = function () {
        return this._wrapperElement.hasClass(CLASS_OPEN);
    };
    /**
     * Opens the select dropdown.
     */
    Select.prototype.open = function () {
        if (!this.isOpen()) {
            this._openByFocus = false;
            this._wrapperElement.removeClass(CLASS_CLOSED);
            this._wrapperElement.addClass(CLASS_OPEN);
            this._dropdownElement.element.addEventListener("click", this._handleDropdownClick);
            this._dropdownElement.element.addEventListener("tap", this._handleDropdownClick);
        }
    };
    /**
     * Closes the select dropdown.
     */
    Select.prototype.close = function () {
        if (this.isOpen()) {
            this._openByFocus = false;
            this._wrapperElement.removeClass(CLASS_OPEN);
            this._wrapperElement.addClass(CLASS_CLOSED);
            // If the Select is filterable and therefore has an input field,
            // reset the value of it to the chosen option
            if (this._isFilterable()) {
                // Unfocus input field
                this._placeholderElement.element.blur();
                if (!this._activeFilter || this._activeFilter === this._lastSelectedOption.innerHTML) {
                    this._setPlaceholder(this._lastSelectedOption.innerHTML);
                }
            }
            this._dropdownElement.element.removeEventListener("click", this._handleDropdownClick);
            this._dropdownElement.element.removeEventListener("tap", this._handleDropdownClick);
            var focusedItem = this._wrapperElement.find("." + CLASS_ITEM_FOCUSED);
            if (focusedItem) {
                focusedItem.removeClass(CLASS_ITEM_FOCUSED);
            }
        }
    };
    /**
     * Returns true when the element has the filter modifier class
     */
    Select.prototype._isFilterable = function () {
        return this._wrapperElement.hasClass(CLASS_FILTERABLE);
    };
    /**
     * Destroys the component and clears all references.
     */
    Select.prototype.destroy = function () {
        window.removeEventListener("click", this._windowClickHandler);
        if (this._dropdownElement) {
            this._dropdownElement.element.removeEventListener("click", this._handleDropdownClick);
            this._dropdownElement.element.removeEventListener("tap", this._handleDropdownClick);
            remove(this._dropdownElement.element);
            this._dropdownElement = undefined;
        }
        if (this._placeholderElement) {
            this._placeholderElement.removeEventListener("keydown", this._filterKeydownHandler);
            this._placeholderElement.removeEventListener("keyup", this._filterKeyupHandler);
            this._placeholderElement.removeEventListener("focus", this._filterFocusHandler);
        }
        if (this._wrapperElement) {
            this._wrapperElement.element.removeEventListener("click", this._clickHandler);
            this._wrapperElement.element.removeEventListener("keydown", this._keydownHandler);
            this._wrapperElement.element.removeEventListener("focus", this._focusHandler);
            this._wrapperElement.element.removeEventListener("blur", this._blurHandler);
            this._wrapperElement = undefined;
        }
        if (this._selectButtonElement) {
            remove(this._selectButtonElement.element);
            this._selectButtonElement = undefined;
        }
        this.removeClass(CLASS_CLOSED);
    };
    return Select;
}(DomElement));
export function init() {
    searchAndInitialize("select", function (e) {
        new Select(e);
    });
}
export default Select;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2Zvcm0vU2VsZWN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxNQUFNLFVBQVUsQ0FBQTtBQUN6RyxPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxLQUFLLE1BQU0sTUFBTSxXQUFXLENBQUE7QUFDbkMsT0FBTyxLQUFLLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQTtBQUV0QyxJQUFNLGlCQUFpQixHQUFHLHFCQUFxQixDQUFBO0FBQy9DLElBQU0sV0FBVyxHQUFHLGVBQWUsQ0FBQTtBQUNuQyxJQUFNLFlBQVksR0FBRyxnQkFBZ0IsQ0FBQTtBQUNyQyxJQUFNLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQTtBQUV6QyxJQUFNLFVBQVUsR0FBRyxjQUFjLENBQUE7QUFDakMsSUFBTSxZQUFZLEdBQUcsZ0JBQWdCLENBQUE7QUFDckMsSUFBTSxjQUFjLEdBQUcsa0JBQWtCLENBQUE7QUFDekMsSUFBTSxnQkFBZ0IsR0FBRyxvQkFBb0IsQ0FBQTtBQUU3QyxJQUFNLFVBQVUsR0FBRyxlQUFlLENBQUE7QUFDbEMsSUFBTSxtQkFBbUIsR0FBRyx5QkFBeUIsQ0FBQTtBQUNyRCxJQUFNLGtCQUFrQixHQUFHLHdCQUF3QixDQUFBO0FBQ25ELElBQU0sbUJBQW1CLEdBQUcseUJBQXlCLENBQUE7QUFFckQsSUFBTSxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQTtBQUN6QyxJQUFNLGtCQUFrQixHQUFHLHNCQUFzQixDQUFBO0FBRWpELElBQU0sYUFBYSxHQUFHLFVBQVUsQ0FBQTtBQUVoQyxJQUFNLGFBQWEsR0FBRyxHQUFHLENBQUE7QUFDekIsSUFBTSxZQUFZLEdBQUcsR0FBRyxDQUFBO0FBRXhCOztHQUVHO0FBQ0g7SUFBcUIsa0NBQTZCO0lBb0NoRCxnQkFBWSxPQUEwQjtRQUF0QyxZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQW1CZjtRQTlCRCx3QkFBd0I7UUFDaEIsc0JBQWdCLEdBQUcsQ0FBQyxDQUFBO1FBSzVCLG9EQUFvRDtRQUNwRCw4Q0FBOEM7UUFDdEMscUJBQWUsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUt6RSxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQTtRQUV6Qiw0QkFBNEI7UUFDNUIsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUE7UUFFckUsc0JBQXNCO1FBQ3RCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDakQsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQ3hELEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDckQsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUNqRCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQy9DLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQzdELEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQ2pFLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQzdELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBRTdELEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTs7SUFDcEIsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDTyw0QkFBVyxHQUFyQjs7UUFDRSxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBc0IsQ0FBQTtRQUMxRixJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQXNCLENBQUE7UUFFN0UsOEZBQThGO1FBQzlGLGtEQUFrRDtRQUNsRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsY0FBYyxJQUFJLFdBQVcsQ0FBQTtRQUV4RCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYyxDQUFDO2FBQy9ELFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQTs7WUFFekIsS0FBZ0IsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxPQUFPLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQXpCLElBQUksR0FBRyxXQUFBO2dCQUNWLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQ25DOzs7Ozs7Ozs7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxVQUFVLENBQWMsS0FBSyxDQUFDO2FBQ3ZELFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQTtRQUUzQixJQUFJLFdBQVcsRUFBRSxHQUFHLENBQUMsSUFBSSxXQUFXLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDM0MscUVBQXFFO1lBQ3JFLGtFQUFrRTtZQUNsRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxVQUFDLEtBQWlCLElBQUssT0FBQSxLQUFLLENBQUMsY0FBYyxFQUFFLEVBQXRCLENBQXNCLENBQUMsQ0FBQTtTQUMzRztRQUVELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQUNuQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTtRQUV4QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUV2RCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUVqQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDbEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBRXJCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDekIsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO1NBQ2Y7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNkO0lBQ0gsQ0FBQztJQUVTLDZCQUFZLEdBQXRCO1FBQ0UscURBQXFEO1FBQ3JELElBQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzFDLElBQUksRUFBRSxFQUFFO1lBQ04sSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDbEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQzVDO1FBRUQsc0JBQXNCO1FBQ3RCLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBQ3RELElBQUksUUFBUSxFQUFFO1lBQ1osSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFBO1NBQ3hEO0lBQ0gsQ0FBQztJQUVTLGtDQUFpQixHQUEzQjtRQUFBLGlCQStEQztRQTlEQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzlCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7aUJBQzlDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUV6QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtTQUM1RDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO2lCQUN2QyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUE7WUFFeEIsSUFBSSxTQUFTLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO2lCQUNsQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUE7WUFFekIsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO2lCQUMvQixRQUFRLENBQUMsZ0JBQWdCLENBQUM7aUJBQzFCLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO1lBRXBDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ3RDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBQ3pDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQzFEO1FBRUQsSUFBSSxlQUFlLEdBQUcsRUFBRSxDQUFBO1FBRXhCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyw0QkFBNEIsQ0FBc0IsSUFBSSxTQUFTLENBQUE7UUFFcEgsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDM0IsZUFBZSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUE7WUFFbkQsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksRUFBRTtnQkFDakMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7YUFDekM7U0FDRjtRQUVELElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGtDQUFrQyxDQUFDLENBQUE7UUFFbkYsSUFBSSxjQUFjLEVBQUU7WUFDbEIsZUFBZSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7U0FDM0M7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzdCLGtHQUFrRztZQUNsRyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFBO2dCQUNsRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFVBQUMsQ0FBQyxJQUFLLE9BQUEsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxFQUExQixDQUEwQixDQUFDLENBQUE7Z0JBQ3JGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLEVBQTVCLENBQTRCLENBQUMsQ0FBQTtnQkFDekYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxVQUFDLENBQUMsSUFBSyxPQUFBLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFBO2FBQ3RGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQTthQUNsRDtZQUVELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtZQUNwRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1NBQ2hFO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUNyQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFBO1FBRXZDLElBQUksY0FBYyxJQUFJLGNBQWMsS0FBSyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDaEUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFBO1NBQzlCO0lBQ0gsQ0FBQztJQUVTLCtCQUFjLEdBQXhCO1FBQ0UsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzdFLElBQUksV0FBVyxLQUFLLElBQUksRUFBRTtZQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFBO1NBQzlEO0lBQ0gsQ0FBQztJQUVPLDRCQUFXLEdBQW5CLFVBQW9CLE9BQWdCO1FBQ2xDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSyxVQUFVLENBQUE7SUFDckQsQ0FBQztJQUVPLDBCQUFTLEdBQWpCLFVBQWtCLE9BQWdCO1FBQ2hDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLENBQUE7SUFDbkQsQ0FBQztJQUVTLCtCQUFjLEdBQXhCLFVBQXlCLE9BQTBCO1FBQ2pELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNoRCxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBRS9CLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUE0QixDQUFDLENBQUE7YUFDaEQ7WUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBMEIsQ0FBQyxDQUFBO2dCQUUzRCxJQUFJLE1BQU0sRUFBRTtvQkFDVixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO2lCQUMxQzthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBRVMsOEJBQWEsR0FBdkIsVUFBd0IsTUFBeUI7UUFDL0MsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQTtRQUUzQixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBTSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsRUFBRSxNQUFNLENBQUMsQ0FBQTtZQUN4RixJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFJLHFCQUFxQixNQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUUscUJBQXFCLENBQUMsQ0FBQTtTQUMzRjtRQUVELElBQUksR0FBRyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQzthQUM1QixRQUFRLENBQUMsVUFBVSxDQUFDO2FBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVoQixJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDbkIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1NBQ2xDO1FBRUQsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ25CLEdBQUcsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtTQUNsQztRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2hDLEdBQUcsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUM1QyxPQUFPLEdBQUcsQ0FBQTtTQUNYO1FBRUQsT0FBTyxTQUFTLENBQUE7SUFDbEIsQ0FBQztJQUVTLDZCQUFZLEdBQXRCLFVBQXVCLFFBQTZCOztRQUNsRCxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBRSxDQUFBO1FBRTNDLElBQUksS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQzthQUM5QixRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUU3QixJQUFJLFdBQVcsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUM7YUFDcEMsUUFBUSxDQUFDLGtCQUFrQixDQUFDO2FBQzVCLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUVqQixLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRTlCLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQTs7WUFDakQsS0FBa0IsSUFBQSxZQUFBLGlCQUFBLE9BQU8sQ0FBQSxnQ0FBQSxxREFBRTtnQkFBdEIsSUFBSSxLQUFLLG9CQUFBO2dCQUNaLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBQ3RDLElBQUksTUFBTSxFQUFFO29CQUNWLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7aUJBQzFCO2FBQ0Y7Ozs7Ozs7OztRQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDeEMsT0FBTyxLQUFLLENBQUE7SUFDZCxDQUFDO0lBRVMsNEJBQVcsR0FBckI7O1FBQ0UsbUZBQW1GO1FBQ25GLG1GQUFtRjtRQUNuRix1RkFBdUY7UUFDdkYsWUFBWTtRQUNaLElBQU0sZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUVsRixJQUFJLFlBQVksR0FBRyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsWUFBYSxDQUFDLENBQUE7UUFDN0QsSUFBSSxXQUFXLEdBQUcsVUFBVSxDQUFDLGdCQUFnQixDQUFDLFdBQVksQ0FBQyxDQUFBO1FBRTNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDL0MsSUFBSSxTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDMUQsSUFBSSxRQUFRLEdBQUcsV0FBVyxHQUFHLFlBQVksR0FBRyxTQUFTLENBQUE7UUFFckQsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsTUFBSSxVQUFZLENBQUMsQ0FBQTs7WUFDN0UsS0FBa0IsSUFBQSxZQUFBLGlCQUFBLE9BQU8sQ0FBQSxnQ0FBQSxxREFBRTtnQkFBdEIsSUFBSSxLQUFLLG9CQUFBO2dCQUNaLElBQUksS0FBSyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxXQUFXLEdBQUcsWUFBWSxDQUFBO2dCQUU3RSxJQUFJLEtBQUssR0FBRyxRQUFRLEVBQUU7b0JBQ3BCLFFBQVEsR0FBRyxLQUFLLENBQUE7aUJBQ2pCO2FBQ0Y7Ozs7Ozs7OztJQUVILENBQUM7SUFFUyxnQ0FBZSxHQUF6QixVQUEwQixNQUFtQjtRQUMzQyxPQUFPLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTztZQUM3QyxNQUFNLEtBQUssSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU87WUFDM0MsTUFBTSxLQUFLLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPO1lBQzVDLE1BQU0sS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQzFDLENBQUM7SUFFUyxrQ0FBaUIsR0FBM0IsVUFBNEIsTUFBbUI7UUFDN0MsSUFBSSxPQUFPLEdBQUcsTUFBcUIsQ0FBQTtRQUNuQyxPQUFPLE9BQU8sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxhQUFhLEVBQUU7WUFDekUsT0FBTyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUE7U0FDaEM7UUFFRCxPQUFPLE9BQU8sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFBO0lBQ2xELENBQUM7SUFFRDs7OztPQUlHO0lBQ08scUNBQW9CLEdBQTlCLFVBQ0UsT0FBZ0IsRUFDaEIsU0FBZ0IsRUFDaEIsV0FBbUI7UUFIckIsaUJBd0dDO1FBdEdDLDBCQUFBLEVBQUEsZ0JBQWdCO1FBQ2hCLDRCQUFBLEVBQUEsbUJBQW1CO1FBRW5CLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsTUFBSSxtQkFBcUIsQ0FBQyxDQUFBO1FBRTFGLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxLQUFLLEVBQUUsRUFBWixDQUFZLEVBQUUsYUFBYSxDQUFDLENBQUE7WUFDN0MsT0FBTTtTQUNQO1FBRUQsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxFQUFFO1lBQzlDLE9BQU07U0FDUDtRQUVELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ3ZDLE1BQU0sSUFBSSxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQTtTQUNyRDtRQUVELElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUV6QixJQUFJLFdBQVcsS0FBSyxJQUFJLEVBQUU7WUFDeEIsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxLQUFLLE9BQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLEVBQW5FLENBQW1FLENBQUUsQ0FBQTtTQUN0RztRQUVELElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQTtRQUV0QixJQUFJLE9BQU8sSUFBSSxPQUFPLElBQUksT0FBTyxLQUFLLE9BQU8sRUFBRTtZQUM3QyxxREFBcUQ7WUFDckQsVUFBVSxHQUFHLElBQUksQ0FBQTtZQUVqQixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUM1QyxrRkFBa0Y7Z0JBQ2xGLE9BQU07YUFDUDtZQUVELE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFBO1NBQ2hDO1FBRUQsSUFBSSxPQUFPLEVBQUU7WUFDWCxrQ0FBa0M7WUFDbEMsSUFBSSxVQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUNqRCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxVQUFRLEVBQW5DLENBQW1DLENBQUMsQ0FBQTtZQUV2RixJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQXlCLFVBQVEsb0JBQWlCLENBQUMsQ0FBQTthQUNwRTtZQUVELHFCQUFxQjtZQUNyQixVQUFVLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTtZQUMzQixHQUFHLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO1NBQzlDO1FBRUQsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLG1CQUFtQjtZQUNwQyxvQkFBb0I7WUFDcEIsSUFBSSxVQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUNqRCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxVQUFRLEVBQW5DLENBQW1DLENBQUMsQ0FBQTtZQUV2RixJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNmLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQXlCLFVBQVEsb0JBQWlCLENBQUMsQ0FBQTthQUNwRTtZQUVELG1CQUFtQjtZQUNuQixVQUFVLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtZQUMxQixHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO1lBRTFDLHFCQUFxQjtZQUNyQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsVUFBVSxDQUFBO1NBRXRDO2FBQU0sRUFBRSxxQkFBcUI7WUFDNUIseURBQXlEO1lBQ3pELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUMzQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFBO2FBQ25EO1NBQ0Y7UUFFRCxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQTtRQUUzQixJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssS0FBSyxJQUFJLFVBQVUsRUFBRTtZQUNoRCxpREFBaUQ7WUFDakQsSUFBSSxDQUFDLGtCQUFtQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUE7WUFDeEMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1NBQ3pCO1FBRUQsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzVFLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtTQUN6QjtRQUVELGlDQUFpQztRQUNqQyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFBO1NBQ3BCO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFFekMsNkJBQTZCO1FBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUE7UUFFNUIsSUFBSSxTQUFTLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDN0IsVUFBVSxDQUFDO2dCQUNULEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUNkLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQTtTQUNsQjtJQUNILENBQUM7SUFFUyxtQ0FBa0IsR0FBNUIsVUFBNkIsZ0JBQXlCOztRQUNwRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQTtRQUU1RSxJQUFJLGdCQUFnQixLQUFLLElBQUksRUFBRTtZQUM3QixJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtZQUU5QyxJQUFJLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUM1QixJQUFJLEdBQUcsRUFBRSxDQUFBOztvQkFDVCxLQUFpQixJQUFBLGtCQUFBLGlCQUFBLGFBQWEsQ0FBQSw0Q0FBQSx1RUFBRTt3QkFBM0IsSUFBSSxJQUFJLDBCQUFBO3dCQUNYLElBQUksSUFBTyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFJLENBQUE7cUJBQzlCOzs7Ozs7Ozs7Z0JBQ0QsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7YUFDMUM7U0FDRjtRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDNUIsQ0FBQztJQUVTLG9DQUFtQixHQUE3QjtRQUNFLElBQUksZUFBZSxHQUF3QixFQUFFLENBQUE7UUFDN0MsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUN4QixFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFVBQUMsTUFBeUI7Z0JBQy9ELElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7b0JBQ3ZDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7aUJBQzdCO1lBQ0gsQ0FBQyxDQUFDLENBQUMsQ0FBQTtTQUNKO1FBQ0QsT0FBTyxlQUFlLENBQUE7SUFDeEIsQ0FBQztJQUVEOzs7T0FHRztJQUNLLGtDQUFpQixHQUF6QjtRQUNFLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLElBQUksRUFBRSxDQUFBO1FBQ3ZDLElBQU0sUUFBUSxHQUFjLEVBQUUsQ0FBQTtRQUM5QixJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFBO1FBRTNDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzlDLElBQU0sS0FBSyxHQUFZLGNBQWMsQ0FBQyxDQUFDLENBQVksQ0FBQTtZQUVuRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxvQkFBb0I7Z0JBQ2pELElBQU0sYUFBYSxHQUFZLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFZLENBQUE7Z0JBQ2hFLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQTtnQkFFakIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUM5QyxJQUFNLFdBQVcsR0FBWSxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQVksQ0FBQTtvQkFFekUsa0JBQWtCO29CQUNsQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsRUFBRTt3QkFDckQsYUFBYSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQTt3QkFDdEMsS0FBSyxHQUFHLElBQUksQ0FBQTtxQkFDYjtpQkFDRjtnQkFFRCw0QkFBNEI7Z0JBQzVCLElBQUksS0FBSyxFQUFFO29CQUNULFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7aUJBQzdCO2FBRUY7aUJBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsa0JBQWtCO2dCQUNwRCxJQUFNLFdBQVcsR0FBWSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBWSxDQUFBO2dCQUU3RCxnQkFBZ0I7Z0JBQ2hCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxFQUFFO29CQUNyRCxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO2lCQUMzQjthQUNGO1NBQ0Y7UUFFRCxPQUFPLFFBQVEsQ0FBQTtJQUNqQixDQUFDO0lBRUQ7O09BRUc7SUFDSyw4QkFBYSxHQUFyQixVQUFzQixJQUFZLEVBQUUsT0FBZTtRQUNqRCxPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7SUFDL0QsQ0FBQztJQUVTLDZCQUFZLEdBQXRCO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7UUFDWCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTtRQUV4QixVQUFVLENBQUM7WUFDVCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQTtRQUMzQixDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUE7SUFDbEIsQ0FBQztJQUVTLDRCQUFXLEdBQXJCO1FBQ0UsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO0lBQ2QsQ0FBQztJQUVTLDZCQUFZLEdBQXRCLFVBQXVCLEtBQWlCO1FBQ3RDLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQTtRQUVuQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxLQUFLLEVBQUU7WUFDcEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQTtZQUNsQyxPQUFNO1NBQ1A7UUFFRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLE1BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssS0FBSyxFQUFFO1lBQ3RFLGdEQUFnRDtZQUNoRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7WUFDYixPQUFPLEdBQUcsSUFBSSxDQUFBO1NBQ2Y7UUFFRCxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBaUIsQ0FBQTtRQUVyQyxJQUFJLENBQUMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxFQUFFO1lBQ2pELGtDQUFrQztZQUNsQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7WUFDOUQsT0FBTyxHQUFHLElBQUksQ0FBQTtTQUNmO1FBRUQsSUFBSSxPQUFPLEVBQUU7WUFDWCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFBO1lBQzlCLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUN0QjtJQUNILENBQUM7SUFFUyxtQ0FBa0IsR0FBNUIsVUFBNkIsS0FBaUI7UUFDNUMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLE1BQU8sQ0FBQyxFQUFFO1lBQ2hGLE9BQU07U0FDUDtRQUVELElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUNkLENBQUM7SUFFUyx5Q0FBd0IsR0FBbEMsVUFBbUMsT0FBZSxFQUFFLFVBQWtCLEVBQUUsT0FBZ0M7UUFDdEcsS0FBSyxJQUFJLEtBQUssR0FBRyxVQUFVLEVBQUUsS0FBSyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDNUQsSUFBSSxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7WUFDekMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtZQUV4QyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUMxQixLQUFLLEdBQUcsQ0FBQyxDQUFBO2FBQ1Y7WUFFRCxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFO2dCQUNqRCxJQUFJLFNBQVMsR0FBRyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtnQkFFOUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsRUFBRTtvQkFDNUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO29CQUM5QixTQUFTLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUE7b0JBQ3RDLE9BQU8sU0FBUyxDQUFBO2lCQUNqQjthQUNGO1NBQ0Y7UUFDRCxPQUFPLFNBQVMsQ0FBQTtJQUNsQixDQUFDO0lBRVMsK0JBQWMsR0FBeEIsVUFBeUIsS0FBb0I7UUFDM0MsSUFBSSxHQUFHLEdBQUcsS0FBSyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUE7UUFDL0IsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFBO1FBRTFDLElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxVQUFVLEVBQUU7WUFDakMsMEJBQTBCO1lBQzFCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUNqQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7YUFDYjtZQUNELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUNwQixPQUFNO1NBQ1A7UUFFRCxJQUFJLE9BQU8sS0FBSyxNQUFNLENBQUMsWUFBWSxJQUFJLE9BQU8sS0FBSyxNQUFNLENBQUMsY0FBYyxFQUFFO1lBQ3hFLHFCQUFxQjtZQUVyQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFJLFVBQVksQ0FBNEIsQ0FBQTtZQUN4RyxJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUV0QixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUE7Z0JBQ2hCLElBQUksU0FBUyxTQUFBLENBQUE7Z0JBRWIsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBSSxrQkFBb0IsQ0FBQyxDQUFBO2dCQUN4RSxJQUFJLFNBQVMsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQTtnQkFFekUsSUFBSSxVQUFVLFNBQUEsQ0FBQTtnQkFFZCxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtvQkFDbkQsSUFBSSxTQUFTLEdBQUcsT0FBTyxLQUFLLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBRTFELElBQUksSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO29CQUV6QyxpREFBaUQ7b0JBQ2pELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRTt3QkFDNUIsU0FBUyxHQUFHLElBQUksQ0FBQTt3QkFDaEIsUUFBUSxHQUFHLEtBQUssQ0FBQTt3QkFFaEIsaUVBQWlFO3dCQUNqRSxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTs0QkFDbkQsUUFBUSxJQUFJLFNBQVMsQ0FBQTs0QkFDckIsUUFBUSxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUE7NEJBRTFCLElBQUksUUFBUSxHQUFHLENBQUMsRUFBRTtnQ0FDaEIsUUFBUSxHQUFHLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBOzZCQUM5Qjs0QkFFRCxVQUFVLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7NEJBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLEVBQUU7Z0NBQzdDLE1BQUs7NkJBQ047eUJBQ0Y7cUJBQ0Y7aUJBQ0Y7Z0JBRUQsOEJBQThCO2dCQUM5QixjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7Z0JBQ2pDLElBQUksU0FBUyxHQUFHLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBO2dCQUNqRCxTQUFTLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUE7Z0JBRXRDLElBQUksU0FBUyxFQUFFO29CQUNiLFNBQVMsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtpQkFDMUM7YUFDRjtZQUVELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUNwQixPQUFNO1NBQ1A7UUFFRCxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDeEQsZ0JBQWdCO1lBRWhCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE1BQUksVUFBWSxDQUE0QixDQUFBO1lBQ3hHLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBRXRCLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQTtnQkFDckIsSUFBSSxnQkFBZ0IsR0FBRyxLQUFLLENBQUE7Z0JBRTVCLEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNuRCxJQUFJLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtvQkFFekMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLEVBQUU7d0JBQ3JDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQTt3QkFFcEMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTt3QkFDeEMsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRTs0QkFDakQsZ0JBQWdCLEdBQUcsSUFBSSxDQUFBOzRCQUN2QixhQUFhLEdBQUcsS0FBSyxDQUFBO3lCQUN0QjtxQkFDRjtpQkFDRjtnQkFFRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUE7Z0JBQ3pHLElBQUksU0FBUyxLQUFLLFNBQVMsRUFBQztvQkFDMUIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUE7aUJBQ25EO2FBQ0Y7WUFFRCxHQUFHLENBQUMsY0FBYyxFQUFFLENBQUE7WUFDcEIsT0FBTTtTQUNQO1FBRUQsSUFBSSxPQUFPLEtBQUssTUFBTSxDQUFDLFNBQVMsSUFBSSxPQUFPLEtBQUssTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUM5RCxzRUFBc0U7WUFDdEUsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBSSxrQkFBb0IsQ0FBRSxDQUFBO1lBQ3BGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtTQUMvRDtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNLLHFDQUFvQixHQUE1QixVQUE2QixDQUFnQjtRQUMzQyxJQUFNLE9BQU8sR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUE7UUFFcEMsdUZBQXVGO1FBQ3ZGLElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxTQUFTLEVBQUU7WUFDaEMsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE1BQUksVUFBWSxDQUFDLENBQUE7WUFFekYsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtnQkFDMUUsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFBO2FBQ3BCO1NBQ0Y7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSyxtQ0FBa0IsR0FBMUIsVUFBMkIsQ0FBZ0I7UUFDekMsSUFBTSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQTBCLENBQUE7UUFFM0MscUJBQXFCO1FBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsYUFBYSxJQUFJLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLGdCQUFnQixJQUFJLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLG1CQUFvQixDQUFDLFNBQVMsRUFBRTtZQUN6SSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUM5QjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNLLG1DQUFrQixHQUExQixVQUEyQixDQUFhO1FBQ3RDLElBQU0sTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUEwQixDQUFBO1FBRTNDLFVBQVUsQ0FBQztZQUNULE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQTtRQUNqQixDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRDs7O09BR0c7SUFDSywyQkFBVSxHQUFsQixVQUFtQixNQUFtQjtRQUFuQix1QkFBQSxFQUFBLFdBQW1CO1FBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQTtRQUMzRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUE7SUFDM0MsQ0FBQztJQUVEOztPQUVHO0lBQ0ssNkJBQVksR0FBcEI7UUFDRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUE7UUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFBO0lBQzNDLENBQUM7SUFFRDs7O09BR0c7SUFDSywyQkFBVSxHQUFsQixVQUFtQixPQUFrQjtRQUFyQyxpQkFXQztRQVZDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBRTdCLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFNO1lBQ3JCLEtBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ2xDLENBQUMsQ0FBQyxDQUFBO1FBRUYsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxtQkFBb0IsQ0FBQyxLQUFLLENBQUE7UUFFcEQsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQ2YsQ0FBQztJQUVEOzs7T0FHRztJQUNLLDJCQUFVLEdBQWxCLFVBQW1CLElBQVU7UUFDM0IsT0FBTyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1NBQ2xDO0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0ssK0JBQWMsR0FBdEIsVUFBdUIsTUFBeUI7UUFDOUMsT0FBTyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUE7SUFDM0UsQ0FBQztJQUVEOzs7T0FHRztJQUNPLGdDQUFlLEdBQXpCLFVBQTBCLElBQVk7UUFDcEMsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDNUIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxtQkFBb0QsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTthQUNoRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO2FBQ3ZDO1NBQ0Y7SUFDSCxDQUFDO0lBTUQsc0JBQUkseUJBQUs7UUFKVDs7O1dBR0c7YUFDSDtZQUNFLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDeEIsT0FBTyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsS0FBSyxFQUFQLENBQU8sQ0FBQyxDQUFBO2FBQ3REO1lBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxFQUFFLEVBQUU7Z0JBQzdCLE9BQU8sSUFBSSxDQUFBO2FBQ1o7WUFFRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFBO1FBQzNCLENBQUM7OztPQUFBO0lBT0Qsc0JBQUksNEJBQVE7UUFMWjs7OztXQUlHO2FBQ0gsVUFBYSxLQUFjO1lBQ3pCLElBQUksS0FBSyxFQUFFO2dCQUNULElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTthQUNmO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTthQUNkO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRDs7O09BR0c7SUFDSCx1QkFBTSxHQUFOO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBRTlDLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxTQUFTLEVBQUUsRUFBRSw4REFBOEQ7WUFDcEcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7U0FDekI7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtRQUVqQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDbEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBRXJCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDekIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDdEM7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCx1QkFBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUE7UUFDeEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFaEQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUUxRCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzFFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUMxRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO0lBQzFFLENBQUM7SUFFRDs7T0FFRztJQUNILHdCQUFPLEdBQVA7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUE7UUFDekMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFN0MsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUU3RCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQzdFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUE7UUFDakYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUM3RSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBRTNFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUNkLENBQUM7SUFFRDs7T0FFRztJQUNILHVCQUFNLEdBQU47UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUNqQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7U0FDYjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFBO1NBQ1o7SUFDSCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsdUJBQU0sR0FBTjtRQUNFLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUE7SUFDbEQsQ0FBQztJQUVEOztPQUVHO0lBQ0gscUJBQUksR0FBSjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUE7WUFFekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDOUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUE7WUFFekMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUE7WUFDbEYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUE7U0FDakY7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxzQkFBSyxHQUFMO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDakIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUE7WUFFekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUE7WUFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUE7WUFFM0MsZ0VBQWdFO1lBQ2hFLDZDQUE2QztZQUM3QyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRTtnQkFDeEIsc0JBQXNCO2dCQUNyQixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBNEIsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtnQkFFN0QsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsbUJBQW9CLENBQUMsU0FBUyxFQUFFO29CQUNyRixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxtQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQTtpQkFDMUQ7YUFDRjtZQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1lBQ3JGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1lBRW5GLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQUksa0JBQW9CLENBQUMsQ0FBQTtZQUVyRSxJQUFJLFdBQVcsRUFBRTtnQkFDZixXQUFXLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUE7YUFDNUM7U0FDRjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNLLDhCQUFhLEdBQXJCO1FBQ0UsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO0lBQ3hELENBQUM7SUFFRDs7T0FFRztJQUNILHdCQUFPLEdBQVA7UUFDRSxNQUFNLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1FBRTdELElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1lBQ3JGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO1lBRW5GLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDckMsSUFBWSxDQUFDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQTtTQUMzQztRQUVELElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzVCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUE7WUFDbkYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtZQUMvRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1NBQ2hGO1FBRUQsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7WUFDN0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtZQUNqRixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBQzdFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFFM0UsSUFBWSxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUE7U0FDMUM7UUFFRCxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pDLElBQVksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUE7U0FDL0M7UUFFRCxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFDSCxhQUFDO0FBQUQsQ0FyOUJBLEFBcTlCQyxDQXI5Qm9CLFVBQVUsR0FxOUI5QjtBQUVELE1BQU0sVUFBVSxJQUFJO0lBQ2xCLG1CQUFtQixDQUFvQixRQUFRLEVBQUUsVUFBQyxDQUFDO1FBQ2pELElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ2YsQ0FBQyxDQUFDLENBQUE7QUFDSixDQUFDO0FBRUQsZUFBZSxNQUFNLENBQUEiLCJmaWxlIjoibWFpbi9zcmMvZm9ybS9TZWxlY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzZWFyY2hBbmRJbml0aWFsaXplLCBwcmV2ZW50RGVmYXVsdCwgZmluZCwgcmVtb3ZlLCBtc0lFVmVyc2lvbiwgc2Nyb2xsSW50b1ZpZXcgfSBmcm9tIFwiLi4vVXRpbHNcIlxuaW1wb3J0IERvbUVsZW1lbnQgZnJvbSBcIi4uL0RvbUVsZW1lbnRcIlxuaW1wb3J0ICogYXMgSW5wdXRzIGZyb20gXCIuLi9JbnB1dHNcIlxuaW1wb3J0ICogYXMgRG9tIGZyb20gXCIuLi9Eb21GdW5jdGlvbnNcIlxuXG5jb25zdCBDTEFTU19QTEFDRUhPTERFUiA9IFwic2VsZWN0X19wbGFjZWhvbGRlclwiXG5jb25zdCBDTEFTU19USFVNQiA9IFwic2VsZWN0X190aHVtYlwiXG5jb25zdCBDTEFTU19CVVRUT04gPSBcInNlbGVjdF9fYnV0dG9uXCJcbmNvbnN0IENMQVNTX0RST1BET1dOID0gXCJzZWxlY3RfX2Ryb3Bkb3duXCJcblxuY29uc3QgQ0xBU1NfT1BFTiA9IFwic2VsZWN0LS1vcGVuXCJcbmNvbnN0IENMQVNTX0NMT1NFRCA9IFwic2VsZWN0LS1jbG9zZWRcIlxuY29uc3QgQ0xBU1NfRElTQUJMRUQgPSBcInNlbGVjdC0tZGlzYWJsZWRcIlxuY29uc3QgQ0xBU1NfRklMVEVSQUJMRSA9IFwic2VsZWN0LS1maWx0ZXJhYmxlXCJcblxuY29uc3QgQ0xBU1NfSVRFTSA9IFwiZHJvcGRvd24taXRlbVwiXG5jb25zdCBDTEFTU19JVEVNX1NFTEVDVEVEID0gXCJkcm9wZG93bi1pdGVtLS1zZWxlY3RlZFwiXG5jb25zdCBDTEFTU19JVEVNX0ZPQ1VTRUQgPSBcImRyb3Bkb3duLWl0ZW0tLWZvY3VzZWRcIlxuY29uc3QgQ0xBU1NfSVRFTV9ESVNBQkxFRCA9IFwiZHJvcGRvd24taXRlbS0tZGlzYWJsZWRcIlxuXG5jb25zdCBDTEFTU19HUk9VUF9JVEVNID0gXCJkcm9wZG93bi1ncm91cFwiXG5jb25zdCBDTEFTU19HUk9VUF9IRUFERVIgPSBcImRyb3Bkb3duLWdyb3VwX19pdGVtXCJcblxuY29uc3QgUVVFUllfTUVTU0FHRSA9IFwiLm1lc3NhZ2VcIlxuXG5jb25zdCBUSU1FT1VUX0NMT1NFID0gMTUwXG5jb25zdCBUSU1FT1VUX0JMVVIgPSA0MDBcblxuLyoqXG4gKiBUaGUgc2VsZWN0IGNvbXBvbmVudCBBUEkuXG4gKi9cbmNsYXNzIFNlbGVjdCBleHRlbmRzIERvbUVsZW1lbnQ8SFRNTFNlbGVjdEVsZW1lbnQ+IHtcbiAgcHJpdmF0ZSBfb3BlbkJ5Rm9jdXM6IGJvb2xlYW5cbiAgcHJpdmF0ZSBfbXVsdGlzZWxlY3Rpb246IGJvb2xlYW5cbiAgcHJpdmF0ZSBfY2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfaGFuZGxlRHJvcGRvd25DbGljazogKGU6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2tleWRvd25IYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfZm9jdXNIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfYmx1ckhhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF93aW5kb3dDbGlja0hhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9maWx0ZXJLZXlkb3duSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2ZpbHRlcktleXVwSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2ZpbHRlckZvY3VzSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG5cbiAgcHJpdmF0ZSBfd3JhcHBlckVsZW1lbnQhOiBEb21FbGVtZW50XG4gIHByaXZhdGUgX2Ryb3Bkb3duRWxlbWVudCE6IERvbUVsZW1lbnQ8SFRNTEVsZW1lbnQ+XG5cbiAgcHJpdmF0ZSBfc2VsZWN0QnV0dG9uRWxlbWVudCE6IERvbUVsZW1lbnRcbiAgcHJpdmF0ZSBfdGh1bWJFbGVtZW50ITogRG9tRWxlbWVudFxuXG4gIHByaXZhdGUgX3BsYWNlaG9sZGVyT3B0aW9uPzogSFRNTE9wdGlvbkVsZW1lbnRcbiAgcHJpdmF0ZSBfcGxhY2Vob2xkZXJFbGVtZW50ITogRG9tRWxlbWVudFxuICBwcml2YXRlIF9wbGFjZWhvbGRlclRleHQhOiBzdHJpbmdcblxuICBwcml2YXRlIF9sYXN0SGFuZGxlZEV2ZW50PzogRXZlbnRcbiAgcHJpdmF0ZSBfbGFzdFNlbGVjdGVkT3B0aW9uPzogSFRNTE9wdGlvbkVsZW1lbnRcblxuICAvLyBNaW5pbXVtIGZpbHRlciBsZW5ndGhcbiAgcHJpdmF0ZSBfbWluRmlsdGVyTGVuZ3RoID0gMlxuXG4gIC8vIFRoZSBrZXl3b3JkIHRoZSBTZWxlY3QgaXMgY3VycmVudGx5IGZpbHRlcmVkIGJ5XG4gIHByaXZhdGUgX2FjdGl2ZUZpbHRlcj86IHN0cmluZ1xuXG4gIC8vIFRoZSBvcHRpb25zIHRoZSBTZWxlY3Qgd2FzIGluaXRpYWxseSBjcmVhdGVkIHVwb25cbiAgLy8gVGhlc2Ugd2lsbCBiZSB1c2VkIGFzIGEgYmFzaXMgZm9yIGZpbHRlcmluZ1xuICBwcml2YXRlIF9pbml0aWFsT3B0aW9ucyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRoaXMuZWxlbWVudC5jaGlsZHJlbilcblxuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBIVE1MU2VsZWN0RWxlbWVudCkge1xuICAgIHN1cGVyKGVsZW1lbnQpXG5cbiAgICB0aGlzLl9vcGVuQnlGb2N1cyA9IGZhbHNlXG5cbiAgICAvLyBDaGVjayBmb3IgbXVsdGktc2VsZWN0aW9uXG4gICAgdGhpcy5fbXVsdGlzZWxlY3Rpb24gPSB0aGlzLmVsZW1lbnQuaGFzQXR0cmlidXRlKFwibXVsdGlwbGVcIikgPT09IHRydWVcblxuICAgIC8vIFNldHVwIGV2ZW50IGNvbnRleHRcbiAgICB0aGlzLl9jbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVDbGljay5iaW5kKHRoaXMpXG4gICAgdGhpcy5faGFuZGxlRHJvcGRvd25DbGljayA9IHRoaXMuX2hhbmRsZUNsaWNrLmJpbmQodGhpcylcbiAgICB0aGlzLl9rZXlkb3duSGFuZGxlciA9IHRoaXMuX2hhbmRsZUtleWRvd24uYmluZCh0aGlzKVxuICAgIHRoaXMuX2ZvY3VzSGFuZGxlciA9IHRoaXMuX2hhbmRsZUZvY3VzLmJpbmQodGhpcylcbiAgICB0aGlzLl9ibHVySGFuZGxlciA9IHRoaXMuX2hhbmRsZUJsdXIuYmluZCh0aGlzKVxuICAgIHRoaXMuX3dpbmRvd0NsaWNrSGFuZGxlciA9IHRoaXMuX2hhbmRsZVdpbmRvd0NsaWNrLmJpbmQodGhpcylcbiAgICB0aGlzLl9maWx0ZXJLZXlkb3duSGFuZGxlciA9IHRoaXMuX2hhbmRsZUZpbHRlcktleWRvd24uYmluZCh0aGlzKVxuICAgIHRoaXMuX2ZpbHRlcktleXVwSGFuZGxlciA9IHRoaXMuX2hhbmRsZUZpbHRlcktleXVwLmJpbmQodGhpcylcbiAgICB0aGlzLl9maWx0ZXJGb2N1c0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVGaWx0ZXJGb2N1cy5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgc2VsZWN0IGNvbXBvbmVudC5cbiAgICpcbiAgICogVGhpcyBtZXRob2QgaW5zcGVjdHMgdGhlIHNlbGVjdCBkZWZpbml0aW9uIGFuZCBpdHMgb3B0aW9ucyBhbmRcbiAgICogZ2VuZXJhdGVzIG5ldyBzdHlsYWJsZSBET00gZWxlbWVudHMgYXJvdW5kIHRoZSBvcmlnaW5hbCBzZWxlY3QtZWxlbWVudFxuICAgKiBkZWZpbml0aW9ucy5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfaW5pdGlhbGl6ZSgpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9wdGlvbiA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFwib3B0aW9uW3NlbGVjdGVkXVwiKSBhcyBIVE1MT3B0aW9uRWxlbWVudFxuICAgIGNvbnN0IGZpcnN0T3B0aW9uID0gdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCJvcHRpb25cIikgYXMgSFRNTE9wdGlvbkVsZW1lbnRcblxuICAgIC8vIFBlciBkZWZhdWx0LCBzZXQgdGhlIGxhc3Qgc2VsZWN0ZWQgb3B0aW9uIHRvIGVpdGhlciB0aGUgb3B0aW9uIHdpdGggYSBcInNlbGVjdGVkXCIgYXR0cmlidXRlLFxuICAgIC8vIG9yLCBpZiBub3QgZm91bmQsIHRvIHRoZSBmaXJzdCBhdmFpbGFibGUgb3B0aW9uXG4gICAgdGhpcy5fbGFzdFNlbGVjdGVkT3B0aW9uID0gc2VsZWN0ZWRPcHRpb24gfHwgZmlyc3RPcHRpb25cblxuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQodGhpcy5lbGVtZW50LnBhcmVudEVsZW1lbnQhKVxuICAgICAgLmFkZENsYXNzKENMQVNTX0NMT1NFRClcblxuICAgIGZvciAobGV0IGNscyBvZiB0aGlzLmNsYXNzZXMpIHtcbiAgICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmFkZENsYXNzKGNscylcbiAgICB9XG5cbiAgICB0aGlzLl9kcm9wZG93bkVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudDxIVE1MRWxlbWVudD4oXCJkaXZcIilcbiAgICAgIC5hZGRDbGFzcyhDTEFTU19EUk9QRE9XTilcblxuICAgIGlmIChtc0lFVmVyc2lvbigpID4gMCAmJiBtc0lFVmVyc2lvbigpIDwgMTIpIHtcbiAgICAgIC8vIFRoaXMgaXMgYSB3b3JrYXJvdW5kIGZvciBJRSBicm93c2VycyAxMSBhbmQgZWFybGllciB3aGVyZSBmb2N1c2luZ1xuICAgICAgLy8gYSBzY3JvbGxhYmxlIGRyb3Bkb3duIGxpc3Qgd2lsbCBjbG9zZSB0aGUgZHJvcGRvd24gcHJlbWF0dXJlbHkuXG4gICAgICB0aGlzLl9kcm9wZG93bkVsZW1lbnQuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIChldmVudDogTW91c2VFdmVudCkgPT4gZXZlbnQucHJldmVudERlZmF1bHQoKSlcbiAgICB9XG5cbiAgICB0aGlzLl9zZXR1cFRhcmdldCgpXG4gICAgdGhpcy5fc2V0dXBQbGFjZWhvbGRlcigpXG5cbiAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLl9kcm9wZG93bkVsZW1lbnQpXG5cbiAgICB0aGlzLl9jcmVhdGVPcHRpb25zKHRoaXMuZWxlbWVudClcblxuICAgIHRoaXMuX3VwZGF0ZVNpemUoKVxuICAgIHRoaXMuX3VwZGF0ZU1lc3NhZ2UoKVxuXG4gICAgaWYgKHRoaXMuZWxlbWVudC5kaXNhYmxlZCkge1xuICAgICAgdGhpcy5kaXNhYmxlKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lbmFibGUoKVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfc2V0dXBUYXJnZXQoKSB7XG4gICAgLy8gbW92ZSB0aGUgaWQgZnJvbSB0aGUgc2VsZWN0IGVsZW1lbnQgdG8gdGhlIHdyYXBwZXJcbiAgICBjb25zdCBpZCA9IHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJpZFwiKVxuICAgIGlmIChpZCkge1xuICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUF0dHJpYnV0ZShcImlkXCIpXG4gICAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJpZFwiLCBpZClcbiAgICB9XG5cbiAgICAvLyBBcHBseSB0aGUgdGFiIGluZGV4XG4gICAgY29uc3QgdGFiSW5kZXggPSB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwidGFiaW5kZXhcIilcbiAgICBpZiAodGFiSW5kZXgpIHtcbiAgICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LnNldEF0dHJpYnV0ZShcInRhYkluZGV4XCIsIHRhYkluZGV4KVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfc2V0dXBQbGFjZWhvbGRlcigpIHtcbiAgICBpZiAoIXRoaXMuX3NlbGVjdEJ1dHRvbkVsZW1lbnQpIHtcbiAgICAgIHRoaXMuX3NlbGVjdEJ1dHRvbkVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfQlVUVE9OKVxuXG4gICAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLl9zZWxlY3RCdXR0b25FbGVtZW50KVxuICAgIH1cblxuICAgIGlmICghdGhpcy5fdGh1bWJFbGVtZW50KSB7XG4gICAgICB0aGlzLl90aHVtYkVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgICAuYWRkQ2xhc3MoQ0xBU1NfVEhVTUIpXG5cbiAgICAgIGxldCB0aHVtYkljb24gPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgICAuYWRkQ2xhc3MoXCJ0aHVtYi1pY29uXCIpXG5cbiAgICAgIGxldCBsb2FkZXIgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgICAuYWRkQ2xhc3MoXCJsb2FkZXItc3Bpbm5lclwiKVxuICAgICAgICAuYWRkQ2xhc3MoXCJsb2FkZXItc3Bpbm5lci0tc21hbGxcIilcblxuICAgICAgdGhpcy5fdGh1bWJFbGVtZW50LmFwcGVuZENoaWxkKGxvYWRlcilcbiAgICAgIHRoaXMuX3RodW1iRWxlbWVudC5hcHBlbmRDaGlsZCh0aHVtYkljb24pXG4gICAgICB0aGlzLl9zZWxlY3RCdXR0b25FbGVtZW50LmFwcGVuZENoaWxkKHRoaXMuX3RodW1iRWxlbWVudClcbiAgICB9XG5cbiAgICBsZXQgcGxhY2Vob2xkZXJUZXh0ID0gXCJcIlxuXG4gICAgdGhpcy5fcGxhY2Vob2xkZXJPcHRpb24gPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihcIm9wdGlvbltzZWxlY3RlZF1bZGlzYWJsZWRdXCIpIGFzIEhUTUxPcHRpb25FbGVtZW50IHx8IHVuZGVmaW5lZFxuXG4gICAgaWYgKHRoaXMuX3BsYWNlaG9sZGVyT3B0aW9uKSB7XG4gICAgICBwbGFjZWhvbGRlclRleHQgPSBEb20udGV4dCh0aGlzLl9wbGFjZWhvbGRlck9wdGlvbilcblxuICAgICAgaWYgKHRoaXMuX211bHRpc2VsZWN0aW9uID09PSB0cnVlKSB7XG4gICAgICAgIHRoaXMuX3BsYWNlaG9sZGVyT3B0aW9uLnNlbGVjdGVkID0gZmFsc2VcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsZXQgc2VsZWN0ZWRPcHRpb24gPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihcIm9wdGlvbltzZWxlY3RlZF06bm90KFtkaXNhYmxlZF0pXCIpXG5cbiAgICBpZiAoc2VsZWN0ZWRPcHRpb24pIHtcbiAgICAgIHBsYWNlaG9sZGVyVGV4dCA9IERvbS50ZXh0KHNlbGVjdGVkT3B0aW9uKVxuICAgIH1cblxuICAgIGlmICghdGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50KSB7XG4gICAgICAvLyBXaGVuIHRoZSBTZWxlY3QgaXMgZmlsdGVyYWJsZSwgY3JlYXRlIGFuIFwiaW5wdXRcIiBhcyB0aGUgcGxhY2Vob2xkZXIgZWxlbWVudCwgb3RoZXJ3aXNlIGEgXCJzcGFuXCJcbiAgICAgIGlmICh0aGlzLl9pc0ZpbHRlcmFibGUoKSkge1xuICAgICAgICB0aGlzLl9wbGFjZWhvbGRlckVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChcImlucHV0XCIpXG4gICAgICAgIHRoaXMuX3BsYWNlaG9sZGVyRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwia2V5dXBcIiwgKGUpID0+IHRoaXMuX2hhbmRsZUZpbHRlcktleXVwKGUpKVxuICAgICAgICB0aGlzLl9wbGFjZWhvbGRlckVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgKGUpID0+IHRoaXMuX2hhbmRsZUZpbHRlcktleWRvd24oZSkpXG4gICAgICAgIHRoaXMuX3BsYWNlaG9sZGVyRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiZm9jdXNcIiwgKGUpID0+IHRoaXMuX2hhbmRsZUZpbHRlckZvY3VzKGUpKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQoXCJzcGFuXCIpXG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3BsYWNlaG9sZGVyRWxlbWVudC5hZGRDbGFzcyhDTEFTU19QTEFDRUhPTERFUilcbiAgICAgIHRoaXMuX3NlbGVjdEJ1dHRvbkVsZW1lbnQuYXBwZW5kQ2hpbGQodGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50KVxuICAgIH1cblxuICAgIHRoaXMuX3NldFBsYWNlaG9sZGVyKHBsYWNlaG9sZGVyVGV4dClcbiAgICB0aGlzLl9wbGFjZWhvbGRlclRleHQgPSBwbGFjZWhvbGRlclRleHRcblxuICAgIGlmIChzZWxlY3RlZE9wdGlvbiAmJiBzZWxlY3RlZE9wdGlvbiAhPT0gdGhpcy5fcGxhY2Vob2xkZXJPcHRpb24pIHtcbiAgICAgIHRoaXMuX3VwZGF0ZVBsYWNlaG9sZGVyKHRydWUpXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF91cGRhdGVNZXNzYWdlKCkge1xuICAgIGNvbnN0IG1lc3NhZ2VOb2RlID0gdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX01FU1NBR0UpXG4gICAgaWYgKG1lc3NhZ2VOb2RlICE9PSBudWxsKSB7XG4gICAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hcHBlbmRDaGlsZChuZXcgRG9tRWxlbWVudChtZXNzYWdlTm9kZSkpXG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBfaXNPcHRHcm91cChlbGVtZW50OiBFbGVtZW50KTogZWxlbWVudCBpcyBIVE1MT3B0R3JvdXBFbGVtZW50IHtcbiAgICByZXR1cm4gZWxlbWVudC50YWdOYW1lLnRvVXBwZXJDYXNlKCkgPT09IFwiT1BUR1JPVVBcIlxuICB9XG5cbiAgcHJpdmF0ZSBfaXNPcHRpb24oZWxlbWVudDogRWxlbWVudCk6IGVsZW1lbnQgaXMgSFRNTE9wdGlvbkVsZW1lbnQge1xuICAgIHJldHVybiBlbGVtZW50LnRhZ05hbWUudG9VcHBlckNhc2UoKSA9PT0gXCJPUFRJT05cIlxuICB9XG5cbiAgcHJvdGVjdGVkIF9jcmVhdGVPcHRpb25zKGVsZW1lbnQ6IEhUTUxTZWxlY3RFbGVtZW50KSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBlbGVtZW50LmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICBsZXQgY2hpbGQgPSBlbGVtZW50LmNoaWxkcmVuW2ldXG5cbiAgICAgIGlmICh0aGlzLl9pc09wdEdyb3VwKGNoaWxkKSkge1xuICAgICAgICB0aGlzLl9hcHBlbmRHcm91cChjaGlsZCBhcyBIVE1MT3B0R3JvdXBFbGVtZW50KVxuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5faXNPcHRpb24oY2hpbGQpKSB7XG4gICAgICAgIGxldCBvcHRpb24gPSB0aGlzLl9jcmVhdGVPcHRpb24oY2hpbGQgYXMgSFRNTE9wdGlvbkVsZW1lbnQpXG5cbiAgICAgICAgaWYgKG9wdGlvbikge1xuICAgICAgICAgIHRoaXMuX2Ryb3Bkb3duRWxlbWVudC5hcHBlbmRDaGlsZChvcHRpb24pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX2NyZWF0ZU9wdGlvbihvcHRpb246IEhUTUxPcHRpb25FbGVtZW50KSB7XG4gICAgbGV0IGh0bWwgPSBvcHRpb24uaW5uZXJIVE1MXG5cbiAgICBpZiAodGhpcy5fYWN0aXZlRmlsdGVyKSB7XG4gICAgICBjb25zdCBzYW5pdGl6ZWRBY3RpdmVGaWx0ZXIgPSB0aGlzLl9hY3RpdmVGaWx0ZXIucmVwbGFjZSgvWy1cXFxcXiQqKz8uKCl8W1xcXXt9XS9nLCBcIlxcXFwkJlwiKVxuICAgICAgaHRtbCA9IGh0bWwucmVwbGFjZShuZXcgUmVnRXhwKGAoJHtzYW5pdGl6ZWRBY3RpdmVGaWx0ZXJ9KWAsIFwiZ2lcIiksIFwiPHN0cm9uZz4kMTwvc3Ryb25nPlwiKVxuICAgIH1cblxuICAgIGxldCBvcHQgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX0lURU0pXG4gICAgICAuc2V0SHRtbChodG1sKVxuXG4gICAgaWYgKG9wdGlvbi5zZWxlY3RlZCkge1xuICAgICAgb3B0LmFkZENsYXNzKENMQVNTX0lURU1fU0VMRUNURUQpXG4gICAgfVxuXG4gICAgaWYgKG9wdGlvbi5kaXNhYmxlZCkge1xuICAgICAgb3B0LmFkZENsYXNzKENMQVNTX0lURU1fRElTQUJMRUQpXG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLl9pc1BsYWNlaG9sZGVyKG9wdGlvbikpIHtcbiAgICAgIG9wdC5zZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIsIG9wdGlvbi52YWx1ZSlcbiAgICAgIHJldHVybiBvcHRcbiAgICB9XG5cbiAgICByZXR1cm4gdW5kZWZpbmVkXG4gIH1cblxuICBwcm90ZWN0ZWQgX2FwcGVuZEdyb3VwKG9wdGdyb3VwOiBIVE1MT3B0R3JvdXBFbGVtZW50KSB7XG4gICAgbGV0IGxhYmVsID0gb3B0Z3JvdXAuZ2V0QXR0cmlidXRlKFwibGFiZWxcIikhXG5cbiAgICBsZXQgZ3JvdXAgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX0dST1VQX0lURU0pXG5cbiAgICBsZXQgZ3JvdXBIZWFkZXIgPSBuZXcgRG9tRWxlbWVudChcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX0dST1VQX0hFQURFUilcbiAgICAgIC5zZXRIdG1sKGxhYmVsKVxuXG4gICAgZ3JvdXAuYXBwZW5kQ2hpbGQoZ3JvdXBIZWFkZXIpXG5cbiAgICBsZXQgb3B0aW9ucyA9IG9wdGdyb3VwLnF1ZXJ5U2VsZWN0b3JBbGwoXCJvcHRpb25cIilcbiAgICBmb3IgKGxldCBlbnRyeSBvZiBvcHRpb25zKSB7XG4gICAgICBsZXQgb3B0aW9uID0gdGhpcy5fY3JlYXRlT3B0aW9uKGVudHJ5KVxuICAgICAgaWYgKG9wdGlvbikge1xuICAgICAgICBncm91cC5hcHBlbmRDaGlsZChvcHRpb24pXG4gICAgICB9XG4gICAgfVxuXG4gICAgdGhpcy5fZHJvcGRvd25FbGVtZW50LmFwcGVuZENoaWxkKGdyb3VwKVxuICAgIHJldHVybiBncm91cFxuICB9XG5cbiAgcHJvdGVjdGVkIF91cGRhdGVTaXplKCkge1xuICAgIC8vIE5vdGU6IE1pcnJvcmluZyB0aGUgRE9NIGFuZCBtZWFzdXJpbmcgdGhlIGl0ZW1zIHVzaW5nIHRoZWlyIGNsaWVudFdpZHRoIHdhcyB2ZXJ5XG4gICAgLy8gdW5yZWxpYWJsZSwgdGhlcmVmb3JlIG1lYXN1cmluZyB3YXMgc3dpdGNoZWQgdG8gdGhlIG5ldyBIVE1MNSBtZWFzdXJlVGV4dCBtZXRob2RcbiAgICAvLyBtYXJnaW5zIGFuZCBwYWRkaW5ncyBhcnJvdW5kIHRoZSB0ZXh0IGFyZSBjb3BpZWQgZnJvbSB0aGUgb3JpZ2luYWwgcGxhY2Vob2xkZXIgaXRlbXNcbiAgICAvLyBkaW1lbnNpb25cbiAgICBjb25zdCBwbGFjZWhvbGRlclN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUodGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50LmVsZW1lbnQpXG5cbiAgICBsZXQgcGFkZGluZ1JpZ2h0ID0gcGFyc2VGbG9hdChwbGFjZWhvbGRlclN0eWxlLnBhZGRpbmdSaWdodCEpXG4gICAgbGV0IHBhZGRpbmdMZWZ0ID0gcGFyc2VGbG9hdChwbGFjZWhvbGRlclN0eWxlLnBhZGRpbmdMZWZ0ISlcblxuICAgIGxldCBmb250ID0gdGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50LmNzcyhcImZvbnRcIilcbiAgICBsZXQgdGV4dFdpZHRoID0gRG9tLnRleHRXaWR0aCh0aGlzLl9wbGFjZWhvbGRlclRleHQsIGZvbnQpXG4gICAgbGV0IG1heFdpZHRoID0gcGFkZGluZ0xlZnQgKyBwYWRkaW5nUmlnaHQgKyB0ZXh0V2lkdGhcblxuICAgIGxldCBvcHRpb25zID0gdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKGAuJHtDTEFTU19JVEVNfWApXG4gICAgZm9yIChsZXQgZW50cnkgb2Ygb3B0aW9ucykge1xuICAgICAgbGV0IHdpZHRoID0gRG9tLnRleHRXaWR0aChEb20udGV4dChlbnRyeSksIGZvbnQpICsgcGFkZGluZ0xlZnQgKyBwYWRkaW5nUmlnaHRcblxuICAgICAgaWYgKHdpZHRoID4gbWF4V2lkdGgpIHtcbiAgICAgICAgbWF4V2lkdGggPSB3aWR0aFxuICAgICAgfVxuICAgIH1cblxuICB9XG5cbiAgcHJvdGVjdGVkIF9pc0J1dHRvblRhcmdldCh0YXJnZXQ6IEV2ZW50VGFyZ2V0KSB7XG4gICAgcmV0dXJuICh0YXJnZXQgPT09IHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQgfHxcbiAgICAgIHRhcmdldCA9PT0gdGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50LmVsZW1lbnQgfHxcbiAgICAgIHRhcmdldCA9PT0gdGhpcy5fc2VsZWN0QnV0dG9uRWxlbWVudC5lbGVtZW50IHx8XG4gICAgICB0YXJnZXQgPT09IHRoaXMuX3RodW1iRWxlbWVudC5lbGVtZW50KVxuICB9XG5cbiAgcHJvdGVjdGVkIF9pc0Ryb3Bkb3duVGFyZ2V0KHRhcmdldDogRXZlbnRUYXJnZXQpIHtcbiAgICBsZXQgY3VycmVudCA9IHRhcmdldCBhcyBIVE1MRWxlbWVudFxuICAgIHdoaWxlIChjdXJyZW50ICE9PSB0aGlzLl9kcm9wZG93bkVsZW1lbnQuZWxlbWVudCAmJiBjdXJyZW50LnBhcmVudEVsZW1lbnQpIHtcbiAgICAgIGN1cnJlbnQgPSBjdXJyZW50LnBhcmVudEVsZW1lbnRcbiAgICB9XG5cbiAgICByZXR1cm4gY3VycmVudCA9PT0gdGhpcy5fZHJvcGRvd25FbGVtZW50LmVsZW1lbnRcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGVzIHRoZSBVSSBpZiB0aGUgc2VsZWN0aW9uIGhhcyBjaGFuZ2VkIGFuZCBtYWtlcyBzdXJlIHRoZVxuICAgKiBzZWxlY3QgY29udHJvbCBhbmQgdGhlIGdlbmVyYXRlZCBtYXJrdXAgYXJlIHN5bmNocm9uaXplZC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfc2VsZWN0ZWRJdGVtQ2hhbmdlZChcbiAgICBuZXdJdGVtOiBFbGVtZW50LFxuICAgIGF1dG9DbG9zZSA9IHRydWUsXG4gICAgbXVsdGlzZWxlY3QgPSBmYWxzZVxuICApIHtcbiAgICBjb25zdCBvbGRJdGVtcyA9IHRoaXMuX2Ryb3Bkb3duRWxlbWVudC5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC4ke0NMQVNTX0lURU1fU0VMRUNURUR9YClcblxuICAgIGlmICghbmV3SXRlbSkge1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLmNsb3NlKCksIFRJTUVPVVRfQ0xPU0UpXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAoRG9tLmhhc0NsYXNzKG5ld0l0ZW0sIENMQVNTX0lURU1fRElTQUJMRUQpKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAoKG9sZEl0ZW1zLmxlbmd0aCA9PT0gMCkgJiYgIW5ld0l0ZW0pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkNhbiBub3Qgc2VsZWN0IHVuZGVmaW5lZCBlbGVtZW50c1wiKVxuICAgIH1cblxuICAgIGxldCBvbGRJdGVtID0gb2xkSXRlbXNbMF1cblxuICAgIGlmIChtdWx0aXNlbGVjdCA9PT0gdHJ1ZSkge1xuICAgICAgb2xkSXRlbSA9IGZpbmQob2xkSXRlbXMsICh4KSA9PiB4LmdldEF0dHJpYnV0ZShcImRhdGEtdmFsdWVcIikgPT09IG5ld0l0ZW0uZ2V0QXR0cmlidXRlKFwiZGF0YS12YWx1ZVwiKSkhXG4gICAgfVxuXG4gICAgbGV0IGlzRGVzZWxlY3QgPSBmYWxzZVxuXG4gICAgaWYgKG5ld0l0ZW0gJiYgb2xkSXRlbSAmJiBvbGRJdGVtID09PSBuZXdJdGVtKSB7XG4gICAgICAvLyBDbGljayBvbiBhIHByZXZpb3VzbHkgc2VsZWN0ZWQgZWxlbWVudCAtPiBkZXNlbGVjdFxuICAgICAgaXNEZXNlbGVjdCA9IHRydWVcblxuICAgICAgaWYgKCF0aGlzLl9wbGFjZWhvbGRlck9wdGlvbiAmJiAhbXVsdGlzZWxlY3QpIHtcbiAgICAgICAgLy8gSWYgdGhlcmUgaXMgbm8gcGxhY2Vob2xkZXIgb3B0aW9uLCBub24gbXVsdGlzZWxlY3Qgb3B0aW9ucyBjYW5ub3QgYmUgZGVzZWxlY3RlZFxuICAgICAgICByZXR1cm5cbiAgICAgIH1cblxuICAgICAgZGVsZXRlIHRoaXMuX2xhc3RTZWxlY3RlZE9wdGlvblxuICAgIH1cblxuICAgIGlmIChvbGRJdGVtKSB7XG4gICAgICAvLyBSZW1vdmUgc2VsZWN0aW9uIG9uIHRoZSBlbGVtZW50XG4gICAgICBsZXQgb2xkVmFsdWUgPSBvbGRJdGVtLmdldEF0dHJpYnV0ZShcImRhdGEtdmFsdWVcIilcbiAgICAgIGxldCBvcHRFbGVtZW50ID0gZmluZCh0aGlzLmVsZW1lbnQub3B0aW9ucywgKHgpID0+ICF4LmRpc2FibGVkICYmIHgudmFsdWUgPT09IG9sZFZhbHVlKVxuXG4gICAgICBpZiAoIW9wdEVsZW1lbnQpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBUaGUgb3B0aW9uIHdpdGggdmFsdWUgJHtvbGRWYWx1ZX0gZG9lcyBub3QgZXhpc3RgKVxuICAgICAgfVxuXG4gICAgICAvLyBVbnNldCBTZWxlY3QgdmFsdWVcbiAgICAgIG9wdEVsZW1lbnQuc2VsZWN0ZWQgPSBmYWxzZVxuICAgICAgRG9tLnJlbW92ZUNsYXNzKG9sZEl0ZW0sIENMQVNTX0lURU1fU0VMRUNURUQpXG4gICAgfVxuXG4gICAgaWYgKCFpc0Rlc2VsZWN0KSB7IC8vIFNlbGVjdCBhbiBvcHRpb25cbiAgICAgIC8vIFNlbGVjdCBhIG5ldyBpdGVtXG4gICAgICBsZXQgbmV3VmFsdWUgPSBuZXdJdGVtLmdldEF0dHJpYnV0ZShcImRhdGEtdmFsdWVcIilcbiAgICAgIGxldCBvcHRFbGVtZW50ID0gZmluZCh0aGlzLmVsZW1lbnQub3B0aW9ucywgKHgpID0+ICF4LmRpc2FibGVkICYmIHgudmFsdWUgPT09IG5ld1ZhbHVlKVxuXG4gICAgICBpZiAoIW9wdEVsZW1lbnQpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBUaGUgb3B0aW9uIHdpdGggdmFsdWUgJHtuZXdWYWx1ZX0gZG9lcyBub3QgZXhpc3RgKVxuICAgICAgfVxuXG4gICAgICAvLyBTZXQgU2VsZWN0IHZhbHVlXG4gICAgICBvcHRFbGVtZW50LnNlbGVjdGVkID0gdHJ1ZVxuICAgICAgRG9tLmFkZENsYXNzKG5ld0l0ZW0sIENMQVNTX0lURU1fU0VMRUNURUQpXG5cbiAgICAgIC8vIFByZXNlcnZlIHNlbGVjdGlvblxuICAgICAgdGhpcy5fbGFzdFNlbGVjdGVkT3B0aW9uID0gb3B0RWxlbWVudFxuXG4gICAgfSBlbHNlIHsgLy8gRGVzZWxlY3QgYW4gb3B0aW9uXG4gICAgICAvLyBLZWVwIHRyYWNrIG9mIGZhbGxpbmcgYmFjayB0byB0aGUgcGxhY2Vob2xkZXIgKGlmIGFueSlcbiAgICAgIGlmICh0aGlzLl9wbGFjZWhvbGRlck9wdGlvbikge1xuICAgICAgICB0aGlzLl9sYXN0U2VsZWN0ZWRPcHRpb24gPSB0aGlzLl9wbGFjZWhvbGRlck9wdGlvblxuICAgICAgfVxuICAgIH1cblxuICAgIGxldCBoYXNTZWxlY3RlZEl0ZW1zID0gdHJ1ZVxuXG4gICAgaWYgKHRoaXMuX211bHRpc2VsZWN0aW9uID09PSBmYWxzZSAmJiBpc0Rlc2VsZWN0KSB7XG4gICAgICAvLyBIYW5kbGUgbm8gc2VsZWN0aW9uIGZvciBub24tbXVsdGlzZWxlY3Qgc3RhdGVzXG4gICAgICB0aGlzLl9wbGFjZWhvbGRlck9wdGlvbiEuc2VsZWN0ZWQgPSB0cnVlXG4gICAgICBoYXNTZWxlY3RlZEl0ZW1zID0gZmFsc2VcbiAgICB9XG5cbiAgICBpZiAodGhpcy5fbXVsdGlzZWxlY3Rpb24gPT09IHRydWUgJiYgdGhpcy5fZ2V0U2VsZWN0ZWRPcHRpb25zKCkubGVuZ3RoID09PSAwKSB7XG4gICAgICBoYXNTZWxlY3RlZEl0ZW1zID0gZmFsc2VcbiAgICB9XG5cbiAgICAvLyBSZXNldCB0aGUgZmlsdGVyIGlmIGZpbHRlcmFibGVcbiAgICBpZiAodGhpcy5fYWN0aXZlRmlsdGVyKSB7XG4gICAgICB0aGlzLl9jbGVhckZpbHRlcigpXG4gICAgfVxuXG4gICAgdGhpcy5fdXBkYXRlUGxhY2Vob2xkZXIoaGFzU2VsZWN0ZWRJdGVtcylcblxuICAgIC8vIERpc3BhdGNoIHRoZSBjaGFuZ2VkIGV2ZW50XG4gICAgdGhpcy5kaXNwYXRjaEV2ZW50KFwiY2hhbmdlXCIpXG5cbiAgICBpZiAoYXV0b0Nsb3NlICYmICFtdWx0aXNlbGVjdCkge1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuY2xvc2UoKVxuICAgICAgfSwgVElNRU9VVF9DTE9TRSlcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX3VwZGF0ZVBsYWNlaG9sZGVyKGhhc1NlbGVjdGVkSXRlbXM6IGJvb2xlYW4pIHtcbiAgICBsZXQgdGV4dCA9IHRoaXMuX3BsYWNlaG9sZGVyT3B0aW9uID8gRG9tLnRleHQodGhpcy5fcGxhY2Vob2xkZXJPcHRpb24pIDogXCIgXCJcblxuICAgIGlmIChoYXNTZWxlY3RlZEl0ZW1zID09PSB0cnVlKSB7XG4gICAgICBsZXQgc2VsZWN0ZWRJdGVtcyA9IHRoaXMuX2dldFNlbGVjdGVkT3B0aW9ucygpXG5cbiAgICAgIGlmIChzZWxlY3RlZEl0ZW1zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgdGV4dCA9IFwiXCJcbiAgICAgICAgZm9yIChsZXQgaXRlbSBvZiBzZWxlY3RlZEl0ZW1zKSB7XG4gICAgICAgICAgdGV4dCArPSBgJHtEb20udGV4dChpdGVtKX0sIGBcbiAgICAgICAgfVxuICAgICAgICB0ZXh0ID0gdGV4dC5zdWJzdHJpbmcoMCwgdGV4dC5sZW5ndGggLSAyKVxuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuX3NldFBsYWNlaG9sZGVyKHRleHQpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2dldFNlbGVjdGVkT3B0aW9ucygpIHtcbiAgICBsZXQgc2VsZWN0ZWRPcHRpb25zOiBIVE1MT3B0aW9uRWxlbWVudFtdID0gW11cbiAgICBpZiAodGhpcy5lbGVtZW50Lm9wdGlvbnMpIHtcbiAgICAgIFtdLmZvckVhY2guY2FsbCh0aGlzLmVsZW1lbnQub3B0aW9ucywgKChvcHRpb246IEhUTUxPcHRpb25FbGVtZW50KSA9PiB7XG4gICAgICAgIGlmIChvcHRpb24uc2VsZWN0ZWQgJiYgIW9wdGlvbi5kaXNhYmxlZCkge1xuICAgICAgICAgIHNlbGVjdGVkT3B0aW9ucy5wdXNoKG9wdGlvbilcbiAgICAgICAgfVxuICAgICAgfSkpXG4gICAgfVxuICAgIHJldHVybiBzZWxlY3RlZE9wdGlvbnNcbiAgfVxuXG4gIC8qKlxuICAgKiBDbG9uZSBhbGwgb2YgdGhlIGluaXRpYWxseSBzZXQgb3B0aW9ucyAoYW5kIG9wdGdyb3VwcykgYW5kIHJldHVybnMgdGhlbSBpbiBhIG5ldyBhcnJheS5cbiAgICogVGhpcyBzZXJ2ZXMgYXMgdGhlIGJhc2lzIGZvciBmaWx0ZXJpbmcuIElmIGEgZmlsdGVyIGlzIHByZXNlbnQsIGl0IHdpbGwgYmUgcmVzcGVjdGVkLlxuICAgKi9cbiAgcHJpdmF0ZSBnZXRJbml0aWFsT3B0aW9ucygpOiBFbGVtZW50W10ge1xuICAgIGNvbnN0IGZpbHRlciA9IHRoaXMuX2FjdGl2ZUZpbHRlciB8fCBcIlwiXG4gICAgY29uc3QgZmlsdGVyZWQ6IEVsZW1lbnRbXSA9IFtdXG4gICAgY29uc3QgaW5pdGlhbE9wdGlvbnMgPSB0aGlzLl9pbml0aWFsT3B0aW9uc1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpbml0aWFsT3B0aW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgY2hpbGQ6IEVsZW1lbnQgPSBpbml0aWFsT3B0aW9uc1tpXSBhcyBFbGVtZW50XG5cbiAgICAgIGlmICh0aGlzLl9pc09wdEdyb3VwKGNoaWxkKSkgeyAvLyBoYW5kbGUgPG9wdGdyb3VwPlxuICAgICAgICBjb25zdCBvcHRHcm91cENsb25lOiBFbGVtZW50ID0gY2hpbGQuY2xvbmVOb2RlKGZhbHNlKSBhcyBFbGVtZW50XG4gICAgICAgIGxldCBmb3VuZCA9IGZhbHNlXG5cbiAgICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCBjaGlsZC5jaGlsZHJlbi5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGNvbnN0IG9wdGlvbkNsb25lOiBFbGVtZW50ID0gY2hpbGQuY2hpbGRyZW5bal0uY2xvbmVOb2RlKHRydWUpIGFzIEVsZW1lbnRcblxuICAgICAgICAgIC8vIEFwcGVuZCBvbiBtYXRjaFxuICAgICAgICAgIGlmICh0aGlzLl9jb250YWluc1dvcmQob3B0aW9uQ2xvbmUuaW5uZXJIVE1MLCBmaWx0ZXIpKSB7XG4gICAgICAgICAgICBvcHRHcm91cENsb25lLmFwcGVuZENoaWxkKG9wdGlvbkNsb25lKVxuICAgICAgICAgICAgZm91bmQgPSB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gUHVzaCBpZiBhbnkgbWF0Y2hlcyBmb3VuZFxuICAgICAgICBpZiAoZm91bmQpIHtcbiAgICAgICAgICBmaWx0ZXJlZC5wdXNoKG9wdEdyb3VwQ2xvbmUpXG4gICAgICAgIH1cblxuICAgICAgfSBlbHNlIGlmICh0aGlzLl9pc09wdGlvbihjaGlsZCkpIHsgLy8gaGFuZGxlIDxvcHRpb24+XG4gICAgICAgIGNvbnN0IG9wdGlvbkNsb25lOiBFbGVtZW50ID0gY2hpbGQuY2xvbmVOb2RlKHRydWUpIGFzIEVsZW1lbnRcblxuICAgICAgICAvLyBQdXNoIG9uIG1hdGNoXG4gICAgICAgIGlmICh0aGlzLl9jb250YWluc1dvcmQob3B0aW9uQ2xvbmUuaW5uZXJIVE1MLCBmaWx0ZXIpKSB7XG4gICAgICAgICAgZmlsdGVyZWQucHVzaChvcHRpb25DbG9uZSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmaWx0ZXJlZFxuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdHJ1ZSBpZiBhIHRleHQgY29udGFpbnMgYSBnaXZlbiBrZXl3b3JkLCBlLmcuIGluIFwiY2FcIiBpbiBcIkNhclwiXG4gICAqL1xuICBwcml2YXRlIF9jb250YWluc1dvcmQodGV4dDogc3RyaW5nLCBrZXl3b3JkOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGV4dC50b0xvd2VyQ2FzZSgpLmluZGV4T2Yoa2V5d29yZC50b0xvd2VyQ2FzZSgpKSA+IC0xXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUZvY3VzKCkge1xuICAgIHRoaXMub3BlbigpXG4gICAgdGhpcy5fb3BlbkJ5Rm9jdXMgPSB0cnVlXG5cbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMuX29wZW5CeUZvY3VzID0gZmFsc2VcbiAgICB9LCBUSU1FT1VUX0JMVVIpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUJsdXIoKSB7XG4gICAgdGhpcy5jbG9zZSgpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgbGV0IGhhbmRsZWQgPSBmYWxzZVxuXG4gICAgaWYgKHRoaXMuX2xhc3RIYW5kbGVkRXZlbnQgPT09IGV2ZW50KSB7XG4gICAgICB0aGlzLl9sYXN0SGFuZGxlZEV2ZW50ID0gdW5kZWZpbmVkXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAodGhpcy5faXNCdXR0b25UYXJnZXQoZXZlbnQudGFyZ2V0ISkgJiYgdGhpcy5fb3BlbkJ5Rm9jdXMgPT09IGZhbHNlKSB7XG4gICAgICAvLyBoYW5kbGUgaGVhZGVyIGl0ZW0gY2xpY2tzIGFuZCB0b2dnbGUgZHJvcGRvd25cbiAgICAgIHRoaXMudG9nZ2xlKClcbiAgICAgIGhhbmRsZWQgPSB0cnVlXG4gICAgfVxuXG4gICAgbGV0IG5ld0l0ZW0gPSBldmVudC50YXJnZXQgYXMgRWxlbWVudFxuXG4gICAgaWYgKCFoYW5kbGVkICYmIERvbS5oYXNDbGFzcyhuZXdJdGVtLCBDTEFTU19JVEVNKSkge1xuICAgICAgLy8gaGFuZGxlIGNsaWNrcyBvbiBkcm9wZG93biBpdGVtc1xuICAgICAgdGhpcy5fc2VsZWN0ZWRJdGVtQ2hhbmdlZChuZXdJdGVtLCB0cnVlLCB0aGlzLl9tdWx0aXNlbGVjdGlvbilcbiAgICAgIGhhbmRsZWQgPSB0cnVlXG4gICAgfVxuXG4gICAgaWYgKGhhbmRsZWQpIHtcbiAgICAgIHRoaXMuX2xhc3RIYW5kbGVkRXZlbnQgPSBldmVudFxuICAgICAgcHJldmVudERlZmF1bHQoZXZlbnQpXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVXaW5kb3dDbGljayhldmVudDogTW91c2VFdmVudCkge1xuICAgIGlmICh0aGlzLl9pc0Ryb3Bkb3duVGFyZ2V0KGV2ZW50LnRhcmdldCEpIHx8IHRoaXMuX2lzQnV0dG9uVGFyZ2V0KGV2ZW50LnRhcmdldCEpKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICB0aGlzLmNsb3NlKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfZm9jdXNPcHRpb25TdGFydGluZ1dpdGgoa2V5Y29kZTogbnVtYmVyLCBzdGFydEluZGV4OiBudW1iZXIsIG9wdGlvbnM6IE5vZGVMaXN0T2Y8SFRNTEVsZW1lbnQ+KSB7XG4gICAgZm9yIChsZXQgaW5kZXggPSBzdGFydEluZGV4OyBpbmRleCA8IG9wdGlvbnMubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICBsZXQgaXRlbSA9IG5ldyBEb21FbGVtZW50KG9wdGlvbnNbaW5kZXhdKVxuICAgICAgbGV0IHZhbHVlID0gaXRlbS5pbm5lclRleHQudG9Mb3dlckNhc2UoKVxuXG4gICAgICBpZiAoaW5kZXggPiBvcHRpb25zLmxlbmd0aCkge1xuICAgICAgICBpbmRleCA9IDBcbiAgICAgIH1cblxuICAgICAgaWYgKHZhbHVlLnN0YXJ0c1dpdGgoSW5wdXRzLmdldEtleVZhbHVlKGtleWNvZGUpKSkge1xuICAgICAgICBsZXQgbmV3T3B0aW9uID0gbmV3IERvbUVsZW1lbnQob3B0aW9uc1tpbmRleF0pXG5cbiAgICAgICAgaWYgKCFuZXdPcHRpb24uaGFzQ2xhc3MoQ0xBU1NfSVRFTV9ESVNBQkxFRCkpIHtcbiAgICAgICAgICBzY3JvbGxJbnRvVmlldyhvcHRpb25zW2luZGV4XSlcbiAgICAgICAgICBuZXdPcHRpb24uYWRkQ2xhc3MoQ0xBU1NfSVRFTV9GT0NVU0VEKVxuICAgICAgICAgIHJldHVybiBuZXdPcHRpb25cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdW5kZWZpbmVkXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUtleWRvd24oZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcbiAgICBsZXQgZXZ0ID0gZXZlbnQgfHwgd2luZG93LmV2ZW50XG4gICAgbGV0IGtleWNvZGUgPSBldmVudC53aGljaCB8fCBldmVudC5rZXlDb2RlXG5cbiAgICBpZiAoa2V5Y29kZSA9PT0gSW5wdXRzLktFWV9FU0NBUEUpIHtcbiAgICAgIC8vIGhhbmRsZSBFc2NhcGUga2V5IChFU0MpXG4gICAgICBpZiAodGhpcy5pc09wZW4oKSkge1xuICAgICAgICB0aGlzLmNsb3NlKClcbiAgICAgIH1cbiAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAoa2V5Y29kZSA9PT0gSW5wdXRzLktFWV9BUlJPV19VUCB8fCBrZXljb2RlID09PSBJbnB1dHMuS0VZX0FSUk9XX0RPV04pIHtcbiAgICAgIC8vIFVwIGFuZCBkb3duIGFycm93c1xuXG4gICAgICBsZXQgb3B0aW9ucyA9IHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChgLiR7Q0xBU1NfSVRFTX1gKSBhcyBOb2RlTGlzdE9mPEhUTUxFbGVtZW50PlxuICAgICAgaWYgKG9wdGlvbnMubGVuZ3RoID4gMCkge1xuXG4gICAgICAgIGxldCBuZXdJbmRleCA9IDBcbiAgICAgICAgbGV0IG9sZE9wdGlvblxuXG4gICAgICAgIGxldCBmb2N1c2VkRWxlbWVudCA9IHRoaXMuX3dyYXBwZXJFbGVtZW50LmZpbmQoYC4ke0NMQVNTX0lURU1fRk9DVVNFRH1gKVxuICAgICAgICBsZXQgc2VhcmNoRm9yID0gZm9jdXNlZEVsZW1lbnQgPyBDTEFTU19JVEVNX0ZPQ1VTRUQgOiBDTEFTU19JVEVNX1NFTEVDVEVEXG5cbiAgICAgICAgbGV0IG5ld0VsZW1lbnRcblxuICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgb3B0aW9ucy5sZW5ndGg7IGluZGV4KyspIHtcbiAgICAgICAgICBsZXQgZGlyZWN0aW9uID0ga2V5Y29kZSA9PT0gSW5wdXRzLktFWV9BUlJPV19ET1dOID8gMSA6IC0xXG5cbiAgICAgICAgICBsZXQgaXRlbSA9IG5ldyBEb21FbGVtZW50KG9wdGlvbnNbaW5kZXhdKVxuXG4gICAgICAgICAgLy8gc2VhcmNoIGZvciBzZWxlY3RlZCBvciBmb2N1c2VkRWxlbWVudCBlbGVtZW50c1xuICAgICAgICAgIGlmIChpdGVtLmhhc0NsYXNzKHNlYXJjaEZvcikpIHtcbiAgICAgICAgICAgIG9sZE9wdGlvbiA9IGl0ZW1cbiAgICAgICAgICAgIG5ld0luZGV4ID0gaW5kZXhcblxuICAgICAgICAgICAgLy8gZ2V0IHRoZSBuZXh0IG5vdCBkaXNhYmxlZCBlbGVtZW50IGluIHRoZSBhcHByb3ByaWF0ZSBkaXJlY3Rpb25cbiAgICAgICAgICAgIGZvciAobGV0IGNvdW50ID0gMDsgY291bnQgPCBvcHRpb25zLmxlbmd0aDsgY291bnQrKykge1xuICAgICAgICAgICAgICBuZXdJbmRleCArPSBkaXJlY3Rpb25cbiAgICAgICAgICAgICAgbmV3SW5kZXggJT0gb3B0aW9ucy5sZW5ndGhcblxuICAgICAgICAgICAgICBpZiAobmV3SW5kZXggPCAwKSB7XG4gICAgICAgICAgICAgICAgbmV3SW5kZXggPSBvcHRpb25zLmxlbmd0aCAtIDFcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIG5ld0VsZW1lbnQgPSBuZXcgRG9tRWxlbWVudChvcHRpb25zW25ld0luZGV4XSlcbiAgICAgICAgICAgICAgaWYgKCFuZXdFbGVtZW50Lmhhc0NsYXNzKENMQVNTX0lURU1fRElTQUJMRUQpKSB7XG4gICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHNldCB0aGUgbmV3IGVsZW1lbnQgZm9jdXNlZFxuICAgICAgICBzY3JvbGxJbnRvVmlldyhvcHRpb25zW25ld0luZGV4XSlcbiAgICAgICAgbGV0IG5ld09wdGlvbiA9IG5ldyBEb21FbGVtZW50KG9wdGlvbnNbbmV3SW5kZXhdKVxuICAgICAgICBuZXdPcHRpb24uYWRkQ2xhc3MoQ0xBU1NfSVRFTV9GT0NVU0VEKVxuXG4gICAgICAgIGlmIChvbGRPcHRpb24pIHtcbiAgICAgICAgICBvbGRPcHRpb24ucmVtb3ZlQ2xhc3MoQ0xBU1NfSVRFTV9GT0NVU0VEKVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBpZiAoSW5wdXRzLmdldEtleVZhbHVlKGtleWNvZGUpICYmICF0aGlzLl9pc0ZpbHRlcmFibGUoKSkge1xuICAgICAgLy8gS2V5Ym9hcmQga2V5c1xuXG4gICAgICBsZXQgb3B0aW9ucyA9IHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChgLiR7Q0xBU1NfSVRFTX1gKSBhcyBOb2RlTGlzdE9mPEhUTUxFbGVtZW50PlxuICAgICAgaWYgKG9wdGlvbnMubGVuZ3RoID4gMCkge1xuXG4gICAgICAgIGxldCBvbGRGb2N1c0luZGV4ID0gMFxuICAgICAgICBsZXQgaGFzRm9jdXNlZE9wdGlvbiA9IGZhbHNlXG5cbiAgICAgICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IG9wdGlvbnMubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICAgICAgbGV0IGl0ZW0gPSBuZXcgRG9tRWxlbWVudChvcHRpb25zW2luZGV4XSlcblxuICAgICAgICAgIGlmIChpdGVtLmhhc0NsYXNzKENMQVNTX0lURU1fRk9DVVNFRCkpIHtcbiAgICAgICAgICAgIGl0ZW0ucmVtb3ZlQ2xhc3MoQ0xBU1NfSVRFTV9GT0NVU0VEKVxuXG4gICAgICAgICAgICBsZXQgdmFsdWUgPSBpdGVtLmlubmVyVGV4dC50b0xvd2VyQ2FzZSgpXG4gICAgICAgICAgICBpZiAodmFsdWUuc3RhcnRzV2l0aChJbnB1dHMuZ2V0S2V5VmFsdWUoa2V5Y29kZSkpKSB7XG4gICAgICAgICAgICAgIGhhc0ZvY3VzZWRPcHRpb24gPSB0cnVlXG4gICAgICAgICAgICAgIG9sZEZvY3VzSW5kZXggPSBpbmRleFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBuZXdPcHRpb24gPSB0aGlzLl9mb2N1c09wdGlvblN0YXJ0aW5nV2l0aChrZXljb2RlLCBoYXNGb2N1c2VkT3B0aW9uID8gb2xkRm9jdXNJbmRleCArIDEgOiAwLCBvcHRpb25zKVxuICAgICAgICBpZiAobmV3T3B0aW9uID09PSB1bmRlZmluZWQpe1xuICAgICAgICAgIHRoaXMuX2ZvY3VzT3B0aW9uU3RhcnRpbmdXaXRoKGtleWNvZGUsIDAsIG9wdGlvbnMpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KClcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmIChrZXljb2RlID09PSBJbnB1dHMuS0VZX0VOVEVSIHx8IGtleWNvZGUgPT09IElucHV0cy5LRVlfVEFCKSB7XG4gICAgICAvLyBIYW5kbGUgZW50ZXIgYW5kIHRhYiBrZXkgYnkgc2VsZWN0aW5nIHRoZSBjdXJyZW50bHkgZm9jdXNlZCBlbGVtZW50XG4gICAgICBsZXQgbmV3SXRlbSA9IHRoaXMuX2Ryb3Bkb3duRWxlbWVudC5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoYC4ke0NMQVNTX0lURU1fRk9DVVNFRH1gKSFcbiAgICAgIHRoaXMuX3NlbGVjdGVkSXRlbUNoYW5nZWQobmV3SXRlbSwgdHJ1ZSwgdGhpcy5fbXVsdGlzZWxlY3Rpb24pXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEZpcmVkIHdoZW4gdGhlIHVzZXIgcHJlc3NlcyBhIGtleSBpbiB0aGUgZmlsdGVyIGZpZWxkXG4gICAqL1xuICBwcml2YXRlIF9oYW5kbGVGaWx0ZXJLZXlkb3duKGU6IEtleWJvYXJkRXZlbnQpOiB2b2lkIHtcbiAgICBjb25zdCBrZXljb2RlID0gZS53aGljaCB8fCBlLmtleUNvZGVcblxuICAgIC8vIElmIHRoZSB1c2VyIGhpdHMgdGhlIGVudGVyIGtleSB3aGlsZSBmaWx0ZXJpbmcgYW5kIHRoZXJlJ3MgYSBzaW5nbGUgbWF0Y2gsIHNlbGVjdCBpdFxuICAgIGlmIChrZXljb2RlID09PSBJbnB1dHMuS0VZX0VOVEVSKSB7XG4gICAgICBjb25zdCBkcm9wZG93bkVsZW1lbnRzID0gdGhpcy5fZHJvcGRvd25FbGVtZW50LmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChgLiR7Q0xBU1NfSVRFTX1gKVxuXG4gICAgICBpZiAoZHJvcGRvd25FbGVtZW50cy5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgdGhpcy5fc2VsZWN0ZWRJdGVtQ2hhbmdlZChkcm9wZG93bkVsZW1lbnRzWzBdLCB0cnVlLCB0aGlzLl9tdWx0aXNlbGVjdGlvbilcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBGaXJlZCB3aGVuIHRoZSB1c2VyIHJlbGVhc2VzIGEga2V5IGluIHRoZSBmaWx0ZXIgZmllbGRcbiAgICovXG4gIHByaXZhdGUgX2hhbmRsZUZpbHRlcktleXVwKGU6IEtleWJvYXJkRXZlbnQpOiB2b2lkIHtcbiAgICBjb25zdCB0YXJnZXQgPSBlLnRhcmdldCBhcyBIVE1MSW5wdXRFbGVtZW50XG5cbiAgICAvLyBGaWx0ZXIgaGFzIGNoYW5nZWRcbiAgICBpZiAodGFyZ2V0LnZhbHVlICE9PSB0aGlzLl9hY3RpdmVGaWx0ZXIgJiYgdGFyZ2V0LnZhbHVlICE9PSB0aGlzLl9wbGFjZWhvbGRlclRleHQgJiYgdGFyZ2V0LnZhbHVlICE9PSB0aGlzLl9sYXN0U2VsZWN0ZWRPcHRpb24hLmlubmVySFRNTCkge1xuICAgICAgdGhpcy5fc2V0RmlsdGVyKHRhcmdldC52YWx1ZSlcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogRmlyZWQgd2hlbiB0aGUgdXNlciBmb2N1c3NlcyB0aGUgZmlsdGVyIGlucHV0IGZpZWxkXG4gICAqL1xuICBwcml2YXRlIF9oYW5kbGVGaWx0ZXJGb2N1cyhlOiBGb2N1c0V2ZW50KTogdm9pZCB7XG4gICAgY29uc3QgdGFyZ2V0ID0gZS50YXJnZXQgYXMgSFRNTElucHV0RWxlbWVudFxuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0YXJnZXQuc2VsZWN0KClcbiAgICB9KVxuICB9XG5cbiAgLyoqXG4gICAqIEZpbHRlcnMgdGhlIFNlbGVjdCBieSBhIGdpdmVuIGZpbHRlciBrZXl3b3JkXG4gICAqIEBwYXJhbSBmaWx0ZXIgS2V5d29yZCB0byBmaWx0ZXIgYnlcbiAgICovXG4gIHByaXZhdGUgX3NldEZpbHRlcihmaWx0ZXI6IHN0cmluZyA9IFwiXCIpOiB2b2lkIHtcbiAgICB0aGlzLl9hY3RpdmVGaWx0ZXIgPSAoZmlsdGVyLmxlbmd0aCA+PSB0aGlzLl9taW5GaWx0ZXJMZW5ndGgpID8gZmlsdGVyIDogXCJcIlxuICAgIHRoaXMuc2V0T3B0aW9ucyh0aGlzLmdldEluaXRpYWxPcHRpb25zKCkpXG4gIH1cblxuICAvKipcbiAgICogUmVzZXRzIHRoZSBmaWx0ZXJcbiAgICovXG4gIHByaXZhdGUgX2NsZWFyRmlsdGVyKCk6IHZvaWQge1xuICAgIGRlbGV0ZSB0aGlzLl9hY3RpdmVGaWx0ZXJcbiAgICB0aGlzLnNldE9wdGlvbnModGhpcy5nZXRJbml0aWFsT3B0aW9ucygpKVxuICB9XG5cbiAgLyoqXG4gICAqIFNldCBuZXcgY29udGVudCBhbmQgcmVsb2FkIHRoZSBTZWxlY3RcbiAgICogQHBhcmFtIGVsZW1lbnRzIEFycmF5IG9mIG5ldyBvcHRpb24gKG9yIG9wdGdyb3VwKSBlbGVtZW50cyB0byBkaXNwbGF5XG4gICAqL1xuICBwcml2YXRlIHNldE9wdGlvbnMob3B0aW9uczogRWxlbWVudFtdKTogdm9pZCB7XG4gICAgdGhpcy5fZW1wdHlOb2RlKHRoaXMuZWxlbWVudClcblxuICAgIG9wdGlvbnMuZm9yRWFjaCgob3B0aW9uKSA9PiB7XG4gICAgICB0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQob3B0aW9uKVxuICAgIH0pXG5cbiAgICAvLyBQcmVzZXJ2ZSBzZWxlY3RlZCB2YWx1ZSBpZiB0aGUgc2VsZWN0ZWRcbiAgICB0aGlzLmVsZW1lbnQudmFsdWUgPSB0aGlzLl9sYXN0U2VsZWN0ZWRPcHRpb24hLnZhbHVlXG5cbiAgICB0aGlzLnJlbG9hZCgpXG4gIH1cblxuICAvKipcbiAgICogQ2xlYXIgYWxsIGNoaWxkcmVuIG9mIGEgZ2l2ZW4gbm9kZVxuICAgKiBAcGFyYW0gbm9kZSBOb2RlXG4gICAqL1xuICBwcml2YXRlIF9lbXB0eU5vZGUobm9kZTogTm9kZSk6IHZvaWQge1xuICAgIHdoaWxlIChub2RlLmZpcnN0Q2hpbGQpIHtcbiAgICAgIG5vZGUucmVtb3ZlQ2hpbGQobm9kZS5maXJzdENoaWxkKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHdoZXRoZXIgYW4gb3B0aW9uIGlzIGEgcGxhY2Vob2xkZXIgb3B0aW9uXG4gICAqL1xuICBwcml2YXRlIF9pc1BsYWNlaG9sZGVyKG9wdGlvbjogSFRNTE9wdGlvbkVsZW1lbnQpOiBib29sZWFuIHtcbiAgICByZXR1cm4gb3B0aW9uLmhhc0F0dHJpYnV0ZShcImRpc2FibGVkXCIpICYmIG9wdGlvbi5oYXNBdHRyaWJ1dGUoXCJzZWxlY3RlZFwiKVxuICB9XG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBwbGFjZWhvbGRlciB2YWx1ZVxuICAgKiBAcGFyYW0gdGV4dCBDb250ZW50IG9mIHRoZSBwbGFjZWhvbGRlclxuICAgKi9cbiAgcHJvdGVjdGVkIF9zZXRQbGFjZWhvbGRlcih0ZXh0OiBzdHJpbmcpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50KSB7XG4gICAgICBpZiAodGhpcy5faXNGaWx0ZXJhYmxlKCkpIHtcbiAgICAgICAgKHRoaXMuX3BsYWNlaG9sZGVyRWxlbWVudCBhcyBEb21FbGVtZW50PEhUTUxJbnB1dEVsZW1lbnQ+KS5lbGVtZW50LnZhbHVlID0gdGV4dFxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50LnNldEh0bWwodGV4dClcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgdmFsdWUgb2YgdGhlIGN1cnJlbnRseSBzZWxlY3RlZCBvcHRpb24uXG4gICAqIElmIG11bHRpcGxlIHNlbGVjdGlvbiBpcyBlbmFibGVkIHRoaXMgcHJvcGVydHkgcmV0dXJucyBhbiBhcnJheSBvZiB2YWx1ZXMuXG4gICAqL1xuICBnZXQgdmFsdWUoKSB7XG4gICAgaWYgKHRoaXMuX211bHRpc2VsZWN0aW9uKSB7XG4gICAgICByZXR1cm4gdGhpcy5fZ2V0U2VsZWN0ZWRPcHRpb25zKCkubWFwKCh4KSA9PiB4LnZhbHVlKVxuICAgIH1cblxuICAgIGlmICh0aGlzLmVsZW1lbnQudmFsdWUgPT09IFwiXCIpIHtcbiAgICAgIHJldHVybiBudWxsXG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuZWxlbWVudC52YWx1ZVxuICB9XG5cbiAgLyoqXG4gICAqIEVuYWJsZXMgb3IgZGlzYWJsZXMgdGhlIHNlbGVjdCBjb21wb25lbnQgZGVwZW5kaW5nIG9uIHRoZVxuICAgKiAndmFsdWUnIHBhcmFtZXRlci5cbiAgICogQHBhcmFtIHt2YWx1ZX0gSWYgdHJ1ZSBkaXNhYmxlcyB0aGUgY29udHJvbDsgZmFsc2UgZW5hYmxlcyBpdC5cbiAgICovXG4gIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgdGhpcy5kaXNhYmxlKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lbmFibGUoKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZWxvYWRzIHRoZSBkcm9wZG93bidzIG9wdGlvbiBkYXRhIGRlZmluaXRpb25zIGZyb20gdGhlIERPTSBhbmQgdXBkYXRlc1xuICAgKiB0aGUgZ2VuZXJhdGVkIGRyb3Bkb3duIGRpc3BsYXkgaXRlbXMuXG4gICAqL1xuICByZWxvYWQoKSB7XG4gICAgLy8gUmVtb3ZlIGFsbCBleGlzdGluZyBjaGlsZCBlbGVtZW50c1xuICAgIHRoaXMuX2VtcHR5Tm9kZSh0aGlzLl9kcm9wZG93bkVsZW1lbnQuZWxlbWVudClcblxuICAgIGlmICh0aGlzLl9hY3RpdmVGaWx0ZXIgPT09IHVuZGVmaW5lZCkgeyAvLyBJZiB0aGUgdXNlciBpcyBmaWx0ZXJpbmcsIGxldCB0aGUgcGxhY2Vob2xkZXIgXCJpbnB1dFwiIGFsaXZlXG4gICAgICB0aGlzLl9zZXR1cFBsYWNlaG9sZGVyKClcbiAgICB9XG5cbiAgICB0aGlzLl9jcmVhdGVPcHRpb25zKHRoaXMuZWxlbWVudClcblxuICAgIHRoaXMuX3VwZGF0ZVNpemUoKVxuICAgIHRoaXMuX3VwZGF0ZU1lc3NhZ2UoKVxuXG4gICAgaWYgKCF0aGlzLl9pc0ZpbHRlcmFibGUoKSkge1xuICAgICAgdGhpcy5fdXBkYXRlUGxhY2Vob2xkZXIoISF0aGlzLnZhbHVlKVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBzZWxlY3QgY29udHJvbCB0byB0aGUgZW5hYmxlZCBzdGF0ZS5cbiAgICovXG4gIGVuYWJsZSgpIHtcbiAgICB0aGlzLmVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKFwiZGlzYWJsZWRcIilcbiAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5yZW1vdmVDbGFzcyhDTEFTU19ESVNBQkxFRClcblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fd2luZG93Q2xpY2tIYW5kbGVyKVxuXG4gICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2xpY2tIYW5kbGVyKVxuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgdGhpcy5fa2V5ZG93bkhhbmRsZXIpXG4gICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiZm9jdXNcIiwgdGhpcy5fZm9jdXNIYW5kbGVyKVxuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImJsdXJcIiwgdGhpcy5fYmx1ckhhbmRsZXIpXG4gIH1cblxuICAvKipcbiAgICogU2V0cyB0aGUgc2VsZWN0IGNvbnRyb2wgdG8gdGhlIGRpc2FibGVkIHN0YXRlLlxuICAgKi9cbiAgZGlzYWJsZSgpIHtcbiAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwiZGlzYWJsZWRcIiwgXCJcIilcbiAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hZGRDbGFzcyhDTEFTU19ESVNBQkxFRClcblxuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fd2luZG93Q2xpY2tIYW5kbGVyKVxuXG4gICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2xpY2tIYW5kbGVyKVxuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgdGhpcy5fa2V5ZG93bkhhbmRsZXIpXG4gICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiZm9jdXNcIiwgdGhpcy5fZm9jdXNIYW5kbGVyKVxuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImJsdXJcIiwgdGhpcy5fYmx1ckhhbmRsZXIpXG5cbiAgICB0aGlzLmNsb3NlKClcbiAgfVxuXG4gIC8qKlxuICAgKiBUb2dnbGVzIHRoZSBvcGVuL2Nsb3NlZCBzdGF0ZSBvZiB0aGUgc2VsZWN0IGRyb3Bkb3duLlxuICAgKi9cbiAgdG9nZ2xlKCkge1xuICAgIGlmICh0aGlzLmlzT3BlbigpKSB7XG4gICAgICB0aGlzLmNsb3NlKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vcGVuKClcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogR2V0cyBpZiB0aGUgc2VsZWN0IGRyb3Bkb3duIGlzIG9wZW4gb3IgY2xvc2VkLlxuICAgKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIG9wZW47IG90aGVyd2lzZSBmYWxzZS5cbiAgICovXG4gIGlzT3BlbigpIHtcbiAgICByZXR1cm4gdGhpcy5fd3JhcHBlckVsZW1lbnQuaGFzQ2xhc3MoQ0xBU1NfT1BFTilcbiAgfVxuXG4gIC8qKlxuICAgKiBPcGVucyB0aGUgc2VsZWN0IGRyb3Bkb3duLlxuICAgKi9cbiAgb3BlbigpIHtcbiAgICBpZiAoIXRoaXMuaXNPcGVuKCkpIHtcbiAgICAgIHRoaXMuX29wZW5CeUZvY3VzID0gZmFsc2VcblxuICAgICAgdGhpcy5fd3JhcHBlckVsZW1lbnQucmVtb3ZlQ2xhc3MoQ0xBU1NfQ0xPU0VEKVxuICAgICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuYWRkQ2xhc3MoQ0xBU1NfT1BFTilcblxuICAgICAgdGhpcy5fZHJvcGRvd25FbGVtZW50LmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2hhbmRsZURyb3Bkb3duQ2xpY2spXG4gICAgICB0aGlzLl9kcm9wZG93bkVsZW1lbnQuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwidGFwXCIsIHRoaXMuX2hhbmRsZURyb3Bkb3duQ2xpY2spXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENsb3NlcyB0aGUgc2VsZWN0IGRyb3Bkb3duLlxuICAgKi9cbiAgY2xvc2UoKSB7XG4gICAgaWYgKHRoaXMuaXNPcGVuKCkpIHtcbiAgICAgIHRoaXMuX29wZW5CeUZvY3VzID0gZmFsc2VcblxuICAgICAgdGhpcy5fd3JhcHBlckVsZW1lbnQucmVtb3ZlQ2xhc3MoQ0xBU1NfT1BFTilcbiAgICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmFkZENsYXNzKENMQVNTX0NMT1NFRClcblxuICAgICAgLy8gSWYgdGhlIFNlbGVjdCBpcyBmaWx0ZXJhYmxlIGFuZCB0aGVyZWZvcmUgaGFzIGFuIGlucHV0IGZpZWxkLFxuICAgICAgLy8gcmVzZXQgdGhlIHZhbHVlIG9mIGl0IHRvIHRoZSBjaG9zZW4gb3B0aW9uXG4gICAgICBpZiAodGhpcy5faXNGaWx0ZXJhYmxlKCkpIHtcbiAgICAgICAgLy8gVW5mb2N1cyBpbnB1dCBmaWVsZFxuICAgICAgICAodGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50LmVsZW1lbnQgYXMgSFRNTElucHV0RWxlbWVudCkuYmx1cigpXG5cbiAgICAgICAgaWYgKCF0aGlzLl9hY3RpdmVGaWx0ZXIgfHwgdGhpcy5fYWN0aXZlRmlsdGVyID09PSB0aGlzLl9sYXN0U2VsZWN0ZWRPcHRpb24hLmlubmVySFRNTCkge1xuICAgICAgICAgIHRoaXMuX3NldFBsYWNlaG9sZGVyKHRoaXMuX2xhc3RTZWxlY3RlZE9wdGlvbiEuaW5uZXJIVE1MKVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX2Ryb3Bkb3duRWxlbWVudC5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9oYW5kbGVEcm9wZG93bkNsaWNrKVxuICAgICAgdGhpcy5fZHJvcGRvd25FbGVtZW50LmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInRhcFwiLCB0aGlzLl9oYW5kbGVEcm9wZG93bkNsaWNrKVxuXG4gICAgICBsZXQgZm9jdXNlZEl0ZW0gPSB0aGlzLl93cmFwcGVyRWxlbWVudC5maW5kKGAuJHtDTEFTU19JVEVNX0ZPQ1VTRUR9YClcblxuICAgICAgaWYgKGZvY3VzZWRJdGVtKSB7XG4gICAgICAgIGZvY3VzZWRJdGVtLnJlbW92ZUNsYXNzKENMQVNTX0lURU1fRk9DVVNFRClcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyB0cnVlIHdoZW4gdGhlIGVsZW1lbnQgaGFzIHRoZSBmaWx0ZXIgbW9kaWZpZXIgY2xhc3NcbiAgICovXG4gIHByaXZhdGUgX2lzRmlsdGVyYWJsZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5fd3JhcHBlckVsZW1lbnQuaGFzQ2xhc3MoQ0xBU1NfRklMVEVSQUJMRSlcbiAgfVxuXG4gIC8qKlxuICAgKiBEZXN0cm95cyB0aGUgY29tcG9uZW50IGFuZCBjbGVhcnMgYWxsIHJlZmVyZW5jZXMuXG4gICAqL1xuICBkZXN0cm95KCkge1xuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fd2luZG93Q2xpY2tIYW5kbGVyKVxuXG4gICAgaWYgKHRoaXMuX2Ryb3Bkb3duRWxlbWVudCkge1xuICAgICAgdGhpcy5fZHJvcGRvd25FbGVtZW50LmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2hhbmRsZURyb3Bkb3duQ2xpY2spXG4gICAgICB0aGlzLl9kcm9wZG93bkVsZW1lbnQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwidGFwXCIsIHRoaXMuX2hhbmRsZURyb3Bkb3duQ2xpY2spXG5cbiAgICAgIHJlbW92ZSh0aGlzLl9kcm9wZG93bkVsZW1lbnQuZWxlbWVudCk7XG4gICAgICAodGhpcyBhcyBhbnkpLl9kcm9wZG93bkVsZW1lbnQgPSB1bmRlZmluZWRcbiAgICB9XG5cbiAgICBpZiAodGhpcy5fcGxhY2Vob2xkZXJFbGVtZW50KSB7XG4gICAgICB0aGlzLl9wbGFjZWhvbGRlckVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgdGhpcy5fZmlsdGVyS2V5ZG93bkhhbmRsZXIpXG4gICAgICB0aGlzLl9wbGFjZWhvbGRlckVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImtleXVwXCIsIHRoaXMuX2ZpbHRlcktleXVwSGFuZGxlcilcbiAgICAgIHRoaXMuX3BsYWNlaG9sZGVyRWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiZm9jdXNcIiwgdGhpcy5fZmlsdGVyRm9jdXNIYW5kbGVyKVxuICAgIH1cblxuICAgIGlmICh0aGlzLl93cmFwcGVyRWxlbWVudCkge1xuICAgICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fY2xpY2tIYW5kbGVyKVxuICAgICAgdGhpcy5fd3JhcHBlckVsZW1lbnQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLl9rZXlkb3duSGFuZGxlcilcbiAgICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImZvY3VzXCIsIHRoaXMuX2ZvY3VzSGFuZGxlcilcbiAgICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImJsdXJcIiwgdGhpcy5fYmx1ckhhbmRsZXIpO1xuXG4gICAgICAodGhpcyBhcyBhbnkpLl93cmFwcGVyRWxlbWVudCA9IHVuZGVmaW5lZFxuICAgIH1cblxuICAgIGlmICh0aGlzLl9zZWxlY3RCdXR0b25FbGVtZW50KSB7XG4gICAgICByZW1vdmUodGhpcy5fc2VsZWN0QnV0dG9uRWxlbWVudC5lbGVtZW50KTtcbiAgICAgICh0aGlzIGFzIGFueSkuX3NlbGVjdEJ1dHRvbkVsZW1lbnQgPSB1bmRlZmluZWRcbiAgICB9XG5cbiAgICB0aGlzLnJlbW92ZUNsYXNzKENMQVNTX0NMT1NFRClcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgc2VhcmNoQW5kSW5pdGlhbGl6ZTxIVE1MU2VsZWN0RWxlbWVudD4oXCJzZWxlY3RcIiwgKGUpID0+IHtcbiAgICBuZXcgU2VsZWN0KGUpXG4gIH0pXG59XG5cbmV4cG9ydCBkZWZhdWx0IFNlbGVjdFxuIl0sInNvdXJjZVJvb3QiOiIuLi8uLi8uLi8uLi8uLiJ9
