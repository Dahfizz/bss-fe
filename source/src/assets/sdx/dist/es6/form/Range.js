import * as tslib_1 from "tslib";
import { TweenLite, Power4 } from "gsap";
import { searchAndInitialize, clamp, preventDefault } from "../Utils";
import * as Inputs from "../Inputs";
import DomElement from "../DomElement";
var MARGIN_TICK = 32;
var CLASS_HTML5 = "html5";
var RANGE_LIGHT = "range--light";
var CLASS_CONTAINER = "range-container";
var CLASS_SLIDER = "range-slider";
var CLASS_ACTIVE = "range--active";
var CLASS_TRACK = "range-track";
var CLASS_TRACK_PROGRESS = "range-track__progress";
var CLASS_TICK = "range-tick";
var CLASS_TICK_LABEL = "range-tick__label";
var CLASS_TICK_ACTIVE = "range-tick--active";
var CLASS_THUMB = "range-thumb";
var CLASS_THUMB_VALUE = "range-thumb__value";
var CLASS_DISABLED = "range--disabled";
var CLASS_DRAGGING = "range--dragging";
/**
 * The range slider component definition.
 */
var Range = /** @class */ (function (_super) {
    tslib_1.__extends(Range, _super);
    function Range(element) {
        var _this = _super.call(this, element) || this;
        // Setup event context
        _this._downHandler = _this._handleDown.bind(_this);
        _this._moveHandler = _this._handleMove.bind(_this);
        _this._endHandler = _this._handleEnd.bind(_this);
        _this._keydownHandler = _this._handleKeydown.bind(_this);
        _this._focusHandler = _this._handleFocus.bind(_this);
        _this._blurHandler = _this._handleBlur.bind(_this);
        _this._resizeHandler = _this.layout.bind(_this);
        _this._initialize();
        if (_this.element.disabled) {
            _this.disable();
        }
        else {
            _this.enable();
        }
        return _this;
    }
    /**
     * Initializes the range slider component.
     *
     * This method inspects the select definition and its options and
     * generates new stylable DOM elements around the original range input-element
     * definitions.
     * @private
     */
    Range.prototype._initialize = function () {
        if (this.hasClass(CLASS_HTML5)) {
            // This element uses HTML5 styling, do not touch it...
            return;
        }
        this._wrapperElement = new DomElement(this.element.parentElement);
        this._rangeContainer = new DomElement("div")
            .addClass(CLASS_CONTAINER);
        this._rangeTrack = new DomElement("div")
            .addClass(CLASS_TRACK);
        // check if range--light slider then add progress
        if (this._wrapperElement.hasClass(RANGE_LIGHT)) {
            this._rangeProgress = new DomElement("div")
                .addClass(CLASS_TRACK_PROGRESS);
            this._rangeTrack.appendChild(this._rangeProgress);
        }
        this._rangeThumb = new DomElement("div")
            .addClass(CLASS_THUMB);
        this._ticksWrapper = new DomElement("div")
            .addClass(CLASS_SLIDER);
        this._rangeContainer.appendChild(this._rangeTrack);
        this._rangeContainer.appendChild(this._ticksWrapper);
        this._rangeContainer.appendChild(this._rangeThumb);
        // add container to wrapper
        this._wrapperElement.appendChild(this._rangeContainer);
        // get min & max definitions
        this._minValue = parseFloat(this.element.min) || 0;
        this._maxValue = parseFloat(this.element.max) || 1;
        // get the label/output format string
        this._formatter = window[this.getAttribute("formatter")];
        // get the output label and move it below the container
        if (this.element.id) {
            this._outputLabel = this._wrapperElement.find("output[for='" + this.element.id + "']");
            if (this._outputLabel) {
                this._wrapperElement.appendChild(this._outputLabel);
            }
        }
        if (!this.element.step) {
            // fix issues with float sliders if the step is undefined
            this.element.step = "any";
        }
        var options = this._getOptionsList();
        if (options && options.length > 1) {
            this._addTicks(options);
        }
        if (this._rangeContainer.element.querySelectorAll("." + CLASS_TICK_LABEL).length <= 1) {
            this._thumbValue = new DomElement("div")
                .addClass(CLASS_THUMB_VALUE);
            this._rangeThumb.appendChild(this._thumbValue);
        }
        this._trackValueTotal = this._maxValue - this._minValue;
        this.layout();
        this._updateTickState();
        // Apply the tab index
        var tabIndex = this.element.getAttribute("tabindex");
        if (tabIndex) {
            this._rangeContainer.setAttribute("tabindex", tabIndex);
        }
        window.addEventListener("resize", this._resizeHandler);
        window.addEventListener("orientationchange", this._resizeHandler);
    };
    Range.prototype._getOptionsList = function () {
        var e_1, _a;
        var options = [];
        var listId = this.getAttribute("list");
        if (listId) {
            var dataList = document.querySelector("#" + listId);
            if (dataList) {
                try {
                    for (var _b = tslib_1.__values(dataList.querySelectorAll("option")), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var entry = _c.value;
                        var value = parseFloat(entry.innerText);
                        var label = entry.getAttribute("label") || parseFloat(value.toFixed(2));
                        options.push({
                            value: value,
                            label: label
                        });
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
        }
        // Sort the list to enable snapping
        options = options.sort(function (a, b) { return a.value - b.value; });
        if (options.length > 1) {
            this._minValue = Number.MAX_VALUE;
            this._maxValue = Number.MIN_VALUE;
            for (var i = 0; i < options.length; i++) {
                this._minValue = Math.min(this._minValue, options[i].value);
                this._maxValue = Math.max(this._maxValue, options[i].value);
            }
        }
        return options;
    };
    Range.prototype._addTicks = function (dataItems) {
        var e_2, _a;
        try {
            for (var dataItems_1 = tslib_1.__values(dataItems), dataItems_1_1 = dataItems_1.next(); !dataItems_1_1.done; dataItems_1_1 = dataItems_1.next()) {
                var entry = dataItems_1_1.value;
                var tickElement = new DomElement("div")
                    .setAttribute("data-value", String(entry.value))
                    .addClass(CLASS_TICK);
                var tickLabel = new DomElement("span")
                    .addClass(CLASS_TICK_LABEL)
                    .setHtml(String(entry.label));
                tickElement.appendChild(tickLabel);
                this._ticksWrapper.appendChild(tickElement);
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (dataItems_1_1 && !dataItems_1_1.done && (_a = dataItems_1.return)) _a.call(dataItems_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
    };
    Range.prototype._isEventOnLabel = function (event) {
        return event.target.classList.contains(CLASS_TICK_LABEL);
    };
    Range.prototype._handleDown = function (event) {
        this._wrapperElement.addClass(CLASS_DRAGGING);
        this._rangeContainer.element.addEventListener("mouseup", this._endHandler);
        document.addEventListener("mousemove", this._moveHandler);
        document.addEventListener("mouseup", this._endHandler);
        this._rangeContainer.element.addEventListener("touchmove", this._moveHandler);
        document.addEventListener("touchend", this._endHandler);
        // Ignore clicks directly on the thumb
        if (event.target !== this._rangeThumb.element && !this._isEventOnLabel(event)) {
            var pos = this._getRelativePosition(event);
            this._setPosition(pos, true, false, false);
        }
    };
    Range.prototype._handleMove = function (event) {
        preventDefault(event);
        this._unfocus();
        if (!this._isEventOnLabel(event)) {
            var pos = this._getRelativePosition(event);
            this._setPosition(pos, true, false, false);
        }
    };
    Range.prototype._handleEnd = function (event) {
        this._wrapperElement.removeClass(CLASS_DRAGGING);
        this._rangeContainer.element.removeEventListener("mouseup", this._endHandler);
        document.removeEventListener("mouseup", this._endHandler);
        document.removeEventListener("mousemove", this._moveHandler);
        this._rangeContainer.element.removeEventListener("touchmove", this._moveHandler);
        document.removeEventListener("touchend", this._endHandler);
        var pos = this._getRelativePosition(event);
        this._setPosition(pos, true, true, true);
        this._handleBlur();
    };
    Range.prototype._handleKeydown = function (event) {
        var keycode = event.which || event.keyCode;
        if (keycode === Inputs.KEY_ESCAPE) {
            // handle Escape key (ESC)
            this._rangeContainer.element.blur();
            return;
        }
        var isUp = keycode === Inputs.KEY_ARROW_UP || keycode === Inputs.KEY_ARROW_RIGHT
            || keycode === Inputs.KEY_PAGE_UP;
        var isDown = keycode === Inputs.KEY_ARROW_DOWN || keycode === Inputs.KEY_ARROW_LEFT
            || keycode === Inputs.KEY_PAGE_DOWN;
        if (isUp || isDown) {
            event.preventDefault();
            var direction = isDown ? -1 : 1;
            // make a larger step if its the vertical arrow or page keys
            if (keycode === Inputs.KEY_ARROW_UP || keycode === Inputs.KEY_ARROW_DOWN ||
                keycode === Inputs.KEY_PAGE_UP || keycode === Inputs.KEY_PAGE_DOWN) {
                direction *= 10;
            }
            var val = this.value;
            if (this._ticksWrapper.element.childNodes.length > 1) {
                val = this._getNextValue(val, direction);
            }
            else {
                var step = this.element.step;
                if (!step || step === "any") {
                    step = "0.1";
                }
                var newVal = val + (parseFloat(step) * direction);
                val = newVal;
            }
            this._setValue(val, true, true);
            return;
        }
    };
    Range.prototype._handleFocus = function () {
        this._rangeContainer.addClass(CLASS_ACTIVE);
    };
    Range.prototype._handleBlur = function () {
        this._rangeContainer.removeClass(CLASS_ACTIVE);
    };
    Range.prototype._unfocus = function () {
        if (document.selection) {
            document.selection.empty();
        }
        else {
            window.getSelection().removeAllRanges();
        }
    };
    Range.prototype._getRelativePosition = function (event) {
        var pageX;
        if ("pageX" in event) {
            pageX = event.pageX;
        }
        else {
            pageX = (event.touches[0] || event.changedTouches[0]).pageX;
        }
        return pageX - this._trackLeftPosition + this._grabPosition;
    };
    /**
     * Validates and updates the position and sets the corresponding value on the slider.
     * @param {position} the new position to set.
     * @param {updateValue} true if the value should be updated as well; otherwise false.
     * @param {snap} true if snapping should be used; otherwise false.
     * @param {animate} true if the UI update should be animated; otherwise false.
     * @private
     */
    Range.prototype._setPosition = function (position, updateValue, snap, animate) {
        if (updateValue === void 0) { updateValue = true; }
        if (snap === void 0) { snap = false; }
        if (animate === void 0) { animate = true; }
        if (position === undefined || position === null || Number.isNaN(position)) {
            throw new Error("Position is not a number");
        }
        // Clamp to min and max range
        var newPos = clamp(position, this._trackPositionMin, this._trackPositionMax);
        if (updateValue) {
            var value = (this._trackValueTotal / this._trackWidth) * newPos + this._minValue;
            if (this._ticksWrapper.element.childNodes.length > 1 && snap) {
                var snapPos = this._getSnapPosition(newPos);
                newPos = snapPos.position;
                value = snapPos.value;
            }
            else if (this.element.step && this.element.step !== "any") {
                var step = parseFloat(this.element.step);
                value = Math.round(value / step) * step;
            }
            this._setValue(value, false, false);
        }
        if (animate && updateValue) {
            this._updateTickState();
        }
        if (animate) {
            TweenLite.to(this._rangeThumb.element, 0.2, {
                left: newPos,
                ease: Power4.easeInOut
            });
            if (this._rangeProgress) {
                TweenLite.to(this._rangeProgress.element, 0.2, {
                    width: newPos,
                    ease: Power4.easeInOut
                });
            }
        }
        else {
            TweenLite.set(this._rangeThumb.element, { left: newPos });
            if (this._rangeProgress) {
                TweenLite.set(this._rangeProgress.element, { width: newPos });
            }
        }
    };
    /**
     * Gets the snap value corresponding to the given value.
     * @param {value} the target value.
     * @returns an object containing the snap position and the corresponding value.
     * @private
     */
    Range.prototype._getSnapValue = function (value) {
        var ticks = this._ticksWrapper.element.children;
        var currentPosition = 0;
        for (var i = 0; i < ticks.length; i++) {
            var currentElement = new DomElement(ticks[i]);
            var currentValue = parseFloat(currentElement.getAttribute("data-value"));
            var currentWidth = currentElement.element.clientWidth;
            var nextElement = void 0;
            var nextValue = Number.MAX_VALUE;
            if (i < ticks.length - 1) {
                nextElement = new DomElement(ticks[i + 1]);
                nextValue = parseFloat(nextElement.getAttribute("data-value"));
            }
            // left most element
            if (i === 0 && value <= currentValue) {
                return {
                    value: currentValue,
                    position: MARGIN_TICK - this._grabPosition
                };
            }
            // right most element
            if (!nextElement && value >= currentValue) {
                return {
                    value: currentValue,
                    position: currentPosition + (currentWidth - MARGIN_TICK) - this._grabPosition - 1
                };
            }
            if (value >= currentValue && value < nextValue) {
                return {
                    value: currentValue,
                    position: currentPosition + (0.5 * currentWidth) - this._grabPosition
                };
            }
            currentPosition += currentWidth;
        }
        throw new Error("Could not determine snap value");
    };
    /**
     * Gets the snap position corresponding to the given position.
     * @param {position} the target position.
     * @returns an object containing the snap position and the corresponding value.
     * @private
     */
    Range.prototype._getSnapPosition = function (position) {
        if (position === undefined || position === null || Number.isNaN(position)) {
            throw new Error("position is not a number");
        }
        var ticks = this._ticksWrapper.element.children;
        var currentPosition = 0;
        for (var i = 0; i < ticks.length; i++) {
            var currentElement = new DomElement(ticks[i]);
            var currentValue = parseFloat(currentElement.getAttribute("data-value"));
            var currentWidth = currentElement.element.clientWidth;
            var nextElement = void 0;
            if (i < ticks.length - 1) {
                nextElement = new DomElement(ticks[i + 1]);
            }
            // left most element
            if (i === 0 && position <= currentPosition + currentWidth) {
                return {
                    value: currentValue,
                    position: MARGIN_TICK - this._grabPosition
                };
            }
            // right most element
            if (!nextElement && position >= currentPosition) {
                return {
                    value: currentValue,
                    position: currentPosition + (currentWidth - MARGIN_TICK) - this._grabPosition - 1
                };
            }
            if (position >= currentPosition && position < (currentPosition + currentWidth)) {
                return {
                    value: currentValue,
                    position: currentPosition + (0.5 * currentWidth) - this._grabPosition
                };
            }
            currentPosition += currentWidth;
        }
        throw new Error("Could not determine snap position");
    };
    /**
     * Gets the next value in the given direction with regards to snapping.
     * @param {value} The current value.
     * @param {direction} The direction (positive or negative integer).
     * @returns The next value.
     * @private
     */
    Range.prototype._getNextValue = function (value, direction) {
        var ticks = this._ticksWrapper.element.children;
        for (var i = 0; i < ticks.length; i++) {
            var currentElement = new DomElement(ticks[i]);
            var currentVal = parseFloat(currentElement.getAttribute("data-value"));
            if (value === currentVal) {
                var index = clamp(i + direction, 0, ticks.length - 1);
                value = parseFloat(ticks[index].getAttribute("data-value"));
            }
        }
        return value;
    };
    Range.prototype._updateTickState = function () {
        if (this._ticksWrapper.element.childNodes.length > 1) {
            var activeTick = this._ticksWrapper.find("." + CLASS_TICK_ACTIVE);
            if (activeTick) {
                activeTick.removeClass(CLASS_TICK_ACTIVE);
            }
            var newActiveTick = this._ticksWrapper.find("." + CLASS_TICK + "[data-value='" + this.value + "']");
            if (newActiveTick) {
                newActiveTick.addClass(CLASS_TICK_ACTIVE);
            }
        }
    };
    Range.prototype._adjustTickLabelPosition = function (tickItem, left) {
        var label = new DomElement(tickItem.querySelector("." + CLASS_TICK_LABEL));
        var dummyElement = new DomElement("span")
            .addClass(CLASS_TICK_LABEL)
            .setAttribute("style", "visibility: hidden; display: inline-block;")
            .setHtml(label.innerText);
        this._rangeContainer.appendChild(dummyElement);
        var width = dummyElement.element.clientWidth / 2;
        this._rangeContainer.removeChild(dummyElement);
        var floatPosition = left ? "left" : "right";
        if (width < MARGIN_TICK) {
            // center small items on the tick
            label.setAttribute("style", floatPosition + ": " + (MARGIN_TICK - Math.floor(width)) + "px; text-align: " + floatPosition + ";");
        }
    };
    Range.prototype._formatOutput = function (value, short) {
        if (this._formatter) {
            return this._formatter(value, short);
        }
        var str = parseFloat(value.toFixed(2));
        return str.toString();
    };
    /**
     * Validates and updates the range value.
     * @param {value} the new value to set.
     * @param {update} true if the UI should be updated; otherwise false.
     * @param {animate} true if the UI update should be animated; otherwise false.
     * @private
     */
    Range.prototype._setValue = function (value, update, animate) {
        if (update === void 0) { update = true; }
        if (animate === void 0) { animate = false; }
        var val = clamp(value, this._minValue, this._maxValue);
        var position;
        if (this._ticksWrapper.element.childNodes.length > 1) {
            var snapValue = this._getSnapValue(val);
            position = snapValue.position;
            val = snapValue.value;
        }
        else {
            position = (this._trackWidth / this._trackValueTotal) * (value - this._minValue);
        }
        this.element.value = String(val);
        if (this._thumbValue) {
            this._thumbValue.setHtml(this._formatOutput(val, true));
        }
        if (this._outputLabel) {
            this._outputLabel.setHtml(this._formatOutput(val, false));
        }
        if (update) {
            this._setPosition(position, false, false, animate);
            this._updateTickState();
        }
        this.dispatchEvent("input");
    };
    Object.defineProperty(Range.prototype, "value", {
        /**
         * Gets the current value.
         */
        get: function () {
            return parseFloat(this.element.value);
        },
        /**
         * Sets the value of the range slider.
         */
        set: function (value) {
            this._setValue(value, true, true);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Force the component to re-layout itself.
     */
    Range.prototype.layout = function () {
        this._grabPosition = Math.round(this._rangeThumb.element.offsetWidth / 2);
        var tickItems = this._rangeContainer.element.querySelectorAll("." + CLASS_TICK);
        var ticksOffset = tickItems && tickItems.length > 0 ? (2 * MARGIN_TICK) : MARGIN_TICK;
        this._trackWidth = this._rangeTrack.element.offsetWidth - ticksOffset;
        this._trackPositionMin = 0;
        this._trackPositionMax = this._rangeTrack.element.clientWidth - this._rangeThumb.element.offsetWidth + 1;
        this._trackLeftPosition = this._rangeTrack.element.getBoundingClientRect().left + MARGIN_TICK;
        var itemCount = tickItems.length - 1;
        this._itemWidth = this._trackWidth / itemCount;
        var outerItemsWidth = (this._itemWidth * 0.5) + MARGIN_TICK;
        for (var i = 0; i <= itemCount; i++) {
            var width = this._itemWidth;
            if (i === 0 || i === itemCount) {
                width = outerItemsWidth;
            }
            var item = new DomElement(tickItems[i]);
            item.setAttribute("style", "width: " + Math.floor(width) + "px;");
        }
        // adjust first and last label positions
        if (tickItems.length > 1) {
            this._adjustTickLabelPosition(tickItems[0], true);
            this._adjustTickLabelPosition(tickItems[tickItems.length - 1], false);
        }
        // update the value
        this._setValue(parseFloat(this.element.value), true, false);
    };
    /**
     * Destroys the components and frees all references.
     */
    Range.prototype.destroy = function () {
        window.removeEventListener("resize", this._resizeHandler);
        window.removeEventListener("orientationchange", this._resizeHandler);
        this._downHandler = null;
        this._moveHandler = null;
        this._endHandler = null;
        this._focusHandler = null;
        this._blurHandler = null;
        this.element = null;
        this._rangeContainer = null;
        this._wrapperElement = null;
    };
    /**
     * @deprecated use destroy() instead.
     * @todo remove in version 2.0.0
     */
    Range.prototype.destoy = function () {
        this.destroy();
    };
    /**
     * Sets the component to the enabled state.
     */
    Range.prototype.enable = function () {
        this.element.removeAttribute("disabled");
        this._wrapperElement.removeClass(CLASS_DISABLED);
        this._rangeContainer.element.addEventListener("mousedown", this._downHandler);
        this._rangeContainer.element.addEventListener("touchstart", this._downHandler);
        this._rangeContainer.element.addEventListener("keydown", this._keydownHandler);
        this._rangeContainer.element.addEventListener("focus", this._focusHandler);
        this._rangeContainer.element.addEventListener("blur", this._blurHandler);
    };
    /**
     * Sets the component to the disabled state.
     */
    Range.prototype.disable = function () {
        this.element.setAttribute("disabled", "");
        this._wrapperElement.addClass(CLASS_DISABLED);
        this._rangeContainer.element.removeEventListener("mousedown", this._downHandler);
        this._rangeContainer.element.removeEventListener("mouseup", this._endHandler);
        this._rangeContainer.element.removeEventListener("mousemove", this._moveHandler);
        this._rangeContainer.element.removeEventListener("touchstart", this._downHandler);
        this._rangeContainer.element.removeEventListener("focus", this._focusHandler);
        this._rangeContainer.element.removeEventListener("blur", this._blurHandler);
    };
    return Range;
}(DomElement));
export function init() {
    searchAndInitialize("input[type='range']", function (e) {
        new Range(e);
    });
}
export default Range;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL2Zvcm0vUmFuZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sTUFBTSxDQUFBO0FBRXhDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxLQUFLLEVBQUUsY0FBYyxFQUFFLE1BQU0sVUFBVSxDQUFBO0FBQ3JFLE9BQU8sS0FBSyxNQUFNLE1BQU0sV0FBVyxDQUFBO0FBQ25DLE9BQU8sVUFBVSxNQUFNLGVBQWUsQ0FBQTtBQUV0QyxJQUFNLFdBQVcsR0FBRyxFQUFFLENBQUE7QUFDdEIsSUFBTSxXQUFXLEdBQUcsT0FBTyxDQUFBO0FBQzNCLElBQU0sV0FBVyxHQUFHLGNBQWMsQ0FBQTtBQUVsQyxJQUFNLGVBQWUsR0FBRyxpQkFBaUIsQ0FBQTtBQUN6QyxJQUFNLFlBQVksR0FBRyxjQUFjLENBQUE7QUFDbkMsSUFBTSxZQUFZLEdBQUcsZUFBZSxDQUFBO0FBRXBDLElBQU0sV0FBVyxHQUFHLGFBQWEsQ0FBQTtBQUNqQyxJQUFNLG9CQUFvQixHQUFHLHVCQUF1QixDQUFBO0FBRXBELElBQU0sVUFBVSxHQUFHLFlBQVksQ0FBQTtBQUMvQixJQUFNLGdCQUFnQixHQUFHLG1CQUFtQixDQUFBO0FBQzVDLElBQU0saUJBQWlCLEdBQUcsb0JBQW9CLENBQUE7QUFFOUMsSUFBTSxXQUFXLEdBQUcsYUFBYSxDQUFBO0FBQ2pDLElBQU0saUJBQWlCLEdBQUcsb0JBQW9CLENBQUE7QUFDOUMsSUFBTSxjQUFjLEdBQUcsaUJBQWlCLENBQUE7QUFFeEMsSUFBTSxjQUFjLEdBQUcsaUJBQWlCLENBQUE7QUFXeEM7O0dBRUc7QUFDSDtJQUFvQixpQ0FBNEI7SUErQjlDLGVBQVksT0FBeUI7UUFBckMsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FtQmY7UUFqQkMsc0JBQXNCO1FBQ3RCLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDL0MsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUMvQyxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQzdDLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFFckQsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQTtRQUNqRCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFBO1FBQy9DLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFFNUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFBO1FBRWxCLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDekIsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFBO1NBQ2Y7YUFBTTtZQUNMLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNkOztJQUNILENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ08sMkJBQVcsR0FBckI7UUFFRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDOUIsc0RBQXNEO1lBQ3RELE9BQU07U0FDUDtRQUVELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFjLENBQUMsQ0FBQTtRQUVsRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksVUFBVSxDQUFpQixLQUFLLENBQUM7YUFDekQsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBRTVCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxVQUFVLENBQWlCLEtBQUssQ0FBQzthQUNyRCxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFeEIsaURBQWlEO1FBQ2pELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLFVBQVUsQ0FBaUIsS0FBSyxDQUFDO2lCQUN4RCxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtZQUVqQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7U0FDbEQ7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksVUFBVSxDQUFpQixLQUFLLENBQUM7YUFDckQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRXhCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxVQUFVLENBQWlCLEtBQUssQ0FBQzthQUN2RCxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUE7UUFFekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBQ2xELElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUNwRCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFbEQsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTtRQUV0RCw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDbEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7UUFFbEQscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxVQUFVLEdBQUksTUFBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFFLENBQUMsQ0FBQTtRQUVsRSx1REFBdUQ7UUFDdkQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGlCQUFlLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxPQUFJLENBQUMsQ0FBQTtZQUNqRixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTthQUNwRDtTQUNGO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFO1lBQ3RCLHlEQUF5RDtZQUN6RCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUE7U0FDMUI7UUFFRCxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7UUFDdEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUN4QjtRQUVELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsTUFBSSxnQkFBa0IsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDckYsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLFVBQVUsQ0FBaUIsS0FBSyxDQUFDO2lCQUNyRCxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtZQUU5QixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7U0FDL0M7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFBO1FBQ3ZELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtRQUViLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO1FBRXZCLHNCQUFzQjtRQUN0QixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQTtRQUN0RCxJQUFJLFFBQVEsRUFBRTtZQUNaLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQTtTQUN4RDtRQUVELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3RELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7SUFDbkUsQ0FBQztJQUVTLCtCQUFlLEdBQXpCOztRQUNFLElBQUksT0FBTyxHQUFhLEVBQUUsQ0FBQTtRQUUxQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ3RDLElBQUksTUFBTSxFQUFFO1lBQ1YsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFJLE1BQVEsQ0FBQyxDQUFBO1lBQ25ELElBQUksUUFBUSxFQUFFOztvQkFDWixLQUFrQixJQUFBLEtBQUEsaUJBQUEsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFBLGdCQUFBLDRCQUFFO3dCQUFsRCxJQUFJLEtBQUssV0FBQTt3QkFDWixJQUFJLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFBO3dCQUN2QyxJQUFJLEtBQUssR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7d0JBRXZFLE9BQU8sQ0FBQyxJQUFJLENBQUM7NEJBQ1gsS0FBSyxPQUFBOzRCQUNMLEtBQUssT0FBQTt5QkFDTixDQUFDLENBQUE7cUJBQ0g7Ozs7Ozs7OzthQUNGO1NBQ0Y7UUFFRCxtQ0FBbUM7UUFDbkMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxFQUFqQixDQUFpQixDQUFDLENBQUE7UUFFbkQsSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUE7WUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFBO1lBRWpDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBQzNELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQTthQUM1RDtTQUNGO1FBRUQsT0FBTyxPQUFPLENBQUE7SUFDaEIsQ0FBQztJQUVTLHlCQUFTLEdBQW5CLFVBQW9CLFNBQW1COzs7WUFDckMsS0FBa0IsSUFBQSxjQUFBLGlCQUFBLFNBQVMsQ0FBQSxvQ0FBQSwyREFBRTtnQkFBeEIsSUFBSSxLQUFLLHNCQUFBO2dCQUNaLElBQUksV0FBVyxHQUFHLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQztxQkFDcEMsWUFBWSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUE7Z0JBRXZCLElBQUksU0FBUyxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQztxQkFDbkMsUUFBUSxDQUFDLGdCQUFnQixDQUFDO3FCQUMxQixPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO2dCQUUvQixXQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFBO2dCQUNsQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQTthQUM1Qzs7Ozs7Ozs7O0lBQ0gsQ0FBQztJQUVTLCtCQUFlLEdBQXpCLFVBQTBCLEtBQVk7UUFDcEMsT0FBUSxLQUFLLENBQUMsTUFBa0IsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUE7SUFDdkUsQ0FBQztJQUVTLDJCQUFXLEdBQXJCLFVBQXNCLEtBQThCO1FBQ2xELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBRTdDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7UUFDMUUsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDekQsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7UUFFdEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUM3RSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUV2RCxzQ0FBc0M7UUFDdEMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUM3RSxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDMUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQTtTQUMzQztJQUNILENBQUM7SUFFUywyQkFBVyxHQUFyQixVQUFzQixLQUE4QjtRQUNsRCxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDckIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO1FBRWYsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQzFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUE7U0FDM0M7SUFDSCxDQUFDO0lBRVMsMEJBQVUsR0FBcEIsVUFBcUIsS0FBOEI7UUFDakQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUE7UUFFaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUM3RSxRQUFRLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUN6RCxRQUFRLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUU1RCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBQ2hGLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO1FBRTFELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUMxQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ3hDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTtJQUNwQixDQUFDO0lBRVMsOEJBQWMsR0FBeEIsVUFBeUIsS0FBb0I7UUFDM0MsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFBO1FBRTFDLElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxVQUFVLEVBQUU7WUFDakMsMEJBQTBCO1lBQzFCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFBO1lBQ25DLE9BQU07U0FDUDtRQUVELElBQU0sSUFBSSxHQUFHLE9BQU8sS0FBSyxNQUFNLENBQUMsWUFBWSxJQUFJLE9BQU8sS0FBSyxNQUFNLENBQUMsZUFBZTtlQUM3RSxPQUFPLEtBQUssTUFBTSxDQUFDLFdBQVcsQ0FBQTtRQUVuQyxJQUFNLE1BQU0sR0FBRyxPQUFPLEtBQUssTUFBTSxDQUFDLGNBQWMsSUFBSSxPQUFPLEtBQUssTUFBTSxDQUFDLGNBQWM7ZUFDaEYsT0FBTyxLQUFLLE1BQU0sQ0FBQyxhQUFhLENBQUE7UUFFckMsSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO1lBQ2xCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQTtZQUV0QixJQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFFL0IsNERBQTREO1lBQzVELElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxZQUFZLElBQUksT0FBTyxLQUFLLE1BQU0sQ0FBQyxjQUFjO2dCQUN0RSxPQUFPLEtBQUssTUFBTSxDQUFDLFdBQVcsSUFBSSxPQUFPLEtBQUssTUFBTSxDQUFDLGFBQWEsRUFBRTtnQkFDcEUsU0FBUyxJQUFJLEVBQUUsQ0FBQTthQUNoQjtZQUVELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7WUFDcEIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDcEQsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFBO2FBQ3pDO2lCQUFNO2dCQUVMLElBQUksSUFBSSxHQUFvQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQTtnQkFDN0MsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLEtBQUssS0FBSyxFQUFFO29CQUMzQixJQUFJLEdBQUcsS0FBSyxDQUFBO2lCQUNiO2dCQUNELElBQUksTUFBTSxHQUFHLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQTtnQkFDakQsR0FBRyxHQUFHLE1BQU0sQ0FBQTthQUNiO1lBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFBO1lBQy9CLE9BQU07U0FDUDtJQUNILENBQUM7SUFFUyw0QkFBWSxHQUF0QjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFBO0lBQzdDLENBQUM7SUFFUywyQkFBVyxHQUFyQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO0lBQ2hELENBQUM7SUFFUyx3QkFBUSxHQUFsQjtRQUNFLElBQUssUUFBZ0IsQ0FBQyxTQUFTLEVBQUU7WUFDOUIsUUFBZ0IsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUE7U0FDcEM7YUFBTTtZQUNMLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQyxlQUFlLEVBQUUsQ0FBQTtTQUN4QztJQUNILENBQUM7SUFFUyxvQ0FBb0IsR0FBOUIsVUFBK0IsS0FBOEI7UUFDM0QsSUFBSSxLQUFLLENBQUE7UUFDVCxJQUFJLE9BQU8sSUFBSSxLQUFLLEVBQUU7WUFDcEIsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUE7U0FDcEI7YUFBTTtZQUNMLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtTQUM1RDtRQUVELE9BQU8sS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFBO0lBQzdELENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ08sNEJBQVksR0FBdEIsVUFDRSxRQUFnQixFQUNoQixXQUFrQixFQUNsQixJQUFZLEVBQ1osT0FBYztRQUZkLDRCQUFBLEVBQUEsa0JBQWtCO1FBQ2xCLHFCQUFBLEVBQUEsWUFBWTtRQUNaLHdCQUFBLEVBQUEsY0FBYztRQUVkLElBQUksUUFBUSxLQUFLLFNBQVMsSUFBSSxRQUFRLEtBQUssSUFBSSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDekUsTUFBTSxJQUFJLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFBO1NBQzVDO1FBRUQsNkJBQTZCO1FBQzdCLElBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO1FBQzVFLElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFBO1lBRWhGLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUM1RCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUE7Z0JBQzNDLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFBO2dCQUN6QixLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQTthQUN0QjtpQkFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLEtBQUssRUFBRTtnQkFDM0QsSUFBTSxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQzFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUE7YUFDeEM7WUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFFRCxJQUFJLE9BQU8sSUFBSSxXQUFXLEVBQUU7WUFDMUIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUE7U0FDeEI7UUFFRCxJQUFJLE9BQU8sRUFBRTtZQUNYLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFO2dCQUMxQyxJQUFJLEVBQUUsTUFBTTtnQkFDWixJQUFJLEVBQUUsTUFBTSxDQUFDLFNBQVM7YUFDdkIsQ0FBQyxDQUFBO1lBRUYsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUN2QixTQUFTLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRTtvQkFDN0MsS0FBSyxFQUFFLE1BQU07b0JBQ2IsSUFBSSxFQUFFLE1BQU0sQ0FBQyxTQUFTO2lCQUN2QixDQUFDLENBQUE7YUFDSDtTQUNGO2FBQU07WUFDTCxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUE7WUFFekQsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUN2QixTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUE7YUFDOUQ7U0FDRjtJQUNILENBQUM7SUFFRDs7Ozs7T0FLRztJQUNPLDZCQUFhLEdBQXZCLFVBQXdCLEtBQWE7UUFDbkMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFBO1FBQ2pELElBQUksZUFBZSxHQUFHLENBQUMsQ0FBQTtRQUV2QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUVyQyxJQUFJLGNBQWMsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUM3QyxJQUFJLFlBQVksR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUUsQ0FBQyxDQUFBO1lBQ3pFLElBQUksWUFBWSxHQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFBO1lBRXJELElBQUksV0FBVyxTQUFBLENBQUE7WUFDZixJQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFBO1lBRWhDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixXQUFXLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUMxQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFFLENBQUMsQ0FBQTthQUNoRTtZQUVELG9CQUFvQjtZQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLFlBQVksRUFBRTtnQkFDcEMsT0FBTztvQkFDTCxLQUFLLEVBQUUsWUFBWTtvQkFDbkIsUUFBUSxFQUFFLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYTtpQkFDM0MsQ0FBQTthQUNGO1lBRUQscUJBQXFCO1lBQ3JCLElBQUksQ0FBQyxXQUFXLElBQUksS0FBSyxJQUFJLFlBQVksRUFBRTtnQkFDekMsT0FBTztvQkFDTCxLQUFLLEVBQUUsWUFBWTtvQkFDbkIsUUFBUSxFQUFFLGVBQWUsR0FBRyxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUM7aUJBQ2xGLENBQUE7YUFDRjtZQUVELElBQUksS0FBSyxJQUFJLFlBQVksSUFBSSxLQUFLLEdBQUcsU0FBUyxFQUFFO2dCQUM5QyxPQUFPO29CQUNMLEtBQUssRUFBRSxZQUFZO29CQUNuQixRQUFRLEVBQUUsZUFBZSxHQUFHLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhO2lCQUN0RSxDQUFBO2FBQ0Y7WUFFRCxlQUFlLElBQUksWUFBWSxDQUFBO1NBQ2hDO1FBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFBO0lBQ25ELENBQUM7SUFFRDs7Ozs7T0FLRztJQUNPLGdDQUFnQixHQUExQixVQUEyQixRQUF3QjtRQUNqRCxJQUFJLFFBQVEsS0FBSyxTQUFTLElBQUksUUFBUSxLQUFLLElBQUksSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3pFLE1BQU0sSUFBSSxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQTtTQUM1QztRQUVELElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQTtRQUNqRCxJQUFJLGVBQWUsR0FBRyxDQUFDLENBQUE7UUFFdkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFFckMsSUFBSSxjQUFjLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDN0MsSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFFLENBQUMsQ0FBQTtZQUN6RSxJQUFJLFlBQVksR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQTtZQUVyRCxJQUFJLFdBQVcsU0FBQSxDQUFBO1lBRWYsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLFdBQVcsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDM0M7WUFFRCxvQkFBb0I7WUFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLFFBQVEsSUFBSSxlQUFlLEdBQUcsWUFBWSxFQUFFO2dCQUN6RCxPQUFPO29CQUNMLEtBQUssRUFBRSxZQUFZO29CQUNuQixRQUFRLEVBQUUsV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhO2lCQUMzQyxDQUFBO2FBQ0Y7WUFFRCxxQkFBcUI7WUFDckIsSUFBSSxDQUFDLFdBQVcsSUFBSSxRQUFRLElBQUksZUFBZSxFQUFFO2dCQUMvQyxPQUFPO29CQUNMLEtBQUssRUFBRSxZQUFZO29CQUNuQixRQUFRLEVBQUUsZUFBZSxHQUFHLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQztpQkFDbEYsQ0FBQTthQUNGO1lBRUQsSUFBSSxRQUFRLElBQUksZUFBZSxJQUFJLFFBQVEsR0FBRyxDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUMsRUFBRTtnQkFDOUUsT0FBTztvQkFDTCxLQUFLLEVBQUUsWUFBWTtvQkFDbkIsUUFBUSxFQUFFLGVBQWUsR0FBRyxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYTtpQkFDdEUsQ0FBQTthQUNGO1lBRUQsZUFBZSxJQUFJLFlBQVksQ0FBQTtTQUNoQztRQUVELE1BQU0sSUFBSSxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQTtJQUN0RCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ08sNkJBQWEsR0FBdkIsVUFBd0IsS0FBYSxFQUFFLFNBQWlCO1FBQ3RELElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQTtRQUVqRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNyQyxJQUFNLGNBQWMsR0FBRyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUMvQyxJQUFJLFVBQVUsR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUUsQ0FBQyxDQUFBO1lBRXZFLElBQUksS0FBSyxLQUFLLFVBQVUsRUFBRTtnQkFDeEIsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7Z0JBQ3JELEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUUsQ0FBQyxDQUFBO2FBQzdEO1NBQ0Y7UUFFRCxPQUFPLEtBQUssQ0FBQTtJQUNkLENBQUM7SUFFUyxnQ0FBZ0IsR0FBMUI7UUFDRSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQUksaUJBQW1CLENBQUMsQ0FBQTtZQUNqRSxJQUFJLFVBQVUsRUFBRTtnQkFDZCxVQUFVLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLENBQUE7YUFDMUM7WUFDRCxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFJLFVBQVUscUJBQWdCLElBQUksQ0FBQyxLQUFLLE9BQUksQ0FBQyxDQUFBO1lBQ3pGLElBQUksYUFBYSxFQUFFO2dCQUNqQixhQUFhLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUE7YUFDMUM7U0FDRjtJQUNILENBQUM7SUFFUyx3Q0FBd0IsR0FBbEMsVUFDRSxRQUFpQixFQUNqQixJQUFhO1FBRWIsSUFBTSxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFJLGdCQUFrQixDQUFFLENBQUMsQ0FBQTtRQUU3RSxJQUFJLFlBQVksR0FBRyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUM7YUFDdEMsUUFBUSxDQUFDLGdCQUFnQixDQUFDO2FBQzFCLFlBQVksQ0FBQyxPQUFPLEVBQUUsNENBQTRDLENBQUM7YUFDbkUsT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUUzQixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUU5QyxJQUFJLEtBQUssR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUE7UUFDaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUE7UUFFOUMsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQTtRQUU3QyxJQUFJLEtBQUssR0FBRyxXQUFXLEVBQUU7WUFDdkIsaUNBQWlDO1lBQ2pDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFLLGFBQWEsV0FBSyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMseUJBQW1CLGFBQWEsTUFBRyxDQUFDLENBQUE7U0FDckg7SUFDSCxDQUFDO0lBRVMsNkJBQWEsR0FBdkIsVUFBd0IsS0FBYSxFQUFFLEtBQWM7UUFDbkQsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUE7U0FDckM7UUFFRCxJQUFNLEdBQUcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ3hDLE9BQU8sR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBQ3ZCLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDTyx5QkFBUyxHQUFuQixVQUNFLEtBQWEsRUFDYixNQUFhLEVBQ2IsT0FBZTtRQURmLHVCQUFBLEVBQUEsYUFBYTtRQUNiLHdCQUFBLEVBQUEsZUFBZTtRQUVmLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDdEQsSUFBSSxRQUFRLENBQUE7UUFFWixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BELElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDekMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUE7WUFDN0IsR0FBRyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUE7U0FDdEI7YUFBTTtZQUNMLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1NBQ2pGO1FBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBRWhDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFBO1NBQ3hEO1FBRUQsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUE7U0FDMUQ7UUFFRCxJQUFJLE1BQU0sRUFBRTtZQUNWLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUE7WUFDbEQsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUE7U0FDeEI7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQzdCLENBQUM7SUFLRCxzQkFBSSx3QkFBSztRQUlUOztXQUVHO2FBQ0g7WUFDRSxPQUFPLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3ZDLENBQUM7UUFaRDs7V0FFRzthQUNILFVBQVUsS0FBYTtZQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDbkMsQ0FBQzs7O09BQUE7SUFTRDs7T0FFRztJQUNILHNCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFBO1FBQ3pFLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE1BQUksVUFBWSxDQUFDLENBQUE7UUFDakYsSUFBTSxXQUFXLEdBQUcsU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFBO1FBRXZGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQTtRQUVyRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFBO1FBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQTtRQUN4RyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFBO1FBRTdGLElBQUksU0FBUyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBO1FBRXBDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUE7UUFDOUMsSUFBTSxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxHQUFHLFdBQVcsQ0FBQTtRQUU3RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUE7WUFFM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7Z0JBQzlCLEtBQUssR0FBRyxlQUFlLENBQUE7YUFDeEI7WUFFRCxJQUFJLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN2QyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxZQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQUssQ0FBQyxDQUFBO1NBQzdEO1FBRUQsd0NBQXdDO1FBQ3hDLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDeEIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQTtZQUNqRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUE7U0FDdEU7UUFFRCxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUE7SUFDN0QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsdUJBQU8sR0FBUDtRQUNFLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBQ3pELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFcEUsSUFBWSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBWSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBWSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDaEMsSUFBWSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDbEMsSUFBWSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFFakMsSUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDNUIsSUFBWSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBWSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUE7SUFDdEMsQ0FBQztJQUVEOzs7T0FHRztJQUNILHNCQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUE7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsc0JBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBQ3hDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBRWhELElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDN0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUM5RSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQzlFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDMUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUMxRSxDQUFDO0lBRUQ7O09BRUc7SUFDSCx1QkFBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQ3pDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFBO1FBRTdDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDaEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUM3RSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO1FBRWhGLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7UUFFakYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUM3RSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO0lBQzdFLENBQUM7SUFDSCxZQUFDO0FBQUQsQ0EzckJBLEFBMnJCQyxDQTNyQm1CLFVBQVUsR0EyckI3QjtBQUVELE1BQU0sVUFBVSxJQUFJO0lBQ2xCLG1CQUFtQixDQUFtQixxQkFBcUIsRUFBRSxVQUFDLENBQUM7UUFDN0QsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDZCxDQUFDLENBQUMsQ0FBQTtBQUNKLENBQUM7QUFFRCxlQUFlLEtBQUssQ0FBQSIsImZpbGUiOiJtYWluL3NyYy9mb3JtL1JhbmdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVHdlZW5MaXRlLCBQb3dlcjQgfSBmcm9tIFwiZ3NhcFwiXG5cbmltcG9ydCB7IHNlYXJjaEFuZEluaXRpYWxpemUsIGNsYW1wLCBwcmV2ZW50RGVmYXVsdCB9IGZyb20gXCIuLi9VdGlsc1wiXG5pbXBvcnQgKiBhcyBJbnB1dHMgZnJvbSBcIi4uL0lucHV0c1wiXG5pbXBvcnQgRG9tRWxlbWVudCBmcm9tIFwiLi4vRG9tRWxlbWVudFwiXG5cbmNvbnN0IE1BUkdJTl9USUNLID0gMzJcbmNvbnN0IENMQVNTX0hUTUw1ID0gXCJodG1sNVwiXG5jb25zdCBSQU5HRV9MSUdIVCA9IFwicmFuZ2UtLWxpZ2h0XCJcblxuY29uc3QgQ0xBU1NfQ09OVEFJTkVSID0gXCJyYW5nZS1jb250YWluZXJcIlxuY29uc3QgQ0xBU1NfU0xJREVSID0gXCJyYW5nZS1zbGlkZXJcIlxuY29uc3QgQ0xBU1NfQUNUSVZFID0gXCJyYW5nZS0tYWN0aXZlXCJcblxuY29uc3QgQ0xBU1NfVFJBQ0sgPSBcInJhbmdlLXRyYWNrXCJcbmNvbnN0IENMQVNTX1RSQUNLX1BST0dSRVNTID0gXCJyYW5nZS10cmFja19fcHJvZ3Jlc3NcIlxuXG5jb25zdCBDTEFTU19USUNLID0gXCJyYW5nZS10aWNrXCJcbmNvbnN0IENMQVNTX1RJQ0tfTEFCRUwgPSBcInJhbmdlLXRpY2tfX2xhYmVsXCJcbmNvbnN0IENMQVNTX1RJQ0tfQUNUSVZFID0gXCJyYW5nZS10aWNrLS1hY3RpdmVcIlxuXG5jb25zdCBDTEFTU19USFVNQiA9IFwicmFuZ2UtdGh1bWJcIlxuY29uc3QgQ0xBU1NfVEhVTUJfVkFMVUUgPSBcInJhbmdlLXRodW1iX192YWx1ZVwiXG5jb25zdCBDTEFTU19ESVNBQkxFRCA9IFwicmFuZ2UtLWRpc2FibGVkXCJcblxuY29uc3QgQ0xBU1NfRFJBR0dJTkcgPSBcInJhbmdlLS1kcmFnZ2luZ1wiXG5cbmV4cG9ydCBpbnRlcmZhY2UgRm9ybWF0dGVyIHtcbiAgKHZhbHVlOiBudW1iZXIsIHNob3J0OiBib29sZWFuKTogc3RyaW5nXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgT3B0aW9uIHtcbiAgdmFsdWU6IG51bWJlclxuICBsYWJlbDogc3RyaW5nIHwgbnVtYmVyXG59XG5cbi8qKlxuICogVGhlIHJhbmdlIHNsaWRlciBjb21wb25lbnQgZGVmaW5pdGlvbi5cbiAqL1xuY2xhc3MgUmFuZ2UgZXh0ZW5kcyBEb21FbGVtZW50PEhUTUxJbnB1dEVsZW1lbnQ+IHtcbiAgcHJpdmF0ZSBfZG93bkhhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9tb3ZlSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2VuZEhhbmRsZXI6IChlOiBFdmVudCkgPT4gdm9pZFxuICBwcml2YXRlIF9rZXlkb3duSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2ZvY3VzSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG4gIHByaXZhdGUgX2JsdXJIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfcmVzaXplSGFuZGxlcjogKGU6IEV2ZW50KSA9PiB2b2lkXG5cbiAgcHJpdmF0ZSBfd3JhcHBlckVsZW1lbnQhOiBEb21FbGVtZW50PEhUTUxFbGVtZW50PlxuICBwcml2YXRlIF9yYW5nZUNvbnRhaW5lciE6IERvbUVsZW1lbnQ8SFRNTERpdkVsZW1lbnQ+XG4gIHByaXZhdGUgX3JhbmdlVHJhY2shOiBEb21FbGVtZW50PEhUTUxEaXZFbGVtZW50PlxuICBwcml2YXRlIF9yYW5nZVByb2dyZXNzITogRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD5cbiAgcHJpdmF0ZSBfdGlja3NXcmFwcGVyITogRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD5cbiAgcHJpdmF0ZSBfcmFuZ2VUaHVtYiE6IERvbUVsZW1lbnQ8SFRNTERpdkVsZW1lbnQ+XG4gIHByaXZhdGUgX3RodW1iVmFsdWUhOiBEb21FbGVtZW50PEhUTUxEaXZFbGVtZW50PlxuICBwcml2YXRlIF9vdXRwdXRMYWJlbD86IERvbUVsZW1lbnQ8RWxlbWVudD5cblxuICBwcml2YXRlIF9taW5WYWx1ZSE6IG51bWJlclxuICBwcml2YXRlIF9tYXhWYWx1ZSE6IG51bWJlclxuICBwcml2YXRlIF90cmFja1ZhbHVlVG90YWwhOiBudW1iZXJcblxuICBwcml2YXRlIF9ncmFiUG9zaXRpb24hOiBudW1iZXJcbiAgcHJpdmF0ZSBfdHJhY2tXaWR0aCE6IG51bWJlclxuICBwcml2YXRlIF90cmFja1Bvc2l0aW9uTWluITogbnVtYmVyXG4gIHByaXZhdGUgX3RyYWNrUG9zaXRpb25NYXghOiBudW1iZXJcbiAgcHJpdmF0ZSBfdHJhY2tMZWZ0UG9zaXRpb24hOiBudW1iZXJcbiAgcHJpdmF0ZSBfaXRlbVdpZHRoITogbnVtYmVyXG5cbiAgcHJpdmF0ZSBfZm9ybWF0dGVyITogRm9ybWF0dGVyXG5cbiAgY29uc3RydWN0b3IoZWxlbWVudDogSFRNTElucHV0RWxlbWVudCkge1xuICAgIHN1cGVyKGVsZW1lbnQpXG5cbiAgICAvLyBTZXR1cCBldmVudCBjb250ZXh0XG4gICAgdGhpcy5fZG93bkhhbmRsZXIgPSB0aGlzLl9oYW5kbGVEb3duLmJpbmQodGhpcylcbiAgICB0aGlzLl9tb3ZlSGFuZGxlciA9IHRoaXMuX2hhbmRsZU1vdmUuYmluZCh0aGlzKVxuICAgIHRoaXMuX2VuZEhhbmRsZXIgPSB0aGlzLl9oYW5kbGVFbmQuYmluZCh0aGlzKVxuICAgIHRoaXMuX2tleWRvd25IYW5kbGVyID0gdGhpcy5faGFuZGxlS2V5ZG93bi5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9mb2N1c0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVGb2N1cy5iaW5kKHRoaXMpXG4gICAgdGhpcy5fYmx1ckhhbmRsZXIgPSB0aGlzLl9oYW5kbGVCbHVyLmJpbmQodGhpcylcbiAgICB0aGlzLl9yZXNpemVIYW5kbGVyID0gdGhpcy5sYXlvdXQuYmluZCh0aGlzKVxuXG4gICAgdGhpcy5faW5pdGlhbGl6ZSgpXG5cbiAgICBpZiAodGhpcy5lbGVtZW50LmRpc2FibGVkKSB7XG4gICAgICB0aGlzLmRpc2FibGUoKVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmVuYWJsZSgpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEluaXRpYWxpemVzIHRoZSByYW5nZSBzbGlkZXIgY29tcG9uZW50LlxuICAgKlxuICAgKiBUaGlzIG1ldGhvZCBpbnNwZWN0cyB0aGUgc2VsZWN0IGRlZmluaXRpb24gYW5kIGl0cyBvcHRpb25zIGFuZFxuICAgKiBnZW5lcmF0ZXMgbmV3IHN0eWxhYmxlIERPTSBlbGVtZW50cyBhcm91bmQgdGhlIG9yaWdpbmFsIHJhbmdlIGlucHV0LWVsZW1lbnRcbiAgICogZGVmaW5pdGlvbnMuXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBwcm90ZWN0ZWQgX2luaXRpYWxpemUoKSB7XG5cbiAgICBpZiAodGhpcy5oYXNDbGFzcyhDTEFTU19IVE1MNSkpIHtcbiAgICAgIC8vIFRoaXMgZWxlbWVudCB1c2VzIEhUTUw1IHN0eWxpbmcsIGRvIG5vdCB0b3VjaCBpdC4uLlxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgdGhpcy5fd3JhcHBlckVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudCh0aGlzLmVsZW1lbnQucGFyZW50RWxlbWVudCEpXG5cbiAgICB0aGlzLl9yYW5nZUNvbnRhaW5lciA9IG5ldyBEb21FbGVtZW50PEhUTUxEaXZFbGVtZW50PihcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX0NPTlRBSU5FUilcblxuICAgIHRoaXMuX3JhbmdlVHJhY2sgPSBuZXcgRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD4oXCJkaXZcIilcbiAgICAgIC5hZGRDbGFzcyhDTEFTU19UUkFDSylcblxuICAgIC8vIGNoZWNrIGlmIHJhbmdlLS1saWdodCBzbGlkZXIgdGhlbiBhZGQgcHJvZ3Jlc3NcbiAgICBpZiAodGhpcy5fd3JhcHBlckVsZW1lbnQuaGFzQ2xhc3MoUkFOR0VfTElHSFQpKSB7XG4gICAgICB0aGlzLl9yYW5nZVByb2dyZXNzID0gbmV3IERvbUVsZW1lbnQ8SFRNTERpdkVsZW1lbnQ+KFwiZGl2XCIpXG4gICAgICAgIC5hZGRDbGFzcyhDTEFTU19UUkFDS19QUk9HUkVTUylcblxuICAgICAgdGhpcy5fcmFuZ2VUcmFjay5hcHBlbmRDaGlsZCh0aGlzLl9yYW5nZVByb2dyZXNzKVxuICAgIH1cblxuICAgIHRoaXMuX3JhbmdlVGh1bWIgPSBuZXcgRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD4oXCJkaXZcIilcbiAgICAgIC5hZGRDbGFzcyhDTEFTU19USFVNQilcblxuICAgIHRoaXMuX3RpY2tzV3JhcHBlciA9IG5ldyBEb21FbGVtZW50PEhUTUxEaXZFbGVtZW50PihcImRpdlwiKVxuICAgICAgLmFkZENsYXNzKENMQVNTX1NMSURFUilcblxuICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLmFwcGVuZENoaWxkKHRoaXMuX3JhbmdlVHJhY2spXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuYXBwZW5kQ2hpbGQodGhpcy5fdGlja3NXcmFwcGVyKVxuICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLmFwcGVuZENoaWxkKHRoaXMuX3JhbmdlVGh1bWIpXG5cbiAgICAvLyBhZGQgY29udGFpbmVyIHRvIHdyYXBwZXJcbiAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLl9yYW5nZUNvbnRhaW5lcilcblxuICAgIC8vIGdldCBtaW4gJiBtYXggZGVmaW5pdGlvbnNcbiAgICB0aGlzLl9taW5WYWx1ZSA9IHBhcnNlRmxvYXQodGhpcy5lbGVtZW50Lm1pbikgfHwgMFxuICAgIHRoaXMuX21heFZhbHVlID0gcGFyc2VGbG9hdCh0aGlzLmVsZW1lbnQubWF4KSB8fCAxXG5cbiAgICAvLyBnZXQgdGhlIGxhYmVsL291dHB1dCBmb3JtYXQgc3RyaW5nXG4gICAgdGhpcy5fZm9ybWF0dGVyID0gKHdpbmRvdyBhcyBhbnkpW3RoaXMuZ2V0QXR0cmlidXRlKFwiZm9ybWF0dGVyXCIpIV1cblxuICAgIC8vIGdldCB0aGUgb3V0cHV0IGxhYmVsIGFuZCBtb3ZlIGl0IGJlbG93IHRoZSBjb250YWluZXJcbiAgICBpZiAodGhpcy5lbGVtZW50LmlkKSB7XG4gICAgICB0aGlzLl9vdXRwdXRMYWJlbCA9IHRoaXMuX3dyYXBwZXJFbGVtZW50LmZpbmQoYG91dHB1dFtmb3I9JyR7dGhpcy5lbGVtZW50LmlkfSddYClcbiAgICAgIGlmICh0aGlzLl9vdXRwdXRMYWJlbCkge1xuICAgICAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLl9vdXRwdXRMYWJlbClcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoIXRoaXMuZWxlbWVudC5zdGVwKSB7XG4gICAgICAvLyBmaXggaXNzdWVzIHdpdGggZmxvYXQgc2xpZGVycyBpZiB0aGUgc3RlcCBpcyB1bmRlZmluZWRcbiAgICAgIHRoaXMuZWxlbWVudC5zdGVwID0gXCJhbnlcIlxuICAgIH1cblxuICAgIGNvbnN0IG9wdGlvbnMgPSB0aGlzLl9nZXRPcHRpb25zTGlzdCgpXG4gICAgaWYgKG9wdGlvbnMgJiYgb3B0aW9ucy5sZW5ndGggPiAxKSB7XG4gICAgICB0aGlzLl9hZGRUaWNrcyhvcHRpb25zKVxuICAgIH1cblxuICAgIGlmICh0aGlzLl9yYW5nZUNvbnRhaW5lci5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC4ke0NMQVNTX1RJQ0tfTEFCRUx9YCkubGVuZ3RoIDw9IDEpIHtcbiAgICAgIHRoaXMuX3RodW1iVmFsdWUgPSBuZXcgRG9tRWxlbWVudDxIVE1MRGl2RWxlbWVudD4oXCJkaXZcIilcbiAgICAgICAgLmFkZENsYXNzKENMQVNTX1RIVU1CX1ZBTFVFKVxuXG4gICAgICB0aGlzLl9yYW5nZVRodW1iLmFwcGVuZENoaWxkKHRoaXMuX3RodW1iVmFsdWUpXG4gICAgfVxuXG4gICAgdGhpcy5fdHJhY2tWYWx1ZVRvdGFsID0gdGhpcy5fbWF4VmFsdWUgLSB0aGlzLl9taW5WYWx1ZVxuICAgIHRoaXMubGF5b3V0KClcblxuICAgIHRoaXMuX3VwZGF0ZVRpY2tTdGF0ZSgpXG5cbiAgICAvLyBBcHBseSB0aGUgdGFiIGluZGV4XG4gICAgY29uc3QgdGFiSW5kZXggPSB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwidGFiaW5kZXhcIilcbiAgICBpZiAodGFiSW5kZXgpIHtcbiAgICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLnNldEF0dHJpYnV0ZShcInRhYmluZGV4XCIsIHRhYkluZGV4KVxuICAgIH1cblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIHRoaXMuX3Jlc2l6ZUhhbmRsZXIpXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJvcmllbnRhdGlvbmNoYW5nZVwiLCB0aGlzLl9yZXNpemVIYW5kbGVyKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9nZXRPcHRpb25zTGlzdCgpIHtcbiAgICBsZXQgb3B0aW9uczogT3B0aW9uW10gPSBbXVxuXG4gICAgbGV0IGxpc3RJZCA9IHRoaXMuZ2V0QXR0cmlidXRlKFwibGlzdFwiKVxuICAgIGlmIChsaXN0SWQpIHtcbiAgICAgIGxldCBkYXRhTGlzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCMke2xpc3RJZH1gKVxuICAgICAgaWYgKGRhdGFMaXN0KSB7XG4gICAgICAgIGZvciAobGV0IGVudHJ5IG9mIGRhdGFMaXN0LnF1ZXJ5U2VsZWN0b3JBbGwoXCJvcHRpb25cIikpIHtcbiAgICAgICAgICBsZXQgdmFsdWUgPSBwYXJzZUZsb2F0KGVudHJ5LmlubmVyVGV4dClcbiAgICAgICAgICBsZXQgbGFiZWwgPSBlbnRyeS5nZXRBdHRyaWJ1dGUoXCJsYWJlbFwiKSB8fCBwYXJzZUZsb2F0KHZhbHVlLnRvRml4ZWQoMikpXG5cbiAgICAgICAgICBvcHRpb25zLnB1c2goe1xuICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICBsYWJlbFxuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBTb3J0IHRoZSBsaXN0IHRvIGVuYWJsZSBzbmFwcGluZ1xuICAgIG9wdGlvbnMgPSBvcHRpb25zLnNvcnQoKGEsIGIpID0+IGEudmFsdWUgLSBiLnZhbHVlKVxuXG4gICAgaWYgKG9wdGlvbnMubGVuZ3RoID4gMSkge1xuICAgICAgdGhpcy5fbWluVmFsdWUgPSBOdW1iZXIuTUFYX1ZBTFVFXG4gICAgICB0aGlzLl9tYXhWYWx1ZSA9IE51bWJlci5NSU5fVkFMVUVcblxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBvcHRpb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHRoaXMuX21pblZhbHVlID0gTWF0aC5taW4odGhpcy5fbWluVmFsdWUsIG9wdGlvbnNbaV0udmFsdWUpXG4gICAgICAgIHRoaXMuX21heFZhbHVlID0gTWF0aC5tYXgodGhpcy5fbWF4VmFsdWUsIG9wdGlvbnNbaV0udmFsdWUpXG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIG9wdGlvbnNcbiAgfVxuXG4gIHByb3RlY3RlZCBfYWRkVGlja3MoZGF0YUl0ZW1zOiBPcHRpb25bXSkge1xuICAgIGZvciAobGV0IGVudHJ5IG9mIGRhdGFJdGVtcykge1xuICAgICAgbGV0IHRpY2tFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQoXCJkaXZcIilcbiAgICAgICAgLnNldEF0dHJpYnV0ZShcImRhdGEtdmFsdWVcIiwgU3RyaW5nKGVudHJ5LnZhbHVlKSlcbiAgICAgICAgLmFkZENsYXNzKENMQVNTX1RJQ0spXG5cbiAgICAgIGxldCB0aWNrTGFiZWwgPSBuZXcgRG9tRWxlbWVudChcInNwYW5cIilcbiAgICAgICAgLmFkZENsYXNzKENMQVNTX1RJQ0tfTEFCRUwpXG4gICAgICAgIC5zZXRIdG1sKFN0cmluZyhlbnRyeS5sYWJlbCkpXG5cbiAgICAgIHRpY2tFbGVtZW50LmFwcGVuZENoaWxkKHRpY2tMYWJlbClcbiAgICAgIHRoaXMuX3RpY2tzV3JhcHBlci5hcHBlbmRDaGlsZCh0aWNrRWxlbWVudClcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgX2lzRXZlbnRPbkxhYmVsKGV2ZW50OiBFdmVudCkge1xuICAgIHJldHVybiAoZXZlbnQudGFyZ2V0IGFzIEVsZW1lbnQpLmNsYXNzTGlzdC5jb250YWlucyhDTEFTU19USUNLX0xBQkVMKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVEb3duKGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LmFkZENsYXNzKENMQVNTX0RSQUdHSU5HKVxuXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCB0aGlzLl9lbmRIYW5kbGVyKVxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgdGhpcy5fbW92ZUhhbmRsZXIpXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNldXBcIiwgdGhpcy5fZW5kSGFuZGxlcilcblxuICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcInRvdWNobW92ZVwiLCB0aGlzLl9tb3ZlSGFuZGxlcilcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwidG91Y2hlbmRcIiwgdGhpcy5fZW5kSGFuZGxlcilcblxuICAgIC8vIElnbm9yZSBjbGlja3MgZGlyZWN0bHkgb24gdGhlIHRodW1iXG4gICAgaWYgKGV2ZW50LnRhcmdldCAhPT0gdGhpcy5fcmFuZ2VUaHVtYi5lbGVtZW50ICYmICF0aGlzLl9pc0V2ZW50T25MYWJlbChldmVudCkpIHtcbiAgICAgIGxldCBwb3MgPSB0aGlzLl9nZXRSZWxhdGl2ZVBvc2l0aW9uKGV2ZW50KVxuICAgICAgdGhpcy5fc2V0UG9zaXRpb24ocG9zLCB0cnVlLCBmYWxzZSwgZmFsc2UpXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVNb3ZlKGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xuICAgIHByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgIHRoaXMuX3VuZm9jdXMoKVxuXG4gICAgaWYgKCF0aGlzLl9pc0V2ZW50T25MYWJlbChldmVudCkpIHtcbiAgICAgIGxldCBwb3MgPSB0aGlzLl9nZXRSZWxhdGl2ZVBvc2l0aW9uKGV2ZW50KVxuICAgICAgdGhpcy5fc2V0UG9zaXRpb24ocG9zLCB0cnVlLCBmYWxzZSwgZmFsc2UpXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVFbmQoZXZlbnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSB7XG4gICAgdGhpcy5fd3JhcHBlckVsZW1lbnQucmVtb3ZlQ2xhc3MoQ0xBU1NfRFJBR0dJTkcpXG5cbiAgICB0aGlzLl9yYW5nZUNvbnRhaW5lci5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIHRoaXMuX2VuZEhhbmRsZXIpXG4gICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm1vdXNldXBcIiwgdGhpcy5fZW5kSGFuZGxlcilcbiAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIHRoaXMuX21vdmVIYW5kbGVyKVxuXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwidG91Y2htb3ZlXCIsIHRoaXMuX21vdmVIYW5kbGVyKVxuICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ0b3VjaGVuZFwiLCB0aGlzLl9lbmRIYW5kbGVyKVxuXG4gICAgbGV0IHBvcyA9IHRoaXMuX2dldFJlbGF0aXZlUG9zaXRpb24oZXZlbnQpXG4gICAgdGhpcy5fc2V0UG9zaXRpb24ocG9zLCB0cnVlLCB0cnVlLCB0cnVlKVxuICAgIHRoaXMuX2hhbmRsZUJsdXIoKVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVLZXlkb3duKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgbGV0IGtleWNvZGUgPSBldmVudC53aGljaCB8fCBldmVudC5rZXlDb2RlXG5cbiAgICBpZiAoa2V5Y29kZSA9PT0gSW5wdXRzLktFWV9FU0NBUEUpIHtcbiAgICAgIC8vIGhhbmRsZSBFc2NhcGUga2V5IChFU0MpXG4gICAgICB0aGlzLl9yYW5nZUNvbnRhaW5lci5lbGVtZW50LmJsdXIoKVxuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgY29uc3QgaXNVcCA9IGtleWNvZGUgPT09IElucHV0cy5LRVlfQVJST1dfVVAgfHwga2V5Y29kZSA9PT0gSW5wdXRzLktFWV9BUlJPV19SSUdIVFxuICAgICAgfHwga2V5Y29kZSA9PT0gSW5wdXRzLktFWV9QQUdFX1VQXG5cbiAgICBjb25zdCBpc0Rvd24gPSBrZXljb2RlID09PSBJbnB1dHMuS0VZX0FSUk9XX0RPV04gfHwga2V5Y29kZSA9PT0gSW5wdXRzLktFWV9BUlJPV19MRUZUXG4gICAgICB8fCBrZXljb2RlID09PSBJbnB1dHMuS0VZX1BBR0VfRE9XTlxuXG4gICAgaWYgKGlzVXAgfHwgaXNEb3duKSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG5cbiAgICAgIGxldCBkaXJlY3Rpb24gPSBpc0Rvd24gPyAtMSA6IDFcblxuICAgICAgLy8gbWFrZSBhIGxhcmdlciBzdGVwIGlmIGl0cyB0aGUgdmVydGljYWwgYXJyb3cgb3IgcGFnZSBrZXlzXG4gICAgICBpZiAoa2V5Y29kZSA9PT0gSW5wdXRzLktFWV9BUlJPV19VUCB8fCBrZXljb2RlID09PSBJbnB1dHMuS0VZX0FSUk9XX0RPV04gfHxcbiAgICAgICAga2V5Y29kZSA9PT0gSW5wdXRzLktFWV9QQUdFX1VQIHx8IGtleWNvZGUgPT09IElucHV0cy5LRVlfUEFHRV9ET1dOKSB7XG4gICAgICAgIGRpcmVjdGlvbiAqPSAxMFxuICAgICAgfVxuXG4gICAgICBsZXQgdmFsID0gdGhpcy52YWx1ZVxuICAgICAgaWYgKHRoaXMuX3RpY2tzV3JhcHBlci5lbGVtZW50LmNoaWxkTm9kZXMubGVuZ3RoID4gMSkge1xuICAgICAgICB2YWwgPSB0aGlzLl9nZXROZXh0VmFsdWUodmFsLCBkaXJlY3Rpb24pXG4gICAgICB9IGVsc2Uge1xuXG4gICAgICAgIGxldCBzdGVwOiBzdHJpbmcgfCBudW1iZXIgPSB0aGlzLmVsZW1lbnQuc3RlcFxuICAgICAgICBpZiAoIXN0ZXAgfHwgc3RlcCA9PT0gXCJhbnlcIikge1xuICAgICAgICAgIHN0ZXAgPSBcIjAuMVwiXG4gICAgICAgIH1cbiAgICAgICAgbGV0IG5ld1ZhbCA9IHZhbCArIChwYXJzZUZsb2F0KHN0ZXApICogZGlyZWN0aW9uKVxuICAgICAgICB2YWwgPSBuZXdWYWxcbiAgICAgIH1cblxuICAgICAgdGhpcy5fc2V0VmFsdWUodmFsLCB0cnVlLCB0cnVlKVxuICAgICAgcmV0dXJuXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVGb2N1cygpIHtcbiAgICB0aGlzLl9yYW5nZUNvbnRhaW5lci5hZGRDbGFzcyhDTEFTU19BQ1RJVkUpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZUJsdXIoKSB7XG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIucmVtb3ZlQ2xhc3MoQ0xBU1NfQUNUSVZFKVxuICB9XG5cbiAgcHJvdGVjdGVkIF91bmZvY3VzKCkge1xuICAgIGlmICgoZG9jdW1lbnQgYXMgYW55KS5zZWxlY3Rpb24pIHtcbiAgICAgIChkb2N1bWVudCBhcyBhbnkpLnNlbGVjdGlvbi5lbXB0eSgpXG4gICAgfSBlbHNlIHtcbiAgICAgIHdpbmRvdy5nZXRTZWxlY3Rpb24oKS5yZW1vdmVBbGxSYW5nZXMoKVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfZ2V0UmVsYXRpdmVQb3NpdGlvbihldmVudDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpIHtcbiAgICBsZXQgcGFnZVhcbiAgICBpZiAoXCJwYWdlWFwiIGluIGV2ZW50KSB7XG4gICAgICBwYWdlWCA9IGV2ZW50LnBhZ2VYXG4gICAgfSBlbHNlIHtcbiAgICAgIHBhZ2VYID0gKGV2ZW50LnRvdWNoZXNbMF0gfHwgZXZlbnQuY2hhbmdlZFRvdWNoZXNbMF0pLnBhZ2VYXG4gICAgfVxuXG4gICAgcmV0dXJuIHBhZ2VYIC0gdGhpcy5fdHJhY2tMZWZ0UG9zaXRpb24gKyB0aGlzLl9ncmFiUG9zaXRpb25cbiAgfVxuXG4gIC8qKlxuICAgKiBWYWxpZGF0ZXMgYW5kIHVwZGF0ZXMgdGhlIHBvc2l0aW9uIGFuZCBzZXRzIHRoZSBjb3JyZXNwb25kaW5nIHZhbHVlIG9uIHRoZSBzbGlkZXIuXG4gICAqIEBwYXJhbSB7cG9zaXRpb259IHRoZSBuZXcgcG9zaXRpb24gdG8gc2V0LlxuICAgKiBAcGFyYW0ge3VwZGF0ZVZhbHVlfSB0cnVlIGlmIHRoZSB2YWx1ZSBzaG91bGQgYmUgdXBkYXRlZCBhcyB3ZWxsOyBvdGhlcndpc2UgZmFsc2UuXG4gICAqIEBwYXJhbSB7c25hcH0gdHJ1ZSBpZiBzbmFwcGluZyBzaG91bGQgYmUgdXNlZDsgb3RoZXJ3aXNlIGZhbHNlLlxuICAgKiBAcGFyYW0ge2FuaW1hdGV9IHRydWUgaWYgdGhlIFVJIHVwZGF0ZSBzaG91bGQgYmUgYW5pbWF0ZWQ7IG90aGVyd2lzZSBmYWxzZS5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHByb3RlY3RlZCBfc2V0UG9zaXRpb24oXG4gICAgcG9zaXRpb246IG51bWJlcixcbiAgICB1cGRhdGVWYWx1ZSA9IHRydWUsXG4gICAgc25hcCA9IGZhbHNlLFxuICAgIGFuaW1hdGUgPSB0cnVlXG4gICkge1xuICAgIGlmIChwb3NpdGlvbiA9PT0gdW5kZWZpbmVkIHx8IHBvc2l0aW9uID09PSBudWxsIHx8IE51bWJlci5pc05hTihwb3NpdGlvbikpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIlBvc2l0aW9uIGlzIG5vdCBhIG51bWJlclwiKVxuICAgIH1cblxuICAgIC8vIENsYW1wIHRvIG1pbiBhbmQgbWF4IHJhbmdlXG4gICAgbGV0IG5ld1BvcyA9IGNsYW1wKHBvc2l0aW9uLCB0aGlzLl90cmFja1Bvc2l0aW9uTWluLCB0aGlzLl90cmFja1Bvc2l0aW9uTWF4KVxuICAgIGlmICh1cGRhdGVWYWx1ZSkge1xuICAgICAgbGV0IHZhbHVlID0gKHRoaXMuX3RyYWNrVmFsdWVUb3RhbCAvIHRoaXMuX3RyYWNrV2lkdGgpICogbmV3UG9zICsgdGhpcy5fbWluVmFsdWVcblxuICAgICAgaWYgKHRoaXMuX3RpY2tzV3JhcHBlci5lbGVtZW50LmNoaWxkTm9kZXMubGVuZ3RoID4gMSAmJiBzbmFwKSB7XG4gICAgICAgIGxldCBzbmFwUG9zID0gdGhpcy5fZ2V0U25hcFBvc2l0aW9uKG5ld1BvcylcbiAgICAgICAgbmV3UG9zID0gc25hcFBvcy5wb3NpdGlvblxuICAgICAgICB2YWx1ZSA9IHNuYXBQb3MudmFsdWVcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5lbGVtZW50LnN0ZXAgJiYgdGhpcy5lbGVtZW50LnN0ZXAgIT09IFwiYW55XCIpIHtcbiAgICAgICAgY29uc3Qgc3RlcCA9IHBhcnNlRmxvYXQodGhpcy5lbGVtZW50LnN0ZXApXG4gICAgICAgIHZhbHVlID0gTWF0aC5yb3VuZCh2YWx1ZSAvIHN0ZXApICogc3RlcFxuICAgICAgfVxuXG4gICAgICB0aGlzLl9zZXRWYWx1ZSh2YWx1ZSwgZmFsc2UsIGZhbHNlKVxuICAgIH1cblxuICAgIGlmIChhbmltYXRlICYmIHVwZGF0ZVZhbHVlKSB7XG4gICAgICB0aGlzLl91cGRhdGVUaWNrU3RhdGUoKVxuICAgIH1cblxuICAgIGlmIChhbmltYXRlKSB7XG4gICAgICBUd2VlbkxpdGUudG8odGhpcy5fcmFuZ2VUaHVtYi5lbGVtZW50LCAwLjIsIHtcbiAgICAgICAgbGVmdDogbmV3UG9zLFxuICAgICAgICBlYXNlOiBQb3dlcjQuZWFzZUluT3V0XG4gICAgICB9KVxuXG4gICAgICBpZiAodGhpcy5fcmFuZ2VQcm9ncmVzcykge1xuICAgICAgICBUd2VlbkxpdGUudG8odGhpcy5fcmFuZ2VQcm9ncmVzcy5lbGVtZW50LCAwLjIsIHtcbiAgICAgICAgICB3aWR0aDogbmV3UG9zLFxuICAgICAgICAgIGVhc2U6IFBvd2VyNC5lYXNlSW5PdXRcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgVHdlZW5MaXRlLnNldCh0aGlzLl9yYW5nZVRodW1iLmVsZW1lbnQsIHsgbGVmdDogbmV3UG9zIH0pXG5cbiAgICAgIGlmICh0aGlzLl9yYW5nZVByb2dyZXNzKSB7XG4gICAgICAgIFR3ZWVuTGl0ZS5zZXQodGhpcy5fcmFuZ2VQcm9ncmVzcy5lbGVtZW50LCB7IHdpZHRoOiBuZXdQb3MgfSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgc25hcCB2YWx1ZSBjb3JyZXNwb25kaW5nIHRvIHRoZSBnaXZlbiB2YWx1ZS5cbiAgICogQHBhcmFtIHt2YWx1ZX0gdGhlIHRhcmdldCB2YWx1ZS5cbiAgICogQHJldHVybnMgYW4gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIHNuYXAgcG9zaXRpb24gYW5kIHRoZSBjb3JyZXNwb25kaW5nIHZhbHVlLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9nZXRTbmFwVmFsdWUodmFsdWU6IG51bWJlcikge1xuICAgIGNvbnN0IHRpY2tzID0gdGhpcy5fdGlja3NXcmFwcGVyLmVsZW1lbnQuY2hpbGRyZW5cbiAgICBsZXQgY3VycmVudFBvc2l0aW9uID0gMFxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aWNrcy5sZW5ndGg7IGkrKykge1xuXG4gICAgICBsZXQgY3VycmVudEVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudCh0aWNrc1tpXSlcbiAgICAgIGxldCBjdXJyZW50VmFsdWUgPSBwYXJzZUZsb2F0KGN1cnJlbnRFbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtdmFsdWVcIikhKVxuICAgICAgbGV0IGN1cnJlbnRXaWR0aCA9IGN1cnJlbnRFbGVtZW50LmVsZW1lbnQuY2xpZW50V2lkdGhcblxuICAgICAgbGV0IG5leHRFbGVtZW50XG4gICAgICBsZXQgbmV4dFZhbHVlID0gTnVtYmVyLk1BWF9WQUxVRVxuXG4gICAgICBpZiAoaSA8IHRpY2tzLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgbmV4dEVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudCh0aWNrc1tpICsgMV0pXG4gICAgICAgIG5leHRWYWx1ZSA9IHBhcnNlRmxvYXQobmV4dEVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiZGF0YS12YWx1ZVwiKSEpXG4gICAgICB9XG5cbiAgICAgIC8vIGxlZnQgbW9zdCBlbGVtZW50XG4gICAgICBpZiAoaSA9PT0gMCAmJiB2YWx1ZSA8PSBjdXJyZW50VmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICB2YWx1ZTogY3VycmVudFZhbHVlLFxuICAgICAgICAgIHBvc2l0aW9uOiBNQVJHSU5fVElDSyAtIHRoaXMuX2dyYWJQb3NpdGlvblxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIHJpZ2h0IG1vc3QgZWxlbWVudFxuICAgICAgaWYgKCFuZXh0RWxlbWVudCAmJiB2YWx1ZSA+PSBjdXJyZW50VmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICB2YWx1ZTogY3VycmVudFZhbHVlLFxuICAgICAgICAgIHBvc2l0aW9uOiBjdXJyZW50UG9zaXRpb24gKyAoY3VycmVudFdpZHRoIC0gTUFSR0lOX1RJQ0spIC0gdGhpcy5fZ3JhYlBvc2l0aW9uIC0gMVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmICh2YWx1ZSA+PSBjdXJyZW50VmFsdWUgJiYgdmFsdWUgPCBuZXh0VmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICB2YWx1ZTogY3VycmVudFZhbHVlLFxuICAgICAgICAgIHBvc2l0aW9uOiBjdXJyZW50UG9zaXRpb24gKyAoMC41ICogY3VycmVudFdpZHRoKSAtIHRoaXMuX2dyYWJQb3NpdGlvblxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGN1cnJlbnRQb3NpdGlvbiArPSBjdXJyZW50V2lkdGhcbiAgICB9XG5cbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJDb3VsZCBub3QgZGV0ZXJtaW5lIHNuYXAgdmFsdWVcIilcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBzbmFwIHBvc2l0aW9uIGNvcnJlc3BvbmRpbmcgdG8gdGhlIGdpdmVuIHBvc2l0aW9uLlxuICAgKiBAcGFyYW0ge3Bvc2l0aW9ufSB0aGUgdGFyZ2V0IHBvc2l0aW9uLlxuICAgKiBAcmV0dXJucyBhbiBvYmplY3QgY29udGFpbmluZyB0aGUgc25hcCBwb3NpdGlvbiBhbmQgdGhlIGNvcnJlc3BvbmRpbmcgdmFsdWUuXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBwcm90ZWN0ZWQgX2dldFNuYXBQb3NpdGlvbihwb3NpdGlvbj86IG51bWJlciB8IG51bGwpIHtcbiAgICBpZiAocG9zaXRpb24gPT09IHVuZGVmaW5lZCB8fCBwb3NpdGlvbiA9PT0gbnVsbCB8fCBOdW1iZXIuaXNOYU4ocG9zaXRpb24pKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJwb3NpdGlvbiBpcyBub3QgYSBudW1iZXJcIilcbiAgICB9XG5cbiAgICBjb25zdCB0aWNrcyA9IHRoaXMuX3RpY2tzV3JhcHBlci5lbGVtZW50LmNoaWxkcmVuXG4gICAgbGV0IGN1cnJlbnRQb3NpdGlvbiA9IDBcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGlja3MubGVuZ3RoOyBpKyspIHtcblxuICAgICAgbGV0IGN1cnJlbnRFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQodGlja3NbaV0pXG4gICAgICBsZXQgY3VycmVudFZhbHVlID0gcGFyc2VGbG9hdChjdXJyZW50RWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIpISlcbiAgICAgIGxldCBjdXJyZW50V2lkdGggPSBjdXJyZW50RWxlbWVudC5lbGVtZW50LmNsaWVudFdpZHRoXG5cbiAgICAgIGxldCBuZXh0RWxlbWVudFxuXG4gICAgICBpZiAoaSA8IHRpY2tzLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgbmV4dEVsZW1lbnQgPSBuZXcgRG9tRWxlbWVudCh0aWNrc1tpICsgMV0pXG4gICAgICB9XG5cbiAgICAgIC8vIGxlZnQgbW9zdCBlbGVtZW50XG4gICAgICBpZiAoaSA9PT0gMCAmJiBwb3NpdGlvbiA8PSBjdXJyZW50UG9zaXRpb24gKyBjdXJyZW50V2lkdGgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICB2YWx1ZTogY3VycmVudFZhbHVlLFxuICAgICAgICAgIHBvc2l0aW9uOiBNQVJHSU5fVElDSyAtIHRoaXMuX2dyYWJQb3NpdGlvblxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIHJpZ2h0IG1vc3QgZWxlbWVudFxuICAgICAgaWYgKCFuZXh0RWxlbWVudCAmJiBwb3NpdGlvbiA+PSBjdXJyZW50UG9zaXRpb24pIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICB2YWx1ZTogY3VycmVudFZhbHVlLFxuICAgICAgICAgIHBvc2l0aW9uOiBjdXJyZW50UG9zaXRpb24gKyAoY3VycmVudFdpZHRoIC0gTUFSR0lOX1RJQ0spIC0gdGhpcy5fZ3JhYlBvc2l0aW9uIC0gMVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChwb3NpdGlvbiA+PSBjdXJyZW50UG9zaXRpb24gJiYgcG9zaXRpb24gPCAoY3VycmVudFBvc2l0aW9uICsgY3VycmVudFdpZHRoKSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHZhbHVlOiBjdXJyZW50VmFsdWUsXG4gICAgICAgICAgcG9zaXRpb246IGN1cnJlbnRQb3NpdGlvbiArICgwLjUgKiBjdXJyZW50V2lkdGgpIC0gdGhpcy5fZ3JhYlBvc2l0aW9uXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgY3VycmVudFBvc2l0aW9uICs9IGN1cnJlbnRXaWR0aFxuICAgIH1cblxuICAgIHRocm93IG5ldyBFcnJvcihcIkNvdWxkIG5vdCBkZXRlcm1pbmUgc25hcCBwb3NpdGlvblwiKVxuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIG5leHQgdmFsdWUgaW4gdGhlIGdpdmVuIGRpcmVjdGlvbiB3aXRoIHJlZ2FyZHMgdG8gc25hcHBpbmcuXG4gICAqIEBwYXJhbSB7dmFsdWV9IFRoZSBjdXJyZW50IHZhbHVlLlxuICAgKiBAcGFyYW0ge2RpcmVjdGlvbn0gVGhlIGRpcmVjdGlvbiAocG9zaXRpdmUgb3IgbmVnYXRpdmUgaW50ZWdlcikuXG4gICAqIEByZXR1cm5zIFRoZSBuZXh0IHZhbHVlLlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9nZXROZXh0VmFsdWUodmFsdWU6IG51bWJlciwgZGlyZWN0aW9uOiBudW1iZXIpIHtcbiAgICBjb25zdCB0aWNrcyA9IHRoaXMuX3RpY2tzV3JhcHBlci5lbGVtZW50LmNoaWxkcmVuXG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRpY2tzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBjb25zdCBjdXJyZW50RWxlbWVudCA9IG5ldyBEb21FbGVtZW50KHRpY2tzW2ldKVxuICAgICAgbGV0IGN1cnJlbnRWYWwgPSBwYXJzZUZsb2F0KGN1cnJlbnRFbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtdmFsdWVcIikhKVxuXG4gICAgICBpZiAodmFsdWUgPT09IGN1cnJlbnRWYWwpIHtcbiAgICAgICAgbGV0IGluZGV4ID0gY2xhbXAoaSArIGRpcmVjdGlvbiwgMCwgdGlja3MubGVuZ3RoIC0gMSlcbiAgICAgICAgdmFsdWUgPSBwYXJzZUZsb2F0KHRpY2tzW2luZGV4XS5nZXRBdHRyaWJ1dGUoXCJkYXRhLXZhbHVlXCIpISlcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdmFsdWVcbiAgfVxuXG4gIHByb3RlY3RlZCBfdXBkYXRlVGlja1N0YXRlKCkge1xuICAgIGlmICh0aGlzLl90aWNrc1dyYXBwZXIuZWxlbWVudC5jaGlsZE5vZGVzLmxlbmd0aCA+IDEpIHtcbiAgICAgIGxldCBhY3RpdmVUaWNrID0gdGhpcy5fdGlja3NXcmFwcGVyLmZpbmQoYC4ke0NMQVNTX1RJQ0tfQUNUSVZFfWApXG4gICAgICBpZiAoYWN0aXZlVGljaykge1xuICAgICAgICBhY3RpdmVUaWNrLnJlbW92ZUNsYXNzKENMQVNTX1RJQ0tfQUNUSVZFKVxuICAgICAgfVxuICAgICAgbGV0IG5ld0FjdGl2ZVRpY2sgPSB0aGlzLl90aWNrc1dyYXBwZXIuZmluZChgLiR7Q0xBU1NfVElDS31bZGF0YS12YWx1ZT0nJHt0aGlzLnZhbHVlfSddYClcbiAgICAgIGlmIChuZXdBY3RpdmVUaWNrKSB7XG4gICAgICAgIG5ld0FjdGl2ZVRpY2suYWRkQ2xhc3MoQ0xBU1NfVElDS19BQ1RJVkUpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9hZGp1c3RUaWNrTGFiZWxQb3NpdGlvbihcbiAgICB0aWNrSXRlbTogRWxlbWVudCxcbiAgICBsZWZ0OiBib29sZWFuXG4gICkge1xuICAgIGNvbnN0IGxhYmVsID0gbmV3IERvbUVsZW1lbnQodGlja0l0ZW0ucXVlcnlTZWxlY3RvcihgLiR7Q0xBU1NfVElDS19MQUJFTH1gKSEpXG5cbiAgICBsZXQgZHVtbXlFbGVtZW50ID0gbmV3IERvbUVsZW1lbnQoXCJzcGFuXCIpXG4gICAgICAuYWRkQ2xhc3MoQ0xBU1NfVElDS19MQUJFTClcbiAgICAgIC5zZXRBdHRyaWJ1dGUoXCJzdHlsZVwiLCBcInZpc2liaWxpdHk6IGhpZGRlbjsgZGlzcGxheTogaW5saW5lLWJsb2NrO1wiKVxuICAgICAgLnNldEh0bWwobGFiZWwuaW5uZXJUZXh0KVxuXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuYXBwZW5kQ2hpbGQoZHVtbXlFbGVtZW50KVxuXG4gICAgbGV0IHdpZHRoID0gZHVtbXlFbGVtZW50LmVsZW1lbnQuY2xpZW50V2lkdGggLyAyXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIucmVtb3ZlQ2hpbGQoZHVtbXlFbGVtZW50KVxuXG4gICAgY29uc3QgZmxvYXRQb3NpdGlvbiA9IGxlZnQgPyBcImxlZnRcIiA6IFwicmlnaHRcIlxuXG4gICAgaWYgKHdpZHRoIDwgTUFSR0lOX1RJQ0spIHtcbiAgICAgIC8vIGNlbnRlciBzbWFsbCBpdGVtcyBvbiB0aGUgdGlja1xuICAgICAgbGFiZWwuc2V0QXR0cmlidXRlKFwic3R5bGVcIiwgYCR7ZmxvYXRQb3NpdGlvbn06ICR7TUFSR0lOX1RJQ0sgLSBNYXRoLmZsb29yKHdpZHRoKX1weDsgdGV4dC1hbGlnbjogJHtmbG9hdFBvc2l0aW9ufTtgKVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfZm9ybWF0T3V0cHV0KHZhbHVlOiBudW1iZXIsIHNob3J0OiBib29sZWFuKSB7XG4gICAgaWYgKHRoaXMuX2Zvcm1hdHRlcikge1xuICAgICAgcmV0dXJuIHRoaXMuX2Zvcm1hdHRlcih2YWx1ZSwgc2hvcnQpXG4gICAgfVxuXG4gICAgY29uc3Qgc3RyID0gcGFyc2VGbG9hdCh2YWx1ZS50b0ZpeGVkKDIpKVxuICAgIHJldHVybiBzdHIudG9TdHJpbmcoKVxuICB9XG5cbiAgLyoqXG4gICAqIFZhbGlkYXRlcyBhbmQgdXBkYXRlcyB0aGUgcmFuZ2UgdmFsdWUuXG4gICAqIEBwYXJhbSB7dmFsdWV9IHRoZSBuZXcgdmFsdWUgdG8gc2V0LlxuICAgKiBAcGFyYW0ge3VwZGF0ZX0gdHJ1ZSBpZiB0aGUgVUkgc2hvdWxkIGJlIHVwZGF0ZWQ7IG90aGVyd2lzZSBmYWxzZS5cbiAgICogQHBhcmFtIHthbmltYXRlfSB0cnVlIGlmIHRoZSBVSSB1cGRhdGUgc2hvdWxkIGJlIGFuaW1hdGVkOyBvdGhlcndpc2UgZmFsc2UuXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBwcm90ZWN0ZWQgX3NldFZhbHVlKFxuICAgIHZhbHVlOiBudW1iZXIsXG4gICAgdXBkYXRlID0gdHJ1ZSxcbiAgICBhbmltYXRlID0gZmFsc2VcbiAgKSB7XG4gICAgbGV0IHZhbCA9IGNsYW1wKHZhbHVlLCB0aGlzLl9taW5WYWx1ZSwgdGhpcy5fbWF4VmFsdWUpXG4gICAgbGV0IHBvc2l0aW9uXG5cbiAgICBpZiAodGhpcy5fdGlja3NXcmFwcGVyLmVsZW1lbnQuY2hpbGROb2Rlcy5sZW5ndGggPiAxKSB7XG4gICAgICBjb25zdCBzbmFwVmFsdWUgPSB0aGlzLl9nZXRTbmFwVmFsdWUodmFsKVxuICAgICAgcG9zaXRpb24gPSBzbmFwVmFsdWUucG9zaXRpb25cbiAgICAgIHZhbCA9IHNuYXBWYWx1ZS52YWx1ZVxuICAgIH0gZWxzZSB7XG4gICAgICBwb3NpdGlvbiA9ICh0aGlzLl90cmFja1dpZHRoIC8gdGhpcy5fdHJhY2tWYWx1ZVRvdGFsKSAqICh2YWx1ZSAtIHRoaXMuX21pblZhbHVlKVxuICAgIH1cblxuICAgIHRoaXMuZWxlbWVudC52YWx1ZSA9IFN0cmluZyh2YWwpXG5cbiAgICBpZiAodGhpcy5fdGh1bWJWYWx1ZSkge1xuICAgICAgdGhpcy5fdGh1bWJWYWx1ZS5zZXRIdG1sKHRoaXMuX2Zvcm1hdE91dHB1dCh2YWwsIHRydWUpKVxuICAgIH1cblxuICAgIGlmICh0aGlzLl9vdXRwdXRMYWJlbCkge1xuICAgICAgdGhpcy5fb3V0cHV0TGFiZWwuc2V0SHRtbCh0aGlzLl9mb3JtYXRPdXRwdXQodmFsLCBmYWxzZSkpXG4gICAgfVxuXG4gICAgaWYgKHVwZGF0ZSkge1xuICAgICAgdGhpcy5fc2V0UG9zaXRpb24ocG9zaXRpb24sIGZhbHNlLCBmYWxzZSwgYW5pbWF0ZSlcbiAgICAgIHRoaXMuX3VwZGF0ZVRpY2tTdGF0ZSgpXG4gICAgfVxuXG4gICAgdGhpcy5kaXNwYXRjaEV2ZW50KFwiaW5wdXRcIilcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSB2YWx1ZSBvZiB0aGUgcmFuZ2Ugc2xpZGVyLlxuICAgKi9cbiAgc2V0IHZhbHVlKHZhbHVlOiBudW1iZXIpIHtcbiAgICB0aGlzLl9zZXRWYWx1ZSh2YWx1ZSwgdHJ1ZSwgdHJ1ZSlcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBjdXJyZW50IHZhbHVlLlxuICAgKi9cbiAgZ2V0IHZhbHVlKCkge1xuICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMuZWxlbWVudC52YWx1ZSlcbiAgfVxuXG4gIC8qKlxuICAgKiBGb3JjZSB0aGUgY29tcG9uZW50IHRvIHJlLWxheW91dCBpdHNlbGYuXG4gICAqL1xuICBsYXlvdXQoKSB7XG4gICAgdGhpcy5fZ3JhYlBvc2l0aW9uID0gTWF0aC5yb3VuZCh0aGlzLl9yYW5nZVRodW1iLmVsZW1lbnQub2Zmc2V0V2lkdGggLyAyKVxuICAgIGNvbnN0IHRpY2tJdGVtcyA9IHRoaXMuX3JhbmdlQ29udGFpbmVyLmVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChgLiR7Q0xBU1NfVElDS31gKVxuICAgIGNvbnN0IHRpY2tzT2Zmc2V0ID0gdGlja0l0ZW1zICYmIHRpY2tJdGVtcy5sZW5ndGggPiAwID8gKDIgKiBNQVJHSU5fVElDSykgOiBNQVJHSU5fVElDS1xuXG4gICAgdGhpcy5fdHJhY2tXaWR0aCA9IHRoaXMuX3JhbmdlVHJhY2suZWxlbWVudC5vZmZzZXRXaWR0aCAtIHRpY2tzT2Zmc2V0XG5cbiAgICB0aGlzLl90cmFja1Bvc2l0aW9uTWluID0gMFxuICAgIHRoaXMuX3RyYWNrUG9zaXRpb25NYXggPSB0aGlzLl9yYW5nZVRyYWNrLmVsZW1lbnQuY2xpZW50V2lkdGggLSB0aGlzLl9yYW5nZVRodW1iLmVsZW1lbnQub2Zmc2V0V2lkdGggKyAxXG4gICAgdGhpcy5fdHJhY2tMZWZ0UG9zaXRpb24gPSB0aGlzLl9yYW5nZVRyYWNrLmVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCArIE1BUkdJTl9USUNLXG5cbiAgICBsZXQgaXRlbUNvdW50ID0gdGlja0l0ZW1zLmxlbmd0aCAtIDFcblxuICAgIHRoaXMuX2l0ZW1XaWR0aCA9IHRoaXMuX3RyYWNrV2lkdGggLyBpdGVtQ291bnRcbiAgICBjb25zdCBvdXRlckl0ZW1zV2lkdGggPSAodGhpcy5faXRlbVdpZHRoICogMC41KSArIE1BUkdJTl9USUNLXG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8PSBpdGVtQ291bnQ7IGkrKykge1xuICAgICAgbGV0IHdpZHRoID0gdGhpcy5faXRlbVdpZHRoXG5cbiAgICAgIGlmIChpID09PSAwIHx8IGkgPT09IGl0ZW1Db3VudCkge1xuICAgICAgICB3aWR0aCA9IG91dGVySXRlbXNXaWR0aFxuICAgICAgfVxuXG4gICAgICBsZXQgaXRlbSA9IG5ldyBEb21FbGVtZW50KHRpY2tJdGVtc1tpXSlcbiAgICAgIGl0ZW0uc2V0QXR0cmlidXRlKFwic3R5bGVcIiwgYHdpZHRoOiAke01hdGguZmxvb3Iod2lkdGgpfXB4O2ApXG4gICAgfVxuXG4gICAgLy8gYWRqdXN0IGZpcnN0IGFuZCBsYXN0IGxhYmVsIHBvc2l0aW9uc1xuICAgIGlmICh0aWNrSXRlbXMubGVuZ3RoID4gMSkge1xuICAgICAgdGhpcy5fYWRqdXN0VGlja0xhYmVsUG9zaXRpb24odGlja0l0ZW1zWzBdLCB0cnVlKVxuICAgICAgdGhpcy5fYWRqdXN0VGlja0xhYmVsUG9zaXRpb24odGlja0l0ZW1zW3RpY2tJdGVtcy5sZW5ndGggLSAxXSwgZmFsc2UpXG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIHRoZSB2YWx1ZVxuICAgIHRoaXMuX3NldFZhbHVlKHBhcnNlRmxvYXQodGhpcy5lbGVtZW50LnZhbHVlKSwgdHJ1ZSwgZmFsc2UpXG4gIH1cblxuICAvKipcbiAgICogRGVzdHJveXMgdGhlIGNvbXBvbmVudHMgYW5kIGZyZWVzIGFsbCByZWZlcmVuY2VzLlxuICAgKi9cbiAgZGVzdHJveSgpIHtcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCB0aGlzLl9yZXNpemVIYW5kbGVyKVxuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwib3JpZW50YXRpb25jaGFuZ2VcIiwgdGhpcy5fcmVzaXplSGFuZGxlcik7XG5cbiAgICAodGhpcyBhcyBhbnkpLl9kb3duSGFuZGxlciA9IG51bGw7XG4gICAgKHRoaXMgYXMgYW55KS5fbW92ZUhhbmRsZXIgPSBudWxsO1xuICAgICh0aGlzIGFzIGFueSkuX2VuZEhhbmRsZXIgPSBudWxsO1xuICAgICh0aGlzIGFzIGFueSkuX2ZvY3VzSGFuZGxlciA9IG51bGw7XG4gICAgKHRoaXMgYXMgYW55KS5fYmx1ckhhbmRsZXIgPSBudWxsO1xuXG4gICAgKHRoaXMgYXMgYW55KS5lbGVtZW50ID0gbnVsbDtcbiAgICAodGhpcyBhcyBhbnkpLl9yYW5nZUNvbnRhaW5lciA9IG51bGw7XG4gICAgKHRoaXMgYXMgYW55KS5fd3JhcHBlckVsZW1lbnQgPSBudWxsXG4gIH1cblxuICAvKipcbiAgICogQGRlcHJlY2F0ZWQgdXNlIGRlc3Ryb3koKSBpbnN0ZWFkLlxuICAgKiBAdG9kbyByZW1vdmUgaW4gdmVyc2lvbiAyLjAuMFxuICAgKi9cbiAgZGVzdG95KCkge1xuICAgIHRoaXMuZGVzdHJveSgpXG4gIH1cblxuICAvKipcbiAgICogU2V0cyB0aGUgY29tcG9uZW50IHRvIHRoZSBlbmFibGVkIHN0YXRlLlxuICAgKi9cbiAgZW5hYmxlKCkge1xuICAgIHRoaXMuZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoXCJkaXNhYmxlZFwiKVxuICAgIHRoaXMuX3dyYXBwZXJFbGVtZW50LnJlbW92ZUNsYXNzKENMQVNTX0RJU0FCTEVEKVxuXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vkb3duXCIsIHRoaXMuX2Rvd25IYW5kbGVyKVxuICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcInRvdWNoc3RhcnRcIiwgdGhpcy5fZG93bkhhbmRsZXIpXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLl9rZXlkb3duSGFuZGxlcilcbiAgICB0aGlzLl9yYW5nZUNvbnRhaW5lci5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJmb2N1c1wiLCB0aGlzLl9mb2N1c0hhbmRsZXIpXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKFwiYmx1clwiLCB0aGlzLl9ibHVySGFuZGxlcilcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBjb21wb25lbnQgdG8gdGhlIGRpc2FibGVkIHN0YXRlLlxuICAgKi9cbiAgZGlzYWJsZSgpIHtcbiAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwiZGlzYWJsZWRcIiwgXCJcIilcbiAgICB0aGlzLl93cmFwcGVyRWxlbWVudC5hZGRDbGFzcyhDTEFTU19ESVNBQkxFRClcblxuICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCB0aGlzLl9kb3duSGFuZGxlcilcbiAgICB0aGlzLl9yYW5nZUNvbnRhaW5lci5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZXVwXCIsIHRoaXMuX2VuZEhhbmRsZXIpXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIHRoaXMuX21vdmVIYW5kbGVyKVxuXG4gICAgdGhpcy5fcmFuZ2VDb250YWluZXIuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFwidG91Y2hzdGFydFwiLCB0aGlzLl9kb3duSGFuZGxlcilcblxuICAgIHRoaXMuX3JhbmdlQ29udGFpbmVyLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImZvY3VzXCIsIHRoaXMuX2ZvY3VzSGFuZGxlcilcbiAgICB0aGlzLl9yYW5nZUNvbnRhaW5lci5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJibHVyXCIsIHRoaXMuX2JsdXJIYW5kbGVyKVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbml0KCkge1xuICBzZWFyY2hBbmRJbml0aWFsaXplPEhUTUxJbnB1dEVsZW1lbnQ+KFwiaW5wdXRbdHlwZT0ncmFuZ2UnXVwiLCAoZSkgPT4ge1xuICAgIG5ldyBSYW5nZShlKVxuICB9KVxufVxuXG5leHBvcnQgZGVmYXVsdCBSYW5nZVxuIl0sInNvdXJjZVJvb3QiOiIuLi8uLi8uLi8uLi8uLiJ9
