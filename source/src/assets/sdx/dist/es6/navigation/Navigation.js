import * as tslib_1 from "tslib";
import { TimelineLite, Power4, Power1 } from "gsap";
import { searchAndInitialize } from "../Utils";
import DomElement from "../DomElement";
import * as Dom from "../DomFunctions";
import SearchInput from "../search/SearchInput";
var CLASS_OPEN = "is-open";
var CLASS_ACTIVE = "is-active";
var QUERY_NAV_HAMBURGER = ".nav-hamburger";
var QUERY_NAV_HB_BODY = ".nav__primary";
var CLASS_NAV_LINK = "nav-link--header";
var QUERY_NAV_LINK_ACTIVE = ".nav-link--header.is-active";
var QUERY_NAV_MOBILE = ".nav__level1 .nav__mainnav .nav__primary";
var QUERY_NAV_LEVEL0 = ".nav__level0";
var QUERY_NAV_LEVEL0_CONTAINER = ".nav__level0 .nav__subnav";
var QUERY_SECTION_OPEN = ".nav-section.is-open";
var QUERY_NAV_LEVEL1 = ".nav__level1 .nav__mainnav";
var QUERY_NAV_LEVEL0_LINK = ".nav-link.nav-link--header";
var QUERY_NAV_LEVEL1_LINK = ".nav-link--header";
var QUERY_NAV_COLUMN = ".nav-col";
var QUERY_NAV_COLUMN_ACTIVE = ".nav-col.is-active";
var QUERY_NAV_BODY = ".nav-body";
var QUERY_NAV_FOOTER = ".nav-footer";
var QUERY_SEARCH_ICON = ".nav-search";
var QUERY_SEARCH_FIELD = ".search__input";
var CLASS_SEARCH_DESKTOP = "search--desktop";
var ANIMATION_START_DELAY = 0.2;
var ANIMATION_OFFSET = 0.05;
var ANIMATION_BODY_DURATION = 0.3;
var ANIMATION_FOOTER_DURATION = 0.1;
/**
 * The navigation component definition.
 */
var Navigation = /** @class */ (function (_super) {
    tslib_1.__extends(Navigation, _super);
    function Navigation(element) {
        var _this = _super.call(this, element) || this;
        _this._navLevel0 = _this.element.querySelector(QUERY_NAV_LEVEL0) || document.createElement("div");
        _this._navLevel0Body = _this.element.querySelector(QUERY_NAV_LEVEL0_CONTAINER) || document.createElement("div");
        _this._navLevel1 = _this.element.querySelector(QUERY_NAV_LEVEL1) || document.createElement("div");
        _this._navMobile = _this.element.querySelector(QUERY_NAV_MOBILE) || document.createElement("div");
        if (!_this._navMobile.parentElement) {
            var dummyParent = document.createElement("div");
            dummyParent.appendChild(_this._navMobile);
        }
        _this._hamburgerElement = _this.element.querySelector(QUERY_NAV_HAMBURGER) || document.createElement("div");
        _this._searchComponents = [];
        _this._level0ClickHandler = _this._handleLevel0Click.bind(_this);
        _this._level1ClickHandler = _this._handleLevel1Click.bind(_this);
        _this._windowClickHandler = _this._handleWindowClick.bind(_this);
        _this._searchClickHandler = _this._handleSearchClick.bind(_this);
        _this._initialize();
        return _this;
    }
    Navigation.prototype._resetMainTimeline = function () {
        var _this = this;
        if (this._tlMain) {
            this._tlMain.kill();
        }
        this._tlMain = new TimelineLite({
            onComplete: function () {
                _this._tlMain = undefined;
            }
        });
    };
    Navigation.prototype._isMobile = function () {
        return Dom.isHidden(this._hamburgerElement, true) === false;
    };
    Navigation.prototype._handleLevel0Click = function (event) {
        var isDesktop = !this._isMobile();
        if (isDesktop) {
            this._resetMainTimeline();
            var navItems = new NavigationItems(this)
                .fromLevel0(event.target);
            if (!navItems.section) {
                return;
            }
            var previousNavLink = this._navLevel0.querySelector(QUERY_NAV_LINK_ACTIVE);
            var previousNavSection = this._navLevel0.querySelector(QUERY_SECTION_OPEN);
            this._toggleContainer(navItems.link, this._navLevel0Body, navItems.section, undefined, previousNavLink, this._navLevel0Body, previousNavSection, undefined, true);
        }
    };
    Navigation.prototype._handleLevel1Click = function (event) {
        var navItems = new NavigationItems(this)
            .fromLevel1(event.target);
        var prevItems = navItems.previousLevel1();
        this._toggleContainer(navItems.link, navItems.container, navItems.section, navItems.footer, prevItems.link, prevItems.container, prevItems.section, prevItems.footer, false);
        return false;
    };
    Navigation.prototype._toggleContainer = function (navLink, navContainer, navSection, navFooter, previousNavLink, previousNavContainer, previousNavSection, previousNavFooter, animateContainer) {
        if (animateContainer === void 0) { animateContainer = false; }
        var isDesktop = !this._isMobile();
        if (previousNavLink && previousNavLink !== navLink && navLink !== this._hamburgerElement) {
            Dom.removeClass(previousNavLink, CLASS_ACTIVE);
        }
        if (Dom.hasClass(navLink, CLASS_ACTIVE)) {
            Dom.removeClass(navLink, CLASS_ACTIVE);
            if (isDesktop) {
                this._onNavigationClosed();
                this._resetMainTimeline();
                this._closeSection(navContainer, navSection, navFooter, this._tlMain, true, animateContainer);
            }
            else if (navLink === this._hamburgerElement) {
                // Close mobile navigation
                this._onNavigationClosed();
                this._resetMainTimeline();
                this._closeSection(navContainer, navSection, undefined, this._tlMain, false, false);
            }
            else if (!isDesktop) {
                // Close the section
                this._closeSection(navContainer, navSection, navFooter, undefined, true, animateContainer);
            }
        }
        else {
            Dom.addClass(navLink, CLASS_ACTIVE);
            if (isDesktop) {
                Dom.addClass(this._navMobile, CLASS_OPEN);
                this._onNavigationOpened();
                this._resetMainTimeline();
                if (previousNavContainer && previousNavSection) {
                    this._closeSection(previousNavContainer, previousNavSection, previousNavFooter, this._tlMain, true, animateContainer);
                }
                this._openSection(navContainer, navSection, navFooter, this._tlMain, true, animateContainer);
            }
            else if (navLink === this._hamburgerElement) {
                // Open mobile navigation
                this._onNavigationOpened();
                this._resetMainTimeline();
                this._openSection(navContainer, navSection, undefined, this._tlMain, false, false);
            }
            else if (!isDesktop) {
                // Open section
                if (previousNavContainer && previousNavSection) {
                    this._closeSection(previousNavContainer, previousNavSection, previousNavFooter, undefined, true, animateContainer);
                }
                this._openSection(navContainer, navSection, navFooter, undefined, true, animateContainer);
            }
        }
    };
    Navigation.prototype._onNavigationOpened = function () {
        Dom.addClass(this._navMobile, CLASS_OPEN);
        Dom.addClass(this._navMobile.parentElement, CLASS_OPEN);
        Dom.addClass(this._hamburgerElement, CLASS_ACTIVE);
        window.addEventListener("click", this._windowClickHandler);
        window.addEventListener("touchend", this._windowClickHandler);
    };
    Navigation.prototype._onNavigationClosed = function () {
        Dom.removeClass(this._navMobile, CLASS_OPEN);
        Dom.removeClass(this._navMobile.parentElement, CLASS_OPEN);
        Dom.removeClass(this._hamburgerElement, CLASS_ACTIVE);
        window.removeEventListener("click", this._windowClickHandler);
        window.removeEventListener("touchend", this._windowClickHandler);
    };
    Navigation.prototype._handleWindowClick = function (event) {
        var target = event.target;
        while (target !== this.element && target.parentElement) {
            target = target.parentElement;
        }
        if (target !== this.element) {
            this.close();
            return false;
        }
        return true;
    };
    Navigation.prototype._openSection = function (navContainer, navSection, navFooter, tl, animateColumns, animateContainer) {
        if (animateColumns === void 0) { animateColumns = true; }
        if (animateContainer === void 0) { animateContainer = false; }
        if (!navSection || !navContainer) {
            return;
        }
        var activeItems = navSection.querySelectorAll(QUERY_NAV_COLUMN);
        if (animateContainer === true) {
            var container = navContainer;
            navContainer = navSection;
            navSection = container;
        }
        if (!tl) {
            tl = new TimelineLite();
        }
        tl.set(navContainer, {
            className: "+=" + CLASS_OPEN
        });
        tl.set(navSection, {
            display: "block"
        });
        tl.to(navSection, ANIMATION_BODY_DURATION, {
            className: "+=" + CLASS_OPEN,
            clearProps: "all",
            ease: [
                Power1.easeIn, Power4.easeOut
            ]
        });
        if (navFooter) {
            tl.set(navFooter.querySelectorAll(QUERY_NAV_COLUMN), {
                className: "+=" + CLASS_ACTIVE
            });
            tl.set(navFooter, {
                display: "block"
            }, 0);
            tl.to(navFooter, ANIMATION_FOOTER_DURATION, {
                className: "+=" + CLASS_OPEN,
                clearProps: "height, display",
                ease: [
                    Power1.easeIn, Power4.easeOut
                ]
            }, "-=0.1");
        }
        if (animateColumns === true) {
            var delay = ANIMATION_START_DELAY;
            var items = activeItems;
            for (var index = 0; index < items.length; index++) {
                tl.to(items[index], 0, {
                    className: "+=" + CLASS_ACTIVE
                }, delay);
                delay += ANIMATION_OFFSET;
            }
        }
    };
    Navigation.prototype._closeSection = function (navContainer, navSection, navFooter, tl, animateColumns, animateContainer) {
        if (animateColumns === void 0) { animateColumns = true; }
        if (animateContainer === void 0) { animateContainer = false; }
        if (!navSection || !navContainer) {
            return;
        }
        var activeItems = navSection.querySelectorAll(QUERY_NAV_COLUMN_ACTIVE);
        if (animateContainer === true) {
            var container = navContainer;
            navContainer = navSection;
            navSection = container;
        }
        if (!tl) {
            tl = new TimelineLite();
        }
        tl.set(navSection, {
            display: "block"
        });
        if (animateColumns === true) {
            tl.set(activeItems, {
                className: "-=" + CLASS_ACTIVE
            }, 0);
        }
        tl.to(navSection, ANIMATION_BODY_DURATION, {
            className: "-=" + CLASS_OPEN,
            ease: [
                Power1.easeIn, Power4.easeOut
            ],
            clearProps: "all",
            onComplete: function () {
                var e_1, _a;
                Dom.removeClass(navContainer, CLASS_OPEN);
                if (animateColumns === true) {
                    try {
                        for (var activeItems_1 = tslib_1.__values(activeItems), activeItems_1_1 = activeItems_1.next(); !activeItems_1_1.done; activeItems_1_1 = activeItems_1.next()) {
                            var active = activeItems_1_1.value;
                            Dom.removeClass(active, CLASS_ACTIVE);
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (activeItems_1_1 && !activeItems_1_1.done && (_a = activeItems_1.return)) _a.call(activeItems_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                }
            }
        }, 0);
        if (navFooter) {
            tl.set(navFooter, {
                display: "block"
            }, 0);
            tl.to(navFooter, ANIMATION_FOOTER_DURATION, {
                className: "-=" + CLASS_OPEN,
                ease: [
                    Power1.easeIn, Power4.easeOut
                ],
                clearProps: "height,display",
                onComplete: function () {
                    var e_2, _a;
                    try {
                        for (var _b = tslib_1.__values(navFooter.querySelectorAll(QUERY_NAV_COLUMN_ACTIVE)), _c = _b.next(); !_c.done; _c = _b.next()) {
                            var active = _c.value;
                            Dom.removeClass(active, CLASS_ACTIVE);
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                }
            }, 0);
        }
    };
    Navigation.prototype._handleSearchClick = function () {
        if (this._searchDesktop) {
            this._searchDesktop.open();
        }
    };
    /**
     * Initializes the navigation component.
     * @private
     */
    Navigation.prototype._initialize = function () {
        var e_3, _a, e_4, _b, e_5, _c;
        try {
            for (var _d = tslib_1.__values(this._navLevel0.querySelectorAll(QUERY_NAV_LEVEL0_LINK)), _e = _d.next(); !_e.done; _e = _d.next()) {
                var navLink = _e.value;
                navLink.addEventListener("click", this._level0ClickHandler);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
            }
            finally { if (e_3) throw e_3.error; }
        }
        try {
            for (var _f = tslib_1.__values(this._navLevel1.querySelectorAll(QUERY_NAV_LEVEL1_LINK)), _g = _f.next(); !_g.done; _g = _f.next()) {
                var navLink = _g.value;
                navLink.addEventListener("click", this._level1ClickHandler);
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (_g && !_g.done && (_b = _f.return)) _b.call(_f);
            }
            finally { if (e_4) throw e_4.error; }
        }
        this._hamburgerElement.addEventListener("click", this._level1ClickHandler);
        // Desktop search icon
        var searchIcon = this.element.querySelector(QUERY_SEARCH_ICON);
        if (searchIcon) {
            searchIcon.addEventListener("click", this._searchClickHandler);
        }
        try {
            for (var _h = tslib_1.__values(this.element.querySelectorAll(QUERY_SEARCH_FIELD)), _j = _h.next(); !_j.done; _j = _h.next()) {
                var search = _j.value;
                var searchComponent = new SearchInput(search);
                if (Dom.hasClass(search, CLASS_SEARCH_DESKTOP) || Dom.hasClass(search.parentElement, CLASS_SEARCH_DESKTOP)) {
                    this._searchDesktop = searchComponent;
                }
                this._searchComponents.push(searchComponent);
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (_j && !_j.done && (_c = _h.return)) _c.call(_h);
            }
            finally { if (e_5) throw e_5.error; }
        }
    };
    /**
     * Closes the navigation.
     */
    Navigation.prototype.close = function () {
        var isMoble = this._isMobile();
        this._resetMainTimeline();
        var level1 = this._navLevel1.querySelector(QUERY_NAV_LINK_ACTIVE);
        var level0 = this._navLevel0.querySelector(QUERY_NAV_LINK_ACTIVE);
        if (!level1 && isMoble && Dom.hasClass(this._hamburgerElement, CLASS_ACTIVE)) {
            level1 = this._hamburgerElement;
        }
        if (level1) {
            var navItems = new NavigationItems(this)
                .fromLevel1(level1);
            Dom.removeClass(navItems.link, CLASS_ACTIVE);
            this._onNavigationClosed();
            this._closeSection(navItems.container, navItems.section, navItems.footer, this._tlMain, !isMoble, false);
        }
        if (level0) {
            var navItems = new NavigationItems(this)
                .fromLevel0(level0);
            Dom.removeClass(navItems.link, CLASS_ACTIVE);
            this._onNavigationClosed();
            this._closeSection(navItems.container, navItems.section, navItems.footer, this._tlMain, !isMoble, true);
        }
    };
    return Navigation;
}(DomElement));
var NavigationItems = /** @class */ (function () {
    function NavigationItems(nav) {
        this._navigation = nav;
    }
    Object.defineProperty(NavigationItems.prototype, "link", {
        get: function () {
            return this._link;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationItems.prototype, "container", {
        get: function () {
            return this._container;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationItems.prototype, "section", {
        get: function () {
            return this._section;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationItems.prototype, "footer", {
        get: function () {
            return this._footer;
        },
        enumerable: true,
        configurable: true
    });
    NavigationItems.prototype.fromLevel0 = function (navLink) {
        while (!Dom.hasClass(navLink, CLASS_NAV_LINK) && navLink.parentElement) {
            navLink = navLink.parentElement;
        }
        this._link = navLink;
        var toggleId = navLink.getAttribute("data-toggle");
        this._container = this._navigation._navLevel0Body;
        this._section = this._navigation._navLevel0.querySelector("#" + toggleId);
        return this;
    };
    NavigationItems.prototype.fromLevel1 = function (navLink) {
        while (navLink.parentElement) {
            if ((navLink === this._navigation._hamburgerElement) || Dom.hasClass(navLink, CLASS_NAV_LINK)) {
                break;
            }
            navLink = navLink.parentElement;
        }
        this._link = navLink;
        this._container = navLink.parentElement;
        this._section = this._container.querySelector(QUERY_NAV_BODY);
        this._footer = this._container.querySelector(QUERY_NAV_FOOTER);
        if (navLink === this._navigation._hamburgerElement) {
            this._container = this._navigation._navLevel1;
            this._section = this._container.querySelector(QUERY_NAV_HB_BODY);
        }
        return this;
    };
    NavigationItems.prototype.previousLevel1 = function () {
        var prev = new NavigationItems(this._navigation);
        prev._link = this._navigation._navLevel1.querySelector(QUERY_NAV_LINK_ACTIVE);
        prev._container = prev._link ? prev._link.parentElement : undefined;
        prev._section = prev._container ? prev._container.querySelector(QUERY_NAV_BODY) : undefined;
        prev._footer = prev._container ? prev._container.querySelector(QUERY_NAV_FOOTER) : undefined;
        return prev;
    };
    NavigationItems.prototype.isHamburger = function () {
        return this._link === this._navigation._hamburgerElement;
    };
    return NavigationItems;
}());
export function init() {
    searchAndInitialize(".nav", function (e) {
        new Navigation(e);
    });
}
export default Navigation;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4vc3JjL25hdmlnYXRpb24vTmF2aWdhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE1BQU0sTUFBTSxDQUFBO0FBQ25ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLFVBQVUsQ0FBQTtBQUM5QyxPQUFPLFVBQVUsTUFBTSxlQUFlLENBQUE7QUFDdEMsT0FBTyxLQUFLLEdBQUcsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0QyxPQUFPLFdBQVcsTUFBTSx1QkFBdUIsQ0FBQTtBQUUvQyxJQUFNLFVBQVUsR0FBRyxTQUFTLENBQUE7QUFDNUIsSUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFBO0FBRWhDLElBQU0sbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUE7QUFDNUMsSUFBTSxpQkFBaUIsR0FBRyxlQUFlLENBQUE7QUFFekMsSUFBTSxjQUFjLEdBQUcsa0JBQWtCLENBQUE7QUFDekMsSUFBTSxxQkFBcUIsR0FBRyw2QkFBNkIsQ0FBQTtBQUUzRCxJQUFNLGdCQUFnQixHQUFHLDBDQUEwQyxDQUFBO0FBQ25FLElBQU0sZ0JBQWdCLEdBQUcsY0FBYyxDQUFBO0FBQ3ZDLElBQU0sMEJBQTBCLEdBQUcsMkJBQTJCLENBQUE7QUFDOUQsSUFBTSxrQkFBa0IsR0FBRyxzQkFBc0IsQ0FBQTtBQUVqRCxJQUFNLGdCQUFnQixHQUFHLDRCQUE0QixDQUFBO0FBRXJELElBQU0scUJBQXFCLEdBQUcsNEJBQTRCLENBQUE7QUFDMUQsSUFBTSxxQkFBcUIsR0FBRyxtQkFBbUIsQ0FBQTtBQUVqRCxJQUFNLGdCQUFnQixHQUFHLFVBQVUsQ0FBQTtBQUNuQyxJQUFNLHVCQUF1QixHQUFHLG9CQUFvQixDQUFBO0FBRXBELElBQU0sY0FBYyxHQUFHLFdBQVcsQ0FBQTtBQUNsQyxJQUFNLGdCQUFnQixHQUFHLGFBQWEsQ0FBQTtBQUV0QyxJQUFNLGlCQUFpQixHQUFHLGFBQWEsQ0FBQTtBQUN2QyxJQUFNLGtCQUFrQixHQUFHLGdCQUFnQixDQUFBO0FBQzNDLElBQU0sb0JBQW9CLEdBQUcsaUJBQWlCLENBQUE7QUFFOUMsSUFBTSxxQkFBcUIsR0FBRyxHQUFHLENBQUE7QUFDakMsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUE7QUFFN0IsSUFBTSx1QkFBdUIsR0FBRyxHQUFHLENBQUE7QUFDbkMsSUFBTSx5QkFBeUIsR0FBRyxHQUFHLENBQUE7QUFFckM7O0dBRUc7QUFDSDtJQUF5QixzQ0FBVTtJQW9CakMsb0JBQVksT0FBZ0I7UUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FxQmY7UUFuQkMsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDL0YsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDN0csS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7UUFFL0YsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDL0YsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFO1lBQ2xDLElBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDL0MsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7U0FDekM7UUFFRCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3pHLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUE7UUFFM0IsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDN0QsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDN0QsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFDN0QsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLENBQUE7UUFFN0QsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFBOztJQUNwQixDQUFDO0lBRVMsdUNBQWtCLEdBQTVCO1FBQUEsaUJBVUM7UUFUQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtTQUNwQjtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxZQUFZLENBQUM7WUFDOUIsVUFBVSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFBO1lBQzFCLENBQUM7U0FDRixDQUFDLENBQUE7SUFDSixDQUFDO0lBRVMsOEJBQVMsR0FBbkI7UUFDRSxPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQTtJQUM3RCxDQUFDO0lBRVMsdUNBQWtCLEdBQTVCLFVBQTZCLEtBQWlCO1FBQzVDLElBQU0sU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO1FBRW5DLElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUE7WUFFekIsSUFBSSxRQUFRLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDO2lCQUNyQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQXFCLENBQUMsQ0FBQTtZQUUxQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRTtnQkFDckIsT0FBTTthQUNQO1lBRUQsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQWlCLENBQUE7WUFDMUYsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBaUIsQ0FBQTtZQUUxRixJQUFJLENBQUMsZ0JBQWdCLENBQ25CLFFBQVEsQ0FBQyxJQUFJLEVBQ2IsSUFBSSxDQUFDLGNBQWMsRUFDbkIsUUFBUSxDQUFDLE9BQU8sRUFDaEIsU0FBUyxFQUNULGVBQWUsRUFDZixJQUFJLENBQUMsY0FBYyxFQUNuQixrQkFBa0IsRUFDbEIsU0FBUyxFQUNULElBQUksQ0FDTCxDQUFBO1NBQ0Y7SUFDSCxDQUFDO0lBRVMsdUNBQWtCLEdBQTVCLFVBQTZCLEtBQWlCO1FBQzVDLElBQUksUUFBUSxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQzthQUNyQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQXFCLENBQUMsQ0FBQTtRQUUxQyxJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxFQUFFLENBQUE7UUFFekMsSUFBSSxDQUFDLGdCQUFnQixDQUNuQixRQUFRLENBQUMsSUFBSSxFQUNiLFFBQVEsQ0FBQyxTQUFTLEVBQ2xCLFFBQVEsQ0FBQyxPQUFPLEVBQ2hCLFFBQVEsQ0FBQyxNQUFNLEVBQ2YsU0FBUyxDQUFDLElBQUksRUFDZCxTQUFTLENBQUMsU0FBUyxFQUNuQixTQUFTLENBQUMsT0FBTyxFQUNqQixTQUFTLENBQUMsTUFBTSxFQUNoQixLQUFLLENBQ04sQ0FBQTtRQUVELE9BQU8sS0FBSyxDQUFBO0lBQ2QsQ0FBQztJQUVTLHFDQUFnQixHQUExQixVQUNFLE9BQW9CLEVBQ3BCLFlBQTBCLEVBQzFCLFVBQXdCLEVBQ3hCLFNBQXVCLEVBQ3ZCLGVBQTZCLEVBQzdCLG9CQUFrQyxFQUNsQyxrQkFBZ0MsRUFDaEMsaUJBQStCLEVBQy9CLGdCQUF3QjtRQUF4QixpQ0FBQSxFQUFBLHdCQUF3QjtRQUV4QixJQUFNLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtRQUVuQyxJQUFJLGVBQWUsSUFBSSxlQUFlLEtBQUssT0FBTyxJQUFJLE9BQU8sS0FBSyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDeEYsR0FBRyxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsWUFBWSxDQUFDLENBQUE7U0FDL0M7UUFFRCxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQyxFQUFFO1lBQ3ZDLEdBQUcsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQyxDQUFBO1lBRXRDLElBQUksU0FBUyxFQUFFO2dCQUNiLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO2dCQUUxQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtnQkFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBO2FBQzlGO2lCQUFNLElBQUksT0FBTyxLQUFLLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDN0MsMEJBQTBCO2dCQUMxQixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtnQkFFMUIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUE7Z0JBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUE7YUFDcEY7aUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDckIsb0JBQW9CO2dCQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTthQUMzRjtTQUNGO2FBQU07WUFDTCxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQTtZQUVuQyxJQUFJLFNBQVMsRUFBRTtnQkFDYixHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUE7Z0JBQ3pDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO2dCQUMxQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtnQkFFekIsSUFBSSxvQkFBb0IsSUFBSSxrQkFBa0IsRUFBRTtvQkFDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRSxrQkFBa0IsRUFBRSxpQkFBaUIsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBO2lCQUN0SDtnQkFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUE7YUFDN0Y7aUJBQU0sSUFBSSxPQUFPLEtBQUssSUFBSSxDQUFDLGlCQUFpQixFQUFFO2dCQUM3Qyx5QkFBeUI7Z0JBQ3pCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFBO2dCQUUxQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtnQkFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQTthQUNuRjtpQkFBTSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNyQixlQUFlO2dCQUNmLElBQUksb0JBQW9CLElBQUksa0JBQWtCLEVBQUU7b0JBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBO2lCQUNuSDtnQkFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTthQUMxRjtTQUNGO0lBQ0gsQ0FBQztJQUVTLHdDQUFtQixHQUE3QjtRQUNFLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQTtRQUN6QyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYyxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQ3hELEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxDQUFBO1FBRWxELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFDMUQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtJQUMvRCxDQUFDO0lBRVMsd0NBQW1CLEdBQTdCO1FBQ0UsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQzVDLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFjLEVBQUUsVUFBVSxDQUFDLENBQUE7UUFDM0QsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLENBQUE7UUFFckQsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtRQUM3RCxNQUFNLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO0lBQ2xFLENBQUM7SUFFUyx1Q0FBa0IsR0FBNUIsVUFBNkIsS0FBaUI7UUFDNUMsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQXFCLENBQUE7UUFFeEMsT0FBTyxNQUFNLEtBQUssSUFBSSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsYUFBYSxFQUFFO1lBQ3RELE1BQU0sR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFBO1NBQzlCO1FBRUQsSUFBSSxNQUFNLEtBQUssSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUMzQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7WUFDWixPQUFPLEtBQUssQ0FBQTtTQUNiO1FBRUQsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBRVMsaUNBQVksR0FBdEIsVUFDRSxZQUEwQixFQUMxQixVQUF3QixFQUN4QixTQUF1QixFQUN2QixFQUFpQixFQUNqQixjQUFxQixFQUNyQixnQkFBd0I7UUFEeEIsK0JBQUEsRUFBQSxxQkFBcUI7UUFDckIsaUNBQUEsRUFBQSx3QkFBd0I7UUFFeEIsSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNoQyxPQUFNO1NBQ1A7UUFFRCxJQUFJLFdBQVcsR0FBRyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUUvRCxJQUFJLGdCQUFnQixLQUFLLElBQUksRUFBRTtZQUM3QixJQUFJLFNBQVMsR0FBRyxZQUFZLENBQUE7WUFDNUIsWUFBWSxHQUFHLFVBQVUsQ0FBQTtZQUN6QixVQUFVLEdBQUcsU0FBUyxDQUFBO1NBQ3ZCO1FBRUQsSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNQLEVBQUUsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFBO1NBQ3hCO1FBRUQsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUU7WUFDbkIsU0FBUyxFQUFFLE9BQUssVUFBWTtTQUM3QixDQUFDLENBQUE7UUFFRixFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRTtZQUNqQixPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUE7UUFFRixFQUFFLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSx1QkFBdUIsRUFBRTtZQUN6QyxTQUFTLEVBQUUsT0FBSyxVQUFZO1lBQzVCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLElBQUksRUFBRTtnQkFDSixNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxPQUFPO2FBQzlCO1NBQ0YsQ0FBQyxDQUFBO1FBRUYsSUFBSSxTQUFTLEVBQUU7WUFDYixFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2dCQUNuRCxTQUFTLEVBQUUsT0FBSyxZQUFjO2FBQy9CLENBQUMsQ0FBQTtZQUVGLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFO2dCQUNoQixPQUFPLEVBQUUsT0FBTzthQUNqQixFQUFFLENBQUMsQ0FBQyxDQUFBO1lBRUwsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUseUJBQXlCLEVBQUU7Z0JBQzFDLFNBQVMsRUFBRSxPQUFLLFVBQVk7Z0JBQzVCLFVBQVUsRUFBRSxpQkFBaUI7Z0JBQzdCLElBQUksRUFBRTtvQkFDSixNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxPQUFPO2lCQUM5QjthQUNGLEVBQUUsT0FBTyxDQUFDLENBQUE7U0FDWjtRQUVELElBQUksY0FBYyxLQUFLLElBQUksRUFBRTtZQUMzQixJQUFJLEtBQUssR0FBRyxxQkFBcUIsQ0FBQTtZQUNqQyxJQUFJLEtBQUssR0FBRyxXQUFXLENBQUE7WUFFdkIsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQ2pELEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRTtvQkFDckIsU0FBUyxFQUFFLE9BQUssWUFBYztpQkFDL0IsRUFBRSxLQUFLLENBQUMsQ0FBQTtnQkFDVCxLQUFLLElBQUksZ0JBQWdCLENBQUE7YUFDMUI7U0FDRjtJQUNILENBQUM7SUFFUyxrQ0FBYSxHQUF2QixVQUNFLFlBQTBCLEVBQzFCLFVBQXdCLEVBQ3hCLFNBQXVCLEVBQ3ZCLEVBQWlCLEVBQ2pCLGNBQXFCLEVBQ3JCLGdCQUF3QjtRQUR4QiwrQkFBQSxFQUFBLHFCQUFxQjtRQUNyQixpQ0FBQSxFQUFBLHdCQUF3QjtRQUV4QixJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ2hDLE9BQU07U0FDUDtRQUVELElBQUksV0FBVyxHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO1FBRXRFLElBQUksZ0JBQWdCLEtBQUssSUFBSSxFQUFFO1lBQzdCLElBQUksU0FBUyxHQUFHLFlBQVksQ0FBQTtZQUM1QixZQUFZLEdBQUcsVUFBVSxDQUFBO1lBQ3pCLFVBQVUsR0FBRyxTQUFTLENBQUE7U0FDdkI7UUFFRCxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ1AsRUFBRSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUE7U0FDeEI7UUFFRCxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRTtZQUNqQixPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUE7UUFFRixJQUFJLGNBQWMsS0FBSyxJQUFJLEVBQUU7WUFDM0IsRUFBRSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xCLFNBQVMsRUFBRSxPQUFLLFlBQWM7YUFDL0IsRUFBRSxDQUFDLENBQUMsQ0FBQTtTQUNOO1FBRUQsRUFBRSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUUsdUJBQXVCLEVBQUU7WUFDekMsU0FBUyxFQUFFLE9BQUssVUFBWTtZQUM1QixJQUFJLEVBQUU7Z0JBQ0osTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsT0FBTzthQUM5QjtZQUNELFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFVBQVUsRUFBRTs7Z0JBQ1YsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFhLEVBQUUsVUFBVSxDQUFDLENBQUE7Z0JBRTFDLElBQUksY0FBYyxLQUFLLElBQUksRUFBRTs7d0JBQzNCLEtBQW1CLElBQUEsZ0JBQUEsaUJBQUEsV0FBVyxDQUFBLHdDQUFBLGlFQUFFOzRCQUEzQixJQUFJLE1BQU0sd0JBQUE7NEJBQ2IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUE7eUJBQ3RDOzs7Ozs7Ozs7aUJBQ0Y7WUFDSCxDQUFDO1NBQ0YsRUFBRSxDQUFDLENBQUMsQ0FBQTtRQUVMLElBQUksU0FBUyxFQUFFO1lBQ2IsRUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUU7Z0JBQ2hCLE9BQU8sRUFBRSxPQUFPO2FBQ2pCLEVBQUUsQ0FBQyxDQUFDLENBQUE7WUFFTCxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSx5QkFBeUIsRUFBRTtnQkFDMUMsU0FBUyxFQUFFLE9BQUssVUFBWTtnQkFDNUIsSUFBSSxFQUFFO29CQUNKLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87aUJBQzlCO2dCQUNELFVBQVUsRUFBRSxnQkFBZ0I7Z0JBQzVCLFVBQVUsRUFBRTs7O3dCQUNWLEtBQW1CLElBQUEsS0FBQSxpQkFBQSxTQUFTLENBQUMsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTs0QkFBbkUsSUFBSSxNQUFNLFdBQUE7NEJBQ2IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUE7eUJBQ3RDOzs7Ozs7Ozs7Z0JBQ0gsQ0FBQzthQUNGLEVBQUUsQ0FBQyxDQUFDLENBQUE7U0FDTjtJQUNILENBQUM7SUFFUyx1Q0FBa0IsR0FBNUI7UUFDRSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtTQUMzQjtJQUNILENBQUM7SUFFRDs7O09BR0c7SUFDTyxnQ0FBVyxHQUFyQjs7O1lBQ0UsS0FBb0IsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBeEUsSUFBSSxPQUFPLFdBQUE7Z0JBQ2QsT0FBTyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTthQUM1RDs7Ozs7Ozs7OztZQUVELEtBQW9CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQXhFLElBQUksT0FBTyxXQUFBO2dCQUNkLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7YUFDNUQ7Ozs7Ozs7OztRQUVELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFFMUUsc0JBQXNCO1FBQ3RCLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUE7UUFDOUQsSUFBSSxVQUFVLEVBQUU7WUFDZCxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO1NBQy9EOztZQUVELEtBQW1CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQWpFLElBQUksTUFBTSxXQUFBO2dCQUNiLElBQUksZUFBZSxHQUFHLElBQUksV0FBVyxDQUFDLE1BQXFCLENBQUMsQ0FBQTtnQkFFNUQsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxvQkFBb0IsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWMsRUFBRSxvQkFBb0IsQ0FBQyxFQUFFO29CQUMzRyxJQUFJLENBQUMsY0FBYyxHQUFHLGVBQWUsQ0FBQTtpQkFDdEM7Z0JBRUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQTthQUM3Qzs7Ozs7Ozs7O0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0gsMEJBQUssR0FBTDtRQUNFLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtRQUM5QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtRQUV6QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBZ0IsQ0FBQTtRQUNoRixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBZ0IsQ0FBQTtRQUVoRixJQUFJLENBQUMsTUFBTSxJQUFJLE9BQU8sSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsRUFBRTtZQUM1RSxNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFBO1NBQ2hDO1FBRUQsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLFFBQVEsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUM7aUJBQ3JDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUVyQixHQUFHLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUE7WUFDNUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUE7WUFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsU0FBVSxFQUFFLFFBQVEsQ0FBQyxPQUFRLEVBQUUsUUFBUSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO1NBQzNHO1FBRUQsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLFFBQVEsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUM7aUJBQ3JDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUVyQixHQUFHLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUE7WUFDNUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUE7WUFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsU0FBVSxFQUFFLFFBQVEsQ0FBQyxPQUFRLEVBQUUsUUFBUSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFBO1NBQzFHO0lBQ0gsQ0FBQztJQUNILGlCQUFDO0FBQUQsQ0FuYUEsQUFtYUMsQ0FuYXdCLFVBQVUsR0FtYWxDO0FBRUQ7SUFNRSx5QkFBWSxHQUFlO1FBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFBO0lBQ3hCLENBQUM7SUFFRCxzQkFBSSxpQ0FBSTthQUFSO1lBQ0UsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFBO1FBQ25CLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0NBQVM7YUFBYjtZQUNFLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQTtRQUN4QixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLG9DQUFPO2FBQVg7WUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7UUFDdEIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxtQ0FBTTthQUFWO1lBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFBO1FBQ3JCLENBQUM7OztPQUFBO0lBRUQsb0NBQVUsR0FBVixVQUFXLE9BQW9CO1FBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxjQUFjLENBQUMsSUFBSSxPQUFPLENBQUMsYUFBYSxFQUFFO1lBQ3RFLE9BQU8sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFBO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUE7UUFFcEIsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUNsRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFBO1FBQ2pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLE1BQUksUUFBVSxDQUFpQixDQUFBO1FBRXpGLE9BQU8sSUFBSSxDQUFBO0lBQ2IsQ0FBQztJQUVELG9DQUFVLEdBQVYsVUFBVyxPQUFvQjtRQUM3QixPQUFPLE9BQU8sQ0FBQyxhQUFhLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsY0FBYyxDQUFDLEVBQUU7Z0JBQzdGLE1BQUs7YUFDTjtZQUVELE9BQU8sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFBO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUE7UUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsYUFBNkIsQ0FBQTtRQUN2RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFXLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBaUIsQ0FBQTtRQUM5RSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFXLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFpQixDQUFBO1FBRS9FLElBQUksT0FBTyxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEVBQUU7WUFDbEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQTtZQUM3QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFpQixDQUFBO1NBQ2pGO1FBRUQsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBRUQsd0NBQWMsR0FBZDtRQUNFLElBQUksSUFBSSxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtRQUVoRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBaUIsQ0FBQTtRQUM3RixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUE7UUFDcEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQWlCLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQTtRQUMzRyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFpQixDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUE7UUFFNUcsT0FBTyxJQUFJLENBQUE7SUFDYixDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFBO0lBQzFELENBQUM7SUFDSCxzQkFBQztBQUFELENBNUVBLEFBNEVDLElBQUE7QUFFRCxNQUFNLFVBQVUsSUFBSTtJQUNsQixtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsVUFBQyxDQUFDO1FBQzVCLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ25CLENBQUMsQ0FBQyxDQUFBO0FBQ0osQ0FBQztBQUVELGVBQWUsVUFBVSxDQUFBIiwiZmlsZSI6Im1haW4vc3JjL25hdmlnYXRpb24vTmF2aWdhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRpbWVsaW5lTGl0ZSwgUG93ZXI0LCBQb3dlcjEgfSBmcm9tIFwiZ3NhcFwiXG5pbXBvcnQgeyBzZWFyY2hBbmRJbml0aWFsaXplIH0gZnJvbSBcIi4uL1V0aWxzXCJcbmltcG9ydCBEb21FbGVtZW50IGZyb20gXCIuLi9Eb21FbGVtZW50XCJcbmltcG9ydCAqIGFzIERvbSBmcm9tIFwiLi4vRG9tRnVuY3Rpb25zXCJcbmltcG9ydCBTZWFyY2hJbnB1dCBmcm9tIFwiLi4vc2VhcmNoL1NlYXJjaElucHV0XCJcblxuY29uc3QgQ0xBU1NfT1BFTiA9IFwiaXMtb3BlblwiXG5jb25zdCBDTEFTU19BQ1RJVkUgPSBcImlzLWFjdGl2ZVwiXG5cbmNvbnN0IFFVRVJZX05BVl9IQU1CVVJHRVIgPSBcIi5uYXYtaGFtYnVyZ2VyXCJcbmNvbnN0IFFVRVJZX05BVl9IQl9CT0RZID0gXCIubmF2X19wcmltYXJ5XCJcblxuY29uc3QgQ0xBU1NfTkFWX0xJTksgPSBcIm5hdi1saW5rLS1oZWFkZXJcIlxuY29uc3QgUVVFUllfTkFWX0xJTktfQUNUSVZFID0gXCIubmF2LWxpbmstLWhlYWRlci5pcy1hY3RpdmVcIlxuXG5jb25zdCBRVUVSWV9OQVZfTU9CSUxFID0gXCIubmF2X19sZXZlbDEgLm5hdl9fbWFpbm5hdiAubmF2X19wcmltYXJ5XCJcbmNvbnN0IFFVRVJZX05BVl9MRVZFTDAgPSBcIi5uYXZfX2xldmVsMFwiXG5jb25zdCBRVUVSWV9OQVZfTEVWRUwwX0NPTlRBSU5FUiA9IFwiLm5hdl9fbGV2ZWwwIC5uYXZfX3N1Ym5hdlwiXG5jb25zdCBRVUVSWV9TRUNUSU9OX09QRU4gPSBcIi5uYXYtc2VjdGlvbi5pcy1vcGVuXCJcblxuY29uc3QgUVVFUllfTkFWX0xFVkVMMSA9IFwiLm5hdl9fbGV2ZWwxIC5uYXZfX21haW5uYXZcIlxuXG5jb25zdCBRVUVSWV9OQVZfTEVWRUwwX0xJTksgPSBcIi5uYXYtbGluay5uYXYtbGluay0taGVhZGVyXCJcbmNvbnN0IFFVRVJZX05BVl9MRVZFTDFfTElOSyA9IFwiLm5hdi1saW5rLS1oZWFkZXJcIlxuXG5jb25zdCBRVUVSWV9OQVZfQ09MVU1OID0gXCIubmF2LWNvbFwiXG5jb25zdCBRVUVSWV9OQVZfQ09MVU1OX0FDVElWRSA9IFwiLm5hdi1jb2wuaXMtYWN0aXZlXCJcblxuY29uc3QgUVVFUllfTkFWX0JPRFkgPSBcIi5uYXYtYm9keVwiXG5jb25zdCBRVUVSWV9OQVZfRk9PVEVSID0gXCIubmF2LWZvb3RlclwiXG5cbmNvbnN0IFFVRVJZX1NFQVJDSF9JQ09OID0gXCIubmF2LXNlYXJjaFwiXG5jb25zdCBRVUVSWV9TRUFSQ0hfRklFTEQgPSBcIi5zZWFyY2hfX2lucHV0XCJcbmNvbnN0IENMQVNTX1NFQVJDSF9ERVNLVE9QID0gXCJzZWFyY2gtLWRlc2t0b3BcIlxuXG5jb25zdCBBTklNQVRJT05fU1RBUlRfREVMQVkgPSAwLjJcbmNvbnN0IEFOSU1BVElPTl9PRkZTRVQgPSAwLjA1XG5cbmNvbnN0IEFOSU1BVElPTl9CT0RZX0RVUkFUSU9OID0gMC4zXG5jb25zdCBBTklNQVRJT05fRk9PVEVSX0RVUkFUSU9OID0gMC4xXG5cbi8qKlxuICogVGhlIG5hdmlnYXRpb24gY29tcG9uZW50IGRlZmluaXRpb24uXG4gKi9cbmNsYXNzIE5hdmlnYXRpb24gZXh0ZW5kcyBEb21FbGVtZW50IHtcbiAgcHVibGljIF9uYXZMZXZlbDA6IEhUTUxFbGVtZW50XG4gIHB1YmxpYyBfbmF2TGV2ZWwwQm9keTogSFRNTEVsZW1lbnRcbiAgcHVibGljIF9uYXZMZXZlbDE6IEhUTUxFbGVtZW50XG5cbiAgcHVibGljIF9oYW1idXJnZXJFbGVtZW50OiBIVE1MRWxlbWVudFxuXG4gIHByaXZhdGUgX25hdk1vYmlsZTogSFRNTEVsZW1lbnRcblxuICBwcml2YXRlIF9zZWFyY2hDb21wb25lbnRzOiBTZWFyY2hJbnB1dFtdXG5cbiAgcHJpdmF0ZSBfbGV2ZWwwQ2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfbGV2ZWwxQ2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfd2luZG93Q2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcbiAgcHJpdmF0ZSBfc2VhcmNoQ2xpY2tIYW5kbGVyOiAoZTogRXZlbnQpID0+IHZvaWRcblxuICBwcml2YXRlIF90bE1haW4/OiBUaW1lbGluZUxpdGVcblxuICBwcml2YXRlIF9zZWFyY2hEZXNrdG9wPzogU2VhcmNoSW5wdXRcblxuICBjb25zdHJ1Y3RvcihlbGVtZW50OiBFbGVtZW50KSB7XG4gICAgc3VwZXIoZWxlbWVudClcblxuICAgIHRoaXMuX25hdkxldmVsMCA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX05BVl9MRVZFTDApIHx8IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIilcbiAgICB0aGlzLl9uYXZMZXZlbDBCb2R5ID0gdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoUVVFUllfTkFWX0xFVkVMMF9DT05UQUlORVIpIHx8IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIilcbiAgICB0aGlzLl9uYXZMZXZlbDEgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfTEVWRUwxKSB8fCBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpXG5cbiAgICB0aGlzLl9uYXZNb2JpbGUgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfTU9CSUxFKSB8fCBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpXG4gICAgaWYgKCF0aGlzLl9uYXZNb2JpbGUucGFyZW50RWxlbWVudCkge1xuICAgICAgbGV0IGR1bW15UGFyZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKVxuICAgICAgZHVtbXlQYXJlbnQuYXBwZW5kQ2hpbGQodGhpcy5fbmF2TW9iaWxlKVxuICAgIH1cblxuICAgIHRoaXMuX2hhbWJ1cmdlckVsZW1lbnQgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfSEFNQlVSR0VSKSB8fCBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpXG4gICAgdGhpcy5fc2VhcmNoQ29tcG9uZW50cyA9IFtdXG5cbiAgICB0aGlzLl9sZXZlbDBDbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVMZXZlbDBDbGljay5iaW5kKHRoaXMpXG4gICAgdGhpcy5fbGV2ZWwxQ2xpY2tIYW5kbGVyID0gdGhpcy5faGFuZGxlTGV2ZWwxQ2xpY2suYmluZCh0aGlzKVxuICAgIHRoaXMuX3dpbmRvd0NsaWNrSGFuZGxlciA9IHRoaXMuX2hhbmRsZVdpbmRvd0NsaWNrLmJpbmQodGhpcylcbiAgICB0aGlzLl9zZWFyY2hDbGlja0hhbmRsZXIgPSB0aGlzLl9oYW5kbGVTZWFyY2hDbGljay5iaW5kKHRoaXMpXG5cbiAgICB0aGlzLl9pbml0aWFsaXplKClcbiAgfVxuXG4gIHByb3RlY3RlZCBfcmVzZXRNYWluVGltZWxpbmUoKSB7XG4gICAgaWYgKHRoaXMuX3RsTWFpbikge1xuICAgICAgdGhpcy5fdGxNYWluLmtpbGwoKVxuICAgIH1cblxuICAgIHRoaXMuX3RsTWFpbiA9IG5ldyBUaW1lbGluZUxpdGUoe1xuICAgICAgb25Db21wbGV0ZTogKCkgPT4ge1xuICAgICAgICB0aGlzLl90bE1haW4gPSB1bmRlZmluZWRcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgcHJvdGVjdGVkIF9pc01vYmlsZSgpIHtcbiAgICByZXR1cm4gRG9tLmlzSGlkZGVuKHRoaXMuX2hhbWJ1cmdlckVsZW1lbnQsIHRydWUpID09PSBmYWxzZVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVMZXZlbDBDbGljayhldmVudDogTW91c2VFdmVudCkge1xuICAgIGNvbnN0IGlzRGVza3RvcCA9ICF0aGlzLl9pc01vYmlsZSgpXG5cbiAgICBpZiAoaXNEZXNrdG9wKSB7XG4gICAgICB0aGlzLl9yZXNldE1haW5UaW1lbGluZSgpXG5cbiAgICAgIGxldCBuYXZJdGVtcyA9IG5ldyBOYXZpZ2F0aW9uSXRlbXModGhpcylcbiAgICAgICAgLmZyb21MZXZlbDAoZXZlbnQudGFyZ2V0IGFzIEhUTUxFbGVtZW50KVxuXG4gICAgICBpZiAoIW5hdkl0ZW1zLnNlY3Rpb24pIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGxldCBwcmV2aW91c05hdkxpbmsgPSB0aGlzLl9uYXZMZXZlbDAucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfTElOS19BQ1RJVkUpISBhcyBIVE1MRWxlbWVudFxuICAgICAgbGV0IHByZXZpb3VzTmF2U2VjdGlvbiA9IHRoaXMuX25hdkxldmVsMC5xdWVyeVNlbGVjdG9yKFFVRVJZX1NFQ1RJT05fT1BFTikhIGFzIEhUTUxFbGVtZW50XG5cbiAgICAgIHRoaXMuX3RvZ2dsZUNvbnRhaW5lcihcbiAgICAgICAgbmF2SXRlbXMubGluayxcbiAgICAgICAgdGhpcy5fbmF2TGV2ZWwwQm9keSxcbiAgICAgICAgbmF2SXRlbXMuc2VjdGlvbixcbiAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICBwcmV2aW91c05hdkxpbmssXG4gICAgICAgIHRoaXMuX25hdkxldmVsMEJvZHksXG4gICAgICAgIHByZXZpb3VzTmF2U2VjdGlvbixcbiAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICB0cnVlXG4gICAgICApXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVMZXZlbDFDbGljayhldmVudDogTW91c2VFdmVudCkge1xuICAgIGxldCBuYXZJdGVtcyA9IG5ldyBOYXZpZ2F0aW9uSXRlbXModGhpcylcbiAgICAgIC5mcm9tTGV2ZWwxKGV2ZW50LnRhcmdldCBhcyBIVE1MRWxlbWVudClcblxuICAgIGxldCBwcmV2SXRlbXMgPSBuYXZJdGVtcy5wcmV2aW91c0xldmVsMSgpXG5cbiAgICB0aGlzLl90b2dnbGVDb250YWluZXIoXG4gICAgICBuYXZJdGVtcy5saW5rLFxuICAgICAgbmF2SXRlbXMuY29udGFpbmVyLFxuICAgICAgbmF2SXRlbXMuc2VjdGlvbixcbiAgICAgIG5hdkl0ZW1zLmZvb3RlcixcbiAgICAgIHByZXZJdGVtcy5saW5rLFxuICAgICAgcHJldkl0ZW1zLmNvbnRhaW5lcixcbiAgICAgIHByZXZJdGVtcy5zZWN0aW9uLFxuICAgICAgcHJldkl0ZW1zLmZvb3RlcixcbiAgICAgIGZhbHNlXG4gICAgKVxuXG4gICAgcmV0dXJuIGZhbHNlXG4gIH1cblxuICBwcm90ZWN0ZWQgX3RvZ2dsZUNvbnRhaW5lcihcbiAgICBuYXZMaW5rOiBIVE1MRWxlbWVudCxcbiAgICBuYXZDb250YWluZXI/OiBIVE1MRWxlbWVudCxcbiAgICBuYXZTZWN0aW9uPzogSFRNTEVsZW1lbnQsXG4gICAgbmF2Rm9vdGVyPzogSFRNTEVsZW1lbnQsXG4gICAgcHJldmlvdXNOYXZMaW5rPzogSFRNTEVsZW1lbnQsXG4gICAgcHJldmlvdXNOYXZDb250YWluZXI/OiBIVE1MRWxlbWVudCxcbiAgICBwcmV2aW91c05hdlNlY3Rpb24/OiBIVE1MRWxlbWVudCxcbiAgICBwcmV2aW91c05hdkZvb3Rlcj86IEhUTUxFbGVtZW50LFxuICAgIGFuaW1hdGVDb250YWluZXIgPSBmYWxzZVxuICApIHtcbiAgICBjb25zdCBpc0Rlc2t0b3AgPSAhdGhpcy5faXNNb2JpbGUoKVxuXG4gICAgaWYgKHByZXZpb3VzTmF2TGluayAmJiBwcmV2aW91c05hdkxpbmsgIT09IG5hdkxpbmsgJiYgbmF2TGluayAhPT0gdGhpcy5faGFtYnVyZ2VyRWxlbWVudCkge1xuICAgICAgRG9tLnJlbW92ZUNsYXNzKHByZXZpb3VzTmF2TGluaywgQ0xBU1NfQUNUSVZFKVxuICAgIH1cblxuICAgIGlmIChEb20uaGFzQ2xhc3MobmF2TGluaywgQ0xBU1NfQUNUSVZFKSkge1xuICAgICAgRG9tLnJlbW92ZUNsYXNzKG5hdkxpbmssIENMQVNTX0FDVElWRSlcblxuICAgICAgaWYgKGlzRGVza3RvcCkge1xuICAgICAgICB0aGlzLl9vbk5hdmlnYXRpb25DbG9zZWQoKVxuXG4gICAgICAgIHRoaXMuX3Jlc2V0TWFpblRpbWVsaW5lKClcbiAgICAgICAgdGhpcy5fY2xvc2VTZWN0aW9uKG5hdkNvbnRhaW5lciwgbmF2U2VjdGlvbiwgbmF2Rm9vdGVyLCB0aGlzLl90bE1haW4sIHRydWUsIGFuaW1hdGVDb250YWluZXIpXG4gICAgICB9IGVsc2UgaWYgKG5hdkxpbmsgPT09IHRoaXMuX2hhbWJ1cmdlckVsZW1lbnQpIHtcbiAgICAgICAgLy8gQ2xvc2UgbW9iaWxlIG5hdmlnYXRpb25cbiAgICAgICAgdGhpcy5fb25OYXZpZ2F0aW9uQ2xvc2VkKClcblxuICAgICAgICB0aGlzLl9yZXNldE1haW5UaW1lbGluZSgpXG4gICAgICAgIHRoaXMuX2Nsb3NlU2VjdGlvbihuYXZDb250YWluZXIsIG5hdlNlY3Rpb24sIHVuZGVmaW5lZCwgdGhpcy5fdGxNYWluLCBmYWxzZSwgZmFsc2UpXG4gICAgICB9IGVsc2UgaWYgKCFpc0Rlc2t0b3ApIHtcbiAgICAgICAgLy8gQ2xvc2UgdGhlIHNlY3Rpb25cbiAgICAgICAgdGhpcy5fY2xvc2VTZWN0aW9uKG5hdkNvbnRhaW5lciwgbmF2U2VjdGlvbiwgbmF2Rm9vdGVyLCB1bmRlZmluZWQsIHRydWUsIGFuaW1hdGVDb250YWluZXIpXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIERvbS5hZGRDbGFzcyhuYXZMaW5rLCBDTEFTU19BQ1RJVkUpXG5cbiAgICAgIGlmIChpc0Rlc2t0b3ApIHtcbiAgICAgICAgRG9tLmFkZENsYXNzKHRoaXMuX25hdk1vYmlsZSwgQ0xBU1NfT1BFTilcbiAgICAgICAgdGhpcy5fb25OYXZpZ2F0aW9uT3BlbmVkKClcbiAgICAgICAgdGhpcy5fcmVzZXRNYWluVGltZWxpbmUoKVxuXG4gICAgICAgIGlmIChwcmV2aW91c05hdkNvbnRhaW5lciAmJiBwcmV2aW91c05hdlNlY3Rpb24pIHtcbiAgICAgICAgICB0aGlzLl9jbG9zZVNlY3Rpb24ocHJldmlvdXNOYXZDb250YWluZXIsIHByZXZpb3VzTmF2U2VjdGlvbiwgcHJldmlvdXNOYXZGb290ZXIsIHRoaXMuX3RsTWFpbiwgdHJ1ZSwgYW5pbWF0ZUNvbnRhaW5lcilcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9vcGVuU2VjdGlvbihuYXZDb250YWluZXIsIG5hdlNlY3Rpb24sIG5hdkZvb3RlciwgdGhpcy5fdGxNYWluLCB0cnVlLCBhbmltYXRlQ29udGFpbmVyKVxuICAgICAgfSBlbHNlIGlmIChuYXZMaW5rID09PSB0aGlzLl9oYW1idXJnZXJFbGVtZW50KSB7XG4gICAgICAgIC8vIE9wZW4gbW9iaWxlIG5hdmlnYXRpb25cbiAgICAgICAgdGhpcy5fb25OYXZpZ2F0aW9uT3BlbmVkKClcblxuICAgICAgICB0aGlzLl9yZXNldE1haW5UaW1lbGluZSgpXG4gICAgICAgIHRoaXMuX29wZW5TZWN0aW9uKG5hdkNvbnRhaW5lciwgbmF2U2VjdGlvbiwgdW5kZWZpbmVkLCB0aGlzLl90bE1haW4sIGZhbHNlLCBmYWxzZSlcbiAgICAgIH0gZWxzZSBpZiAoIWlzRGVza3RvcCkge1xuICAgICAgICAvLyBPcGVuIHNlY3Rpb25cbiAgICAgICAgaWYgKHByZXZpb3VzTmF2Q29udGFpbmVyICYmIHByZXZpb3VzTmF2U2VjdGlvbikge1xuICAgICAgICAgIHRoaXMuX2Nsb3NlU2VjdGlvbihwcmV2aW91c05hdkNvbnRhaW5lciwgcHJldmlvdXNOYXZTZWN0aW9uLCBwcmV2aW91c05hdkZvb3RlciwgdW5kZWZpbmVkLCB0cnVlLCBhbmltYXRlQ29udGFpbmVyKVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX29wZW5TZWN0aW9uKG5hdkNvbnRhaW5lciwgbmF2U2VjdGlvbiwgbmF2Rm9vdGVyLCB1bmRlZmluZWQsIHRydWUsIGFuaW1hdGVDb250YWluZXIpXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9vbk5hdmlnYXRpb25PcGVuZWQoKSB7XG4gICAgRG9tLmFkZENsYXNzKHRoaXMuX25hdk1vYmlsZSwgQ0xBU1NfT1BFTilcbiAgICBEb20uYWRkQ2xhc3ModGhpcy5fbmF2TW9iaWxlLnBhcmVudEVsZW1lbnQhLCBDTEFTU19PUEVOKVxuICAgIERvbS5hZGRDbGFzcyh0aGlzLl9oYW1idXJnZXJFbGVtZW50LCBDTEFTU19BQ1RJVkUpXG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX3dpbmRvd0NsaWNrSGFuZGxlcilcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInRvdWNoZW5kXCIsIHRoaXMuX3dpbmRvd0NsaWNrSGFuZGxlcilcbiAgfVxuXG4gIHByb3RlY3RlZCBfb25OYXZpZ2F0aW9uQ2xvc2VkKCkge1xuICAgIERvbS5yZW1vdmVDbGFzcyh0aGlzLl9uYXZNb2JpbGUsIENMQVNTX09QRU4pXG4gICAgRG9tLnJlbW92ZUNsYXNzKHRoaXMuX25hdk1vYmlsZS5wYXJlbnRFbGVtZW50ISwgQ0xBU1NfT1BFTilcbiAgICBEb20ucmVtb3ZlQ2xhc3ModGhpcy5faGFtYnVyZ2VyRWxlbWVudCwgQ0xBU1NfQUNUSVZFKVxuXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG4gICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJ0b3VjaGVuZFwiLCB0aGlzLl93aW5kb3dDbGlja0hhbmRsZXIpXG4gIH1cblxuICBwcm90ZWN0ZWQgX2hhbmRsZVdpbmRvd0NsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgbGV0IHRhcmdldCA9IGV2ZW50LnRhcmdldCBhcyBIVE1MRWxlbWVudFxuXG4gICAgd2hpbGUgKHRhcmdldCAhPT0gdGhpcy5lbGVtZW50ICYmIHRhcmdldC5wYXJlbnRFbGVtZW50KSB7XG4gICAgICB0YXJnZXQgPSB0YXJnZXQucGFyZW50RWxlbWVudFxuICAgIH1cblxuICAgIGlmICh0YXJnZXQgIT09IHRoaXMuZWxlbWVudCkge1xuICAgICAgdGhpcy5jbG9zZSgpXG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG5cbiAgICByZXR1cm4gdHJ1ZVxuICB9XG5cbiAgcHJvdGVjdGVkIF9vcGVuU2VjdGlvbihcbiAgICBuYXZDb250YWluZXI/OiBIVE1MRWxlbWVudCxcbiAgICBuYXZTZWN0aW9uPzogSFRNTEVsZW1lbnQsXG4gICAgbmF2Rm9vdGVyPzogSFRNTEVsZW1lbnQsXG4gICAgdGw/OiBUaW1lbGluZUxpdGUsXG4gICAgYW5pbWF0ZUNvbHVtbnMgPSB0cnVlLFxuICAgIGFuaW1hdGVDb250YWluZXIgPSBmYWxzZVxuICApIHtcbiAgICBpZiAoIW5hdlNlY3Rpb24gfHwgIW5hdkNvbnRhaW5lcikge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgbGV0IGFjdGl2ZUl0ZW1zID0gbmF2U2VjdGlvbi5xdWVyeVNlbGVjdG9yQWxsKFFVRVJZX05BVl9DT0xVTU4pXG5cbiAgICBpZiAoYW5pbWF0ZUNvbnRhaW5lciA9PT0gdHJ1ZSkge1xuICAgICAgbGV0IGNvbnRhaW5lciA9IG5hdkNvbnRhaW5lclxuICAgICAgbmF2Q29udGFpbmVyID0gbmF2U2VjdGlvblxuICAgICAgbmF2U2VjdGlvbiA9IGNvbnRhaW5lclxuICAgIH1cblxuICAgIGlmICghdGwpIHtcbiAgICAgIHRsID0gbmV3IFRpbWVsaW5lTGl0ZSgpXG4gICAgfVxuXG4gICAgdGwuc2V0KG5hdkNvbnRhaW5lciwge1xuICAgICAgY2xhc3NOYW1lOiBgKz0ke0NMQVNTX09QRU59YFxuICAgIH0pXG5cbiAgICB0bC5zZXQobmF2U2VjdGlvbiwge1xuICAgICAgZGlzcGxheTogXCJibG9ja1wiXG4gICAgfSlcblxuICAgIHRsLnRvKG5hdlNlY3Rpb24sIEFOSU1BVElPTl9CT0RZX0RVUkFUSU9OLCB7XG4gICAgICBjbGFzc05hbWU6IGArPSR7Q0xBU1NfT1BFTn1gLFxuICAgICAgY2xlYXJQcm9wczogXCJhbGxcIixcbiAgICAgIGVhc2U6IFtcbiAgICAgICAgUG93ZXIxLmVhc2VJbiwgUG93ZXI0LmVhc2VPdXRcbiAgICAgIF1cbiAgICB9KVxuXG4gICAgaWYgKG5hdkZvb3Rlcikge1xuICAgICAgdGwuc2V0KG5hdkZvb3Rlci5xdWVyeVNlbGVjdG9yQWxsKFFVRVJZX05BVl9DT0xVTU4pLCB7XG4gICAgICAgIGNsYXNzTmFtZTogYCs9JHtDTEFTU19BQ1RJVkV9YFxuICAgICAgfSlcblxuICAgICAgdGwuc2V0KG5hdkZvb3Rlciwge1xuICAgICAgICBkaXNwbGF5OiBcImJsb2NrXCJcbiAgICAgIH0sIDApXG5cbiAgICAgIHRsLnRvKG5hdkZvb3RlciwgQU5JTUFUSU9OX0ZPT1RFUl9EVVJBVElPTiwge1xuICAgICAgICBjbGFzc05hbWU6IGArPSR7Q0xBU1NfT1BFTn1gLFxuICAgICAgICBjbGVhclByb3BzOiBcImhlaWdodCwgZGlzcGxheVwiLFxuICAgICAgICBlYXNlOiBbXG4gICAgICAgICAgUG93ZXIxLmVhc2VJbiwgUG93ZXI0LmVhc2VPdXRcbiAgICAgICAgXVxuICAgICAgfSwgXCItPTAuMVwiKVxuICAgIH1cblxuICAgIGlmIChhbmltYXRlQ29sdW1ucyA9PT0gdHJ1ZSkge1xuICAgICAgbGV0IGRlbGF5ID0gQU5JTUFUSU9OX1NUQVJUX0RFTEFZXG4gICAgICBsZXQgaXRlbXMgPSBhY3RpdmVJdGVtc1xuXG4gICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgaXRlbXMubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICAgIHRsLnRvKGl0ZW1zW2luZGV4XSwgMCwge1xuICAgICAgICAgIGNsYXNzTmFtZTogYCs9JHtDTEFTU19BQ1RJVkV9YFxuICAgICAgICB9LCBkZWxheSlcbiAgICAgICAgZGVsYXkgKz0gQU5JTUFUSU9OX09GRlNFVFxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBfY2xvc2VTZWN0aW9uKFxuICAgIG5hdkNvbnRhaW5lcj86IEhUTUxFbGVtZW50LFxuICAgIG5hdlNlY3Rpb24/OiBIVE1MRWxlbWVudCxcbiAgICBuYXZGb290ZXI/OiBIVE1MRWxlbWVudCxcbiAgICB0bD86IFRpbWVsaW5lTGl0ZSxcbiAgICBhbmltYXRlQ29sdW1ucyA9IHRydWUsXG4gICAgYW5pbWF0ZUNvbnRhaW5lciA9IGZhbHNlXG4gICkge1xuICAgIGlmICghbmF2U2VjdGlvbiB8fCAhbmF2Q29udGFpbmVyKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG5cbiAgICBsZXQgYWN0aXZlSXRlbXMgPSBuYXZTZWN0aW9uLnF1ZXJ5U2VsZWN0b3JBbGwoUVVFUllfTkFWX0NPTFVNTl9BQ1RJVkUpXG5cbiAgICBpZiAoYW5pbWF0ZUNvbnRhaW5lciA9PT0gdHJ1ZSkge1xuICAgICAgbGV0IGNvbnRhaW5lciA9IG5hdkNvbnRhaW5lclxuICAgICAgbmF2Q29udGFpbmVyID0gbmF2U2VjdGlvblxuICAgICAgbmF2U2VjdGlvbiA9IGNvbnRhaW5lclxuICAgIH1cblxuICAgIGlmICghdGwpIHtcbiAgICAgIHRsID0gbmV3IFRpbWVsaW5lTGl0ZSgpXG4gICAgfVxuXG4gICAgdGwuc2V0KG5hdlNlY3Rpb24sIHtcbiAgICAgIGRpc3BsYXk6IFwiYmxvY2tcIlxuICAgIH0pXG5cbiAgICBpZiAoYW5pbWF0ZUNvbHVtbnMgPT09IHRydWUpIHtcbiAgICAgIHRsLnNldChhY3RpdmVJdGVtcywge1xuICAgICAgICBjbGFzc05hbWU6IGAtPSR7Q0xBU1NfQUNUSVZFfWBcbiAgICAgIH0sIDApXG4gICAgfVxuXG4gICAgdGwudG8obmF2U2VjdGlvbiwgQU5JTUFUSU9OX0JPRFlfRFVSQVRJT04sIHtcbiAgICAgIGNsYXNzTmFtZTogYC09JHtDTEFTU19PUEVOfWAsXG4gICAgICBlYXNlOiBbXG4gICAgICAgIFBvd2VyMS5lYXNlSW4sIFBvd2VyNC5lYXNlT3V0XG4gICAgICBdLFxuICAgICAgY2xlYXJQcm9wczogXCJhbGxcIixcbiAgICAgIG9uQ29tcGxldGU6ICgpID0+IHtcbiAgICAgICAgRG9tLnJlbW92ZUNsYXNzKG5hdkNvbnRhaW5lciEsIENMQVNTX09QRU4pXG5cbiAgICAgICAgaWYgKGFuaW1hdGVDb2x1bW5zID09PSB0cnVlKSB7XG4gICAgICAgICAgZm9yIChsZXQgYWN0aXZlIG9mIGFjdGl2ZUl0ZW1zKSB7XG4gICAgICAgICAgICBEb20ucmVtb3ZlQ2xhc3MoYWN0aXZlLCBDTEFTU19BQ1RJVkUpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgMClcblxuICAgIGlmIChuYXZGb290ZXIpIHtcbiAgICAgIHRsLnNldChuYXZGb290ZXIsIHtcbiAgICAgICAgZGlzcGxheTogXCJibG9ja1wiXG4gICAgICB9LCAwKVxuXG4gICAgICB0bC50byhuYXZGb290ZXIsIEFOSU1BVElPTl9GT09URVJfRFVSQVRJT04sIHtcbiAgICAgICAgY2xhc3NOYW1lOiBgLT0ke0NMQVNTX09QRU59YCxcbiAgICAgICAgZWFzZTogW1xuICAgICAgICAgIFBvd2VyMS5lYXNlSW4sIFBvd2VyNC5lYXNlT3V0XG4gICAgICAgIF0sXG4gICAgICAgIGNsZWFyUHJvcHM6IFwiaGVpZ2h0LGRpc3BsYXlcIixcbiAgICAgICAgb25Db21wbGV0ZTogKCkgPT4ge1xuICAgICAgICAgIGZvciAobGV0IGFjdGl2ZSBvZiBuYXZGb290ZXIucXVlcnlTZWxlY3RvckFsbChRVUVSWV9OQVZfQ09MVU1OX0FDVElWRSkpIHtcbiAgICAgICAgICAgIERvbS5yZW1vdmVDbGFzcyhhY3RpdmUsIENMQVNTX0FDVElWRSlcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0sIDApXG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIF9oYW5kbGVTZWFyY2hDbGljaygpIHtcbiAgICBpZiAodGhpcy5fc2VhcmNoRGVza3RvcCkge1xuICAgICAgdGhpcy5fc2VhcmNoRGVza3RvcC5vcGVuKClcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZXMgdGhlIG5hdmlnYXRpb24gY29tcG9uZW50LlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJvdGVjdGVkIF9pbml0aWFsaXplKCkge1xuICAgIGZvciAobGV0IG5hdkxpbmsgb2YgdGhpcy5fbmF2TGV2ZWwwLnF1ZXJ5U2VsZWN0b3JBbGwoUVVFUllfTkFWX0xFVkVMMF9MSU5LKSkge1xuICAgICAgbmF2TGluay5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fbGV2ZWwwQ2xpY2tIYW5kbGVyKVxuICAgIH1cblxuICAgIGZvciAobGV0IG5hdkxpbmsgb2YgdGhpcy5fbmF2TGV2ZWwxLnF1ZXJ5U2VsZWN0b3JBbGwoUVVFUllfTkFWX0xFVkVMMV9MSU5LKSkge1xuICAgICAgbmF2TGluay5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5fbGV2ZWwxQ2xpY2tIYW5kbGVyKVxuICAgIH1cblxuICAgIHRoaXMuX2hhbWJ1cmdlckVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuX2xldmVsMUNsaWNrSGFuZGxlcilcblxuICAgIC8vIERlc2t0b3Agc2VhcmNoIGljb25cbiAgICBsZXQgc2VhcmNoSWNvbiA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFFVRVJZX1NFQVJDSF9JQ09OKVxuICAgIGlmIChzZWFyY2hJY29uKSB7XG4gICAgICBzZWFyY2hJY29uLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLl9zZWFyY2hDbGlja0hhbmRsZXIpXG4gICAgfVxuXG4gICAgZm9yIChsZXQgc2VhcmNoIG9mIHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFFVRVJZX1NFQVJDSF9GSUVMRCkpIHtcbiAgICAgIGxldCBzZWFyY2hDb21wb25lbnQgPSBuZXcgU2VhcmNoSW5wdXQoc2VhcmNoIGFzIEhUTUxFbGVtZW50KVxuXG4gICAgICBpZiAoRG9tLmhhc0NsYXNzKHNlYXJjaCwgQ0xBU1NfU0VBUkNIX0RFU0tUT1ApIHx8IERvbS5oYXNDbGFzcyhzZWFyY2gucGFyZW50RWxlbWVudCEsIENMQVNTX1NFQVJDSF9ERVNLVE9QKSkge1xuICAgICAgICB0aGlzLl9zZWFyY2hEZXNrdG9wID0gc2VhcmNoQ29tcG9uZW50XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3NlYXJjaENvbXBvbmVudHMucHVzaChzZWFyY2hDb21wb25lbnQpXG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENsb3NlcyB0aGUgbmF2aWdhdGlvbi5cbiAgICovXG4gIGNsb3NlKCkge1xuICAgIGxldCBpc01vYmxlID0gdGhpcy5faXNNb2JpbGUoKVxuICAgIHRoaXMuX3Jlc2V0TWFpblRpbWVsaW5lKClcblxuICAgIGxldCBsZXZlbDEgPSB0aGlzLl9uYXZMZXZlbDEucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfTElOS19BQ1RJVkUpIGFzIEhUTUxFbGVtZW50XG4gICAgbGV0IGxldmVsMCA9IHRoaXMuX25hdkxldmVsMC5xdWVyeVNlbGVjdG9yKFFVRVJZX05BVl9MSU5LX0FDVElWRSkgYXMgSFRNTEVsZW1lbnRcblxuICAgIGlmICghbGV2ZWwxICYmIGlzTW9ibGUgJiYgRG9tLmhhc0NsYXNzKHRoaXMuX2hhbWJ1cmdlckVsZW1lbnQsIENMQVNTX0FDVElWRSkpIHtcbiAgICAgIGxldmVsMSA9IHRoaXMuX2hhbWJ1cmdlckVsZW1lbnRcbiAgICB9XG5cbiAgICBpZiAobGV2ZWwxKSB7XG4gICAgICBsZXQgbmF2SXRlbXMgPSBuZXcgTmF2aWdhdGlvbkl0ZW1zKHRoaXMpXG4gICAgICAgIC5mcm9tTGV2ZWwxKGxldmVsMSlcblxuICAgICAgRG9tLnJlbW92ZUNsYXNzKG5hdkl0ZW1zLmxpbmssIENMQVNTX0FDVElWRSlcbiAgICAgIHRoaXMuX29uTmF2aWdhdGlvbkNsb3NlZCgpXG4gICAgICB0aGlzLl9jbG9zZVNlY3Rpb24obmF2SXRlbXMuY29udGFpbmVyISwgbmF2SXRlbXMuc2VjdGlvbiEsIG5hdkl0ZW1zLmZvb3RlciwgdGhpcy5fdGxNYWluLCAhaXNNb2JsZSwgZmFsc2UpXG4gICAgfVxuXG4gICAgaWYgKGxldmVsMCkge1xuICAgICAgbGV0IG5hdkl0ZW1zID0gbmV3IE5hdmlnYXRpb25JdGVtcyh0aGlzKVxuICAgICAgICAuZnJvbUxldmVsMChsZXZlbDApXG5cbiAgICAgIERvbS5yZW1vdmVDbGFzcyhuYXZJdGVtcy5saW5rLCBDTEFTU19BQ1RJVkUpXG4gICAgICB0aGlzLl9vbk5hdmlnYXRpb25DbG9zZWQoKVxuICAgICAgdGhpcy5fY2xvc2VTZWN0aW9uKG5hdkl0ZW1zLmNvbnRhaW5lciEsIG5hdkl0ZW1zLnNlY3Rpb24hLCBuYXZJdGVtcy5mb290ZXIsIHRoaXMuX3RsTWFpbiwgIWlzTW9ibGUsIHRydWUpXG4gICAgfVxuICB9XG59XG5cbmNsYXNzIE5hdmlnYXRpb25JdGVtcyB7XG4gIHByaXZhdGUgX25hdmlnYXRpb246IE5hdmlnYXRpb25cbiAgcHJpdmF0ZSBfbGluayE6IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX2NvbnRhaW5lcj86IEhUTUxFbGVtZW50XG4gIHByaXZhdGUgX3NlY3Rpb24/OiBIVE1MRWxlbWVudFxuICBwcml2YXRlIF9mb290ZXI/OiBIVE1MRWxlbWVudFxuICBjb25zdHJ1Y3RvcihuYXY6IE5hdmlnYXRpb24pIHtcbiAgICB0aGlzLl9uYXZpZ2F0aW9uID0gbmF2XG4gIH1cblxuICBnZXQgbGluaygpIHtcbiAgICByZXR1cm4gdGhpcy5fbGlua1xuICB9XG5cbiAgZ2V0IGNvbnRhaW5lcigpIHtcbiAgICByZXR1cm4gdGhpcy5fY29udGFpbmVyXG4gIH1cblxuICBnZXQgc2VjdGlvbigpIHtcbiAgICByZXR1cm4gdGhpcy5fc2VjdGlvblxuICB9XG5cbiAgZ2V0IGZvb3RlcigpIHtcbiAgICByZXR1cm4gdGhpcy5fZm9vdGVyXG4gIH1cblxuICBmcm9tTGV2ZWwwKG5hdkxpbms6IEhUTUxFbGVtZW50KSB7XG4gICAgd2hpbGUgKCFEb20uaGFzQ2xhc3MobmF2TGluaywgQ0xBU1NfTkFWX0xJTkspICYmIG5hdkxpbmsucGFyZW50RWxlbWVudCkge1xuICAgICAgbmF2TGluayA9IG5hdkxpbmsucGFyZW50RWxlbWVudFxuICAgIH1cblxuICAgIHRoaXMuX2xpbmsgPSBuYXZMaW5rXG5cbiAgICBsZXQgdG9nZ2xlSWQgPSBuYXZMaW5rLmdldEF0dHJpYnV0ZShcImRhdGEtdG9nZ2xlXCIpXG4gICAgdGhpcy5fY29udGFpbmVyID0gdGhpcy5fbmF2aWdhdGlvbi5fbmF2TGV2ZWwwQm9keVxuICAgIHRoaXMuX3NlY3Rpb24gPSB0aGlzLl9uYXZpZ2F0aW9uLl9uYXZMZXZlbDAucXVlcnlTZWxlY3RvcihgIyR7dG9nZ2xlSWR9YCkhIGFzIEhUTUxFbGVtZW50XG5cbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgZnJvbUxldmVsMShuYXZMaW5rOiBIVE1MRWxlbWVudCkge1xuICAgIHdoaWxlIChuYXZMaW5rLnBhcmVudEVsZW1lbnQpIHtcbiAgICAgIGlmICgobmF2TGluayA9PT0gdGhpcy5fbmF2aWdhdGlvbi5faGFtYnVyZ2VyRWxlbWVudCkgfHwgRG9tLmhhc0NsYXNzKG5hdkxpbmssIENMQVNTX05BVl9MSU5LKSkge1xuICAgICAgICBicmVha1xuICAgICAgfVxuXG4gICAgICBuYXZMaW5rID0gbmF2TGluay5wYXJlbnRFbGVtZW50XG4gICAgfVxuXG4gICAgdGhpcy5fbGluayA9IG5hdkxpbmtcbiAgICB0aGlzLl9jb250YWluZXIgPSBuYXZMaW5rLnBhcmVudEVsZW1lbnQhIGFzIEhUTUxFbGVtZW50XG4gICAgdGhpcy5fc2VjdGlvbiA9IHRoaXMuX2NvbnRhaW5lciEucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfQk9EWSkhIGFzIEhUTUxFbGVtZW50XG4gICAgdGhpcy5fZm9vdGVyID0gdGhpcy5fY29udGFpbmVyIS5xdWVyeVNlbGVjdG9yKFFVRVJZX05BVl9GT09URVIpISBhcyBIVE1MRWxlbWVudFxuXG4gICAgaWYgKG5hdkxpbmsgPT09IHRoaXMuX25hdmlnYXRpb24uX2hhbWJ1cmdlckVsZW1lbnQpIHtcbiAgICAgIHRoaXMuX2NvbnRhaW5lciA9IHRoaXMuX25hdmlnYXRpb24uX25hdkxldmVsMVxuICAgICAgdGhpcy5fc2VjdGlvbiA9IHRoaXMuX2NvbnRhaW5lci5xdWVyeVNlbGVjdG9yKFFVRVJZX05BVl9IQl9CT0RZKSEgYXMgSFRNTEVsZW1lbnRcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpc1xuICB9XG5cbiAgcHJldmlvdXNMZXZlbDEoKSB7XG4gICAgbGV0IHByZXYgPSBuZXcgTmF2aWdhdGlvbkl0ZW1zKHRoaXMuX25hdmlnYXRpb24pXG5cbiAgICBwcmV2Ll9saW5rID0gdGhpcy5fbmF2aWdhdGlvbi5fbmF2TGV2ZWwxLnF1ZXJ5U2VsZWN0b3IoUVVFUllfTkFWX0xJTktfQUNUSVZFKSEgYXMgSFRNTEVsZW1lbnRcbiAgICBwcmV2Ll9jb250YWluZXIgPSBwcmV2Ll9saW5rID8gcHJldi5fbGluay5wYXJlbnRFbGVtZW50ISA6IHVuZGVmaW5lZFxuICAgIHByZXYuX3NlY3Rpb24gPSBwcmV2Ll9jb250YWluZXIgPyBwcmV2Ll9jb250YWluZXIucXVlcnlTZWxlY3RvcihRVUVSWV9OQVZfQk9EWSkhIGFzIEhUTUxFbGVtZW50IDogdW5kZWZpbmVkXG4gICAgcHJldi5fZm9vdGVyID0gcHJldi5fY29udGFpbmVyID8gcHJldi5fY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoUVVFUllfTkFWX0ZPT1RFUikhIGFzIEhUTUxFbGVtZW50IDogdW5kZWZpbmVkXG5cbiAgICByZXR1cm4gcHJldlxuICB9XG5cbiAgaXNIYW1idXJnZXIoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2xpbmsgPT09IHRoaXMuX25hdmlnYXRpb24uX2hhbWJ1cmdlckVsZW1lbnRcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5pdCgpIHtcbiAgc2VhcmNoQW5kSW5pdGlhbGl6ZShcIi5uYXZcIiwgKGUpID0+IHtcbiAgICBuZXcgTmF2aWdhdGlvbihlKVxuICB9KVxufVxuXG5leHBvcnQgZGVmYXVsdCBOYXZpZ2F0aW9uXG4iXSwic291cmNlUm9vdCI6Ii4uLy4uLy4uLy4uLy4uIn0=
