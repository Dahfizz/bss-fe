# [SDX - Swisscom Digital Experience](https://sdxcode.swisscom.ch)

The SDX Library is a custom framework for building responsive, mobile-first sites and applications tailored to the Swisscom CI/CD. Inside you’ll find high quality HTML, CSS, and JavaScript to make starting any project easier than ever.

URL: [sdxcode.swisscom.ch](https://sdxcode.swisscom.ch)

## Table of Contents

* [Development](#development)
* [Versioning](#versioning)

### What's Included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
sdx/
├── css/
│   ├── sdx.css
├── js/
└── fonts/
```

To install it via NPM run the following command in your project directory:

```sh
npm install git+ssh://git@git.swisscom.ch:7999/sdx/sdx.git --save
```

## Supported Browsers

#### Desktop
Chrome 50+, Firefox 45+, Safari 7+, IE 10+, Edge 25+

#### Mobile
Mobile Safari 7+, Chrome for Android 50+

## Development

Make sure you have the following prerequisites installed:
- [NodeJS 4.5+](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)
- [Ruby](https://www.ruby-lang.org/en/)

- Install `sass` and `hologram` gems
```sh
$ gem install sass hologram
```

We use gulp as the build tool, to start a local server just run the default task:
```sh
$ yarn install
$ yarn run gulp
$ open http://localhost:3000
```

## Continuous Integration and Deployment

Every commit to the SDX Library repository is automatically built by our CI system. Commits and merges to `master` and `develop` branches are automatically deployed to the respective staging servers. The following table shows the different branches and their deployment targets:

| Branch                | Target                                                 | Description                                                    |
| --------------------- | ------------------------------------------------------ | -------------------------------------------------------------- |
| `master`              | https://sdxcode.swisscom.ch                            | The latest released version of the library.                    |
| `develop`             | https://sdx-styleguide-develop.scapp-corp.swisscom.com | The latest version of the development branch, may be unstable. |
| `release/*`           | https://sdx-styleguide-preview.scapp-corp.swisscom.com | Preview of the next unreleased version, may be unstable.                  |
| `feature/*`           | https://sdx-styleguide-preview.scapp-corp.swisscom.com | Preview of the latest feature, may be unstable.                |

## Versioning

For transparency into our release cycle and in striving to maintain backward compatibility, the SDX Library is maintained under [the Semantic Versioning guidelines](http://semver.org/).
