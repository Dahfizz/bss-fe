import { searchAndInitialize, remove } from "../Utils"
import DomElement from "../DomElement"

const CLASS_HAS_VALUE = "is-fixed"
const CLASS_MESSAGE = ".message"

/**
 * Input field component
 */
class InputField extends DomElement {

  constructor (element) {
    super(element)

    this._changedHandler = this.onValueChanged.bind(this)
    this._animationStartHandler = this._onAnimationStart.bind(this)
    this._initialize()
  }

  /**
   * Initializes the input field component.
   * @private
   */
  _initialize() {
    this.element.addEventListener("input", this._changedHandler)

    if (this.element.getAttribute("type") === "password") {
      this.element.addEventListener("animationstart", this._animationStartHandler)
    }

    this.onValueChanged()
  }

  _onAnimationStart(e) {
    if (e.animationName === "onAutoFillStart") {
      this.onValueChanged(true)
    }
  }

  /**
   * Notifies the input field component that it's value has been changed.
   */
  onValueChanged(force = false) {
    if (this.element.value && this.element.value !== "" || force === true) {
      this.addClass(CLASS_HAS_VALUE)
    } else {
      this.removeClass(CLASS_HAS_VALUE)
      this.element.value = ""
    }
  }

  /**
   * Destroys the component and frees all references.
   */
  destroy() {
    this.element.removeEventListener("input", this._changedHandler)

    if (this.element.getAttribute("type") === "password") {
      this.element.removeEventListener("animationstart", this._animationStartHandler)
    }

    this._changedHandler = undefined
    this._animationStartHandler = undefined
  }

  /**
   * Displays the specified error text underneath the input field.
   * @param {text} text The error text/html to display; or undefined to hide the message.
   */
  showError(text) {
    let message
    if (this.element.parentElement) {
      let msg = this.element.parentElement.querySelector(CLASS_MESSAGE)

      if (msg) {
        message = new DomElement(msg)
      }
    }

    if (!text || text === "") {
      if (message) {
        remove(message.element)
      }

      this.removeClass("invalid")
      return
    }

    this.addClass("invalid")

    if (!message) {
      message = new DomElement("div")
        .addClass("message")

      this.element.parentElement.appendChild(message.element)
    } else {
      message.empty()
    }

    const icon = new DomElement("i")
      .addClass("icon")
      .addClass("icon-026-exclamation-mark-circle")
      .setAttribute("aria-hidden", true)

    const msg = new DomElement("span")
      .setHtml(text)

    message.appendChild(icon)
    message.appendChild(msg)
  }
}

export function init() {
  searchAndInitialize(".input-field input", (e) => {
    new InputField(e)
  }, (e) => e.parentElement)
}

export default InputField
