import { searchAndInitialize, preventDefault, find, remove, msIEVersion } from "../Utils"
import DomElement from "../DomElement"
import * as Inputs from "../Inputs"
import * as Dom from "../DomFunctions"

const CLASS_PLACEHOLDER = "select__placeholder"
const CLASS_THUMB = "select__thumb"
const CLASS_BUTTON = "select__button"
const CLASS_DROPDOWN = "select__dropdown"

const CLASS_OPEN = "select--open"
const CLASS_CLOSED = "select--closed"
const CLASS_DISABLED = "select--disabled"

const CLASS_ITEM = "dropdown-item"
const CLASS_ITEM_SELECTED = "dropdown-item--selected"
const CLASS_ITEM_FOCUSED = "dropdown-item--focused"
const CLASS_ITEM_DISABLED = "dropdown-item--disabled"

const CLASS_GROUP_ITEM = "dropdown-group"
const CLASS_GROUP_HEADER = "dropdown-group__item"

const QUERY_MESSAGE = ".message"

const TIMEOUT_CLOSE = 150
const TIMEOUT_BLUR = 400

/**
 * The select component API.
 */
class Select extends DomElement {

  constructor(element) {
    super(element)

    this._openByFocus = false

    // Check for multi-selection
    this._multiselection = this.element.hasAttribute("multiple") === true

    // Setup event context
    this._clickHandler = this._handleClick.bind(this)
    this._handleDropdownClick = this._handleClick.bind(this)
    this._keydownHandler = this._handleKeydown.bind(this)
    this._focusHandler = this._handleFocus.bind(this)
    this._blurHandler = this._handleBlur.bind(this)
    this._windowClickHandler = this._handleWindowClick.bind(this)

    this._initialize()
  }

  /**
   * Initializes the select component.
   *
   * This method inspects the select definition and its options and
   * generates new stylable DOM elements around the original select-element
   * definitions.
   * @private
   */
  _initialize() {
    this._wrapperElement = new DomElement(this.element.parentNode)
      .addClass(CLASS_CLOSED)

    for (let cls of this.classes) {
      this._wrapperElement.addClass(cls)
    }

    this._dropdownElement = new DomElement("div")
      .addClass(CLASS_DROPDOWN)

    if (msIEVersion() > 0 && msIEVersion() < 12) {
      // This is a workaround for IE browsers 11 and earlier where focusing
      // a scrollable dropdown list will close the dropdown prematurely.
      this._dropdownElement.element.addEventListener("mousedown", (event) => event.preventDefault())
    }

    this._setupTarget()
    this._setupPlaceholder()

    this._wrapperElement.appendChild(this._dropdownElement)

    this._createOptions(this.element)

    this._updateSize()
    this._updateMessage()

    if (this.element.disabled) {
      this.disable()
    } else {
      this.enable()
    }
  }

  _setupTarget() {
    // move the id from the select element to the wrapper
    const id = this.element.getAttribute("id")
    if (id) {
      this.element.removeAttribute("id")
      this._wrapperElement.setAttribute("id", id)
    }

    // Apply the tab index
    const tabIndex = this.element.getAttribute("tabindex")
    if (tabIndex) {
      this._wrapperElement.setAttribute("tabIndex", tabIndex)
    }
  }

  _setupPlaceholder() {
    if (!this._selectButtonElement) {
      this._selectButtonElement = new DomElement("div")
        .addClass(CLASS_BUTTON)

      this._wrapperElement.appendChild(this._selectButtonElement)
    }

    if (!this._thumbElement) {
      this._thumbElement = new DomElement("div")
        .addClass(CLASS_THUMB)

      let thumbIcon = new DomElement("div")
        .addClass("thumb-icon")

      this._thumbElement.appendChild(thumbIcon)
      this._selectButtonElement.appendChild(this._thumbElement)
    }

    let placeholderText = ""

    this._placeholderOption = this.element.querySelector("option[value='']")

    if (this._placeholderOption) {
      placeholderText = Dom.text(this._placeholderOption)

      if (this._multiselection === true) {
        this._placeholderOption.selected = false
      }
    }

    let selectedOption = this.element.querySelector("option[selected]")
    if (selectedOption) {
      placeholderText = Dom.text(selectedOption)
    }

    if (!this._placeholderElement) {
      this._placeholderElement = new DomElement("span")
        .addClass(CLASS_PLACEHOLDER)

      this._selectButtonElement.appendChild(this._placeholderElement)
    }

    this._placeholderElement
      .setHtml(placeholderText)

    this._placeholderText = placeholderText

    if (selectedOption !== this._placeholderOption) {
      this._updatePlaceholder(true)
    }
  }

  _updateMessage() {
    const messageNode = this._wrapperElement.element.querySelector(QUERY_MESSAGE)
    if (messageNode !== null) {
      this._wrapperElement.appendChild(new DomElement(messageNode))
    }
  }

  _createOptions(element) {
    for (let i = 0; i < element.childNodes.length; i++) {
      let child = element.childNodes[i]

      if (child.tagName === "OPTGROUP") {
        this._appendGroup(child)
      }

      if (child.tagName === "OPTION") {
        let option = this._createOption(child)

        if (option) {
          this._dropdownElement.appendChild(option)
        }
      }
    }
  }

  _createOption(option) {
    let opt = new DomElement("div")
      .addClass(CLASS_ITEM)
      .setHtml(option.innerHTML)

    if (option.selected) {
      opt.addClass(CLASS_ITEM_SELECTED)
    }

    if (option.disabled) {
      opt.addClass(CLASS_ITEM_DISABLED)
    }

    if (option.value) {
      opt.setAttribute("data-value", option.value)
      return opt
    }

    return undefined
  }

  _appendGroup(optgroup) {
    let label = optgroup.getAttribute("label")

    let group = new DomElement("div")
      .addClass(CLASS_GROUP_ITEM)

    let groupHeader = new DomElement("div")
      .addClass(CLASS_GROUP_HEADER)
      .setHtml(label)

    group.appendChild(groupHeader)

    let options = optgroup.querySelectorAll("option")
    for (let entry of options) {
      let option = this._createOption(entry)
      if (option) {
        group.appendChild(option)
      }
    }

    this._dropdownElement.appendChild(group)
    return group
  }

  _updateSize() {
    // Note: Mirroring the DOM and measuring the items using their clientWidth was very
    // unreliable, therefore measuring was switched to the new HTML5 measureText method
    // margins and paddings arround the text are copied from the original placeholder items
    // dimension
    const placeholderStyle = window.getComputedStyle(this._placeholderElement.element, null)

    let paddingRight = parseFloat(placeholderStyle.paddingRight)
    let paddingLeft = parseFloat(placeholderStyle.paddingLeft)

    let font = this._placeholderElement.css("font")
    let textWidth = Dom.textWidth(this._placeholderText, font)
    let maxWidth = paddingLeft + paddingRight + textWidth

    let options = this._wrapperElement.element.querySelectorAll(`.${CLASS_ITEM}`)
    for (let entry of options) {
      let width = Dom.textWidth(Dom.text(entry), font) + paddingLeft + paddingRight

      if (width > maxWidth) {
        maxWidth = width
      }
    }

    this._dropdownElement.setAttribute("style", `min-width: ${Math.round(maxWidth + 4)}px;`)
  }

  _isButtonTarget(target) {
    return (target === this._wrapperElement.element ||
      target === this._placeholderElement.element ||
      target === this._selectButtonElement.element ||
      target === this._thumbElement.element)
  }

  _isDropdownTarget(target) {
    let current = target
    while (current !== this._dropdownElement.element && current.parentNode) {
      current = current.parentNode
    }

    return current === this._dropdownElement.element
  }

  /**
   * Updates the UI if the selection has changed and makes sure the
   * select control and the generated markup are synchronized.
   * @private
   */
  _selectedItemChanged(oldItems, newItem, autoClose = true, multiselect = false) {
    if (!newItem) {
      setTimeout(() => this.close(), TIMEOUT_CLOSE)
      return
    }

    if (Dom.hasClass(newItem, CLASS_ITEM_DISABLED)) {
      return
    }

    if ((oldItems.length === 0) && !newItem) {
      throw new Error("Can not select undefined elements")
    }

    let oldItem = oldItems[0]
    if (multiselect === true) {
      oldItem = find(oldItems, (x) => x.getAttribute("data-value") === newItem.getAttribute("data-value"))
    }

    if (newItem && oldItem && oldItem === newItem) {
      // Click on a previously selected element -> deselect
      newItem = undefined

      if (!this._placeholderOption && !multiselect) {
        // If there is no placeholder option, non multiselect options cannot be deselected
        return
      }
    }

    if (oldItem) {
      // Remove selection on the element
      let oldValue = oldItem.getAttribute("data-value") || -1
      let optElement = find(this.element.options, (x) => x.value === oldValue)

      if (!optElement) {
        throw new Error(`The option with value ${oldValue} does not exist`)
      }

      optElement.selected = false
      Dom.removeClass(oldItem, CLASS_ITEM_SELECTED)
    }

    if (newItem) {
      // Select a new item
      let newValue = newItem.getAttribute("data-value") || -1
      let optElement = find(this.element.options, (x) => x.value === newValue)

      if (!optElement) {
        throw new Error(`The option with value ${newValue} does not exist`)
      }

      optElement.selected = true
      Dom.addClass(newItem, CLASS_ITEM_SELECTED)
    }

    let hasSelectedItems = true
    if (this._multiselection === false && !newItem) {
      // Handle no selection for non multiselect states
      this._placeholderOption.selected = true
      hasSelectedItems = false
    }

    if (this._multiselection === true && this._getSelectedOptions().length === 0) {
      hasSelectedItems = false
    }

    this._updatePlaceholder(hasSelectedItems)

    // Dispatch the changed event
    this.dispatchEvent("change")

    if (autoClose && !multiselect) {
      setTimeout(() => {
        this.close()
      }, TIMEOUT_CLOSE)
    }
  }

  _updatePlaceholder(hasSelectedItems) {
    let text = this._placeholderOption ? Dom.text(this._placeholderOption) : " "

    if (hasSelectedItems === true) {
      let selectedItems = this._getSelectedOptions()

      if (selectedItems.length > 0) {
        text = ""
        for (let item of selectedItems) {
          text += `${Dom.text(item)}, `
        }
      }

      text = text.substring(0, text.length - 2)
    }

    // Update the placeholder text
    if (this._placeholderElement) {
      this._placeholderElement.setHtml(text)
    }
  }

  _getSelectedOptions() {
    let selectedOptions = []
    if (this.element.options) {
      [].forEach.call(this.element.options, ((option) => {
        if (option.selected) {
          selectedOptions.push(option)
        }
      }))
    }
    return selectedOptions
  }

  _handleFocus() {
    this.open()
    this._openByFocus = true

    setTimeout(() => {
      this._openByFocus = false
    }, TIMEOUT_BLUR)
  }

  _handleBlur() {
    this.close()
  }

  _handleClick(event) {
    let handled = false

    if (this._lastHandledEvent === event) {
      this._lastHandledEvent = undefined
      return
    }

    if (this._isButtonTarget(event.target) && this._openByFocus === false) {
      // handle header item clicks and toggle dropdown
      this.toggle()
      handled = true
    }

    let newItem = event.target

    if (!handled && Dom.hasClass(newItem, CLASS_ITEM)) {
      // handle clicks on dropdown items
      let oldItems = Array(...this._dropdownElement.element.querySelectorAll(`.${CLASS_ITEM_SELECTED}`))
      this._selectedItemChanged(oldItems, newItem, true, this._multiselection)
      handled = true
    }

    if (handled) {
      this._lastHandledEvent = event
      preventDefault(event)
    }
  }

  _handleWindowClick(event) {
    if (this._isDropdownTarget(event.target) || this._isButtonTarget(event.target)) {
      return
    }

    this.close()
  }

  _handleKeydown(event) {
    let evt = event || window.event
    let keycode = event.which || event.keyCode

    if (keycode === Inputs.KEY_ESCAPE) {
      // handle Escape key (ESC)
      if (this.isOpen()) {
        this.close()
      }
      evt.preventDefault()
      return
    }

    if (keycode === Inputs.KEY_ARROW_UP || keycode === Inputs.KEY_ARROW_DOWN) {
      // Up and down arrows

      let options = this._wrapperElement.element.querySelectorAll(`.${CLASS_ITEM}`)
      if (options.length > 0) {

        let newIndex = 0
        let oldOption

        let focusedElement = this._wrapperElement.find(`.${CLASS_ITEM_FOCUSED}`)
        let searchFor = focusedElement ? CLASS_ITEM_FOCUSED : CLASS_ITEM_SELECTED

        let newElement

        for (let index = 0; index < options.length; index++) {
          let direction = keycode === Inputs.KEY_ARROW_DOWN ? 1 : -1

          let item = new DomElement(options[index])

          // search for selected or focusedElement elements
          if (item.hasClass(searchFor)) {
            oldOption = item
            newIndex = index

            // get the next not disabled element in the apropriate direction
            for (let count = 0; count < options.length; count++) {
              newIndex += direction
              newIndex %= options.length

              if (newIndex < 0) {
                newIndex = options.length - 1
              }

              newElement = new DomElement(options[newIndex])
              if (!newElement.hasClass(CLASS_ITEM_DISABLED)) {
                break
              }
            }
          }
        }

        // set the new element focused
        let newOption = new DomElement(options[newIndex])
        newOption.addClass(CLASS_ITEM_FOCUSED)

        if (oldOption) {
          oldOption.removeClass(CLASS_ITEM_FOCUSED)
        }
      }

      evt.preventDefault()
      return
    }

    if (keycode === Inputs.KEY_ENTER || keycode === Inputs.KEY_TAB) {
      // Handle enter and tab key by selecting the currently focused element
      let newItem = this._dropdownElement.element.querySelector(`.${CLASS_ITEM_FOCUSED}`)
      let oldItems = Array(...this._dropdownElement.element.querySelectorAll(`.${CLASS_ITEM_SELECTED}`))
      this._selectedItemChanged(oldItems, newItem, true, this._multiselection)
    }
  }

  /**
   * Gets the value of the currently selected option.
   * If multiple selection is enabled this property returns an array of values.
   */
  get value() {
    if (this._multiselection) {
      return Array(...this._getSelectedOptions()).map((x) => x.value)
    }

    if (this.element.value === "") {
      return null
    }

    return this.element.value
  }

  /**
   * Enables or disables the select component depending on the
   * 'value' parameter.
   * @param {value} If true disables the control; false enables it.
   */
  set disabled(value) {
    if (value) {
      this.disable()
    } else {
      this.enable()
    }
  }

  /**
   * Registers an event listener on the select control.
   */
  addEventListener(type, listener) {
    this.element.addEventListener(type, listener)
  }

  /**
   * Reloads the dropdown's option data definitions from the DOM and updates
   * the generated dropdown display items.
   */
  reload() {
    // Remove all existing child elements
    while (this._dropdownElement.element.firstChild) {
      this._dropdownElement.element.removeChild(this._dropdownElement.element.firstChild)
    }

    this._setupPlaceholder()
    this._createOptions(this.element)

    this._updateSize()
    this._updateMessage()
  }

  /**
   * Sets the select control to the enabled state.
   */
  enable() {
    this.element.removeAttribute("disabled")
    this._wrapperElement.removeClass(CLASS_DISABLED)

    window.addEventListener("click", this._windowClickHandler)

    this._wrapperElement.element.addEventListener("click", this._clickHandler)
    this._wrapperElement.element.addEventListener("keydown", this._keydownHandler)
    this._wrapperElement.element.addEventListener("focus", this._focusHandler)
    this._wrapperElement.element.addEventListener("blur", this._blurHandler)
  }

  /**
   * Sets the select control to the disabled state.
   */
  disable() {
    this.element.setAttribute("disabled", "")
    this._wrapperElement.addClass(CLASS_DISABLED)

    window.removeEventListener("click", this._windowClickHandler)

    this._wrapperElement.element.removeEventListener("click", this._clickHandler)
    this._wrapperElement.element.removeEventListener("keydown", this._keydownHandler)
    this._wrapperElement.element.removeEventListener("focus", this._focusHandler)
    this._wrapperElement.element.removeEventListener("blur", this._blurHandler)

    this.close()
  }

  /**
   * Toggles the open/closed state of the select dropdown.
   */
  toggle() {
    if (this.isOpen()) {
      this.close()
    } else {
      this.open()
    }
  }

  /**
   * Gets if the select dropdown is open or closed.
   * @return {boolean} True if open; otherwise false.
   */
  isOpen() {
    return this._wrapperElement.hasClass(CLASS_OPEN)
  }

  /**
   * Opens the select dropdown.
   */
  open() {
    if (!this.isOpen()) {
      this._openByFocus = false

      this._wrapperElement.removeClass(CLASS_CLOSED)
      this._wrapperElement.addClass(CLASS_OPEN)

      this._dropdownElement.element.addEventListener("click", this._handleDropdownClick)
      this._dropdownElement.element.addEventListener("tap", this._handleDropdownClick)
    }
  }

  /**
   * Closes the select dropdown.
   */
  close() {
    if (this.isOpen()) {
      this._openByFocus = false

      this._wrapperElement.removeClass(CLASS_OPEN)
      this._wrapperElement.addClass(CLASS_CLOSED)

      this._dropdownElement.element.removeEventListener("click", this._handleDropdownClick)
      this._dropdownElement.element.removeEventListener("tap", this._handleDropdownClick)

      let focusedItem = this._wrapperElement.find(`.${CLASS_ITEM_FOCUSED}`)
      if (focusedItem) {
        focusedItem.removeClass(CLASS_ITEM_FOCUSED)
      }
    }
  }

  /**
   * Destroys the component and clears all references.
   */
  destroy() {
    window.removeEventListener("click", this._windowClickHandler)

    if (this._dropdownElement) {
      this._dropdownElement.element.removeEventListener("click", this._handleDropdownClick)
      this._dropdownElement.element.removeEventListener("tap", this._handleDropdownClick)

      remove(this._dropdownElement.element)
      this._dropdownElement = undefined
    }

    if (this._wrapperElement) {
      this._wrapperElement.element.removeEventListener("click", this._clickHandler)
      this._wrapperElement.element.removeEventListener("keydown", this._keydownHandler)
      this._wrapperElement.element.removeEventListener("focus", this._focusHandler)
      this._wrapperElement.element.removeEventListener("blur", this._blurHandler)

      this._wrapperElement = undefined
    }

    if (this._selectButtonElement) {
      remove(this._selectButtonElement.element)
      this._selectButtonElement = undefined
    }

    this.removeClass(CLASS_CLOSED)
  }
}

export function init() {
  searchAndInitialize("select", (e) => {
    new Select(e)
  })
}

export default Select
