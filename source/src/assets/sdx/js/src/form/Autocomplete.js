import { searchAndInitialize, preventDefault } from "../Utils"
import { empty, addClass, removeClass, hasClass } from "../DomFunctions"
import DomElement from "../DomElement"
import * as Inputs from "../Inputs"

const QUERY_DROPDOWN = ".js-autocomplete"
const CLASS_RESULT = "autocomplete__result"
const CLASS_OPEN = "is-open"
const CLASS_HOVER = "js-hover"
const ATTRIBUTE_VALUE = "data-value"

const TIMEOUT_BLUR = 400

/**
 * Autocomplete component
 * @fires Autocomplete#change
 */
class Autocomplete extends DomElement {

  constructor(element, configuration) {
    super(element)

    this._input = this.element.querySelector("input")
    this._dropdown = this.element.querySelector(QUERY_DROPDOWN)

    // Setup event context
    this._clickHandler = this._handleClick.bind(this)
    this._windowClickHandler = this._handleWindowClick.bind(this)
    this._keyUpHandler = this._handleKeyUp.bind(this)
    this._keyDownHandler = this._handleKeyDown.bind(this)
    this._blurHandler = this._handleBlur.bind(this)

    if (configuration) {
      this._minChars = configuration.minChars
      this._source = configuration.source
    }

    if (!this._minChars || this._minChars < 0) {
      this._minChars = 2
    }

    this._initialize()
  }

  /**
   * Initializes the Autocomplete component.
   * @private
   */
  _initialize() {
    this._clearSuggestions()

    if (this._input.getAttribute("disabled")) {
      this.disable()
    } else {
      this.enable()
    }

    // Disable browser autofill
    this._input.setAttribute("autocomplete", "off")
  }

  /**
   * The Autocomplete component configuration object
   * @callback Autocomplete~Suggest
   * @property {String} term - The current search term.
   * @property {String[]} matches - The list of matching strings.
   */

  /**
   * The Autocomplete component configuration object
   * @callback Autocomplete~Source
   * @property {String} term - The current search term.
   * @property {Autocomplete~Suggest} suggest - The autocomplete callback function to report the results.
   */

  /**
   * The Autocomplete component configuration object
   * @typedef {Object} Autocomplete~Config
   * @property {Number} minChars - The minimal required characters to start querying for autocomplete matches.
   * @property {Autocomplete~Source} source - The autocomplete source function.
   */

  /**
   * Updates the autocomplete component configuration for the current instance
   * @param {Autocomplete~Config} configuration The configuration object
   */
  configure(configuration) {
    if (!configuration) {
      return
    }

    if (configuration.minChars) {
      this._minChars = Math.min(configuration.minChars, 1)
    }

    if (configuration.source) {
      this._source = configuration.source
    }

    this._clearSuggestions()
  }

  /**
   * Sets the select control to the enabled state.
   */
  enable() {
    if (!this._input) {
      return
    }

    this._input.removeAttribute("disabled")

    this._input.addEventListener("keyup", this._keyUpHandler)
    this._input.addEventListener("keydown", this._keyDownHandler)
    this._input.addEventListener("blur", this._blurHandler)
  }

  /**
   * Sets the select control to the disabled state.
   */
  disable() {
    if (!this._input) {
      return
    }

    this._input.setAttribute("disabled", true)

    this._input.removeEventListener("keyup", this._keyUpHandler)
    this._input.removeEventListener("keydown", this._keyDownHandler)
    this._input.removeEventListener("blur", this._blurHandler)

    this.close()
  }

  /**
   * Destroys the component and frees all references.
   */
  destroy() {
    this.disable()

    this._keyUpHandler = undefined
    this._keyDownHandler = undefined
    this._windowClickHandler = undefined
    this._blurHandler = undefined

    this._input = undefined
  }

  /**
   * Closes the suggestions dropdown.
   */
  open() {
    this._dropdown.addEventListener("click", this._clickHandler)
    window.addEventListener("click", this._windowClickHandler)

    this.addClass(CLASS_OPEN)
  }

  /**
   * Opens the suggestions dropdown.
   */
  close() {
    this._dropdown.removeEventListener("click", this._clickHandler)
    window.removeEventListener("click", this._windowClickHandler)

    this.removeClass(CLASS_OPEN)
  }

  /**
   * Registers an event listener on the component.
   */
  addEventListener(type, listener) {
    this.element.addEventListener(type, listener)
  }

  /**
   * Unregisters an event listener on the component.
   */
  removeEventListener(type, listener) {
    this.element.removeEventListener(type, listener)
  }

  /**
   * Gets the value of the input field.
   * @returns {String} The value of the input field.
   */
  get value() {
    return this._input.value
  }

  _handleClick(event) {
    if (!this._isDropdownTarget(event.target)) {
      return
    }

    let current = event.target
    while (current.nodeName !== "LI" && current.parentNode) {
      current = current.parentNode
    }

    if (current.nodeName === "LI") {
      preventDefault(event)
      this._selectItem(current)
    }
  }

  _handleBlur() {
    setTimeout(() => {
      this.close()
    }, TIMEOUT_BLUR)
  }

  _handleKeyUp(event) {
    let evt = event || window.event
    let keycode = event.which || event.keyCode

    if (Inputs.containsKey(keycode, [Inputs.KEY_ARROW_UP, Inputs.KEY_ARROW_DOWN, Inputs.KEY_ENTER, Inputs.KEY_TAB])) {
      // Do not handle these events on keyup
      preventDefault(evt)
      return
    }

    if (evt.currentTarget && evt.currentTarget.value && evt.currentTarget.value.length >= this._minChars) {
      this._getSuggestion(evt.currentTarget.value)
    } else {
      this.close()
    }
  }

  _handleKeyDown(event) {

    let evt = event || window.event
    let keycode = event.which || event.keyCode
    const isOpen = hasClass(this.element, CLASS_OPEN)

    if (keycode === Inputs.KEY_ESCAPE && isOpen === true) {
      // handle Escape key (ESC)
      this.close()
      preventDefault(evt)
      return
    }

    if (isOpen === true && Inputs.containsKey(keycode, [Inputs.KEY_ENTER, Inputs.KEY_TAB])) {
      let focusedElement = this._suggestionList.querySelector(`.${CLASS_HOVER}`)

      preventDefault(evt)
      this._selectItem(focusedElement)
      return
    }

    if (isOpen === true && Inputs.containsKey(keycode, [Inputs.KEY_ARROW_UP, Inputs.KEY_ARROW_DOWN])) {
      // Up and down arrows

      let focusedElement = this._suggestionList.querySelector(`.${CLASS_HOVER}`)
      if (focusedElement) {
        removeClass(focusedElement, CLASS_HOVER)

        const children = new Array(...this._suggestionList.childNodes)

        const totalNodes = children.length - 1
        const direction = keycode === Inputs.KEY_ARROW_UP ? -1 : 1

        let index = children.indexOf(focusedElement)

        index = Math.max(Math.min(index + direction, totalNodes), 0)
        focusedElement = this._suggestionList.childNodes[index]

      } else {
        focusedElement = this._suggestionList.querySelector("li")
      }

      addClass(focusedElement, CLASS_HOVER)
      preventDefault(evt)
      return
    }
  }

  _handleWindowClick(event) {
    if (this._isDropdownTarget(event.target)) {
      return
    }

    this.close()
  }

  _selectItem(item) {
    if (!item) {
      return
    }

    const text = item.getAttribute(ATTRIBUTE_VALUE)
    if (text) {
      this._input.value = text

      // Dispatch the changed event
      this.dispatchEvent("change")
    }

    this.close()
  }

  _isDropdownTarget(target) {
    let current = target
    while (current !== this._dropdown && current.parentNode) {
      current = current.parentNode
    }

    return current === this._dropdown
  }

  _clearSuggestions() {
    // Clear the dropdown item
    empty(this._dropdown)

    this._suggestionList = document.createElement("ul")
    this._dropdown.appendChild(this._suggestionList)
  }

  _addSuggestion(text, term) {
    const sanitizedTerm = term.replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&")
    const html = text.replace(new RegExp(`(${sanitizedTerm})`, "gi"), "<strong>$1</strong>")

    const textElement = new DomElement("span")
      .setHtml(html)

    const innerElement = new DomElement("div")
      .addClass(CLASS_RESULT)
      .appendChild(textElement)

    const liElement = new DomElement("li")
      .setAttribute(ATTRIBUTE_VALUE, text)
      .appendChild(innerElement)

    this._suggestionList.appendChild(liElement.element)
  }

  _getSuggestion(term) {
    if (!this._source) {
      throw new Error("The source function is undefined, cannot load suggestions")
    }

    this._source(term, (matches, termused) => {
      this._onMatchesReceived(matches, termused)
    })
  }

  _onMatchesReceived(matches, term) {
    this._clearSuggestions()

    if (!matches || matches.length === 0) {
      this.close()
    } else {
      // Clear the dropdown item
      empty(this._suggestionList)

      for (let match of matches) {
        this._addSuggestion(match, term)
      }

      this.open()
    }
  }
}

/**
 * Change event
 *
 * @event Autocomplete#change
 * @type {object}
 */

export function init() {
  searchAndInitialize(".input-field--autocomplete", (e) => {
    new Autocomplete(e)
  })
}

export default Autocomplete
