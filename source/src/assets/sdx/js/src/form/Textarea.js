import { searchAndInitialize } from "../Utils"
import DomElement from "../DomElement"
import * as Dom from "../DomFunctions"

const QUERY_TEXTAREA = "textarea"
const CLASS_HAS_VALUE = "is-fixed"

/**
 * Textarea component
 */
class Textarea extends DomElement {

  constructor (element) {
    super(element)

    this._area = this.element.querySelector(QUERY_TEXTAREA)

    this._focusChangedHandler = this._focusChanged.bind(this)
    this._valueChangedHandler = this._onValueChanged.bind(this)
    this._resizeHandler = this._updateHeight.bind(this)

    this._initialize()
  }

  /**
   * Initializes the textarea component.
   * @private
   */
  _initialize() {
    this._minRows = this._area.getAttribute("data-min-rows") || 3
    this._maxRows = this._area.getAttribute("data-max-rows") || Number.MAX_SAFE_INTEGER

    // Make sure min an max are property specified
    this._minRows = Math.min(this._minRows, this._maxRows)
    this._maxRows = Math.max(this._minRows, this._maxRows)

    this._lineHeight = parseInt(this._css(this._area, "line-height"), 10)

    this._updateBaseHeight = Dom.isHidden(this._area, true)
    this._calculateBaseHeight()

    // add event listeners
    this._area.addEventListener("focus", this._focusChangedHandler)
    this._area.addEventListener("blur", this._focusChangedHandler)
    this._area.addEventListener("input", this._valueChangedHandler)

    window.addEventListener("resize", this._resizeHandler)
    window.addEventListener("orientationchange", this._resizeHandler)

    this._onValueChanged()
  }

  _calculateBaseHeight() {
    // temporary clear the content to take measurements
    let value = this._area.value
    this._area.value = ""

    this._baseHeight = this._area.offsetHeight - this._lineHeight
    this._baseScrollHeight = this._area.scrollHeight - this._lineHeight

    // restore initial content
    this._area.value = value
  }

  _focusChanged() {
    this._updateHeight()
  }

  _css(element, property) {
    return window.getComputedStyle(element, null).getPropertyValue(property)
  }

  _updateHeight() {
    let hasFocus = this._area === document.activeElement
    let maxRows, rows = 0

    if (this._updateBaseHeight === true && Dom.isHidden(this._area, true) === false) {
      this._calculateBaseHeight()
      this._updateBaseHeight = false
    }

    // Calculate the apropriate size for the control
    if (!this._hasValue()) {
      // Handle empty states
      rows = hasFocus === true ? this._minRows : 1
      maxRows = rows
    } else {
      // Reset the height for calculation of the row count
      this._area.style.height = "auto"

      // Get the new height
      rows = Math.ceil((this._area.scrollHeight - this._baseScrollHeight) / this._lineHeight) + 1
      maxRows = Math.max(Math.min(this._maxRows, rows), this._minRows)
    }

    if (rows > this._maxRows) {
      this._area.style.overflow = "auto"
    } else {
      this._area.style.overflow = "hidden"
    }

    const height = ((maxRows - 1)  * this._lineHeight) + this._baseHeight
    this._area.style.height = `${height}px`
  }

  _hasValue() {
    return this._area.value && this._area.value.length > 0
  }

  _onValueChanged() {
    if (this._hasValue()) {
      Dom.addClass(this._area, CLASS_HAS_VALUE)
    } else {
      Dom.removeClass(this._area, CLASS_HAS_VALUE)
      this._area.value = ""
    }

    this._updateHeight()
  }

  /**
   * Destroys the component and clears all references.
   */
  destroy() {
    window.removeEventListener("resize", this._resizeHandler)
    window.removeEventListener("orientationchange", this._resizeHandler)

    this._area.removeEventListener("focus", this._focusChangedHandler)
    this._area.removeEventListener("blur", this._focusChangedHandler)
    this._area.removeEventListener("input", this._valueChangedHandler)

    this._focusChangedHandler = null
    this._valueChangedHandler = null
    this._area = null
    this._minRows = null
    this._maxRows = null
    this._lineHeight = null
    this._baseHeight = null
    this._baseScrollHeight = null
    this.element = null
  }
}

export function init() {
  searchAndInitialize(".input-multiline", (e) => {
    new Textarea(e)
  })
}

export default Textarea
