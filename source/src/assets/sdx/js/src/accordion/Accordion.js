/* eslint-disable */

import { searchAndInitialize } from '../Utils';
import { TweenLite, Power1, Power4 } from 'gsap';
import DomElement from '../DomElement';
import * as Dom from '../DomFunctions';

const QUERY_TOGGLE = '.accordion__toggle';
const QUERY_OPEN_SECTION = '.accordion__item.is-open';
const QUERY_COLLAPSE = '.accordion__collapse';

const CLASS_ITEM = 'accordion__item';
const CLASS_OPEN = 'is-open';

const REGEX_HIDDEN = /accordion--hidden-.*/;

const ANIMATION_OPEN = 0.3;

const ANIMATION_DELAY_OPEN = 0.2;
const ANIMATION_VISIBLE_OPEN = 0.5;

const ANIMATION_VISIBLE_CLOSE = 0.1;

/**
 * The Accordion component
 */
class Accordion extends DomElement {
  /**
   * Creates and initializes the Accordion component.
   * @param {DomElement} - The root element of the Accordion component.
   */
  constructor(element) {
    super(element);

    this._sectionClickHandler = this._handleSectionClick.bind(this);
    this._initialize();
  }

  /**
   * Initializes the Accordion component.
   * @private
   */
  _initialize() {
    if (this.element.className.split(' ').some(c => REGEX_HIDDEN.test(c))) {
      let indicator = new DomElement('input')
        .setAttribute('type', 'hidden')
        .addClass('js-hidden');

      this.appendChild(indicator);
      this._hiddenIndicator = indicator.element;
    }

    for (let toggle of this.element.querySelectorAll(QUERY_TOGGLE)) {
      toggle.addEventListener('click', this._sectionClickHandler);
    }
  }

  _handleSectionClick(event) {
    if (this._hiddenIndicator) {
      let style = window.getComputedStyle(this._hiddenIndicator, null);

      if (style.visibility !== 'visible') {
        return;
      }
    }

    let navSection = event.target.parentElement;

    while (!Dom.hasClass(navSection, CLASS_ITEM) && navSection.parentElement) {
      navSection = navSection.parentElement;
    }

    let prevSection = this.element.querySelector(QUERY_OPEN_SECTION);

    if (prevSection && prevSection !== navSection) {
      this._toggleSection(prevSection);
    }

    this._toggleSection(navSection);
  }

  _toggleSection(accSection) {
    let collapseElement = accSection.querySelector(QUERY_COLLAPSE);

    if (Dom.hasClass(accSection, CLASS_OPEN)) {
      Dom.removeClass(accSection, CLASS_OPEN);
      this._closeCollapseSection(collapseElement);
    } else {
      Dom.addClass(accSection, CLASS_OPEN);
      this._openCollapseSection(collapseElement);
    }
  }

  _openCollapseSection(el) {
    TweenLite.killTweensOf(el);

    TweenLite.set(el, {
      display: 'block'
    });

    TweenLite.to(el, ANIMATION_OPEN, {
      className: `+=${CLASS_OPEN}`,
      ease: [Power1.easeIn, Power4.easeOut]
    });

    TweenLite.to(el, ANIMATION_VISIBLE_OPEN, {
      autoAlpha: 1,
      delay: ANIMATION_DELAY_OPEN
    });

    // set aria expanded
    el.setAttribute('aria-expanded', true);
  }

  _closeCollapseSection(el) {
    TweenLite.killTweensOf(el);

    TweenLite.to(el, ANIMATION_VISIBLE_CLOSE, {
      autoAlpha: 0
    });

    TweenLite.to(el, ANIMATION_OPEN, {
      className: `-=${CLASS_OPEN}`,
      ease: [Power1.easeIn, Power4.easeOut],
      onComplete: () => {
        TweenLite.set(el, {
          clearProps: 'display, visibility, opacity'
        });
      }
    });

    // set aria expanded
    el.setAttribute('aria-expanded', false);
  }

  /**
   * Removes all event handlers and clears references.
   */
  destroy() {
    for (let toggle of this.element.querySelectorAll(QUERY_TOGGLE)) {
      toggle.removeEventListener('click', this._sectionClickHandler);
    }

    this._sectionClickHandler = null;
    this.element = null;
  }
}

export function init() {
  searchAndInitialize('.accordion', e => {
    new Accordion(e);
  });
}

export default Accordion;
