import { TimelineLite } from "gsap"
import DomElement from "../DomElement"

const CLASS_ITEMS = ".toolbar__item"
const CLASS_SHOW = "item--show"

const ANIMATION_START_DELAY = 0.1
const ANIMATION_OFFSET = 0.05

/**
 * Toolbar component. Use this component to show and hide the
 * individual toolbar items.
 */
class Toolbar extends DomElement {

  /**
   * Makes the toolbar items visible.
   */
  show() {
    let delay = ANIMATION_START_DELAY
    let items = this.element.querySelectorAll(CLASS_ITEMS)

    let timeline = new TimelineLite()
    for (let index = 0; index < items.length; index++) {
      timeline.to(items[index], 0, { className: `+=${CLASS_SHOW}` }, delay)
      delay += ANIMATION_OFFSET
    }
  }

  /**
   * Hides the toolbar items.
   */
  hide() {
    let delay = ANIMATION_START_DELAY
    let items = this.element.querySelectorAll(CLASS_ITEMS)

    let timeline = new TimelineLite()
    for (let index = items.length - 1; index >= 0; index--) {
      timeline.to(items[index], 0, { className: `-=${CLASS_SHOW}` }, delay)
      delay += ANIMATION_OFFSET
    }
  }

  /**
   * Toggles the toolbar items visibility.
   */
  toggle() {
    if (this.element.querySelectorAll(`.${CLASS_SHOW}`).length  === 0) {
      this.show()
    } else {
      this.hide()
    }
  }
}

export default Toolbar
