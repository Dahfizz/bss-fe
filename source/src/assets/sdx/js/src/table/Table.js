import { searchAndInitialize } from "../Utils"
import DomElement from "../DomElement"
import * as Dom from "../DomFunctions"

const QUERY_HEADER = "thead th"

const CLASS_SORTED_ASCENDING = "js-ascending"
const CLASS_SORTED_DESCENDING = "js-descending"
const CLASS_ARROW = "arrow-icon"

/**
 * The Table component. Adds additional capabilities to standard HTML 5 tables.
 */
class Table extends DomElement {

  /**
   * Creates a new instance of the table component.
   */
  constructor(element) {
    super(element)

    this._headerClickHandler = this._handleHeaderClick.bind(this)

    this._body = this.element.querySelector("tbody")
    this._rows = this._body.getElementsByTagName("tr")

    this._initialize()
  }

  _initialize() {
    for (let header of this.element.querySelectorAll(QUERY_HEADER)) {
      if (header.getAttribute("data-type")) {
        header.addEventListener("click", this._headerClickHandler)

        let arrowElement = new DomElement("div")
          .addClass(CLASS_ARROW)
          .element

        header.appendChild(arrowElement)
      }
    }
  }

  _handleHeaderClick(e) {
    const th = e.target
    this.sort(th)
  }

  /**
   * Sorts the table according to the specified table header element.
   * The column is sorted ascending by default if no direction is specified and no
   * existing sort order class is found in the markup.
   *
   * If the displayed data is not suitable for sorting `<td/>` elements can define a `data-value` attribute
   * which is then used for the data-source.
   *
   * @param {TableHeader} tableHeader The header element of the row to sort by.
   * @param {Number} direction The direction to sort, `1` for ascending, `-1` for descending order. This parameter is optional.
   * @param {function} equalityComparer The equiality comparer function to compare individual cell values.
   */
  sort(tableHeader, direction = undefined, equalityComparer = undefined) {
    if (!tableHeader || tableHeader.tagName !== "TH") {
      throw new Error("The parameter 'tableHeader' must be a valid column header node")
    }

    if (direction !== 1 && direction !== -1 && direction) {
      throw new Error(`Parameter out of range, parameter 'direction' with value '${direction}' must be either -1, 1 or undefined`)
    }

    const columnIndex = tableHeader.cellIndex

    if (!equalityComparer) {
      let dataType = tableHeader.getAttribute("data-type")
      equalityComparer = this._getComparer(dataType)
    }

    if (columnIndex >= this.element.querySelectorAll(QUERY_HEADER).length) {
      throw new Error("Column out of range")
    }

    for (let header of this.element.querySelectorAll(QUERY_HEADER)) {
      if (header !== tableHeader) {
        Dom.removeClass(header, CLASS_SORTED_ASCENDING)
        Dom.removeClass(header, CLASS_SORTED_DESCENDING)
      }
    }

    if (Dom.hasClass(tableHeader, CLASS_SORTED_ASCENDING)) {
      Dom.removeClass(tableHeader, CLASS_SORTED_ASCENDING)
      Dom.addClass(tableHeader, CLASS_SORTED_DESCENDING)

      direction = direction || -1
    } else {
      Dom.removeClass(tableHeader, CLASS_SORTED_DESCENDING)
      Dom.addClass(tableHeader, CLASS_SORTED_ASCENDING)
      direction = direction || 1
    }

    this._quicksort(columnIndex, 0, this._rows.length - 1, direction, equalityComparer)
  }

  _getCell(column, row) {
    return this._rows[row].cells[column]
  }

  _getRow(row) {
    return this._rows[row]
  }

  _getComparer(dataType) {
    switch (dataType) {
      case "number": {
        // parse the string as a number
        return (a, b) => parseFloat(a) - parseFloat(b)
      }
      default: {
        // compare strings
        return (a, b) => {
          if (a < b) {
            return -1
          }
          if (a > b) {
            return 1
          }

          return 0
        }
      }
    }
  }

  _quicksort(column, left, right, direction = 1, equalityComparer) {
    if (right - left > 1) {

      let partition = this._partition(column, left, right, direction, equalityComparer)

      if (left < partition - 1) {
        this._quicksort(column, left, partition - 1, direction, equalityComparer)
      }

      if (partition < right) {
        this._quicksort(column, partition, right, direction, equalityComparer)
      }
    }
  }

  _partition(column, left, right, direction, equalityComparer) {
    let pivot = this._getCell(column, Math.floor((right + left) / 2))
    let i = left
    let j = right

    while (i <= j) {
      while (this._equals(this._getCell(column, i), pivot, equalityComparer) * direction < 0) {
        i++
      }

      while (this._equals(this._getCell(column, j), pivot, equalityComparer) * direction > 0) {
        j--
      }

      if (i <= j) {
        this._swap(i, j)
        i++
        j--
      }
    }

    return i
  }

  _equals(a, b, equalityComparer) {
    let dataA = a.getAttribute("data-value")
    let dataB = b.getAttribute("data-value")

    dataA = dataA || a.textContent || a.innerText
    dataB = dataB || b.textContent || b.innerText

    return equalityComparer(dataA, dataB)
  }

  _swap(i, j) {
    let tmpNode = this._body.replaceChild(this._getRow(i), this._getRow(j))
    const referenceRow = this._getRow(i)

    if (!referenceRow) {
      this._body.appendChild(tmpNode)
    } else {
      this._body.insertBefore(tmpNode, referenceRow)
    }
  }

  /**
   * Destroys the component and clears all references.
   */
  destroy() {
    for (let header of this.element.querySelectorAll(QUERY_HEADER)) {
      header.removeEventListener("click", this._headerClickHandler)
    }

    this._headerClickHandler = null
    this._body = null
    this._rows = null
  }
}

export function init() {
  searchAndInitialize("table", (e) => {
    new Table(e)
  })
}

export default Table
