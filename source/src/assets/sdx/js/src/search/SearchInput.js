import { TimelineLite, Power4, Power1 } from "gsap"
import DomElement from "../DomElement"
import * as Inputs from "../Inputs"
import { searchAndInitialize, preventDefault, msIEVersion } from "../Utils"
import { addClass, removeClass, getAttributeReference, hasClass, parentWithClass } from "../DomFunctions"

const QUERY_SEARCH_INPUT = "input.search__field"
const QUERY_BTN_CLOSE = ".search__icon-close"

const QUERY_LIVE_SUGESTIONS = ".js-suggestions"
const QUERY_LIVE_FOOTER = ".js-footer"

const CLASS_ACTIVE = "is-active"
const CLASS_OPEN = "is-open"

const CLASS_SEARCH = "search"

const ANIMATION_SUGGESTIONS_DURATION = 0.3
const ANIMATION_FOOTER_DURATION = 0.1

/**
 * The search input component definition.
 */
class SearchInput extends DomElement {

  constructor(element) {
    super(element)

    this._input = this.element.querySelector(QUERY_SEARCH_INPUT)
    this._form = this.element.querySelector("form")
    this._btnClose = this.element.querySelector(QUERY_BTN_CLOSE)

    let liveSearch = getAttributeReference(this.element, "data-live")
    if (liveSearch) {
      this._liveSuggestions = liveSearch.querySelector(QUERY_LIVE_SUGESTIONS)
      this._liveFooter = liveSearch.querySelector(QUERY_LIVE_FOOTER)

      if (this._liveSuggestions) {
        this._liveContainer = this._liveSuggestions.parentNode
      }
    }

    this._focusHandler = this._handleInputFocus.bind(this)
    this._blurHandler = this._handleInputBlur.bind(this)
    this._closeHandler = this.close.bind(this)
    this._windowClickHandler = this._handleWindowClick.bind(this)
    this._keydownHandler = this._handleKeydown.bind(this)
    this._resizeHandler = this._handleResize.bind(this)

    this._initialize()
  }

  _initialize() {
    this._input.addEventListener("focus", this._focusHandler)
    this._input.addEventListener("blur", this._blurHandler)

    if (msIEVersion() > 0) {
      // This is a workaround for IE browsers where a focused
      // input's cursor bleeds trough even if hidden

      window.addEventListener("resize", this._resizeHandler)
      window.addEventListener("orientationchange", this._resizeHandler)
    }

    if (this._btnClose) {
      this._btnClose.addEventListener("click", this._closeHandler)
    }
  }

  _handleInputFocus() {
    this.addClass(CLASS_ACTIVE)
  }

  _handleInputBlur() {
    this.removeClass(CLASS_ACTIVE)
  }

  _handleWindowClick(event) {
    let target = event.target

    if (!parentWithClass(target, CLASS_SEARCH)) {
      this.close()
      return false
    }

    return true
  }

  _handleKeydown(event) {
    let evt = evt || window.event
    let keycode = event.which || event.keyCode

    if (keycode === Inputs.KEY_ESCAPE) {
      this.close()
      preventDefault(event)
    }
  }

  _handleResize() {
    let style = window.getComputedStyle(this.element, null)
    if (style.display === "none") {
      this._input.blur()
    }
  }

  _resetMainTimeline() {
    if (this._tlMain) {
      this._tlMain.stop()
    }

    this._tlMain = new TimelineLite({
      onComplete: () => {
        this._tlMain = undefined
      }
    })
  }

  /**
   * Gets the search input text content.
   * @returns {String} The input text.
   */
  get value() {
    return this._input.value
  }

  /**
   * Opens/activates the search input.
   */
  open() {
    this.addClass(CLASS_OPEN)
    this._input.focus()

    setTimeout(() => {
      window.addEventListener("click", this._windowClickHandler)
      window.addEventListener("touchend", this._windowClickHandler)
      window.addEventListener("keydown", this._keydownHandler)
    }, 50)
  }

  /**
   * Closes/deactivates the search input.
   */
  close() {
    this._form.reset()
    this.removeClass(CLASS_OPEN)

    this.closeLiveSearch()

    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)
    window.removeEventListener("keydown", this._keydownHandler)
  }

  /**
   * Opens the live search suggestions.
   */
  openLiveSearch() {
    if (!this._liveSuggestions || hasClass(this._liveSuggestions, CLASS_OPEN) === true) {
      return
    }

    if (this._liveContainer) {
      addClass(this._liveContainer, CLASS_OPEN)
    }

    this._resetMainTimeline()

    this._tlMain.set(this._liveSuggestions, {
      display: "block"
    })

    this._tlMain.to(this._liveSuggestions, ANIMATION_SUGGESTIONS_DURATION, {
      className: `+=${CLASS_OPEN}`,
      clearProps: "all",
      ease: [
        Power1.easeIn, Power4.easeOut
      ]
    })

    if (this._liveFooter) {
      this._tlMain.set(this._liveFooter, {
        display: "block"
      }, 0)

      this._tlMain.to(this._liveFooter, ANIMATION_FOOTER_DURATION, {
        className: `+=${CLASS_OPEN}`,
        clearProps: "height, display",
        ease: [
          Power1.easeIn, Power4.easeOut
        ]
      }, "-=0.1")
    }
  }

  /**
   * Closes the live search suggestions.
   */
  closeLiveSearch() {
    if (!this._liveSuggestions || hasClass(this._liveSuggestions, CLASS_OPEN) === false) {
      return
    }

    this._resetMainTimeline()

    this._tlMain.set(this._liveSuggestions, {
      display: "block"
    })

    if (this._liveFooter) {
      this._tlMain.set(this._liveFooter, {
        display: "block"
      }, 0)

      this._tlMain.to(this._liveFooter, ANIMATION_FOOTER_DURATION, {
        className: `-=${CLASS_OPEN}`,
        clearProps: "height, display",
        ease: [
          Power1.easeIn, Power4.easeOut
        ]
      })
    }

    this._tlMain.to(this._liveSuggestions, ANIMATION_SUGGESTIONS_DURATION, {
      className: `-=${CLASS_OPEN}`,
      clearProps: "all",
      ease: [
        Power1.easeIn, Power4.easeOut
      ],
      onComplete: () => {
        if (this._liveContainer) {
          removeClass(this._liveContainer, CLASS_OPEN)

        }
      }
    }, "-=0.1")
  }

  /**
   * Destroys the component and clears all references.
   */
  destroy() {
    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)
    window.removeEventListener("keydown", this._keydownHandler)

    this._input.removeEventListener("focus", this._focusHandler)
    this._input.removeEventListener("blur", this._blurHandler)

    window.removeEventListener("resize", this._resizeHandler)
    window.removeEventListener("orientationchange", this._resizeHandler)

    if (this._btnClose) {
      this._btnClose.removeEventListener("click", this._closeHandler)
    }

    this._input = null
    this._form = null
    this._btnClose = null

    this._focusHandler = null
    this._blurHandler = null
    this._closeHandler = null
    this._windowClickHandler = null
    this._keydownHandler = null

    this._liveSuggestions = null
    this._liveFooter = null
  }

  /**
   * Determines if the SearchInput is open/visible.
   * @return {Boolean} - True if open; otherwise false.
   */
  isOpen() {
    return this.hasClass(CLASS_OPEN)
  }
}

export function init() {
  searchAndInitialize(".search.search__input", (e) => {
    new SearchInput(e)
  })
}

export default SearchInput
