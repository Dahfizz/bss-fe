import { searchAndInitialize } from "../Utils"
import { TimelineLite, Power4, Power1 } from "gsap"
import DomElement from "../DomElement"
import * as Dom from "../DomFunctions"
import SearchInput from "../search/SearchInput"

const CLASS_OPEN = "is-open"
const CLASS_ACTIVE = "is-active"

const QUERY_NAV_HAMBURGER = ".nav-hamburger"
const QUERY_NAV_HB_BODY = ".nav__primary"

const CLASS_NAV_LINK = "nav-link--header"
const QUERY_NAV_LINK_ACTIVE = ".nav-link--header.is-active"

const QUERY_NAV_MOBILE = ".nav__level1 .nav__mainnav .nav__primary"
const QUERY_NAV_LEVEL0 = ".nav__level0"
const QUERY_NAV_LEVEL0_CONTAINER = ".nav__level0 .nav__subnav"
const QUERY_SECTION_OPEN = ".nav-section.is-open"

const QUERY_NAV_LEVEL1 = ".nav__level1 .nav__mainnav"

const QUERY_NAV_LEVEL0_LINK = ".nav-link.nav-link--header"
const QUERY_NAV_LEVEL1_LINK = ".nav-link--header"

const QUERY_NAV_COLUMN = ".nav-col"
const QUERY_NAV_COLUMN_ACTIVE = ".nav-col.is-active"

const QUERY_NAV_BODY = ".nav-body"
const QUERY_NAV_FOOTER = ".nav-footer"

const QUERY_SEARCH_ICON = ".nav-search"
const QUERY_SEARCH_FIELD = ".search__input"
const CLASS_SEARCH_DESKTOP = "search--desktop"

const ANIMATION_START_DELAY = 0.2
const ANIMATION_OFFSET = 0.05

const ANIMATION_BODY_DURATION = 0.3
const ANIMATION_FOOTER_DURATION = 0.1

/**
 * The navigation component definition.
 */
class Navigation extends DomElement {

  constructor(element) {
    super(element)

    this._navLevel0 = this.element.querySelector(QUERY_NAV_LEVEL0) || document.createElement("div")
    this._navLevel0Body = this.element.querySelector(QUERY_NAV_LEVEL0_CONTAINER) || document.createElement("div")
    this._navLevel1 = this.element.querySelector(QUERY_NAV_LEVEL1) || document.createElement("div")

    this._navMobile = this.element.querySelector(QUERY_NAV_MOBILE)
    if (!this._navMobile) {
      let dummyParent = document.createElement("div")
      this._navMobile = document.createElement("div")
      dummyParent.appendChild(this._navMobile)
    }

    this._hamburgerElement = this.element.querySelector(QUERY_NAV_HAMBURGER) || document.createElement("div")
    this._searchComponents = []

    this._level0ClickHandler = this._handleLevel0Click.bind(this)
    this._level1ClickHandler = this._handleLevel1Click.bind(this)
    this._windowClickHandler = this._handleWindowClick.bind(this)
    this._searchClickHandler = this._handleSearchClick.bind(this)

    this._initialize()
  }

  _resetMainTimeline() {
    if (this._tlMain) {
      this._tlMain.stop()
    }

    this._tlMain = new TimelineLite({
      onComplete: () => {
        this._tlMain = undefined
      }
    })
  }

  _isMobile() {
    return Dom.isHidden(this._hamburgerElement, true) === false
  }

  _handleLevel0Click(event) {
    const isDesktop = !this._isMobile()

    if (isDesktop) {
      this._resetMainTimeline()

      let navItems = new NavigationItems(this)
        .fromLevel0(event.target)

      if (!navItems.section) {
        return
      }

      let previousNavLink = this._navLevel0.querySelector(QUERY_NAV_LINK_ACTIVE)
      let previousNavSection = this._navLevel0.querySelector(QUERY_SECTION_OPEN)

      this._toggleContainer(navItems.link, this._navLevel0Body, navItems.section, undefined,
        previousNavLink, this._navLevel0Body, previousNavSection, undefined, true)
    }
  }

  _handleLevel1Click(event) {
    let navItems = new NavigationItems(this)
      .fromLevel1(event.target)

    let prevItems = navItems.previousLevel1()

    this._toggleContainer(navItems.link, navItems.container, navItems.section, navItems.footer, prevItems.link,
      prevItems.container, prevItems.section, prevItems.footer, false)

    return false
  }

  _toggleContainer(navLink, navContainer, navSection, navFooter, previousNavLink, previousNavContainer, previousNavSection, previousNavFooter, animateContainer = false) {
    const isDesktop = !this._isMobile()

    if (previousNavLink && previousNavLink !== navLink && navLink !== this._hamburgerElement) {
      Dom.removeClass(previousNavLink, CLASS_ACTIVE)
    }

    if (Dom.hasClass(navLink, CLASS_ACTIVE)) {
      Dom.removeClass(navLink, CLASS_ACTIVE)

      if (isDesktop) {
        this._onNavigationClosed()

        this._resetMainTimeline()
        this._closeSection(navContainer, navSection, navFooter, this._tlMain, true, animateContainer)
      } else if (navLink === this._hamburgerElement) {
        // Close mobile navigation
        this._onNavigationClosed()

        this._resetMainTimeline()
        this._closeSection(navContainer, navSection, undefined, this._tlMain, false, false)
      } else if (!isDesktop) {
        // Close the section
        this._closeSection(navContainer, navSection, navFooter, undefined, true, animateContainer)
      }
    } else {
      Dom.addClass(navLink, CLASS_ACTIVE)

      if (isDesktop) {
        Dom.addClass(this._navMobile, CLASS_OPEN)
        this._onNavigationOpened()
        this._resetMainTimeline()

        if (previousNavContainer && previousNavSection) {
          this._closeSection(previousNavContainer, previousNavSection, previousNavFooter, this._tlMain, true, animateContainer)
        }
        this._openSection(navContainer, navSection, navFooter, this._tlMain, true, animateContainer)
      } else if (navLink === this._hamburgerElement) {
        // Open mobile navigation
        this._onNavigationOpened()

        this._resetMainTimeline()
        this._openSection(navContainer, navSection, undefined, this._tlMain, false, false)
      } else if (!isDesktop) {
        // Open section
        if (previousNavContainer && previousNavSection) {
          this._closeSection(previousNavContainer, previousNavSection, previousNavFooter, undefined, true, animateContainer)
        }
        this._openSection(navContainer, navSection, navFooter, undefined, true, animateContainer)
      }
    }
  }

  _onNavigationOpened() {
    Dom.addClass(this._navMobile, CLASS_OPEN)
    Dom.addClass(this._navMobile.parentElement, CLASS_OPEN)
    Dom.addClass(this._hamburgerElement, CLASS_ACTIVE)

    window.addEventListener("click", this._windowClickHandler)
    window.addEventListener("touchend", this._windowClickHandler)
  }

  _onNavigationClosed() {
    Dom.removeClass(this._navMobile, CLASS_OPEN)
    Dom.removeClass(this._navMobile.parentElement, CLASS_OPEN)
    Dom.removeClass(this._hamburgerElement, CLASS_ACTIVE)

    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)
  }

  _handleWindowClick(event) {
    let target = event.target

    while (target !== this.element && target.parentElement) {
      target = target.parentElement
    }

    if (target !== this.element) {
      this.close()
      return false
    }

    return true
  }

  _openSection(navContainer, navSection, navFooter, tl, animateColumns = true, animateContainer = false) {
    if (!navSection) {
      return
    }

    let activeItems = navSection.querySelectorAll(QUERY_NAV_COLUMN)

    if (animateContainer === true) {
      let container = navContainer
      navContainer = navSection
      navSection = container
    }

    if (!tl) {
      tl = new TimelineLite()
    }

    tl.set(navContainer, {
      className: `+=${CLASS_OPEN}`
    })

    tl.set(navSection, {
      display: "block"
    })

    tl.to(navSection, ANIMATION_BODY_DURATION, {
      className: `+=${CLASS_OPEN}`,
      clearProps: "all",
      ease: [
        Power1.easeIn, Power4.easeOut
      ]
    })

    if (navFooter) {
      tl.set(navFooter.querySelectorAll(QUERY_NAV_COLUMN), {
        className: `+=${CLASS_ACTIVE}`
      })

      tl.set(navFooter, {
        display: "block"
      }, 0)

      tl.to(navFooter, ANIMATION_FOOTER_DURATION, {
        className: `+=${CLASS_OPEN}`,
        clearProps: "height, display",
        ease: [
          Power1.easeIn, Power4.easeOut
        ]
      }, "-=0.1")
    }

    if (animateColumns === true) {
      let delay = ANIMATION_START_DELAY
      let items = activeItems

      for (let index = 0; index < items.length; index++) {
        tl.to(items[index], 0, {
          className: `+=${CLASS_ACTIVE}`
        }, delay)
        delay += ANIMATION_OFFSET
      }
    }
  }

  _closeSection(navContainer, navSection, navFooter, tl, animateColumns = true, animateContainer = false) {
    if (!navSection) {
      return
    }

    let activeItems = navSection.querySelectorAll(QUERY_NAV_COLUMN_ACTIVE)

    if (animateContainer === true) {
      let container = navContainer
      navContainer = navSection
      navSection = container
    }

    if (!tl) {
      tl = new TimelineLite()
    }

    tl.set(navSection, {
      display: "block"
    })

    if (animateColumns === true) {
      tl.set(activeItems, {
        className: `-=${CLASS_ACTIVE}`
      }, 0)
    }

    tl.to(navSection, ANIMATION_BODY_DURATION, {
      className: `-=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ],
      clearProps: "all",
      onComplete: () => {
        Dom.removeClass(navContainer, CLASS_OPEN)

        if (animateColumns === true) {
          for (let active of activeItems) {
            Dom.removeClass(active, CLASS_ACTIVE)
          }
        }
      }
    }, 0)

    if (navFooter) {
      tl.set(navFooter, {
        display: "block"
      }, 0)

      tl.to(navFooter, ANIMATION_FOOTER_DURATION, {
        className: `-=${CLASS_OPEN}`,
        ease: [
          Power1.easeIn, Power4.easeOut
        ],
        clearProps: "height,display",
        onComplete: () => {
          for (let active of navFooter.querySelectorAll(QUERY_NAV_COLUMN_ACTIVE)) {
            Dom.removeClass(active, CLASS_ACTIVE)
          }
        }
      }, 0)
    }
  }

  _handleSearchClick() {
    this._searchDesktop.open()
  }

  /**
   * Initializes the navigation component.
   * @private
   */
  _initialize() {
    for (let navLink of this._navLevel0.querySelectorAll(QUERY_NAV_LEVEL0_LINK)) {
      navLink.addEventListener("click", this._level0ClickHandler)
    }

    for (let navLink of this._navLevel1.querySelectorAll(QUERY_NAV_LEVEL1_LINK)) {
      navLink.addEventListener("click", this._level1ClickHandler)
    }

    this._hamburgerElement.addEventListener("click", this._level1ClickHandler)

    // Desktop search icon
    let searchIcon = this.element.querySelector(QUERY_SEARCH_ICON)
    if (searchIcon) {
      searchIcon.addEventListener("click", this._searchClickHandler)
    }

    for (let search of this.element.querySelectorAll(QUERY_SEARCH_FIELD)) {
      let searchComponent = new SearchInput(search)

      if (Dom.hasClass(search, CLASS_SEARCH_DESKTOP) || Dom.hasClass(search.parentNode, CLASS_SEARCH_DESKTOP)) {
        this._searchDesktop = searchComponent
      }

      this._searchComponents.push(searchComponent)
    }
  }

  /**
   * Closes the navigation.
   */
  close() {
    let isMoble = this._isMobile()
    this._resetMainTimeline()

    let level1 = this._navLevel1.querySelector(QUERY_NAV_LINK_ACTIVE)
    let level0 = this._navLevel0.querySelector(QUERY_NAV_LINK_ACTIVE)

    if (!level1 && isMoble && Dom.hasClass(this._hamburgerElement, CLASS_ACTIVE)) {
      level1 = this._hamburgerElement
    }

    if (level1) {
      let navItems = new NavigationItems(this)
        .fromLevel1(level1)

      Dom.removeClass(navItems.link, CLASS_ACTIVE)
      this._onNavigationClosed()
      this._closeSection(navItems.container, navItems.section, navItems.footer, this._tlMain, !isMoble, false)
    }

    if (level0) {
      let navItems = new NavigationItems(this)
        .fromLevel0(level0)

      Dom.removeClass(navItems.link, CLASS_ACTIVE)
      this._onNavigationClosed()
      this._closeSection(navItems.container, navItems.section, navItems.footer, this._tlMain, !isMoble, true)
    }
  }
}

class NavigationItems {
  constructor(nav) {
    this._navigation = nav

    this._link = undefined
    this._container = undefined
    this._section = undefined
    this._footer = undefined
  }

  get link() {
    return this._link
  }

  get container() {
    return this._container
  }

  get section() {
    return this._section
  }

  get footer() {
    return this._footer
  }

  fromLevel0(navLink) {
    while (!Dom.hasClass(navLink, CLASS_NAV_LINK) && navLink.parentElement) {
      navLink = navLink.parentElement
    }

    this._link = navLink

    let toggleId = navLink.getAttribute("data-toggle")
    this._container = this._navigation._navLevel0Body
    this._section = this._navigation._navLevel0.querySelector(`#${toggleId}`)

    return this
  }

  fromLevel1(navLink) {
    while (navLink.parentElement) {
      if ((navLink === this._navigation._hamburgerElement) || Dom.hasClass(navLink, CLASS_NAV_LINK)) {
        break
      }

      navLink = navLink.parentElement
    }

    this._link = navLink
    this._container = navLink.parentElement
    this._section = this._container.querySelector(QUERY_NAV_BODY)
    this._footer = this._container.querySelector(QUERY_NAV_FOOTER)

    if (navLink === this._navigation._hamburgerElement) {
      this._container = this._navigation._navLevel1
      this._section = this._container.querySelector(QUERY_NAV_HB_BODY)
    }

    return this
  }

  previousLevel1() {
    let prev = new NavigationItems(this._navigation)

    prev._link = this._navigation._navLevel1.querySelector(QUERY_NAV_LINK_ACTIVE)
    prev._container = prev._link ? prev._link.parentElement : undefined
    prev._section = prev._container ? prev._container.querySelector(QUERY_NAV_BODY) : undefined
    prev._footer = prev._container ? prev._container.querySelector(QUERY_NAV_FOOTER) : undefined

    return prev
  }

  isHamburger() {
    return this._link === this._navigation._hamburgerElement
  }
}

export function init() {
  searchAndInitialize(".nav", (e) => {
    new Navigation(e)
  })
}

export default Navigation
