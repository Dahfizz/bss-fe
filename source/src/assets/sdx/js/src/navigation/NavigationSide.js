import { searchAndInitialize, preventDefault } from "../Utils"
import { TimelineLite, Power4, Power1, TweenLite } from "gsap"
import { hasClass, addClass, removeClass } from "../DomFunctions"
import DomElement from "../DomElement"

const CLASS_OPEN = "is-open"
const CLASS_ACTIVE = "is-active"

const QUERY_SITE_WRAPPER = ".js-site-wrapper"
const QUERY_NAV_HAMBURGER = ".js-site-wrapper .js-hamburger"

const QUERY_NAV_ITEM = ".js-nav-item"

const NAV_LINK_INITIAL_SCALE = 0.9
const ANIMATION_DURATION_LINKS = 0.1
const ANIMATION_DURATION_NAV = 0.3

const ANIMATION_STAGGER_DELAY = 0.05

/**
 * The navigation side component definition.
 */
class NavigationSide extends DomElement {

  constructor(element) {
    super(element)

    this._clickHandler = this._handleClick.bind(this)
    this._windowClickHandler = this._handleWindowClick.bind(this)

    this._siteWrapper = document.querySelector(QUERY_SITE_WRAPPER)
    this._hamburgerElement = document.querySelector(QUERY_NAV_HAMBURGER) || document.createElement("div")
    this._navItems = this.element.querySelectorAll(QUERY_NAV_ITEM)

    this._initialize()
  }

  _initialize() {
    this._hamburgerElement.addEventListener("click", this._clickHandler)
  }

  _handleClick(event) {
    preventDefault(event)
    this.toggle()
  }

  _handleWindowClick(event) {
    let target = event.target

    while (target !== this.element && target.parentElement) {
      target = target.parentElement
    }

    if (target !== this.element) {
      this.close()
      return false
    }

    return true
  }

  /**
   * Toggles the side navigation.
   */
  toggle() {
    if (hasClass(this.element, CLASS_OPEN) === false) {
      this.open()
    } else {
      this.close()
    }
  }

  /**
   * Opens the slide navigation.
   */
  open() {
    let elements = [this.element, this._siteWrapper]
    TweenLite.killTweensOf(elements)

    setTimeout(() => {
      window.addEventListener("click", this._windowClickHandler)
      window.addEventListener("touchend", this._windowClickHandler)
    }, 50)

    let tl = new TimelineLite()

    addClass(this._hamburgerElement, CLASS_ACTIVE)

    tl.to(elements, ANIMATION_DURATION_NAV, {
      className: `+=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ]
    })

    tl.staggerFrom(this._navItems, ANIMATION_DURATION_LINKS, {
      autoAlpha: 0,
      scaleX: NAV_LINK_INITIAL_SCALE,
      scaleY: NAV_LINK_INITIAL_SCALE
    }, ANIMATION_STAGGER_DELAY)
  }

  /**
   * Closes the side navigation.
   */
  close() {
    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)

    let elements = [this.element, this._siteWrapper]
    TweenLite.killTweensOf(elements)

    removeClass(this._hamburgerElement, CLASS_ACTIVE)

    let tl = new TimelineLite()

    tl.to(elements, ANIMATION_DURATION_NAV, {
      className: `-=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ]
    })
  }

  /**
   * Destroys the component and removes all event
   * subscriptions and references.
   */
  destroy() {
    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)

    this._windowClickHandler = null

    this._clickHandler = null
    this._siteWrapper =  null

    this._hamburgerElement = null
    this._navItems = null
  }
}

export function init() {
  searchAndInitialize(".nav-side", (e) => {
    new NavigationSide(e)
  })
}

export default NavigationSide
