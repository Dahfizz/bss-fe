/**
 * Calls the callback function when the document has been completely parsed.
 * @param {callback} value The callback function to execute.
 */

/* eslint-disable */
export function onDocumentReady(callback) {
  function completed() {
    document.removeEventListener('DOMContentLoaded', completed, false);
    window.removeEventListener('load', completed, false);
    callback();
  }

  if (document.readyState === 'complete') {
    setTimeout(callback);
  } else {
    document.addEventListener('DOMContentLoaded', completed, false);

    // A fallback to window.onload, that will always work
    window.addEventListener('load', completed, false);
  }
}

export function eachElement(selectors, callback) {
  if (!callback) {
    throw new Error('The callback cannot be undefined');
  }

  let elements = Array.from(document.querySelectorAll(selectors));

  for (let e of elements) {
    callback(e);
  }
}

/**
 * Searches for elements with the given selector and calls the callback
 * function if the `data-init` attribute is present on the element.
 * @param {selector} value The query.
 * @param {callback} value The callback function to initialize the element.
 * @param {function} initSelector The inititalization element selector function.
 */
export function searchAndInitialize(
  selector,
  callback,
  initSelector = undefined
) {
  if (!callback) {
    throw new Error('The callback cannot be undefined');
  }

  let elements = Array.from(document.querySelectorAll(selector));

  for (let e of elements) {
    let initElement = e;

    if (initSelector) {
      initElement = initSelector(e);
    }

    if (initElement.getAttribute('data-init') === 'auto') {
      callback(e);
    }
  }
}

/**
 * Returns a number whose value is limited to the given range.
 *
 * Example: limit the output of this computation to between 0 and 255
 * Utils.clamp(number, 0, 255)
 *
 * @param {Number} value The number to clamp
 * @param {Number} min The lower boundary of the output range
 * @param {Number} max The upper boundary of the output range
 * @returns A number in the range [min, max]
 * @type Number
 */
export function clamp(value, min, max) {
  return Math.min(Math.max(value, min), max);
}

/**
 * A polyfill for Event.preventDefault().
 * @param {Event} event - The event to prevent the default action.
 */
export function preventDefault(event) {
  if (event.preventDefault) {
    event.preventDefault();
  } else {
    event.returnValue = false;
  }
}

/**
 * A polyfill for Node.remove().
 * @param {Node} node - The node to remove.
 */
export function remove(node) {
  if (!node || !node.parentNode) {
    return;
  }

  node.parentNode.removeChild(node);
}

/**
 * A simple polyfill for the Array.find() method.
 * @param {Array} array - The array to search in.
 * @param {function} expression - The expresstion to evaluate. Must return true if the element matches.
 */
export function find(array, expression) {
  for (let i = 0; i < array.length; i++) {
    let item = array[i];
    if (expression(item) === true) {
      return item;
    }
  }

  return undefined;
}

/**
 * Checks the useragent and returns the Microsoft Internet Explorer / Edge version.
 * If another browser is detected 0 is returned.
 */
export function msIEVersion(userAgent = undefined) {
  // see http://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery
  const ua = userAgent || window.navigator.userAgent;

  const msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  const trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    const rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  const edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return 0;
}
