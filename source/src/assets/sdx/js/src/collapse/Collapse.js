import { preventDefault } from "../Utils"
import { TweenLite, Power1, Power4 } from "gsap"
import DomElement from "../DomElement"
import { addClass, hasClass, removeClass, isHidden } from "../DomFunctions"

const CLASS_OPEN = "is-open"

const ANIMATION_OPEN = 0.3

/**
 * The Collapse component.
 */
class Collapse extends DomElement {

  /**
   * Creates and initializes the Collapse component.
   * @param {DomElement} - The root element of the Collapse component.
   */
  constructor(element) {
    super(element)

    this._clickHandler = this._handleClick.bind(this)
    this._initialize()
  }

  /**
   * Initializes the Collapse component.
   * @private
   */
  _initialize() {
    let dataTarget = this.element.getAttribute("data-target")
    if (dataTarget === null || dataTarget === "") {

      /* eslint-disable no-console */
      console.error("A collapsible element requires a 'data-target' that specifies the element to collapse")
      console.info(this.element)
      /* eslint-enable no-console */

      return
    }

    let hiddenTarget = this.element.getAttribute("data-hidden")
    if (hiddenTarget !== null && hiddenTarget !== "") {
      this._hiddenIndicator = document.querySelector(hiddenTarget)
    }

    this._collapsibleElements = document.querySelectorAll(dataTarget)
    this.element.addEventListener("click", this._clickHandler)
  }

  _handleClick(event) {
    preventDefault(event)
    this.toggle()
  }

  /**
   * Toggles the collapseible.
   */
  toggle() {
    if (this._hiddenIndicator && isHidden(this._hiddenIndicator, false) === true) {
      return
    }

    if (hasClass(this.element, CLASS_OPEN) === false) {
      addClass(this.element, CLASS_OPEN)

      for (let s of this._collapsibleElements) {
        this._openCollapse(s)
      }
    } else {
      removeClass(this.element, CLASS_OPEN)

      for (let s of this._collapsibleElements) {
        this._closeCollapse(s)
      }
    }
  }

  _openCollapse(el) {
    TweenLite.killTweensOf(el)

    TweenLite.set(el, {
      display: "block"
    })

    TweenLite.to(el, ANIMATION_OPEN, {
      className: `+=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ]
    })

    // set aria expanded
    el.setAttribute("aria-expanded", true)
  }

  _closeCollapse(el) {
    TweenLite.killTweensOf(el)

    TweenLite.to(el, ANIMATION_OPEN, {
      className: `-=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ],
      onComplete: () => {
        TweenLite.set(el, {
          clearProps: "display"
        })
      }
    })

    // set aria expanded
    el.setAttribute("aria-expanded", false)
  }

  /**
   * Removes all event handlers and clears references.
   */
  destroy() {
    this._collapsibleElements = null

    if (this._clickHandler) {
      this.element.removeEventListener("click", this._clickHandler)
    }

    this.element = null
  }
}

export function init() {
  let elements = document.querySelectorAll("[data-toggle='collapse']")
  for (let e of elements) {
    if (e.getAttribute("data-init") === "auto") {
      new Collapse(e)
    }
  }
}

export default Collapse
