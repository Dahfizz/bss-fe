import { searchAndInitialize } from "../Utils"
import DomElement from "../DomElement"
import { parentWithClass, getRootElement } from "../DomFunctions"

const CLASS_BORDER = "empty-state__border"
const CLASS_BORDER_MODAL = "empty-state__border--modal"
const CLASS_ACTIVE = "is-active"
const CLASS_HASFILES = "has-files"
const CLASS_MODAL = "empty-state--modal"
const CLASS_MODAL_CONTENT = "modal__content"

const QUERY_MODAL_BODY = ".modal__body"
const QUERY_FILE = "input[type='file']"

/**
 * Empty state pattern
 */
class EmptyState extends DomElement {

  /**
   * Creates and initializes the Empty-State pattern component.
   * @param {DomElement} - root element of the empty-state pattern.
   */
  constructor(element) {
    super(element)

    this._border = new DomElement("div")
      .addClass(CLASS_BORDER)

    this._fileInput = this.element.querySelector(QUERY_FILE)
    this._button = this.element.querySelector("label")

    this._fileChangedHandler = this._handleFileChanged.bind(this)
    this._preventEventsHandler = this._preventDragEvents.bind(this)
    this._dragEnterHandler = this._handleDragEnter.bind(this)
    this._dragLeaveHandler = this._handleDragLeave.bind(this)
    this._dropHandler = this._handleDrop.bind(this)

    this._isDragging = false

    this._initialize()
  }

  _initialize() {
    if (this.hasClass(CLASS_MODAL)) {
      // handle modal dialogs
      this._dragArea = parentWithClass(this.element, CLASS_MODAL_CONTENT)
      let borderArea = this._dragArea.querySelector(QUERY_MODAL_BODY)
      borderArea.setAttribute("style", "pointer-events: none;")

      this._border = new DomElement("div")
        .addClass(CLASS_BORDER)
        .addClass(CLASS_BORDER_MODAL)

      borderArea.appendChild(this._border.element)


    } else {
      // normal modal dialog
      this._dragArea = this.element
      let borderArea = getRootElement()

      if (!borderArea.querySelector(`.${CLASS_BORDER}`)) {
        borderArea.appendChild(this._border.element)
      }
    }

    const form = this.element.querySelector("form")

    for (let event of ["drag", "dragstart", "dragend", "dragover", "dragenter", "dragleave", "drop"]) {
      this.element.addEventListener(event, this._preventEventsHandler)
      form.addEventListener(event, this._preventEventsHandler)
      this._dragArea.addEventListener(event, this._preventEventsHandler)
    }

    this._dragArea.addEventListener("dragover", this._dragEnterHandler)
    this._dragArea.addEventListener("dragenter", this._dragEnterHandler)

    this._dragArea.addEventListener("dragleave", this._dragLeaveHandler)
    this._dragArea.addEventListener("dragend", this._dragLeaveHandler)
    this._dragArea.addEventListener("drop", this._dragLeaveHandler)

    this._dragArea.addEventListener("drop", this._dropHandler)
    this._fileInput.addEventListener("change", this._fileChangedHandler)
  }

  _preventDragEvents(e) {
    e.preventDefault()
    e.stopPropagation()

    return false
  }

  _handleDragEnter() {
    if (this._isDragging === true) {
      return
    }

    this._isDragging = true
    this._button.setAttribute("style", "pointer-events: none;")

    this.addClass(CLASS_ACTIVE)
    this._border.addClass(CLASS_ACTIVE)
  }

  _handleDragLeave() {
    if (this._isDragging === false) {
      return
    }

    this._isDragging = false
    this._button.setAttribute("style", "")

    this.removeClass(CLASS_ACTIVE)
    this._border.removeClass(CLASS_ACTIVE)
  }

  _handleDrop(e) {
    this._fileInput.files = e.dataTransfer.files
  }

  _handleFileChanged() {
    let files = this._fileInput.files

    if (files && files.length > 0) {
      this.addClass(CLASS_HASFILES)
    } else {
      this.removeClass(CLASS_HASFILES)
    }
  }

  /**
   * Gets the currently selected files.
   */
  get files() {
    return this._fileInput.files
  }
}

export function init() {
  searchAndInitialize(".empty-state", (e) => {
    new EmptyState(e)
  })
}

export default EmptyState
