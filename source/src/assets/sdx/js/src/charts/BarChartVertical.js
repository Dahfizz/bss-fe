import DomElement from "../DomElement"
import { searchAndInitialize } from "../Utils"
import { text } from "../DomFunctions"
import { createLegendItem, isColor, removeAllChildren } from "./ChartFunctions"

import { TimelineLite, Power4 } from "gsap"

const QUERY_DATA_CATEGORIES = ".js-data-list .js-category"
const QUERY_DATA_ITEMS = ".js-data-list .js-data"
const QUERY_CHART = ".js-chart"
const QUERY_LEGEND = ".bar-chart__legend"

const CLASS_INDICATOR = "indicator"
const CLASS_LABEL_X = "axis-x-label"
const CLASS_INDICATOR_WRAPPER = "indicator-wrapper"
const CLASS_INDICATOR_INNER_WRAPPER = "indicator-wrapper-inner"
const CLASS_INDICATOR_EMPTY = "empty"

const CLASS_TOOLTIP = "tooltip"
const CLASS_TOOLTIP_RIGHT = "tooltip--right"
const CLASS_TOOLTIP_MULTILINE = "tooltip--multiline"

const ANIMATION_DURATION = 0.5

/**
 * Bar Chart Horizontal Component.
 */
class BarChartVertical extends DomElement {

  /**
   * Creates and initializes the bar chart horizontal component.
   * @param {DomElement} - root element of the chart.
   */
  constructor(element, data) {
    super(element)
    this._data = data

    this._initialize()
  }

  _initialize() {
    this._unit = this.getAttribute("data-unit") || ""

    this._minValue = parseFloat(this.getAttribute("data-min")) || 0
    this._maxValue = parseFloat(this.getAttribute("data-max")) || 100

    this._chart = this.element.querySelector(QUERY_CHART)
    this._legend = this.element.querySelector(QUERY_LEGEND)

    if (!this._data) {
      this._data = this._tryGetData(this.element)
    }

    this._render()
  }

  _tryGetData(element) {
    let data = {
      categories: [],
      items: []
    }

    let categories = element.querySelectorAll(QUERY_DATA_CATEGORIES)
    let items = element.querySelectorAll(QUERY_DATA_ITEMS)

    for (let category of categories) {
      data.categories.push(
        {
          title: text(category),
          color: category.getAttribute("data-color")
        }
      )
    }

    for (let item of items) {
      let dataEnty = {
        title: text(item),
        class: item.getAttribute("data-class"),
        values: []
      }

      let vals = item.getAttribute("data-value")
      if (vals) {
        for (let val of vals.split(",")) {
          dataEnty.values.push(parseFloat(val))
        }
      }

      data.items.push(dataEnty)
    }

    return data
  }

  _getTooltipContent(entry, categories) {
    let tooltip = ""
    for (let i = 0; i < entry.values.length; i++) {
      tooltip += `${categories[i].title}: ${entry.values[i]} ${this._unit}\n`
    }

    return tooltip.trim()
  }

  _render() {
    if (this._legend) {
      removeAllChildren(this._legend)

      for (let category of this._data.categories) {
        const legendItem = createLegendItem(category)
        this._legend.appendChild(legendItem)
      }
    }

    removeAllChildren(this._chart)

    let animationStages = []

    for (let item of this._data.items) {
      let element = new DomElement("li")

      if (item.class && item.class !== "") {
        element.addClass(item.class)
      }

      let listElement = new DomElement("ul")
        .addClass(CLASS_INDICATOR_WRAPPER)

      let wrapper = new DomElement("div")
        .addClass(CLASS_INDICATOR_INNER_WRAPPER)
      listElement.appendChild(wrapper)

      element.appendChild(listElement)

      let tooltip = this._getTooltipContent(item, this._data.categories)
      if (tooltip && tooltip !== "") {
        wrapper
          .addClass(CLASS_TOOLTIP)
          .addClass(CLASS_TOOLTIP_RIGHT)
          .setAttribute("aria-label", tooltip)

        if (item.values.length > 1) {
          wrapper.addClass(CLASS_TOOLTIP_MULTILINE)
        }
      }

      for (let i = 0; i < item.values.length; i++) {
        const height = (this._chart.offsetHeight / this._maxValue) * item.values[i]

        let indicator = new DomElement("li")
          .addClass(CLASS_INDICATOR)
          .setAttribute("style", `height: ${height}px;`)

        if (height > 0) {
          let color = this._data.categories[i].color
          if (isColor(color) === true) {
            indicator.setAttribute("style", `background-color: ${color};`)
          } else {
            indicator.addClass(color)
          }

          if (animationStages.length <= i) {
            animationStages.push([])
          }

          animationStages[i].push(indicator.element)
        } else {
          indicator.addClass(CLASS_INDICATOR_EMPTY)
        }

        wrapper.appendChild(indicator)
      }

      element.appendChild(new DomElement("div")
        .addClass(CLASS_LABEL_X)
        .setHtml(item.title))

      this._chart.appendChild(element.element)
    }

    let tl = new TimelineLite()
    for (let i = 0; i < animationStages.length; i++) {
      tl.from(animationStages[i], ANIMATION_DURATION, {
        height: 0,
        ease: Power4.easeInOut,
        autoRound: false
      })

      if (this._legend) {
        tl.from(this._legend.children[i], ANIMATION_DURATION, {
          opacity: 0,
          ease: Power4.easeInOut
        }, `-=${ANIMATION_DURATION}`)
      }
    }
  }

  /**
   * Updates the bar chart with the specified data definitions.
   * @param {Array} - bar chart data definitions.
   */
  update(data) {
    if (data) {
      this._data = data
    }

    this._render()
  }

  /**
   * Removes all event handlers and clears references.
   */
  destroy() {
    this._data = undefined

    if (this._legend) {
      removeAllChildren(this._legend)
      this._legend = undefined
    }
  }

  /**
   * @deprecated use destroy() instead.
   * @todo remove in version 2.0.0
   */
  destory() {
    this.destroy()
  }
}

export function init() {
  searchAndInitialize(".bar-chart-vertical", (e) => {
    new BarChartVertical(e)
  })
}

export default BarChartVertical
