/* eslint-disable */

import DomElement from '../DomElement';
import { searchAndInitialize, remove } from '../Utils';
import { getAttributeReference } from '../DomFunctions';
import {
  tryGetData,
  createLegendItem,
  isColor,
  removeAllChildren
} from './ChartFunctions';

import { TimelineLite, Power4 } from 'gsap';

const QUERY_INDICATOR = '.bar-chart__progress';
const QUERY_DETAIL_RIGHT = '.detail-right';
const QUERY_DETAIL_BOTTOM = '.detail-bottom';
const QUERY_PROGRESS = '.bar-chart__progress';

const CLASS_UNLIMITED = 'bar-chart-horizontal--unlimited';
const CLASS_LIMITED = 'bar-chart-horizontal--limited';

const CLASS_DETAIL_VALUE = 'value';
const CLASS_DETAIL_UNIT = 'unit';

const CLASS_INDICATOR = 'indicator';
const CLASS_INDICATOR_WRAPPER = 'indicator-wrapper';

const CLASS_TOOLTIP = 'tooltip';
const CLASS_TOOLTIP_MULTILINE = 'tooltip--multiline';

const ANIMATION_DURATION = 0.5;

/**
 * Bar Chart Horizontal Component.
 */
class BarChartHorizontal extends DomElement {
  /**
   * Creates and initializes the bar chart horizontal component.
   * @param {DomElement} - root element of the chart.
   */
  constructor(element, data) {
    super(element);
    this._data = data;

    this._legendItems = [];

    this._initialize();
  }

  _initialize() {
    this._indicatorWrapper = this.element.querySelector(QUERY_INDICATOR);

    this._unit = this.getAttribute('data-unit') || '';
    this._maxValue = parseFloat(this.getAttribute('data-max'));
    this._precision = parseInt(this.getAttribute('data-precision'), 10) || 0;

    this._isUnlimited = this.hasClass(CLASS_UNLIMITED);
    this._isLimited = this.hasClass(CLASS_LIMITED);

    this._progessWrapper = this.element.querySelector(QUERY_PROGRESS);

    if (this._isLimited === true) {
      this._detailRight = this.element.querySelector(QUERY_DETAIL_BOTTOM);
    } else {
      this._detailRight = this.element.querySelector(QUERY_DETAIL_RIGHT);
    }

    if (this._isUnlimited === false && this._isLimited === false) {
      this._legend = getAttributeReference(this.element, 'data-legend');
    }

    if (!this._data) {
      this._data = tryGetData(this.element);
    }

    this._render();
  }

  _render() {
    let dataOne = this._data[0];
    let dataTwo = this._data[1];

    let tl = new TimelineLite();
    let tooltip =
      this._isLimited === false
        ? this._getTooltipContent(this._data)
        : undefined;

    let animatedValueElement;

    // Cleanup
    removeAllChildren(this._detailRight);
    removeAllChildren(this._progessWrapper);

    // Clear only own legend items
    for (let item of this._legendItems) {
      remove(item);
    }
    this._legendItems = [];

    if (dataOne) {
      if (
        this._isUnlimited === false ||
        (this._isUnlimited === true && !dataTwo)
      ) {
        let valElement = (animatedValueElement = this._createValueElement(
          dataOne
        ));
        this._detailRight.appendChild(valElement);

        if (this._isLimited === false) {
          let separatorElement = new DomElement('div')
            .addClass(CLASS_DETAIL_UNIT)
            .setHtml(` ${this._unit}`).element;

          this._detailRight.appendChild(separatorElement);
        }
      }

      // Add the indicator
      let indicator = this._addIndicator(dataOne, tooltip);

      tl.from(indicator, ANIMATION_DURATION, {
        width: 0,
        ease: Power4.easeInOut
      });

      // Animate the value if required
      if (animatedValueElement && this._isLimited === true) {
        let counter = { var: 0 };
        tl.to(
          counter,
          ANIMATION_DURATION,
          {
            var: dataOne.value,
            roundProps: 'var',
            onUpdate: () => {
              animatedValueElement.innerHTML = `${counter.var}`;
            },
            ease: Power4.easeOut
          },
          `-=${ANIMATION_DURATION}`
        );
      }

      // Add the legend
      if (this._legend) {
        const legendItem = createLegendItem(dataOne);
        this._legend.appendChild(legendItem);
        this._legendItems.push(legendItem);

        tl.from(
          legendItem,
          ANIMATION_DURATION,
          {
            opacity: 0,
            ease: Power4.easeInOut
          },
          `-=${ANIMATION_DURATION}`
        );
      }
    }

    if (dataTwo) {
      let valElement = this._createValueElement(dataTwo);

      let unitElement = new DomElement('div')
        .addClass(CLASS_DETAIL_UNIT)
        .setHtml(` ${this._unit}`).element;

      this._detailRight.appendChild(valElement);
      this._detailRight.appendChild(unitElement);

      // Add the indicator
      let indicator = this._addIndicator(dataTwo, tooltip);
      tl.from(indicator, ANIMATION_DURATION, {
        width: 0,
        ease: Power4.easeInOut
      });

      // Add the legend
      if (this._legend) {
        const legendItem = createLegendItem(dataTwo);
        this._legend.appendChild(legendItem);
        this._legendItems.push(legendItem);

        tl.from(
          legendItem,
          ANIMATION_DURATION,
          {
            opacity: 0,
            ease: Power4.easeInOut
          },
          `-=${ANIMATION_DURATION}`
        );
      }
    }

    if (this._isLimited === true) {
      let valElement = this._createValueElement({ value: this._maxValue });

      let unitElement = new DomElement('div')
        .addClass(CLASS_DETAIL_UNIT)
        .setHtml(` ${this._unit}`).element;

      this._detailRight.appendChild(valElement);
      this._detailRight.appendChild(unitElement);
    }
  }

  _createValueElement(data) {
    let unlimitedPrefix = '';

    if (this._isUnlimited === true) {
      unlimitedPrefix = '+';
    }

    let value = parseFloat(data.value);
    if (value <= 0) {
      value = '.00';
    } else {
      value = value.toFixed(this._precision);
    }

    return new DomElement('div')
      .addClass(CLASS_DETAIL_VALUE)
      .setHtml(`${unlimitedPrefix}${value}`).element;
  }

  _addIndicator(data, tooltip) {
    let width = (99.8 / this._maxValue) * data.value;

    let indicator = new DomElement('div').addClass(CLASS_INDICATOR);

    if (isColor(data.color) === true) {
      indicator.setAttribute('style', `background-color: ${data.color};`);
    } else {
      indicator.addClass(data.color);
    }

    let indicatorWrapper = new DomElement('div')
      .addClass(CLASS_INDICATOR_WRAPPER)
      .setAttribute('style', `width: ${width}%`)
      .appendChild(indicator)
      .setAttribute('onclick', 'void(0)');

    if (tooltip && tooltip !== '') {
      indicatorWrapper
        .addClass(CLASS_TOOLTIP)
        .addClass(CLASS_TOOLTIP_MULTILINE)
        .setAttribute('aria-label', tooltip);
    }

    this._progessWrapper.appendChild(indicatorWrapper.element);
    return indicatorWrapper.element;
  }

  _getTooltipContent(dataList) {
    let tooltip = '';
    for (let data of dataList) {
      tooltip += `${data.title}: ${data.value} ${this._unit}\n`;
    }

    return tooltip.trim();
  }

  /**
   * Updates the bar chart with the specified data definitions.
   * @param {Array} - bar chart data definitions.
   */
  update(data) {
    if (data) {
      this._data = data;
    }

    this._render();
  }

  /**
   * Removes all event handlers and clears references.
   */
  destroy() {
    this._data = undefined;

    removeAllChildren(this._detailRight);
    removeAllChildren(this._progessWrapper);

    this._detailRight = undefined;
    this._progessWrapper = undefined;

    for (let item of this._legendItems) {
      remove(item);
    }

    this._legendItems = undefined;
    this._legend = undefined;
  }

  /**
   * @deprecated use destroy() instead.
   * @todo remove in version 2.0.0
   */
  destory() {
    this.destroy();
  }
}

export function init() {
  searchAndInitialize('.bar-chart-horizontal', e => {
    new BarChartHorizontal(e);
  });
}

export default BarChartHorizontal;
