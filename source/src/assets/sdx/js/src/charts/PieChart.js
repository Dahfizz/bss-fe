/* eslint-disable */

import DomElement from '../DomElement';
import { searchAndInitialize } from '../Utils';
import { removeAllChildren, tryGetData, isColor } from './ChartFunctions';
import { TimelineLite, Power4 } from 'gsap';

const QUERY_CHART = '.js-chart';
const QUERY_LEGEND = '.js-legend';

const DASH_SEPARATOR_WIDTH = 3;
const ANIMATION_DURATION = 1.5;

const QUERY_META_TITLE = '.meta .title';
const QUERY_META_SUBTITLE = '.meta .subtitle';

/**
 * Pie Chart Component.
 */
class PieChart extends DomElement {
  /**
   * Creates and initializes the Pie Chart component.
   * @param {DomElement} - root element of the chart.
   * @param {Array} - pie chart data definitions.
   */
  constructor(element, data = undefined) {
    super(element);
    this._data = data;

    this._initialize();
  }

  _initialize() {
    this._chart = this.element.querySelector(QUERY_CHART);
    this._legend = this.element.querySelector(QUERY_LEGEND);
    this._title = this.element.querySelector(QUERY_META_TITLE);
    this._subtitle = this.element.querySelector(QUERY_META_SUBTITLE);

    this._unit = this.getAttribute('data-unit') || '';

    if (!this._data) {
      this._data = tryGetData(this.element);
    }

    this._render();
  }

  _render() {
    const total = this._data.reduce((a, b) => a + b.value, 0);
    const r = 16;
    const dashTotal = 2 * r * Math.PI;

    let currentRotate = 9;

    // Cleanup
    removeAllChildren(this._chart);

    if (this._legend) {
      removeAllChildren(this._legend);
    }

    let tl = new TimelineLite();

    let percentageAdjustTotal = 0;
    let percentageAdjust = 0;
    const separatorPercentage = DASH_SEPARATOR_WIDTH / 100;

    for (let i = 0; i < this._data.length; i++) {
      const entry = this._data[i];

      const percentage = entry.value / total;
      if (percentage < separatorPercentage) {
        percentageAdjustTotal += separatorPercentage - percentage;
        percentageAdjust++;
      }
    }

    if (percentageAdjust > 0) {
      percentageAdjust =
        percentageAdjustTotal / (this._data.length - percentageAdjust);
    }

    for (let i = 0; i < this._data.length; i++) {
      const entry = this._data[i];

      const displayPercentage = entry.value / total;
      const percentage = Math.max(
        separatorPercentage,
        displayPercentage - percentageAdjust
      );

      let dashWidth = percentage * dashTotal - DASH_SEPARATOR_WIDTH;

      let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      svg.setAttribute('viewBox', '0 0 34 34');
      svg.setAttribute('role', 'img');
      svg.setAttribute('aria-labelledby', 'title desc');

      let title = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'title'
      );
      title.setAttribute('id', 'title');
      title.innerHTML = `Pie chart segment ${Math.floor(
        displayPercentage * 100
      )}%`;

      let description = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'desc'
      );
      description.setAttribute('id', 'desc');
      description.innerHTML = `${entry.title}: ${entry.value}`;

      let circle = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'circle'
      );
      circle.setAttribute('cx', 17);
      circle.setAttribute('cy', 17);
      circle.setAttribute('r', r);
      circle.setAttribute('stroke-dasharray', `${dashWidth} ${dashTotal}`);

      circle.setAttribute('style', 'stroke-width: 0');

      if (isColor(entry.color) === true) {
        circle.setAttribute('stroke', `${entry.color}`);
      } else {
        circle.setAttribute('class', entry.color);
      }

      circle.setAttribute('role', 'presentation');
      svg.setAttribute('style', `transform: rotate(${currentRotate}deg);`);

      svg.appendChild(title);
      svg.appendChild(description);
      svg.appendChild(circle);
      this._chart.appendChild(svg);

      let duration = ANIMATION_DURATION * percentage;

      tl.set(circle, {
        clearProps: 'strokeWidth'
      });

      if (circle.classList) {
        tl.from(circle, duration, {
          strokeDasharray: '0.5 100',
          ease: Power4.ease
        });
      } else {
        // IE 10 & 11 fallback since animating stroke-dasharray does not work
        // we have to do it manualy

        let counter = { var: 0.5 };
        tl.to(counter, duration, {
          var: dashWidth,
          onUpdate: () => {
            circle.setAttribute(
              'stroke-dasharray',
              `${counter.var} ${dashTotal}`
            );
          },
          ease: Power4.ease
        });
      }

      // Legend
      if (this._legend && this._data.length > 1) {
        let bullet = new DomElement('span').addClass('bullet');

        if (isColor(entry.color) === true) {
          bullet.setAttribute('style', `background-color: ${entry.color}`);
        } else {
          bullet.addClass(entry.color);
        }

        let caption = new DomElement('span').setHtml(entry.title);

        let legendItem = new DomElement('li')
          .appendChild(bullet)
          .appendChild(caption);

        this._legend.appendChild(legendItem.element);

        tl.from(
          legendItem.element,
          duration,
          {
            opacity: 0,
            ease: Power4.easeInOut
          },
          `-=${duration}`
        );
      }

      currentRotate += 360 * percentage;

      if (i === this._data.length - 1) {
        this._title.innerHTML = `${entry.value} ${this._unit}`;
        this._subtitle.innerHTML = entry.title;
      }
    }
  }

  /**
   * Updates the pie chart with the specified data definitions.
   * @param {Array} - pie chart data definitions.
   */
  update(data) {
    if (data) {
      this._data = data;
    }

    this._render();
  }

  /**
   * Removes all event handlers and clears references.
   */
  destroy() {
    this._data = undefined;
    this._title = undefined;
    this._subtitle = undefined;
    this._unit = undefined;

    removeAllChildren(this._chart);
    this._chart = undefined;

    if (this._legend) {
      removeAllChildren(this._legend);
      this._legend = undefined;
    }
  }

  /**
   * @deprecated use destroy() instead.
   * @todo remove in version 2.0.0
   */
  destory() {
    this.destroy();
  }
}

export function init() {
  searchAndInitialize('.pie-chart', e => {
    new PieChart(e);
  });
}

export default PieChart;
