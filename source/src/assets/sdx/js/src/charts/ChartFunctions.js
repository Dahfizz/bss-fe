/* eslint-disable */

import DomElement from '../DomElement';
import { text } from '../DomFunctions';

const QUERY_DATA = '.js-data';

export function tryGetData(element) {
  let data = [];
  let elements = element.querySelectorAll(QUERY_DATA);

  for (let entry of elements) {
    let value = parseFloat(entry.getAttribute('data-value'));
    let color = entry.getAttribute('data-color');
    let title = text(entry);

    let item = {
      title,
      value,
      color
    };

    data.push(item);
  }

  return data;
}

export function removeAllChildren(node) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

export function createLegendItem(data) {
  const bullet = new DomElement('span').addClass('bullet');

  if (isColor(data.color) === true) {
    bullet.setAttribute('style', `background-color: ${data.color};`);
  } else {
    bullet.addClass(data.color);
  }

  const caption = new DomElement('span').setHtml(data.title);

  return new DomElement('li').appendChild(bullet).appendChild(caption).element;
}

export function isColor(str) {
  const pattern = /^#/i;
  return pattern.test(str);
}
