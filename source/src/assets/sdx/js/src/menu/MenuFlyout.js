import { TweenLite, Power1, Power4 } from "gsap"
import Popper from "popper.js"

import DomElement from "../DomElement"
import { addClass, hasClass, removeClass, isHidden, parentWithClass } from "../DomFunctions"

const CLASS_OPEN = "is-open"
const CLASS_MENU = "js-flyout"
const CLASS_TABS = "tabs"

const ANIMATION_OPEN = 0.3

/**
 * A component for the flyout menu.
 */
class MenuFlyout extends DomElement {

  /**
   * Creates and initializes the flyout component.
   * @param {DomElement} - The root element of the flyout menu component.
   */
  constructor(element) {
    super(element)

    this._clickHandler = this._handleClick.bind(this)
    this._windowClickHandler = this._handleWindowClick.bind(this)

    this._animationDuration = ANIMATION_OPEN

    this._initialize()
  }

  /**
   * Initializes the flyout component.
   * @private
   */
  _initialize() {
    let dataTarget = this.element.getAttribute("data-target")
    if (dataTarget === null || dataTarget === "") {

      /* eslint-disable no-console */
      console.error("A flyout menu element requires a 'data-target' that specifies the element to collapse")
      console.info(this.element)
      /* eslint-enable no-console */
      return
    }

    if (this._useDynamicPlacement()) {
      this._dynamicPlacement = true
    }

    let hiddenTarget = this.element.getAttribute("data-hidden")
    if (hiddenTarget !== null && hiddenTarget !== "") {
      this._hiddenIndicator = document.querySelector(hiddenTarget)
    }

    this._flyoutElement = document.querySelector(dataTarget)
    this.element.addEventListener("click", this._clickHandler)

  }

  _handleClick() {
    this.toggle()
  }

  _handleWindowClick(event) {
    let target = event.target

    if (parentWithClass(target, CLASS_MENU) === this._flyoutElement) {
      return false
    }

    while (target !== this.element && target.parentElement) {
      target = target.parentElement
    }

    if (target !== this.element) {
      this.close()
      return false
    }

    return true
  }

  _useDynamicPlacement() {
    return parentWithClass(this.element, CLASS_TABS)
  }

  _openMenu(el) {
    TweenLite.killTweensOf(el)

    TweenLite.set(el, {
      display: "block"
    })

    if (this._dynamicPlacement === true) {
      const popperOptions = {
        placement: "bottom",
        flip: {
          enabled: false
        },
        eventsEnabled: false
      }

      this._popperInstance = new Popper(this.element, this._flyoutElement, popperOptions)
    }

    TweenLite.to(el, this._animationDuration, {
      className: `+=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ]
    })

    // set aria expanded
    el.setAttribute("aria-expanded", true)

    this.dispatchEvent("opened")
  }

  _closeMenu(el) {
    TweenLite.killTweensOf(el)

    if (this._popperInstance) {
      this._popperInstance.destroy()
      this._popperInstance = undefined
    }

    TweenLite.to(el, ANIMATION_OPEN, {
      className: `-=${CLASS_OPEN}`,
      ease: [
        Power1.easeIn, Power4.easeOut
      ],
      onComplete: () => {
        TweenLite.set(el, {
          clearProps: "display"
        })
      }
    })

    // set aria expanded
    el.setAttribute("aria-expanded", false)

    this.dispatchEvent("closed")
  }

  /**
   * Sets the opening animation duration.
   * @param {durationInSeconds} - The animation duration in seconds.
   */
  set animationDuration(durationInSeconds) {
    this._animationDuration = durationInSeconds
  }

  /**
   * Opens the flyout menu.
   * @fires Modal#opened
   */
  open() {
    if (this._hiddenIndicator && isHidden(this._hiddenIndicator, false) === true) {
      return
    }

    if (hasClass(this.element, CLASS_OPEN) === true) {
      return
    }

    addClass(this.element, CLASS_OPEN)
    this._openMenu(this._flyoutElement)

    setTimeout(() => {
      window.addEventListener("click", this._windowClickHandler)
      window.addEventListener("touchend", this._windowClickHandler)
    }, 50)
  }

  /**
   * Closes the flyout menu.
   * @fires Modal#closed
   */
  close() {
    if (this._hiddenIndicator && isHidden(this._hiddenIndicator, false) === true) {
      return
    }

    if (hasClass(this.element, CLASS_OPEN) === false) {
      return
    }

    removeClass(this.element, CLASS_OPEN)

    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)

    this._closeMenu(this._flyoutElement)
  }

  /**
   * Toggles the flyout menu.
   * @fires Modal#opened
   * @fires Modal#closed
   */
  toggle() {
    if (hasClass(this.element, CLASS_OPEN) === false) {
      this.open()
    } else {
      this.close()
    }
  }

  /**
   * Removes all event handlers and clears references.
   */
  destroy() {
    this._flyoutElement = null

    window.removeEventListener("click", this._windowClickHandler)
    window.removeEventListener("touchend", this._windowClickHandler)

    if (this._clickHandler) {
      this.element.removeEventListener("click", this._clickHandler)
    }

    if (this._popperInstance) {
      this._popperInstance.destroy()
      this._popperInstance = undefined
    }

    this._clickHandler = null
    this._windowClickHandler = null
    this.element = null
  }

  /**
   * Fired when the flyout menu is opened by the anchor link or using the
   * {@link MenuFlyout#open} method.
   * @event MenuFlyout#opened
   * @type {object}
   */

  /**
   * Fired when the flyout menu is closed by the user or using the
   * {@link MenuFlyout#close} method.
   * @event MenuFlyout#closed
   * @type {object}
   */
}

export function init() {
  let elements = document.querySelectorAll("[data-toggle='flyout']")
  for (let e of elements) {
    if (e.getAttribute("data-init") === "auto") {
      new MenuFlyout(e)
    }
  }
}

export default MenuFlyout
