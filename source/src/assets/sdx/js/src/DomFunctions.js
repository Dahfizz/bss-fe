/* eslint-disable */
export function addClass(element, name) {
  if (typeof name !== 'string') {
    throw new Error('Expected string class name');
  }

  element.classList.add(name);
}

export function removeClass(element, name) {
  if (typeof name !== 'string') {
    throw new Error('Expected string class name');
  }

  element.classList.remove(name);
}

export function hasClass(element, name) {
  if (typeof name !== 'string') {
    throw new Error('Expected string class name');
  }

  return element.classList.contains(name);
}

export function toggleClass(element, name) {
  if (typeof name !== 'string') {
    throw new Error('Expected string class name');
  }

  element.classList.toggle(name);
}

/**
 * Determines if the given element is hidden from view.
 * @param {Element} Element The dom element to check.
 * @param {boolean} includeParents If set to `true` searches up the DOM and checks parent visibility as well. Defaults to `false`.
 */
export function isHidden(element, includeParents = false) {
  if (includeParents === false) {
    const style = window.getComputedStyle(element);
    return style.display === 'none' || element.offsetLeft < 0;
  }

  let result;
  while (
    (result = isHidden(element, false)) === false &&
    element.parentElement
  ) {
    element = element.parentElement;
  }

  return result;
}

/**
 * Gets the text of an element an makes sure this works on all browsers.
 */
export function text(element) {
  return element.textContent || element.innerText;
}

export function parentWithClass(element, className) {
  let current = element;

  while (!hasClass(current, className) && current.parentElement) {
    current = current.parentElement;
  }

  if (hasClass(current, className)) {
    return current;
  }

  return undefined;
}

export function textWidth(text, font) {
  // NOTE: this width measuring algorithm is a lot faster
  // but does unfortunately not work on IE 10...

  // let canvas = document.createElement("canvas")
  // let context = canvas.getContext("2d")
  // context.font = font
  // let metrics = context.measureText(text)
  // return Math.round(metrics.width)

  let div = document.createElement('div');
  div.innerHTML = text;

  div.style.font = font;
  div.style.position = 'absolute';
  div.style.left = -10000;
  div.style.right = -10000;
  div.style.visibility = 'hidden';

  document.body.appendChild(div);

  const result = div.offsetWidth;
  document.body.removeChild(div);

  return result;
}

export function css(element, property) {
  return window.getComputedStyle(element, null).getPropertyValue(property);
}

/**
 * Gets the single element referenced in an items data-* attribute.
 * @param {DomElement} element - The element containing the reference attribute.
 * @param {string} attribute - The name of the reference attribute.
 * @returns {DomElement} The referenced element; or `undefined` if the reference is invalid
 * or the attribute could not be found.
 */
export function getAttributeReference(element, attribute) {
  const attrValue = element.getAttribute(attribute);

  if (!attrValue || attrValue === '') {
    return undefined;
  }

  return document.querySelector(attrValue);
}

/**
 * Gets the document root element (normally the body element)
 * If the document uses a sdx-container wrapper this is returned instead.
 * @returns {Element} The root dom element.
 */
export function getRootElement() {
  let element = document.querySelector('.sdx-container');
  if (!element) {
    element = document.body;
  }

  return element;
}

/**
 * Removes all child nodes from the provided element.
 * @param {Element} element The Dom element
 */
export function empty(element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}
