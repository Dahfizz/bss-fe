/* eslint-disable */
import * as Dom from './DomFunctions';

/* global htmlEvents:true */

/**
 * A wrapper class for DOM Elements.
 */
class DomElement {
  /**
   * Creates a new instance.
   * @param {Element} - The element to wrap.
   * @param {String} - The DOM element to create.
   */
  constructor(element) {
    if (typeof element === 'string') {
      this.element = document.createElement(element);
    } else {
      this.element = element;
    }
  }

  /**
   * Adds the specified CSS class to the element.
   * @param {String} - The class name to add.
   * @return {DomElement} Returns the current instance for fluent chaining of calls.
   */
  addClass(name) {
    Dom.addClass(this.element, name);
    return this;
  }

  /**
   * Removes the specified CSS class from the element.
   * @param {String} - The class name to remove.
   * @return {DomElement} Returns the current instance for fluent chaining of calls.
   */
  removeClass(name) {
    Dom.removeClass(this.element, name);
    return this;
  }

  hasClass(name) {
    return Dom.hasClass(this.element, name);
  }

  toggleClass(name) {
    Dom.toggleClass(this.element, name);
    return this;
  }

  get classes() {
    return this.element.classList;
  }

  setId(id) {
    this.element.setAttribute('id', id);
    return this;
  }

  get innerText() {
    return Dom.text(this.element);
  }

  get innerHtml() {
    return this.element.innerHTML;
  }

  setHtml(value) {
    if (typeof value !== 'string') {
      throw new Error('Expected HTML string');
    }

    this.element.innerHTML = value;
    return this;
  }

  getAttribute(name) {
    return this.element.getAttribute(name);
  }

  setAttribute(name, value) {
    this.element.setAttribute(name, value);
    return this;
  }

  addEventListener(name, event) {
    this.element.addEventListener(name, event);
  }

  appendChild(newChild) {
    if (!(newChild instanceof DomElement)) {
      throw new Error('Only other DomElements can be added as children');
    }

    this.element.appendChild(newChild.element);
    return this;
  }

  prependChild(newChild) {
    if (!(newChild instanceof DomElement)) {
      throw new Error('Only other DomElements can be added as children');
    }

    this.element.insertBefore(newChild.element, this.element.firstChild);
    return this;
  }

  insertBefore(newChild) {
    if (!(newChild instanceof DomElement)) {
      throw new Error('Only other DomElements can be added as children');
    }

    this.element.parentNode.insertBefore(newChild.element, this.element);
    return this;
  }

  insertAfter(newChild) {
    if (!(newChild instanceof DomElement)) {
      throw new Error('Only other DomElements can be added as children');
    }

    this.element.parentNode.insertBefore(
      newChild.element,
      this.element.nextSibling
    );
    return this;
  }

  removeChild(oldChild) {
    if (!(oldChild instanceof DomElement)) {
      throw new Error('Only a DomElements child can be removed');
    }

    this.element.removeChild(oldChild.element);
  }

  find(selectors) {
    let e = this.element.querySelector(selectors);
    if (e) {
      return new DomElement(e);
    }

    return undefined;
  }

  wrapWithElement(wrapperElement) {
    this.element.parentNode.replaceChild(wrapperElement.element, this.element);
    wrapperElement.element.appendChild(this.element);

    return this;
  }

  dispatchEvent(eventName) {
    let event;
    let el = this.element;

    if (document.createEvent) {
      event = document.createEvent('HTMLEvents');
      event.initEvent(eventName, true, true);
    } else if (document.createEventObject) {
      // IE < 9
      event = document.createEventObject();
      event.eventType = eventName;
    }
    event.eventName = eventName;
    if (el.dispatchEvent) {
      el.dispatchEvent(event);
    } else if (el.fireEvent && htmlEvents[`on${eventName}`]) {
      // IE < 9
      el.fireEvent(`on${event.eventType}`, event); // can trigger only real event (e.g. 'click')
    } else if (el[eventName]) {
      el[eventName]();
    } else if (el[`on${eventName}`]) {
      el[`on${eventName}`]();
    }
  }

  css(property) {
    return Dom.css(this.element, property);
  }

  /**
   * Removes all child nodes of the current DomElement.
   */
  empty() {
    while (this.element.firstChild) {
      this.element.removeChild(this.element.firstChild);
    }
  }
}

export default DomElement;
