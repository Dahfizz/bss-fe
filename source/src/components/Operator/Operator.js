// Copyright 2019 Swisscom Apps Team
// Copyright 2019 Parker Berberian - UNH-IOL - nfvlab@iol.unh.edu
import { mapGetters, mapMutations } from 'vuex'
import apiServices from '../../services/apiServices.js'

export default {
  name: 'Operator',
  data () {
    return {
      orderDetails: '',
      logs: ''
    }
  },
  methods: {
    ...mapMutations([
      'setOrders',
      'setOrder',
      'setCurrentStep',
      'setLogs',
      'startListeningOrdersStatus',
      'setOrderDetails'
    ]),
    async getOrderDetailsById (orderId) {
      const order = await apiServices.getOrderDetailsById(orderId)
      this.orderDetails = JSON.stringify(order, null, 4)
    },
    clearLocalStorage () {
      localStorage.clear()
    },

    setStateColor (stateId) {
      let color
      switch (stateId) {
        case 1:
          color = '#25B252'
          break
        case 2:
          color = '#FF8B2E'
          break
        case 3:
          color = '#25B252'
          break
        case 4:
          color = '#BE0000'
          break
        case 5:
          color = '#DD1122'
          break
        default:
          color = '#DD1122'
          break
      }
      return color
    },

    syntaxHighlight (json) {
      json = json
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
      return json.replace(
        /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
        function (match) {
          var cls = 'number'
          if (/^"/.test(match)) {
            if (/:$/.test(match)) {
              cls = 'key'
            } else {
              cls = 'string'
            }
          } else if (/true|false/.test(match)) {
            cls = 'boolean'
          } else if (/null/.test(match)) {
            cls = 'null'
          }
          return '<span class="' + cls + '">' + match + '</span>'
        }
      )
    },

    async deleteOrder (e, id) {
      // Prevent the container from showing the content
      e.stopPropagation()
      await apiServices.deleteOrder(id)
      this.updateOrders()
    },

    async updateOrders () {
      const orders = await apiServices.getOrders()
      this.setOrders(orders)
    }
  },
  computed: {
    ...mapGetters([
      'getOrders',
      'getSelectedProduct',
      'getProducts',
      'getCurrentStep',
      'getOrder',
      'getLogs'
    ])
  },
  async created () {
    this.updateOrders()
    setInterval(this.updateOrders, 10000)
  }
}
