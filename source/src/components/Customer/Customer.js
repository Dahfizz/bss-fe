// Copyright 2019 Swisscom Apps Team
// Copyright 2019 Parker Berberian - UNH-IOL - nfvlab@iol.unh.edu
import { mapGetters, mapMutations } from 'vuex'
import apiServices from '../../services/apiServices.js'
import Card from '../Card/Card.vue'
import Ordercard1 from '../Ordercard1/Ordercard1.vue'
import OrderForm from '../OrderForm/OrderForm.vue'

export default {
  name: 'Customer',
  components: {
    Card,
    Ordercard1,
    OrderForm
  },
  methods: {
    ...mapMutations([
      'setProducts',
      'setProduct',
      'setCurrentStep',
      'startListeningOrderStatus',
      'postProduct',
      'postOrder',
      'setOrder',
      'setOrders'
    ]),
    async selectProduct (product) {
      const result = await apiServices.postOrder(product.product_id)
      const order = await apiServices.getOrderById(result.orderid[0])
      this.setOrder(order)
      const orders = await apiServices.getOrders()
      this.setOrders(orders)
      this.setCurrentStep(1)
    },
    setStateColor (stateId) {
      let color
      switch (stateId) {
        case 1:
          color = '#25B252'
          break
        case 2:
          color = '#FF8B2E'
          break
        case 3:
          color = '#25B252'
          break
        case 4:
          color = '#BE0000'
          break
        case 5:
          color = '#DD1122'
          break
        default:
          color = '#DD1122'
          break
      }
      return color
    }
  },
  computed: {
    ...mapGetters([
      'getProducts',
      'getCurrentStep',
      'isBoring',
      'getSelectedProduct',
      'getMoveAvailable',
      'getOrder'
    ])
  },
  async created () {
    const products = await apiServices.getProducts()
    this.setProducts(products)
  }
}
