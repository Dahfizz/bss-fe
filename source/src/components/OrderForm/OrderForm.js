// Copyright 2019 Swisscom Apps Team
// Copyright 2019 Parker Berberian - UNH-IOL - nfvlab@iol.unh.edu
import VendorContainer from '../VendorContainer/VendorContainer.vue'
import { mapGetters, mapMutations } from 'vuex'
import apiServices from '../../services/apiServices'

export default {
  name: 'OrderForm',
  data: function () {
    return {
      ont_vendor: '',
      ont_serial_number: '',
      pnf_name: '',
      rgw_mac: '',
      s_vlan: '',
      remote_id: '',
      c_vlan: '',
      up_speed: '10',
      down_speed: '10',
      product_id: 2,
      nhood: '',
    }
  },
  components: {
    VendorContainer
  },
  methods: {
    ...mapMutations([
      'setCurrentStep',
      'setOrder'
    ]),
    async submitForm () {
      var v1 = this.$data.ont_serial_number.charAt(this.$data.ont_serial_number.length - 1)
      v1 = parseInt(v1, 16)
      if (this.$data.c_vlan == "") this.$data.c_vlan = this.$data.nhood + "" + v1 //concats last digit of SN and cvlan
      if (this.$data.remote_id == "") this.$data.remote_id = this.$data.s_vlan + ":" + this.$data.c_vlan
      this.$data.product_id = this.getSelectedProduct.product_id
      const orderId = await apiServices.postOrder(this.$data)
      const order = await apiServices.getOrderById(orderId.orderid)
      this.setOrder(order)
      this.setCurrentStep(2)
    },
    populateForm (vendor) {
      this.rgw_mac = vendor.rgw_mac
      this.ont_vendor = vendor.name
      this.ont_serial_number = vendor.serial
      this.s_vlan = vendor.s_vlan
      this.c_vlan = vendor.c_vlan
      this.remote_id = vendor.remote_id
      this.pnf_name = vendor.pnf_name
      this.up_speed = this.getSelectedProduct.product_speed
      this.down_speed = this.getSelectedProduct.product_speed
    },
    clearLocalStorage () {
      this.setCurrentStep(0)
      localStorage.clear('state')
    }
  },
  computed: {
    ...mapGetters(['getSelectedProduct'])
  }
}
