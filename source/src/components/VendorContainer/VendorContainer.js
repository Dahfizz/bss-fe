// Copyright 2019 Swisscom Apps Team
// Copyright 2019 Brandon Lo - UNH-IOL - nfvlab@iol.unh.edu
import apiServices from '../../services/apiServices.js'

export default {
  name: 'VendorContainer',
  data: function () {
    return {
      vendors: []
    }
  },
  async created () {
    this.vendors = await apiServices.getVendors()
  },
  methods: {
    populateForm (vendor) {
      this.$emit('populateForm', vendor)
    }
  }
}
