// Copyright 2019 Swisscom Apps Team
// Copyright 2019 Parker Berberian - UNH-IOL - nfvlab@iol.unh.edu
import { mapGetters, mapMutations, mapActions } from 'vuex'
import apiServices from '../../services/apiServices.js'

export default {
  name: 'Card',
  props: {
    item: Object
  },
  methods: {
    ...mapMutations([
      'setProducts',
      'setProduct',
      'setCurrentStep',
      'postProduct',
      'postOrder',
      'setOrder',
      'setProcessingClass',
      'setMainText',
      'setCardBoarder'
    ]),
    ...mapActions(['startListeningOrderStatus']),
    selectMoveCPE (productId) {
      // This shouldn't be called
      this.setCurrentStep(2)
      apiServices.getMoveCPE(productId)
      this.setProcessingClass('progress_circle_in_progress')
      this.setCardBoarder('card--message-important')
      this.setMainText(
        'Thank you for your order we are working on it. Please stand by....'
      )
      this.startListeningOrderStatus()
    },
    clearLocalStorage () {
      this.setCurrentStep(0)
      localStorage.clear('state')
    },
    startListeningOrderStatus () {
      // This method starts polling for the state
      // of the current order
      // Polling is stopped when we go to a different step
      console.log('start polling for order')
      var interval = setInterval(async () => {
        const order = await apiServices.getOrderById(this.getOrder.order_id)
        console.log('order', order)
        this.setOrder(order) // visually updates if the order state changed

        if (this.getCurrentStep < 2) {
          clearInterval(interval)
        }
      }, 1000)
    }
  },
  computed: {
    ...mapGetters([
      'getProducts',
      'getCurrentStep',
      'getSelectedProduct',
      'getMoveAvailable',
      'getOrder',
      'getProcessingClass',
      'getMainText',
      'getCompletedClass',
      'getCompletedLineClass',
      'getCompletedText',
      'getCardBoarder'
    ])
  },
  created () {
    this.startListeningOrderStatus()
  }
}
