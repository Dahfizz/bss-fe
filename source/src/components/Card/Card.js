// Copyright 2019 Swisscom Apps Team
// Copyright 2019 Parker Berberian - UNH-IOL - nfvlab@iol.unh.edu
import { mapGetters, mapMutations } from 'vuex'
import apiServices from '../../services/apiServices.js'

export default {
  name: 'Card',
  props: {
    product: Object
  },
  data () {
    return {}
  },
  methods: {
    ...mapMutations([
      'setProducts',
      'setProduct',
      'setCurrentStep',
      'startListeningOrderStatus',
      'postProduct',
      'postOrder',
      'setOrder'
    ]),
    async selectProduct (product) {
      this.setProduct(product)
      this.setCurrentStep(1) // advance to next step
      console.log(this.getProduct)
    }
  },
  computed: {
    ...mapGetters(['getProducts', 'getCurrentStep'])
  }
}
