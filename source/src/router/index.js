import Vue from 'vue'
import Router from 'vue-router'
import { Customer, Operator } from '@/components'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Customer',
      component: Customer
    },
    {
      path: '/operator',
      name: 'Operator',
      component: Operator
    }
  ]
})
