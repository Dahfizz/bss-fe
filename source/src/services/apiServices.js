import httpServiceApp, { config } from './httpServiceApp'
import 'babel-polyfill'

const getProducts = async () => {
  const result = await httpServiceApp.get('products', config)
  console.log(result.data)
  return result.data
}
const getOrders = async () => {
  const result = await httpServiceApp.get('orders')
  return result.data
}
const getOrderById = async orderId => {
  const result = await httpServiceApp.get('order' + '/' + orderId)
  return result.data.orders[0]
}
const getOrderDetailsById = async orderId => {
  const result = await httpServiceApp.get('order' + '/' + orderId + '/details/')
  return result.data
}
const postOrder = async formData => {
  const result = await httpServiceApp.post('insertorder', formData)
  return result.data
}
const getPlugCPE = orderId => {
  httpServiceApp.get('plugcpe' + '/' + orderId)
}
const getMoveCPE = orderId => {
  httpServiceApp.get('movecpe' + '/' + orderId)
}
const getLogs = async () => {
  const result = await httpServiceApp.get('dmaapevents')
  return result.data
}
const getVendors = async () => {
  const result = await httpServiceApp.get('vendors')
  return result.data.vendors
}

const deleteOrder = async (id) => {
  await httpServiceApp.post('deleteorder', {
    id: id
  })
  // No response yet so we don't return anything
}

const apiServices = {
  getProducts,
  getOrders,
  getOrderById,
  postOrder,
  getPlugCPE,
  getMoveCPE,
  getLogs,
  getOrderDetailsById,
  getVendors,
  deleteOrder
}

export default apiServices
