import axios from 'axios'

const httpService = axios.create({
  baseURL: process.env.baseURL,
  mode: 'no-cors'
})

httpService.interceptors.response.use(
  response => response,
  error => {
    console.log('error: ', error)

    return Promise.reject(error)
  }
)

export default httpService
