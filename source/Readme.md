# bbs

> BBS UI Demo April 2019

## Build Setup

``` bash
# install dependencies
npm install

# configure
Ensure "baseURL" is set to the correct IP in config/prod.env.js

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Code Overview

    This dashboard is written in Vue. All the interesting code is in src/components and src/store/modules.
    The flow of the dashboard happens in "steps". The outer page is rendered and each inner component
    does some work and then changes the step, redrawing the page.
